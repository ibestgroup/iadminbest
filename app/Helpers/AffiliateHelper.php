<?php
namespace App\Helpers;

use App\Libraries\Domain;
use App\Models\Affilate;
use App\Models\HashPower;
use App\Models\MarksLevelInfo;
use App\Models\PaymentsOrder;
use App\Models\Users;
use Carbon\Carbon;
use App\Models\MarksInside;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class AffiliateHelper
{
    /**
     * @param $amount
     * @param string $type
     */

    public static function hashPower($amount, $type = 'payment', $db = '', $us_id = '') {
        $domain = new Domain();
        if ($db == '') {
            $db = $domain->getDB();
        }

        if ($us_id == '') {
            $us_id = Auth::id();
        }

        Config::set("database.connections.other", [
            'driver' => 'mysql',
            "host" => "localhost",
            "database" => $db,
            "username" => "root",
            "password" => "DrinkVodkaPlayDotka228",
        ]);

        $user = Users::on('other')->where('id', $us_id)->first();

//        echo $user['refer'];

        $parentsAll[1] = $user['refer'];
        for ($i = 1; $i < 8; $i++) {
            $parentsAll[$i+1] = Users::on('other')->where('id', $parentsAll[$i])->first()['refer'];
        }

        $hashPower = HashPower::on('other')->where('id',1)->first();
        if ($hashPower['status'] == 1) {

            if ($type == 'payment') {

                $resultSum = $amount/100*$hashPower->payment;
                Users::on('other')->where('id', $us_id)->increment('balance_hash_power', $resultSum);

            } elseif ($type == 'buy_order') {

                $resultSum = $amount/100*$hashPower->buy_order;
                Users::on('other')->where('id', $us_id)->increment('balance_hash_power', $resultSum);

            } elseif ($type == 'payment_ref') {

                for ($i = 1; $i < 8; $i++) {
                    if ($hashPower['level_'.$i] and $parentsAll[$i] != 1) {
                        $resultSum = $amount/100*$hashPower['level_'.$i];
                        Users::on('other')->where('id', $parentsAll[$i])->increment('balance_hash_power', $resultSum);
                    }
                }

            } elseif ($type == 'buy_ref') {

                for ($i = 1; $i < 8; $i++) {
                    if ($hashPower['level_buy_'.$i] and $parentsAll[$i] != 1) {
                        $resultSum = $amount / 100 * $hashPower['level_buy_' . $i];
                        Users::on('other')->where('id', $parentsAll[$i])->increment('balance_hash_power', $resultSum);
                    }
                }

            }
        }
    }

    /**
     * @param $amount
     */

    public static function referral($amount, $db, $us_id) {

        Config::set("database.connections.other", [
            'driver' => 'mysql',
            "host" => "localhost",
            "database" => $db,
            "username" => "root",
            "password" => "DrinkVodkaPlayDotka228"
        ]);
        $user = Users::on('other')->where('id', $us_id)->first();
        $affiliate = Affilate::on('other')->where('id', 1)->first();
        $parentsAll[1] = $user['refer'];

        for ($i = 1; $i < 8; $i++) {
            $parentsAll[$i+1] = Users::on('other')->where('id', $parentsAll[$i])->first()['refer'];
        }

        for ($i = 1; $i < 8; $i++) {
            if ($parentsAll[$i] != 1) {
                $resultSum = $amount/100*$affiliate['level_'.$i];
                Users::on('other')->where('id', $parentsAll[$i])->increment('balance_cashout', $resultSum);
                PaymentsOrder::on('other')->create([
                    'from_user' => $us_id,
                    'to_user' => $parentsAll[$i],
                    'from_wallet' => 0,
                    'wallet' => 0,
                    'from_level' => 0,
                    'level' => 0,
                    'amount' => $resultSum,
                    'date' => now(),
                    'direct' => 0,
                    'mark' => 0,
                    'type' => 'affiliate',
                ]);
            }
        }
    }

}