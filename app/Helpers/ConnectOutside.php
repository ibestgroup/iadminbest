<?php

$settings = array(
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'database',
    'username' =>'user',
    'password' => 'password',
    'collation' => 'utf8_general_ci',
    'charset'   => 'utf8',
    'strict'   => false,
    'prefix' => '',
);

//Create connection DB
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

$capsule = new Capsule;
$capsule->addConnection($settings);
$dispatcher = new Dispatcher(new Container);
$capsule->setEventDispatcher($dispatcher);
$capsule->setAsGlobal();
$capsule->bootEloquent();