<?php
namespace App\Helpers;

use App\Libraries\Helpers;
use App\Models\MarksLevelInfo;
use App\Models\OrdersMarkInside;
use App\Models\PaymentsOrder;
use App\Models\SiteInfo;
use Carbon\Carbon;
use App\Models\MarksInside;
use Illuminate\Support\Facades\Log;

class MarksHelper
{
    public static function calcMarkPrice($id, $level = 1) {
        $mark = MarksInside::whereId($id)->first();
        $marksLevelInfo = MarksLevelInfo::whereId($id)->first();
        $price = 0;

        if ($mark['type'] == 1) {
            $price = $mark['level'.$level] + $marksLevelInfo['to_par_'.$level] + $marksLevelInfo['to_admin_'.$level];
        } else {
            for ($i = 1; $mark['level'] >= $i; $i++) {
                $price += $mark['level'.$i] + $marksLevelInfo['to_par_'.$i] + $marksLevelInfo['to_admin_'.$i];
            }
        }

        $sumInviter = 0;
        for ($i = 1; $i <= MarksHelper::globalCount(); $i++) {
            $sumInviter = $sumInviter + $marksLevelInfo['to_inviter_' . $i];
        }

        if ($sumInviter > 0) {
            $price = $price + $sumInviter;
        }

        return $price;
    }

    public static function calcMarkPercent($balance, $mark) {
        return round($balance * 100 / self::markProfit($mark), 2);
    }

    public static function getMarkDays($start) {
        $cDate = Carbon::parse($start);
        return $cDate->diffInDays();
    }

    public static function markUsers($mark) {
        $people = 0;

        for ($i = 1; $i <= $mark->level; $i++) {
            $people += pow($mark->width, $i);
        }

        return $people;
    }

    public static function markUser($level, $width) {
        $people = 0;

        for ($i = 1; $i <= $level; $i++) {
            $people += pow($width, $i);
        }

        return $people;
    }

    public static function markName($id) {
        $mark = MarksInside::find($id);
        return $mark['name'];
    }

    public static function markStatus($id) {
        $mark = MarksInside::find($id);
        return $mark['status'];
    }

    public static function markProfit($id) {
        $mark = MarksInside::find($id);

        $level_price = ['', $mark['level1'], $mark['level2'], $mark['level3'], $mark['level4'], $mark['level5'], $mark['level6'], $mark['level7'], $mark['level8'], $mark['level9'], $mark['level10']];
        $width = $mark['width'];
        $level = $mark['level'];

        $profit = 0;

        $i = 1;
        while ($i < $level + 1) {
            $profit = $profit + ($level_price[$i] * pow($width, $i));
            $i++;
        }

        $profit = round($profit, 2);

        if ($id == 0) $profit = 0;

        return $profit;
    }

    public static function markPeople($id) {
        $mark = MarksInside::find($id);

        $width = $mark['width'];
        $level = $mark['level'];

        $people = 0;

        $i = 1;
        while ($i < $level + 1) {
            $people = $people + pow($width, $i);
            $i++;
        }

        if ($id == 0) $people = 0;

        return $people;
    }

    public static function  markReset($type = 1) {
        $count = MarksInside::all()->count();

        for ($i = 1; $i <= $count; $i++) {

            MarksInside::where('id', $i)
                ->update([
                    'name' => 'Тариф №'.$i,
                    'status' => 0,
                    'level' => 2,
                    'width' => 2,
                    'reinvest' => 0,
                    'overflow' => 1,
                    'type' => $type,
                    'to_par' => 0,
                    'to_admin' => 0,
                    'level1' => 10,
                    'level2' => 20,
                    'level3' => 30,
                    'level4' => 40,
                    'level5' => 50,
                    'level6' => 60,
                    'level7' => 70,
                    'level8' => 80,
                    'reinvest_first' => 0,
                    'only_reinvest' => 0
                ]);
        }

    }

    public static function profitLevel($id, $level, $admin = false) {

        $mark = MarksInside::find($id);
        $overflow = $mark->overflow;
        $profit = 0;
        $countPeople = 0;
        $multiplier = SiteInfo::find(1)->alter_fee ? 100 : 90;

        if ($admin) {
            for ($i = 1; $i <= $level; $i++) {
                $countPeople += pow($mark['width'], $i);
            }
            $profit = (Helpers::myRound($mark['level'.$level]/100*$multiplier) * 1) * $countPeople;
        } else {
            $countPeople = pow($mark['width'], $level);
            $profit = (Helpers::myRound($mark['level'.$level]/100*$multiplier) * 1) * pow($mark['width'], $level);
        }

        if ($overflow == 0) {
            $profit = 999999;
        }


        return $profit;
    }

    public static function reservedLevel($id, $level) {
        $mark = MarksInside::find($id);
        $marksLevelInfo = MarksLevelInfo::find($id);
        $overflow = $mark->overflow;
        $sumReinvest = 0;
        if ($marksLevelInfo['reinvest_type_'.$level] and $marksLevelInfo['reinvest_'.$level]) {
            $sumReinvest = self::priceReinvest($id, $level) * $marksLevelInfo['reinvest_type_'.$level];
        }

        if ($overflow == 0) {
            $sumReinvest = 0;
        }

        return self::priceNextLevel($id, $level) + $sumReinvest;
    }

    public static function cashoutLevel($id, $level, $admin = false) {
        return self::profitLevel($id, $level, $admin) - self::reservedAllForLevel($id, $level);
    }

    public static function priceNextLevel($id, $level) {

        $level = $level + 1;
        $mark = MarksInside::find($id);
        $marksLevelInfo = MarksLevelInfo::find($id);
        $price = 0;
        if ($mark['type'] == 1) {
            if ($level <= $mark->level) {
                $price = $mark['level'.$level] + $marksLevelInfo['to_par_'.$level] + $marksLevelInfo['to_admin_'.$level];
            }
        }

        return $price;
    }

    public static function priceReinvest($id, $level) {

        $marksLevelInfo = MarksLevelInfo::find($id);

        $price = 0;

        if ($marksLevelInfo['reinvest_type_'.$level] and $marksLevelInfo['reinvest_'.$level]) {
            $price = self::markEnter($marksLevelInfo['reinvest_'.$level]);
        }

        return $price;
    }


    public static function priceLevel($id, $level) {

        $mark = MarksInside::find($id);
        $marksLevelInfo = MarksLevelInfo::find($id);

        $price = 0;
        if ($level <= $mark->level) {
            $price = $mark['level'.$level] + $marksLevelInfo['to_par_'.$level] + $marksLevelInfo['to_admin_'.$level];
        }

        return $price;
    }

    public static function markInfo($id, $level, $name) {

        $name = preg_replace('/mark_level_/', '', $name);

        switch ($name) {
            case 'price_sum':
                return self::priceLevel($id, $level);
            case 'profit':
                return MarksHelper::profitLevel($id, $level);
            case 'people':
                return MarksHelper::peopleLevel($id, $level);
            case 'price':
                $mark = MarksInside::whereId($id)->first();
                return $mark['level' . $level];
            case 'reinvest':
                return MarksHelper::markName($id);
            default;
                break;
        }

        $marksLevelInfo = MarksLevelInfo::whereId($id)->first();

        return $marksLevelInfo[$name . '_' . $level];
    }

    public static function markEnter($id) {

        $mark = MarksInside::find($id);
        $marksLevelInfo = MarksLevelInfo::find($id);
        $result = 0;

        for ($i = 1; $i <= $mark['level']; $i++) {
            if ($mark['type'] == 2 or $i <= 1) {
                $result += $mark['level'.$i] + $marksLevelInfo['to_par_'.$i] + $marksLevelInfo['to_admin_'.$i];
            }
        }

        for ($i = 1; $i <= MarksHelper::globalCount('level'); $i++) {
            $result += $marksLevelInfo['to_inviter_'.$i];
        }

        return $result;
    }

    public static function reservedAllForLevel ($id, $level) {

        $marks = MarksInside::where('id', $id)->first();
        $marksLevelInfo = MarksLevelInfo::where('id', $id)->first();
        $reinvest = $marksLevelInfo['reinvest_'.$level];
        $reinvestType = $marksLevelInfo['reinvest_type_'.$level];
        $reinvestFirst = $marksLevelInfo['reinvest_first_'.$level];
        $reinvestFirstType = $marksLevelInfo['reinvest_first_type_'.$level];
        $reinvestLevel = 0;
        $reinvestFirstLevel = 0;

        if ($marks->type == 1) {
            $priceLevel = self::priceLevel($id, $level+1);
        } else {
            $priceLevel = 0;
        }

        if ($reinvestType and $reinvest) {
            $reinvestLevel = self::markEnter($reinvest) * $reinvestType;
        }

        if ($reinvestFirst) {
            $reinvestFirstLevel = self::markEnter($reinvestFirstType) * $reinvestFirst;
        }

        $result = $priceLevel + $reinvestLevel + $reinvestFirstLevel;

        return $result;

    }

    public static function marksError($type = 'bool') {
        $marks = MarksInside::all();
        $message = '';
        if (Helpers::siteType() == 1) {
            foreach ($marks as $mark) {
                $markLevelInfo = MarksLevelInfo::where('id', $mark->id)->first();
                if (MarksHelper::markEnter($mark->id) == 0 and $mark->status > 0) {
                    $message .= "Тариф \"$mark->name\" (#$mark->id)<br>Цена не может быть нулевой <br>";
                }


                for ($i = 1; $i <= $mark->level; $i++) {

                    $profitLevel = round(self::profitLevel($mark->id, $i), Helpers::siteCurrency('round'));

                    $reservedLevel = round(self::reservedAllForLevel($mark->id, $i), Helpers::siteCurrency('round'));


                    if ($profitLevel < $reservedLevel) {
                        if ($type == 'bool') {
                            if ($mark->overflow != 0) {
                                return true;
                            }
                        }

                        if ($mark->overflow != 0) {
                            $message .= "Тариф \"$mark->name\" (#$mark->id)<br>На уровне $i  ошибка: \"Доход уровня $i меньше чем весь его резерв (следующий уровень или другие тарифы).\" <br>";
                        }

                    } else {
    //                    $message .= "На уровне $i нет ошибок<br>";
                    }
                }

                for ($i = 1; $i <= MarksHelper::globalCount(); $i++) {

                    if ($markLevelInfo['to_inviter_'.$i] < 0 or
                        $markLevelInfo['to_admin_'.$i] < 0 or
                        $markLevelInfo['to_par_'.$i] < 0 or
                        $markLevelInfo['to_inviter_'.$i] < 0 or
                        $mark['level'.$i] < 0) {
                        if ($type == 'bool') {
                            if ($mark->overflow != 0) {
                                return true;
                            }
                        }
                    }
                }
    //            $message .= "<br>";
            }
        }

        return $message;
    }


    public static function peopleLevel($id, $level) {
        $mark = MarksInside::find($id);

        $width = $mark['width'];
        $people = pow($width, $level);

        if ($id == 0) $people = 0;

        return $people;
    }

    public static function checkClosed($tarifId) {
        $tarif = OrdersMarkInside::where('id', $tarifId)->first();
        $peopleNumbers = self::markPeople($tarif->mark);
        $peopleNumbersNow = PaymentsOrder::where('wallet', $tarif->id)->count();

        if ($peopleNumbersNow < $peopleNumbers) {
            return false;
        } else {
            return true;
        }

    }

    public static function checkForDelete($id, $mess = 0) {
        $orderCount = OrdersMarkInside::where('mark', $id)->whereRaw('id != par_or')->count();

        $result = true;
        $message = [];

        if ($orderCount) {
            $result = false;
            $message[] = 'Есть активированные тарифы';
        }

        $marksInside = MarksInside::all();
        $flag = 0;
        foreach ($marksInside as $mark) {
            $levelInfo = MarksLevelInfo::where('id', $mark->id)->first();
            for ($i = 1; $i <= self::globalCount(); $i++) {
                if ($levelInfo->{'reinvest_'.$i} == $id) {
                    $result = false;
                    if ($flag == 0) {
                        $message['error_reinvest'] = 'В этот тариф есть реинвесты с других тарифов:';
                    }
                    $message['error_reinvest'] .= '<br>'.$mark->name." уровень $i";
                    $flag = 1;
//                    break;
                }
            }
            if ($flag) break;
        }

        if ($id == 1) {
            $result = false;
            $message = ['Первый тариф нельзя удалить'];
        }

        if ($mess) {
            return $message;
        }

        return $result;

    }

    public static function globalCount($type = 'level') {
        if ($type == 'level') {
            return 10;
        }
        return 1;
    }

}