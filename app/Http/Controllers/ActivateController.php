<?php

namespace App\Http\Controllers;

use App\Helpers\MarksHelper;
use App\Libraries\ActivateAuto;
use App\Libraries\Activate;

use App\Libraries\Domain;
use App\Libraries\Helpers;
use App\Models\MarksInside;
use App\Models\OrdersMarkInside;
use App\Models\PaymentsOrder;
use App\Models\SiteInfo;
use App\Models\Transaction;
use App\Models\Users;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Nexmo\User\User;

class ActivateController extends Controller
{

    public $db;
    public $userId;
    public $markId;
    public $mark;
    public $siteInfo;

    public function create() {

        $marks = MarksInside::where('status', 2)->get();

        return view('templates/core/activate', compact('marks'));
    }

    public function activate(Request $request) {

        if (MarksHelper::marksError()) {
            return Response::json([
                "success" => false,
                "message" => "Ошибка #203655. Есть ошибка в одном из маркетингов. Перейдите в админ панель, чтобы исправить её.",
            ], 200, [], JSON_UNESCAPED_UNICODE);
        }

        $domain = new Domain();

        $this->db = $domain->getDB();
        $this->mark = MarksInside::whereId($request->input('tarif'))->first();
        $this->markId = $this->mark->id;
        $this->siteInfo = SiteInfo::whereId(1)->first();
        $this->userId = $request->input('us_id') ?? Auth::id();

        return $this->activateQueue($this->markId, $this->userId, $this->db);
    }

    public function activateQueue($markId, $userId, $db = '', $auto = false) {

        $domain = new Domain();

        $siteInfo = SiteInfo::find(1);
        $mark = MarksInside::whereId($markId)->first();
        $db = $db ?: $domain->getDB();
        $userId = $userId ?: $this->userId;
        $user = Users::find($userId);

        $flag = 0;
        if (!$mark->required_mark) {
            $requiredMarks = MarksInside::where('required_mark', 1);
            if ($requiredMarks->count()) {
                foreach ($requiredMarks->get() as $item) {
                    if ($flag == 1) {
                        break;
                    }
                    if (OrdersMarkInside::where('id_user', $userId)->where('mark', $item->id)->count()) {
                        $flag = 1;
                    }
                }
                if (!$flag) {
                    return Response::json([
                        "success" => false,
                        "message" => "У вас не активировано ниодного обязательного тарифа"
                    ], 200, [], JSON_UNESCAPED_UNICODE);
                }

            }
        }

        if (!$auto) {
            if ($user->privelege < 2 and $siteInfo->start == 0 and $userId != 1) {
                return Response::json([
                    "success" => false,
                    "message" => "Активации закрыты. Дождитесь старта проекта"
                ], 200, [], JSON_UNESCAPED_UNICODE);
            }
        }
        if ($mark['status'] < 2 and $this->userId != 1) {
            return Response::json([
                "success" => false,
                "message" => "Этот маркетинг нельзя активировать сразу."
            ], 200, [], JSON_UNESCAPED_UNICODE);
        }

        DB::beginTransaction();

        $user = Users::find($userId);

        $resultPrice = MarksHelper::calcMarkPrice($mark->id);
        $resultBalance = $user->balance;

        if ($resultBalance < $resultPrice) {
            DB::rollBack();
            return Response::json(["success" => false, 'message' => 'Недостаточно средств для активации'], 200, [], JSON_UNESCAPED_UNICODE);
        }

        try {
            Users::whereId($userId)->decrement('balance', $resultPrice);

            \App\Jobs\Activate::dispatch($userId, $mark->id, $db);

            DB::commit();

            if (!$auto) {
                return Response::json([
                    "success" => true,
                    "message" => "Активация добавлена в очередь.",
                ], 200, [], JSON_UNESCAPED_UNICODE);
            }

        } catch (\Exception $e) {
            Log::info($e);
            DB::rollBack();

            return Response::json([
                "success" => true,
                "message" => "Что-то пошло не так.",
            ], 200, [], JSON_UNESCAPED_UNICODE);
        }

    }

    public static function activatetest(Request $request)
    {

        if ($request->input('clear') == 'Y') {
            OrdersMarkInside::where('id', '>', 9)->delete();
            OrdersMarkInside::query()->update(['balance' => 0, 'status' => 0, 'level' => 1, 'cashout' => 0]);
            PaymentsOrder::query()->delete();
            Users::where('id', 1)->update(['balance' => 10000]);
            exit();
        }

        $siteInfo = SiteInfo::find(1);
        $mark = MarksInside::find(1);

//        if (Auth::user()['privelege'] < 2 and $siteInfo['start'] == 0) {
//            return Response::json([
//                "success" => false,
//                "message" => "Активации закрыты. Дождитесь старта проекта"
//            ], 200);
//        }

        if ($mark['only_reinvest'] == 1) {
            return Response::json([
                "success" => false,
                "message" => "Этот маркетинг нельзя активировать сразу."
            ], 200);
        }

        if ($siteInfo['mark_type'] == 3) {

                $activate = new Activate();

                if ($request->input('us_id')) {
                    $us_id = $request->input('us_id');
                } else {
                    $us_id = Auth::id();
                }

                DB::beginTransaction();
                $countRefNum = Users::where('ref_num', 10)->count();

                $i = 0;
                while ($countRefNum) {
                    $countRefNum = Users::where('ref_num', 10)->count();
                    $i++;
                    if ($i > 20) {
                        DB::rollBack();
                        return Response::json([
                            "success" => false,
                            "message" => "Слишком большая очередь, попробуйте снова"
                        ], 200);
                    }
                }

                Users::where('id', $us_id)->update([
                    'ref_num' => 10
                ]);

                DB::commit();

                try {
                    $activate->activatetest($us_id, $request->input('tarif'));
                    DB::beginTransaction();
                        Users::where('id', $us_id)->update([
                            'ref_num' => 0
                        ]);
                    DB::commit();
                    return Response::json([
                        "success" => true,
                        "message" => "Тариф активирован"
                    ], 200);
                } catch (\Exception $e) {
                    DB::rollBack();
                    return Response::json([
                        "success" => false,
                        "message" => $e->getMessage()
                    ], 200);
                }

        } elseif ($siteInfo['mark_type'] == 4) {

            $activate = new ActivateAuto();

            if ($request->input('us_id')) {
                $us_id = $request->input('us_id');
            } else {
                $us_id = Auth::id();
            }



            $tarif = (int)$request->input('tarif');

            try {
//                DB::transaction(function () use ($activate, $request, $us_id, $tarif) {
                    $activate->activateAuto($us_id, $tarif);
//                }, 5);
                return Response::json([
                    "success" => true,
                    "message" => "Тариф активирован"
                ], 200);
            } catch (\Exception $e) {
                return Response::json([
                    "success" => false,
                    "message" => $e->getMessage()
                ], 200);
            }

        }
    }
}
