<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class AdController extends Controller
{


    public static function adEdit()
    {
        $ad = Ad::find(1);
        return view('templates/adEdit', array(
            'ad' => $ad,
        ));
    }

    public static function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'text' => 'required'
        ], [
            'name.required' => 'Заполните вопрос',
            'text.lte' => 'Укажите ответ'
        ]);



        $ad = Ad::find(1);

        $ad->state = $request->status;
        $ad->type = $request->type;
        $ad->head = $request->name;
        $ad->text = $request->text;


        if ($ad->save()) {
            return Response::json([
                "message" => "Объявление сохранено"
            ], 200);
        }

        return Response::json([
            "message" => "Не получилось сохранить объявление"
        ], 422);

    }

}
