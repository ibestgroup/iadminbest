<?php

namespace App\Http\Controllers;

use App\Libraries\Activate;
use App\libraries\Constructor;
use App\Libraries\Helpers;
use App\libraries\Landing;
use App\Libraries\MD5Hasher;
use App\Libraries\Wallet;
use App\Models\Affilate;
use App\Models\AuthPages;
use App\Models\Birds;
use App\Models\BirdsOrder;
use App\Models\BonusPayment;
use App\Models\BonusSetting;
use App\Models\Cashout;
use App\Models\CashoutRequest;
use App\Models\CoreBuyTemplates;
use App\Models\CorePages;
use App\Models\CoreParking;
use App\Models\CoreSiteElements;
use App\Models\CoreTemplates;
use App\Models\CoreTemplatesOrder;
use App\Models\CoreUsers;
use App\Models\Currency;
use App\Models\HashPower;
use App\Models\Marks;
use App\Models\MarksInside;
use App\Models\MarksLevelInfo;
use App\Models\NotifyTheme;
use App\Models\OrdersMarkInside;
use App\Helpers\MarksHelper;
use App\Models\Pages;
use App\Models\Payments;
use App\Models\SiteBalance;
use App\Models\SiteElements;
use App\Models\SiteInfo;
use App\Models\SiteStart;
use App\Models\Start;
use App\Models\Templates;
use App\Models\Transaction;
use App\Models\TypesOrder;
use App\Models\Users;
use App\Models\PaymentsOrder;
use App\Models\StatisticVisit;
use App\Models\UsersData;
use Faker\Provider\Payment;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use App\Libraries\Domain;
use App\Libraries\Multimark;
use App\Models\Menu;
use App\Models\ProfilePageSetting;

class AdminCPController extends Controller
{
    public function __construct() {
        $this->domain = new Domain();
    }

    public function view() {

        $count['payments'] = Payments::all()->count();
        $count['payments_sum'] = Payments::sum('amount');
        $count['users'] = Users::all()->count();
        $count['activation_sum'] = PaymentsOrder::sum('amount');
        $count['activation'] = OrdersMarkInside::all()->count();
        $count['visit'] = StatisticVisit::sum('visit');
        $count['visit_prof'] = StatisticVisit::sum('visit_prof');

        $siteInfo = SiteInfo::find(1);

        $idSite = Helpers::getSiteId();

        $parking = CoreParking::where('site_id', $idSite)->first();

        $payments = Payments::orderBy('id', 'desc')->take(10)->get();
        $users = Users::orderBy('id', 'desc')->take(10)->get();
        $activations = OrdersMarkInside::orderBy('id', 'desc')->take(10)->get();

        return view("templates/admin/adminCP", compact('count', 'siteInfo', 'parking', 'payments', 'users', 'activations'));
    }

    public function chartPayments(Request $request) {

        $data = Payments::whereBetween('date', [
                                            new Carbon($request->input('after')),
                                            new Carbon($request->input('before'))
                                        ])->get();

        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function chartActivation(Request $request) {

        $data['count'] = OrdersMarkInside::whereBetween(DB::raw('date(date)'), [new Carbon($request->input('after')), new Carbon($request->input('before'))])->get();
        $data['amount'] = PaymentsOrder::where('to_user', Auth::id())->where('wallet', 'to_balance')->whereBetween(DB::raw('date(date)'), [new Carbon($request->input('after')), new Carbon($request->input('before'))])
            ->get();

        $data['step'] = round(count($data['amount']), -2)/10;

        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function chartVisit(Request $request) {

        $data = StatisticVisit::whereBetween('day', [new Carbon($request->input('after')), new Carbon($request->input('before'))])->get();

        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function chartRegister(Request $request) {

        $data = Users::where('refer', Auth::id())->whereBetween(DB::raw('date(regdate)'), [new Carbon($request->input('after')), new Carbon($request->input('before'))])->get();

        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function paymentsAdmin() {

        $payments = Payments::paginate(30);

        return view("templates/admin/paymentsAdmin", array(
            'payments' => $payments,
        ));
    }

    public function cashoutsAdmin() {

        $cashouts = Cashout::paginate(30);

        return view("templates/admin/cashoutsAdmin", array(
            'cashouts' => $cashouts,
        ));
    }

    public function loadUsers(Request $request) {

        $user = $request->input('user');
        $data = Users::where('login', 'like', '%'.$user.'%')->orWhere('id', $user)->get();

        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function allUsers(Request $request) {
        $users = Users::paginate(20);
        return view('templates/admin/allUsers', compact('users'));
    }

    public function userInfo($id) {
        $user = Users::find($id);
        $cashouts = Cashout::where('id_user', $id)->get();
        $payments = Payments::where('user_id', $id)->get();
        $referrals = Users::where('refer', $id)->get();

        $paymentOrders = OrdersMarkInside::where('id_user', $id)->get();

        if (Helpers::siteType() == 1) {
            $activates = OrdersMarkInside::where('id_user', $id)->get();
        } else {
            $activates = BirdsOrder::where('user_id', $id)->get();
        }

        return view('templates/admin/userInfo', compact('user', 'activates', 'cashouts', 'payments', 'paymentOrders', 'referrals'));
    }

    public function userStore(Request $request) {

        $user = Users::find($request->us_id);

        $user->privelege = $request->privelege;

        if ($user->save()) {
            return Response::json([
                "message" => "Данные сохранены"
            ], 200);
        }

    }

    public function markEdit() {

        $marks = MarksInside::all();
        $marksLevelInfo = MarksLevelInfo::all();

        $siteType = Helpers::siteType();

        if ($siteType > 1) {
            return redirect()->route('admin');
        }

        return view('templates.admin.markEditCustom', array(
            'marks' => $marks,
            'marksLevelInfo' => $marksLevelInfo
        ));

    }

    public function markEditNew() {

        $marks = MarksInside::all();
        $marksLevelInfo = MarksLevelInfo::all();

        $siteType = Helpers::siteType();

        if ($siteType > 1) {
            return redirect()->route('admin');
        }

        return view('templates.admin.markEditNew', array(
            'marks' => $marks,
            'marksLevelInfo' => $marksLevelInfo
        ));

    }

    public function markStore(Request $request) {

        $siteInfo = SiteInfo::find(1);

        for ($i = 1; $i <= MarksHelper::globalCount(); $i ++) {
            if ($request['level'.$i] < 0 or
                $request['to_par_'.$i] < 0 or
                $request['to_admin_'.$i] < 0 or
                $request['reinvest_'.$i] < 0 or
                $request['reinvest_type_'.$i] < 0 or
                $request['to_inviter_'.$i] < 0 or
                $request['reinvest_first_'.$i] < 0) {
                return Response::json([
                    "type" => "error",
                    "message" => "Сумма не может быть отрицательной"
                ], 200);
            }

            $checkMark = MarksInside::where('id', $request['reinvest_'.$i])->count();
            if (!$checkMark and $request['reinvest_'.$i] != 0) {
                return Response::json([
                    "type" => "error",
                    "message" => "Реинвест в несуществующий тариф"
                ], 200);
            }
        }

//        dd($request->mark_id);
        $ordersMark = OrdersMarkInside::where('mark', $request->mark_id)->whereRaw('id != par_or')->count();
        if ($ordersMark) {
            return Response::json([
                "type" => "error",
                "message" => "В этом тарифе уже есть активированные маркетинги, вы не можете его изменить"
            ], 200);
        }

        $item = MarksInside::find($request->mark_id);

        $item->name = $request->name;
        $item->status = $request->status;
        $item->level = $request->level;
        $item->width = $request->width;
        $item->reinvest = 0;
        $item->overflow = $request->overflow;
        $item->type = $request->type;
        $item->required_mark = $request->required_mark ? 1 : 0;


        $itemInfoLevels = MarksLevelInfo::find($request->mark_id);

        for ($i = 1; $i <= MarksHelper::globalCount(); $i++) {
            $item->{'level'.$i} = $request->{'level'.$i};


            $itemInfoLevels['to_par_'.$i] = ($request['to_par_'.$i])?$request['to_par_'.$i]:0;
            $itemInfoLevels['to_admin_'.$i] = ($request['to_admin_'.$i])?$request['to_admin_'.$i]:0;
            $itemInfoLevels['reinvest_type_'.$i] = ($request['reinvest_type_'.$i] and $item->overflow != 0)?$request['reinvest_type_'.$i]:0;
            $itemInfoLevels['reinvest_'.$i] = ($request['reinvest_'.$i] and $item->overflow != 0)?$request['reinvest_'.$i]:0;
            $itemInfoLevels['reinvest_first_'.$i] = ($request['reinvest_first_'.$i] and $item->overflow != 0)?$request['reinvest_first_'.$i]:0;
            $itemInfoLevels['reinvest_first_type_'.$i] = ($request['reinvest_first_type_'.$i] and $item->overflow != 0)?$request['reinvest_first_type_'.$i]:0;
            $itemInfoLevels['to_inviter_'.$i] = ($request['to_inviter_'.$i])?$request['to_inviter_'.$i]:0;

            if (($itemInfoLevels['reinvest_type_'.$i] > 10 or $itemInfoLevels['reinvest_first_'.$i] > 10) and $request->level >= $i) {
                return Response::json([
                    "type" => "error",
                    "message" => "Максимальное количество реинвестов 10",
                ], 200);
            }

            $arrayPriority = [
                1234,
                1342,
                1432,
                4321,
                3421,
                2134
            ];

            if (in_array($request['priority_'.$i], $arrayPriority) or $item->overflow == 0) {
                if ($item->overflow == 0) {
                    $itemInfoLevels['priority_'.$i] = 2134;
                } else {
                    $itemInfoLevels['priority_'.$i] = ($request['priority_'.$i])?$request['priority_'.$i]:0;
                }
            } else {
                return Response::json([
                    "type" => "error",
                    "message" => "Невозможный приоритет",
                ], 200);
            }
        }

        if ($item->save() AND $itemInfoLevels->save()) {
//                $constructor = new Constructor();
//                $constructor->updateUserPages();
            return Response::json([
                "message" => __('main.mark_saved'),
            ], 200);
        }

        return Response::json([
            "message" => "Не получилось сохранить маркетинг"
        ], 422);
    }

    public function markCreate(Request $request) {

        $mark = MarksInside::create([
            'name' => $request->name,
            'status' => 0,
            'level' => $request->level,
            'width' => $request->width,
            'reinvest' => 0,
            'overflow' => 1,
            'type' => 1,
            'level1' => 10,
            'level2' => 20,
            'level3' => 30,
            'level4' => 40,
            'level5' => 50,
            'level6' => 60,
            'level7' => 70,
            'level8' => 80,
        ]);

        MarksLevelInfo::create([
            'id' => $mark->id,
            'to_par_1' => 0,
            'to_par_2' => 0,
            'to_par_3' => 0,
            'to_par_4' => 0,
            'to_par_5' => 0,
            'to_par_6' => 0,
            'to_par_7' => 0,
            'to_par_8' => 0,
            'to_admin_1' => 0,
            'to_admin_2' => 0,
            'to_admin_3' => 0,
            'to_admin_4' => 0,
            'to_admin_5' => 0,
            'to_admin_6' => 0,
            'to_admin_7' => 0,
            'to_admin_8' => 0,
            'reinvest_type_1' => 0,
            'reinvest_type_2' => 0,
            'reinvest_type_3' => 0,
            'reinvest_type_4' => 0,
            'reinvest_type_5' => 0,
            'reinvest_type_6' => 0,
            'reinvest_type_7' => 0,
            'reinvest_type_8' => 0,
            'reinvest_1' => 0,
            'reinvest_2' => 0,
            'reinvest_3' => 0,
            'reinvest_4' => 0,
            'reinvest_5' => 0,
            'reinvest_6' => 0,
            'reinvest_7' => 0,
            'reinvest_8' => 0,
            'reinvest_first_1' => 0,
            'reinvest_first_2' => 0,
            'reinvest_first_3' => 0,
            'reinvest_first_4' => 0,
            'reinvest_first_5' => 0,
            'reinvest_first_6' => 0,
            'reinvest_first_7' => 0,
            'reinvest_first_8' => 0,
            'priority_1' => 1234,
            'priority_2' => 1234,
            'priority_3' => 1234,
            'priority_4' => 1234,
            'priority_5' => 1234,
            'priority_6' => 1234,
            'priority_7' => 1234,
            'priority_8' => 1234,
            'to_inviter_1' => 0,
            'to_inviter_2' => 0,
            'to_inviter_3' => 0,
            'to_inviter_4' => 0,
            'to_inviter_5' => 0,
            'to_inviter_6' => 0,
            'to_inviter_7' => 0,
            'to_inviter_8' => 0,
        ]);

        $order = OrdersMarkInside::create([
            'id_user' => 1,
            'parent' => 1,
            'mark' => $mark->id,
            'level' => 10,
            'balance' => 0,
            'pay_people' => 0,
            'status' => 0,
            'par_or' => 1,
            'date' => now(),
            'reinvest' => 0,
            'cashout' => 0,
            ]);

        $order->par_or = $order->id;
        $order->save();

        return Response::json([
            "type" => "success",
            "message" => "Тариф $request->name создан"
        ], 200);
    }

    public function markDelete(Request $request) {
        $markId = $request->mark_id;


        if (!MarksHelper::checkForDelete($markId)) {
            return Response::json([
                "type" => "error",
                "message" => "Нельзя удалить потому что: <br> -".implode('<br>-', MarksHelper::checkForDelete($markId,1)),
            ], 200);
        }

        MarksInside::where('id', $markId)->delete();
        MarksLevelInfo::where('id', $markId)->delete();
        OrdersMarkInside::where('mark', $markId)->delete();


        return Response::json([
            "type" => "warning",
            "message" => "Тариф $request->id удален"
        ], 200);
    }

    public function siteInfoEdit() {

        $siteInfo = SiteInfo::find(1);
        $siteStart = SiteStart::find(1);

        $siteStart['start_reg'] = date("Y-m-d\TH:i", strtotime($siteStart['start_reg']));
        $siteStart['start_pay'] = date("Y-m-d\TH:i", strtotime($siteStart['start_pay']));
        $bonuses = BonusPayment::all();
        $bonusSetting = BonusSetting::first();
        $affilate = Affilate::first();
        $hashPower = HashPower::first();
        $currency = Currency::first();
        $domain = new Domain();

        return view('templates/admin/siteInfoEdit', array(
            'siteInfo' => $siteInfo,
            'siteStart' => $siteStart,
            'bonuses' => $bonuses,
            'bonusSetting' => $bonusSetting,
            'affilate' => $affilate,
            'hashPower' => $hashPower,
            'currency' => $currency,
            'domain' => $domain->getDB(),
        ));

    }

    public function siteInfoStore(Request $request) {


        $item = SiteInfo::find(1);
        $domain = new Domain();


        $subBalance = SiteInfo::all()->sum('balance');
        if ($item['value'] != $request->value and $subBalance) {
            return Response::json([
                "message" => "Вы не можете сейчас изменить валюту сайта"
            ], 200);
        }

        if (!$item['start']) {
            $item->value = $request->value ?? $item->value;
        }

        if (isset($request->logo)) {
            $item->logo = $request->logo;
        }

        if (isset($request->payment_to)) {
            if ($request->payment_to == 0 or $request->payment_to == 1) {
                $item->payment_to = $request->payment_to;
            }
        }

        if (isset($request->payment_pay)) {
            if ($request->payment_pay == 0 or $request->payment_pay == 1) {
                $item->payment_pay = $request->payment_pay;
            }
        }

        if (isset($request->cashout_confirm)) {
            $item->cashout_confirm = $request->cashout_confirm;
        }

        if (isset($request->min_cashout)) {
            if ($request->min_cashout > 0 and $request->min_cashout <= 1000) {
                $item->min_cashout = $request->min_cashout;
            }
        }

        if (isset($request->max_cashout)) {
            if ($request->max_cashout > 1000 and $request->max_cashout <= 15000) {
                $item->max_cashout = $request->max_cashout;
            }
        }

        if (isset($request->cashout_limit_day)) {
            if ($request->cashout_limit_day > 10 and $request->cashout_limit_day <= 15000) {
                $item->cashout_limit_day = $request->cashout_limit_day;
            }
        }

        if (isset($request->cashout_limit_day_all)) {
            if ($request->cashout_limit_day_all > 10 and $request->cashout_limit_day_all <= 100000) {
                $item->cashout_limit_day_all = $request->cashout_limit_day_all;
            }
        }

        if (isset($request->cashout_before_start)) {
            if ($request->cashout_before_start == 0 or $request->cashout_before_start == 1) {
                $item->cashout_before_start = $request->cashout_before_start;
            }
        }

        if ($request->file('logo')) {
            $path = $request->file('logo')->store('sites/'.$domain->getDB());

            $item->update([
                'logo' => $path
            ]);
        }

        if ($request->file('no_image')) {

            $path = $request->file('no_image')->store('sites/'.$domain->getDB());

            $item->update([
                'no_image' => $path
            ]);
        }





        $item->name = $request->name;

        $itemStart = SiteStart::find(1);
        $siteType = Helpers::siteType();

        if (!$item['start'] and isset($request->start_pay)) {

            if (Domain::checkParking() or $siteType == 1) {

                $itemStart->start_reg = $request->start_reg ?? $itemStart->start_reg;
                $itemStart->start_pay = $request->start_pay ?? $itemStart->start_pay;

            } else {

                return Response::json([
                    "type" => "error",
                    "message" => "Вы не можете устанавливать дату старта, пока не припаркован домен.",
                ], 200);
            }
        }

        if ($item->save() and $itemStart->save()) {
            return Response::json([
                "message" => "Настройки сохранены"
            ], 200);
        }

    }

    public function currencyStore(Request $request) {
        $domain = new Domain();

        Currency::find(1)->update([
            'status' => $request->status,
            'price' => $request->price,
            'name' => $request->name,
        ]);

        if ($request->file('img')) {

            $path = $request->file('no_image')->store('sites/'.$domain->getDB());

            Currency::find(1)->update([
                'img' => $path,
            ]);

        }

        return Response::json([
            "message" => "Настройки валюты сохранены"
        ], 200);

    }

    public function storeMenu(Request $request) {


        if ($request->type == 'sidebar') {
            $id = 1;
        } elseif ($request->type == 'top_menu') {
            $id = 2;
        }

        Menu::find($id)->update([
            'styles' => $request->html_menu
        ]);


        return Response::json([
            "message" => ["type" => "success", "message" => "Меню сохранено"]
        ], 200);
    }

    public function storeSite(Request $request) {

        if (isset($request->width_full)) {
            $width = 1;
        } else {
            $width = 0;
        }

        ProfilePageSetting::find(1)->update([
            'width_full' => $width,
            'width' => $request->width,
            'bg_color' => $request->bg_color,
            'bg_img' => ($request->bg_img)?$request->bg_img:'',
            'sidebar_bg_color' => $request->sidebar_bg_color,
            'sidebar_bg_img' => ($request->sidebar_bg_img)?$request->sidebar_bg_img:'',
            'menu_bg_color' => $request->menu_bg_color,
            'menu_bg_img' => ($request->menu_bg_img)?$request->menu_bg_img:'',

            'header_width' => $request->header_width,
            'header_img' => ($request->header_img)?$request->header_img:'',
        ]);

        return Response::json([
            "message" => ["type" => "success", "message" => "Настройки сайта сохранены"]
        ], 200);
    }

    public function pages() {
//        $pages = Pages::all();
        $template = Helpers::siteTemplate();
        $pages = Pages::where('template', $template)->orWhere('id', 1)->get();

        return view('templates.admin.pages', compact('pages'));
    }

    public function pagesStore(Request $request) {
        $page = Pages::where('id', $request->id)->first();

        if ($request->type_post == 'access') {
            if ($request->id != 1) {
                Pages::where('id', $request->id)->update([
                    'access' => $request->access,
                    'condition' => $request->condition,
                ]);

                return Response::json([
                    "type" => "success",
                    "message" => "Доступ к странице изменен"
                ], 200);
            } else {

                return Response::json([
                    "type" => "error",
                    "message" => "Нельзя именить доступ к этой странице"
                ], 200);

            }
        }

        if ($request->edit == 'enable') {
            Pages::where('id', $request->id)->update(['status' => 1]);
            $message = 'включен';
        } elseif ($request->edit == 'disable') {
            Pages::where('id', $request->id)->update(['status' => 0]);
            $message = 'выключен';
        } elseif ($request->edit == 'delete') {
            if ($page->name == 'profile') {
                return Response::json([
                    "type" => "error",
                    "message" => "Это стандартная страница по умолчанию, ее невозможно удалить",
                ], 200);
            }

            $grids = SiteElements::where('page', $page->name)->where('type', 'grid')->get();
            foreach ($grids as $grid) {
                $elements = SiteElements::where('parent_id', $grid->id_in_system)->get();
                foreach ($elements as $element) {
                    SiteElements::where('id', $element->id)->delete();
                }
            }
            SiteElements::where('page', $page->name)->where('type', 'grid')->delete();

            Pages::where('id', $request->id)->delete();
            $message = 'удален';
        }

        return Response::json([
            "type" => "success",
            "message" => "Блок $page->name_ru $message"
        ], 200);
    }

    public function pagesAdd(Request $request) {
        $template = Helpers::siteTemplate();
        if ($request->name == '') {
            $name = Helpers::translit($request->name_ru);
        } else {
            $name = Helpers::translit($request->name);
        }

        $pageCount = Pages::where('name', $name)->count();
        if ($pageCount) {
            return Response::json([
                "type" => "error",
                "message" => "Такая страница уже существует. Выберите другое имя."
            ], 200);
        }

        Pages::create([
            'status' => $request->status,
            'native' => 0,
            'name' => $name,
            'name_ru' => $request->name_ru,
            'template' => $template,
        ]);

        $elementsCreate = SiteElements::create([
            'id_in_system' => 0,
            'type' => 'grid',
            'name' => 'content-'.$name,
            'name_ru' => 'Область для '.$request->name_ru,
            'parent_id' => 'content',
            'option_value' => 'a:1:{s:3:"col";s:2:"12";}',
            'page' => $name,
            'sort' => 0,
            'template' => Helpers::siteTemplate(),
        ]);
        SiteElements::where('id', $elementsCreate->id)->update(['id_in_system' => $elementsCreate->id]);

        return Response::json([
            "type" => "success",
            "message" => "Страница $request->name_ru создана"
        ], 200);
    }

    public function templates($id = 0) {

        $templateCount = 0;
        $siteType = Helpers::siteType();
        if ($id) {

            $templateCount = Templates::where('id', $id)->count();

            if ($templateCount) {

                $templateInfo = Templates::where('id', $id)->first();
                $grids = SiteElements::where('template', $id)->where('type', 'grid')->get();

                foreach ($grids as $key => $grid) {
                    $grid->children = SiteElements::where('parent_id', $grid['id_in_system'])->where('template', $id)->get();
                    foreach ($grid->children as $child) {
                        $child->option_value = preg_replace_callback ( '!s:(\d+):"(.*?)";!', function($match) {
                            return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
                        },$child->option_value );
                        $child->option_value = @unserialize($child->option_value);
                    }
                }

                $pages = Helpers::getPages($id);

                return view('templates/admin/templateInfo', compact('templateInfo', 'grids', 'pages'));

            }
        }


        $templates = Templates::all();
        $storeTemplates = CoreTemplates::where('type', $siteType)->where('status', 2)->get();
        $libraryTemplates = CoreBuyTemplates::where('user_id', Helpers::getSiteAdmin())->get();

        return view('templates/admin/templates', compact('templates', 'storeTemplates', 'libraryTemplates'));
    }

    public function templatesStore(Request $request) {
        if (isset($request->activate)) {
            Templates::where('id', '>', 0)->update(['status' => 0]);
            Templates::where('id', $request->id)->update(['status' => 1]);

            return Response::json([
                "type" => "success", "message" => "Шаблон активирован",
            ], 200);

        } elseif (isset($request->delete)) {

            if (Templates::where('id', $request->id)->first()->status) {
                return Response::json([
                    "type" => "error", "message" => "Вы не можете удалить включенный шаблон"
                ], 200);
            }

            SiteElements::where('template', $request->id)->delete();
            Templates::where('id', $request->id)->delete();

            return Response::json([
                "type" => "warning", "message" => "Шаблон удален"
            ], 200);

        }
    }

    public function templatesMarket(Request $request) {
        $siteType = Helpers::siteType();
        $siteAdmin = Helpers::getSiteAdmin();
        $type = $request->input('type');
        if (!isset($type)) {
            $type = 'all';
        }

        if ($type == 'free') {
            $storeTemplates = CoreTemplates::where('type', $siteType)->where('status', 2)->where('price', 0)->paginate(16);
        } else {
            $storeTemplates = CoreTemplates::where('type', $siteType)->where('status', 2)->paginate(16);
        }


        return view('templates/admin/templatesMarket', compact('storeTemplates', 'siteAdmin'));
    }

    public function templatesLibrary() {
        $siteType = Helpers::siteType();
        $siteAdmin = Helpers::getSiteAdmin();

        $libraryTemplates = CoreBuyTemplates::where('user_id', Helpers::getSiteAdmin())->paginate(16);
        return view('templates/admin/templatesLibrary', compact('libraryTemplates', 'siteAdmin'));
    }

    public function templateToStore(Request $request) {

        if (!isset($request->price)) {
            $request->price = 0;
        }

        $siteType = Helpers::siteType();

        if ($request->price < 0) {
            return Response::json([
                "type" => "error",
                "message" => "Цена не может быть отрицательной",
            ], 200);
        }

        if ($request->price > 500) {
            return Response::json([
                "type" => "error",
                "message" => "Максимальная цена 500 рублей за шаблон",
            ], 200);
        }

        $status = 0;
        if (isset($request->status)) {
            $status = $request->status;
        }

        $template = CoreTemplates::create([
            'user_id' => Helpers::getSiteAdmin(),
            'status' => $status,
            'type' => $siteType,
            'name' => $request->name ?? 'Название',
            'img' => $request->img ?? 'no_image',
            'price' => $request->price ?? 0,
            'date' => now(),
        ]);

        CoreBuyTemplates::create([
            'user_id' => Helpers::getSiteAdmin(),
            'template_id' => $template->id,
            'date' => strtotime(now())
        ]);

        Templates::where('id', $request->id)->update([
            'store' => $template->id,
        ]);

        $pages = Pages::where('template', $request->id)->get();
        $siteElements = SiteElements::where('template', $request->id)->get();



        foreach ($pages as $page) {
            CorePages::create([
                'status' => $page->status,
                'native' => $page->native,
                'name' => $page->name,
                'name_ru' => $page->name_ru,
                'template' => $template->id,
            ]);
        }

        foreach ($siteElements as $siteElement) {
            CoreSiteElements::create([
                'id_in_system' => $siteElement->id_in_system,
                'type' => $siteElement->type,
                'name' => $siteElement->name,
                'name_ru' => $siteElement->name_ru,
                'parent_id' => $siteElement->parent_id,
                'option_value' => $siteElement->option_value,
                'page' => $siteElement->page,
                'sort' => $siteElement->sort,
                'template' => $template->id,
            ]);
        }

        return Response::json([
            "type" => "success",
            "message" => "Шаблон добавлен в магазин",
        ], 200);

    }

    public function templatesDelete(Request $request) {
        $id = $request->id;
        $template = Helpers::siteTemplate();
        if (isset($request->template)) {
            $template = $request->template;
        }

        $element = SiteElements::where('id_in_system', $id)->where('template', $template)->first();

        if (in_array($element['parent_id'], ['right', 'left', 'top'])) {
            return Response::json([
                "type" => "error", "message" => "Нельзя удалять области для всех страниц"
            ], 200);
        }

        $count = SiteElements::where('parent_id', $id)->where('template', $template)->count();

        if ($count) {
            return Response::json([
                "type" => "error", "message" => "В этой области есть виджеты. Нельзя удалить"
            ], 200);
        }

        SiteElements::where('id_in_system', $id)->where('template', $template)->delete();

        return Response::json([
            "type" => "warning", "message" => "Элемент удален"
        ], 200);

    }

    public function templateDeleteFromLibrary(Request $request) {

    }

    public function templateMarketEdit(Request $request) {
        $templateId = $request->id;
        $price = (int)$request->price ?? 0;
        $template = CoreTemplates::where('id', $templateId)->first();

        if (Helpers::getSiteAdmin() != $template->user_id) {
            return Response::json([
                "type" => "error",
                "message" => "Вы не автор этого шаблона",
            ], 200);
        }

        if ($request->status >= 2 and $template->status < 2) {
            return Response::json([
                "type" => "error",
                "message" => "Подайте шаблон на модерацию",
            ], 200);
        }

        if ($price > 500 or $price < 0) {
            if (Helpers::getSiteAdmin() != $template->id) {
                return Response::json([
                    "type" => "error",
                    "message" => "Укажите цену в диапазоне от 0 до 500",
                ], 200);
            }
        }

        CoreTemplates::where('id', $templateId)->update([
            'status' => $request->status?? $template->status,
            'name' => $request->name ?? $template->name,
            'price' => $price,
            'img' => $request->img?? $template->img,
        ]);

        return Response::json([
            "type" => "success",
            "message" => "Шаблон изменен",
        ], 200);
    }

    public function buyTemplate(Request $request) {


        $templateCount = CoreTemplates::where('id', $request->template_id)->count();
        if (!$templateCount) {
            return Response::json([
                'type' => 'error',
                'message' => 'Шаблон не существует',
            ], 200);
        }

        $template = CoreTemplates::where('id', $request->template_id)->first();
        $templateElements = CoreSiteElements::where('template', $template->id)->get();
        $templateElementsFirst = CoreSiteElements::where('template', $template->id)->first()->id_in_system;
        $templatePages = CorePages::where('template', $template->id)->get();


        $typeOrder = 'import';
        if (isset($request->type_order)) {
            $typeOrder = $request->type_order;
        }

        if ($typeOrder == 'buy') {
            $count = CoreBuyTemplates::where('user_id', Helpers::getSiteAdmin())->where('template_id', $request->template_id)->count();

            if ($count) {
                return Response::json([
                    'type' => 'error',
                    'message' => 'Вы уже добавили шаблон в свою библиотеку',
                ], 200);

            } else {


                if (!in_array(Helpers::siteCurrency('code'), ['RUB', 'USD', 'EUR'])) {
                    return Response::json([
                        'type' => 'error',
                        'message' => 'Покупка невозможна',
                    ], 200);
                }


                $count = CoreTemplatesOrder::where('user_id', Helpers::getSiteAdmin())->where('template_id', $request->template_id)->count();

                if ($template->price > 0 and Helpers::getSiteAdmin() != $template->user_id and !$count) {
                    if (Helpers::checkSandBox()) {
                        return Response::json([
                            "type" => "error",
                            "message" => "Покупка невозможна в режиме песочницы",
                        ], 200);
                    }

                    $resultDecrement = Helpers::currencyConverter('RUB', Helpers::siteCurrency('code'), $template->price);
                    if (Auth::user()->balance < $resultDecrement) {
                        return Response::json([
                            'type' => 'error',
                            'message' => 'На вашем балансе недостаточно средств',
                        ], 200);
                    }
                    $templateReward = Helpers::myRound($template->price * 0.5);
                    Users::where('id', 1)->decrement('balance', $resultDecrement);
                    CoreUsers::where('id', $template->user_id)->increment('balance', $templateReward);
                    $orderTemplate = CoreTemplatesOrder::create([
                        'template_id' => $template->id,
                        'user_id' => Helpers::getSiteAdmin(),
                        'to_user' => $template->user_id,
                        'amount' => $template->price,
                        'date' => strtotime(now()),
                    ]);
                    Cashout::create([
                        'id_user' => 1,
                        'payment_system' => 101010,
                        'id_payment' => $orderTemplate->id,
                        'currency' => Helpers::siteCurrency(),
                        'amount' => $template->price,
                        'status' => 2,
                        'to' => $template->user_id,
                        'date' => now(),
                    ]);
                }

                CoreBuyTemplates::create([
                    'user_id' => Helpers::getSiteAdmin(),
                    'template_id' => $template->id,
                    'date' => strtotime(now())
                ]);

                return Response::json([
                    'type' => 'success',
                    'message' => 'Шаблон добавлен в вашу библиотеку шаблонов',
                ], 200);

            }

        } elseif ($typeOrder == 'import') {

            $countBuy = CoreBuyTemplates::where('user_id', Helpers::getSiteAdmin())->where('template_id', $template->id)->count();

            if (!$countBuy) {
                return Response::json([
                    'type' => 'error',
                    'message' => 'Этот шаблон еще не добавлен в вашу библиотеку шаблонов',
                ], 200);
            }

            $create = Templates::create([
                'status' => 0,
                'name' => $template->name,
                'name_ru' => $template->name,
                'store' => $template->id,
            ]);


//            DB::statement("alter table site_elements AUTO_INCREMENT = 0");

            foreach ($templateElements as $element) {

//                $lastId = SiteElements::orderBy('id', 'desc')->first()->id;
//                $result = $lastId - $element->id_in_system;
//                $diff = $result + 1;
//                $element->id_in_system += $diff;
//                $arrayCheck = ['left', 'right', 'content', 'top', 0];
//                if (!in_array($element->parent_id, $arrayCheck)){
//                    $element->parent_id += $diff;
//                }

//                dd($element->id_in_system, $element->parent_id);

                $createElement = SiteElements::create([
                    'id_in_system' => $element->id_in_system,
                    'type' => $element->type,
                    'name' => $element->name,
                    'name_ru' => $element->name_ru,
                    'parent_id' => $element->parent_id,
                    'option_value' => $element->option_value,
                    'page' => $element->page,
                    'sort' => $element->sort,
                    'template' => $create->id,
                ]);

//                SiteElements::where('id', $createElement->id)->update(['id' => $element->id_in_system, 'parent_id' => $element->parent_id]);

            }

            foreach ($templatePages as $page) {
                Pages::create([
                    'status' => $page->status,
                    'native' => $page->native,
                    'name' => $page->name,
                    'name_ru' => $page->name_ru,
                    'template' => $create->id,
                ]);
            }

            return Response::json([
                'type' => 'success',
                'message' => 'Шаблон был загружен в ваши шаблоны',
            ], 200);

        }

    }

    public function templatesAdd(Request $request) {

        $name = Helpers::translit($request->name_ru);
        $optionValue = ProfileBuilderController::defaultTemplate();


        $templateCreate = Templates::create([
            'status' => 0,
            'name' => $name,
            'name_ru' => $request->name_ru,
            'store' => 0,
        ]);

        $elementsCreate = SiteElements::create([
            'id_in_system' => 0,
            'type' => 'style',
            'name' => 'main-style',
            'name_ru' => 'Оформление сайта',
            'parent_id' => 0,
            'option_value' => $optionValue,
            'sort' => 0,
            'page' => 'all',
            'template' => $templateCreate->id,
        ]);

        SiteElements::where('id', $elementsCreate->id)->update(['id_in_system' => $elementsCreate->id]);

        return Response::json([
            "type" => "success", "message" => "Шаблон создан. Теперь вы можете переключиться на него и начать редактировать"
        ], 200);

    }

    public function templateDuplicate(Request $request) {


    }

    public function authPages() {
        $authPages = AuthPages::first();
        return view('templates/admin/authPages', compact('authPages'));

    }

    public function authPagesStore(Request $request) {

        AuthPages::find(1)->update([
            'login' => $request->login_page,
            'register' => $request->register_page,
        ]);

        return Response::json([
            "type" => "success",
            "message" => "Сохранено"
        ], 200);

    }

    /*
     *
     * Для фермы
     *
     */

    /*
     * Настройка птиц
     */

    public function createBirds() {
        $birds = Birds::all();
        $types = TypesOrder::all();

        return view("templates/admin/birds", array(
            'birds' => $birds,
            'typesOrder' => $types
        ));
    }

    public function storeBirds(Request $request) {

        if ($request->file('bird_image')) {
            $path = $request->file('bird_image')->store('sites/'.$this->domain->getDB().'/birds');
        } else {
            $path = '/storage/app/avatars/0.jpg';
        }

        if ($request->end == 0) {
            $request->only_end = 0;
        }

        switch ($request->type_time) {
            case 1:
                $second = $request->second;
                break;
            case 2:
                $second = $request->second * 60;
                break;
            case 3:
                $second = $request->second * 60 * 60;
                break;
            case 4:
                $second = $request->second * 60 * 60 * 24;
                break;
            default:
                $second = $request->second;
        }


        if ($request->price > $request->max_price) {
            $request->max_price = $request->price;
        }

        Birds::create([
            'name' => $request->name,
            'type' => $request->type,
            'range' => $request->range,
            'price' => $request->price,
            'max_price' => $request->max_price,
            'status' => 1,
            'image' => $path,
            'profit' => $request->profit,
            'second' => $second,
            'end' => $request->end,
            'only_end' => $request->only_end,
            'buy_btn' => $request->buy_btn,
        ]);

        return Response::json([
            "message" => "Настройки сохранены"
        ], 200);
    }

    public function updateBirds(Request $request) {
        $domain = new Domain();

        if (BirdsOrder::where('bird_id', $request->bird_id)->count()) {
            $birds = Birds::find($request->bird_id);

            $messageError = '';

            if ($birds['range'] != $request->range) {
                $messageError .= '<br>- Тип цены';
            }

            if ($birds['type'] != $request->type) {
                $messageError .= '<br>- Что зарабатывает';
            }

            if ($birds['profit'] != $request->profit) {
                $messageError .= '<br>- Доход';
            }

            if ($birds['price'] != $request->price) {
                $messageError .= '<br>- Каждые сколько секунд';
            }

            if ($messageError != '') {
                return Response::json([
                    "type" => "error",
                    "message" => "Есть купленные птицы этой категории. Невозможно редактировать следующие поля:".$messageError
                ], 200);
            }
        }

        if ($request->select_now_activate == 0) {
            $request->now_activate = 0;
        }

        if ($request->file('bird_image')) {
            $path = $request->file('bird_image')->store('sites/'.$domain->getDB().'/birds');
        } else {
            $path = Birds::where('id', $request->bird_id)->first()['image'];
        }

        if ($request->end == 0) {
            $request->only_end = 0;
        }

        if ($request->price > $request->max_price) {
            $request->max_price = $request->price;
        }

        Birds::find($request->bird_id)
            ->update([
                'name' => $request->name,
                'type' => $request->type,
                'range' => $request->range,
                'price' => $request->price,
                'max_price' => $request->max_price,
                'status' => 1,
                'image' => $path,
                'profit' => $request->profit,
                'second' => $request->second,
                'end' => $request->end,
                'only_end' => $request->only_end,
                'buy_btn' => $request->buy_btn,
            ]);


        return Response::json([
            "message" => "Настройки сохранены"
        ], 200);
    }

    public function deleteBirds(Request $request) {

        if (BirdsOrder::where('bird_id', $request->bird_id)->count()) {
            return Response::json([
                "type" => "error",
                "message" => "У пользователей есть эти вклады во владении. Немозможно удалить"
            ], 200);
        }

        Birds::find($request->bird_id)->delete();



        return Response::json([
            "type" => "warning",
            "message" => "Объект удален"
        ], 200);
    }

    /*
     * Бонусы за пополнение
     */

    public function storeBonus(Request $request) {
        BonusPayment::create([
            'more' => $request->more,
            'less' => $request->less,
            'type' => $request->type,
            'amount' => $request->amount
        ]);

        return Response::json([
            "message" => "Бонус добавлен"
        ], 200);
    }

    public function deleteBonus(Request $request) {
        BonusPayment::where('id', $request->bonus_id)->delete();


        return Response::json([
            "message" => "Бонус удален"
        ], 200);
    }

    /*
     * Бонусы за клик
     */

    public function updateBonusSetting(Request $request) {

        if ($request->more > $request->less) {
            $request->less = $request->more;
        }

        if ($request->more < 0 or $request->less < 0 or $request->second < 0 or $request->cashout_for_buy < 0) {
            return Response::json([
                "type" => "error",
                "message" => "Отрицательные значения невозможно установить"
            ], 200);
        }

        if (!BonusSetting::where('id', 1)->count()) {
            BonusSetting::where('id', '!=', 1)->delete();
            DB::statement("alter table bonus_setting auto_increment = 0");
            BonusSetting::create([
                'id' => 1,
                'more' => 0,
                'less' => 0,
                'second' => 0,
                'cashout_for_buy' => 0,
            ]);
        }

        BonusSetting::find(1)->update([
            'status' => $request->status,
            'more' => $request->more,
            'less' => $request->less,
            'second' => $request->second,
            'cashout_for_buy' => $request->cashout_for_buy
        ]);

        return Response::json([
            "message" => "Настройки бонусов за клик сохранены"
        ], 200);
    }

    public function affiliateStore(Request $request) {
        $sumPercent = 0;
        for ($i = 1; $i <= 8; $i++) {
            $sumPercent = $sumPercent + $request['level_'.$i];
        }

        if ($sumPercent > 90) {
            return Response::json([
                "errors" => ["Сумма реферальных не может быть больше 90%"],
            ], 422);
        }

        Affilate::find(1)->update([
            'level_1' => $request->level_1,
            'level_2' => $request->level_2,
            'level_3' => $request->level_3,
            'level_4' => $request->level_4,
            'level_5' => $request->level_5,
            'level_6' => $request->level_6,
            'level_7' => $request->level_7,
            'level_8' => $request->level_8
        ]);

        return Response::json([
            "message" => "Настройки реферальной программы сохранены"
        ], 200);
    }

    public function typesOrderCreate() {
        $types = TypesOrder::all();
        $currency = Currency::first();

        return view("templates.admin.typesOrder", array(
            'typesOrder' => $types,
            'currency' => $currency,
        ));
    }

    public function typesOrderStore(Request $request) {

        if ($request->file('image')) {
            $path = $request->file('image')->store('sites/'.$this->domain->getDB().'/eggs');
        } else {
            $path = '/storage/app/avatars/0.jpg';
        }

        TypesOrder::create([
            'bird' => $request->bird,
            'egg' => $request->egg,
            'image' => $path,
            'price' => $request->price,
            'end' => 0,
            'only_end' => 0,
            'percent_cashout' => $request->percent_cashout,

        ]);

        return Response::json([
            "message" => "Объект успешно добавлен"
        ], 200);
    }

    public function typesOrderEdit(Request $request) {

        $typesOrder = TypesOrder::find($request->type_id);

        foreach ($typesOrder->birds as $bird) {
            if (BirdsOrder::where('bird_id', $bird['id'])->count()) {

                $messageError = '';

                if ($typesOrder['end'] != $request->end) {
                    $messageError .= '<br>- Срок жизни';
                }

                if ($typesOrder['only_end'] != $request->only_end) {
                    $messageError .= '<br>- Сбор только по окончанию срока жизни';
                }

                if ($typesOrder['price'] != $request->price) {
                    $messageError .= '<br>- Цена продукта';
                }

                if ($messageError != '') {
                    return Response::json([
                        "type" => "error",
                        "message" => "Есть купленные птицы этой категории. Невозможно редактировать следующие поля:".$messageError
                    ], 200);
                }
            }
        }

        if ($request->percent_cashout < 0 or $request->percent_cashout > 100) {
            return Response::json([
                "type" => "error",
                "message" => "Процент на вывод может быть от 0 до 100."
            ], 200);
        }

        if ($request->file('image')) {

            $path = $request->file('image')->store('sites/'.$this->domain->getDB().'/birds');

        } else {
            $path = TypesOrder::where('id', $request->type_id)->first()['image'];
        }

        if ($request->end == 0) {
            $request->only_end = 0;
        }


        TypesOrder::find($request->type_id)->update([
            'bird' => $request->bird,
            'egg' => $request->egg,
            'image' => $path,
            'price' => $request->price,
            'end' => 0,
            'only_end' => 0,
            'percent_cashout' => $request->percent_cashout,
        ]);

        return Response::json([
            "message" => "Объект успешно изменен"
        ], 200);
    }

    public function typesOrderDelete(Request $request) {
        $typesOrder = TypesOrder::find($request->type_id);

        if (Birds::whereType($request->type_id)->count()) {
            return Response::json([
                "type" => "error",
                "message" => "В этой категории есть созданные вклады. Невозможно удалить. Перейдите на страницу вкладов, и удалите все влкады из этой категории"
            ], 200);
        }

        TypesOrder::find($request->type_id)->delete();
        return Response::json([
            "type" => "warning",
            "message" => "Категория успешно удалена"
        ], 200);

    }


    public function storeHashPower(Request $request) {

        HashPower::where('id', '>', 0)->update([
            'id' => 1,
            'status' => $request->status,
            'name' => $request->name,
            'price' => $request->price,
            'payment' => $request->payment,
            'buy_order' => $request->buy_order,
            'level_1' => $request->level_1,
            'level_2' => $request->level_2,
            'level_3' => $request->level_3,
            'level_4' => $request->level_4,
            'level_5' => $request->level_5,
            'level_6' => $request->level_6,
            'level_7' => $request->level_7,
            'level_8' => $request->level_8,
            'level_buy_1' => $request->level_buy_1,
            'level_buy_2' => $request->level_buy_2,
            'level_buy_3' => $request->level_buy_3,
            'level_buy_4' => $request->level_buy_4,
            'level_buy_5' => $request->level_buy_5,
            'level_buy_6' => $request->level_buy_6,
            'level_buy_7' => $request->level_buy_7,
            'level_buy_8' => $request->level_buy_8,
        ]);

        return Response::json([
            "message" => "Настройки псевдоочков сохранены"
        ], 200);
    }

    public function birdsList(Request $request) {
        $birds = BirdsOrder::all();
        if (!empty($request->input('type'))) {
            $birds = BirdsOrder::whereHas('bird', function ($q) use ($request) {
                $q->where('type', $request->input('type'));
            })->get();
        }

        if (!empty($request->input('order_id'))) {
            $birds = BirdsOrder::whereHas('bird', function ($q) use ($request) {
                $q->where('id', $request->input('order_id'));
            })->get();
        }

        $typesOrder = TypesOrder::where('status', 1)->get();

        return view("templates.admin.birdsList", array(
            'birds' => $birds,
            'typesOrder' => $typesOrder
        ));
    }

    public function birdDelete(Request $request) {
        if (BirdsOrder::destroy($request->bird_id)) {
            return Response::json([
                "type" => "warning",
                "message" => "Объект удален"
            ], 200);
        }

        return Response::json([
            "type" => "error",
            "message" => "Не получилось удалить объект"
        ], 200);
    }

    public function birdEdit(Request $request) {
        if (BirdsOrder::where('id', $request->bird_id)->update([
            'stock' => $request->stock
        ])) {
            return Response::json([
                "type" => "success",
                "message" => "Объект отредактирован"
            ], 200);
        }

        return Response::json([
            "type" => "error",
            "message" => "Не получилось отредактировать объект"
        ], 200);
    }

    public function cashoutRequestCreate() {
        $cashouts = Cashout::where('status', 0)->get();

        return view("templates.admin.cashoutRequest", compact('cashouts'));
    }

    public function cashoutRequestConfirm(Request $request) {

        $domain = new Domain();
        $db = $domain->getDB();


        $arrayException = ['myferm_iadminbest',
                            'planet2021_iadminbest',
                            'goodvest_iadminbest',
                            'mining2020_iadminbest',
                            'x2game_iadminbest',
                            'crash_iadminbest',
                            'fastmooney_iadminbest',
                            'malina_iadminbest',
                            'makemoney_iadminbest',
                            'helixvest_iadminbest',
                            'dohodmlm_iadminbest',
                            'topsetevik_iadminbest',
                            'avtomobili_iadminbest',
                            'profitcheck_iadminbest',
                            'moneycollider_iadminbest',
                            'sprintuniverse_iadminbest',
                            ];

        if (!in_array($db, $arrayException)) {
            return Response::json([
                "type" => "error",
                "message" => "Ошибка.",
            ], 200);
        }


        DB::beginTransaction();

        $count = Cashout::where('id', $request->id)->count();

        if (!$count) {
            return Response::json([
                "type" => "error",
                "message" => "Ошибка. Такой транзации не существует."
            ], 200);
        }

        $cashout = Cashout::where('id', $request->id)->first();
        if ($cashout->status) {

            DB::rollBack();
            return Response::json([
                "type" => "error",
                 "message" => "Эта транзакция уже была выполнена, либо отменена."
            ], 200);

        } else {

            $siteCurrency = Helpers::siteCurrency();

            if ($siteCurrency > 3) {
                $balanceSite = PaymentsCrypto::sum('amount')/100*90;
            } else {
                $balanceSite = Payments::sum('amount')/100*90;
            }
            $cashoutAll = Cashout::where('status', 2)->where('payment_system', '!=', 1919)->where('payment_system', '!=', 333)->sum('amount') + $cashout->amount;
            Cashout::where('id', $cashout->id)->update(['status' => 2]);

            if ($balanceSite < $cashoutAll) {
                DB::rollBack();
                return Response::json([
                    "type" => "error",
                    "message" => ["Ошибка #308966. На балансе сайта не достаточно средств."],
                ], 200);
            }


            $response = json_decode(Wallet::send($cashout->payment_system, $cashout->to, $cashout->amount));

            if ($response->desc == 'Payment send') {
                DB::commit();

                SiteBalance::find(1)->increment('cashout', $cashout->amount);


                return Response::json([
                    "success" => true,
                    "message" => "Деньги успешно отправлены"
                ], 200);

            } elseif ($response->desc == 'Error') {
                DB::rollBack();
                return Response::json([
                    "errors" => ["Произошла ошибка"]
                ], 422);

            } elseif ($response->desc == 'ErrorType') {
                DB::rollBack();
                return Response::json([
                    "errors" => ["Валюты сайтов не совпадают"]
                ], 422);
            } elseif ($response->desc == 'Not found') {
                DB::rollBack();
                return Response::json([
                    "errors" => ["Такой Payeer кошелек не существует"]
                ], 422);

            } elseif ($response->desc == 'ErrorSelf') {
                DB::rollBack();
                return Response::json([
                    "errors" => ["Нельзя перевести самому себе"]
                ], 422);
            } else {
                DB::commit();
                return Response::json([
                    "errors" => ["Что-то пошло не так."]
                ], 422);
            }

        }

    }

    public function documentation() {
        $lang = Config::get('app.locale');

        if ($lang == 'en') {
            return view('templates.admin.documentation_en');
        } else {
            return view('templates.admin.documentationNew');
        }

    }

    public function documentationNew() {
        $lang = Config::get('app.locale');

        return view('templates.admin.documentationNew');

    }

    public function ideasStore() {

    }

    public function ideasVote() {

    }

    public static function json() {
        $siteInfo = [];
        $siteInfo['currency'] = Helpers::siteCurrency();
        $marks = MarksInside::all();
        $siteInfo['site_levels'] = MarksHelper::globalCount();
        $siteInfo['site_id'] = Helpers::getSiteId();
        $siteInfo['name'] = SiteInfo::where('id', 1)->first()->name;

        foreach ($marks as $key => $val) {
            $siteInfo['marks'][$key]['id'] = $val->id;
            $siteInfo['marks'][$key]['name'] = $val->name;
            $siteInfo['marks'][$key]['level'] = $val->level;
        }

        return $siteInfo;
    }

    public function activates() {
        $activates = OrdersMarkInside::paginate(30);

        return view('templates.admin.activatesAdmin', compact('activates'));
    }

    public function autoActivate(Request $request) {

        if (MarksHelper::marksError()) {
            return Response::json([
                "success" => false,
                "message" => "Ошибка #203655. Есть ошибка в одном из маркетингов. Перейдите в админ панель, чтобы исправить её.",
            ], 200);
        }

        $marks = MarksInside::all();

        $mark = $request->input('mark');
        if (isset($mark)) {
            $markEnter = MarksHelper::markEnter($mark);
            $users = Users::whereDoesntHave('userMarks', function (Builder $query) use ($mark) {
                $query->where('mark', $mark);
            })->where('balance', '>=', $markEnter)->count();

            return Response::json([
                "count" => $users,
                "markEnter" => $markEnter,
                "message" => "Цена тарифа: ".$markEnter." Эта сумма есть у ".$users." пользователей"
            ], 200);
        }

        return view('templates.admin.autoActivate', compact('marks'));

    }

    public function autoActivateStore(Request $request) {

        if (MarksHelper::marksError()) {
            return Response::json([
                "success" => false,
                "message" => "Ошибка #203655. Есть ошибка в одном из маркетингов. Перейдите в админ панель, чтобы исправить её.",
            ], 200);
        }

        $start = SiteInfo::find(1)->start;
        $mark = $request->mark;
        $checkMark = MarksInside::where('id', $request->mark)->count();
        if ($mark == 0 or !$checkMark) {
            return Response::json([
                "type" => "error",
                "message" => "Тариф не существует"
            ], 200);
        }
        $markEnter = MarksHelper::markEnter($mark);
        $activateCount = $request->activate_count ?? 50;


        if ($activateCount < 0 or $activateCount > 50) {
            return Response::json([
                "type" => "error",
                "message" => "Вы не можете активировать больше 50 пользователей одновременно"
            ], 200);
        }


        if ($start) {
            return Response::json([
                "type" => "error",
                "message" => "Вы не можете делать автоактивации после старта проекта"
            ], 200);
        }

        $type = 'real';
        if ($request->time_type == 2) {
            $type = 'start';
        }

//        if ($request->type_user == 1) {
//
//        } else {
//
//        }

        $users = Users::whereDoesntHave('userMarks', function (Builder $query) use ($mark) {
            $query->where('mark', $mark);
        })->where('balance', '>=', $markEnter)->limit($activateCount)->get();

        foreach ($users as $key => $user) {
            $activate = new ActivateController();
            $activate->activateQueue($mark, $user->id, '', true);
        }

        return Response::json([
            "type" => "success",
            "message" => "Успешно активировано $activateCount пользовалей"
        ], 200);
    }

    public function sandBoxCreate(Request $request) {
        $siteInfo = SiteInfo::find(1);
        $marks = MarksInside::all();

        return view("templates/admin/sandBox", compact('siteInfo', 'marks'));
    }

    public function sandBox(Request $request) {
        if (Auth::id() != 1) {
            return Response::json([
                "type" => "error",
                "message" => "Вы не можете перевести сайт в режим песочницы",
            ], 200);
        }

        if ($request->type == 'enable') {
            if (Payments::count()) {
                return Response::json([
                    "type" => "error",
                    "message" => "Вы не можете перевести сайт в режим песочницы",
                ], 200);
            } else {
                SiteInfo::find(1)->update(['sandbox' => 1]);
                Start::find(1)->update([
                    'start_reg' => '0000-00-00 00:00:00',
                    'start_pay' => '0000-00-00 00:00:00',
                ]);

                return Response::json([
                    "type" => "success",
                    "message" => "Сайт переведен в тестовый режим",
                    "action" => "sandbox-enable",
                ], 200);
            }
        }

        if ($request->type == 'disabled') {
            OrdersMarkInside::whereRaw('id != par_or')->delete();
            OrdersMarkInside::query()->update(['balance' => 0, 'cashout' => 0]);
            PaymentsOrder::query()->delete();
            Users::where('id', 1)->update(['balance' => 0]);
            Users::where('id', '!=', 1)->delete();
            UsersData::where('id_user', '!=', 1)->delete();
            Transaction::query()->delete();

            Start::find(1)->update([
                'start_reg' => '0000-00-00 00:00:00',
                'start_pay' => '0000-00-00 00:00:00',
            ]);

            SiteInfo::find(1)->update([
                'start' => 0,
            ]);


            DB::statement('ALTER TABLE users AUTO_INCREMENT = 0');
            DB::statement('ALTER TABLE users_data AUTO_INCREMENT = 0');
            DB::statement('ALTER TABLE payments_order AUTO_INCREMENT = 0');
            DB::statement('ALTER TABLE orders_mark_inside AUTO_INCREMENT = 0');
            DB::statement('ALTER TABLE transaction AUTO_INCREMENT = 0');


            SiteInfo::find(1)->update(['sandbox' => 0]);

            return Response::json([
                "type" => "success",
                "message" => "Тестовый режим был выключен",
                "action" => "sandbox-disable",
            ], 200);
        }

        if (!Helpers::checkSandBox()) {
            return Response::json([
                "type" => "error",
                "message" => "Включите режим песочницы",
            ], 200);
        }

        if ($request->type == 'user_create') {
            $userCount = $request->count;
            if ($userCount > 1000) {
                return Response::json([
                    "type" => "error",
                    "message" => "Вы не можете создать больше 1000 пользователей одним запросом",
                ], 200);
            }

            if (Auth::id() == 1 and isset($userCount)){

                if ($userCount <= 0) {
                    return Response::json([
                        "type" => "error",
                        "message" => "Введите положительное число",
                    ], 200);
                }

                $userRefer = (isset($request->refer))?$request->refer:1;

                if (!Users::where('id', $userRefer)->count()) {
                    return Response::json([
                        "type" => "error",
                        "message" => "Куратора с таким ID не существует",
                    ], 200);
                }

                $hasher = new MD5Hasher();

                for ($i = 1; $i <= $userCount; $i++) {
                    $rand = rand(100000, 999999);
                    $newUserTest = Users::create([
                        'login' => 'user'.$rand,
                        'email' => 'user'.$rand,
                        'password' => $hasher->make('123123',['user'.$rand]),
                        'regdate' => now(),
                        'active' => 123,
                        'parent' => 0,
                        'refer' => $userRefer ?? 1,
                        'ref_num' => 0,
                        'ref_quan' => 0,
                        'last_active' => now(),
                        'comet_last_active' => time(),
                        'privelege' => 1,
                        'status' => 'offline',
                        'payeer_wallet' => 0,
                        'balance' => 0,
                        'balance_cashout' => 0,
                        'balance_hash_power' => 0,
                        'type_follow' => 0,

                    ]);

                    UsersData::create([
                        'id_user' => $newUserTest->id,
                        'lastname'  => '',
                        'name' => '',
                        'fathername' => '',
                        'vk' => '',
                        'twitter' => '',
                        'facebook' => '',
                        'instagram' => '',
                        'skype' => '',
                        'hide_page' => 0,
                        'open_wall' => 0,
                        'sex' => 0,
                        'telephone' => 0,
                        'rang' => 0,
                        'link_visit' => 0,
                        'text_status' => '',
                    ]);
                }

                return Response::json([
                    "type" => "success",
                    "message" => "$userCount пользователей были созданы",
                ], 200);
            }
        }

        if ($request->type == 'balance') {
            if ($request->amount <= 0) {
                return Response::json([
                    "type" => "error",
                    "message" => "Введите положительное число",
                ], 200);
            }

            if ($request->user_id == 0) {
                $users = Users::query();
                $message[0] = "Всем пользователям";
            } else {
                if (!Users::where('id', $request->user_id)->count()) {
                    return Response::json([
                        "type" => "error",
                        "message" => "Пользоватлея с таким ID не существует",
                    ], 200);
                } else {
                    $users = Users::where('id', $request->user_id);
                    $message[0] = "Пользователю $request->user_id (".Helpers::convertUs('login', $request->user_id).")";
                }
            }

            if ($request->what_do == 'increment') {
                $users->increment('balance', $request->amount);
                $message[1] = "было зачислено на баланс";
            } else {
                $users->update(['balance' => $request->amount]);
                $message[1] = "был установлен баланс";
            }


            return Response::json([
                "type" => "success",
                "message" => "$message[0] $message[1] $request->amount",
            ], 200);

        }

    }

    public static function createNotify() {
        $notify = NotifyTheme::query()->first();
        return view('templates.admin.notify', compact('notify'));
    }

    public static function storeNotify(Request $request) {
        $positionArray = [
            'top',
            'topLeft',
            'topCenter',
            'topRight',
            'center',
            'centerLeft',
            'centerRight',
            'bottom',
            'bottomLeft',
            'bottomCenter',
            'bottomRight',
        ];
        $themeArray = [
            'mint',
            'sunset',
            'relax',
            'nest',
            'metroui',
            'semanticui',
            'light',
            'bootstrap-v3',
            'bootstrap-v4',
        ];

        if (!in_array($request->position, $positionArray) or !in_array($request->theme, $themeArray)) {
            dd($request->position, $request->theme, $positionArray, $themeArray);
            return Response::json([
                "type" => "error",
                "message" => "Выбран не существующий параметр",
            ], 200);
        }

        NotifyTheme::query()->update([
            'theme' => $request->theme,
            'position' => $request->position,
        ]);

        return Response::json([
            "type" => "success",
            "message" => "Стиль уведомлений был изменен",
        ], 200);
    }

}
