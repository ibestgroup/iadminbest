<?php

namespace App\Http\Controllers;

use App\Libraries\Domain;
use App\Libraries\PartnerCore;
use App\Models\Ad;
use App\Models\Affilate;
use App\Models\CorePartnerBalance;
use App\Models\CorePartnerPayments;
use App\Models\CoreSiteList;
use App\Models\CoreSitesBalance;
use App\Models\CoreUsers;
use App\Models\HashPower;
use App\Models\Payments;
use App\Models\PaymentSystems;
use App\Models\SiteInfo;
use App\Models\Users;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Nexmo\User\User;
use Illuminate\Support\Facades\Schema;

class AffiliateController extends Controller
{


    public static function createHashPower()
    {
        $hashPower = HashPower::find(1)->first();
        return view('templates/hashPower', array(
            'hashPower' => $hashPower,
        ));
    }

    public static function createAffiliate()
    {
        $domain = new Domain();


        if ($domain->getDB() == 'iadminbest') {

            $partnerBalanceCount = CorePartnerBalance::where('user_id', Auth::id())->count();

            if (!$partnerBalanceCount) {
                CorePartnerBalance::create([
                    'user_id' => Auth::id(),
                    'rub' => 0,
                    'usd' => 0,
                    'eur' => 0,
                    'btc' => 0,
                    'eth' => 0,
                    'xrp' => 0,
                    'ltc' => 0,
                    'doge' => 0,
                    'ltct' => 0,
                ]);
            }


            $partner = new PartnerCore();
            $rangs = $partner->privelegeInfo();

//            $affiliate = Affilate::find(1)->first();
            $referrals = Users::where('refer', Auth::id())->get();
            $referals = Users::where('refer', Auth::id())->paginate(30);




            foreach ($referrals as $key => $referral) {
                if ($key >= 0 and $key < 5000) {
                    $sites = CoreSiteList::where('user_id', $referral->id)->get();
                    foreach ($sites as $site) {

                        Config::set("database.connections.other", [
                            'driver' => 'mysql',
                            "host" => "localhost",
                            "database" => $site->subdomain,
                            "username" => "root",
                            "password" => "DrinkVodkaPlayDotka228"
                        ]);

                        while (true) {
                            try {
                                $payments = Payments::on('other')->where('payment_system', 1)->get();
                                break;
                            } catch (\Exception $e) {
                                DB::connection('other')->statement("ALTER TABLE `payment` ADD `payment_system` INT NOT NULL DEFAULT '1' AFTER `date`;");
                            }
                        }


                        foreach ($payments as $payment) {

                            $partnerCore = new PartnerCore();
                            $user = Users::where('id', Auth::id())->first();
                            $percent = $partnerCore->percent($user->privelege);

                            DB::beginTransaction();

                            $countPayment = CorePartnerPayments::where('transaction_id', $payment->id)->where('site_id', $site->id)->count();

                            if ($countPayment == 0) {

                                $fee = $payment->amount / 100 * 10;
                                $resultAmount = $fee / 100 * $percent;

                                CorePartnerPayments::create([
                                    'transaction_id' => $payment->id,
                                    'site_id' => $site->id,
                                    'site_admin' => $referral->id,
                                    'to_user' => $referral->refer,
                                    'amount' => $payment->amount,
                                    'parent_amount' => $resultAmount,
                                    'privilege' => $user->privelege,
                                    'payment_system' => 1,
                                    'currency' => 1,
                                    'date' => $payment->date,
                                ]);

                                CorePartnerBalance::where('user_id', Auth::id())->increment('rub', $resultAmount);

                            }


                            DB::commit();

                            $partner = new PartnerCore();
                            $partner->updatePrivelege(Auth::id());

                        }


                        DB::disconnect('other');

                    }
                }


            }
//            $userLogin = Users::where('id', Auth::id())->first();
//            if ($userLogin->login == 'liana12345') {
//                dd($userLogin->login);
//            }
                $paymentSystems = PaymentSystems::all();
                return view('templates/core/affiliate', array(
                    'rangs' => $rangs,
                    'referrals' => $referrals,
                    'referals' => $referals,
                    'paymentSystems' => $paymentSystems,
                ));

        } else {

//            $affiliate = Affilate::find(1)->first();
//            return view('templates/affiliate', array(
//                'affiliate' => $affiliate,
//            ));

        }
    }

    public function userInfo($id) {
        $siteList = CoreSiteList::where('user_id', $id)->get();
        $user = CoreUsers::where('id', $id)->first();
        $userParent = CoreUsers::where('id', $user->refer)->first();


        foreach ($siteList as $site) {
            Config::set("database.connections.other", [
                'driver' => 'mysql',
                "host" => "localhost",
                "database" => $site->subdomain,
                "username" => "root",
                "password" => "DrinkVodkaPlayDotka228"
            ]);

            $sites[$site->subdomain]['id'] = $site->id;
            $sites[$site->subdomain]['name'] = SiteInfo::on('other')->first()->name;
            $sites[$site->subdomain]['payment'] = Payments::on('other')->sum('amount');
            $sites[$site->subdomain]['users'] = Users::on('other')->count();
            $sites[$site->subdomain]['profit'] = CorePartnerPayments::where('to_user', $userParent->id)->where('site_id', $site->id)->sum('parent_amount');

            DB::disconnect('other');
        }

        return view('templates/core/affiliateUser', compact('sites', 'user', 'userParent'));

    }

    public function siteInfo($id) {
        $site = CoreSiteList::where('id', $id)->first();

        Config::set("database.connections.other", [
            'driver' => 'mysql',
            "host" => "localhost",
            "database" => $site->subdomain,
            "username" => "root",
            "password" => "DrinkVodkaPlayDotka228"
        ]);

        $siteInfo = SiteInfo::on('other')->first();
        $payments = CorePartnerPayments::where('site_id', $id)->orderBy('date', 'desc')->paginate(30);

        $partnerCore = new PartnerCore();
        $privilegeInfo = $partnerCore->privelegeInfo();

        return view('templates/core/affiliateSite', compact('site', 'payments', 'siteInfo', 'privilegeInfo'));

    }

    public function sitesInfo() {

        $payments = CorePartnerPayments::where('to_user', Auth::id())->orderBy('date', 'desc')->paginate(30);

        $partnerCore = new PartnerCore();
        $privilegeInfo = $partnerCore->privelegeInfo();

        return view('templates/core/affiliateSite', compact('site', 'payments', 'privilegeInfo'));

    }

}
