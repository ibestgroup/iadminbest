<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Libraries\Domain;
use App\Models\AuthPages;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function showLoginForm(Request $request)
    {
        $domain = new Domain();

        $pages = AuthPages::find(1);
        $pages = $pages->login;
        $redirect = $request->input('redirect') ?? '';

        if ($pages != '/login') {
            if ($redirect != 'no') {
                return redirect($pages);
            }
        }


        if ($domain->checkCore()) {
            return view('auth.core.login');
        } else {
            return view('auth.login');
        }
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'login' => 'required|max:255',
            'password' => 'required|max:255'
        ], [
            'login.required' => 'Вы не указали логин',
            'password.required' => 'Вы не указали пароль',
        ]);

        /**
         * TODO:Передаем в attempt массив с логином и паролем, так как я долбаебушка
         * и не знаю как передать в кастомный провайдер опции через attempt
         */

        $ajax = 0;
        if (isset($request->ajax)) {
            $ajax = 1;
        }

        $credentials = array('login' => $request->login, 'password' => [$request->password, $request->login]);

        if (Auth::attempt($credentials, $request->remember))
        {
            //TODO: Временно пишем указатель на админа в сессиию
            if ($request->login == 'admin') {
                $_SESSION['admin'] = true;
                $_SERVER['admin_true'] = true;
            } else {
                $_SESSION['admin'] = false;
            }

            if (!$ajax) {
                return Redirect::to('profile');
            } else {
                return Response::json([
                    "type" => "success",
                    "url" => "/profile",
                ], 200);
            }
        } else {

            if (!$ajax) {
                throw ValidationException::withMessages([
                    $this->username() => ['Неверный логин или пароль.'],
                ])->redirectTo('login');
            } else {

                return Response::json([
                    "type" => "error",
                    "error" => ["Неверный логин или пароль"]
                ], 200);
            }

        }

    }

    public function username()
    {
        return 'login';
    }
}