<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\TelegramBotController;
use App\Libraries\Domain;
use App\Libraries\Helpers;
use App\Models\AuthPages;
use App\Models\Notice;
use App\Models\SiteInfo;
use App\Models\Users;
use App\Http\Controllers\Controller;
use App\Models\UsersData;
use Illuminate\Http\Request;
use App\Libraries\MD5Hasher;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\URL;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function register(Request $request) {

        if (Helpers::checkSandBox()) {
            return Response::json([
                "type" => "error",
                "message" => "Регистрации не доступны в режиме песочницы",
            ], 200);
        }

        $this->validate($request, [
            'login' => 'required|string|max:20|unique:users|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
//            'terms' => 'accepted',
        ], [
            'login.required' => __('main.register_login_required'),
            'login.regex' => __('main.register_login_regex'),
            'login.max' => __('main.register_login_max'),
            'login.unique' => __('main.register_login_unique'),
            'email.required' => __('main.register_email_required'),
            'email.max' => __('main.register_email_max'),
            'email.unique' => __('main.register_email_unique'),
            'password.required' => __('main.register_password_required'),
            'password.confirmed' => __('main.register_password_confirmed'),
            'password.min' => __('main.register_password_min'),
//            'terms.required' => __('main.register_terms_required'),
//            'terms.accepted' => __('main.register_terms_accepted'),
        ]);

        $user = $this->create($request->all());
        $ajax = 0;
        if (isset($request->ajax)) {
            $ajax = 1;
        }

        $authPages = AuthPages::first()->login ?? '/login';

        if (!$ajax) {
            return redirect()->to($authPages);
        } else {
            return Response::json([
                "type" => "success",
                "url" => $authPages,
            ], 200);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //TODO: Уточнить по всем полям, для их корректного заполнения при регистрации

//        dd($data);

        $hasher = new MD5Hasher();
        $emailToken = Str::random(64);
        $siteInfo = SiteInfo::find(1);


        if (empty($data['refer'])) {
            $data['refer'] = 1;

            if (Users::where('id', $data['refer'])->count()) {
                $data['refer'] = (int)$data['refer'];
            }
        }

        if (!empty($data['refer_login'])) {
            if (Users::where('login', $data['refer_login'])->count()) {
                $data['refer'] = Users::where('login', $data['refer_login'])->first()['id'];
            }
        } else {

            $data['refer_login'] = \Illuminate\Support\Facades\Cookie::get('refer_login');
            if (!empty($data['refer_login'])) {
                if (Users::where('login', $data['refer_login'])->count()) {
                    $data['refer'] = Users::where('login', $data['refer_login'])->first()['id'];
                }
            }
        }


        $user = Users::create([
            'login' => $data['login'],
            'email' => $data['email'],
            'password' => $hasher->make($data['password'],[$data['login']]),
            'regdate' => now(),
            'active' => $emailToken,
            'parent' => 0,
            'refer' => $data['refer'],
            'ref_num' => 0,
            'ref_quan' => 0,
            'last_active' => now(),
            'comet_last_active' => time(),
            'privelege' => 1,
            'status' => 'offline',
            'payeer_wallet' => 0,
            'balance' => 0,
            'type_follow' => 0,
            'telegram_bind_code' => Str::random(24),

        ], [$data['login']]);

        UsersData::create([
            'id_user' => $user->id,
            'lastname' => '',
            'name' => '',
            'fathername' => '',
            'vk' => '',
            'twitter' => '',
            'facebook' => '',
            'instagram' => '',
            'skype' => '',
            'hide_page' => 0,
            'open_wall' => 0,
            'sex' => 0,
            'telephone' => '',
            'rang' => 1,
            'link_visit' => 0,
            'text_status' => '',
            'bulb' => 0
        ]);

        Notice::create([
            'user_id' => $user->id,
            'type' => 'account',
            'subtype' => 'register',
            'head' => 'Регистрация',
            'text' => "Поздравляем с регистрацией на нашем сайте! Вы сделали правильный выбор!",
            'view' => 0,
            'date' => now(),
        ]);

        if (Users::where('refer', $data['refer'])->count() == 0) {
            $numRef = "Ваш первый";
        } else {
            $numRef = "Ваш <b>".Users::where('refer', $data['refer'])->count()."-й</b> по счету";
        }

        Users::where('id', $data['refer'])->increment('ref_quan', 1);

        Notice::create([
            'user_id' => $data['refer'],
            'type' => 'referrals',
            'subtype' => 'register_ref',
            'head' => 'У вас новый реферал!',
            'text' => "Поздравляем по вашей ссылке зарегистрировался ".$numRef." реферал!",
            'view' => 0,
            'date' => now(),
        ]);
        TelegramBotController::sendNotice(
            $data['refer'],
            "Поздравляем по вашей ссылке зарегистрировался ".$numRef." реферал!",
            (new Domain())->getDB()
        );



        $user = Users::where('login', $data['login'])->first();
        $url = URL::to('/');

//        Mail::send('mail.register', ['token' => $emailToken, 'user' => $user, 'siteInfo' => $siteInfo, 'url' => $url], function ($m) use ($user, $siteInfo) {
//            $m->from('info@iadmin.work', $siteInfo['name']);
//            $m->to($user->email)->subject('Благодарим за регистрацию на '.$siteInfo['name']);
//        });

        return $user;
    }
}
