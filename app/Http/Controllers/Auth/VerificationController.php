<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\VerifiesEmails;
use App\Models\Users;

class VerificationController extends Controller
{

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function update($token) {
        $user = Users::where('active', $token);

        if ($user->count()) {
            Users::where('active', $token)
                ->update(['email_verified_at' => now()]);
            $success = true;
        } else {
            $success = false;
        }

        return view('auth.passwords.verify', compact('success'));
    }
}