<?php

namespace App\Http\Controllers;

use App\Helpers\AffiliateHelper;
use App\Libraries\CPayeer;
use App\Libraries\Helpers;
use App\Libraries\PartnerCore;
use App\Models\Affilate;
use App\Models\BonusPayment;
use App\Models\CashoutCrypto;
use App\Models\CoreParking;
use App\Models\CoreSiteList;
use App\Models\Currency;
use App\Models\HashPower;
use App\Models\Notice;
use App\Models\PaymentFinestCore;
use App\Models\Payments;
use App\Models\PaymentsCrypto;
use App\Models\SiteBalance;
use App\Models\SiteInfo;
use App\Models\Transaction;
use App\Models\Users;
//use Auth;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Libraries\Domain;
use Illuminate\Http\RedirectResponse;
use App\Libraries\CoinPaymentsAPI;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Config;
use Ixudra\Curl\Facades\Curl;
use Nexmo\User\User;

class BalanceCallbackController extends Controller
{

    public $value = 0;

    public function __construct()
    {

        $this->value = SiteInfo::find(1)['value'];

    }

    public function checkPay(Request $request) {
        // Отклоняем запросы с IP-адресов, которые не принадлежат Payeer
        $m_key = 'DannaDiD19';
        if (!in_array($_SERVER['REMOTE_ADDR'], array('185.71.65.92', '185.71.65.189', '149.202.17.210'))) return;

        if (isset($request->m_operation_id) && isset($request->m_sign)) {

            $m_key = 'DannaDiD19';
            // Формируем массив для генерации подписи
            $arHash = array(
                $request->m_operation_id,
                $request->m_operation_ps,
                $request->m_operation_date,
                $request->m_operation_pay_date,
                $request->m_shop,
                $request->m_orderid,
                $request->m_amount,
                $request->m_curr,
                $request->m_desc,
                $request->m_status
            );

            // Если были переданы дополнительные параметры, то добавляем их в массив
            if (isset($request->m_params)) {
                $arHash[] = $request->m_params;
            }

            // Добавляем в массив секретный ключ
            $arHash[] = $m_key;

            // Формируем подпись
            $sign_hash = strtoupper(hash('sha256', implode(':', $arHash)));

            $myParams = json_decode($request->m_params);

            // Если подписи совпадают и статус платежа “Выполнен”
            if ($request->m_sign == $sign_hash && $request->m_status == 'success')
            {

                Config::set("database.connections.other", [
                    'driver' => 'mysql',
                    "host" => "localhost",
                    "database" => $myParams->reference->site_db,
                    "username" => "root",
                    "password" => "DrinkVodkaPlayDotka228"
                ]);

                Config::set("database.connections.core", [
                    'driver' => 'mysql',
                    "host" => "localhost",
                    "database" => 'iadminbest',
                    "username" => "root",
                    "password" => "DrinkVodkaPlayDotka228"
                ]);

                $currency = Currency::on('other')->first();
                $siteInfo = SiteInfo::on('other')->first();
                $paymentCount = Payments::on('other')->where('payment_id', $request->m_operation_id)->count();

                if ($paymentCount == 0) {

                    $resultAmount = $request->m_amount;

                    $hashPower = HashPower::on('other')->first();
                    if ($hashPower->status == 1) {
                        AffiliateHelper::hashPower($resultAmount, 'payment', $myParams->reference->site_db, $myParams->reference->user_id);
                        AffiliateHelper::hashPower($resultAmount, 'payment_ref', $myParams->reference->site_db, $myParams->reference->user_id);
                    }

                    if ($currency->status == 1) {
                        $resultAmount = $request->m_amount*$currency['price'];
                    }

                    $siteType = CoreSiteList::on('core')->where('subdomain', $myParams->reference->site_db)->first()['type'];

                    if ($siteType > 1) {
                        AffiliateHelper::referral($resultAmount, $myParams->reference->site_db, $myParams->reference->user_id);
                    }

                    if ($siteInfo->payment_to == 1 and $siteType > 1) {
                        Users::on('other')->where('id', $myParams->reference->user_id)->increment('balance_cashout', $resultAmount);
                    } else {
                        Users::on('other')->where('id', $myParams->reference->user_id)->increment('balance', $resultAmount);
                    }

                    $payment = Payments::on('other')->create([
                        'user_id' => $myParams->reference->user_id,
                        'payment_id' => $request->m_operation_id,
                        'amount' => $request->m_amount,
                        'date' => now(),
                        'payment_system' => 1,
                    ]);

                    Transaction::on('other')->create([
                        'transaction_id' => $payment->id,
                        'from_user' => 0,
                        'to_user' => $myParams->reference->user_id,
                        'type' => 'payment',
                        'date' => now(),
                    ]);

                    SiteBalance::on('other')->where('id', 1)->increment('balance', $request->m_amount);

                    exit($request->m_orderid.'|success');
                }

                exit($request->m_orderid.'|success');

            }

            exit($request->m_orderid.'|error');

        }

    }

    public function successPay(Request $request) {

//        $domain = new Domain();
//        $resultDomain = $domain->resultDomain($request->m_params->reference->site_id);
//
//        return redirect('http://'.$resultDomain.'/profile');

    }

    public function checkPayPM(Request $request) {
        $secret = strtoupper( md5('dXM0M9839FoDBFqcHGiKsqncF') );

        if ($request->PAYMENT_UNUTS == 'USD') {
            $paymentSystem = 2;
        } else {
            $paymentSystem = 3;
        }

        $hash = $request->PAYMENT_ID.':'.
            $request->PAYEE_ACCOUNT.':'.
            $request->PAYMENT_AMOUNT.':'.
            $request->PAYMENT_UNITS.':'.
            $request->PAYMENT_BATCH_NUM.':'.
            $request->PAYER_ACCOUNT.':'.
            $secret.':'.
            $request->TIMESTAMPGMT;

//        dd($hash);


        $hash = strtoupper( md5($hash) );


        if ( $hash != $_POST['V2_HASH'] ) exit('error');

        $countPayment = Payments::where('payment_id', $request->PAYMENT_BATCH_NUM)->where('payment_system', $paymentSystem)->count();

        if ($countPayment == 0) {
            $payment = Payments::create([
                'user_id' => $request->PAYMENT_ID,
                'payment_id' => $request->PAYMENT_BATCH_NUM,
                'amount' => $request->PAYMENT_AMOUNT,
                'date' => now(),
                'payment_system' => $paymentSystem,
            ]);

            Transaction::create([
                'transaction_id' => $payment->id,
                'from_user' => 0,
                'to_user' => $request->PAYMENT_ID,
                'type' => 'payment',
                'date' => now(),
            ]);

            Users::where('id', $request->PAYMENT_ID)->increment('balance', $request->PAYMENT_AMOUNT);
        }

        return redirect('/profile');

    }

    public function successPayPM(Request $request) {
        dd('321');
    }

    public function failsPayPM(Request $request) {
        dd('321');
    }

    public function checkPayCrypto(Request $request) {
//        require($_SERVER['DOCUMENT_ROOT'].'/classes/coinpayments/coinpayments.inc.php');
//        $cp_debug_email = 'c';
//
//        Mail::send('mail.error', ['info' => 'Списание'], function ($m) {
//            $m->from('hello@app.com', 'Your Application');
//
//            $m->to('youngoldman@yandex.ru', 'Арнольд')->subject('CoinPayments IPN Success');
//        });

        // TODO: Тут обработка колбэков от платежки

        // Fill these in with the information from your CoinPayments.net account.
        //	http://mlmone.click/classes/coinpayments/ipn_page.php
        //$cp_merchant_id = 'b153f424d3c1af24c89dcf2e371e1b7f';
        $cp_merchant_id = '9c7c0bab836c0cbd41b67f8965e534bb';
        $cp_ipn_secret = 'DelegaTion10';
        $request_cp = file_get_contents('php://input');


//        Mail::send('mail.error', ['info' => 'Списание', 'request_str' => $request->getQueryString(), 'request' => $request, 'hmac' => $request_cp], function ($m) {
//            $m->from('hello@app.com', 'Your Application');
//            $m->to('youngoldman@yandex.ru', 'Арнольд')->subject('CoinPayments IPN Success');
//        });

        function errorAndDie($error_msg) {
            global $cp_debug_email;
//            global $request;
//            if (!empty($cp_debug_email)) {
//                $report = 'Error: '.$error_msg."\n\n";
//                $report .= "POST Data\n\n";
//                foreach ($request->input() as $k => $v) {
//                    $report .= "|$k| = |$v|\n";
//                }
//            }
                mail('youngoldman@yandex.ru', 'CoinPayments IPN Error', 'IPN Error: '.$error_msg);
            die('IPN Error: '.$error_msg);
        }



        if (!isset($request->ipn_mode) || $request->ipn_mode != 'hmac') {
            errorAndDie('IPN Mode is not HMAC');
        }


        if (!isset($_SERVER['HTTP_HMAC']) || empty($_SERVER['HTTP_HMAC'])) {
            errorAndDie('No HMAC signatures sent.');
        }

//        $request_cp = $request->getQueryString();
        $request_cp = file_get_contents('php://input');

        if ($request_cp === FALSE || empty($request_cp)) {
            errorAndDie('Error reading POST data');
        }

        if (!isset($request->merchant) || $request->merchant != trim($cp_merchant_id)) {
            errorAndDie('No or incorrect Merchant ID passed');
        }



        $hmac = hash_hmac("sha512", $request_cp, trim($cp_ipn_secret));
        if ($hmac != $_SERVER['HTTP_HMAC']) {
            //if ($hmac != $_SERVER['HTTP_HMAC']) { //<-- Use this if you are running a version of PHP below 5.6.0 without the hash_equals function
            errorAndDie('HMAC signature does not match');
        }

        // HMAC Signature verified at this point, load some variables.

        $txn_id = $request->txn_id;
        $address = trim($request->address);
        $amount = floatval($request->amount);
        $fee = floatval($request->fee);
        $amount = $amount - $fee;
        $status = intval($request->status);
        $tr_id = intval($request->tr_id);
        $curr = $request->currency;
//        $CONNECT = mysqli_connect('localhost', $request->input('site_dom'), 'GenRiGeil120AntarktiDa', $request->input('site_dom'));

        $getDB = $request->input('site_dom');
        $userId = $request->input('user_id');
        $confirms = $request->confirms;

        Config::set("database.connections.other", [
            'driver' => 'mysql',
            "host" => "localhost",
            "database" => $getDB,
            "username" => "root",
            "password" => "DrinkVodkaPlayDotka228"
        ]);

        Config::set("database.connections.core", [
            'driver' => 'mysql',
            "host" => "localhost",
            "database" => 'iadminbest',
            "username" => "root",
            "password" => "DrinkVodkaPlayDotka228"
        ]);


        if ($status >= 100 || $status == 2 || $curr == "LTCT") {
            //TODO: Обновляем баланс в таблице пользователей, следом обновляем нашу транзакцию
            if ($request->ipn_type == 'deposit') {

//                mysqli_query($CONNECT, "UPDATE `users`  SET `balance` = `balance` + $amount WHERE `id` = '$_REQUEST[user_id]'");
//                mysqli_query($CONNECT, "UPDATE `payment_crypto` SET `status` = 2, `date` = NOW(), `confirm` = '$_REQUEST[confirms]' WHERE `address` = '$address'");

                $payment = PaymentsCrypto::where('address', $address)->first();

                Users::on('other')->where('id', $userId)->increment('balance', $amount);
                PaymentsCrypto::on('other')->where('address', $address)->update([
                    'amount' => $amount,
                    'status' => 2,
                    'date' => now(),
                    'confirm' => $confirms,

                    'tx_id' => $txn_id,
                ]);

                Transaction::on('other')->create([
                    'transaction_id' => $payment->id,
                    'from_user' => 0,
                    'to_user' => $payment->id_user,
                    'type' => 'payments_crypto',
                    'date' => now(),
                ]);

            } else {

//                mysqli_query($CONNECT, "UPDATE `cashout_crypto` SET `status` = 2, `tx_id` = '$txn_id', `date` = NOW() WHERE `id` = '$tr_id'");

                CashoutCrypto::on('other')->where('id', $tr_id)->update([
                    'status' => 2,
                    'tx_id' => $txn_id,
                    'date' => now(),
                ]);

            }


        } else if ($status < 0) {
            if ($request->ipn_type == 'deposit') {
                //TODO: Если транзакция есть,  то  обновляем таблицу payment_crypto
//                mysqli_query($CONNECT, "UPDATE `payment_crypto` SET `amount` = '$amount', `tx_id` = '$txn_id',
// 					`status` = -1, `date` = NOW(), `confirm` = '$_REQUEST[confirms]' WHERE `address` = '$address'");

                PaymentsCrypto::on('other')->where('address', $address)->update([
                    'amount' => $amount,
                    'tx_id' => $txn_id,
                    'status' => -1,
                    'date' => now(),
                    'confirm' => $confirms,
                ]);
            } else {
//                mysqli_query($CONNECT, "UPDATE `cashout_crypto` SET `amount` = '$amount', `tx_id` = '$txn_id',
// 					`status` = -1, `date` = NOW(), `confirm` = '0' WHERE `id` = '$tr_id'");

                CashoutCrypto::on('other')->where('id', $tr_id)->update([
                    'status' => -1,
                    'date' => now(),
                    'tx_id' => $txn_id,
                    'amount' => $amount,
                    'confirm' => 0,
                ]);
            }
        } else {
            if ($getDB && $userId) {
                if ($request->ipn_type == 'deposit') {
                    //TODO: Если транзакция есть,  то  обновляем таблицу payment_crypto
//                    mysqli_query($CONNECT, "UPDATE `payment_crypto` SET `amount` = '$amount', `tx_id` = '$txn_id',
// 					`status` = 1, `date` = NOW(), `confirm` = '$_REQUEST[confirms]' WHERE `address` = '$address'");
                    PaymentsCrypto::on('other')->where('address', $address)->update([
                        'amount' => $amount,
                        'tx_id' => $txn_id,
                        'status' => -1,
                        'date' => now(),
                        'confirm' => $confirms,
                    ]);
                } else {
//                    mail($cp_debug_email, 'CoinPayments IPN Success', 'Списание');
//                    mysqli_query($CONNECT, "UPDATE `cashout_crypto` SET `amount` = '$amount', `tx_id` = '$txn_id',
// 					`status` = 1, `date` = NOW(), `confirm` = '0' WHERE `id` = '$tr_id'");

                    CashoutCrypto::on('other')->where('id', $tr_id)->update([
                        'status' => -1,
                        'date' => now(),
                        'tx_id' => $txn_id,
                        'amount' => $amount,
                        'confirm' => 0,
                    ]);
                }
            }
        }
        die('IPN OK');

        //process IPN here
    }

    public function successPayCrypto(Request $request) {
        dd('321');
    }

    public function failsPayCrypto(Request $request) {
        dd('321');
    }

    public function checkPayBeta(Request $request) {

        $domain = new Domain();
        $resultDomain = $domain->resultDomain();
        $userComment = explode(':', $request->user_comment);

        $params = [
            'amount' => $request->amount,
            'orderId' => $request->orderId,
//            'user_comment' => $request->user_comment,
        ];

        $sign = md5(implode('', $params) . '89ec8d252bc9a43ec1acb477ea740c05');



         $checkPayment = Payments::where('payment_id', $request->id)->count();
         if ($checkPayment) {
             exit('Эта транзакция уже выполнена');
         } else {


             if ($sign == $request->sign) {

                 $payment = Payments::create([
                     'user_id' => (int)$userComment[0],
                     'payment_id' => $request->id,
                     'amount' => $userComment[1],
                     'date' => now(),
                     'payment_system' => 955,
                 ]);

                 Transaction::create([
                     'transaction_id' => $payment->id,
                     'from_user' => 0,
                     'to_user' => (int)$userComment[0],
                     'type' => 'payment',
                     'date' => now(),
                 ]);

                 SiteBalance::where('id', 1)->increment('balance', $userComment[1]);
                 Users:: where('id', $userComment[0])->increment('balance', $userComment[1]);

//                 Mail::send('mail.checkCallBack', ['params' => $request, 'sign' => $sign], function ($m) {
//                     $m->from('hello@app.com', 'Your Application');
//                     $m->to('youngoldman@yandex.ru', 'Арнольд')->subject('CallBack');
//                 });

             } else {
//                 Mail::send('mail.checkCallBack', ['params' => $request, 'sign' => $sign], function ($m) {
//                     $m->from('hello@app.com', 'Your Application');
//                     $m->to('youngoldman@yandex.ru', 'Арнольд')->subject('CallBackFalse');
//                 });
                 exit('Цифровая подпись не совпадает');
             }
         }

         die('OK');
    }

    public function successPayBeta(Request $request) {

    }

    public function failsPayBeta(Request $request) {

    }

    public function checkCashoutBeta(Request $request) {

    }



    public function checkPayFinest(Request $request) {

        $domain = new Domain();
        $resultDomain = $domain->resultDomain();

        $transactionInfo = explode('---', $request->input('payment_id'));
        $userID = $transactionInfo[0];
        $siteDB = $transactionInfo[1];
        $siteID = $transactionInfo[2];
        $paymentID = $transactionInfo[3];
        $amount = $request->input('amount');

        Config::set("database.connections.other", [
            'driver' => 'mysql',
            "host" => "localhost",
            "database" => $siteDB,
            "username" => "root",
            "password" => "DrinkVodkaPlayDotka228"
        ]);

        Config::set("database.connections.core", [
            'driver' => 'mysql',
            "host" => "localhost",
            "database" => "iadminbest",
            "username" => "root",
            "password" => "DrinkVodkaPlayDotka228"
        ]);


        $checkPayment = Payments::on('other')->where('payment_id', $paymentID)->count();
        $checkPaymentCore = PaymentFinestCore::where('payment_id', $request->input('payment_id'))->count();
        if ($checkPayment or $checkPaymentCore) {
            exit('Эта транзакция уже выполнена');
        } else {

            $secret_key = '06F3B0728C76EEE14F64F324E3C50BB';
            $sign = base64_encode(sha1($request->input('amount') ."|09092020134053|". $secret_key));
//            dd($request->input('amount'), $request->input('merchant'), $secret_key, $sign, $request->input('signature'));

            if ($sign == $request->input('signature') and $request->input('status') == 'success') {

                $payment = Payments::on('other')->create([
                    'user_id' => (int)$userID[0],
                    'payment_id' => $paymentID,
                    'amount' => $amount,
                    'date' => now(),
                    'payment_system' => 997,
                ]);

                PaymentFinestCore::create([
                    'payment_id' => $request->input('payment_id'),
                    'site_id' => $siteID,
                ]);

                Transaction::on('other')->create([
                    'transaction_id' => $payment->id,
                    'from_user' => 0,
                    'to_user' => (int)$userID,
                    'type' => 'payment',
                    'date' => now(),
                ]);

                SiteBalance::on('other')->where('id', 1)->increment('balance', $amount);
                Users::on('other')->where('id', $userID)->increment('balance', $amount);

                return redirect('/profile');

                 Mail::send('mail.checkCallBack', ['params' => $request, 'sign' => $sign], function ($m) {
                     $m->from('hello@app.com', 'Your Application');
                     $m->to('youngoldman@yandex.ru', 'Арнольд')->subject('CallBack');
                 });

            } else {
                 Mail::send('mail.checkCallBack', ['params' => $request, 'sign' => $sign], function ($m) {
                     $m->from('hello@app.com', 'Your Application');
                     $m->to('youngoldman@yandex.ru', 'Арнольд')->subject('CallBackFalse');
                 });
                exit('Цифровая подпись не совпадает');
            }
        }

        die('OK');
    }

    public function successPayFinest(Request $request) {

    }

    public function failsPayFinest(Request $request) {

    }

    public function checkPayObm(Request $request) {
        $domain = new Domain();
        $paymentId = $request->input('payment_id');

        $transactionInfo = explode('-', $request->input('payment_id'));
        $userID = $transactionInfo[0];
        $siteID = $transactionInfo[1];
        $paymentID = $transactionInfo[2];
        $amountWithFee = $transactionInfo[3] ?? 0;
        $siteDB = CoreSiteList::find($siteID)->subdomain;

        Config::set("database.connections.other", [
            'driver' => 'mysql',
            "host" => "localhost",
            "database" => $siteDB,
            "username" => "root",
            "password" => "DrinkVodkaPlayDotka228"
        ]);

        $salt = 'DannaDiD19';
        $data = [
            'payment_id' => $paymentId,
        ];

        $data = json_encode($data);

        $dpaySecure = base64_encode(md5($salt . base64_encode(sha1($data, true)) . $salt, true));


        $answer = json_decode(Curl::to('https://acquiring_api.obmenka.ua/api/einvoice/status')
            ->withContentType('application/json')
            ->withHeaders( array(
                'DPAY_CLIENT: 2934',
                'DPAY_SECURE: '.$dpaySecure ) )
            ->withData($data)
            ->post());

        $amountResult = $amountWithFee ? $amountWithFee/110*100 : $answer->accrual_amount;
        $checkPayment = Payments::on('other')->where('payment_id', $paymentID)->count();
        if ($checkPayment) {
            exit('Эта транзакция уже выполнена');
        } else {
            if ($answer->status == 'PAYED' or $answer->status == 'FINISHED' ) {

                SiteBalance::on('other')->where('id', 1)->increment('balance', $amountResult);
                Users::on('other')->where('id', $userID)->increment('balance', $amountResult);

                $payment = Payments::on('other')->create([
                    'user_id' => (int)$userID,
                    'payment_id' => $paymentID,
                    'amount' => $amountResult,
                    'date' => now(),
                    'payment_system' => 996,
                ]);

                Transaction::on('other')->create([
                    'transaction_id' => $payment->id,
                    'from_user' => 0,
                    'to_user' => (int)$userID,
                    'type' => 'payment',
                    'date' => now(),
                ]);

                $siteType = CoreSiteList::on('core')->where('subdomain', $siteDB)->first()['type'];
                $resultAmount = $amountResult;
                if ($siteType > 1) {
                    AffiliateHelper::referral($resultAmount, $siteDB, $userID);
                }
            }
        }
    }

    public function checkPayFK(Request $request) {
        $merchant_secret = config('paysystems.freekassa.password');

        if (!Helpers::checkWhitelist('freekassa')) {
            die("hacking attempt!");
        }

        $sign = md5($request->MERCHANT_ID.':'.$request->AMOUNT.':'.$merchant_secret.':'.$request->MERCHANT_ORDER_ID);
        if ($sign == $request->SIGN) {
            $pr_or = $request->MERCHANT_ORDER_ID;
            preg_match_all('/(.*?)-(.*?)-.+-(.*)/', $pr_or, $pr_or);
            $siteID = $request->us_siteid;
            $siteDB = Helpers::getSiteDB($siteID);
            $userID = $request->us_id;

            Config::set("database.connections.other", [
                'driver' => 'mysql',
                "host" => "localhost",
                "database" => $siteDB,
                "username" => "root",
                "password" => "DrinkVodkaPlayDotka228"
            ]);

            $operation = $request->intid;
            $amount = number_format($request->AMOUNT, 2, '.', '');
            $check_operation = Payments::on('other')->where('payment_id', $operation)->count();


            if ( !$check_operation ) {

                SiteBalance::on('other')->where('id', 1)->increment('balance', $amount);
                Users::on('other')->where('id', $userID)->increment('balance', $amount);

                $payment = Payments::on('other')->create([
                    'user_id' => (int)$userID,
                    'payment_id' => $operation,
                    'amount' => $amount,
                    'date' => now(),
                    'payment_system' => 750,
                ]);

                Transaction::on('other')->create([
                    'transaction_id' => $payment->id,
                    'from_user' => 0,
                    'to_user' => (int)$userID,
                    'type' => 'payment',
                    'date' => now(),
                ]);

                $siteType = CoreSiteList::on('core')->where('subdomain', $siteDB)->first()['type'];
                $resultAmount = $amount;
                if ($siteType > 1) {
                    AffiliateHelper::referral($resultAmount, $siteDB, $userID);
                }

                die('YES');
            } else {
                die('NO');
            }
        } else {
            die('NO');
        }

    }

    public function successPayFK(Request $request) {

        $domain = new Domain();

        $orderId = $request->MERCHANT_ORDER_ID;

        $orderArray = explode('-', $orderId);
        $siteId = $orderArray[0];
        $telegramMode = $orderArray[3] ?? 0;


        if ($telegramMode == 1) {
            $telegramBotUsername = CoreSiteList::find($siteId)->telegram_bot_username;
            return redirect("https://t.me/" . $telegramBotUsername);
        }

        $resultDomain = $domain->resultDomain($siteId);
        $redirect = 'https://'.$resultDomain.'/profile';

        return redirect($redirect);
    }

    public function failsPayFK(Request $request) {


        $domain = new Domain();

        $orderId = $request->MERCHANT_ORDER_ID;
        preg_match_all('/(.*?)-(.*?)-.+/', $orderId, $orderId);
        $siteDB = $orderId[1][0];

        $siteID = Helpers::getSiteId($siteDB);
        $resultDomain = $domain->resultDomain($siteID);

        $redirect = 'https://'.$resultDomain.'/profile';

        return redirect($redirect);
    }

    public function checkPaymentPayeer($userId = 0) {

        $userId = $userId ?: Auth::id();
        $comment = $userId . '-' . Helpers::getSiteId();
        $currency = Helpers::siteCurrency('code');

        $accountNumber = 'P1061336169';
//        $accountNumber = 'P26340592';
//        $accountNumber = 'P1051636730';
//        $apiId = '243544111';
//        $apiId = '1411339228';
        $apiId = '1544396526';
        $apiKey = 'DannaDiD19DannaDiD19';
//        $accountNumber = 'P1008663143';
//        $apiId = '865042113';
//        $apiKey = 'DannaDiD19DannaDiD19';

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://payeer.com/ajax/api/api.php?history");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "account=$accountNumber&apiId=$apiId&apiPass=$apiKey&count=100&action=history");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/x-www-form-urlencoded"
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        $history = json_decode($response)->history;
        $sumAmount = 0;
        $countPayment = 0;
        DB::beginTransaction();
        foreach ($history as $item) {

            if (@$item->comment == $comment and $item->creditedCurrency == $currency) {
                $check = Payments::where('payment_id', $item->id)->where('payment_system', 2000)->count();
                if (!$check) {
                    Payments::create([
                        'user_id' => $userId,
                        'payment_id' => $item->id,
                        'amount' => $item->creditedAmount,
                        'date' => $item->date,
                        'payment_system' => 2000,
                    ]);
                    Users::find($userId)->increment('balance', $item->creditedAmount);
                    $sumAmount += $item->creditedAmount;
                    $countPayment++;
                    $domain = new Domain();
                    $siteDB = $domain->getDB();
                    $siteType = CoreSiteList::where('subdomain', $siteDB)->first()['type'];
                    $resultAmount = $item->creditedAmount;
                    if ($siteType > 1) {
                        AffiliateHelper::referral($resultAmount, $siteDB, $userId);
                    }
                }
            }
        }

        DB::commit();
        if ($countPayment) {
            return Response::json([
                "type" => "success",
                "message" => "Количество транзакций: $countPayment на сумму $sumAmount",
            ], 200);
        } else {
            return Response::json([
                "type" => "error",
                "message" => "Мы не нашли ни одной транзакции",
            ], 200);
        }
    }
}
