<?php

namespace App\Http\Controllers;

use App\Libraries\Helpers;
use App\Libraries\PayTM;
use App\Models\Payments;
use App\Models\PaymentsCrypto;
use App\Models\PaymentSystems;
use App\Models\SiteInfo;
use App\Models\Users;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Libraries\Domain;
use Illuminate\Http\RedirectResponse;
use App\Libraries\CoinPaymentsAPI;
use Illuminate\Support\Facades\Mail;
use Ixudra\Curl\Facades\Curl;


class BalanceController extends Controller
{

    public $value = 0;
    static public $encryptionMethod = 'AES-256-CBC';
    static public $iv = '1sbbbsdfgdfhsdgf'; // just use it as is.. initial vector

    public function __construct()
    {

        $user = Users::find(1);
//        Mail::send('mail.error', ['info' => 'Сообщение'], function ($m) {
//            $m->from('info@mlmone.click', 'MLMone.click');
//
//            $m->to('youngoldman@yandex.ru')->subject('Your Reminder!');
//        });
//        mail('youngoldman@yandex.ru', 'CoinPayments IPN Error', $report);
//        mail("youngoldman@yandex.ru", "Новое письмо", "Пример сообщения","From: info@mlmone.click \r\n");
        $this->value = SiteInfo::find(1)['value'];
    }

    public function balancePage() {
        if (Helpers::typeCurrent($this->value) == 'fiat') {
            $info[0] = '';
        } else {
            $info = PaymentsCrypto::where('id_user', Auth::id())->where('status', 0)->get();
            $value = SiteInfo::find(1)['value'];
            $currentCode = Helpers::getCurrent($value,'code');

            //TODO: Смотрим, есть ли уже созданный кошелек для текущего пользователя, который не пополнялся;
            if ($info->count() == 0) {
                $cps = new CoinPaymentsAPI();
                $res = $cps->GetCallbackAddress($currentCode, COIN_CALLBACK_URL.'&user_id='.Auth::id());

                $address = $res['result']['address'];
                $tag = 0;
                if ($value == 8) {
                    $address = $res['result']['dest_tag'];
                    $tag = $res['result']['address'];
                }

                PaymentsCrypto::create([
                    'address' => $address,
                    'tx_id' => '',
                    'date' => now(),
                    'confirm' => '0',
                    'status' => '0',
                    'amount' => '0',
                    'id_user' => Auth::id(),
                    'tag' => $tag
                ]);

            }

            $info = PaymentsCrypto::where('id_user', Auth::id())->where('status', 0)->get();

        }
        return view('templates/sites/balance', ['info' => $info[0]]);
    }

    public function balance(Request $request)
    {

        // generate dXM0M9839FoDBFqcHGiKsqncF


//        if (isset($request->valval)) {
//            $currency = $request->valval;
//        }
        $system = $request->system;
        $currency = Helpers::siteCurrency('code');
        $siteCurrency = Helpers::siteCurrency();
        $domain = new Domain();
        $resultDomain = $domain->resultDomain();

        $m_orderid = Payments::count() + 1;
        $m_orderid = (string)$m_orderid;

        if (in_array($system, [1,2,114])) {


//            $m_shop = '907884420';
//            $m_shop = '888509418';
            $m_shop = '876599192';
            if ($domain->getDB() == 'globalm_iadminbest') {
                $m_shop = '1088463316';
            }
            $m_amount = number_format($request->input('amount'), 2, '.', '');
            $m_curr = $currency;
            $m_desc = base64_encode('Пополнение ' . $resultDomain);
            $m_key = 'DannaDiD19';

            $arHash = array(
                $m_shop,
                $m_orderid,
                $m_amount,
                $m_curr,
                $m_desc,
            );


            $arParams = array(
                'success_url' => 'http://' . $resultDomain . '/profile',
                'reference' => array(
                    'user_id' => Auth::id(),
                    'site_id' => Helpers::getSiteId(),
                    'site_db' => $domain->getDB(),
                ),
            );

            $key = md5('DannaDiD19' . $m_orderid);
            $m_params = @urlencode(base64_encode(openssl_encrypt(json_encode($arParams), 'AES-256-CBC', $key, OPENSSL_RAW_DATA)));

            $arHash[] = $m_params;
            $arHash[] = $m_key;

            $sign = strtoupper(hash('sha256', implode(":", $arHash)));

            $redirect = 'https://payeer.com/merchant/?m_shop=' . $m_shop . '&m_orderid=' . $m_orderid . '&m_amount=' . $m_amount . '&m_curr=' . $m_curr . '&m_desc=' . urlencode($m_desc) . '&m_sign=' . $sign . '&m_params=' . urlencode($m_params) . '&m_cipher_method=AES-256-CBC&m_process=send';

            return redirect($redirect);

        } elseif (in_array($system, [64,69])) {

            $amount = number_format($request->input('amount'), 2, '.', '');
//            $accountPM = '5053227';
//            $passwordPM = 'DannaDiD19';
            $merchantNamePM = 'iAdmin.best';
            $orderPM = Payments::count() + 1;

            if ($siteCurrency == 2) {
                $walletPM = 'U22457714';
            } else {
                $walletPM = 'E22621241';
            }

//            $postData = [
//                'PAYEE_ACCOUNT' => $walletPM,
//                'PAYEE_NAME' => $merchantNamePM,
//                'PAYMENT_AMOUNT' => $amount,
//                'PAYMENT_AMOUNT' => $currency,
//                'STATUS_URL' => 'https://'.$resultDomain.'/checkpaypm',
//                'ORDER_NUM' => $orderPM,
//                'PAYMENT_METHOD' => 'Perfect Money',
//            ];


            echo '
            <h1>Переадресация</h1>
            <form action="https://perfectmoney.is/api/step1.asp" method="POST" id="myForm" style="display:none;">
                <p>
                    <input type="hidden" name="PAYEE_ACCOUNT" value="'.$walletPM.'">
                    <input type="hidden" name="PAYEE_NAME" value="'.$merchantNamePM.'">
                    <input type="number" step="0.01" name="PAYMENT_AMOUNT" value="'.$amount.'" placeholder="Сумма">
                    <input type="hidden" name="PAYMENT_UNITS" value="'.$currency.'">
                    <input type="hidden" name="PAYMENT_ID" value="'.Auth::id().'">
                    <input type="hidden" name="STATUS_URL"
                           value="https://'.$resultDomain.'/checkpaypm">
                   <input type="hidden" name="PAYMENT_URL"
                           value="https:$//'.$resultDomain.'/checkpaypm">
                    <input type="hidden" name="NOPAYMENT_URL"
                           value="https://'.$resultDomain.'/checkpaypm">
                           
                    <input type="hidden" name="BAGGAGE_FIELDS"
                           value="ORDER_NUM CUST_NUM">
                    <input type="hidden" name="ORDER_NUM" value="'.$orderPM.'">
                    <input type="submit" name="PAYMENT_METHOD" value="PerfectMoney account">
                </p>
            </form>
            <script type="text/javascript">
                document.getElementById("myForm").submit();
            </script>';


//            <input type="hidden" name="PAYMENT_URL"
//                           value="http://dohodmlm.iadminlocal.best/successpaypm">
//                    <input type="hidden" name="NOPAYMENT_URL"
//                           value="http://dohodmlm.iadminlocal.best/failspaypm">

        } elseif (in_array($system, [950])) {


            $key = 'ed4e8f0b73d7db990256bfebe3826c74';
            $i = 'f09a98e1-c749-11ea-a574-a4bf0124fbf3';



            $data = [
                // data for automatic payer registration:
                'email'                      =>  $request->input('email')?:'perevaloff.vic@yandex.ru',
                'country'                    =>  $request->input('country')?:'RU',
                'firstname'                  =>  $request->input('firstname')?:'Данил',
                'lastname'                   =>  $request->input('lastname')?:'Сысоев',
                'phone'                      =>  $request->input('phone')?:'89518700719',
                //advanced params
                'callback_uri'               =>  $request->input('callback_uri')?:'http://mysite.ru/callback_uri',
                'merchant_id'                =>  $request->input('merchant_id')?:'f09a98e1-c749-11ea-a574-a4bf0124fbf3',
                'remote_order_id'            =>  $request->input('remote_order_id')?:'0000002',
                'autopay'                    =>  $request->input('autopay')?:0,
                // account details data:
                //from
                'from_currency'              =>  $request->input('from_currency')?:'bf24f3ce-dea5-45d3-a3cb-9a1a617325a9',
                'from_psp_id'                =>  $request->input('from_psp_id')?:'426e5ca9-0d83-4da9-8ca8-62ae252a5b86',
                'from_account_id'            =>  $request->input('from_account_id')?:'',
                'from_remote_account_id'     =>  $request->input('from_account_id')?:'',
                'from_amount'                =>  $request->input('from_amount')?:'1.0',
                //to
                'to_currency'                =>  $request->input('to_currency')?:'271b23ed-65c4-4472-9e4e-4096077e2ca3',
                'to_psp_id'                  =>  $request->input('to_psp_id')?:'23b9b43c-d3b0-4654-bbfe-bebb82d83da7',
                'to_account_id'              =>  $request->input('to_account_id')?:'d4ff54ad-5c9f-4120-b5da-e44d81385c8c',
                'to_remote_account_id'       =>  $request->input('to_account_id')?:'',
                'to_amount'                  =>  null,

            ];

            $q = self::encode($data, $key);
            $q = urlencode($q);
            $i = urlencode($i);

//            dd($data, $key, $q);

            $redirect = 'https://noxtrade.ee/pay?i=' . $i . '&q=' . $q;

            return redirect($redirect);

        } elseif (in_array($system, [955])) {

//            $currency = 'RUB';
            $amountWithFee = $request->input('amount') * 1.09;
            $amountResultFormat = number_format($amountWithFee, 2, '.', '');
//            dd($amountWithFee);
            $userId = Auth::id();

            $userComment = $userId.':'.$request->input('amount');

            $params = [
                'amount' => $amountResultFormat,
                'currency' => $currency,
                'orderId' => $m_orderid,
                'urlResult' => 'https://'.$resultDomain.'/checkpaybeta',
                'urlSuccess' => 'https://'.$resultDomain.'/profile',
                'urlFail' => 'https://'.$resultDomain.'/profile',
                'paymentSystem' => 'card',
                'user_comment' => $userComment,
                'locale' => 'en',
            ];
            $sign = md5(implode('', $params) . '89ec8d252bc9a43ec1acb477ea740c05');

            $redirectUrl = json_decode(Curl::to('https://merchant.betatransfer.io/api/payment?token=bcbbaeade259b067d9631b0ed714d703')
                ->withData([
                    'amount' => $amountResultFormat,
                    'currency' => $currency,
                    'orderId' => $m_orderid,
                    'urlResult' => $params['urlResult'],
                    'urlSuccess' => $params['urlSuccess'],
                    'urlFail' => $params['urlFail'],
                    'sign' => $sign,
                    'paymentSystem' => $params['paymentSystem'],
                    'user_comment' => $userComment,
                    'locale' => $params['locale']
                ])
            ->post());
//            dd($redirectUrl, $params);

            return redirect($redirectUrl->urlPayment);

        } elseif (in_array($system, [956])) {
            return redirect('http://dohodmlm.iadminlocal.best/paymentcrypto?sum='.$request->input('amount'));
//            return redirect('https://'.$resultDomain.'/paymentcrypto?sum='.$request->input('amount'));
        } elseif (in_array($system, [700,701,702,703])) {

            $paymentSystem = PaymentSystems::where('code', $system)->first();
            if ($paymentSystem->currency != Helpers::siteCurrency('number')) {
                exit('Ошибка');
            }

            $amountResult = $request->input('amount')/100*(100+10);
//            $amountResult = $request->input('amount')/100*(100+$paymentSystem->input_fee);

            switch($system) {
                case 700:
                    $paymentMethod = 'yandex';
                    break;
                case 701:
                    $paymentMethod = 'qiwi';
                    break;
                case 702:
                    $paymentMethod = 'visamaster.rur';
                    break;
                case 703:
                    $paymentMethod = 'advcash.usd';
                    break;
                default:
                    $paymentMethod = 'yandex';
            }

            $clientID = 2934;
            $secret = "DannaDiD19";
            $currency = Helpers::siteCurrency('code');
            $currency = ($currency == 'RUB')?'RUR':$currency;

            $rand = rand(1,9999999);
            $paymentId = Auth::id() . '-' . Helpers::getSiteId() . '-' . $rand . '-' . $amountResult;

            $siteName = SiteInfo::find(1)['name'];
            $dom = new Domain();
            $resURL = $dom->resultDomain();

            $data = [
                "CLIENT_ID" => $clientID,
                "INVOICE_ID" => $paymentId,
                "AMOUNT" => $amountResult,
                "CURRENCY" => $currency,
                "PAYMENT_CURRENCY" => $paymentMethod,
                "DESCRIPTION" => 'Пополнение баланса на '.$siteName.'('.$resURL.') Пользователь #'.Auth::id(),
                "SUCCESS_URL" => "https://$resultDomain/profile",
                "FAIL_URL" => "https://$resultDomain/profile",
                "STATUS_URL" => "https://$resultDomain/checkpayobm?payment_id=".$paymentId
            ];

            $sign = base64_encode(md5($secret . base64_encode(sha1(implode("", $data), true)) . $secret, true));

            $data["SIGN_ORDER"] = implode(";", array_keys($data));
            $data["SIGN"] = $sign;

            echo '
                <form id="myForm" action="https://acquiring.obmenka.ua/acs" method="POST">
                <input type="hidden" name="CLIENT_ID" value="'.$data['CLIENT_ID'].'" />
                <input type="hidden" name="SIGN" value="'.$data['SIGN'].'" />
                <input type="hidden" name="SIGN_ORDER" value="'.$data['SIGN_ORDER'].'" />
                <input type="hidden" name="INVOICE_ID" value="'.$data['INVOICE_ID'].'" />
                <input type="hidden" name="AMOUNT" value="'.$data['AMOUNT'].'" />
                <input type="hidden" name="CURRENCY" value="'.$data['CURRENCY'].'" />
                <input type="hidden" name="PAYMENT_CURRENCY" value="'.$data['PAYMENT_CURRENCY'].'" />
                <input type="hidden" name="DESCRIPTION" value="'.$data['DESCRIPTION'].'" />
                <input type="hidden" name="SUCCESS_URL" value="'.$data['SUCCESS_URL'].'" />
                <input type="hidden" name="FAIL_URL" value="'.$data['FAIL_URL'].'" />
                <input type="hidden" name="STATUS_URL" value="'.$data['STATUS_URL'].'" />
                </form>
                
                <script type="text/javascript">
                    document.getElementById("myForm").submit();
                </script>
            ';



        } elseif (in_array($system, [750])) {

            $paymentSystem = PaymentSystems::where('code', $system)->first();
            if ($paymentSystem->currency != Helpers::siteCurrency('number')) {
                exit('Ошибка');
            }

            $merchant_id = config('paysystems.freekassa.id');
            $secret_word = config('paysystems.freekassa.password');

            $amount = (int)$request->input('amount');
            $ordersCount = Payments::all()->count();

            $domain = new Domain();
            $siteId = Helpers::getSiteId();
            $order_id = $siteId . '-' . Auth::id() . '-' . ($ordersCount + 1);

            $order_amount = number_format($amount, 2, '.', '');


            $sign = md5($merchant_id . ':' . $amount . ':' . $secret_word . ':RUB:' . $order_id);
            $redirect = 'https://pay.freekassa.ru/?' .
                'm='. $merchant_id . '&' .
                'o=' . $order_id . '&' .
                'oa=' . $order_amount . '&' .
                's=' . $sign . '&' .
                'currency=RUB&' .
                'us_order=' . $order_id .'&' .
                'us_siteid=' .$siteId. '&' .
                'us_id=' . Auth::id() . '&' .
                'us_parking=' . $domain->parking();
            return redirect($redirect);

        } elseif (in_array($system, [997])) {

            $domain = new Domain();

            $currency = Helpers::siteCurrency('code');
            $amountWithFee = $request->input('amount');
            $amountResultFormat = number_format($amountWithFee, 2, '.', '');
            $userId = Auth::id();

            $userComment = $userId.':'.$request->input('amount');
            $merchantFH = '09092020134053';
            $secretKey = '06F3B0728C76EEE14F64F324E3C50BB';
            $token = $hash = base64_encode(sha1($amountResultFormat ."|". $merchantFH ."|". $secretKey));



            $paymentId = Auth::id().'---'.$domain->getDB().'---'.Helpers::getSiteId().'---'.rand(1,9999999);
            $details = urlencode($domain->getDB());
            $params = [
                'action' => 'shop',
                'member' => '89518700719',
                'merchant' => $merchantFH,
                'amount' => $amountResultFormat,
                'currency' => $currency,
                'token' => $token,
                'payment_id' => $paymentId,
                'country' => 'ru', // ru/world
                'new' => 'y',
                'details' => $details,
                's_url' => 'https://'.$resultDomain.'/successpayfinest',
                'f_url' => 'https://'.$resultDomain.'/profile',
            ];
//            dd($params);

            echo '
            <h1>Переадресация</h1>
            <form id="myForm" method="post" action="https://finesthour-pay.com/epay/paymentgate.php" style="display: none;">
                <input type="hidden" name="action" value="'.$params['action'].'">
                <input type="hidden" name="member" value="'.$params['member'].'">
                <input type="hidden" name="merchant" value="'.$params['merchant'].'">
                <input type="hidden" name="amount" value="'.$params['amount'].'">
                <input type="hidden" name="currency" value="'.$params['currency'].'">
                <input type="hidden" name="token" value="'.$params['token'].'">
                <input type="hidden" name="payment_id" value="'.$params['payment_id'].'">
                <input type="hidden" name="country" value="'.$params['country'].'">
                <input type="hidden" name="new" value="'.$params['new'].'">
                <input type="hidden" name="details" value="'.$params['details'].'">
                <input type="hidden" name="s_url" value="'.$params['s_url'].'">
                <input type="hidden" name="f_url" value="'.$params['f_url'].'">
                <input type="submit" value="Оплатить">
            </form>
            <script type="text/javascript">
                document.getElementById("myForm").submit();
            </script>';

        } elseif (in_array($system, [2000])) {

            $redirect = 'https://'.$resultDomain.'/topupbalance';

            return redirect($redirect);


        }

    }

    public function paymentPayeer() {
        $demo = $_COOKIE['demo'] ?? 0;

        $comment = Auth::id().'-'.Helpers::getSiteId();
        $payments = Payments::where('user_id', Auth::id())->where('payment_system', 2000)->get();

        return view('templates.sites.topUpBalance', compact('comment', 'payments', 'demo'))->render();
    }

    static public function encode($dataToEncrypt, $secretHash)
    {
        $textToEncrypt = \json_encode($dataToEncrypt, 1);
        $encryptedText = '';
        $encryptedText = \openssl_encrypt(
            self::_addCrc($textToEncrypt),
            self::$encryptionMethod,
            $secretHash,
            0,
            self::$iv
        );
//        dd($encryptedText);
        return $encryptedText;
    }

    static public function decode($encryptedText, $secretHash)
    {
        $decryptedText = '';
        $decryptedText = \openssl_decrypt(
            $encryptedText,
            self::$encryptionMethod,
            $secretHash,
            0,
            self::$iv
        );

        $data = \json_decode($decryptedText, true);
        return $data['data'];
    }

    static protected function _addCrc($text)
    {
        $crc32 = \crc32($text);

        $dataContainer = [
            'data' => $text,
            'crc' => $crc32
        ];

        return \json_encode($dataContainer, 1);
    }

    public function paymentCrypto(Request $request) {
        $domain = new Domain();
        $dirTemplates = $domain->dirTemplates();
        return view('templates.'.$dirTemplates.'.paymentCrypto', compact('request'));
    }


    public static function getDepositLink($amount, $userID = null, $telegramMode = 0)
    {
        $userID = $userID ?: Auth::id();

        $merchant_id = '229678';
        $secret_word = 'vurgzpla';

        $ordersCount = Payments::all()->count();

        $domain = new Domain();
        $siteId = Helpers::getSiteId($domain->getDB());
        $order_id = $siteId . '-' . $userID . '-' . ($ordersCount + 1) . '-' . $telegramMode;
        $order_amount = number_format($amount, 2, '.', '');


        $sign = md5($merchant_id . ':' . $order_amount . ':' . $secret_word . ':' . $order_id);
        $link = 'http://www.free-kassa.ru/merchant/cash.php?' .
            'm=' . $merchant_id . '&' .
            'o=' . $order_id . '&' .
            'oa=' . $order_amount . '&' .
            's=' . $sign . '&' .
            'us_order=' . $order_id . '&' .
            'us_site='.$siteId . '&' .
            'us_id=' . $userID . '&' .
            'us_parking=' . $domain->parking();

        $button[] = [
            [
                'text' => 'Пополнить баланс',
                'url' => $link
            ]
        ];
        return $button;
    }
}
