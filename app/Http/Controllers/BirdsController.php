<?php

namespace App\Http\Controllers;

use App\Helpers\AffiliateHelper;
use App\Libraries\Helpers;
use App\Models\Ad;
use App\Models\Birds;
use App\Models\BirdsOrder;
use App\Models\BonusActivate;
use App\Models\BonusSetting;
use App\Models\Currency;
use App\Models\SiteInfo;
use App\Models\Stock;
use App\Models\TypesOrder;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Models\Users;

class BirdsController extends Controller
{


    public static function create(Request $request)
    {
        $birds = Birds::all();
        $typesOrder = TypesOrder::where('status', 1)->get();

        if (!empty($request->input('type'))) {
            $birds = Birds::where('type', $request->input('type'))->get();
        }

        return view('templates.sites.birds.buyBirds', array(
            'birds' => $birds,
            'typesOrder' => $typesOrder,
        ));
    }

    public static function store(Request $request)
    {
        $siteInfo = SiteInfo::find(1);

        $birdsUser = BirdsOrder::where('user_id', Auth::id())->where('bird_id', $request->bird_id)->count();
        $birdInfo = Birds::find($request->bird_id);
        $birdsActiveCount = BirdsOrder::where('user_id', Auth::id())->where('bird_id', $request->bird_id)->where('status', '<', 2)->count();

        if ($birdInfo->now_activate > 0) {
            if ($birdsActiveCount >= $birdInfo->now_activate) {
                return Response::json([
                    "type" => "error",
                    "message" => "У Вас максимальное количество активных вкладов этого типа."
                ], 200);
            }
        }

        DB::beginTransaction();

        $debit = $birdInfo['price'];
        $number = 1;


        if ($birdInfo['range'] == 1) {

            $debit = $request->sum;
            $number = $request->sum;

            if ($debit < $birdInfo['price'] or $debit > $birdInfo['max_price']) {
                return Response::json([
                    "type" => "error",
                    "message" => "Вклад можно сделать только в диапазоне от $birdInfo[price] до $birdInfo[max_price]"
                ], 200);
            }

        }


        if ($siteInfo->payment_pay == 1) {
            $balanceResult = Auth::user()->balance_cashout;
        } else {
            $balanceResult = Auth::user()->balance;
        }



        if($balanceResult < $debit) {
            DB::rollBack();
            return Response::json([
                "type" => "error",
                "message" => "Недостаточно средтсв"
            ], 200);
        }

        AffiliateHelper::hashPower($debit, 'buy_ref');
        AffiliateHelper::hashPower($debit, 'buy_order');

        if ($birdsUser == 0 or $birdInfo->end == 1 or $birdInfo->range == 1) {
            BirdsOrder::create([
                'user_id' => Auth::id(),
                'bird_id' => $request->bird_id,
                'balance' => 0,
                'created_at' => time(),
                'updated_at' => time(),
                'cashout' => 0,
                'number' => $number,
                'stock' => 0,
                'status' => 0,
            ]);


            if ($siteInfo->payment_pay == 1) {
                Users::find(Auth::id())->decrement('balance_cashout', $debit);
            } else {
                Users::find(Auth::id())->decrement('balance', $debit);
            }

        } else {

            BirdsOrder::where('user_id', Auth::id())
                ->where('bird_id', $request->bird_id)
                ->increment('number');

            $bird = BirdsOrder::where('user_id', Auth::id())->where('bird_id', $request->bird_id)->first();
            $bird['egg'] = floor((time() - $bird['updated_at']) * ($bird['number'] * $birdInfo['profit']/$birdInfo['second'])) + $bird['balance'];
            BirdsOrder::where('id', $bird['id'])->update([
                'balance' => $bird['egg']
            ]);
            if ($bird['egg'] != $bird['balance']) {
                BirdsOrder::where('id', $bird['id'])->update([
                    'updated_at' => time()
                ]);
            }

            if ($siteInfo->payment_pay == 1) {
                Users::find(Auth::id())->decrement('balance_cashout', $debit);
            } else {
                Users::find(Auth::id())->decrement('balance', $debit);
            }

        }
        Users::where('id', Auth::id())->update(['type_follow' => 1]);
        DB::commit();
        return Response::json([
            "message" => "Поздравляем! Ваша покупка успешно прошла"
        ], 200);

    }

    public function myBirds(Request $request) {

        $birds = BirdsOrder::where('user_id', Auth::id())->where('status', '<', 2)
            ->whereHas('bird.typesOrder', function ($q) use ($request) {
                $q->where('status', 1);
            })->get();
        if (!empty($request->input('type'))) {
            $birds = BirdsOrder::where('user_id', Auth::id())->where('status', '<', 2)
                ->whereHas('bird', function ($q) use ($request) {
                    $q->where('type', $request->input('type'));
                })->whereHas('bird.typesOrder', function ($q) use ($request) {
                    $q->where('status', 1);
                })->get();
        }

        $typesOrder = TypesOrder::where('status', 1)->get();

        foreach ($birds as $key => $bird) {

            $resultEnd = $bird['created_at'] + $bird->bird->second;
            $resultTime = time();
            $status = 0;
            if ($resultEnd <= $resultTime and $bird->bird->end == 1) {

                $resultTime = $resultEnd;
                $status = 1;

            }


            if ($bird->bird->range == 1) {
                $birds[$key]['egg'] =
                    ($resultTime - $bird['updated_at']) *
                    ($bird['number']/100) * ($bird->bird->profit/$bird->bird->second) +
                    $bird->balance;

            } else {

                $birds[$key]['egg'] =
                    ($resultTime - $bird['updated_at']) *
                    ($bird['number'] * $bird->bird->profit/$bird->bird->second) +
                    $bird->balance;

            }

            $birds[$key]['remain_time'] = $resultEnd - $resultTime;
            $birds[$key]['egg_round'] = round($birds[$key]['egg'], 5);

            BirdsOrder::where('id', $bird['id'])->update([
                'balance' => $birds[$key]['egg']
            ]);

            if ($birds[$key]['egg'] != $bird['balance']) {
                if ($bird->bird->end == 1) {
                    if ($bird['updated_at'] < $resultEnd) {
                        BirdsOrder::where('id', $bird['id'])->update([
                            'updated_at' => $resultTime,
                            'status' => $status,
                        ]);
                    }
                } else {
                    BirdsOrder::where('id', $bird['id'])->update([
                        'updated_at' => $resultTime,
                        'status' => $status,
                    ]);
                }
            }
        }

        return view('templates.sites.birds.myBirds', array(
            'birds' => $birds,
            'typesOrder' =>$typesOrder,
        ));


    }

    public function collectEgg(Request $request) {
        $bird = BirdsOrder::find($request->bird_id);
        $birdInfo = Birds::find($bird['bird_id']);
        $currency = Currency::first();
        $typeOrder = $bird->bird->typesOrder->id;

        DB::beginTransaction();
        if ($bird['user_id'] != Auth::id()) {
            DB::rollBack();
            return Response::json([
                "message" => "Ошибка"
            ], 200);
        }

        if ($bird['status'] == 0 and $bird->bird->only_end == 1) {
            DB::rollBack();
            return Response::json([
                "type" => "error",
                "message" => "Вы не можете собрать, пока не закончится срок жизни"
            ], 200);
        }

        $resultEnd = $bird['created_at'] + $bird->bird->second;
        $resultTime = time();

        if ($resultEnd <= $resultTime and $bird->bird->end == 1) {
            $resultTime = $resultEnd;
        }



        $numberEgg = ($resultTime - $bird['updated_at']) * ($bird['number'] * $birdInfo['profit']/$birdInfo['second']) + $bird['balance'];

        if ($bird->bird->range == 1) {
            $numberEgg = ($resultTime - $bird['updated_at']) * ($bird['number']/100) * ($bird->bird->profit/$bird->bird->second) + $bird->balance;
        } else {
            $numberEgg = ($resultTime - $bird['updated_at']) * ($bird['number'] * $birdInfo['profit']/$birdInfo['second']) + $bird['balance'];
        }

        if ($numberEgg < 1) {
            return Response::json([
                "type" => "error",
                "message" => 'Вы не можете вывести меньше 1'
            ], 200);
        }

        $numberEgg = round($numberEgg, 5);
        BirdsOrder::where('id', $request->bird_id)->update([
            'updated_at' => $resultTime,
            'balance' => 0,
        ]);

        if ($bird['status'] == 1) {
            BirdsOrder::where('id', $request->bird_id)->increment('status');
        }

        $countStock = Stock::where('user_id', Auth::id())->where('type_order', $typeOrder)->count();

        if (!$countStock) {
            Stock::create([
                'user_id' => Auth::id(),
                'type_order' => $bird->bird->typesOrder->id,
                'amount' => 0,
                'last_cashout' => 0,
            ]);
        }

        $toBalance = 1;
        if ($toBalance == 1) {

            if (Helpers::checkCurrency()) {
                $numberEgg = $numberEgg/$currency['price'];
            }

            $amount = round($numberEgg/$bird->bird->typesOrder->price, 2);
            $resultCashout = round($amount*($bird->bird->typesOrder->percent_cashout/100), 2);
            $resultForBuy = round($amount - $resultCashout, 2);


            Users::where('id', Auth::id())->increment('balance_cashout', $resultCashout);
            Users::where('id', Auth::id())->increment('balance', $resultForBuy);



            DB::commit();

            $messageCashout = '';
            $messageForBuy = '';

            if ($resultCashout > 0) {
                $messageCashout = "На баланс для вывода: ".$resultCashout."<br>";
            }

            if ($resultForBuy > 0) {
                $messageForBuy = "На баланс для покупок: ".$resultForBuy."<br>";
            }

            return Response::json([
                "type" => "success",
                "message" => "Выведено: <br> ".$messageCashout.$messageForBuy,
            ], 200);

        } else {

            Stock::where('user_id', Auth::id())->where('type_order', $typeOrder)->increment('amount', $numberEgg);

        }

        DB::commit();

        return Response::json([
            "type" => "success",
            "message" => 'Вы собрали ресурс "'.$bird->bird->typesOrder->egg.'"'
        ], 200);

    }

    public function createStock() {
        $typesOrder = TypesOrder::all();
        $siteInfo = SiteInfo::find(1);


        foreach ($typesOrder as $key => $order) {

            $typesOrder[$key]['egg_number'] = 0;
            $typesOrder[$key]['egg_number_cashout'] = 0;

            $stock = Stock::where('user_id', Auth::id())->where('type_order', $order['id']);
            if ($stock->count()) {
                $typesOrder[$key]['egg_number'] = $stock->first()['amount'];
                $typesOrder[$key]['egg_number_cashout'] = $typesOrder[$key]['egg_number_cashout'] + $typesOrder[$key]['egg_number'] * ($typesOrder[$key]['percent_cashout']/100);
            }
        }

        return view('templates.sites.birds.stock', array(
            'typesOrder' => $typesOrder,
            'siteInfo' => $siteInfo,
        ));

    }

    public function storeStock(Request $request) {

        $birds = Birds::where('type', $request->type_order)->get();
        $typeOrder = TypesOrder::where('id', $request->type_order)->first();

        DB::beginTransaction();
        $stock = Stock::where('user_id', Auth::id())->where('type_order', $request->type_order);
        $amount = $stock->first()['amount'];

        if ($amount == 0) {
            DB::rollBack();
            return Response::json([
                "type" => "error",
                "message" => "Нет ресурсов"
            ], 200);
        }


        $amount = round($amount/$typeOrder->price, 2);
        $resultCashout = round($amount*($typeOrder->percent_cashout/100), 2);
        $resultForBuy = round($amount - $resultCashout, 2);

        $stock->update(['amount' => 0]);
        Users::where('id', Auth::id())->increment('balance_cashout', $resultCashout);
        Users::where('id', Auth::id())->increment('balance', $resultForBuy);

        DB::commit();
        return Response::json([
            "type" => "success",
            "message" => "Выведено: <br> На баланс для вывода: ".$resultCashout."<br> На баланс для покупок ".$resultForBuy."<br>"
        ], 200);
    }

    public function resourcesInfo() {
        $typesOrder = TypesOrder::all();
        $siteInfo = SiteInfo::find(1);


        return view('templates.sites.birds.priceResources', array(
            'typesOrder' => $typesOrder,
            'siteInfo' => $siteInfo,
        ));
    }
}
