<?php

namespace App\Http\Controllers;

use App\Libraries\Helpers;
use App\Models\Ad;
use App\Models\BonusActivate;
use App\Models\BonusSetting;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Models\Users;

class BonusClickController extends Controller
{


    public static function create($page = 'profile')
    {
        $ad = Ad::find(1);
        return view('templates.sites.bonusClick', array(
            'ad' => $ad,
            'page' => $page
        ));
    }

    public static function store()
    {

        $siteType = Helpers::siteType();

        if ($siteType == 1) {
            return Response::json([
                "errors" => ["Ошибка."],
            ], 422);
        }

        $bonusObj = BonusSetting::first();
        $bonusCount = BonusActivate::where('user_id', Auth::id())->count();


        if ($bonusCount) {
            $bonusUser = BonusActivate::where('user_id', Auth::id())->orderBy('id', 'desc')->first();
            $now = time();
            $diff = $now - $bonusUser['created_at'];
            if ($diff < $bonusObj['second']) {
                $resultSecond = $bonusObj['second'] - $diff;

                return Response::json([
                    "errors" => ["Вы не можете еще получить бонус. Осталось ".$resultSecond."сек."],
                ], 422);
            }
        }

        $bonusResult = rand($bonusObj['more'], $bonusObj['less']);

        Users::find(Auth::id())->increment('balance', $bonusResult);
        BonusActivate::create([
            'user_id' => Auth::id(),
            'amount' => $bonusResult,
            'created_at' => time()
        ]);


        return Response::json([
            "message" => "Поздравляем! Вы получили бонус: ".$bonusResult
        ], 200);

    }

}
