<?php

namespace App\Http\Controllers;

use App\Libraries\Domain;
use App\Libraries\Helpers;
use App\Models\CoreBotWidgets;
use App\Models\CoreSiteList;
use App\Models\TelegramBotSettings;
use App\Models\TelegramMenu;
use App\Models\Users;
use Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;

class BotSettingsController extends Controller
{
    public $domain;

    const BOT_SETTINGS_FIELDS = [
        'header',
        'footer',
        'separator',
    ];

    const ACTIONS = [
        'deposit',
        'withdraw',
        'activate',
    ];

    public function __construct()
    {
        $this->domain = new Domain();
    }

    public function index()
    {

        $isConnected = TelegramBotController::isConnected();
        $widgetList = CoreBotWidgets::all();

        return view('templates.admin.botSettings', compact('isConnected', 'widgetList'));
    }

    public function getJSON($id = null)
    {

        if ($id) {
            if (in_array($id, self::BOT_SETTINGS_FIELDS)) {
                return $this->getTemplate($id);
            }
            $item = TelegramMenu::find($id);
            $item->form_widget = $item->widget ? $this->getFormEditWidget($item->id) : '';
            return json_encode($item);
        }

        $mainSection = TelegramMenu::where('id', 1)->where('parent_id', 1)->first();

        $sections['name'] = $mainSection->name;
        $sections['id'] = $mainSection->id;
        $sections['children'] = self::build($mainSection->id);
        $sections['type'] = $mainSection->type;
        $sections['line_break'] = $mainSection->line_break;
        $sections['sort'] = $mainSection->sort;

        return json_encode([$sections]);
    }

    public function getTemplate($part)
    {
        $array['type'] = 'part';
        $array['part'] = $part;
        $array['id'] = $part;
        $array['text'] = TelegramBotSettings::whereId(1)->first()[$part];
        return json_encode($array);
    }

    public function saveMove(Request $request)
    {

        $action = $request->input('tree');

        $movedNode = $action['moved_node'];
        $parentNode = $action['parent_node'];
        $sortNode = $action['sort_node'];

        foreach ($sortNode as $key => $value) {
            TelegramMenu::where('id', $value)->update([
                'sort' => $key
            ]);
        }

        TelegramMenu::where('id', $movedNode)->update([
            'parent_id' => $parentNode
        ]);
        TelegramMenu::where('sort', 0)->update([
            'line_break' => 0
        ]);

        $data = TelegramMenu::where('id', '>', 0)->get();
        return Response::json([
            "message" => "message",
            "node" => $movedNode,
            "data" => $data,
        ], 200);

    }

    public static function build($parentId)
    {
        $sections = TelegramMenu::where('parent_id', $parentId)->where('id', '!=', 1)->orderBy('sort')->get()->toArray();
        foreach ($sections as $key => $item) {
            $resultSections[$key]['type'] = $item['type'];
            $resultSections[$key]['line_break'] = $item['line_break'];
            $resultSections[$key]['name'] = $item['name'];
            $resultSections[$key]['id'] = $item['id'];
            $resultSections[$key]['sort'] = $item['sort'];
            if (TelegramMenu::where('parent_id', $item['id'])->count()) {
                $resultSections[$key]['children'] = self::build($item['id']);
            }
        }

        return $resultSections;
    }

    public static function create(Request $request)
    {
        $item = TelegramMenu::create([
            'name' => 'Новый пункт',
            'type' => 'menu',
            'text' => 'Содержимое нового пункта',
            'callback' => '',
            'parent_id' => $request->id,
            'sort' => 999
        ]);

        return Response::json([
            'type' => 'success',
            'id' => $item->id,
            'name' => 'Новый пункт',
        ], 200);
    }

    public static function update(Request $request)
    {
        $item = TelegramMenu::where('id', $request->id);
        if (in_array($request->id, self::BOT_SETTINGS_FIELDS)) {
            TelegramBotSettings::whereId(1)->update([
                $request->id => preg_replace(['/\<br \/\>/', "/\n/"], ['\n', ''], $request->text),
            ]);

            return Response::json([
                'type' => 'success',
                'message' => 'Элемент сохранен',
            ], 200);
        }

        if ($item->count()) {
            $item->update([
                'name' => $request->name,
                'text' => preg_replace(['/\<br \/\>/', "/\n/"], ['\n', ''], $request->text),
                'widget' => $request->widget,
                'widget_settings' => json_encode($request->widget_settings),
            ]);


            return Response::json([
                'type' => 'success',
                'message' => 'Элемент сохранен',
            ], 200);

        } else {

            return Response::json([
                'type' => 'error',
                'message' => 'Элемент не существует',
            ], 200);
        }

    }

    public static function remove(Request $request)
    {
        TelegramMenu::where('id', $request->id)->delete();

        return Response::json([
            'type' => 'success',
            'id' => $request->id,
        ], 200);
    }

    public function addApiKey(Request $request)
    {
        if (!$this->checkConnetion($request->key)) {
            return Response::json([
                'type' => 'error',
                'message' => 'Не удалось подключиться к боту. Возможно вы не правильно указали API ключ.'
            ], 200);
        }

        $bot = new \TelegramBot\Api\BotApi($request->key);

        CoreSiteList::find(Helpers::getSiteId())->update([
            'telegram_api' => $request->key,
            'telegram_bot_id' => $bot->getMe()->getId(),
            'telegram_bot_username' => $bot->getMe()->getUsername(),
        ]);

        Users::find(1)->update([
            'telegram_bind_code' => Str::random(24),
        ]);

        return Response::json([
            'type' => 'success',
            'message' => 'Бот успешно подключен'
        ], 200);

    }

    public function checkConnetion($apiKey)
    {
        $bot = new \TelegramBot\Api\BotApi($apiKey);
        try {
            $url = $this->domain->nativeDomain() . '/telegbot';

            // ngrok test url here`
            // $url = 'https://a92a-109-106-141-115.ngrok.io/telegbot';

            $bot->setWebhook($url);
            return true;
        } catch (\Exception $e) {
            Log::info($e);
            return false;
        }
    }

    public function getFormWidget(Request $request)
    {
        $widget = \View::exists('templates.sites.botwidgets.edit.' . $request->widget) ? view('templates.sites.botwidgets.edit.' . $request->widget)->render() : '';

        return Response::json([
            'type' => 'success',
            'message' => 'Подгружена форма редактирования',
            'widget' => $widget,
        ], 200);
    }

    public function getFormEditWidget($id)
    {
        $menu = TelegramMenu::find($id);
        $widget = $menu->widget;
        if (in_array($widget, self::BOT_SETTINGS_FIELDS) or in_array($widget, self::ACTIONS)) {
            return false;
        }
        return view('templates.sites.botwidgets.edit.' . $widget)->render();
    }

    public function manual()
    {
        return view('templates.admin.botSettingsManual')->render();
    }

    public function lineBreak(Request $request)
    {
        TelegramMenu::whereId($request->id)->update([
            'line_break' => $request->lineBreak == 'true' ? 1 : 0,
        ]);
    }

}
