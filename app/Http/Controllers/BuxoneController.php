<?php

namespace App\Http\Controllers;

use App\Models\Buxone\UsersBuxone;
use App\Models\Buxone\UsersBuxoneSync;
use App\Libraries\Helpers;
use App\Libraries\MD5Hasher;
use App\Models\Cashout;
use App\Models\SyncBuxone;
use App\Models\Users;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Nexmo\User\User;

class BuxoneController extends Controller
{

    public static function create()
    {

        Config::set("database.connections.buxone", [
            "host" => "127.0.0.1",
            "database" => 'bux_one',
            "username" => 'root',
            "password" => "DrinkVodkaPlayDotka228",
            'driver' => 'mysql'
        ]);

        $dbConnect = DB::connection('buxone');
        $resultLogin = Auth::user()->login;
        $userCount = count($dbConnect->select("select * from user where `username` = '".$resultLogin."'"));

        $userIdBuxone = SyncBuxone::where('user_id', Auth::id())->first();
        $userBuxone = UsersBuxone::where('id', $userIdBuxone['user_buxone'])->first();

        while ($userCount > 0) {
            $resultLogin = $resultLogin.rand(1000, 9999);
            $userCount = count($dbConnect->select("select * from user where `username` = '".$resultLogin."'"));
        }

        $resultLogin = preg_replace('/admin/', 'login', $resultLogin);

        return view('templates.sites.buxone', array(
            'resultLogin' => $resultLogin,
            'userBuxone' => $userBuxone,
            'page' => 'sd',
        ));
    }

    public static function register(Request $request)
    {

        Config::set("database.connections.buxone", [
            "host" => "127.0.0.1",
            "database" => 'bux_one',
            "username" => 'root',
            "password" => "DrinkVodkaPlayDotka228",
            'driver' => 'mysql'
        ]);

        $refererIdBuxone = 0;
        if (SyncBuxone::where('user_id', 1)->count()) {
            $refererIdBuxone = SyncBuxone::where('user_id', 1)->first()['user_buxone'];
        }

        $hasher = new MD5Hasher();
        $password = $hasher->makeBuxone($request->pass);
        $email = Auth::user()->email;
        $pincode = (string)rand(10000,99999);
        $dbConnect = DB::connection('buxone');


        $userCount = count($dbConnect->select("select * from user where `username` = '$request->login'"));
        $userEmailCount = count($dbConnect->select("select * from user where `email` = '$email'"));

        if ($request->pass != $request->re_pass) {
            return Response::json([
                "type" => "error",
                "message" => "Пароли не совпадают"
            ], 200);
        }

        if ($userCount) {
            return Response::json([
                "type" => "error",
                "message" => "Пользователь с таким логином уже есть на iBux.best"
            ], 200);
        }

        if ($userEmailCount) {
            return Response::json([
                "type" => "error",
                "message" => "Пользователь с Вашим email($email) уже есть на iBux.best"
            ], 200);
        }

        $userCreate = new UsersBuxone();


        $userCreate->username = $request->login;
        $userCreate->password = $password;
        $userCreate->email = $email;
        $userCreate->wmid = '';
        $userCreate->purse = '';
        $userCreate->payeer = '';
        $userCreate->wmlogin = 0;
        $userCreate->icq = 0;
        $userCreate->browser = '';
        $userCreate->ip = '';
        $userCreate->reg_ip = '';
        $userCreate->referals = 0;
        $userCreate->referer = $refererIdBuxone;
        $userCreate->referer2 = 0;
        $userCreate->money = '0.0000';
        $userCreate->adv_money = '0.0000';
        $userCreate->savingscards = '0.0000';
        $userCreate->teaserweb = '0.0000';
        $userCreate->paid = '0.00';
        $userCreate->allpay = '0.00';
        $userCreate->toref = '0.0000';
        $userCreate->toref2 = '0.0000';
        $userCreate->golos = '0.0000';
        $userCreate->country = '0.00';
        $userCreate->city_code = '';
        $userCreate->regdate = time();
        $userCreate->logindate = time();
        $userCreate->wake = time();
        $userCreate->account = 0;
        $userCreate->avatar = 0;
        $userCreate->act_code = 'no.png';
        $userCreate->activate = '1';
        $userCreate->autoproc = 1;
        $userCreate->autoback = 0;
        $userCreate->rsite = 0;
        $userCreate->blockip = '0';
        $userCreate->reyting = 0;
        $userCreate->lastpay = '0.0';
        $userCreate->blockpay = 0;
        $userCreate->blockuser = 0;
        $userCreate->pincode = $pincode;
        $userCreate->addreyting = '0';
        $userCreate->sex = 0;
        $userCreate->birthday = 0;
        $userCreate->messblock = '0';
        $userCreate->admmessblock = 0;
        $userCreate->forumblock = 0;
        $userCreate->showbirthday = 0;
        $userCreate->walls_message_access = 0;
        $userCreate->flood_walls = 0;
        $userCreate->serf_view = 0;
        $userCreate->pixword_view = 0;
        $userCreate->test_view = 0;
        $userCreate->tasks_view = 0;
        $userCreate->userdel = 0;
        $userCreate->bone_craps_win = 0;
        $userCreate->bone_craps_los = '0.00';
        $userCreate->bj_win = '0.00';
        $userCreate->bj_los = '0.00';
        $userCreate->battleip = '0.00';
        $userCreate->ths_win = 0;
        $userCreate->ths_los = '0.00';
        $userCreate->knb_win = '0.00';
        $userCreate->knb_los = '0.00';
        $userCreate->rb_win = '0.00';
        $userCreate->rb_los = '0.00';
        $userCreate->help_project = 0;
        $userCreate->ref100 = 0;
        $userCreate->ref500 = 0;
        $userCreate->ref1000 = 0;
        $userCreate->statusMaster = 0;
        $userCreate->statusSMaster = 0;
        $userCreate->statusBiznes = 0;
        $userCreate->pay100 = 0;
        $userCreate->pay500 = 0;
        $userCreate->pay1000 = 0;
        $userCreate->pay5000 = 0;
        $userCreate->pay10000 = 0;
        $userCreate->forummes50 = 0;
        $userCreate->forummes100 = 0;
        $userCreate->forummes250 = 0;
        $userCreate->forummes500 = 0;
        $userCreate->forummes1000 = 0;

        $userCreate->save();
        $newId = $userCreate->id;


        UsersBuxoneSync::create([
            'user_buxone' => $newId,
            'user_out_site' =>Auth::id(),
            'site_type' => 1,
            'site_id' => Helpers::getSiteId(),
        ]);

        SyncBuxone::create([
            'user_id' => Auth::id(),
            'user_buxone' => $newId,
        ]);







        if (1) {
            return Response::json([
                "message" => "Пользователь успешно создан"
            ], 200);
        }

    }

    public static function auth(Request $request) {
        Config::set("database.connections.buxone", [
            "host" => "127.0.0.1",
            "database" => 'bux_one',
            "username" => 'root',
            "password" => "DrinkVodkaPlayDotka228",
            'driver' => 'mysql'
        ]);
        $hasher = new MD5Hasher();
        $userPass = $hasher->makeBuxone($request->pass);
        $userCount = UsersBuxone::where('username', $request->login)->where('password', $userPass)->count();


        if (!isset($request->login) or !isset($request->pass)) {
            return Response::json([
                "type" => "error",
                "message" => "Заполнены не все поля",
            ], 200);
        }

        if (!$userCount) {
            return Response::json([
                "type" => "error",
                "message" => "Неверный логин или пароль",
            ], 200);
        }

        $userBuxone = UsersBuxone::where('username', $request->login)->where('password', $userPass)->first();

        UsersBuxoneSync::create([
            'user_buxone' => $userBuxone['id'],
            'user_out_site' =>Auth::id(),
            'site_type' => 1,
            'site_id' => Helpers::getSiteId(),
        ]);

        SyncBuxone::create([
            'user_id' => Auth::id(),
            'user_buxone' => $userBuxone['id'],
        ]);


        return Response::json([
            "type" => "success",
            "message" => "Аккаунт привязан к Вашему профилю",
        ], 200);

    }

    public static function logout() {
        Config::set("database.connections.buxone", [
            "host" => "127.0.0.1",
            "database" => 'bux_one',
            "username" => 'root',
            "password" => "DrinkVodkaPlayDotka228",
            'driver' => 'mysql'
        ]);

        $userIdBuxone = SyncBuxone::where('user_id', Auth::id())->first()['user_buxone'];
        $siteId = Helpers::getSiteId();

        UsersBuxoneSync::where('user_buxone', $userIdBuxone)->where('site_id', $siteId)->delete();
        SyncBuxone::where('user_id', Auth::id())->delete();


        return Response::json([
            "type" => "success",
            "message" => "Профиль отключен",
        ], 200);

    }

    public static function cashout(Request $request) {
        Config::set("database.connections.buxone", [
            "host" => "127.0.0.1",
            "database" => 'bux_one',
            "username" => 'root',
            "password" => "DrinkVodkaPlayDotka228",
            'driver' => 'mysql'
        ]);

        $sum = $request->sum;

        DB::beginTransaction();

        $siteType = Helpers::siteType();
        $userInfo = Users::where('id', Auth::id())->first();

        if ($siteType == 1) {
            $balance = $userInfo->balance;
        } else {
            $balance = $userInfo->balance_cashout;
        }

        if ($balance >= $request->sum) {
            if ($siteType == 1) {
                Users::where('id', Auth::id())->decrement('balance', $request->sum);
            } else {
                Users::where('id', Auth::id())->decrement('balance_cashout', $request->sum);
            }
            $userIdBuxone = SyncBuxone::where('user_id', Auth::id())->first()['user_buxone'];
            UsersBuxone::where('id', $userIdBuxone)->increment('money', $request->sum);
            Cashout::create([
                'id_user' => Auth::id(),
                'payment_system' => 221,
                'id_payment' => 0,
                'currency',
                'amount',
                'status',
                'to',
                'date'

            ]);
        }



        return Response::json([
            "type" => "success",
            "message" => "Средства переведены на ваш аккаунт в iBux.best",
        ], 200);

    }

}
