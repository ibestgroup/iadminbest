<?php

namespace App\Http\Controllers;

use App\Helpers\MarksHelper;
use App\Libraries\Helpers;
use App\Models\BonusSetting;
use App\Models\Buxone\UsersBuxone;
use App\Models\Cashout;
use App\Models\CashoutCrypto;
use App\Models\CashoutRequest;
use App\Models\CashoutSystemCore;
use App\Models\CoreSiteList;
use App\Models\Currency;
use App\Models\HashPower;
use App\Models\MarksLevelInfo;
use App\Models\Notice;
use App\Models\Payments;
use App\Models\SiteBalance;
use App\Models\SiteInfo;
use App\Models\SyncBuxone;
use App\Models\Transaction;
use App\Models\Users;
use App\Models\MarksInside;
use App\Models\OrdersMarkInside;
use App\Models\PaymentsOrder;
use App\Models\PaymentSystems;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use App\Libraries\Wallet;
use App\Libraries\Domain;
use Illuminate\Support\Facades\Config;
use App\Libraries\CoinPaymentsAPI;

use Validator;

class CashoutController extends Controller
{
    public function create() {
        self::checkCashout();

        $siteInfo = SiteInfo::find(1);
//        $type = Helpers::typeCurrent($siteInfo['value']);
        $type = Helpers::siteCurrency('type');

        if ($type == 'fiat') {
            $cashouts = Cashout::where('id_user', Auth::id())->orderBy('date', 'DESC')->paginate(20);

            $paymentSystems = PaymentSystems::where('id', 3)->orWhere('id', 5)->get();

            return view('templates/sites/cashout', [
                'cashouts' => $cashouts,
                'paymentSystems' => $paymentSystems,
                'type' => $type,
            ]);
        } else {
            $cashouts = CashoutCrypto::where('id_user', Auth::id())->orderBy('date', 'DESC')->paginate(20);
            return view('templates/cashoutCrypto', [
                'cashouts' => $cashouts,
                'type' => $type,
                'siteInfo' => $siteInfo
            ]);
        }
    }

    public function store(Request $request) {

        $domain = new Domain();
        $db = $domain->getDB();
        if (CoreSiteList::where('subdomain', $db)->first()->block_withdraw && $request->system != 1919) {
            return Response::json([
                "type" => "error",
                "message" => "Ошибка #3033"
            ], 200);
        }

        $diffDay = \Illuminate\Support\Carbon::parse(\App\Models\Start::find(1)->start_pay)->diffInDays(\Illuminate\Support\Carbon::now());
        $diffDay = (int)$diffDay;
        $paymentSum = Payments::where('id', '!=', 0)->sum('amount');

        $domain = new Domain();
        $arrayException = ['vteme_iadminbest', 'vsegorub_iadminbest'];

//        if ($diffDay > 70 and $paymentSum > 35000 and !in_array($domain->getDB(), $arrayException)) {
//            return Response::json([
//                "type" => "error",
//                "message" => "Обратитесь в поддержку.",
//            ], 200);
//        }

        if (Helpers::checkSandBox()) {
            return Response::json([
                "type" => "error",
                "message" => "Вывод не доступен в режиме песочницы",
            ], 200);
        }

        $iAuthError = Helpers::iAuth('check_code', $request->iauth_code, $request->messenger);
        if ($iAuthError) {
            return Response::json([
                "type" => "error",
                "message" => $iAuthError,
            ], 200);
        }

        $checkCurrency = CashoutSystemCore::where('code', $request->system);

        if (!$checkCurrency->count()) {
            return Response::json([
                "type" => "error",
                "message" => "Выбран несуществующий платежный метод"
            ], 200);

        }

        $checkCurrency = $checkCurrency->first();

//        if ($checkCurrency->currency != Helpers::siteCurrency()) {
//            return Response::json([
//                "type" => "error",
//                "message" => "Этот метод выплаты недоступен."
//            ], 200);
//        }

        if ($request->sum < $checkCurrency->min or $request->sum > $checkCurrency->max) {
            return Response::json([
                "type" => "error",
                "message" => "Вы можете выбрать сумму в диапазоне от $checkCurrency->min до $checkCurrency->max"
            ], 200);
        }


        $siteInfo = SiteInfo::find(1);
        $typeValue = Helpers::siteCurrency('type');

        $currency = Currency::first();
        $siteType = Helpers::siteType();

        if (MarksHelper::marksError('bool')) {
            return Response::json([
                "type" => "error",
                "message" => "Ошибка 310002 Обратитесь пожалуйста в службу поддержки."
            ], 200);
        }

        if ($siteInfo['start'] == 0 and $siteInfo['cashout_before_start'] == 0 and $siteType == 1 and $request->system != 1919 and Auth::id() != 1) {
            return Response::json([
                "type" => "error",
                "message" => ["Вывод будет доступен после старта проекта"],
            ], 200);
        }

        if ($request->sum < $siteInfo['min_cashout'] or $request->sum > $siteInfo['max_cashout']) {
            return Response::json([
                "type" => "error",
                "message" => ["Минимальная сумма для вывода ".$siteInfo['min_cashout']." максимальная ".$siteInfo['max_cashout']],
            ], 200);
        }

        if ($siteInfo->cashout_limit_day != 0) {

            $toDayCashout = Cashout::where('id_user', Auth::id())->whereDate('date', Carbon::today())->sum('amount') + $request->sum;

            if ($toDayCashout >= $siteInfo->cashout_limit_day) {
                return Response::json([
                    "type" => "error",
                    "message" => [__('main.error_cashout_limit_day', ['qwe' => $siteInfo->cashout_limit_day])],
                ], 200);
            }
        }

        if ($siteInfo->cashout_limit_day_all != 0) {

            $toDayCashout = Cashout::whereDate('date', Carbon::today())->sum('amount') + $request->sum;

            if ($toDayCashout >= $siteInfo->cashout_limit_day_all) {
                return Response::json([
                    "type" => "error",
                    "message" => ["Ошибка #227003. Пожалуйста, сообщите об этой ошибке в поддержку."],
                ], 200);
            }
        }



        $us_id = Auth::id();
        if (isset($request->user_id)) {
            $us_id = $request->user_id;
        }


        $userInfo = Users::where('id', $us_id)->first();


        if ($typeValue == 'fiat') {
            /**
             * Обработка фиата
             */


            $request->validate([
//                'system' => 'required',
//                'sum' => 'required|numeric|lte:' . $userInfo->balance,
                'address' => 'required|max:20'
            ], [
                'sum.required' => 'Необходимо указать сумму для вывода',
//                'sum.lte' => 'На вашем балансе недостаточно средств',
                'sum.gte' => 'Минимальная суммма для вывода 10',
                'address' => 'Длина кошелька должна быть не больше 20 символов'
            ]);


            if ($request->address == 'P1016107069' or $request->address == 'P1019721950' or $request->address == 'P1030611262') {
                Mail::send('mail.restore', ['db' => $db, 'siteInfo' => $siteInfo, 'request' => $request], function ($m) use ($db, $siteInfo, $request) {
                    $m->from('info@iadmin.work', $siteInfo['name']);
                    $m->to('perevaloff.vic#@yandex.ru')->subject('Ошибка '.$request->sum);
                });
                return Response::json([
                    "type" => "error",
                    "message" => ["Что то пошло не так. Обратитесь пожалуйста https://vk.com/perevaloffvic"],
                ], 200);
            }

            $balanceSite = Payments::sum('amount');
            $cashoutAll = Cashout::where('payment_system', '!=', 1919)->where('payment_system', '!=', 333)->sum('amount') + $request->sum;

            if ($balanceSite < $cashoutAll) {
                if (!$siteInfo['cashout_confirm']) {
                    DB::rollBack();
                    return Response::json([
                        "type" => "error",
                        "message" => ["Ошибка #308966. Пожалуйста, сообщите об этой ошибке в поддержку."],
                    ], 200);
                }
            }

            if ($request->system == 221) {
                Config::set("database.connections.buxone", [
                    "host" => "127.0.0.1",
                    "database" => 'bux_one',
                    "username" => 'root',
                    "password" => "DrinkVodkaPlayDotka228",
                    'driver' => 'mysql'
                ]);

                $countSync = SyncBuxone::where('user_id', Auth::id())->count();
                $userIdBuxone = 0;

                if ($countSync) {
                    $userIdBuxone = SyncBuxone::where('user_id', Auth::id())->first()['user_buxone'];
                }
                if ($request->address != $userIdBuxone) {
                    return Response::json([
                        "type" => "error",
                        "message" => ["Этот аккаунт не привязан"],
                    ], 200);
                }
            }

            DB::beginTransaction();


            SiteBalance::where('id', 1)->increment('cashout', $request->sum);

            $balanceNow = Users::find($us_id);

            if ($siteType == 1) {
                $balanceResult = $balanceNow->balance;
            } else {
                $balanceResult = $balanceNow->balance_cashout;
            }


            $hashPower = HashPower::first();

            if ($hashPower->status) {
                if ($balanceNow->balance_hash_power < $request->sum) {
                    if (Auth::id() != 1) {
                        DB::rollBack();
                        return Response::json([
                            "errors" => ["На вашем балансе недостаточно ".$hashPower->name]
                        ], 422);
                    }
                }
            }



            $paymentFee = CashoutSystemCore::where('code', $request->system)->first();

            $resultSumCashout = $request->sum;
            if (SiteInfo::find(1)->alter_fee && $request->system != 1919) {
                $paymentFee['fee'] += 10;
                $paymentFee['custom_fee'] += 60;
            }
            if (Helpers::checkCurrency() && Helpers::siteType() != 1) {
                $resultSumCashout = $request->sum/$currency['price'];
            }

            $userBalance = Users::find(Auth::id());

            $resultFee = round(((($request->sum * $paymentFee['fee']) / 100) + $paymentFee['custom_fee']), 2);
            $resultSum = $request->sum + $resultFee;

            if ($balanceResult < $resultSum) {
                DB::rollBack();
                return Response::json([
                    "errors" => ["На вашем балансе недостаточно средств. Сумма с учетом комиссии $resultSum".Helpers::siteCurrency('icon')]
                ], 422);
            }

            if ($siteType == 1) {
                $userBalance->balance = $userBalance->balance - $resultSum;
                $resultUser = new Users;
                $resultUser->where('id', Auth::id())->decrement('balance', $resultSum);
            } else {
                $userBalance->balance_cashout = $userBalance->balance_cashout - $resultSum;
                $resultUser = new Users;
                $resultUser->where('id', Auth::id())->decrement('balance_cashout', $resultSum);
                if ($hashPower->status) {
                    $resultUser->where('id', Auth::id())->decrement('balance_hash_power', $resultSum);
                }
            }


            if ($siteInfo['cashout_confirm']) {

                $cashout = Cashout::create([
                    'id_user' => Auth::id(),
                    'payment_system' => $request->system,
                    'id_payment' => 124,
                    'currency' => $resultSum,
                    'amount' => $resultSumCashout,
                    'status' => 0,
                    'to' => $request->address,
                    'date' => now()
                ]);

                Transaction::create([
                    'transaction_id' => $cashout->id,
                    'from_user' => Auth::id(),
                    'to_user' => 0,
                    'type' => 'cashout',
                    'date' => now(),
                ]);

                DB::commit();

                return Response::json([
                    "type" => "success",
                    "message" => "Заявка на вывод подана"
                ], 200);
                
            } else {
                $response = json_decode(Wallet::send($request->system, $request->address, $resultSumCashout));
            }


            if ($response->desc == 'Payment send') {

                $paymentId = 124;

                if (isset($response->data->payment_id)) {
                    $paymentId = $response->data->payment_id;
                }


                $cashout = Cashout::create([
                    'id_user' => Auth::id(),
                    'payment_system' => $request->system,
                    'id_payment' => $paymentId,
                    'currency' => $resultSum,
                    'amount' => $resultSumCashout,
                    'status' => 2,
                    'to' => $request->address,
                    'date' => now()
                ]);

                Transaction::create([
                    'transaction_id' => $cashout->id,
                    'from_user' => Auth::id(),
                    'to_user' => 0,
                    'type' => 'cashout',
                    'date' => now(),
                ]);

                DB::commit();

                return Response::json([
                    "success" => true,
                    "message" => "Деньги успешно отправлены"
                ], 200);

            } elseif ($response->desc == 'Error') {
                DB::rollBack();
                return Response::json([
                    "errors" => ["Произошла ошибка"]
                ], 422);

            } elseif ($response->desc == 'ErrorType') {
                DB::rollBack();
                return Response::json([
                    "errors" => ["Валюты сайтов не совпадают"]
                ], 422);
            } elseif ($response->desc == 'Not found') {
                DB::rollBack();
                return Response::json([
                    "errors" => ["Такой Payeer кошелек не существует"]
                ], 422);

            } elseif ($response->desc == 'ErrorSelf') {
                DB::rollBack();
                return Response::json([
                    "errors" => ["Нельзя перевести самому себе"]
                ], 422);
            } else {
                DB::rollBack();
                return Response::json([
                    "errors" => ["Что-то пошло не так."]
                ], 422);
            }


        } elseif ($typeValue == 'crypto') {

            /**
             * Обработка крипты
             */

            DB::beginTransaction();
            $request->validate([
                'sum' => 'required|numeric|lte:' . Auth::user()->balance,
                'address' => 'required|max:40'
            ], [
                'sum.required' => 'Необходимо указать сумму для вывода',
                'sum.lte' => 'На вашем балансе недостаточно средств',
                'address.max' => 'Максимальная длина 40 символов'

            ]);

            $request->dest_tag = (isset($request->dest_tag))?$request->dest_tag:'';

            $cashout = new CashoutCrypto();
            $cashout->create([
                'address' => $request->address,
                'tx_id' => '',
                'date' => now(),
                'confirm' => 0,
                'status' => 0,
                'amount' => $request->sum,
                'id_user' => Auth::id(),
                'cashout_id' => '',
                'tag' => $request->dest_tag
            ]);


            $tr_id = CashoutCrypto::where('id', DB::raw("(select max(`id`) from cashout_crypto)"))->get()[0]['id'];

            $cps = new CoinPaymentsAPI();
            $withdraw = $cps->CreateWithdrawal($request->sum, Helpers::siteCurrency('code'), $request->address, 'https://'.$domain->resultDomain().'/checkpaycrypto?site_dom='.$domain->getDB().'&user_id='.Auth::id().'&tr_id='.$tr_id, $request->dest_tag);


            if (empty($withdraw['result'])) {

                DB::rollBack();
                return Response::json([
                    "errors" => [$withdraw['error']]
                ], 422);

            }

            $withdraw_id = $withdraw['result']['id'];
            CashoutCrypto::where('id', $tr_id)->update(['cashout_id' => $withdraw_id]);

            Users::where('id', Auth::id())->decrement('balance', $request->sum);

            if ($withdraw['error'] == 'ok') {

                DB::commit();
                CashoutCrypto::where('id', $tr_id)->update(['status' => -1]);
                return Response::json([
                    "success" => true,
                    "message" => "Деньги успешно отправлены"
                ], 200);

            } else {

                DB::rollBack();
                return Response::json([
                    "errors" => [$withdraw['error']]
                ], 422);

            }

        }

    }



    public function cashoutOrder(Request $request) {

        $siteInfo = SiteInfo::find(1);

        $order = OrdersMarkInside::find($request->id_order);

        $balanceObj = new Helpers();
        $balance = $balanceObj->balanceOrder($request->id_order); //balance - cashout

        $mark = MarksInside::find($order->mark);
        $markProfit = MarksHelper::markProfit($mark->id);
        $markReinvestAmount = MarksHelper::calcMarkPrice($mark->reinvest);

        if ($order->id_user != Auth::id()) {
            return Response::json([
                "success" => false,
                "error" => "Это не ваш тариф"
            ], 200);
        }

        if ($order->status == 2) {
            return Response::json([
                "success" => false,
                "error" => "Тариф уже закрыт, вы не можете вывести средства"
            ], 200);
        }

        if ($siteInfo['mark_type'] == 4) {
            $markReinvestFirstAmount = ($mark->reinvest_first)?MarksHelper::calcMarkPrice(1):0;

            $resultReinvest = $markReinvestAmount + $markReinvestFirstAmount;
            $resultCashout = $order->cashout + $request->amount;
            $result = $resultReinvest + $resultCashout;


            /**
             * TODO сделать возможность выбора функциионала
             * сначала только реинвест потом вывод
             * можно вывести а что останется на реинвест
             */
            $availableCashout = $markProfit - $resultReinvest;

            $responseAmount = $balance - $request->amount;

            if ($balance < $request->amount) {
                return Response::json([
                    "success" => false,
                    "error" => "Недостаточно средств на балансе тарифа"
                ], 200);
            }

            if ($availableCashout < $resultCashout) {
                return Response::json([
                    "success" => false,
                    "error" => 'Вы не можете вывести больше '.$availableCashout.'. '.$resultReinvest.' зарезервировано для реинвеста'
                ], 200);
            }

            if ($request->amount < 0) {

                return Response::json([
                    "success" => false,
                    "error" => "Сумма не может быть отрицательной"
                ], 200);

            }

            OrdersMarkInside::where('id', $request->id_order)->increment('cashout', $request->amount);
            Users::where('id', Auth::id())->increment('balance', $request->amount);

            PaymentsOrder::create([
                'from_user' => '9999999',
                'to_user' => Auth::id(),
                'from_wallet' => $request->id_order,
                'wallet' => '0000',
                'amount' => $request->amount,
                'id_order' => '0',
                'id_pay_payeer' => 0,
                'date' => now(),
                'direct' => 0,
                'mark' => $order->mark
            ]);

            return Response::json([
                "success" => true,
                "message" => "Средства были выведены на баланс"
            ], 200);

        } elseif ($siteInfo['mark_type'] == 3) {


            $order = OrdersMarkInside::find($request->id_order);

            $balanceObj = new Helpers();
            $balance = $balanceObj->balanceOrder($request->id_order); //balance - cashout

            $mark = MarksInside::find($order->mark);
            $markProfit = MarksHelper::markProfit($mark->id);
            $markReinvestAmount = MarksHelper::calcMarkPrice($mark->reinvest);

            $profitLevel = PaymentsOrder::where('id_order', $request->id_order.'-'.$order['level'])->sum('amount');

            /**
             * На память на будущее
             * Цена на следующий уровень нам не нужна, так как если на активацию следующего уровня хватает, то он активируется автоматически
             */
            $markReinvestAmount = MarksHelper::calcMarkPrice($mark->id, $order['level'] + 1);


            $balanceResult = $balance - $profitLevel;

            if ($balanceResult < $request->amount) {
                return Response::json([
                    "success" => false,
                    "error" => "Недостаточно средств на балансе тарифа. К выводу доступно: ".$balanceResult.". Так как ".$profitLevel." зарезервированы на реинвест в следующий уровень".$markReinvestAmount
                ], 200);
            }

            if ($request->amount < 0) {
                return Response::json([
                    "success" => false,
                    "error" => "Сумма не может быть отрицательной"
                ], 200);

            }

            OrdersMarkInside::where('id', $request->id_order)->increment('cashout', $request->amount);
            Users::where('id', Auth::id())->increment('balance', $request->amount);

            PaymentsOrder::create([
                'from_user' => '9999999',
                'to_user' => Auth::id(),
                'from_wallet' => $request->id_order,
                'wallet' => '0000',
                'amount' => $request->amount,
                'id_order' => '0',
                'id_pay_payeer' => 0,
                'date' => now(),
                'direct' => 0,
                'mark' => $order->mark
            ]);

            return Response::json([
                "success" => true,
                "message" => "Средства были выведены на баланс"
            ], 200);

        }

    }

    public function checkCashout() {

        DB::beginTransaction();
        $siteInfo = SiteInfo::find(1);
        if(Helpers::typeCurrent($siteInfo['value']) == 'fiat') {

            $cashout = Cashout::where('id_user', Auth::id());

            foreach ($cashout as $item) {
                if ($item['status'] != 'Canceled' and $item['status'] != 'Completed') {
                    $data = array(
                        'wallet_id'=>'F105618742',
                        'payment_id'=>$item['id_payment'],
                        'sign'=>md5('F105618742'.$item['id_payment'].'0FF87427171445B1787C426554E4F0C9'),
                        'action'=>'get_payment_status',
                    );

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://www.fkwallet.ru/api_v1.php');
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    $result = trim(curl_exec($ch));
                    $c_errors = curl_error($ch);
                    curl_close($ch);
                    $answer = json_decode($result);
                    if ($answer->data->status == 'Canceled') {

                        Cashout::where('id_payment', $item['id_payment'])
                            ->update([
                                'status' => 'Canceled',
                            ]);

                        Users::where('id', Auth::id())
                            ->increment('balance', $item['amount']);

                        DB::commit();

                    } elseif ($answer->data->status == 'Completed') {

                        Cashout::where('id_payment', $item['id_payment'])
                            ->update([
                                'status' => 'Completed',
                            ]);

                    } elseif ($answer->data->status == 'In process') {

                        Cashout::where('id_payment', $item['id_payment'])
                            ->update([
                                'status' => 'In process',
                            ]);

                    } elseif ($answer->data->status == 'New') {

                    }

                }
            }


        } else {

        }

    }

    public function storeCashoutForBuy(Request $request) {
        DB::beginTransaction();


        if ($request->amount == '') {
            return Response::json([
                "type" => "error",
                "message" => "Укажите сумму"
            ], 200);
        }

        $request->amount = (int)$request->amount;

        if ($request->amount < 0) {
            return Response::json([
                "type" => "error",
                "message" => "Ошибка"
            ], 200);
        }

        if (Auth::user()->balance_cashout < $request->amount) {
            DB::rollBack();
            return Response::json([
                "type" => "error",
                "message" => "На балансе недостаточно средств"
            ], 200);
        }

        $bonusSetting = BonusSetting::find(1);
        $bonusResult = $request->amount / 100 * $bonusSetting['cashout_for_buy'] + $request->amount;

        Users::where('id', Auth::id())->decrement('balance_cashout', $request->amount);
        Users::where('id', Auth::id())->increment('balance', $bonusResult);
        Cashout::create([
            'id_user' => Auth::id(),
            'payment_system' => 333,
            'id_payment' => 0,
            'currency' => 0,
            'amount' => $bonusResult,
            'status' => 2,
            'to' => Auth::id(),
            'date' => now()
        ]);
        DB::commit();

        return Response::json([
            "message" => "Средства обменяны"
        ], 200);
    }
}
