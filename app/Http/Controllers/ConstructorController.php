<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class ConstructorController extends Controller
{

    public function create() {
        if (Auth::id() != 1) {
            return redirect('profile');
        }

        require $_SERVER['DOCUMENT_ROOT']."/iadminbest/constructor/index.php";
    }

}
