<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\CoreSiteList;
use App\Models\SiteInfo;
use App\Models\Users;
//use Auth;
use App\Models\UsersData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class CreateSiteController extends Controller
{

    public static function store(Request $request)
    {

        if (strlen($request->domain) > 25) {
            return Response::json([
                "type" => "error",
                "message" => "Максимальная длина домена 25"
            ], 200);
        }

        if ($request->site_type < 1 or $request->site_type > 3) {

            return Response::json([
                "type" => "error",
                "message" => "Ну зачем вам панель разработчика? Для вас сделали все максимально просто, не усложняйте себе жизнь =)"
            ], 200);
        }

        if (!preg_match('/^[0-9a-z]+$/', $request->domain)) {
            return Response::json([
                "type" => "error",
                "message" => "Разрешены только маленькие латинские буквы и цифры"
            ], 200);
        }

        if (CoreSiteList::where('subdomain', $request->domain."_iadminbest")->count()) {
            return Response::json([
                "type" => "error",
                "message" => "Такой сайт уже существует",
            ], 200);
        }

        $currency = $request->currency ?? 1;

        Config::set("database.connections.new", [
            "host" => "127.0.0.1",
            "database" => null,
            "username" => 'root',
            "password" => "DrinkVodkaPlayDotka228",
            'driver' => 'mysql'
        ]);

        try {
            DB::connection('new')->statement("CREATE DATABASE `".$request->domain."_iadminbest`");
        } catch (\Exception $e) {
            return Response::json([
                "type" => "error",
                "message" => "Произошла ошибка при создании, попробуйте выбрать другое имя",
            ], 200);
        }

        $newConnect = mysqli_connect('127.0.0.1', 'root', 'DrinkVodkaPlayDotka228', $request->domain.'_iadminbest');

        Config::set("database.connections.newupdate", [
            "host" => "127.0.0.1",
            "database" => $request->domain.'_iadminbest',
            "username" => 'root',
            "password" => "DrinkVodkaPlayDotka228",
            'driver' => 'mysql'
        ]);

        $lang = $request->lang ?? Config::get('app.locale');


        CoreSiteList::create([
            'user_id' => Auth::id(),
            'subdomain' => $request->domain.'_iadminbest',
            'type' => $request->site_type,
            'create_date' => now(),
            'lang' => $lang ?? 'ru',
            'currency' => $currency ?? 1,
        ]);


        if ($request->site_type == 1) {
            if ($lang == 'ru') {
                $filename = 'iadminbest_example.sql';
            } else {
                $filename = 'iadminbest_example_en.sql';
            }
        } else if ($request->site_type == 2) {
            $filename = 'iadminbest_invest.sql';
        } else if ($request->site_type == 3) {
            $filename = 'iadminbest_ferm.sql';
        }
        $templine = '';
        $lines = file($filename);
        foreach ($lines as $line) {
            if (substr($line, 0, 2) == '--' || $line == '')
                continue;
            $templine .= $line;
            if (substr(trim($line), -1, 1) == ';') {
                mysqli_query($newConnect, $templine);
                $templine = '';
            }
        }

        Users::on('newupdate')->where('id', 1)->update([
            'password' => '339dfa6c304829ecb33ac6104c559097',
        ]);

        UsersData::on('newupdate')->where('id_user', 1)->update([
            'link_visit' => 0
        ]);

        if ($lang == 'ru') {
            $siteName = 'Мой сайт';
        } else{
            $siteName = 'My website';
        }

        SiteInfo::on('newupdate')->where('id', 1)->update([
            'name' => $siteName,
            'no_image' => 'resource/avatars/0.jpg',
            'logo' => 'resource/logo.png',
        ]);

//        DB::on('newupdate')->statement("ALTER TABLE `users` CHANGE `balance` `balance` DECIMAL(10,8) NOT NULL;");



        self::createMainPage($request->domain.'_iadminbest');

        return Response::json([
            "head" => "Ваш сайт успешно создан",
            "message" => "Ваш сайт успешно создан теперь он доступен по ссылке <a href='https://".$request->domain.".iadmin.work'>https://".$request->domain.".iadmin.work</a>. <br> Для авторизации используйте стандартные логин и пароль."
        ], 200);

    }

    public static function createMainPage($domain) {

//        mkdir("resource/sites/".$domain."/", 0777);
//        mkdir("resource/sites/".$domain."/logo", 0777);
//        mkdir("resource/sites/".$domain."/helper", 0777);
//        mkdir("resource/sites/".$domain."/avatars", 0777);
        mkdir($_SERVER['DOCUMENT_ROOT'].'/sites/'.$domain, 0777,true);
        $file = $_SERVER['DOCUMENT_ROOT'].'/sites/'.$domain.'/index.html';

        $lang = Config::get('app.locale');

        if ($lang == 'ru') {

            $title = 'Новый сайт';
            $text = '<br><br><h2 class="title editContent">Приветствуем тебя на твоем новом сайте</h2>
                    <hr class="mr-tb-20">
                    <p class="title-sub editContent"></p>
                    <p><span style="font-size: 18px;"> </span><span style="font-family: inherit; font-size: 18px; font-style: inherit; font-variant-ligatures: inherit; font-variant-caps: inherit; font-weight: inherit; color: rgb(255, 255, 255); text-align: center;">Теперь ты владелец этого сайта. Мы можем лишь помогать тебе поддерживать его.</span></p>
                    <p style=\'margin-bottom: 10px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 20px; line-height: inherit; font-family: "Open Sans", sans-serif; vertical-align: baseline; color: rgb(255, 255, 255); text-align: center;\'><span style="margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 18px; line-height: inherit; font-family: inherit; vertical-align: baseline;">Для работы с сайтом нажми на кнопку "ВХОД" и введи стандартные логин и пароль.</span></p>
                    <p style=\'margin-bottom: 10px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 20px; line-height: inherit; font-family: "Open Sans", sans-serif; vertical-align: baseline; color: rgb(255, 255, 255); text-align: center;\'><span style="margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 18px; line-height: inherit; font-family: inherit; vertical-align: baseline;">Логин: admin Пароль: admin</span></p>
                    <p></p>
                    <a href="/login" class="btn btn-line light btn-sm mr-t-40" style="border-radius: 4px; font-size: 14px; background-color: rgba(0, 0, 0, 0);">ВХОД</a>';

        } else {

            $title = 'My website';
            $text = '<br><br><h2 class="title editContent">Welcome to your new website</h2>
                    <hr class="mr-tb-20">
                    <p class="title-sub editContent"></p>
                    <p><span style="font-size: 18px;"> </span><span style="font-family: inherit; font-size: 18px; font-style: inherit; font-variant-ligatures: inherit; font-variant-caps: inherit; font-weight: inherit; color: rgb(255, 255, 255); text-align: center;">Now this is absolutely your web project. We can only help and support you.</span></p>
                    <p style=\'margin-bottom: 10px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 20px; line-height: inherit; font-family: "Open Sans", sans-serif; vertical-align: baseline; color: rgb(255, 255, 255); text-align: center;\'><span style="margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 18px; line-height: inherit; font-family: inherit; vertical-align: baseline;">To setup this new project, press "Login" and input default username and password. (admin/admin)</span></p>
                    <p style=\'margin-bottom: 10px; padding: 0px; border: 0px; font-variant-numeric: inherit; font-variant-east-asian: inherit; font-stretch: inherit; font-size: 20px; line-height: inherit; font-family: "Open Sans", sans-serif; vertical-align: baseline; color: rgb(255, 255, 255); text-align: center;\'><span style="margin: 0px; padding: 0px; border: 0px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; font-size: 18px; line-height: inherit; font-family: inherit; vertical-align: baseline;">Login: admin Password: admin</span></p>
                    <p></p>
                    <a href="/login" class="btn btn-line light btn-sm mr-t-40" style="border-radius: 4px; font-size: 14px; background-color: rgba(0, 0, 0, 0);">Sign in</a>';

        }

        if (!file_exists($file)) {
            $fp = fopen($file, "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту),мы создаем файл
            fwrite($fp, "
                <!DOCTYPE html>
<!--[if lt IE 7 ]><html class=\"ie ie6\" lang=\"en\"> <![endif]--><!--[if IE 7 ]><html class=\"ie ie7\" lang=\"en\"> <![endif]--><!--[if IE 8 ]><html class=\"ie ie8\" lang=\"en\"> <![endif]--><!--[if (gte IE 9)|!(IE)]><!--><html lang=\"en\">
<!--<![endif]--><head>
<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">
<!-- Basic Page Needs
    ================================================== --><meta charset=\"utf-8\">
<title>".$title."</title>
<meta name=\"description\" content=\"\">
<meta name=\"keywords\" content=\"\">
<meta name=\"author\" content=\"\">
<!-- Mobile Specific Metas
    ================================================== --><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1\">
<meta http-equiv=\"x-ua-compatible\" content=\"IE=9\">
<!-- Font Awesome --><link href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\" rel=\"stylesheet\">
<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. --><!--[if lt IE 9]>
      <script src=\"js/html5shiv.js\"></script>
      <script src=\"js/respond.min.js\"></script>
    <![endif]--><!--headerIncludes--><!-- CSS
    ================================================== --><link rel=\"stylesheet\" href=\"/iadminbest/constructor/elements/stylesheets/menu.css\">
<link rel=\"stylesheet\" href=\"/iadminbest/constructor/elements/stylesheets/flat-ui-slider.css\">
<link rel=\"stylesheet\" href=\"/iadminbest/constructor/elements/stylesheets/base.css\">
<link rel=\"stylesheet\" href=\"/iadminbest/constructor/elements/stylesheets/landings.css\">
<link rel=\"stylesheet\" href=\"/iadminbest/constructor/bootstrap/css/bootstrap.css\">
<link rel=\"stylesheet\" href=\"/iadminbest/constructor/elements/stylesheets/skeleton.css\">
<link rel=\"stylesheet\" href=\"/iadminbest/constructor/elements/stylesheets/main.css\">
<link rel=\"stylesheet\" href=\"/iadminbest/constructor/elements/stylesheets/landings_layouts.css\">
<link rel=\"stylesheet\" href=\"/iadminbest/constructor/elements/stylesheets/box.css\">
<link rel=\"stylesheet\" href=\"/iadminbest/constructor/elements/stylesheets/pixicon.css\">
<link rel=\"stylesheet\" href=\"/iadminbest/constructor/elements/stylesheets/rgen.min.css\">
<link href=\"/iadminbest/constructor/elements/assets/css/animations.min.css\" rel=\"stylesheet\" type=\"text/css\" media=\"all\">
<link rel=\"stylesheet\" href=\"/iadminbest/constructor/css/cryptocoins.css\">
<!--[if lt IE 9]>
      <script src=\"http://html5shim.googlecode.com/svn/trunk/html5.js\"></script>
    <![endif]--><!-- Favicons
    ================================================== --><link rel=\"shortcut icon\" href=\"/iadminbest/storage/app/\">
<!--    <link rel=\"apple-touch-icon\" href=\"/constructor/elements/images/apple-touch-icon.png\">--><!--    <link rel=\"apple-touch-icon\" sizes=\"72x72\" href=\"/constructor/elements/images/apple-touch-icon-72x72.png\">--><!--    <link rel=\"apple-touch-icon\" sizes=\"114x114\" href=\"/constructor/elements/images/apple-touch-icon-114x114.png\">-->
</head>
<body>

<div id=\"page\">
<!-- Video section -->
<!-- / Video section --><!-- Video section -->
<!-- / Video section --><!-- Video section -->
<!-- / Video section --><!-- Video section -->
<!-- / Video section --><!-- Video section -->
<section class=\"video-section video-section-3\"><div class=\"pix_builder_video_bg\" data-type=\"bg-video\" style=\"outline-offset: -3px;\">
        <div class=\"bg-section bg-cover bg-cc full-wh\" id=\"videobg1\" data-bg=\"images/bg7.jpg\">
            <b class=\"full-wh\" style=\"background: linear-gradient(to top right, #ff8d00 60%, #222933 61%)\"></b>
            <!-- BACKGROUND VIDEO -->
            <div class=\"videobg\" data-video=\"dorZ3vag5PI\"></div>
        </div>
        <div class=\"container\">
            <div class=\"content\">".$text."
                <br><br>
</div>
        </div>
<!-- /.container -->
    </div>
</section><!-- / Video section -->
</div>

<script src=\"/constructor/elements/js/jquery-1.8.3.min.js\" type=\"text/javascript\"></script><script src=\"/constructor/elements/js-files/jquery.easing.1.3.js\" type=\"text/javascript\"></script><script src=\"/constructor/elements/js-files/jquery.common.min.js\" type=\"text/javascript\"></script><script src=\"/constructor/elements/js-files/jquery.ui.touch-punch.min.js\"></script><script src=\"/constructor/elements/js-files/jquery.mb.YTPlayer.min.js\"></script><script src=\"/constructor/elements/assets/js/smoothscroll.min.js\" type=\"text/javascript\"></script><script src=\"/constructor/elements/js-files/ticker.js\" type=\"text/javascript\"></script><script src=\"/constructor/elements/js-files/bootstrap.min.js\"></script><script src=\"/constructor/elements/js-files/bootstrap-switch.js\"></script><script src=\"/constructor/elements/assets/js/appear.min.js\" type=\"text/javascript\"></script><script src=\"/constructor/elements/js-files/custom.js\" type=\"text/javascript\"></script><script src=\"/constructor/elements/js-files/custom3.js\" type=\"text/javascript\"></script><script src=\"/constructor/js/prebuild.js\"></script><script src=\"/constructor/elements/assets/js/appear.min.js\" type=\"text/javascript\"></script><script src=\"/constructor/elements/assets/js/animations.js\" type=\"text/javascript\"></script>
</body>
</html>

                ");
            fclose($fp);
            chmod($file, 0777);
        }
    }

}
