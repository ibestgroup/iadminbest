<?php

namespace App\Http\Controllers;

use App\Libraries\Helpers;
use App\Models\CashoutCrypto;
use App\Models\Payments;
use App\Models\PaymentsCrypto;
use App\Models\SiteInfo;
use App\Models\Users;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use App\Libraries\Domain;
use Illuminate\Http\RedirectResponse;
use App\Libraries\CoinPaymentsAPI;
use Mail;

class CryptoHandlerController extends Controller
{

    public $cp_merchant_id = '999975313f47d5cf4c33570cabdefd82';
    public $cp_ipn_secret = 'tDjNCuEM6PbUiSu';


    public function handler(Request $request)
    {
//        dd($request->abra);
        if (!isset($request->ipn_mode) || $request->ipn_mode != 'hmac') {
            $this->errorAndDie('IPN Mode is not HMAC');
        }

        if (!isset($_SERVER['HTTP_HMAC']) || empty($_SERVER['HTTP_HMAC'])) {
            $this->errorAndDie('No HMAC signature sent.');
        }

        $requestCheck = file_get_contents('php://input');
        if ($requestCheck === FALSE || empty($requestCheck)) {
            $this->errorAndDie('Error reading POST data');
        }

        if (!isset($request->merchant) || $request->merchant != trim($this->cp_merchant_id)) {
            $this->errorAndDie('No or incorrect Merchant ID passed');
        }

        $hmac = hash_hmac("sha512", $requestCheck, trim($this->cp_ipn_secret));
        if ($hmac != $_SERVER['HTTP_HMAC']) {
            //if ($hmac != $_SERVER['HTTP_HMAC']) { <-- Use this if you are running a version of PHP below 5.6.0 without the hash_equals function
            $this->errorAndDie('HMAC signature does not match');
        }

        // HMAC Signature verified at this point, load some variables.


        $txn_id = $request->txn_i;
        $address = trim($request->address);
        $amount = floatval($request->amount);
        $status = intval($request->status);
        $tr_id = intval($request->tr_id);

//        $CONNECT = mysqli_connect('localhost', $_REQUEST['site_dom'], 'GenRiGeil120AntarktiDa', $_REQUEST['site_dom']);

        if ($status >= 100 || $status == 2) {
            //TODO: Обновляем баланс в таблице пользователей, следом обновляем нашу транзакцию
            if ($request->ipn_type == 'deposit') {
                Users::where('id', $request->user_id)
                    ->increment('balance', $amount);

                PaymentsCrypto::where('address', $address)
                    ->update(['status' => 2, 'date' => now(), 'confirm' => $request->confirms]);
            } else {
                CashoutCrypto::where('id', $tr_id)
                    ->update(['status' => 2, 'tx_id' => $txn_id, 'date' => now()]);
            }
        } else if ($status < 0) {
            if ($request->ipn_type == 'deposit') {
                //TODO: Если транзакция есть,  то  обновляем таблицу payment_crypto
                PaymentsCrypto::where('address', $address)
                    ->update(['amount' => $amount, 'tx_id' => $txn_id, 'status' => -1, 'date' => now(), 'confirm' => $request->confirms]);
            } else {
                CashoutCrypto::where('id', $tr_id)
                    ->update(['amount' => $amount, 'tx_id' => $tr_id, 'status' => -1, 'date' => now(), 'confirm' => 0]);
            }
        } else {
            if ($request->site_dom && $request->user_id) {
                if ($request->ipn_type == 'deposit') {
                    //TODO: Если транзакция есть,  то  обновляем таблицу payment_crypto

                    PaymentsCrypto::where('address', $address)
                        ->update(['amount' => $amount, 'tx_id' => $txn_id, 'status' => -1, 'date' => now(), 'confirm' => $request->confirms]);

                } else {

                    Mail::send('mail.error', ['info' => 'Списание'], function ($m) {
                        $m->from('hello@app.com', 'Your Application');

                        $m->to('youngoldman@yandex.ru', 'Арнольд')->subject('CoinPayments IPN Success');
                    });

                    CashoutCrypto::where('id', $tr_id)
                        ->update(['amount' => $amount, 'tx_id' => $tr_id, 'status' => -1, 'date' => now(), 'confirm' => 0]);
                }
            }
        }
        die('IPN OK');
    }

    function errorAndDie($error_msg) {
//        if (!empty($cp_debug_email)) {
        if (1) {
            $report = 'Error: '.$error_msg."\n\n";
            $report .= "POST Data\n\n";
            foreach ($_REQUEST as $k => $v) {
                $report .= "|$k| = |$v|\n";
            }
            Mail::send('mail.error', ['info' => $report], function ($m) {
                $m->from('hello@app.com', 'Your Application');

                $m->to('youngoldman@yandex.ru', 'Арнольд')->subject('CoinPayments IPN Success');
            });
            mail('youngoldman@yandex.ru', 'CoinPayments IPN Error', $report);
        }
        die('IPN Error: '.$error_msg);
    }




}
