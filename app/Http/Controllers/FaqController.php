<?php

namespace App\Http\Controllers;

use App\Models\Faq;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public static function all(Request $request)
    {
        $items = Faq::all();

        return view('templates.sites.faq.faq', compact('items'));
    }

    public static function faqAdd()
    {
        $items = Faq::all();

        return view('templates.sites.faq.faqAdd', compact('items'));
    }

    public static function info($id)
    {
        $item = Faq::where('id', $id)->first();

        return view('templates.sites.faq.faqInfo', compact('item'));
    }

    public static function edit(Request $request)
    {
        Faq::where('id', $request->id)->update([
            'ask' => $request->ask,
            'answer' => $request->answer,
        ]);

        return Response::json([
            "message" => "Вопрос отредактирован"
        ], 200);
    }

    public static function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'text' => 'required'
        ], [
            'name.required' => 'Заполните вопрос',
            'text.lte' => 'Укажите ответ'
        ]);


        $faq = Faq::create([
            'ask' => $request->name,
            'answer' => $request->text
        ]);

        return Response::json([
            "type" => "success",
            "message" => "Вопрос успешно создан"
        ], 200);
    }

    public static function delete(Request $request) {

        if (Faq::destroy($request->id)) {
            return Response::json([
                "message" => "Вопрос удален"
            ], 200);
        }

        return Response::json([
            "message" => "Не получилось удалить вопрос"
        ], 422);

    }

}
