<?php
namespace App\Http\Controllers;

use App\Models\Friends;
use App\Models\News;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class FriendsController extends Controller {
    public static function create(Request $request) {

        $type = $request->input('type');

        if ($type == 'new') {
            $item = Friends::where('toid', Auth::id())->where('status', 0)->paginate(15);
            $title = "Новые заявки";
            $emptyMessage = "У Вас нет новых заявок в друзья";
        } elseif ($type == 'out') {
            $item = Friends::where('fromid', Auth::id())->where('status', 0)->paginate(15);
            $title = "Исходящие заявки";
            $emptyMessage = "У Вас нет исходящих заявок в друзья";
        } else {
            $item = Friends::where('toid', Auth::id())->where('status', 1)->paginate(15);
            $title = "Друзья";
            $emptyMessage = "У Вас пока что нет друзей на сайте";
        }

        return view('templates/friends/friends', array(
            'friends' => $item,
            'title' => $title,
            'emptyMessage' => $emptyMessage,
            'type' => $type
        ));
    }

    public static function addFriend(Request $request) {
        $friend = Friends::where('fromid', Auth::id())->where('toid', $request->id_user)->count();


        if ($friend == 0) {
            Friends::create([
                'toid' => $request->id_user,
                'fromid' => Auth::id(),
                'status' => 0
            ]);
            return Response::json([
                "success" => "Заявка отправлена"
            ], 200);
        } else {
            return Response::json([
                "errors" => ["Вы уже отправили заявку, ожидайте подтверждения"]
            ], 422);
        }
    }

    public static function confirmFriend(Request $request) {
        $friend = Friends::where('toid', Auth::id())->where('fromid', $request->id_user)->get()[0];
        if ($friend['status'] == 0) {

            Friends::create([
                'toid' => $request->id_user,
                'fromid' => Auth::id(),
                'status' => 1
            ]);

            Friends::where('toid', Auth::id())
                ->where('fromid', $request->id_user)
                ->update(['status' => 1]);

            return Response::json([
                "success" => "Заявка подтверждена"
            ], 200);
        } else {

            return Response::json([
                "errors" => ["Ошибка"]
            ], 200);
        }
    }

    public static function deleteFriend(Request $request) {
        Friends::where('toid', Auth::id())->where('fromid', $request->id_user)->delete();
        Friends::where('toid', $request->id_user)->where('fromid', Auth::id())->delete();


        return Response::json([
            "success" => "Заявка удалена"
        ], 200);
    }
}
