<?php
namespace App\Http\Controllers;

use App\Libraries\Domain;
use App\Models\News;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class NewsController extends Controller {

    public static function all() {
        $domain = new Domain();

        $dirTemplates = $domain->dirTemplates();
        $news = News::all()->sortByDesc('id');

        return view('templates/'.$dirTemplates.'/news/news', array(
            'news' => $news
        ));
    }

    public static function view($id) {
        $news = News::where('id', $id)->get();

        $domain = new Domain();
        $dirTemplates = $domain->dirTemplates();

        return view('templates/'.$dirTemplates.'/news/oneNews', array(
            'news' => $news[0]
        ));
    }

    public static function addNews() {

        if (Auth::id() != 1) {

            return redirect()->route('profile');
        }

        $domain = new Domain();
        $dirTemplates = $domain->dirTemplates();

        return view('templates/'.$dirTemplates.'/news/newsAdd');
    }

    public static function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'text' => 'required'
        ], [
            'name.required' => 'Заполните Название',
            'text.lte' => 'Заполните содержимое новости'
        ]);


        $news = News::create([
            'prev' => $request->name,
            'text' => $request->text,
            'date' => now(),
            'color' => $request->color,
            'icon' => $request->icon,
            'color_icon' => $request->color_icon,
        ]);

        return Response::json([
            "success" => true,
            "message" => "Новость успешно создана",
            "link" => route('goToNews', ['id' => $news->id])
        ], 200);

    }

    public static function delete(Request $request) {
        if (News::destroy($request->id)) {
            return Response::json([
                "message" => "Новость удалена"
            ], 200);
        }

        return Response::json([
            "message" => "Не получилось удалить новость"
        ], 422);

    }
}
