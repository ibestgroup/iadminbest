<?php
namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Notice;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class NoticeController extends Controller {
    public static function all() {
        $notice = Notice::where('user_id', Auth::id())->orderByDesc('id')->paginate(50);
        $dateMain = '';



        return view('templates/notice', compact('notice', 'dateMain'));
    }

    public static function view($id) {
        $news = News::where('id', $id)->get();

        return view('templates/news/oneNews', array(
            'news' => $news[0]
        ));
    }

    public static function addNews() {

        if (Auth::id() == 1) {

            return redirect()->route('profile');
        }

        return view('templates/news/newsAdd');
    }

    public static function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'text' => 'required'
        ], [
            'name.required' => 'Заполните Название',
            'text.lte' => 'Заполните содержимое новости'
        ]);


        $news = News::create([
            'prev' => $request->name,
            'text' => $request->text,
            'date' => now()
        ]);

        return Response::json([
            "success" => true,
            "message" => "Новость успешно создана",
            "link" => route('goToNews', ['id' => $news->id])
        ], 200);

    }

    public static function delete(Request $request) {
        if (News::destroy($request->id)) {
            return Response::json([
                "message" => "Новость удалена"
            ], 200);
        }

        return Response::json([
            "message" => "Не получилось удалить новость"
        ], 422);

    }
}
