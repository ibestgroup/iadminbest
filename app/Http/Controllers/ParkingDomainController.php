<?php

namespace App\Http\Controllers;

use App\Libraries\Domain;
use App\Libraries\Helpers;
use App\Models\CoreParking;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class ParkingDomainController extends Controller
{



    public function create() {
        $idSite = Helpers::getSiteId();

        $parking = CoreParking::where('site_id', $idSite)->first();
        return view('templates/admin/parkingDomain', compact('parking'));

    }

    public function store(Request $request) {

        $idSite = Helpers::getSiteId();

        if ($request->domain == 'iadmin.best') {
            return Response::json([
                "type" => "error",
                "error" => 'Ошибка, вы не можете зарегистрировать этот домен'
            ], 200);
        }

        $mystring = $request->domain;
        $findme   = 'iadmin';
        $pos = strpos($mystring, $findme);

        if ($pos === false) {
        } else {
            return Response::json([
                "type" => "error",
                "error" => 'Ошибка, вы не можете зарегистрировать этот домен'
            ], 200);
        }

        if ($request->domain == 'iadmin.best') {
            return Response::json([
                "type" => "error",
                "error" => 'Ошибка, вы не можете зарегистрировать этот домен'
            ], 200);
        }

        if (CoreParking::where('site_id', $idSite)->count()) {
            return Response::json([
                "success" => false,
                "error" => 'У вас уже есть зарегистрированный домен'
            ], 200);
        }

        CoreParking::create([
            'site_id' => $idSite,
            'domain' => $request->domain,
            'delegate' => 0,
            'code' => rand(0000001, 9999999),
        ]);

        return Response::json([
            "message" => "Домен добавлен",
        ], 200);

    }

    public function delegate() {
        $idSite = Helpers::getSiteId();
        $parking = CoreParking::where('site_id', $idSite)->first();


        $code =  Curl::to('http://'.$parking['domain'].'/check-delegate')
            ->get();
        $codeSSL =  Curl::to('https://'.$parking['domain'].'/check-delegate')
            ->get();


        $code = json_decode($code);
        $codeSSL = json_decode($codeSSL);

        $code = (isset($code->code))?$code->code:'';
        $codeSSL = (isset($codeSSL->code))?$codeSSL->code:'';

        if ($code == $parking['code'] or $codeSSL == $parking['code']) {

            CoreParking::where('site_id', $idSite)->update(['delegate' => 1]);

            return Response::json([
                "message" => "Домен делегирован"
            ], 200);

        } else {

            return Response::json([
                "type" => "error",
                "message" => "Не получилось делегировать домен. Либо он еще не припарковался, либо вы не правильно ввели домен"
            ], 200);
        }


    }

    public function delete() {

        $idSite = Helpers::getSiteId();
        CoreParking::where('site_id', $idSite)->delete();

        return Response::json([
            "type" => "warning",
            "message" => "Домен удален"
        ], 200);

    }

    public function codeDelegate() {

        $domain = new Domain();

        $idSite = Helpers::getSiteId($domain->getDB());

        $parking = CoreParking::where('site_id', $idSite)->first();

        $code = json_encode(['code' => $parking['code']]);
        return $code;
//        return Response::json([
//            "code" => $parking['code']
//        ], 200);

    }

}
