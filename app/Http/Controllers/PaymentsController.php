<?php
namespace App\Http\Controllers;

use App\Models\Payments;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PaymentsController extends Controller {
    public static function all() {
        $payments = Payments::where('payment_user', Auth::id())->paginate(20);
        return view('templates/payments', array(
            'payments' => $payments
        ));
    }

    public static function incoming() {
        $payments = Payments::where('amount', '<', 150)->paginate(20);

        return view('templates/payments', array(
            'payments' => $payments
        ));
    }
}