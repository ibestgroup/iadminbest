<?php

namespace App\Http\Controllers;

use App\Libraries\Activate;
use App\Libraries\Domain;
use App\Models\CoreSiteList;
use App\Models\Notice;
use App\Models\OrdersMarkInside;
use App\Models\Payments;
use App\Models\PaymentsOrder;
use App\Models\Promo;
use App\Models\Roulette;
use App\Models\SiteElements;
use App\Models\SiteInfo;
use App\Models\StatisticVisit;
use App\Models\Templates;
use App\Models\Users;
use App\Models\UsersData;
use App\Models\Ad;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use App\Libraries\MD5Hasher;
use App\Libraries\Helpers;
use Illuminate\Support\Facades\Schema;

class ProfileBuilderController extends Controller
{

    public function store(Request $request)
    {
        $type = $request->type;
        $message = ($type == 'grid')?'Колонка добавлена':'Элемент добавлен';

        $optionValue = $request->input();
        unset($optionValue['_token']);

        $optionValue['widget'] = (isset($optionValue['widget']))?$optionValue['widget']:'';

        if ($optionValue['widget'] == 'transaction') {
            $optionValue['payment'] = 1;
            $optionValue['cashout'] = 1;
            $optionValue['payments_order'] = 1;

            $optionValue['rows'] = [
                0 => [
                    'head' => 'ID',
                    'value' => 'id',
                ],
                1 => [
                    'head' => 'От кого',
                    'value' => 'from_user',
                ],
                2 => [
                    'head' => 'Кому',
                    'value' => 'to_user',
                ],
                3 => [
                    'head' => 'Сумма',
                    'value' => 'amount',
                ],
                4 => [
                    'head' => 'Тип',
                    'value' => 'type',
                ],
                5 => [
                    'head' => 'Дата',
                    'value' => 'date',
                ],
            ];
        }

        if (in_array($request->grid, ['right', 'left', 'top'])) {
            $request->page = 'all';
        }

        $template = Templates::where('status', 1)->first()['id'];


        if (!in_array($type, ['grid', 'style'])) {
            $optionValue = Helpers::serialize_corrector(self::defaultTemplates($type, $optionValue['widget'], $optionValue['col'], $optionValue['head']));
        }

//        dd($optionValue);

        $optionValue = serialize($optionValue);


        $name = Helpers::translit($request->name_ru);

        $elementsCreate = SiteElements::create([
            'id_in_system' => 0,
            'type' => $type,
            'name' => $name,
            'name_ru' => $request->name_ru,
            'parent_id' => $request->grid,
            'option_value' => $optionValue,
            'page' => $request->page,
            'sort' => 0,
            'template' => $template,
        ]);

        SiteElements::where('id', $elementsCreate->id)->update(['id_in_system' => $elementsCreate->id]);

        return Response::json([
            "type" => "success",
            "message" => $message
        ], 200);

    }

    public function updateSort(Request $request) {
        SiteElements::where('id_in_system', $request->id)->where('template', Helpers::siteTemplate())->update([
           'parent_id' => $request->parent,
           'sort' => $request->num,
        ]);
    }

    public function updateElement(Request $request) {

        $optionValueOld = SiteElements::where('id_in_system', $request->block_id)->where('template', Helpers::siteTemplate())->first();
        $optionValueOld = unserialize($optionValueOld['option_value']);
        $optionValue = $request->input();

        $optionValue['payments_order'] = (isset($request->payments_order))?1:0;
        $optionValue['cashout'] = (isset($request->cashout))?1:0;
        $optionValue['payments_order'] = (isset($request->payments_order))?1:0;
        $optionValue['first_page'] = (isset($request->first_page))?1:0;

        if (isset($request->type)) {
            if ($request->type == 'main-style') {
                $optionValue['header']['status'] = (isset($request['header']['status']))?1:0;
                $optionValue['left_sidebar']['status'] = (isset($request['left_sidebar']['status']))?1:0;
                $optionValue['top_sidebar']['status'] = (isset($request['top_sidebar']['status']))?1:0;
                $optionValue['right_sidebar']['status'] = (isset($request['right_sidebar']['status']))?1:0;

                $optionValue['header']['width_full'] = (isset($request['header']['width_full']));
                $optionValue['header']['width'] = (isset($request['header']['width']))?$request['header']['width']:$optionValueOld['header']['width'];
                $optionValue['main_style']['width_full'] = (isset($request['main_style']['width_full']));
                $optionValue['main_style']['width'] = (isset($request['main_style']['width']))?$request['main_style']['width']:$optionValueOld['main_style']['width'];
            }
        }

        if (isset($request->font_head)) {
            $optionValue['font_head']['weight'] = (isset($request['font_head']['weight']))?1:0;
            $optionValue['font_head']['style'] = (isset($request['font_head']['style']))?1:0;
        }

        $optionValue['active'] = (isset($request['active']))?1:0;
        $optionValue['not_active'] = (isset($request['not_active']))?1:0;
        $optionValue['random_referrals'] = (isset($request['random_referrals']))?1:0;
        $optionValue['top_btn'] = (isset($request['top_btn']))?1:0;
        $optionValue['mark_id_view'] = (isset($request['mark_id_view']))?1:0;
        $optionValue['level_up_view'] = (isset($request['level_up_view']))?1:0;
        $optionValue['next_overflow'] = (isset($request['next_overflow']))?1:0;


        unset($optionValue['_token']);

        if (!is_array($optionValueOld)) {
            $optionValueOld = [];
        }

//        dd(array_merge($optionValueOld, $optionValue));
        if (isset($optionValue['content'])) {
            $contentLength = strlen($optionValue['content']);
            if ($contentLength > 25000) {
                return Response::json([
                    "type" => "error",
                    "message" => "Больше 25000 символов. У вас $contentLength",
                    "block" => $request->block_id,
                ], 200);
            }
        }

        $optionValue = serialize(array_merge($optionValueOld, $optionValue));


        SiteElements::where('id_in_system', $request->block_id)->where('template', Helpers::siteTemplate())
            ->update([
                'option_value' => $optionValue,
            ]);


        $message = ($request->type == 'main-style')?__('main.design_changed'):__('main.item_changed');

        return Response::json([
            "type" => "success",
            "message" => $message,
            "block" => $request->block_id,
        ], 200);
    }

    public static function selectValue() {

    }

    public function infoElement($id) {
        $element = SiteElements::where('id', $id)->first();
        $element['option_value'] = unserialize($element['option_value']);

        return json_encode($element);
    }

    public function siteStyle() {


        $optionValue = [
            'top_sidebar' => [
                'status' => 1,
                'style' => [
                    'height' => '70',
                    'bg_color' => '#aaaaaa',
                    'bg_img' => ''
                ],
            ],
            'left_sidebar' => [
                'status' => 1,
                'style' => [
                    'width' => '200',
                    'bg_color' => '#aaaaaa',
                    'bg_img' => ''
                ],
            ],
            'right_sidebar' => [
                'status' => 1,
                'style' => [
                    'width' => '200',
                    'bg_color' => '#aaaaaa',
                    'bg_img' => ''
                ],
            ],
            'header' => [
                'status' => 1,
                'width_full' => 0,
                'width' => 1000,
                'height' => 'auto',
                'img' => '',
                'border' => [
                    'width' => '',
                    'style' => '',
                    'color' => '',
                ],
                'border_radius' => [
                    'tl' => '',
                    'tr' => '',
                    'br' => '',
                    'bl' => '',
                ],
            ],
            'main_style' => [
                'width_full' => 1,
                'width' => 1000,
                'bg_color_body' => '#eeeeee',
                'bg_img_body' => '',
                'bg_color_content' => '#eeeeee',
                'bg_img_content' => '',
                'margin_top' => '',
                'border' => [
                    'width' => '',
                    'style' => '',
                    'color' => '',
                ],
                'border_radius' => [
                    'tl' => '',
                    'tr' => '',
                    'br' => '',
                    'bl' => '',
                ],
            ]
        ];

        $optionValue = serialize($optionValue);

        SiteElements::where('name', 'main-style')->update(['option_value' => $optionValue]);

    }

    public static function defaultTemplate() {
        $template = 'a:22:{s:11:"top_sidebar";a:2:{s:6:"status";i:1;s:5:"style";a:3:{s:8:"bg_color";s:7:"#ff8d00";s:6:"height";s:2:"70";s:6:"bg_img";N;}}s:12:"left_sidebar";a:2:{s:6:"status";i:1;s:5:"style";a:3:{s:8:"bg_color";s:7:"#222a33";s:5:"width";s:3:"220";s:6:"bg_img";N;}}s:13:"right_sidebar";a:2:{s:5:"style";a:3:{s:8:"bg_color";s:7:"#ffffff";s:5:"width";s:3:"300";s:6:"bg_img";N;}s:6:"status";i:0;}s:6:"header";a:7:{s:6:"status";i:1;s:5:"width";s:4:"1000";s:6:"height";s:1:"0";s:3:"img";N;s:6:"border";a:3:{s:5:"width";s:1:"0";s:5:"color";s:7:"#000000";s:5:"style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:10:"width_full";b:0;}s:10:"main_style";a:9:{s:5:"width";s:4:"1200";s:13:"bg_color_body";s:7:"#e8e7ca";s:11:"bg_img_body";N;s:16:"bg_color_content";s:7:"#dbceaf";s:14:"bg_img_content";N;s:6:"border";a:3:{s:5:"width";s:1:"0";s:5:"color";s:7:"#000000";s:5:"style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:10:"margin_top";s:2:"50";s:10:"width_full";b:0;}s:4:"type";s:10:"main-style";s:8:"block_id";s:1:"1";s:4:"page";s:7:"profile";s:4:"head";s:23:"Мои операции";s:9:"box_style";a:6:{s:15:"text_align_head";s:6:"center";s:2:"bg";s:7:"#ffffff";s:7:"bg_head";s:7:"#ffffff";s:10:"color_head";s:7:"#000000";s:6:"border";a:3:{s:12:"border_width";s:1:"2";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}}s:3:"col";s:2:"12";s:16:"last_transaction";s:1:"1";s:10:"first_page";i:0;s:14:"payments_order";i:0;s:7:"cashout";i:0;s:7:"payment";s:1:"1";s:4:"rows";a:6:{i:0;a:2:{s:4:"head";s:2:"ID";s:5:"value";s:14:"id_transaction";}i:1;a:2:{s:4:"head";s:10:"Сумма";s:5:"value";s:6:"amount";}i:2;a:2:{s:4:"head";s:6:"Тип";s:5:"value";s:4:"type";}i:3;a:2:{s:4:"head";s:8:"Дата";s:5:"value";s:4:"date";}i:4;a:2:{s:4:"head";s:8:"Кому";s:5:"value";s:7:"to_user";}i:5;a:2:{s:4:"head";s:13:"От кого";s:5:"value";s:9:"from_user";}}s:12:"widget_style";a:10:{s:11:"border_line";a:3:{s:12:"border_width";s:1:"1";s:12:"border_color";s:7:"#ff9f00";s:12:"border_style";s:5:"solid";}s:16:"text_align_table";s:6:"center";s:11:"bg_color_th";s:7:"#ffffff";s:8:"color_th";s:7:"#000000";s:13:"bg_color_from";s:7:"#bbbbbb";s:10:"color_from";s:7:"#ffffff";s:11:"bg_color_to";s:7:"#ffffff";s:8:"color_to";s:7:"#bbbbbb";s:13:"bg_color_td_h";s:7:"#000000";s:10:"color_td_h";s:7:"#ffffff";}s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;}';

        $template = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $template);
        $template = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $template);

        $arrTemplate = unserialize($template);

        return $template;
    }

    public static function defaultWidget($box, $widget) {

    }

    public static function defaultBox($box) {
        $boxes = [
            'mainbox' => 'a:8:{s:4:"page";s:7:"profile";s:7:"name_ru";s:5:"12213";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:4:"head";s:21:"Мой профиль";s:8:"block_id";s:3:"178";s:9:"box_style";a:10:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:8:"bg_color";s:7:"#222a33";s:20:"bg_color_transparent";s:1:"1";s:13:"bg_color_head";s:7:"#ff8d00";s:25:"bg_color_head_transparent";s:1:"1";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:4:"grid";s:1:"6";}',
        ];



        return unserialize($boxes[$box]);

    }

    public static function defaultTemplates($box, $widget, $col = 12, $head = 'Название блока') {

        $widgets = [
            'balance' => 'a:12:{s:4:"page";s:7:"profile";s:7:"name_ru";s:6:"123123";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:7:"balance";s:4:"head";s:31:"Пополнить баланс";s:8:"block_id";s:2:"69";s:6:"amount";N;s:9:"box_style";a:8:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:7:"PT Sans";s:4:"size";s:2:"20";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#0f181a";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:9:"label_sum";s:25:"Введите сумму";s:12:"widget_style";a:15:{s:16:"text_align_label";s:4:"left";s:10:"font_label";a:3:{s:6:"family";s:7:"PT Sans";s:4:"size";s:2:"15";s:6:"weight";s:1:"1";}s:16:"label_color_text";s:7:"#fffef9";s:14:"bg_color_input";s:7:"#e8e7ca";s:11:"color_input";s:7:"#8e8d7f";s:12:"border_input";a:3:{s:12:"border_width";s:1:"1";s:12:"border_color";s:7:"#e8e7ca";s:12:"border_style";s:5:"solid";}s:19:"border_radius_input";a:4:{s:2:"tl";s:1:"3";s:2:"tr";s:2:"50";s:2:"bl";s:1:"3";s:2:"br";s:2:"50";}s:12:"bg_color_btn";s:7:"#ff8d00";s:9:"color_btn";s:7:"#ffffff";s:17:"border_radius_btn";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"5";s:2:"br";s:1:"5";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#db7a00";s:11:"color_btn_h";s:7:"#ffffff";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:4:"link";a:1:{s:4:"text";s:31:"Пополнить баланс";}}',
            'profileedit' => 'a:23:{s:4:"page";s:7:"profile";s:7:"name_ru";s:6:"wqeeqw";s:4:"grid";s:1:"2";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:11:"profileedit";s:4:"head";s:41:"Редактировать профиль";s:8:"block_id";s:3:"154";s:9:"box_style";a:8:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"20";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:5:"input";a:3:{i:1;a:3:{s:4:"name";s:12:"Аватар";s:5:"value";s:6:"avatar";s:4:"icon";s:4:"none";}i:2;a:3:{s:4:"name";s:6:"Имя";s:5:"value";s:4:"name";s:4:"icon";s:10:"fa fa-male";}i:4;a:3:{s:4:"name";s:2:"VK";s:5:"value";s:2:"vk";s:4:"icon";s:11:"fa fa-users";}}s:12:"widget_style";a:19:{s:12:"width_avatar";s:3:"200";s:12:"color_avatar";s:7:"#000000";s:13:"border_avatar";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#ff8d00";s:12:"border_style";s:6:"outset";}s:20:"border_radius_avatar";a:4:{s:2:"tl";s:3:"100";s:2:"tr";s:3:"100";s:2:"bl";s:3:"100";s:2:"br";s:1:"0";}s:16:"label_color_text";s:7:"#fafefb";s:21:"label_text_align_text";s:4:"left";s:15:"label_font_text";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"20";}s:14:"bg_color_input";s:7:"#dbceaf";s:11:"color_input";s:7:"#878171";s:12:"border_input";a:3:{s:12:"border_width";s:1:"1";s:12:"border_color";s:7:"#878171";s:12:"border_style";s:5:"solid";}s:19:"border_radius_input";a:4:{s:2:"tl";s:3:"100";s:2:"tr";s:3:"100";s:2:"bl";s:3:"100";s:2:"br";s:3:"100";}s:12:"bg_color_btn";s:7:"#ff8d00";s:9:"color_btn";s:7:"#ffffff";s:17:"border_radius_btn";a:4:{s:2:"tl";s:1:"7";s:2:"tr";s:1:"7";s:2:"bl";s:1:"7";s:2:"br";s:1:"7";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#dc7a00";s:11:"color_btn_h";s:7:"#ebebea";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:1:"7";s:2:"tr";s:1:"7";s:2:"bl";s:1:"7";s:2:"br";s:1:"7";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#eff0ef";s:12:"border_style";s:5:"solid";}}s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;s:4:"name";s:12:"Виктор";s:8:"password";N;s:21:"password_confirmation";N;s:4:"link";a:1:{s:4:"text";s:18:"Пополнить";}s:2:"vk";N;}',
            'tarifs' => 'a:25:{s:4:"page";s:7:"profile";s:7:"name_ru";s:19:"Мои тарифы";s:4:"grid";s:1:"2";s:4:"type";s:10:"main-style";s:3:"col";s:2:"12";s:6:"widget";s:6:"tarifs";s:4:"head";s:19:"Мои тарифы";s:8:"block_id";s:3:"132";s:9:"box_style";a:8:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"17";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:11:"last_tarifs";s:1:"5";s:7:"top_btn";i:0;s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:4:"list";a:3:{i:1;a:3:{s:4:"name";s:29:"Название тарифа";s:5:"value";s:9:"mark_name";s:4:"icon";s:10:"fa fa-leaf";}i:2;a:3:{s:4:"name";s:18:"Подробнее";s:5:"value";s:9:"mark_more";s:4:"icon";s:11:"fa fa-gears";}i:3;a:3:{s:4:"name";s:12:"Резерв";s:5:"value";s:12:"mark_balance";s:4:"icon";s:4:"none";}}s:12:"widget_style";a:64:{s:11:"border_line";a:3:{s:12:"border_width";s:1:"4";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:10:"text_align";s:6:"center";s:4:"font";a:3:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"15";s:6:"weight";s:1:"1";}s:8:"color_h3";s:7:"#ffffff";s:13:"text_align_h3";s:6:"center";s:7:"font_h3";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"25";}s:11:"bg_color_th";s:7:"#dbceaf";s:8:"color_th";s:7:"#878171";s:11:"bg_color_tr";s:7:"#dbceaf";s:8:"color_tr";s:7:"#8e8d7f";s:13:"bg_color_tr_h";s:7:"#ff8d00";s:10:"color_tr_h";s:7:"#ffffff";s:16:"bg_color_btn_top";s:7:"#ff8d00";s:13:"color_btn_top";s:7:"#ffffff";s:21:"border_radius_btn_top";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:14:"border_btn_top";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:18:"bg_color_btn_h_top";s:7:"#ffffff";s:15:"color_btn_h_top";s:7:"#000000";s:23:"border_radius_btn_h_top";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:16:"border_btn_h_top";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:17:"bg_color_btn_more";s:7:"#ff8d00";s:14:"color_btn_more";s:7:"#ffffff";s:22:"border_radius_btn_more";a:4:{s:2:"tl";s:2:"50";s:2:"tr";s:2:"50";s:2:"bl";s:2:"50";s:2:"br";s:2:"50";}s:15:"border_btn_more";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:19:"bg_color_btn_h_more";s:7:"#ffffff";s:16:"color_btn_h_more";s:7:"#000000";s:24:"border_radius_btn_h_more";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"5";s:2:"br";s:1:"5";}s:17:"border_btn_h_more";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:12:"bg_color_btn";s:7:"#ff8d00";s:9:"color_btn";s:7:"#ffffff";s:17:"border_radius_btn";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#d37500";s:11:"color_btn_h";s:7:"#e8e8e8";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_a";s:7:"#d17400";s:11:"color_btn_a";s:7:"#e1e1e1";s:19:"border_radius_btn_a";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:12:"border_btn_a";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:10:"border_img";a:3:{s:12:"border_width";s:1:"3";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:17:"border_radius_img";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:9:"color_img";s:7:"#ffffff";s:14:"text_align_img";s:6:"center";s:8:"font_img";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"19";}s:15:"border_img_free";a:3:{s:12:"border_width";s:1:"3";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:22:"border_radius_img_free";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:14:"color_img_free";s:7:"#ffffff";s:19:"text_align_img_free";s:6:"center";s:13:"font_img_free";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"20";}s:13:"color_h3_tree";s:7:"#c4c4c4";s:18:"text_align_h3_tree";s:4:"left";s:12:"font_h3_tree";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"22";}s:16:"border_line_tree";a:3:{s:12:"border_width";s:1:"2";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:21:"text_align_table_tree";s:4:"left";s:15:"font_table_tree";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"19";}s:16:"bg_color_th_tree";s:7:"#ffffff";s:13:"color_th_tree";s:7:"#ababab";s:18:"bg_color_from_tree";s:7:"#cb4109";s:15:"color_from_tree";s:7:"#000000";s:11:"bg_color_to";s:7:"#32d91d";s:8:"color_to";s:7:"#000000";s:18:"bg_color_td_h_tree";s:7:"#e60be2";s:15:"color_td_h_tree";s:7:"#000000";}s:4:"rows";a:4:{i:5;a:2:{s:4:"head";s:2:"ID";s:5:"value";s:14:"id_transaction";}i:6;a:2:{s:4:"head";s:8:"Кому";s:5:"value";s:7:"to_user";}i:7;a:2:{s:4:"head";s:13:"От кого";s:5:"value";s:9:"from_user";}i:8;a:2:{s:4:"head";s:10:"Сумма";s:5:"value";s:6:"amount";}}s:6:"header";a:7:{s:6:"status";i:1;s:5:"width";s:4:"1000";s:6:"height";s:1:"0";s:3:"img";N;s:6:"border";a:3:{s:5:"width";s:1:"0";s:5:"color";s:7:"#000000";s:5:"style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:10:"width_full";b:0;}s:11:"top_sidebar";a:2:{s:6:"status";i:1;s:5:"style";a:3:{s:8:"bg_color";s:7:"#ff8d00";s:6:"height";s:2:"70";s:6:"bg_img";N;}}s:12:"left_sidebar";a:2:{s:6:"status";i:1;s:5:"style";a:3:{s:8:"bg_color";s:7:"#222a33";s:5:"width";s:3:"220";s:6:"bg_img";N;}}s:13:"right_sidebar";a:2:{s:6:"status";i:1;s:5:"style";a:3:{s:8:"bg_color";s:7:"#ffffff";s:5:"width";s:3:"300";s:6:"bg_img";N;}}s:10:"main_style";a:9:{s:5:"width";s:4:"1200";s:13:"bg_color_body";s:7:"#e8e7ca";s:11:"bg_img_body";N;s:16:"bg_color_content";s:7:"#dbceaf";s:14:"bg_img_content";N;s:6:"border";a:3:{s:5:"width";s:1:"0";s:5:"color";s:7:"#000000";s:5:"style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:10:"margin_top";s:2:"50";s:10:"width_full";b:0;}}',
            'news' => 'a:10:{s:7:"name_ru";s:6:"йцу";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:4:"news";s:4:"head";s:14:"Новости";s:8:"block_id";s:2:"64";s:9:"box_style";a:8:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"20";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:12:"widget_style";a:26:{s:13:"bg_color_head";s:7:"#dbceaf";s:10:"color_head";s:7:"#878171";s:10:"color_date";s:7:"#9e9e9e";s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:1:"0";}s:11:"border_head";a:3:{s:12:"border_width";s:1:"1";s:12:"border_color";s:7:"#878171";s:12:"border_style";s:5:"solid";}s:22:"bg_color_timeline_date";s:7:"#ff8d00";s:22:"bg_color_timeline_line";s:7:"#ff8d00";s:14:"color_timeline";s:7:"#ffffff";s:22:"border_radius_timeline";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:27:"border_radius_timeline_icon";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"5";s:2:"br";s:1:"5";}s:13:"bg_color_text";s:7:"#e8e7ca";s:10:"color_text";s:7:"#8e8d7f";s:15:"text_align_text";s:4:"left";s:9:"font_text";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:1:"0";}s:15:"bg_color_footer";s:7:"#dbceaf";s:17:"text_align_footer";s:5:"right";s:11:"font_footer";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:1:"0";}s:12:"bg_color_btn";s:7:"#ff8d00";s:9:"color_btn";s:7:"#ffffff";s:17:"border_radius_btn";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"5";s:2:"br";s:1:"5";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#cc7100";s:11:"color_btn_h";s:7:"#ffffff";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:9:"last_news";s:1:"5";s:4:"page";s:7:"profile";}',
            'profilebox' => 'a:22:{s:4:"page";s:7:"profile";s:7:"name_ru";s:5:"12213";s:4:"grid";s:1:"6";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:10:"profilebox";s:4:"head";s:21:"Мой профиль";s:8:"block_id";s:3:"133";s:9:"box_style";a:8:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:5:"image";s:6:"avatar";s:4:"text";s:5:"login";s:4:"icon";s:10:"fa fa-user";s:4:"link";a:2:{s:4:"text";N;s:4:"link";N;}s:12:"widget_style";a:16:{s:9:"width_img";s:3:"150";s:11:"color_login";s:7:"#ff8d00";s:13:"border_radius";a:4:{s:2:"tl";s:3:"200";s:2:"tr";s:3:"200";s:2:"bl";s:3:"200";s:2:"br";s:3:"200";}s:10:"border_img";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#ff8d00";s:12:"border_style";s:5:"solid";}s:17:"bg_color_elements";s:7:"#dbceaf";s:8:"color_tl";s:7:"#222a33";s:8:"color_tr";s:7:"#878171";s:11:"border_line";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:12:"bg_color_btn";s:7:"#ff8d00";s:9:"color_btn";s:7:"#ffffff";s:17:"border_radius_btn";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#e07d00";s:11:"color_btn_h";s:7:"#eaeaea";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"5";s:2:"br";s:1:"5";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;s:4:"list";a:4:{i:1;a:3:{s:4:"name";s:12:"Баланс";s:5:"value";s:7:"balance";s:4:"icon";s:9:"fa fa-rub";}i:2;a:3:{s:4:"name";s:14:"Куратор";s:5:"value";s:12:"login_parent";s:4:"icon";s:10:"fa fa-male";}i:3;a:3:{s:4:"name";s:39:"Количество рефералов";s:5:"value";s:9:"count_ref";s:4:"icon";s:11:"fa fa-users";}i:4;a:3:{s:4:"name";s:34:"Переходы по ссылке";s:5:"value";s:10:"link_visit";s:4:"icon";s:15:"fa fa-user-plus";}}}',
            'transaction' => 'a:21:{s:7:"name_ru";s:8:"sasadasd";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:11:"transaction";s:4:"head";s:23:"Мои операции";s:7:"payment";s:1:"1";s:7:"cashout";i:1;s:14:"payments_order";i:1;s:4:"rows";a:5:{i:1;a:2:{s:4:"head";s:2:"ID";s:5:"value";s:14:"id_transaction";}i:2;a:2:{s:4:"head";s:10:"Сумма";s:5:"value";s:6:"amount";}i:3;a:2:{s:4:"head";s:13:"От кого";s:5:"value";s:9:"from_user";}i:4;a:2:{s:4:"head";s:8:"Кому";s:5:"value";s:7:"to_user";}i:6;a:2:{s:4:"head";s:8:"Дата";s:5:"value";s:4:"date";}}s:8:"block_id";s:2:"61";s:9:"box_style";a:8:{s:15:"text_align_head";s:6:"center";s:9:"font_head";a:2:{s:6:"family";s:7:"PT Sans";s:4:"size";s:2:"20";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#000000";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:16:"last_transaction";s:1:"2";s:12:"widget_style";a:23:{s:11:"border_line";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ff9f00";s:12:"border_style";s:5:"solid";}s:16:"text_align_table";s:6:"center";s:10:"font_table";a:2:{s:6:"family";s:7:"PT Sans";s:4:"size";s:2:"15";}s:11:"bg_color_th";s:7:"#dbceaf";s:8:"color_th";s:7:"#000000";s:13:"bg_color_from";s:7:"#e8e7ca";s:10:"color_from";s:7:"#dd3a5c";s:11:"bg_color_to";s:7:"#e8e7ca";s:8:"color_to";s:7:"#43cf83";s:13:"bg_color_td_h";s:7:"#dbceaf";s:10:"color_td_h";s:7:"#878171";s:12:"bg_color_btn";s:7:"#ff8d00";s:9:"color_btn";s:7:"#ffffff";s:17:"border_radius_btn";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#dbceaf";s:11:"color_btn_h";s:7:"#878171";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_a";s:7:"#dbceaf";s:11:"color_btn_a";s:7:"#878171";s:19:"border_radius_btn_a";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:12:"border_btn_a";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}}s:10:"first_page";i:0;s:4:"page";s:7:"profile";s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;s:11:"last_tarifs";s:1:"2";s:4:"list";a:3:{i:1;a:3:{s:4:"name";s:29:"Название тарифа";s:5:"value";s:9:"mark_name";s:4:"icon";s:10:"fa fa-leaf";}i:2;a:3:{s:4:"name";s:18:"Подробнее";s:5:"value";s:9:"mark_more";s:4:"icon";s:11:"fa fa-gears";}i:3;a:3:{s:4:"name";s:12:"Резерв";s:5:"value";s:12:"mark_balance";s:4:"icon";s:4:"none";}}}',
            'referrals' => 'a:19:{s:4:"page";s:7:"profile";s:7:"name_ru";s:18:"Рефераллы";s:4:"grid";s:1:"2";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:9:"referrals";s:4:"head";s:16:"Рефералы";s:8:"block_id";s:3:"122";s:9:"box_style";a:8:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:14:"last_referrals";s:2:"10";s:12:"widget_style";a:23:{s:11:"border_line";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:16:"text_align_table";s:6:"center";s:10:"font_table";a:2:{s:6:"family";s:7:"PT Sans";s:4:"size";s:2:"15";}s:11:"bg_color_th";s:7:"#e8e7ca";s:8:"color_th";s:7:"#878171";s:17:"bg_color_activate";s:7:"#e8e7ca";s:14:"color_activate";s:7:"#ff8d00";s:21:"bg_color_not_activate";s:7:"#e8e7ca";s:18:"color_not_activate";s:7:"#8e8d7f";s:13:"bg_color_td_h";s:7:"#222a33";s:10:"color_td_h";s:7:"#919499";s:12:"bg_color_btn";s:7:"#ff8d00";s:9:"color_btn";s:7:"#ffffff";s:17:"border_radius_btn";a:4:{s:2:"tl";s:1:"7";s:2:"tr";s:1:"7";s:2:"bl";s:1:"7";s:2:"br";s:1:"7";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"3";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#db7a00";s:11:"color_btn_h";s:7:"#f0f0f0";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"3";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_a";s:7:"#d37500";s:11:"color_btn_a";s:7:"#e7e7e7";s:19:"border_radius_btn_a";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:12:"border_btn_a";a:3:{s:12:"border_width";s:1:"3";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}}s:6:"active";i:1;s:10:"not_active";i:1;s:10:"first_page";i:0;s:14:"payments_order";i:0;s:7:"cashout";i:0;s:16:"random_referrals";i:0;s:4:"list";a:4:{i:1;a:3:{s:4:"name";s:10:"Логин";s:5:"value";s:5:"login";s:4:"icon";s:10:"fa fa-male";}i:2;a:3:{s:4:"name";s:34:"Переходы по ссылке";s:5:"value";s:10:"link_visit";s:4:"icon";s:13:"fa fa-sliders";}i:3;a:3:{s:4:"name";s:39:"Количество рефералов";s:5:"value";s:9:"count_ref";s:4:"icon";s:11:"fa fa-users";}i:4;a:3:{s:4:"name";s:20:"Заработано";s:5:"value";s:6:"profit";s:4:"icon";s:11:"fa fa-money";}}s:7:"top_btn";i:0;}',
            'faq' => 'a:9:{s:7:"name_ru";s:8:"чаво";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:3:"faq";s:4:"head";s:46:"Часто задаваемые вопросы";s:8:"block_id";s:2:"67";s:9:"box_style";a:7:{s:15:"text_align_head";s:4:"left";s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#0f181a";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"5";s:2:"br";s:1:"5";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:12:"widget_style";a:9:{s:13:"bg_color_head";s:7:"#dbceaf";s:10:"color_head";s:7:"#878171";s:15:"text_align_head";s:4:"left";s:11:"border_head";a:3:{s:12:"border_width";s:1:"3";s:12:"border_color";s:7:"#878171";s:12:"border_style";s:5:"solid";}s:13:"bg_color_text";s:7:"#e8e7ca";s:10:"color_text";s:7:"#8e8d7f";s:15:"text_align_text";s:4:"left";s:11:"border_text";a:3:{s:12:"border_width";s:1:"1";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:18:"border_radius_body";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}}s:4:"page";s:7:"profile";}',
            'promo' => 'a:17:{s:4:"page";s:7:"profile";s:7:"name_ru";s:3:"ewq";s:4:"grid";s:1:"2";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:5:"promo";s:4:"head";s:28:"Промоматериалы";s:8:"block_id";s:3:"156";s:9:"box_style";a:8:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:12:"widget_style";a:27:{s:10:"border_img";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#ff8d00";s:12:"border_style";s:5:"solid";}s:13:"bg_color_head";s:7:"#dbceaf";s:10:"color_head";s:7:"#878171";s:15:"text_align_head";s:6:"center";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"20";}s:19:"border_radius_promo";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:14:"bg_color_promo";s:7:"#e8e7ca";s:12:"border_promo";a:3:{s:12:"border_width";s:1:"2";s:12:"border_color";s:7:"#dbceaf";s:12:"border_style";s:5:"solid";}s:16:"bg_color_reflink";s:7:"#222a33";s:13:"color_reflink";s:7:"#909397";s:18:"text_align_reflink";s:6:"center";s:12:"font_reflink";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"20";}s:15:"padding_reflink";a:4:{s:1:"t";s:2:"25";s:1:"b";s:2:"25";s:1:"l";s:1:"0";s:1:"r";s:1:"0";}s:21:"border_radius_reflink";a:4:{s:2:"tl";s:2:"20";s:2:"tr";s:2:"20";s:2:"bl";s:2:"20";s:2:"br";s:2:"20";}s:14:"border_reflink";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#919499";s:12:"border_style";s:5:"solid";}s:14:"bg_color_input";s:7:"#dbceaf";s:11:"color_input";s:7:"#878171";s:12:"border_input";a:3:{s:12:"border_width";s:1:"1";s:12:"border_color";s:7:"#878171";s:12:"border_style";s:5:"solid";}s:19:"border_radius_input";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:12:"bg_color_btn";s:7:"#000000";s:9:"color_btn";s:7:"#000000";s:17:"border_radius_btn";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:10:"border_btn";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#000000";s:11:"color_btn_h";s:7:"#000000";s:19:"border_radius_btn_h";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:12:"border_btn_h";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;}',
            'cashout' => 'a:22:{s:4:"page";s:7:"profile";s:7:"name_ru";s:10:"Вывод";s:4:"grid";s:1:"2";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:7:"cashout";s:4:"head";s:23:"Форма вывода";s:8:"block_id";s:3:"158";s:6:"system";s:2:"45";s:3:"sum";N;s:7:"address";N;s:9:"box_style";a:8:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"20";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:9:"label_sum";N;s:12:"widget_style";a:15:{s:16:"text_align_label";s:4:"left";s:10:"font_label";a:2:{s:6:"family";s:7:"PT Sans";s:4:"size";s:2:"17";}s:16:"label_color_text";s:7:"#ffffff";s:14:"bg_color_input";s:7:"#dbceaf";s:11:"color_input";s:7:"#878171";s:12:"border_input";a:3:{s:12:"border_width";s:1:"1";s:12:"border_color";s:7:"#878171";s:12:"border_style";s:5:"solid";}s:19:"border_radius_input";a:4:{s:2:"tl";s:2:"20";s:2:"tr";s:2:"20";s:2:"bl";s:2:"20";s:2:"br";s:2:"20";}s:12:"bg_color_btn";s:7:"#ff8d00";s:9:"color_btn";s:7:"#ffffff";s:17:"border_radius_btn";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#dd7b00";s:11:"color_btn_h";s:7:"#ededed";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:4:"link";a:1:{s:4:"text";s:14:"Вывести";}s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;}',
            'activate' => 'a:18:{s:4:"page";s:7:"profile";s:7:"name_ru";s:33:"Активация тарифов";s:4:"grid";s:1:"2";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:8:"activate";s:4:"head";s:35:"Активировать тариф";s:8:"block_id";s:3:"157";s:9:"box_style";a:8:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:11:"width_tarif";s:1:"4";s:12:"widget_style";a:11:{s:13:"padding_tarif";a:4:{s:1:"t";s:2:"25";s:1:"b";s:2:"25";s:1:"l";s:2:"15";s:1:"r";s:2:"15";}s:16:"text_align_tarif";s:6:"center";s:10:"font_tarif";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:18:"bg_color_btn_tarif";s:7:"#ff8d00";s:15:"color_btn_tarif";s:7:"#ffffff";s:23:"border_radius_btn_tarif";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:16:"border_btn_tarif";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:20:"bg_color_btn_h_tarif";s:7:"#e17d00";s:17:"color_btn_h_tarif";s:7:"#e8e8e8";s:25:"border_radius_btn_h_tarif";a:4:{s:2:"tl";s:2:"40";s:2:"tr";s:2:"40";s:2:"bl";s:2:"40";s:2:"br";s:1:"5";}s:18:"border_btn_h_tarif";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;}',
            'content' => 'a:18:{s:4:"page";s:7:"profile";s:7:"name_ru";s:33:"Активация тарифов";s:4:"grid";s:1:"2";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:7:"content";s:4:"head";s:35:"Активировать тариф";s:8:"block_id";s:3:"157";s:9:"box_style";a:8:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:11:"width_tarif";s:1:"4";s:12:"widget_style";a:11:{s:13:"padding_tarif";a:4:{s:1:"t";s:2:"25";s:1:"b";s:2:"25";s:1:"l";s:2:"15";s:1:"r";s:2:"15";}s:16:"text_align_tarif";s:6:"center";s:10:"font_tarif";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:18:"bg_color_btn_tarif";s:7:"#ff8d00";s:15:"color_btn_tarif";s:7:"#ffffff";s:23:"border_radius_btn_tarif";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:16:"border_btn_tarif";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:20:"bg_color_btn_h_tarif";s:7:"#e17d00";s:17:"color_btn_h_tarif";s:7:"#e8e8e8";s:25:"border_radius_btn_h_tarif";a:4:{s:2:"tl";s:2:"40";s:2:"tr";s:2:"40";s:2:"bl";s:2:"40";s:2:"br";s:1:"5";}s:18:"border_btn_h_tarif";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;}',
            'verticalmenu' => 'a:16:{s:4:"name";s:11:"top_sidebar";s:6:"widget";s:12:"verticalmenu";s:12:"widget_style";a:13:{s:5:"float";s:5:"right";s:12:"bg_color_btn";s:7:"#ff8d00";s:24:"bg_color_btn_transparent";N;s:9:"color_btn";s:7:"#ffffff";s:17:"border_radius_btn";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"1";s:12:"border_color";s:7:"#ec8300";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#ec8301";s:26:"bg_color_btn_h_transparent";N;s:11:"color_btn_h";s:7:"#fffdfe";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:2:"10";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"1";s:12:"border_color";s:7:"#ec8301";s:12:"border_style";s:5:"solid";}s:14:"color_icon_btn";s:7:"#fdfffb";s:16:"color_icon_btn_h";s:7:"#fffefc";}s:5:"links";a:1:{i:1;a:3:{s:4:"text";s:14:"Профиль";s:4:"link";s:8:"/profile";s:4:"icon";s:10:"fa fa-star";}}s:4:"page";s:7:"profile";s:8:"block_id";s:1:"7";s:9:"box_style";a:2:{s:6:"margin";a:2:{s:3:"top";s:2:"30";s:6:"bottom";s:2:"10";}s:5:"float";s:4:"left";}s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;s:3:"col";s:2:"12";s:4:"head";s:19:"Левое меню";}',
            'horizontalmenu' => 'a:13:{s:4:"name";s:11:"top_sidebar";s:6:"widget";s:14:"horizontalmenu";s:12:"widget_style";a:10:{s:12:"bg_color_btn";s:7:"#222a33";s:9:"color_btn";s:7:"#ff8d00";s:17:"border_radius_btn";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:6:"dotted";}s:14:"bg_color_btn_h";s:7:"#dc7a00";s:11:"color_btn_h";s:7:"#fffeff";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ff911b";s:12:"border_style";s:5:"solid";}s:14:"color_icon_btn";s:7:"#ff8d00";s:16:"color_icon_btn_h";s:7:"#ffffff";}s:5:"links";a:2:{i:0;a:3:{s:4:"text";s:14:"Главная";s:4:"link";s:8:"/profile";s:4:"icon";s:11:"fa fa-users";}i:1;a:3:{s:4:"text";s:14:"Профиль";s:4:"link";s:8:"/profile";s:4:"icon";s:16:"fa fa-tachometer";}}s:4:"page";s:7:"profile";s:8:"block_id";s:3:"110";s:9:"box_style";a:2:{s:6:"margin";a:2:{s:3:"top";s:1:"0";s:6:"bottom";s:1:"0";}s:5:"float";s:5:"right";}s:4:"type";s:10:"main-style";s:6:"header";a:7:{s:6:"status";i:1;s:5:"width";s:4:"1000";s:6:"height";s:1:"0";s:3:"img";N;s:6:"border";a:3:{s:5:"width";s:1:"0";s:5:"color";s:7:"#000000";s:5:"style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:10:"width_full";b:0;}s:11:"top_sidebar";a:2:{s:6:"status";i:1;s:5:"style";a:3:{s:8:"bg_color";s:7:"#ff8d00";s:6:"height";s:2:"70";s:6:"bg_img";N;}}s:12:"left_sidebar";a:2:{s:6:"status";i:1;s:5:"style";a:3:{s:8:"bg_color";s:7:"#5ba8fb";s:5:"width";s:3:"220";s:6:"bg_img";N;}}s:13:"right_sidebar";a:2:{s:5:"style";a:3:{s:8:"bg_color";s:7:"#aa1920";s:5:"width";s:3:"300";s:6:"bg_img";N;}s:6:"status";i:0;}s:10:"main_style";a:9:{s:5:"width";s:4:"1200";s:13:"bg_color_body";s:7:"#e8e7ca";s:11:"bg_img_body";N;s:16:"bg_color_content";s:7:"#920700";s:14:"bg_img_content";N;s:6:"border";a:3:{s:5:"width";s:1:"0";s:5:"color";s:7:"#000000";s:5:"style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:10:"margin_top";s:2:"50";s:10:"width_full";b:0;}}',
            'content' => 'a:19:{s:4:"page";s:4:"test";s:7:"name_ru";s:5:"12213";s:4:"grid";s:1:"6";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:7:"content";s:4:"head";s:45:"Блок с примером контента";s:8:"block_id";s:2:"23";s:9:"box_style";a:10:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:8:"bg_color";s:7:"#ffffff";s:20:"bg_color_transparent";s:1:"1";s:13:"bg_color_head";s:7:"#ff8d00";s:25:"bg_color_head_transparent";s:1:"1";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:11:"width_tarif";s:1:"4";s:12:"widget_style";a:11:{s:13:"padding_tarif";a:4:{s:1:"t";s:2:"25";s:1:"b";s:2:"25";s:1:"l";s:2:"15";s:1:"r";s:2:"15";}s:16:"text_align_tarif";s:6:"center";s:10:"font_tarif";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:18:"bg_color_btn_tarif";s:7:"#ff8d00";s:15:"color_btn_tarif";s:7:"#ffffff";s:23:"border_radius_btn_tarif";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:2:"10";s:2:"br";s:2:"10";}s:16:"border_btn_tarif";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:20:"bg_color_btn_h_tarif";s:7:"#e17d00";s:17:"color_btn_h_tarif";s:7:"#e8e8e8";s:25:"border_radius_btn_h_tarif";a:4:{s:2:"tl";s:2:"40";s:2:"tr";s:2:"40";s:2:"bl";s:2:"40";s:2:"br";s:1:"5";}s:18:"border_btn_h_tarif";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;s:7:"content";s:4:"Test";}',
            'mydeposits' => 'a:23:{s:4:"page";s:7:"profile";s:7:"name_ru";s:5:"12213";s:4:"grid";s:1:"6";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:10:"mydeposits";s:4:"head";s:19:"Мои вклады";s:8:"block_id";s:2:"34";s:9:"box_style";a:10:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:8:"bg_color";s:7:"#222a33";s:20:"bg_color_transparent";s:1:"1";s:13:"bg_color_head";s:7:"#ff8d00";s:25:"bg_color_head_transparent";s:1:"1";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:11:"width_tarif";s:1:"4";s:12:"widget_style";a:50:{s:11:"border_line";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:10:"text_align";s:6:"center";s:4:"font";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"13";}s:8:"color_h3";s:7:"#000000";s:13:"text_align_h3";s:4:"left";s:7:"font_h3";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:1:"0";}s:11:"bg_color_th";s:7:"#dbceaf";s:23:"bg_color_th_transparent";N;s:8:"color_th";s:7:"#878171";s:11:"bg_color_tr";s:7:"#e8e7ca";s:23:"bg_color_tr_transparent";N;s:8:"color_tr";s:7:"#8e8d7f";s:13:"bg_color_tr_h";s:7:"#dbceaf";s:25:"bg_color_tr_h_transparent";N;s:10:"color_tr_h";s:7:"#878171";s:16:"bg_color_btn_top";s:7:"#ff8d00";s:28:"bg_color_btn_top_transparent";N;s:13:"color_btn_top";s:7:"#000000";s:21:"border_radius_btn_top";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:14:"border_btn_top";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:18:"bg_color_btn_h_top";s:7:"#000000";s:30:"bg_color_btn_h_top_transparent";N;s:15:"color_btn_h_top";s:7:"#000000";s:23:"border_radius_btn_h_top";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:16:"border_btn_h_top";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:17:"bg_color_btn_more";s:7:"#ff8d00";s:29:"bg_color_btn_more_transparent";N;s:14:"color_btn_more";s:7:"#ffffff";s:22:"border_radius_btn_more";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:15:"border_btn_more";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:19:"bg_color_btn_h_more";s:7:"#d77800";s:31:"bg_color_btn_h_more_transparent";N;s:16:"color_btn_h_more";s:7:"#e8e8e8";s:24:"border_radius_btn_h_more";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:17:"border_btn_h_more";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:12:"bg_color_btn";s:7:"#e1e1e1";s:24:"bg_color_btn_transparent";N;s:9:"color_btn";s:7:"#000000";s:17:"border_radius_btn";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:10:"border_btn";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#000000";s:26:"bg_color_btn_h_transparent";N;s:11:"color_btn_h";s:7:"#000000";s:19:"border_radius_btn_h";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:12:"border_btn_h";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_a";s:7:"#000000";s:26:"bg_color_btn_a_transparent";N;s:11:"color_btn_a";s:7:"#000000";s:19:"border_radius_btn_a";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:12:"border_btn_a";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:1;s:7:"content";s:4:"Test";s:7:"bird_id";s:2:"11";s:11:"last_tarifs";s:1:"1";s:4:"list";a:6:{i:1;a:3:{s:4:"name";s:29:"Название вклада";s:5:"value";s:12:"deposit_name";s:4:"icon";s:14:"fa fa-bullseye";}i:2;a:3:{s:4:"name";s:23:"Нами вложено";s:5:"value";s:14:"deposit_number";s:4:"icon";s:16:"fa fa-line-chart";}i:3;a:3:{s:4:"name";s:31:"Осталось времени";s:5:"value";s:15:"deposit_seconds";s:4:"icon";s:9:"fa fa-bed";}i:4;a:3:{s:4:"name";s:12:"Ресурс";s:5:"value";s:16:"deposit_resource";s:4:"icon";s:10:"fa fa-star";}i:5;a:3:{s:4:"name";s:19:"На балансе";s:5:"value";s:15:"deposit_balance";s:4:"icon";s:11:"fa fa-money";}i:6;a:3:{s:4:"name";s:14:"Собрать";s:5:"value";s:11:"deposit_btn";s:4:"icon";s:11:"fa fa-cloud";}}s:13:"last_deposits";s:1:"5";}',
            'store' => 'a:23:{s:4:"page";s:7:"profile";s:7:"name_ru";s:5:"12213";s:4:"grid";s:1:"6";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:5:"store";s:4:"head";s:25:"Сделать вклад";s:8:"block_id";s:2:"35";s:9:"box_style";a:10:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:8:"bg_color";s:7:"#222a33";s:20:"bg_color_transparent";s:1:"1";s:13:"bg_color_head";s:7:"#ff8d00";s:25:"bg_color_head_transparent";s:1:"1";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:11:"width_tarif";s:1:"4";s:12:"widget_style";a:50:{s:11:"border_line";a:3:{s:12:"border_width";s:1:"5";s:12:"border_color";s:7:"#222a33";s:12:"border_style";s:5:"solid";}s:10:"text_align";s:6:"center";s:4:"font";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"14";}s:8:"color_h3";s:7:"#000000";s:13:"text_align_h3";s:4:"left";s:7:"font_h3";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:1:"0";}s:11:"bg_color_th";s:7:"#dbceaf";s:23:"bg_color_th_transparent";N;s:8:"color_th";s:7:"#878171";s:11:"bg_color_tr";s:7:"#e8e7ca";s:23:"bg_color_tr_transparent";N;s:8:"color_tr";s:7:"#8e8d7f";s:13:"bg_color_tr_h";s:7:"#dbceaf";s:25:"bg_color_tr_h_transparent";N;s:10:"color_tr_h";s:7:"#878171";s:16:"bg_color_btn_top";s:7:"#ff8d00";s:28:"bg_color_btn_top_transparent";N;s:13:"color_btn_top";s:7:"#000000";s:21:"border_radius_btn_top";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:14:"border_btn_top";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:18:"bg_color_btn_h_top";s:7:"#000000";s:30:"bg_color_btn_h_top_transparent";N;s:15:"color_btn_h_top";s:7:"#000000";s:23:"border_radius_btn_h_top";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:16:"border_btn_h_top";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:17:"bg_color_btn_more";s:7:"#000000";s:29:"bg_color_btn_more_transparent";N;s:14:"color_btn_more";s:7:"#000000";s:22:"border_radius_btn_more";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:15:"border_btn_more";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:19:"bg_color_btn_h_more";s:7:"#000000";s:31:"bg_color_btn_h_more_transparent";N;s:16:"color_btn_h_more";s:7:"#000000";s:24:"border_radius_btn_h_more";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:17:"border_btn_h_more";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:12:"bg_color_btn";s:7:"#000000";s:24:"bg_color_btn_transparent";N;s:9:"color_btn";s:7:"#000000";s:17:"border_radius_btn";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:10:"border_btn";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#000000";s:26:"bg_color_btn_h_transparent";N;s:11:"color_btn_h";s:7:"#000000";s:19:"border_radius_btn_h";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:12:"border_btn_h";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_a";s:7:"#000000";s:26:"bg_color_btn_a_transparent";N;s:11:"color_btn_a";s:7:"#000000";s:19:"border_radius_btn_a";a:4:{s:2:"tl";N;s:2:"tr";N;s:2:"bl";N;s:2:"br";N;}s:12:"border_btn_a";a:3:{s:12:"border_width";N;s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;s:7:"content";s:4:"Test";s:13:"last_deposits";s:1:"5";s:4:"list";a:5:{i:1;a:3:{s:4:"name";s:16:"Название";s:5:"value";s:4:"name";s:4:"icon";s:4:"none";}i:2;a:3:{s:4:"name";s:8:"Цена";s:5:"value";s:5:"price";s:4:"icon";s:11:"fa fa-money";}i:3;a:3:{s:4:"name";s:22:"Доход в день";s:5:"value";s:16:"store_profit_day";s:4:"icon";s:16:"fa fa-area-chart";}i:4;a:3:{s:4:"name";s:21:"Доход всего";s:5:"value";s:16:"store_profit_all";s:4:"icon";s:12:"fa fa-rocket";}i:6;a:3:{s:4:"name";s:25:"Сделать вклад";s:5:"value";s:9:"store_btn";s:4:"icon";s:14:"fa fa-bullseye";}}s:7:"bird_id";s:2:"12";s:3:"sum";s:4:"5000";}',
            'bonusclick' => 'a:22:{s:4:"page";s:7:"profile";s:7:"name_ru";s:5:"12212";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:10:"bonusclick";s:4:"head";s:24:"Бонус за клик";s:8:"block_id";s:2:"39";s:6:"amount";N;s:9:"box_style";a:10:{s:15:"text_align_head";s:6:"center";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:8:"bg_color";s:7:"#222a33";s:20:"bg_color_transparent";s:1:"1";s:13:"bg_color_head";s:7:"#ff8d00";s:25:"bg_color_head_transparent";s:1:"1";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:9:"label_sum";s:17:"sadadsdasdsadsadf";s:12:"widget_style";a:13:{s:20:"text_align_text_area";s:6:"center";s:14:"font_text_area";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"17";}s:15:"color_text_area";s:7:"#fcfcfc";s:12:"bg_color_btn";s:7:"#ff8d00";s:24:"bg_color_btn_transparent";N;s:9:"color_btn";s:7:"#ffffff";s:17:"border_radius_btn";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"5";s:2:"br";s:1:"5";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#db7a00";s:26:"bg_color_btn_h_transparent";N;s:11:"color_btn_h";s:7:"#ffffff";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:4:"link";a:1:{s:4:"text";s:27:"Получить бонус";}s:4:"grid";s:1:"6";s:14:"payments_order";i:0;s:7:"cashout";i:0;s:10:"first_page";i:0;s:6:"active";i:0;s:10:"not_active";i:0;s:16:"random_referrals";i:0;s:7:"top_btn";i:0;s:4:"text";N;s:9:"text_area";s:43:"Получить бонус от 1 до 100";}',
            'exchange' => 'a:12:{s:4:"page";s:7:"profile";s:7:"name_ru";s:6:"123123";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:6:"widget";s:8:"exchange";s:4:"head";s:31:"Пополнить баланс";s:8:"block_id";s:2:"69";s:6:"amount";N;s:9:"box_style";a:8:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:7:"PT Sans";s:4:"size";s:2:"20";}s:2:"bg";s:7:"#222a33";s:7:"bg_head";s:7:"#ff8d00";s:10:"color_head";s:7:"#0f181a";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:9:"label_sum";s:25:"Введите сумму";s:12:"widget_style";a:15:{s:16:"text_align_label";s:4:"left";s:10:"font_label";a:3:{s:6:"family";s:7:"PT Sans";s:4:"size";s:2:"15";s:6:"weight";s:1:"1";}s:16:"label_color_text";s:7:"#fffef9";s:14:"bg_color_input";s:7:"#e8e7ca";s:11:"color_input";s:7:"#8e8d7f";s:12:"border_input";a:3:{s:12:"border_width";s:1:"1";s:12:"border_color";s:7:"#e8e7ca";s:12:"border_style";s:5:"solid";}s:19:"border_radius_input";a:4:{s:2:"tl";s:1:"3";s:2:"tr";s:2:"50";s:2:"bl";s:1:"3";s:2:"br";s:2:"50";}s:12:"bg_color_btn";s:7:"#ff8d00";s:9:"color_btn";s:7:"#ffffff";s:17:"border_radius_btn";a:4:{s:2:"tl";s:1:"5";s:2:"tr";s:1:"5";s:2:"bl";s:1:"5";s:2:"br";s:1:"5";}s:10:"border_btn";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:14:"bg_color_btn_h";s:7:"#db7a00";s:11:"color_btn_h";s:7:"#ffffff";s:19:"border_radius_btn_h";a:4:{s:2:"tl";s:1:"0";s:2:"tr";s:1:"0";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:12:"border_btn_h";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#000000";s:12:"border_style";s:5:"solid";}}s:4:"link";a:1:{s:4:"text";s:31:"Пополнить баланс";}}',
        ];

        $boxes = [
            'mainbox' => 'a:8:{s:4:"page";s:7:"profile";s:7:"name_ru";s:5:"12213";s:4:"type";s:7:"mainbox";s:3:"col";s:2:"12";s:4:"head";s:21:"Мой профиль";s:8:"block_id";s:3:"178";s:9:"box_style";a:10:{s:15:"text_align_head";s:4:"left";s:9:"font_head";a:2:{s:6:"family";s:5:"Arial";s:4:"size";s:2:"18";}s:8:"bg_color";s:7:"#222a33";s:20:"bg_color_transparent";s:1:"1";s:13:"bg_color_head";s:7:"#ff8d00";s:25:"bg_color_head_transparent";s:1:"1";s:10:"color_head";s:7:"#222a33";s:6:"border";a:3:{s:12:"border_width";s:1:"0";s:12:"border_color";s:7:"#ffffff";s:12:"border_style";s:5:"solid";}s:13:"border_radius";a:4:{s:2:"tl";s:2:"10";s:2:"tr";s:2:"10";s:2:"bl";s:1:"0";s:2:"br";s:1:"0";}s:7:"padding";a:2:{s:4:"left";s:2:"15";s:5:"right";s:2:"15";}}s:4:"grid";s:1:"6";}',
            'sidebar' => 'a:1:{s:3:"col";s:2:"12";}',
        ];

        $boxNow = unserialize($boxes[$box]);
        $boxNow['head'] = $head;
        $boxNow['col'] = $col;
        $widgetNow = unserialize($widgets[$widget]);

        $result = serialize(array_merge($widgetNow, $boxNow));


        return $result;

    }
}
