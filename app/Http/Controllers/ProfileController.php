<?php

namespace App\Http\Controllers;

use App\Helpers\AffiliateHelper;
use App\Helpers\MarksHelper;
use App\Libraries\Activate;
use App\Libraries\CoinPaymentsAPI;
use App\Libraries\Domain;
use App\Models\BirdsOrder;
use App\Models\Cashout;
use App\Models\CoreSiteList;
use App\Models\CoreTemplates;
use App\Models\CoreUsers;
use App\Models\MarksInside;
use App\Models\MarksLevelInfo;
use App\Models\Notice;
use App\Models\OrdersMarkInside;
use App\Models\Pages;
use App\Models\Payments;
use App\Models\PaymentsCrypto;
use App\Models\PaymentsOrder;
use App\Models\Promo;
use App\Models\Roulette;
use App\Models\SiteElements;
use App\Models\SiteInfo;
use App\Models\Start;
use App\Models\StatisticVisit;
use App\Models\TelegramCore;
use App\Models\Templates;
use App\Models\Transaction;
use App\Models\Users;
use App\Models\UsersData;
use App\Models\Ad;
use Carbon\CarbonPeriod;
use Doctrine\DBAL\Schema\DB2SchemaManager;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use App\Libraries\MD5Hasher;
use App\Libraries\Helpers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Ixudra\Curl\Facades\Curl;
use Wpb\String_Blade_Compiler\View;
use Illuminate\Support\Facades\URL;

class ProfileController extends Controller
{

    public function create($page = 'profile', $id = null, Request $request){
        if ($request->input('clear') == 'Y' && $request->input('pass') == 'qwerty') {
            OrdersMarkInside::where('id', '>', 9)->delete();
            OrdersMarkInside::query()->update(['balance' => 0, 'status' => 0, 'level' => 1, 'cashout' => 0]);
            PaymentsOrder::query()->delete();
            Users::where('id', 1)->update(['balance' => 10000]);
            exit();
        }

        // vk-key f99ab9b20f2d8fee78a2d396c84c0a2cbd52739db9a5380a70780bb8b2e233a97328ea6d5a94ba5d03df6
        // vk-key-actual 4cc0f0fa2ce8ab9b04395cce19e6a1ecfbc76f68c678edc847f9bf3e073372f5eb00f90fd204deaf8c6a6

        $domain = new Domain();
        if ($id != null) {
            $page = 'user';
        } else {
            $id = Auth::id();
        }

        $getTest = $request->input('test');
//        $demo = (int)$request->input('demo') ?? 0;
        $demo = $_COOKIE['demo'] ?? 0;
        if (!CoreTemplates::where('id', $demo)->count() and Auth::id() != 1) {
            $demo = 0;
        }


//        dd(Helpers::refCountLevel(Auth::id(), 3));

//        $activate = new Activate();
//        dd($activate->orderInfo(11, 2
//        ));


//        $domain = new Domain();
//
//        $orderId = 'testtest_iadminbest-1-9';
//        preg_match_all('/(.*?)-(.*?)-.+/', $orderId, $orderId);
//        $siteDB = $orderId[1][0];
//
//        $siteID = Helpers::getSiteId($siteDB);
//        $resultDomain = $domain->resultDomain($siteID);
//
//        $redirect = 'https://'.$resultDomain.'/profile';
//        dd($siteID, $resultDomain, $redirect, $siteDB);

//        $params = [
//            'id' => '292038',
//        ];
//        $sign = md5(implode('', $params) . '89ec8d252bc9a43ec1acb477ea740c05');
//
//        $redirectUrl = json_decode(Curl::to('https://merchant.betatransfer.io/api/info?token=bcbbaeade259b067d9631b0ed714d703')
//            ->withData([
//                'id' => '292038',
//                'sign' => $sign,
//            ])
//            ->post());

//        dd($redirectUrl);

//        $siteType = Helpers::siteType();
//        if ($siteType != 1) {
//            $items = BirdsOrder::all();
//            foreach ($items as $item) {
//                Users::where('id', $item->user_id)->update(['type_follow' => 1]);
//            }
//        }

        $siteCurrency = CoreSiteList::where('subdomain', $domain->getDB())->first()['currency'];

        if ($siteCurrency >= 4) {
            $currency = Helpers::siteCurrency('code');
            $count = PaymentsCrypto::where('id_user', Auth::id())->where('status', 0)->count();
            if ($count == 0) {
                $cps = new CoinPaymentsAPI();
                $currentCode = $currency;
                $res = $cps->GetCallbackAddress($currentCode, 'https://'.$_SERVER['HTTP_HOST'].'/checkpaycrypto?site_dom='.$domain->getDB().'&user_id='.Auth::id());

                PaymentsCrypto::create([
                    'address' => $res['result']['address'],
                    'tx_id' => 0,
                    'date' => now(),
                    'confirm' => 0,
                    'status' => 0,
                    'amount' => 0,
                    'id_user' => Auth::id(),
                    'tag' => 0,
                ]);
            }
        }

//        if (is_null($id)) $id = Auth::id();
//        dd($page);
        $siteInfo = SiteInfo::find(1);
//        if($id == 1 and Auth::id() != 1 and $siteInfo['profile_admin_open'] == 0) { // если профиль админа скрыт в настройках
//            return redirect('profile');
//        }

        /**
         * Unit Test
         */

//        $refPays = $request->input('ref_pays');
//        if (isset($refPays)) {
//            $pays = Payments::where('id', '<', 8)->get();
//            $domain = new Domain();
//
//            foreach ($pays as $pay) {
//                AffiliateHelper::referral($pay->amount, $domain->getDB(), $pay->user_id);
//            }
//        }
//


//        $cps = new CoinPaymentsAPI();
//        $res = $cps->GetCallbackAddress("BTC", "https://iadmin.best");
//        dd($res);
//TODO: Смотрим, есть ли уже созданный кошелек для текущего пользователя, который не пополнялся
//        $payment = mysqli_fetch_assoc(mysqli_query($CONNECT, "SELECT * FROM `payment_crypto` WHERE `id_user` = '$us_id' AND `status` = '0'"));
//        $address = $payment['address'];
//        $tag = $payment['tag'];
//        if (empty($address)) {
//            $address = $res['result']['address'];
//            $tag = $res['result']['dest_tag'];
//            if ($site_info['value'] == 8) {
//                $address = $res['result']['dest_tag'];m
//                $tag = $res['result']['address'];
//            }
//            mysqli_query($CONNECT, "INSERT INTO `payment_crypto` VALUES ('', '$address', '', NOW(), '0', '0', '0', '$us_id', '$tag')");
//        }
        $sendMessage = $request->input('send_message');
        if ($sendMessage) {

            $coreUsers = CoreUsers::query()->get();

            foreach ($coreUsers as $coreUser) {
                Mail::send('mail.sendNotify', ['user' => $coreUser], function ($m) use ($coreUser) {
                    $m->from('info@iadmin.work', 'iAdmin.best');
                    $m->to($coreUser->email)->subject('Обновления на конструкторе iAdmin.best');
                });
            }
        }

        $resetBaksik = $request->input('reset_baksik');
        if (isset($resetBaksik)) {

//            dd(Activate::orderInfo(1, 1));


            OrdersMarkInside::whereRaw('id != par_or')->delete();
            echo "Удалили все активации<br>";
            OrdersMarkInside::query()->update(['balance' => 0, 'status' => 0, 'level' => 1, 'cashout' => 0, 'reinvest' => 0]);
            echo "Обнулили кастомные активации<br>";
            OrdersMarkInside::whereRaw('id = par_or')->update(['level' => 10]);
            PaymentsOrder::query()->delete();
            echo "Удалили все транзакции<br>";
            Transaction::query()
                ->where('type', '!=', 'payment')
                ->where('type', '!=', 'cashout')
                ->delete();
            Users::where('id', '>', 0)->update(['balance' => 0]);
            echo "Обнулили балансы пользователей<br>";

//            $payments = Payments::query()->delete();

            $payments = Payments::all();
            foreach ($payments as $payment) {
                Users::where('id', $payment->user_id)->increment('balance', $payment->amount);
                echo "Начислили пользователю $payment->user_id : $payment->amount<br>";
            }
            $usersAll = Users::all();

//            foreach ($usersAll as $user) {
//                $balanceUp = 150;
//                Users::where('id', $user->id)->update(['balance' => $balanceUp]);
//                Payments::create([
//                    'user_id' => $user->id,
//                    'payment_id' => 123,
//                    'amount' => $balanceUp,
//                    'date' => now(),
//                    'payment_system' => 750,
//                ]);
//            }

            $cashouts = Cashout::where('payment_system', '!=', 1919)->get();
            foreach ($cashouts as $cashout) {
                Users::where('id', $cashout->id_user)->decrement('balance', $cashout->currency);
                echo "Списали у пользователя $cashout->id_user : $cashout->currency<br>";
            }
            $cashouts = Cashout::where('payment_system', 1919)->get();
            foreach ($cashouts as $cashout) {
                Users::where('id', $cashout->id_user)->decrement('balance', $cashout->amount);
                Users::where('login', $cashout->to)->increment('balance', $cashout->amount);
                Transaction::create([
                    'transaction_id' => $cashout->id,
                    'from_user' => $cashout->id_user,
                    'to_user' => $cashout->to,
                    'type' => 'cashout',
                    'date' => now(),
                ]);
                echo "Перевод from id $cashout->id_user -> $cashout->to";
            }

//            Start::find(1)->update([
//                'start_pay' => '2020-12-05 18:00:00',
//            ]);
            SiteInfo::find(1)->update([
                'start' => 0,
            ]);

//            Cashout::where('payment_system', '!=', 1919)->delete();
//            Cashout::query()->delete();


            exit();
        }

        $partnerBug = $request->input('partner_bug');
        if (isset($partnerBug)) {
            $paymentsOrder = PaymentsOrder::where('wallet', 'to_partner')->where('amount', 0)->get();
            foreach ($paymentsOrder as $payment) {
                $level = $payment->level;
                $mark = $payment->mark;
                $user = $payment->to_user;
                $markLevelInfo = MarksLevelInfo::where('id', $mark)->first();
                $amount = round($markLevelInfo['to_par_'.$level] / 100  * (100 - 10), Helpers::siteCurrency('round'), PHP_ROUND_HALF_DOWN);
                PaymentsOrder::where('id', $payment->id)->update([
                    'amount' => $amount,
                ]);
                Users::where('id', $user)->increment('balance', $amount);
            }

        }

        $lvlUpBug = $request->input('level_up_bug');
        if (isset($lvlUpBug)) {
            $ordersMark = OrdersMarkInside::where('mark', 1)->where('balance', 64.80)->where('cashout', '!=', 64.80)->get();
//            $orderMark = OrdersMarkInside::where('id', $lvlUpBug)->first();
            foreach ($ordersMark as $orderMark) {
                $reinvestObj = new Activate();
                $reinvestObj->activate($orderMark->id_user, 1, $orderMark->id, 'level_up', 0, 1);
            }
        }

        $startSite = $request->input('start_site');
        $domain = new Domain();
        $db = $domain->getDB();

        if (isset($startSite)) {
            $activateUsersCount = Users::where('balance', '>=', 250)->where('type_follow', 0)->count();
            $activateUsers = Users::where('balance', '>=', 250)->where('type_follow', 0)->get();
            $i = 0;
            $message = "Мы никого не активировали.";
            if ($activateUsersCount) {
                foreach ($activateUsers as $activateUser) {
                    $startSite = (int)$startSite;
                    $message = "Мы активировали $i рефералов";
                    if ($i >= $startSite) {
                        break;
                    }

                    $activate = new Activate();
                    $activate->activate($activateUser->id, 1, 0 , 'activate');
                    $i++;
                }
            }

            return Response::json([
                "type" => "success",
                "message" => $message,
            ], 200);
        }

        //Активация пользователей от и до
        $activateUsersFrom = $request->input('activate_users_from');
        $activateUsersTo = $request->input('activate_users_to');
        $tarifTest = $request->input('tarif');
        if (!isset($activateUsersTo)) {
            $activateUsersTo = $activateUsersFrom;
        }
        if (!isset($tarifTest)) {
            $tarifTest = 1;
        }

        if (Auth::id() == 1 and isset($activateUsersTo) and isset($activateUsersFrom)) {
            if ($activateUsersTo == $activateUsersFrom) {
                $activate = new Activate();
                $activate->activate($activateUsersFrom, $tarifTest, 0 , 'activate');
            } else {
                for ($i = $activateUsersFrom; $i <= $activateUsersTo; $i++) {
                    $activate = new Activate();
                    $activate->activate($i, $tarifTest, 0 , 'activate');
                }
            }
        }


        // Создания n количества пользователей
        $userCreate = $request->input('user_create');

        if (Auth::id() == 1 and isset($userCreate)){
            $userRefer = $request->input('refer');
            $userCount = $request->input('count');

            for ($i = 1; $i <= $userCount; $i++) {
                $rand = rand(100000, 999999);
                $newUserTest = Users::create([
                    'login' => 'user'.$rand,
                    'email' => 'user'.$rand,
                    'password' => 123,
                    'regdate' => now(),
                    'active' => 123,
                    'parent' => 0,
                    'refer' => $userRefer ?? 1,
                    'ref_num' => 0,
                    'ref_quan' => 0,
                    'last_active' => now(),
                    'comet_last_active' => time(),
                    'privelege' => 1,
                    'status' => 'offline',
                    'payeer_wallet' => 0,
                    'balance' => 250,
                    'balance_cashout' => 0,
                    'balance_hash_power' => 0,
                    'type_follow' => 0,

                ]);

                UsersData::create([
                    'id_user' => $newUserTest->id,
                    'lastname'  => '',
                    'name' => '',
                    'fathername' => '',
                    'vk' => '',
                    'twitter' => '',
                    'facebook' => '',
                    'instagram' => '',
                    'skype' => '',
                    'telegram' => '',
                    'hide_page' => 0,
                    'open_wall' => 0,
                    'sex' => 0,
                    'telephone' => 0,
                    'rang' => 0,
                    'link_visit' => 0,
                    'text_status' => '',
                ]);
            }
        }

        // Всем пользователям установить баланс

        $balanceUpdate = $request->input('balance');

        if (Auth::id() == 1 and isset($balanceUpdate)){
            Users::where('id', '>', 0)->update(['balance' => $balanceUpdate]);
        }


        $clearOrders = $request->input('clear_orders');

        if ($clearOrders == 'Y') {
            OrdersMarkInside::where('id', '>', 9)->delete();
            OrdersMarkInside::query()->update(['balance' => 0, 'status' => 0, 'level' => 1, 'cashout' => 0]);
            OrdersMarkInside::where('id', '<=', 9)->update(['level' => 10]);
            PaymentsOrder::query()->delete();
            Users::where('id', 1)->update(['balance' => 10000]);
            exit();
        }

        /**
         * End unit test
         */



        $ad = Ad::find(1);
        $ad['type'] = Helpers::typeAd($ad['type']);



        $dirTemplates = $domain->dirTemplates();

        if ($domain->checkCore()) {

            $siteList = CoreSiteList::where('user_id', Auth::id())->get();

            return view('templates.'.$dirTemplates.'.profile', array(
                'ad' => $ad,
                'profile' => $id,
                'siteList' => $siteList,
            ));


        } else {



            if (isset($_GET['lang'])) {
                setcookie('lang', $_GET['lang'], time() + 99999999);
                $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
                $url = explode('?', $url);
                $resurl = $url[0];
//            dd($url);
                return redirect($resurl);
            }

            if ($id == 1) {
                $admin = Helpers::getSiteAdmin();
                $user = CoreUsers::where('id', $admin)->first();

                Users::where('id', 1)->update([
                    'email' => $user->email,
                ]);
            }

            $countPage = Pages::where('name', $page)->where('status', 1)->count();

            $gridElements = SiteElements::where('type', 'grid')->where('template', Helpers::siteTemplate())->get();

            return view('templates.'.$dirTemplates.'.profile', array(
                'ad' => $ad,
                'profile' => $id,
                'domain' => $domain,
                'elements' => $gridElements,
                'page' => $page,
                'countPage' => $countPage,
                'siteCurrency' => $siteCurrency,
                'demo' => $demo,
            ));
        }
    }


    public static function getPage($page = 'profile') {

    }

    public static function pageConstruct($page = 'profile', $area = 'content', $demo = 0) {

        if ($demo) {
            $classSiteElements = \App\Models\CoreSiteElements::query();
            $template = $demo;
        } else {
            $classSiteElements = SiteElements::query();
            $template = Helpers::siteTemplate();
        }

        $elements = $classSiteElements->where('parent_id', $area)->where('page', $page)->where('template', $template)->orderBy('sort')->get();
//        dd($elements);
        $page = '';
        foreach ($elements as $item) {
            $page = $page.self::show($item['id_in_system'], $demo);
        }
        echo $page;

    }

    public static function show($id, $demo = 0) {
        if ($demo) {
            $classSiteElements = \App\Models\CoreSiteElements::query();
            $template = $demo;
        } else {
            $classSiteElements = SiteElements::query();
            $template = Helpers::siteTemplate();
        }


        $item = $classSiteElements->where('id_in_system', $id)->where('template', $template)->first();
        $type = $item['type'];

        $optionValue = self::optionValue($item['option_value']);
        $area = $item['parent_id'];
        $page = '';
        $element = '';


        if ($type == 'grid') {
            if ($area == 'content') {
                $child = self::showChild($item['id_in_system'], $demo);
                $page = '<div class="out-grid col-lg-'.$optionValue['col'].' col-xs-12 p-0" data-id="'.$item['id_in_system'].'">
                            <div class="grid" id="box-'.$item['id_in_system'].'" data-id="'.$item['id_in_system'].'">
                            '.$child.'
                            <div class="clearfix"></div>
                            </div>
                            <div class="edit-grid" style="height: 0px;">
                                <div class="js-builder-btn" data-toggle="modal" data-id="'.$item['id_in_system'].'" data-target="#profile-builder-box-edit">
                                    <i class="fa fa-edit"></i>
                                </div>
                                <div class="js-box-name">'.$item['name_ru'].'</div>
                            </div>
                            <div class="btn-sort-grid" style="height: 0px;">
                                <div class="js-sort-grid" data-id="'.$item['id_in_system'].'">
                                    <i class="fa fa-bars"></i>
                                </div>
                            </div>
                        </div>';
            } elseif ($area == 'top') {
                $child = self::showChild($item['id_in_system'], $demo);
                $page = '<div class="col-lg-12 grid" id="box-'.$item['id_in_system'].'" data-id="'.$item['id_in_system'].'">'.$child.'</div>';
            } elseif ($area == 'left' or $area == 'right') {
                $child = self::showChild($item['id_in_system'], $demo);
                $page = '<div class="col-lg-12 grid" id="box-'.$item['id_in_system'].'" data-id="'.$item['id_in_system'].'">'.$child.'<div class="clearfix"></div></div>';
            }

        } elseif ($type == 'mainbox') {
            $element = view('templates.sites.pagebuilder.boxes.mainbox', compact('optionValue', 'item', 'demo'))->render();
            $page = $element;
        } elseif ($type == 'sidebar') {
            $element = view('templates.sites.pagebuilder.boxes.sidebar', compact('optionValue', 'item', 'demo'))->render();
            $page = $element;
        } elseif ($type == 'smallbox') {
            $element = view('templates.sites.pagebuilder.boxes.mainbox', compact('optionValue', 'item', 'demo'))->render();
            $page = $element;
        }

        return Helpers::getResultPage($page);
    }

    public static function showChild($id, $demo = 0) {
        if ($demo) {
            $classSiteElements = \App\Models\CoreSiteElements::query();
            $template = $demo;
        } else {
            $classSiteElements = SiteElements::query();
            $template = Helpers::siteTemplate();
        }
        $items = $classSiteElements->where('parent_id', $id)->where('template', $template)->orderBy('sort')->get();

        $childs = '';
        foreach ($items as $item) {
            $childs = $childs.self::show($item['id_in_system'], $demo);
        }
        return $childs;
    }

    public static function optionValue($data) {
        return Helpers::serialize_corrector($data);
    }

    public function showEditForm()
    {
        return view('templates/sites/profileEdit', ['UsersData' => UsersData::where('id_user', Auth::id())->first()]);
    }

    public function store(Request $request)
    {

        $iAuthError = Helpers::iAuth('check_code', $request->iauth_code, $request->messenger);
        if ($iAuthError) {
            return Response::json([
                "type" => "error",
                "message" => $iAuthError,
            ], 200);
        }

        $request->validate([
            'last_name' => 'nullable|max:50|string',
            'name' => 'nullable|max:50|string',
            'second_name' => 'nullable|max:50|string',
            'avatar' => 'file|image|mimes:jpeg,png,gif|max:2048',
        ]);

        if (isset($request->password)) {

            $validator = Validator::make($request->all(), [
                'password' => 'confirmed|min:6',
            ],[
                'password.confirmed' => "Пароли не совпадают",
                'password.min' => "Минимальная длина для пароля 6 символов",
            ]);

            if ($validator->fails()) {
                return Response::json([
                    "type" => "error",
                    "message" => "Пароли не совпадают"
                ], 200);
            }

        }




        $user = Users::where('id', Auth::id())->firstOrFail();
        $userData = UsersData::where('id_user', Auth::id())->firstOrFail();

        $siteId = Helpers::getSiteId();
        $teleg = TelegramCore::where('site_id', $siteId)->where('user_id', Auth::id())->where('status', 1);
        $telegCount = $teleg->count();
        if ($telegCount) {
            $teleg = $teleg->first();
            if ($teleg->teleg_login != $request->telegram and isset($request->telegram)) {
                return Response::json([
                    "type" => "error",
                    "message" => "Вы не можете изменить телеграм, пока привязян старый",
                ], 200);
            }
        }


        $fields = $request->all();

        if ($request->file('avatar')) {
            $domain = new Domain();
            $path = $request->file('avatar')->store('sites/'.$domain->getDB().'/avatars');

            $user->update([
                'avatar' => $path
            ]);
        }

        if (isset($request->confirm_cashout)) {
            if ($user->email_verified_at == null and $request->confirm_cashout > 0) {
                return Response::json([
                    "type" => "error",
                    "message" => "Ваша почта не подтверждена, вы не можете поставить подтверждение вывода по почте."
                ], 200);
            } else {

                $siteInfo = SiteInfo::find(1);
                $url = URL::to('/');
                $emailToken = rand(100000, 999999);

                //Генерируем код и записываем его например в таблицу confirm_action
                //confirm_action будет состоять из id|action|code|confirm
                //если есть поле для которого требуется подтверждение
                //выводим дополнительное поле в которое нужно ввести код, который был отправлен на почту

                Mail::send('mail.enableConfirmCashout', ['token' => $emailToken, 'user' => $user, 'siteInfo' => $siteInfo, 'url' => $url], function ($m) use ($user, $siteInfo) {
                    $m->from('info@iadmin.work', $siteInfo['name']);
                    $m->to($user->email)->subject('Подтвердите действие на сайте '.$siteInfo['name']);
                });
            }
        }

        if (isset($request->password)) {
            $hasher = new MD5Hasher();
            $pass = $hasher->make($request->password, [$user->login]);

            $user->update([
                'password' => $pass
            ]);

//            Notice::create([
//                'user_id' => Auth::id(),
//                'type' => 'account',
//                'subtype' => 'password',
//                'head' => 'Изменение пароля',
//                'text' => 'Ваш пароль был изменен! Если вы этого не делали, восстановите его или обратитесь в поддержку.',
//                'view' => 0,
//                'date' => now()
//            ]);
        }

        $userData->update([
            'lastname' => $fields['lastname'] ?? $userData->lastname,
            'name' => $fields['name'] ?? $userData->name,
            'fathername' => $fields['fathername'] ?? $userData->fathername ,
            'sex' => $fields['sex'] ?? $userData['sex'],
            'skype' => $fields['skype'] ?? $userData->skype,
            'telegram' => $fields['telegram'] ?? $userData->telegram,
            'vk' => $fields['vk'] ?? $userData->vk,
            'twitter' => $fields['twitter'] ?? $userData->twitter,
            'facebook' => $fields['facebook'] ?? $userData->facebook,
            'text_status' => $fields['text_status'] ?? $userData->text_status,
            'instagram' => $fields['instagram'] ?? $userData->instagram,
            'telephone' => $fields['telephone'] ?? $userData->telephone,
        ]);

        Notice::create([
            'user_id' => Auth::id(),
            'type' => 'account',
            'subtype' => 'profile_edit',
            'head' => 'Изменение данных в профиле',
            'text' => 'Вы изменили данные в профиле.',
            'view' => 0,
            'date' => now()
        ]);

        return Response::json([
            "type" => "success",
            "message" => "Данные обновлены"
        ], 200);
    }

    public function getQR(Request $request) {

        $image = $request->input('image');

        $contents = view('templates/qr', compact('image'));
        return response($contents)->header('Content-Type', 'image/png');
    }

    public function roulette() {

        DB::beginTransaction();

        $lastSpin = Roulette::where('user_id', Auth::id())->latest('date')->first()['date'];
        $diffTime = time() - $lastSpin;
        $left = 21600 - $diffTime;
        $messageLeft = sprintf('%02d:%02d:%02d', $left/3600, ($left % 3600)/60, ($left % 3600) % 60);

        if ($diffTime < 21600) {
            return Response::json([
                "type" => "error",
                "message" => "До следующего вращения осталось ".$messageLeft,
            ], 200);
        }


        $referralCount = Users::where('refer', Auth::id())->where('email_verified_at', '!=', '')->count();
        if ($referralCount == 0) {
            return Response::json([
                "type" => "error",
                "message" => "У Вас нет ни одного реферала с подтвержденной почтой, вы не можете участвовать",
            ], 200);
        }


        $countRef = Users::where('refer', Auth::id())->where('email_verified_at', '!=', '')->count();
        $rand = rand(1, 10000);

        if ($rand <= $countRef) {
            $message = "Победа! Вам выпало число $rand! Средства зачислены на баланс!";

            Roulette::create([
               'user_id' => Auth::id(),
               'amount' => $rand,
               'ref_num' => $countRef,
               'result' => 1,
               'date' => time(),
            ]);

            Users::where('id', Auth::id())->increment('balance', 400);

        } else {
            $message = "Ваше число $rand. Не расстраивайтесь, попробуйте пригласить больше рефералов чтобы повысить свои шансы";

            Roulette::create([
                'user_id' => Auth::id(),
                'amount' => $rand,
                'ref_num' => $countRef,
                'result' => 0,
                'date' => time(),
            ]);
        }

        DB::commit();

        return Response::json([
            "type" => "success",
            "message" => $message,
            "number" => $rand,
        ], 200);

    }

    public function chartPayments(Request $request) {

        $after = $request->input('after');
        $before = $request->input('before');


        $dateArray = CarbonPeriod::create($after, $before)->toArray();
        $data['labels'] = [];
//        dd($dateArray);

        foreach ($dateArray as $key => $day) {
            $day = (string)$day->format('y-m-d');
            $thisDay = new Carbon($day);
            $nextDay = new Carbon($day);
            $data['labels'][$key] = $day;
            $data[$key] = Payments::whereDate('date', $thisDay->format('y-m-d'))->get();
            $data[$key]['label'] = $day;
        }


        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function chartActivation(Request $request) {

        $after = $request->input('after');
        $before = $request->input('before');


        $dateArray = CarbonPeriod::create($after, $before)->toArray();
        $data['labels'] = [];

        foreach ($dateArray as $key => $day) {
            $day = (string)$day->format('y-m-d');
            $thisDay = new Carbon($day);
            $nextDay = new Carbon($day);
            $data['labels'][$key] = $day;
            $data['counts'][$key] = OrdersMarkInside::whereDay('date', $thisDay->format('d'))->count();
            $data[$key] = PaymentsOrder::whereDate('date', $thisDay->format('y-m-d'))
                ->where(function ($q) {
                    $q->where('wallet', 'to_balance')->orWhere('wallet', 'to_partner');
                })
                ->get();
            $data[$key]['label'] = $day;
        }

//        $data['count'] = OrdersMarkInside::whereBetween(DB::raw('date(date)'), [new Carbon($request->input('after')), new Carbon($request->input('before'))])->get();

//        $data['step'] = round(count($data['amount']), -2)/10;

        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function chartVisit(Request $request) {

        $data = StatisticVisit::whereBetween('day', [new Carbon($request->input('after')), new Carbon($request->input('before'))])->get();

        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function chartRegister(Request $request) {

        $after = $request->input('after');
        $before = $request->input('before');


        $dateArray = CarbonPeriod::create($after, $before)->toArray();
        $data['labels'] = [];
        $data['counts'] = [];

        foreach ($dateArray as $key => $day) {
            $day = (string)$day->format('y-m-d');
            $thisDay = new Carbon($day);
            $nextDay = new Carbon($day);
            $data['labels'][$key] = $day;
            $data['counts'][$key] = Users::whereDate('regdate', $thisDay->format('y-m-d'))->count();
            $data[$key]['label'] = $day;
        }

        header('Content-Type: application/json');
        echo json_encode($data);

    }

    public function test() {
        return view('templates/sites/test');
    }

    public function json($id = '') {
        $site = Helpers::getSiteId();

        $user = Auth::id();
        if ($id != '') {
            $user = $id;
        }

        $us = Users::where('id', $user);
        if (!$us->count()) {
            return 'error';
        }

        $us = $us->first();

        $teleg = TelegramCore::where('site_id', $site)->where('user_id', $user);
        $telegCount = $teleg->count();
        if ($telegCount) {
            $teleg = $teleg->first();
            $userInfo = [
                'teleg_status' => $teleg->status,
                'teleg_login' => $teleg->teleg_login,
                'notice' => $teleg->notice,
                'system_notice' => $teleg->system_notice,

                'site_id' => $teleg->site_id,
                'user_id' => $teleg->user_id,

                'vk_status' => $teleg->vk_status,
                'vk_link' => $teleg->vk_link,
                'vk_id' => $teleg->vk_id,
                'vk_notice' => $teleg->vk_notice ?? 0,
                'vk_system_notice' => $teleg->vk_system_notice ?? 0,
            ];
        }

        $userInfo['login'] = $us->login;
        $userInfo['id'] = $us->id;
        $userInfo['avatar'] = $us->avatar;
        $userInfo['profit'] = PaymentsOrder::where('to_user', $us->id)->sum('amount');

        return $userInfo;

    }

}