<?php

namespace App\Http\Controllers;

use App\Models\Promo;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class PromoController extends Controller
{
    public static function all()
    {
        $items = Promo::all();

        return view('templates.sites.promo.promo', compact('items'));
    }

    public static function promoAdd()
    {
        $items = Promo::all();

        return view('templates.sites.promo.promoAdd', compact('items'));
    }

    public static function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'image' => 'required',
            'width' => 'required',
            'height' => 'required'
        ], [
            'name.required' => 'Укажите имя',
            'image.required' => 'Укажите ответ',
            'width.required' => 'Задайте ширину',
            'height.required' => 'Задайте высоту',
        ]);


        Promo::create([
            'name' => $request->name,
            'image_link' => $request->image,
            'width' => $request->width,
            'height' => $request->height
        ]);

        return Response::json([
            "success" => true,
            "message" => "Промоматериал успешно создан"
        ], 200);
    }

    public static function delete(Request $request) {
        if (Promo::destroy($request->id)) {
            return Response::json([
                "message" => "Промоматериал удален"
            ], 200);
        }

        return Response::json([
            "message" => "Не получилось удалить промоматериал"
        ], 422);

    }

    public static function promoViewEdit($id) {
        $item = Promo::find($id);

        return view('templates.sites.promo.promoEdit', array(
            'item' => $item
        ));
    }

    public static function promoEdit(Request $request) {

        $promo = Promo::find($request->id);

        $promo->name = $request->name;
        $promo->image_link = $request->image;
        $promo->width = $request->width;
        $promo->height = $request->height;


        if ($promo->save()) {
            return Response::json([
                "message" => "Промоматериал сохранен"
            ], 200);
        }

        return Response::json([
            "message" => "Не получилось сохранить промоматериал"
        ], 422);

    }


}
