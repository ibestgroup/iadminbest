<?php

namespace App\Http\Controllers;

use App\Libraries\Multimark;
use App\Models\MarksInside;
use App\Models\OrdersMarkInside;
use App\Models\PaymentsOrder;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;

class ReferalTree extends Controller
{
    public static $widgetSettings;

    function create(Request $request) {

        $markGet = $request->input('mark');
        $orders = DB::table('marks_inside')
            ->select(['marks_inside.*', 'marks_inside.level as m_level', 'orders_mark_inside.*'])
            ->leftJoin('orders_mark_inside', 'marks_inside.id', '=', 'orders_mark_inside.mark')
            ->where('id_user','=', Auth::id())->where('marks_inside.status','>','0');
        if (isset($markGet)) {
            $ordersCount = $orders->where('mark', $markGet)->count();
            $orders = $orders->where('mark', $markGet)->paginate(5);
        } else {
            $ordersCount = $orders->count();
            $orders = $orders->paginate(5);
            $markGet = 0;
        }

        $marks = MarksInside::where('status', '>', 0)->get();

        return view("templates.sites.referalTree", compact('orders', 'marks', 'ordersCount', 'markGet'));
    }

    function detail(Request $request) {
        $order = OrdersMarkInside::find($request->id);

        $tree = new Multimark(Auth::id(), $order->mark, true);

        $result = [
            'name' => Auth::user()->login,
            'children' => $tree->createTree($request->id)
        ];

        return Response::json($result);
    }

    static function referralPayList($array, $markNum, $orderMain, $level = 0, $url = '', $freePlaceNaming = 'свободное место') {
        $mark = MarksInside::where('id', $markNum)->first();
        foreach ($array as $key => $order) {
            $orderInfo = OrdersMarkInside::where('id', $order['id'])->first();
            $userInfo = Users::where('id', $order['user'])->first();
            $payment = PaymentsOrder::where('wallet', $orderMain)->where('level', $level)->where('from_user', $order['user'])->count();

            if($payment) {
                $bg = '#7e4de1';
            } else {
                $bg = '#ff8d2b';
            }

            if($level == 0) {
                $widthStyle = 100;
                $bg = '#7e4de1';
            } else {
                $widthStyle = 100/$mark['width'];
            }

            if ($userInfo->avatar == 'no_image') {
                $avatar = "/iadminbest/storage/app/".\App\Models\SiteInfo::find(1)->first()['no_image'];
            } else {
                $avatar = "/iadminbest/storage/app/".$userInfo->avatar;
            }

            if ($order['name'] == Auth::user()->login) {
                $loginTree = '<i class="fa fa-user"></i> Вы';
            } else {
                $loginTree = $order['name'];
            }

            $levelView = $orderInfo->id != $orderInfo->par_or ? $orderInfo['level'] . ' уровень' : '';

            echo "
            <div style='text-align:center; min-height: 30px; color: $bg; width: ". $widthStyle ."%; float: left;'>
                <div style='padding: 10px 2px;' class='img-tree'>
                    <a href='".$url."?tarif_id=".$orderInfo->id."'><img src=\"$avatar\" style='width: 100px; height: 100px; max-width: 100%;' class=\"img-circle-my\" alt=\"".$userInfo->login."\"></a>
                    <br>" . $loginTree . "<br>" . $levelView . " 
                </div>";

                if ($level+1 <= $mark['level']) {
                    ReferalTree::referralPayList($order['children'], $markNum, $orderMain, $level + 1, $url, $freePlaceNaming);
                }

                if (count($order['children']) < $mark['width'] and $level + 1 <= $mark['level']) {
                    if ($mark['width'] <= 8) {
                        ReferalTree::emptyPlace($level+1, $mark['level'], $mark['width'], count($order['children']) + 1, $freePlaceNaming);
                    }
                }

            echo "<div class='clearfix'></div>
                </div>";
        }

    }

    static function emptyPlace($level, $levelMax, $width, $count, $freePlaceNaming = 'свободное место') {
        $marks = MarksInside::where('id', 6)->first();

        $avatar = "/iadminbest/storage/app/".\App\Models\SiteInfo::find(1)->first()['no_image'];
        $bg = '#777';
        for ($i = $count; $i <= $width; $i++) {
            echo "
            <div style='text-align:center; min-height: 30px; color: $bg; width: " . 100 / $width . "%; float: left;'>
                <div style='padding: 10px 2px;' class='img-tree-free'>
                    <img src=\"".$avatar."\" style='width: 100px; height: 100px; max-width: 100%;' class=\"img-circle-my circle-free\" alt=\"User Image\">
                    <br>" . $freePlaceNaming . "
                </div>";

            if ($level + 1 <= $levelMax) {
                ReferalTree::emptyPlace($level + 1, $levelMax, $width, 1, $freePlaceNaming);
            }

            echo "<div class='clearfix'></div>
                </div>";
        }
    }

    public function info($id) {
        $order = OrdersMarkInside::where('id', $id)->first();

        $paymentsOrder = PaymentsOrder::where('from_wallet', $id)->orWhere('wallet', $id)->get();
//        dd($order, $paymentsOrder);
        return view("templates.sites.referalTreeInfo", compact('order', 'paymentsOrder'));

    }



    public static function ordersMarkQuery($userId, $params = [])
    {
        self::$widgetSettings = $params['widget_settings'] ?? [];
        $orders = OrdersMarkInside::where('id_user', $userId);

        return $orders;
    }

    public static function getOrdersMark($userId, $params = [])
    {
        $orders = self::ordersMarkQuery($userId, $params);
        if (isset($params['page']) and isset(self::$widgetSettings['per_page'])) {
//            self::$widgetSettings['per_page'] = self::$widgetSettings['per_page'] > 8 ? 8 : self::$widgetSettings['per_page'];
            if (!isset(self::$widgetSettings['only_first_page'])) {
                $orders = $orders->skip(($params['page'] - 1) * self::$widgetSettings['per_page']);
            }
            $orders = $orders->limit(self::$widgetSettings['per_page']);
        }

        return $orders->get();
    }

    public static function getOrdersMarkCount($userId, $params)
    {
        return self::ordersMarkQuery($userId, $params)->count();
    }

    public static function treeText($array, $markNum, $level = 0) {

        $resultString = $level == 0 ? "\n" : "";

        $mark = MarksInside::where('id', $markNum)->first();

        foreach ($array as $key => $order) {
            $orderInfo = OrdersMarkInside::where('id', $order['id'])->first();
            $dashes = str_repeat('  ', $level) . '╚═╦═';
            $loginTree = $order['name'];
            $resultString .= $dashes . ' ' . $loginTree . '   (' . $orderInfo['level'] . " ур)\n";

            if ($level+1 <= $mark['level']) {
                $resultString .= ReferalTree::treeText($order['children'], $markNum, $level + 1);
            }

//            if (count($order['children']) < $mark['width'] and $level + 1 <= $mark['level']) {
//                if ($mark['width'] <= 8) {
//                    $resultString .= ReferalTree::emptyPlaceText($level+1, $mark['level'], $mark['width'], count($order['children']) + 1);
//                }
//            }

        }

        return $resultString;

    }

    static function emptyPlaceText($level, $levelMax, $width, $count) {

        $resultString = '';
        for ($i = $count; $i <= $width; $i++) {

            $dashes = str_repeat('  ', $level) . '╚═╦═';
            $resultString .= $dashes . "свободное место\n";

            if ($level + 1 <= $levelMax) {
                 $resultString .= ReferalTree::emptyPlaceText($level + 1, $levelMax, $width, 1);
            }

        }
        return $resultString;
    }
}
