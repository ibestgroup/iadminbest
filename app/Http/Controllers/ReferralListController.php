<?php
namespace App\Http\Controllers;

use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class ReferralListController extends Controller {

    public static $widgetSettings;

    public static function all() {

        $onPage = Input::get('on_page');
        if (empty($onPage)) $onPage = 10;

        $referral = Users::where('refer', Auth::id())->with('users_data')->paginate($onPage);

        return view('templates/referralList', array(
            'referral' => $referral,
            'days' => 0
        ));
    }

    public static function active() {

        $onPage = Input::get('on_page');
        if (empty($onPage)) $onPage = 10;

        $referral = Users::where('refer', Auth::id())->where('type_follow', '>', 0)->with('users_data')->paginate($onPage);

        return view('templates/referralList', array(
            'referral' => $referral,
            'days' => 0
        ));
    }


    public static function regLastDay() {

        $days = Input::get('days');
        if (empty($days)) $days = 7;

        $onPage = Input::get('on_page');
        if (empty($onPage)) $onPage = 10;

        $toDate = Carbon::now();
        $fromDate = $toDate->copy()->subDays($days);


        $referral = Users::where('refer', Auth::id())->whereBetween('regdate', array($fromDate->toDateTimeString(), $toDate->toDateTimeString()) )->with('users_data')->paginate($onPage);

        return view('templates/referralList', array(
            'referral' => $referral,
            'days' => $days
        ));

    }

    public static function referralsQuery($parentId, $params = [])
    {
        self::$widgetSettings = $params['widget_settings'];
        $users = Users::where('refer', $parentId);
        $type = $widgetSettings['type'] ?? 'all';
        $typeFollow = $type == 'activate' ? 1 : 0;

        if ($type != 'all') {
            $users = $users->where('type_follow', $typeFollow);
        }

        return $users;
    }

    public static function getReferrals($parentId, $params)
    {
        $users = self::referralsQuery($parentId, $params);
        if (isset($params['page']) and isset(self::$widgetSettings['per_page'])) {
            $users = $users
                ->skip(($params['page'] - 1) * self::$widgetSettings['per_page'])
                ->limit(self::$widgetSettings['per_page']);
        }

        return $users->get();
    }

    public static function getReferralCount($parentId, $params)
    {
        return self::referralsQuery($parentId, $params)->count();
    }

}