<?php

namespace App\Http\Controllers;

use App\Libraries\Domain;
use App\libraries\Landing;
use App\Models\PaymentsOrder;
use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class StartPageController extends Controller
{
    public function start() {
        if (isset($_GET['lang'])) {
            setcookie('lang', $_GET['lang'], time() + 99999999);
            $url = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            $url = explode('?', $url);
            $resurl = $url[0];
//            dd($url);
            return redirect($resurl);
        }
        if (isset($_GET['id'])) {
            $referLogin = Users::where('id', $_GET['id'])->first();
            Cookie::queue('refer_login', $referLogin['login'], 86000);
        }

        if (isset($_GET['ref'])) {
            Cookie::queue('refer_login', $_GET['ref'], 86000);
        }

        $landing = new Landing();
        $domain = new Domain();
        if ($domain->checkCore()) {

            if ($_SERVER['HTTP_HOST'] == 'icash.best' or $_SERVER['HTTP_HOST'] == 'icashlocal.best' or $_SERVER['HTTP_HOST'] == 'icash.work' or $_SERVER['HTTP_HOST'] == 'icashlocal.work' or $_SERVER['HTTP_HOST'] == 'piramidayspeha.ru') {
                return view('templates/core/icash');
            } else {
                return view('templates/core/home');
            }

        }

//        dd($landing->sitePath);

        return Landing::getUserPage($landing->sitePath.'/'.'index.html');
    }

    public function showPageOrFail() {
        $landing = new Landing();
        if (file_exists($landing->sitePath.'/'.$_SERVER['REQUEST_URI'])) {
            return Landing::getUserPage($landing->sitePath.'/'.$_SERVER['REQUEST_URI']);
        }

        return response('404.php');
    }
}
