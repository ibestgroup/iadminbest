<?php

namespace App\Http\Controllers;

use App\Helpers\MarksHelper;
use App\Libraries\Domain;
use App\Libraries\Helpers;
use App\Libraries\Multimark;
use App\Libraries\Wallet;
use App\Models\Ad;
use App\Models\CashoutSystemCore;
use App\Models\CoreSiteList;
use App\Models\MarksInside;
use App\Models\OrdersMarkInside;
use App\Models\PaymentSystems;
use App\Models\SiteInfo;
use App\Models\TelegramBotSettings;
use App\Models\TelegramCore;
use App\Models\TelegramMenu;
use App\Models\Users;
use App\Models\UsersData;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Ixudra\Curl\Facades\Curl;
use Nexmo\User\User;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\Debug\Exception\DummyException;
use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Objects\Update;
use Telegram\Bot\TelegramClient;
use Telegram\Bot\Exceptions\TelegramSDKException;
use TelegramBot\Api\Exception;

class TelegramBotController extends Controller
{
    public $refCode;
    public $bot;
    public $botSettings;
    public $user;
    public $request;
    public $parentId;
    public $telegramId;
    public $telegramLogin;
    public $typeMessage;
    public $userLogin;
    public $keyboard;
    public $keyboardPagination;
    public $keyboardItems;
    public $chatId;
    public $messageId;
    public $messageText;
    public $resultMenu;
    public $domain;
    public $db;

    const ACTIONS = [
        'deposit',
        'withdraw',
        'activate',
    ];

    public function __construct(Request $request)
    {

        $this->domain = new Domain();
        $this->db = $this->domain->getDB();
        $this->request = $request->input();
        $this->registerBot();

        $this->typeMessage = (isset($this->request['callback_query'])) ? 'callback_query' : 'message';
        $this->telegramId = $this->request[$this->typeMessage]['from']['id'];
        $this->telegramLogin = $this->request[$this->typeMessage]['from']['username'] ?? '';
        $this->botSettings = TelegramBotSettings::find(1);

    }

    public function telegBot()
    {

        if ($this->typeMessage == 'message') {
            $this->chatId = $this->request['message']['chat']['id'];
            $this->messageText = $this->request['message']['text'];

            $this->registerAdmin();

            if (!Users::where('telegram_id', $this->telegramId)->count()) {
                if (isset($this->request['message']['text'])) {
                    $this->detectRegisterAction($this->request['message']['text']);
                }
            } else {
                $this->user = Users::where('telegram_id', $this->telegramId)->first();
                $this->userLogin = $this->user->login ?? $this->telegramLogin;

                if (isset($this->request['message']['text'])) {

                    if ($this->detectAction($this->request['message']['text'], $this->user->telegram_state)) {
                        $resultMenu = $this->getMenu();
                        $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup($resultMenu['keyboard']);
                        $this->send(
                            Helpers::getResultPage($this->resultMenu['text'], $this->user->id),
                            $keyboard
                        );
                    }
                }
            }
        } else {

            $this->chatId = $this->request['callback_query']['message']['chat']['id'];
            $this->messageId = $this->request['callback_query']['message']['message_id'];

            if (Users::where('telegram_id', $this->telegramId)->count()) {
                $this->user = Users::where('telegram_id', $this->telegramId)->first();
            }

            if (isset($this->request['callback_query'])) {
                $answer = explode('_', $this->request['callback_query']['data'])[1] ?? 1;

                $this->resultMenu = $this->getMenu($answer);
                $this->keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup($this->resultMenu['keyboard']);
                try {
                    $this->editMessage();
                } catch (\Exception $e) {
                    Log::info('Telegram bot error: ' . $e);
                    die('OK');
                }
            }

            die('OK');

        }
    }

    public function getMenu($answer = 1)
    {
        $telegramMenu = TelegramMenu::where('id', $answer)->where('type', 'menu')->first();
        if (!$telegramMenu) {
            $telegramMenu = TelegramMenu::find(1);
        }
        $telegramButtons = TelegramMenu::where('parent_id', $telegramMenu->id)->where('id', '!=', 1)->orderBy('sort')->get();
        $widget = isset($this->request['callback_query']['data']) ? $this->widget($this->request['callback_query']['data']) : '';

        $text = html_entity_decode($telegramMenu->text);
        $keyboard = [];
        $itemButton = [];
        $i = 0;
        foreach ($telegramButtons as $item) {
            if (isset($item->line_break) and $item->line_break) {
                $i++;
            }
            $itemButton[$i][] = [
                'text' => $item->name,
                'callback_data' => 'item_' . $item->id,
            ];
        }

        if (isset($this->keyboardItems)) {
            if (isset($this->keyboardItems[0]['callback_data'])) {
                $keyboard[] = $this->keyboardItems;
            } else {
                foreach ($this->keyboardItems as $item) {
                    $keyboard[] = $item;
                }
            }
        }

        if (isset($this->keyboardPagination)) {
            $keyboard[] = $this->keyboardPagination;
        }

        if (!empty($itemButton)) {
            foreach ($itemButton as $item) {
                $keyboard[] = $item;
            }
        }

        if ($telegramMenu->id != 1) {
            $backMenu = TelegramMenu::where('id', $telegramMenu->parent_id)->first();
            $keyboard[] = [
                [
                    'text' => '<<<- Главное меню',
                    'callback_data' => 'main',
                ],
                [
                    'text' => '<- Назад',
                    'callback_data' => 'item_' . $backMenu->id,
                ],
            ];
        }

        $text .= "\n $widget \n";
        $separator = $this->botSettings->separator;
        $separate = "\n $separator \n";
        $header = $this->botSettings->header ? $this->botSettings->header . $separate : '';
        $footer = $this->botSettings->footer ? $separate . $this->botSettings->footer : '';
        $result = $header . $text . $footer;
        $result = preg_replace('/\\\n/', PHP_EOL, $result);

        $this->resultMenu = ['text' => $result, 'keyboard' => $keyboard];
        $this->keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup($this->keyboard);
        return ['text' => $result, 'keyboard' => $keyboard];
    }

    public function registerBot()
    {

        Config::set("database.connections", [
            "host" => "127.0.0.1",
            "database" => $this->db,
            "username" => "root",
            "password" => "DrinkVodkaPlayDotka228",
            "driver" => "mysql",
            "charset" => "utf8mb4",
            'strict' => false,
        ]);

        $apiKey = self::getApiKey($this->db);

        $this->bot = new \TelegramBot\Api\BotApi($apiKey);
    }

    public function getRefCode()
    {

    }

    public static function getApiKey($subdomain)
    {
        return CoreSiteList::where('subdomain', $subdomain)->first()['telegram_api'];
    }

    public static function isConnected()
    {
        return CoreSiteList::find(Helpers::getSiteId())->telegram_api ? true : false;
    }

    public function detectAction($text, $state) {
        $this->chatId = $this->request['message']['chat']['id'];
        $stateValue = $this->getStateValue();

        $state = explode('_', $state);

        if (!in_array($state[0], self::ACTIONS)) {
            $keyboard = new \TelegramBot\Api\Types\ReplyKeyboardMarkup([['Главное меню']]);
            $this->send(
                'Главное меню',
                $keyboard
            );
            return true;
        }

        switch ($state[0]) {
            case 'deposit':
                if (isset($state[1])) {
                    switch ($state[1]) {
                        case 'system':
                            $string = "0 - Отмена\n";

                            $this->send("Выберите платежную систему: \n1 - Payeer\n2 - YMoney \n3 - QIWI \n$string");

                            $value = ['amount' => $text];
                            $this->updateState($state[0] . '_agree', $value);
                            break;
                        case 'agree':
                            $systems = ['', 'Payeer', 'YMoney', 'QIWI'];
                            if ($text == 0) {
                                $this->send('Отменено');
                            }
                            if (!array_key_exists($text, $systems)) {
                                $this->send('Платежный метод не существует. Выберите из списка.');
                            } else {
                                $this->send("Вы указали сумму: " . $stateValue['amount'] . "руб. и платежную систему {$systems[$text]}\nВсе верно?\n1 - Да\n2 - Нет");
                                $value = ['system' => $text];
                                $this->updateState($state[0] . '_link', $value);
                            }
                            break;
                        case 'link':
                            if ($text == 1) {
                                if ($stateValue['system'] == 1) {
                                    $siteId = Helpers::getSiteId();
                                    $this->send("Отправьте {$stateValue['amount']} на кошелек P1051636730 с комментарием <b>{$this->user->id}-{$siteId}</b>\n1 - Я отправил \n2 - Отменить");
                                    $this->updateState($state[0] . '_checkdeposit');
                                } else {
                                    $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                                        BalanceController::getDepositLink(
                                            $stateValue['amount'],
                                            $this->user->id,
                                            1
                                        )
                                    );

                                    $this->send('Ваша ссылка на пополнение баланса.', $keyboard);
                                    $this->updateState();
                                }
                            } else {
                                $this->send('Введите сумму снова');
                                $this->updateState($state[0] . '_agree');
                            }
                            break;
                        case 'checkdeposit':
                            if ($text == '1') {
                                $callback = new BalanceCallbackController();
                                $answer = $callback->checkPaymentPayeer($this->user->id)->original;
                                if ($answer['type'] == 'error') {
                                    $this->send($answer['message'] . "\n1 - Я отправил \n2 - Отменить");
                                } else {
                                    $this->send($answer['message']);
                                    $this->updateState();
                                }
                            } else {
                                $this->send('Возвращаю в главное меню');
                                $this->updateState();
                            }
                            break;
                    }
                }
                break;
            case 'withdraw':
                if (isset($state[1])) {
                    switch ($state[1]) {
                        case 'system':
                            $systems = CashoutSystemCore::where('currency', 1)->where('status', 1)->get();
                            $string = '';
                            foreach ($systems as $item) {
                                $string .= "{$item['id']} - {$item['name']}\n";
                            }
                            $string .= "0 - Отмена\n";

                            $this->send("Выберите платежную систему: \n$string");

                            $value = ['amount' => $text];
                            $this->updateState($state[0] . '_wallet', $value);
                            break;
                        case 'wallet':
                            if ($text == 0) {
                                $this->send("Вывод отменен");
                                $this->updateState();
                                break;
                            }
                            $systemId = (int)$text;
                            if (!CashoutSystemCore::whereId($systemId)->count()) {
                                $this->send('Вы выбрали несуществующую платежную систему');
                                break;
                            }

                            $this->send('Введите номер для отправление');
                            $value = ['system' => $systemId];
                            $this->updateState($state[0] . '_agree', $value);
                            break;
                        case 'agree':
                            $value = ['wallet' => $text];
                            $systemName = CashoutSystemCore::whereId($stateValue['system'])->first()->name;

                            $this->send("Ваши введенные данные. Подтвердите выплату:\nСумма: {$stateValue['amount']}\nПлатежная система: {$systemName}\nНомер кошелька: {$text}\n \n 1 - Да \n 2 - Нет
                            ");
                            $this->updateState($state[0] . '_withdraw', $value);
                            break;
                        case 'withdraw':
                            if ($text == 1) {

                                DB::beginTransaction();

                                $cashoutCode = CashoutSystemCore::whereId($stateValue['system'])->first()->code;
                                $withdrawInfo = Wallet::withdrawAccess($cashoutCode, $stateValue['amount'], $this->user->id);
                                if (!$withdrawInfo['access']) {
                                    $this->send($withdrawInfo['message']);
                                    DB::rollBack();
                                    $this->updateState();
                                    break;
                                }

                                Users::whereId($this->user->id)->decrement('balance', $withdrawInfo['result_sum']);
                                try {
                                    $send = json_decode(Wallet::send($cashoutCode, $stateValue['wallet'], $stateValue['amount']));
                                    if ($send->desc == 'Payment send') {
                                        $this->send('Деньги отправлены');
                                        DB::commit();
                                    } else {
                                        $this->send('Что-то пошло не так');
                                        DB::rollBack();
                                    }
                                } catch (\Exception $e) {
                                    Log::info($e);
                                    $this->updateState();
                                    break;
                                }
                            } else {
                                $this->send('Операция отменена');
                            }
                            $this->updateState();
                            break;
                    }
                }
                break;
            case 'activate':
                if (isset($state[1])) {
                    switch ($state[1]) {
                        case 'choose':
                            if ($text == 0) {
                                $this->send("Вы отменили активацию");
                                $this->updateState();
                                break;
                            }
                            $markId = (int)$text;
                            if (!MarksInside::whereId($markId)->count()) {
                                $this->send('Вы выбрали несуществующий тариф');
                                break;
                            }

                            $markName = MarksHelper::markName($markId);
                            $markPrice = MarksHelper::markEnter($markId);
                            $value = ['mark' => $text];
                            $this->send("Вы выбрали тариф \"{$markName}\", у вас спишется с баланса: {$markPrice}. Подтвердите покупку. \n 1 - Да \n 2 - Нет");
                            $this->updateState($state[0] . '_agree', $value);
                            break;
                        case 'agree':
                            if ($text == 1) {
                                $activate = new ActivateController();
                                Log::info('mark' . $stateValue['mark']);
                                Log::info('user' . $this->user->id);
                                $message = json_decode($activate->activateQueue((int)$stateValue['mark'], (int)$this->user->id, $this->db)->getContent())->message;
                                $this->send($message);
                            } else {
                                $this->send("Вы отменили активацию");
                            }
                            $this->updateState();
                            break;
                    }
                }
                break;
        }

        // TODO if telegram_state_value == start - call main menu

    }

    public function detectRegisterAction($text)
    {
        $start = explode(' ', $text)[0] ?? '';
        $actionText = explode(' ', $text)[1] ?? '';
        $this->chatId = $this->request['message']['chat']['id'];
        $fromId = $this->request['message']['from']['id'];
        $fromUsername = $this->request['message']['from']['username'];
        $userId = 0;
        if ($start === '/start' and !empty($actionText)) {
            $actionInfo = explode('_', $actionText);
            $action = $actionInfo[0] ?? '';
            $actionValue = $actionInfo[1] ?? '';
            switch ($action) {
                case 'bind':
                    $userCompare = Users::where('login', $actionValue)->first();

                    if (!isset($actionInfo[2]) or $userCompare->telegram_bind_code != $actionInfo[2]) {
                        if (!Users::where('login', $actionValue)->count()) {
                            $this->send(
                                'Привязка аккаунта не удалась. Пользователь не существует.'
                            );
                        }
                        $this->send(
                            'Привязка аккаунта не удалась. Неверный токен.'
                        );

                    } else {
                        Users::where('login', $actionValue)->update([
                            'telegram_id' => $fromId,
                            'telegram_login' => $fromUsername,
                        ]);

                        $keyboard = new \TelegramBot\Api\Types\ReplyKeyboardMarkup([['Главное меню']]);
                        $this->send(
                            'Главное меню',
                            $keyboard
                        );

                        $this->send(
                            'Аккаунт успешно привязан'
                        );
                        $userId = $userCompare->id;
                    }


                    Users::where('login', $actionValue)->update([
                        'telegram_bind_code' => Str::random(24),
                    ]);
                    break;
                case 'refcode':
                    $parent = Users::where('telegram_id', $actionValue)->first();
                    $this->parentId = $parent ? $parent->id : 1;
                    $userId = $this->createUser();


                    $resultMenu = $this->getMenu();
                    $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup($resultMenu['keyboard']);
                    $this->send(
                        Helpers::getResultPage($this->resultMenu['text'], $userId),
                        $keyboard
                    );
                    break;
                default:
                    break;
            }
        } else {
            $userId = $this->createUser();
        }

        if ($userId) {
            $newUser = Users::find($userId);
            $welcome = $newUser->login ?: $newUser->login;
            $this->send('Добро пожаловать, ' . $welcome . '. Отправьте любое сообщение чтобы вызвать главное меню.');
        }
    }

    public function createUser() {

        $user = Users::create([
            'login' => '',
            'email' => '',
            'password' => '',
            'regdate' => now(),
            'active' => '',
            'parent' => 0,
            'refer' => $this->parentId ?? 1,
            'ref_num' => 0,
            'ref_quan' => 0,
            'last_active' => now(),
            'comet_last_active' => time(),
            'privelege' => 1,
            'status' => 'offline',
            'payeer_wallet' => 0,
            'balance' => 0,
            'type_follow' => 0,
            'telegram_bind_code' => Str::random(24),
            'telegram_id' => $this->request['message']['from']['id'],
            'telegram_login' => $this->request['message']['from']['username'],
        ]);

        UsersData::create([
            'id_user' => $user->id,
            'lastname' => '',
            'name' => '',
            'fathername' => '',
            'vk' => '',
            'twitter' => '',
            'facebook' => '',
            'instagram' => '',
            'skype' => '',
            'hide_page' => 0,
            'open_wall' => 0,
            'sex' => 0,
            'telephone' => '',
            'rang' => 1,
            'link_visit' => 0,
            'text_status' => '',
            'bulb' => 0
        ]);

        if ($this->parentId != 1) {
            TelegramBotController::sendNotice(
                $this->parentId,
                "Поздравляем по вашей ссылке зарегистрировался реферал! Его логин в телеграм @{$this->request['message']['from']['username']}",
                $this->db
            );
        }

        return $user->id;
    }

    public function widget($data)
    {

        $answer = explode('_', $data);
        $id = $answer[1] ?? 1;
        $page = $answer[2] ?? 1;
        $item = $answer[3] ?? 0;
        $menu = TelegramMenu::find($id);
        $widget = $menu->widget;
        $widgetSettings = json_decode($menu->widget_settings ?: '{}', JSON_OBJECT_AS_ARRAY);

        $perPage = $widgetSettings['per_page'] ?? 10;
        switch ($widget) {
            case 'tree':
                $params = [
                    'page' => $page,
                    'per_page' => $perPage,
                    'widget_settings' => $widgetSettings ?? []
                ];

                $orders = ReferalTree::getOrdersMark($this->user->id, $params);
                if (!isset($widgetSettings['only_first_page'])) {
                    $count = ReferalTree::getOrdersMarkCount($this->user->id, $params);
                    $this->keyboardPagination = !isset($widgetSettings['only_first_page']) ? $this->pagination($id, $count, $page, $perPage) : null;
                }

                if ($item) {
                    $order = OrdersMarkInside::find($item);
                    $string = "Реферальное дерево для тарифа " . MarksHelper::markName($order->mark);

                    $result = Multimark::createTree1($order->id);
                    $result = [0 => [
                        'id' => $order->id,
                        'user' => $order->id_user,
                        'name' => \App\Libraries\Helpers::convertUs('telegram_login', $order->id_user),
                        'children' => $result
                    ]];

                    $string .= '<pre>' . ReferalTree::treeText($result, $order->mark) . '</pre>';

                    $this->keyboardItems[] = [
                        'text' => "Назад",
                        'callback_data' => "item_{$id}_{$page}"
                    ];
                } else {
                    $string = "<pre>╔" . Helpers::abbreviation('ID', 7, '═') . Helpers::abbreviation('Тариф', 19, '═') .Helpers::abbreviation("Дата", 12, '═', false) . "╗\n";
                    $i = 0;
                    foreach ($orders as $key => $item) {
                        $values['id'] = '╟' . Helpers::abbreviation($item->id, 7);
                        $values['mark'] = Helpers::abbreviation(MarksHelper::markName($item->mark), 19);
                        $values['date'] = Helpers::abbreviation(date("n-j H:i", strtotime($item->date)), 12) . '╢';
                        $string .= implode("", $values) . "\n";

                        $this->keyboardItems[$i][] = [
                            'text' => "{$item->id}",
                            'callback_data' => "item_{$id}_{$page}_{$item->id}"
                        ];
                        if (count($this->keyboardItems[$i]) >= 8) {
                            $i++;
                        }
                    }
                    $string .= '╚' . Helpers::abbreviation('', 38, '═') . '╝</pre>';
                }


                return $string;
            case 'transaction':
                $params = [
                    'page' => $page,
                    'per_page' => $perPage,
                    'widget_settings' => $widgetSettings ?? []
                ];

                $transactions = TransactionController::getTransactions($this->user->id, $params);
                if (!isset($widgetSettings['only_first_page'])) {
                    $count = TransactionController::getTransactionCount($this->user->id, $params);
                    $this->keyboardPagination = !isset($widgetSettings['only_first_page']) ? $this->pagination($id, $count, $page, $perPage) : null;
                }
                $string = "<pre>╔" .
                                Helpers::abbreviation("ID", 7, '═') .
                                Helpers::abbreviation("От_кого", 8, '═') .
                                Helpers::abbreviation("Кому", 7, '═') .
                                Helpers::abbreviation("Сумма", 7, '═') .
                                Helpers::abbreviation("Дата", 12, '═', false) .
                                "╗\n";
                foreach ($transactions as $key => $item) {
                    $amount = $item->info($item->type)->amount ?? 0;
                    $values['id'] = '╟' . Helpers::abbreviation($item->id, 7);
                    $values['from_user'] = Helpers::abbreviation(Helpers::convertUs('telegram_login', $item->from_user), 8);
                    $values['to_user'] = Helpers::abbreviation(Helpers::convertUs('telegram_login', $item->to_user), 7);
                    $values['amount'] = Helpers::abbreviation($amount, 7);
                    $values['date'] = Helpers::abbreviation(date("n-j H:i", strtotime($item->date)), 12) . '╢';
                    $string .= implode("", $values) . "\n";
                }
                $string .= '╚' . Helpers::abbreviation('', 41, '═') . '╝</pre>';

                return $string;

            case 'referrals':
                $params = [
                    'page' => $page,
                    'per_page' => $perPage,
                    'widget_settings' => $widgetSettings ?? [],
                ];

                $referrals = ReferralListController::getReferrals($this->user->id, $params);
                if (!isset($widgetSettings['only_first_page'])) {
                    $count = ReferralListController::getReferralCount($this->user->id, $params);

                    $this->keyboardPagination = !isset($widgetSettings['only_first_page']) ? $this->pagination($id, $count, $page, $perPage) : null;
                }

                $string = "<pre>╔" .
                    Helpers::abbreviation("ID", 7, '═') .
                    Helpers::abbreviation("Логин", 8, '═') .
                    Helpers::abbreviation("Статус", 14, '═') .
                    Helpers::abbreviation("Дата регистрации", 12, '═') .
                    "╗\n";
                foreach ($referrals as $key => $item) {
                    $values['id'] = '╟' . Helpers::abbreviation($item->id, 7);
                    $values['login'] = Helpers::abbreviation($item->login ?: $item->telegram_login, 8);
                    $values['activated'] = Helpers::abbreviation($item->type_follow ? 'Активирован' : 'Не активирован', 14);
                    $values['regdate'] = Helpers::abbreviation(date("n-j H:i", strtotime($item->regdate)), 12) . '╢';
                    $string .= implode("", $values) . "\n";
                }
                $string .= '╚' . Helpers::abbreviation('', 41, '═') . '╝</pre>';
                return $string;

            case 'deposit':
                $this->updateState('deposit_system');
                $string = 'Для пополнения баланса введите сумму';
                return $string;

            case 'withdraw':
                $this->updateState('withdraw_system');
                $string = 'Введите сумму';
                return $string;
            case 'activate':
                $this->updateState('activate_choose');

                $marks = MarksInside::where('status', '>', 1)->get();
                $string = "\n";
                foreach ($marks as $mark) {
                    $string .= $mark['id'] . " - " . $mark['name'] . " - Цена: " . MarksHelper::markEnter($mark['id']) . "\n";
                }
                $string .= "0 - Отмена\n";

                return $string;
            default:
                $this->updateState();
                break;
        }
    }

    public function pagination($widgetId, $count, $page, $perPage)
    {
        $countPages = $count / $perPage;
        $countPages = (int)$countPages + 1;

        if ($countPages < 2) {
            return [];
        }

        $keyboardPagination = [];
        $prevPage = $page - 1;
        $nextPage = $page + 1;

        if ($page > 2) {
            $keyboardPagination[] = [
                    'text' => "\xE2\x8F\xAA 1",
                    'callback_data' => 'item_' . $widgetId . '_1'
            ];
        }

        if ($page > 1) {
            $keyboardPagination [] = [
                'text' => "\xE2\x97\x80 " . $prevPage,
                'callback_data' => 'item_' . $widgetId . '_' . $prevPage,
            ];
        }

        $keyboardPagination[] = [
            'text' => $page,
            'callback_data' => 'item_' . $widgetId . '_' . $page,
        ];

        if ($countPages - $page >= 1) {
            $keyboardPagination[] = [
                'text' => $nextPage . " \xE2\x96\xB6",
                'callback_data' => 'item_' . $widgetId . '_' . $nextPage,
            ];
        }

        if ($countPages - $page >= 2) {
            $keyboardPagination [] = [
                'text' => $countPages . " \xE2\x8F\xA9 ",
                'callback_data' => 'item_' . $widgetId . '_' . $countPages,
            ];
        }

        return $keyboardPagination;
    }

    public function updateState($state = 'start', $value = [])
    {
        if (!empty($value)) {
            $value += json_decode(Users::find($this->user->id)->telegram_state_value, JSON_OBJECT_AS_ARRAY);
        }
        $stateOld = Users::find($this->user->id)->state;
        Users::find($this->user->id)->update([
            'telegram_state' => $state,
            'telegram_state_value' => json_encode($value),
        ]);

//        if ($state == 'start' and $stateOld != 'start') {
//            $this->getMenu();
//        }
    }

    public function getStateValue()
    {
        return json_decode(Users::find($this->user->id)->telegram_state_value, JSON_OBJECT_AS_ARRAY);
    }

    public function send($text, $keyboard = null)
    {
        $this->bot->sendMessage(
            $this->chatId,
            self::cutoff($text),
            'HTML',
            false,
            null,
            $keyboard
        );
    }

    public function editMessage()
    {
        $this->bot->editMessageText(
            $this->chatId,
            $this->messageId,
            self::cutoff(Helpers::getResultPage($this->resultMenu['text'], $this->user->id)),
            'HTML',
            false,
            $this->keyboard
        );
    }

    public function registerAdmin()
    {
        if (empty(Users::find(1)->telegram_id)) {
            if ($this->request['message']['text'] == Users::find(1)->telegram_bind_code) {
                Users::whereId(1)->update([
                    'telegram_id' => $this->telegramId,
                    'telegram_login' => $this->telegramLogin,
                ]);

                $keyboard = new \TelegramBot\Api\Types\ReplyKeyboardMarkup([['Главное меню']]);
                $this->send(
                    'Главное меню',
                    $keyboard
                );

                $this->send(
                    "Этот телеграм аккаунт теперь закреплен как администратор.\nТеперь обновите страницу на которой копировали токен, и можете начать редактирование бота.\nОтправьте любое сообщение для вызова меню."
                );
            } else {
                $this->send(
                    'К боту нужно добавить администратора, чтобы начать работу. Пожалуйста, отправьте токен в сообщении'
                );
            }
            die('OK');
        }
    }

    public static function cutoff($string, $size = 4000)
    {
        $pre = strpos($string, '<pre>') === false ? '' : '</pre>';
        return strlen($string) >= $size ? mb_substr($string, 0, $size, 'utf-8') . "...$pre" : $string;
    }

    public static function sendNotice($userId, $message, $db)
    {
        $telegram = Users::whereId($userId)->first()->telegram_id ?? 0;

        if ($telegram) {
            Config::set("database.connections", [
                "host" => "127.0.0.1",
                "database" => $db,
                "username" => "root",
                "password" => "DrinkVodkaPlayDotka228",
                "driver" => "mysql",
                "charset" => "utf8mb4",
                'strict' => false,
            ]);

            $apiKey = self::getApiKey($db);

            $bot = new \TelegramBot\Api\BotApi($apiKey);

            $bot->sendMessage(
                $telegram,
                $message,
                'HTML'
            );
        }
    }
}