<?php

namespace App\Http\Controllers;

use App\Libraries\Domain;
use App\Libraries\Helpers;
use App\Models\Ad;
use App\Models\CoreSiteList;
use App\Models\SiteInfo;
use App\Models\TelegramCore;
use App\Models\Users;
use App\Models\UsersData;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\Debug\Exception\DummyException;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\TelegramClient;

class TelegramController extends Controller
{


    public static function telegram()
    {

        $domain = new Domain();

        $bot = new \TelegramBot\Api\BotApi('1120419890:AAGWD_OA2vfJdhr37fjoDuCKGpJjbRm94NY');

        http_response_code(200);
        $data = json_decode(file_get_contents('php://input'), TRUE);


        if (isset($data['callback_query'])) {
                $arResult = $data;
                $userId = $arResult['callback_query']['from']['id'];
                $userLogin = $arResult['callback_query']['from']['username'];
                $dataCB = $arResult['callback_query']['data'];

                if ($dataCB == 'testtest_iadminbest') {

                    $buttons = [];

                    array_push($buttons, ["text" => "back", "callback_data" => "/start"]);
                    array_push($buttons, ["text" => "newbutton", "callback_data" => "qweewq"]);

                    $sendData['text'] = 'Я нашел тебя на этих сайтах';

                    $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                        [
                            $buttons
                        ]
                    );

                    $bot->editMessageText($userId, $arResult['callback_query']['message']['message_id'], 'newtext', null, true, $keyboard);


                    return Response::json("OK", 200);
                } else {

                    preg_match_all('/site\_id\:(.*)/',$dataCB, $pos);
                    $siteId = $pos[1][0];

                    $rand = rand(100000, 999999);
                    TelegramCore::where('site_id', $siteId)->where('teleg_login', '@'.$userLogin)->update([
                        'code' => $rand,
                        'date_last_code' => strtotime(now()),
                        'updated_at' => strtotime(now())
                    ]);

                    $bot->sendMessage($userId, "Ваш код для подтверждения: ".$rand);
                    return Response::json("OK", 200);
                }

        } else {
            $sendData['chat_id'] = $data['message']['chat']['id'];
            $message = $data['message']['text'];
            $info = $data['message'];


            $count = TelegramCore::where('teleg_login', '@'.$info['chat']['username'])->count();

            $sendData['chat_id'] = $data['message']['chat']['id'];

            if ($message == '/start' or $message == '/code' or $message == 'код' or $message == 'дай код') {

                if ($count == 1) {

                    $telegInfo = TelegramCore::where('teleg_login', '@'.$info['chat']['username'])->first();
                    TelegramCore::where('teleg_login', '@'.$info['chat']['username'])->update([
                        'teleg_id' => $sendData['chat_id'],
                        'teleg_chat_id' => $sendData['chat_id'],
                    ]);


                    $siteDB = CoreSiteList::where('id', $telegInfo->site_id)->first()['subdomain'];
                    $siteDomain = $domain->resultDomain($telegInfo->site_id);

                    Config::set("database.connections.other", [
                        'driver' => 'mysql',
                        "host" => "localhost",
                        "database" => $siteDB,
                        "username" => "root",
                        "password" => "DrinkVodkaPlayDotka228"
                    ]);

                    $user = Users::on('other')->where('id', $telegInfo->user_id)->first();

                    if ($message == '/start') {
                        $sendData['text'] = "Привет! Кажется я тебя знаю! Так-так-так... посмотрим... Да, точно! Ты ".$user->login.", на сайте $siteDomain правильно?";
                        $bot->sendMessage($sendData['chat_id'], $sendData['text']);
                    }
                    TelegramController::sendCodeArr(['site_id' => $telegInfo->site_id, 'user_id' => $user->id]);

                    return Response::json("OK", 200);


                } elseif ($count > 1) {

                    $telegInfo = TelegramCore::where('teleg_login', '@'.$info['chat']['username'])->get();

                    TelegramCore::where('teleg_login', '@'.$info['chat']['username'])->update([
                        'teleg_id' => $sendData['chat_id'],
                        'teleg_chat_id' => $sendData['chat_id'],
                    ]);

                    $buttons = [];

                    foreach ($telegInfo as $key => $ti) {

                        $siteDB = CoreSiteList::where('id', $ti->site_id)->first()['subdomain'];
                        $siteDomain = $domain->resultDomain($ti->site_id);

                        Config::set("database.connections.other", [
                            'driver' => 'mysql',
                            "host" => "localhost",
                            "database" => $siteDB,
                            "username" => "root",
                            "password" => "DrinkVodkaPlayDotka228"
                        ]);

                        $siteInfo = substr(SiteInfo::on('other')->first()->name, 0, 20);

                        $user = Users::on('other')->where('id', $ti->user_id)->first();

                        array_push($buttons, ["text" => "$siteInfo", "callback_data" => "site_id:".$ti->site_id]);

                        DB::disconnect('other');

                    }

                    $sendData['text'] = 'Я нашел тебя на этих сайтах';

                    $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                        [
                            $buttons
                        ]
                    );

                    $bot->sendMessage($sendData['chat_id'], $sendData['text'], null, false, null, $keyboard);

                    return Response::json("OK", 200);


                } else {

                    $sendData['text'] = 'Я не нашел тебя ни на одном подвластном мне сервисе';
                    $bot->sendMessage($sendData['chat_id'], $sendData['text']);

                    return Response::json("OK", 200);

                }


            } else {

                $sendData['text'] = 'Bye bro';
                $bot->sendMessage($sendData['chat_id'], $sendData['text']);

                return Response::json("OK", 200);
            }
        }
    }

    public function telegramAdd(Request $request) {


        $site = Helpers::getSiteId();
        $user = Auth::id();

        $telegCount = TelegramCore::where('site_id', $site)->where('user_id', $user)->where('status', 1)->count();



        if (!$telegCount and isset($request->teleg_login)) {
            TelegramCore::where('site_id', $site)
                ->where('user_id', $user)
                ->where('status', 0)
                ->delete();

            TelegramCore::create([
                'status' => 0,
                'teleg_login' => $request->teleg_login,
                'teleg_id' => 0,
                'teleg_chat_id' => 0,
                'user_id' => Auth::id(),
                'site_id' => Helpers::getSiteId(),
                'code' => rand(100000, 999999),
                'date_last_code' => strtotime(now()),
                'notice' => 1,
                'system_notice' => 1,
                'created_at' => strtotime(now()),
                'updated_at' => strtotime(now())
            ]);
        }
    }

    public static function telegramConfirm(Request $request) {

        $site = Helpers::getSiteId();
        $user = Auth::id();

        $teleg = TelegramCore::where('site_id', $site)->where('user_id', $user);
        $telegAr = $teleg->first();
        if ($request->iauth_code == $telegAr->code and $request->iauth_code != 0) {

            $teleg->update([
                'status' => 1,
            ]);

            UsersData::where('id_user', $user)->update([
                'telegram' => $telegAr->teleg_login,
            ]);

            return Response::json([
                'type' => 'success',
                'message' => 'Телеграм аккаунт успешно привязан',
            ], 200);
        } else {
            Helpers::iAuth('reset');
            return Response::json([
                'type' => 'error',
                'message' => 'Код введен неверно. Получите новый код.',
            ], 200);
        }
    }

    public static function telegramUnlink(Request $request) {

        $iAuthError = Helpers::iAuth('check_code', $request->iauth_code);
        if ($iAuthError) {
            return Response::json([
                "type" => "error",
                "message" => $iAuthError,
            ], 200);
        }

        $site = Helpers::getSiteId();
        $user = Auth::id();

        $teleg = TelegramCore::where('site_id', $site)->where('user_id', $user)->first();


        TelegramCore::where('id', $teleg->id)->delete();
        UsersData::where('id_user', $user)->update(['telegram' => '']);

        return Response::json([
            'type' => 'warning',
            'message' => "Телеграм $teleg->teleg_login отключен от вашего аккаунта",
        ], 200);
    }

    public static function telegramNoticeEdit(Request $request) {

        $iAuthError = Helpers::iAuth('check_code', $request->iauth_code);
        if ($iAuthError) {
            return Response::json([
                "type" => "error",
                "message" => $iAuthError,
            ], 200);
        }

        $site = Helpers::getSiteId();
        $user = Auth::id();
        $notice = $request->notice ?? 1;
        $systemNotice = $request->system_notice ?? 1;


        TelegramCore::where('site_id', $site)->where('user_id', $user)->update([
                'notice' => $notice,
                'system_notice' => $systemNotice,
            ]);

        return Response::json([
            'type' => 'success',
            'message' => "Настройки оповещений изменены",
        ], 200);
    }

    public static function sendCode(Request $request, $data = []) {

        $siteId = $request->site_id;
        $userId = $request->user_id;

        $count = TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->count();
        if ($count) {
            $rand = rand(100000,999999);
            TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->update(['code' => $rand]);
            $teleg = TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->first();

            $bot = new \TelegramBot\Api\BotApi('1120419890:AAGWD_OA2vfJdhr37fjoDuCKGpJjbRm94NY');
            $bot->sendMessage($teleg->teleg_id, "Держи код для подтверждения: ".$rand." только тссс... никому его не говори, даже мне.");
        }

    }

    public static function sendCodeArr($data = []) {

        $siteId = $data['site_id'];
        $userId = $data['user_id'];

        $count = TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->count();
        if ($count) {
            $rand = rand(100000,999999);
            TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->update(['code' => $rand]);
            $teleg = TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->first();

            $bot = new \TelegramBot\Api\BotApi('1120419890:AAGWD_OA2vfJdhr37fjoDuCKGpJjbRm94NY');
            $bot->sendMessage($teleg->teleg_id, "Держи код для подтверждения: ".$rand." только тссс... никому его не говори, даже мне.");
        }

    }

}