<?php

namespace App\Http\Controllers;

use App\Models\Tickets;
use App\Models\TicketsAnswer;
use Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;

class TicketsController extends Controller
{
    public static function all(Request $request)
    {
        if ($request['my'] == 'Y')
            $tickets = Tickets::where('id_user', Auth::id())->paginate(20);
        else
            $tickets = Tickets::paginate(20);

        return view('templates/tickets/tickets', array(
            'tickets' => $tickets
        ));
    }

    public static function view($id)
    {
        $ticket = Tickets::where('id_ticket', '=', $id)->get();
        $answers = TicketsAnswer::where('id_ticket', '=', $id)->paginate(20);

        return view('templates/tickets/ticket', array(
            'ticket' => $ticket[0],
            'answers' => $answers
        ));
    }

    public static function addTicket()
    {
        return view('templates/tickets/ticketsAdd');
    }

    public static function addAnswer(Request $request)
    {

        $request->validate([
            'text' => 'required'
        ], [
            'text.required' => 'Сообщение не может быть пустым'
        ]);

        $answer = TicketsAnswer::create([
            'id_user' => Auth::id(),
            'id_ticket' => $request->id_ticket,
            'text' => $request->text,
            'rating' => 0,
            'date' => now()
        ]);

        return Response::json([
            "success" => true,
            "message" => "Ответ успешно отправлен"

        ], 200);

        return redirect()->route('addAnswer', ['id' => $request->id_ticket]);
    }

    public static function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'text' => 'required'
        ], [
            'name.required' => 'Заполните Название',
            'text.lte' => 'Укажите суть вопроса'
        ]);


        $ticket = Tickets::create([
            'id_user' => Auth::id(),
            'name_ticket' => $request->name,
            'text' => $request->text,
            'date' => now(),
            'open' => 1,
            'hide' => 1,
            'rating' => 0,
            'ticket_view' => 0
        ]);

        return Response::json([
            "success" => true,
            "message" => "Тикет успешно создан",
            "link" => route('goToTicket', ['id' => $ticket->id])
        ], 200);
    }

}
