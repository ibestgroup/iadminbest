<?php
namespace App\Http\Controllers;

use App\Models\Cashout;
use App\Models\SiteInfo;
use App\Models\Transaction;
use App\Models\Users;
use App\Models\PaymentsOrder;
use App\Models\Payments;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;

class TransactionController extends Controller {

    public static $widgetSettings;

    public static function all(Request $request) {

        $filter['payment'] = Input::get('payment');
        $filter['cashout'] = Input::get('cashout');
        $filter['transaction'] = Input::get('transaction');

        $payments = Payments::where('payment_user', Auth::id())->get()->toArray();
        $cashout = Cashout::where('id_user', Auth::id())->get()->toArray();
        $paymentsOrder = PaymentsOrder::where('from_user', Auth::id())->orWhere('to_user', Auth::id())->get()->toArray();

        $siteInfo =  SiteInfo::find(1);

        if ($filter['payment'] or $filter['cashout'] or $filter['transaction']) {
            if ($filter['payment'] != 'Y')
                $payments = array();
            if ($filter['cashout'] != 'Y')
                $cashout = array();
            if ($filter['transaction'] != 'Y')
                $paymentsOrder = array();
        }

        $transaction = array_merge($payments, $cashout, $paymentsOrder);

        foreach ($transaction as $key => $value) {
            $count = count($value);
            if ($count == 5) {
                $transaction[$key]['table'] = 'payment';
            } elseif ($count == 9) {
                $transaction[$key]['table'] = 'cashout';
            } else {
                $transaction[$key]['table'] = 'payments_order';
            }
            $transaction[$key]['date'] = strtotime($value['date']);
        }

        $date=array();
        foreach($transaction as $key=>$arr){
            $date[$key]=$arr['date'];
        }

        array_multisort($date, SORT_DESC, $transaction);

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($transaction);
        $perPage = 20;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());

        return view('templates.sites.transaction', ['items' => $paginatedItems, 'siteInfo' => $siteInfo, 'filter' => $filter]);
    }

    public static function transactionsQuery($userId, $params = [])
    {
        self::$widgetSettings = $params['widget_settings'] ?? [];
        $transactions = Transaction::where(function ($q) use ($userId) {
                                    $q->where('from_user', $userId)
                                        ->orWhere('to_user', $userId);
                                    })->where('type', self::$widgetSettings['type']);

        return $transactions;
    }

    public static function getTransactions($userId, $params = [])
    {
        $transactions = self::transactionsQuery($userId, $params);

        if (isset($params['page']) and isset(self::$widgetSettings['per_page'])) {
            if (!isset(self::$widgetSettings['only_first_page'])) {
                $transactions = $transactions->skip(($params['page'] - 1) * self::$widgetSettings['per_page']);
            }
            $transactions = $transactions->limit(self::$widgetSettings['per_page']);
        }

        return $transactions->get();
    }

    public static function getTransactionCount($userId, $params)
    {
        return self::transactionsQuery($userId, $params)->count();
    }

}