<?php

namespace App\Http\Controllers;

use App\Libraries\Domain;
use App\Libraries\Helpers;
use App\Models\Ad;
use App\Models\CoreSiteList;
use App\Models\SiteInfo;
use App\Models\TelegramCore;
use App\Models\Users;
use App\Models\UsersData;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use phpDocumentor\Reflection\Types\Object_;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\Debug\Exception\DummyException;
use Telegram\Bot\Api;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\TelegramClient;

class VkController extends Controller
{

    public function __construct() {
        define('CALLBACK_API_CONFIRMATION_TOKEN', 'b8ef332d'); // Строка, которую должен вернуть сервер
        define('VK_API_ACCESS_TOKEN', '4cc0f0fa2ce8ab9b04395cce19e6a1ecfbc76f68c678edc847f9bf3e073372f5eb00f90fd204deaf8c6a6'); // Ключ доступа сообщества

        define('CALLBACK_API_EVENT_CONFIRMATION', 'confirmation'); // Тип события о подтверждении сервера
        define('CALLBACK_API_EVENT_MESSAGE_NEW', 'message_new'); // Тип события о новом сообщении
        define('CALLBACK_API_EVENT_MESSAGE_REPLY', 'message_reply'); // Тип события о новом сообщении
        define('VK_API_ENDPOINT', 'https://api.vk.com/method/'); // Адрес обращения к API
        define('VK_API_VERSION', '5.89'); // Используемая версия API

    }

    public function getVkId(Request $request) {
        $url = $request->url;
        $pageVk = file_get_contents($url);
        preg_match('~<a href="/photo(.*?)_~', $pageVk, $matches);
        $result = [
            'vk_id' => $matches[1]
        ];
        return $result;
    }

    public static function testVk() {


        $message = [];
        $message['body'] = 'https://pereliv.iadmin.work';
        $message['user_id'] = 527512598;
        $peer_id = 527512598;

        $count = TelegramCore::where('vk_id', 527512598)->count();
        $rand = rand(100000, 999999);

        if ($count == 1) {
            $telegInfo = TelegramCore::where('vk_id', 527512598)->first();
            TelegramCore::where('vk_id', $message['user_id'])->update([
                'vk_code' => $rand,
            ]);

            $siteDB = CoreSiteList::where('id', $telegInfo->site_id)->first()['subdomain'];

            Config::set("database.connections.other", [
                'driver' => 'mysql',
                "host" => "localhost",
                "database" => $siteDB,
                "username" => "root",
                "password" => "DrinkVodkaPlayDotka228"
            ]);

            $user = Users::on('other')->where('id', $telegInfo->user_id)->first();


            $sendData['text'] = "Привет, $user->login! Твой код подтверждения: $rand";
            self::sendMessageVk($message['user_id'], $sendData['text']);

        } elseif ($count > 1) {
            $text = $message['body'];
            $siteInfo = (array)json_decode(@file_get_contents($text.'/get-site-info'));
            if (isset($siteInfo['site_id'])) {
                $iAuth = TelegramCore::where('site_id', $siteInfo['site_id'])->where('vk_id', $message['user_id'])->where('vk_status', 1);
                $iAuthCount = $iAuth->count();
                $iAuthRec = $iAuth->first();
                if ($iAuthCount) {
                    $user = Users::where('id', $iAuthRec->user_id)->first();
                    $iAuth->update([
                        'vk_code' => $rand,
                        'date_last_code' => strtotime(now()),
                        'updated_at' => strtotime(now())
                    ]);
                    $sendData['text'] = "Привет, $user->login! Твой код подтверждения: $rand для сайта $siteInfo[name]";
                    self::sendMessageVk($message['user_id'], $sendData['text']);
                }
            } else {
                $buttonsStart = '{"one_time": false,"inline": true,"buttons": [[';
                $iAuthArray = TelegramCore::where('vk_id', 527512598)->get();
                $buttons = '';
                foreach ($iAuthArray as $item) {
                    $domain = new Domain();
                    $siteUrl = $domain->resultDomain($item->site_id);
                    $buttons .= '{"action": {"type": "text","payload": "{\"site_id\":\"'.$siteUrl.'\"}","label": "'.$siteUrl.'"},"color": "secondary"},';
                }
                $buttons = substr($buttons, 0, -1);
                $buttonsEnd = ']]}';
                $buttons = $buttonsStart.$buttons.$buttonsEnd;
                $sendData['text'] = "Привет! Я нашел тебя на нескольких сайтах. Выбери нужный.";
                self::sendMessageVk($message['user_id'], $sendData['text'], $buttons);
            }
        } else {
            self::sendMessageVk($message['user_id'], "Я не нашел тебя =(");
        }

//        self::sendMessageVk($peer_id, "Hello world! (peer_id: {$peer_id})");
        echo('ok');





        exit();

        $userId = '26871600';

        $array['user_id'] = $userId;
        $array['peer_id'] = $userId;
        $array['message'] = 'Привет!'.$userId;

        $buttons = '{"one_time": false,"inline": true,"buttons": [[{"action": {"type": "text","payload": "{\"button\":\"btn_1\"}","label": "1233123"},"color": "secondary"}]]}';

//        self::send_message(527512598, 'hello', $buttons);
//        exit('ok');

//        dd($buttons);
        $sendData['text'] = "Привет! Я нашел тебя на нескольких сайтах. Выбери нужный.";
//        self::sendMessageVk(527512598, $sendData['text'], $buttons);

        self::send_message(527512598, $sendData['text'], $buttons);
    }

    public static function send_message($peer_id, $message, $array = '') {



        self::api('messages.send', array(
            'peer_id' => $peer_id,
            'message' => $message,
            'keyboard' => $array
        ));
    }

    public function generateButtons(array $buttons, bool $inline = false) : string {
        $array = [
            'one_time' => !$inline,
            'inline' => $inline,
            'buttons' => [],
        ];

        foreach ($buttons as $item) {
            $array['buttons'][][] = [
                'action' => [
                'type' => $item['type'],
                'payload' => json_encode($item['payload'], JSON_UNESCAPED_UNICODE),
                'label' => $item['label'],
            ],
                'color' => $item['color'],
            ];
        }

        return json_encode($array, JSON_UNESCAPED_UNICODE);
    }

    public static function api($method, $params) {
        $params['access_token'] = VK_API_ACCESS_TOKEN;
        $params['v'] = VK_API_VERSION;
        $query = http_build_query($params);
        $url = VK_API_ENDPOINT . $method . '?' . $query;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_exec($curl);
        curl_close($curl);
    }

    public static function vk()
    {

        $event = json_decode(file_get_contents('php://input'), true);

        switch ($event['type']) {
            // Подтверждение сервера
            case CALLBACK_API_EVENT_CONFIRMATION:
                echo(CALLBACK_API_CONFIRMATION_TOKEN);
                break;
            // Получение нового сообщения
            case CALLBACK_API_EVENT_MESSAGE_NEW:
                $message = $event['object'];
                $peer_id = $message['peer_id'] ?: $message['user_id'];
                self::send_message($peer_id, "Hello world123! (peer_id: {$peer_id})");
                echo('ok');
                break;
            case CALLBACK_API_EVENT_MESSAGE_REPLY:
                $message = $event['object'];
                $peer_id = $message['peer_id'] ?: $message['user_id'];
                self::send_message($peer_id, "Hello world321s! (peer_id: {$peer_id})");
                echo('ok');
                break;
            default:
                echo('ok');
                break;
        }


//        $confirmation = 'b8ef332d';
//        $token = '4cc0f0fa2ce8ab9b04395cce19e6a1ecfbc76f68c678edc847f9bf3e073372f5eb00f90fd204deaf8c6a6';
//
//        $data = (array)$request->all();
//
//        switch ($data['type']) {
//            case 'confirmation':
//                return $confirmation;
//                break;
//            case 'message_new':
//
//
//                $userId = $data['object']['user_id'];
//
////                $userInfo = json_decode(file_get_contents("https://api.vk.com/method/users.get?user_ids=$userId&v=5.0"));
////                $userName = $userInfo->response[0]->first_name;
//
//                $message = $data['object']['body'];
//                $messagesArray = [
//                    'Привет' => "Привет, asd!",
//                    'Пока' => "Пока, sad!",
//                    'Ок' => "кО",
//                ];
//
//                foreach ($messagesArray as $key => $value) {
//                    if ($message == $key) {
//                        $answer = $value;
//                    }
//                }
//
//                $params = [
//                    'message' => $answer,
//                    'peer_id' => $userId,
//                    'access_token' => $token,
//                    'v' => '5.67',
//                ];
//
//
//                $query = http_build_query($params);
////                $url = 'https://api.vk.com/method/message.send?'.$query;
//
////                $curl = curl_init($url);
////                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
////                $json = curl_exec($curl);
////                $error = curl_error($curl);
////                if ($error) {
////                    log_error($error);
////                    throw new Exception("Failed message.send request");
////                }
//
////                curl_close($curl);
//
////                $response = json_decode($json, true);
////                if (!$response || !isset($response['response'])) {
////                    log_error($json);
////                    throw new Exception("Invalid response for message.send request");
////                }
////
////                return $response['response'];
//
//
//
////                $getParams = http_build_query($requestParams);
//                file_get_contents('https://api.vk.com/method/message.send?'.$query);
//
//
//                echo 'ok';
//                exit();
//                break;
//        }
//
//
//        echo 'ok';
//        exit();


//        return '1465bea0';

        $domain = new Domain();

        $bot = new \TelegramBot\Api\BotApi('1120419890:AAGWD_OA2vfJdhr37fjoDuCKGpJjbRm94NY');

        http_response_code(200);


        if (isset($data['callback_query'])) {
                $arResult = $data;
                $userId = $arResult['callback_query']['from']['id'];
                $userLogin = $arResult['callback_query']['from']['username'];
                $dataCB = $arResult['callback_query']['data'];

                if ($dataCB == 'testtest_iadminbest') {

                    $buttons = [];

                    array_push($buttons, ["text" => "back", "callback_data" => "/start"]);
                    array_push($buttons, ["text" => "newbutton", "callback_data" => "qweewq"]);

                    $sendData['text'] = 'Я нашел тебя на этих сайтах';

                    $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                        [
                            $buttons
                        ]
                    );

                    $bot->editMessageText($userId, $arResult['callback_query']['message']['message_id'], 'newtext', null, true, $keyboard);


                    return Response::json("OK", 200);
                } else {

                    preg_match_all('/site\_id\:(.*)/',$dataCB, $pos);
                    $siteId = $pos[1][0];

                    $rand = rand(100000, 999999);
                    TelegramCore::where('site_id', $siteId)->where('teleg_login', '@'.$userLogin)->update([
                        'code' => $rand,
                        'date_last_code' => strtotime(now()),
                        'updated_at' => strtotime(now())
                    ]);

                    $bot->sendMessage($userId, "Ваш код для подтверждения: ".$rand);
                    return Response::json("OK", 200);
                }

        } else {
            $sendData['chat_id'] = $data['message']['chat']['id'];
            $message = $data['message']['text'];
            $info = $data['message'];


            $count = TelegramCore::where('teleg_login', '@'.$info['chat']['username'])->count();

            $sendData['chat_id'] = $data['message']['chat']['id'];

            if ($message == '/start' or $message == '/code' or $message == 'код' or $message == 'дай код') {

                if ($count == 1) {

                    $telegInfo = TelegramCore::where('teleg_login', '@'.$info['chat']['username'])->first();
                    TelegramCore::where('teleg_login', '@'.$info['chat']['username'])->update([
                        'teleg_id' => $sendData['chat_id'],
                        'teleg_chat_id' => $sendData['chat_id'],
                    ]);


                    $siteDB = CoreSiteList::where('id', $telegInfo->site_id)->first()['subdomain'];
                    $siteDomain = $domain->resultDomain($telegInfo->site_id);

                    Config::set("database.connections.other", [
                        'driver' => 'mysql',
                        "host" => "localhost",
                        "database" => $siteDB,
                        "username" => "root",
                        "password" => "DrinkVodkaPlayDotka228"
                    ]);

                    $user = Users::on('other')->where('id', $telegInfo->user_id)->first();

                    if ($message == '/start') {
                        $sendData['text'] = "Привет! Кажется я тебя знаю! Так-так-так... посмотрим... Да, точно! Ты ".$user->login.", на сайте $siteDomain правильно?";
                        $bot->sendMessage($sendData['chat_id'], $sendData['text']);
                    }
                    TelegramController::sendCodeArr(['site_id' => $telegInfo->site_id, 'user_id' => $user->id]);

                    return Response::json("OK", 200);


                } elseif ($count > 1) {

                    $telegInfo = TelegramCore::where('teleg_login', '@'.$info['chat']['username'])->get();

                    TelegramCore::where('teleg_login', '@'.$info['chat']['username'])->update([
                        'teleg_id' => $sendData['chat_id'],
                        'teleg_chat_id' => $sendData['chat_id'],
                    ]);

                    $buttons = [];

                    foreach ($telegInfo as $key => $ti) {

                        $siteDB = CoreSiteList::where('id', $ti->site_id)->first()['subdomain'];
                        $siteDomain = $domain->resultDomain($ti->site_id);

                        Config::set("database.connections.other", [
                            'driver' => 'mysql',
                            "host" => "localhost",
                            "database" => $siteDB,
                            "username" => "root",
                            "password" => "DrinkVodkaPlayDotka228"
                        ]);

                        $siteInfo = substr(SiteInfo::on('other')->first()->name, 0, 20);

                        $user = Users::on('other')->where('id', $ti->user_id)->first();

                        array_push($buttons, ["text" => "$siteInfo", "callback_data" => "site_id:".$ti->site_id]);

                        DB::disconnect('other');

                    }

                    $sendData['text'] = 'Я нашел тебя на этих сайтах';

                    $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                        [
                            $buttons
                        ]
                    );

                    $bot->sendMessage($sendData['chat_id'], $sendData['text'], null, false, null, $keyboard);

                    return Response::json("OK", 200);


                } else {

                    $sendData['text'] = 'Я не нашел тебя ни на одном подвластном мне сервисе';
                    $bot->sendMessage($sendData['chat_id'], $sendData['text']);

                    return Response::json("OK", 200);

                }


            } else {

                $sendData['text'] = 'Bye bro';
                $bot->sendMessage($sendData['chat_id'], $sendData['text']);

                return Response::json("OK", 200);
            }
        }
    }

    public function telegramAdd(Request $request) {


        $site = Helpers::getSiteId();
        $user = Auth::id();

        $telegCount = TelegramCore::where('site_id', $site)->where('user_id', $user)->where('status', 1)->count();

        if (!$telegCount and isset($request->teleg_login)) {
            TelegramCore::where('site_id', $site)
                ->where('user_id', $user)
                ->where('status', 0)
                ->delete();

            TelegramCore::create([
                'status' => 0,
                'teleg_login' => $request->teleg_login,
                'teleg_id' => 0,
                'teleg_chat_id' => 0,
                'user_id' => Auth::id(),
                'site_id' => Helpers::getSiteId(),
                'code' => rand(100000, 999999),
                'date_last_code' => strtotime(now()),
                'notice' => 1,
                'system_notice' => 1,
                'created_at' => strtotime(now()),
                'updated_at' => strtotime(now())
            ]);
        }
    }

    public static function telegramConfirm(Request $request) {

        $site = Helpers::getSiteId();
        $user = Auth::id();

        $teleg = TelegramCore::where('site_id', $site)->where('user_id', $user);
        $telegAr = $teleg->first();
        if ($request->iauth_code == $telegAr->code and $request->iauth_code != 0) {

            $teleg->update([
                'status' => 1,
            ]);

            UsersData::where('id_user', $user)->update([
                'telegram' => $telegAr->teleg_login,
            ]);

            return Response::json([
                'type' => 'success',
                'message' => 'Телеграм аккаунт успешно привязан',
            ], 200);
        } else {
            Helpers::iAuth('reset');
            return Response::json([
                'type' => 'error',
                'message' => 'Код введен неверно. Получите новый код.',
            ], 200);
        }
    }

    public static function telegramUnlink(Request $request) {

        $iAuthError = Helpers::iAuth('check_code', $request->iauth_code);
        if ($iAuthError) {
            return Response::json([
                "type" => "error",
                "message" => $iAuthError,
            ], 200);
        }

        $site = Helpers::getSiteId();
        $user = Auth::id();

        $teleg = TelegramCore::where('site_id', $site)->where('user_id', $user)->first();


        TelegramCore::where('id', $teleg->id)->delete();
        UsersData::where('id_user', $user)->update(['telegram' => '']);

        return Response::json([
            'type' => 'warning',
            'message' => "Телеграм $teleg->teleg_login отключен от вашего аккаунта",
        ], 200);
    }

    public static function telegramNoticeEdit(Request $request) {

        $iAuthError = Helpers::iAuth('check_code', $request->iauth_code);
        if ($iAuthError) {
            return Response::json([
                "type" => "error",
                "message" => $iAuthError,
            ], 200);
        }

        $site = Helpers::getSiteId();
        $user = Auth::id();
        $notice = $request->notice ?? 1;
        $systemNotice = $request->system_notice ?? 1;


        TelegramCore::where('site_id', $site)->where('user_id', $user)->update([
                'notice' => $notice,
                'system_notice' => $systemNotice,
            ]);

        return Response::json([
            'type' => 'success',
            'message' => "Настройки оповещений изменены",
        ], 200);
    }

    public static function sendCode(Request $request, $data = []) {

        $siteId = $request->site_id;
        $userId = $request->user_id;

        $count = TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->count();
        if ($count) {
            $rand = rand(100000,999999);
            TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->update(['code' => $rand]);
            $teleg = TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->first();

            $bot = new \TelegramBot\Api\BotApi('1120419890:AAGWD_OA2vfJdhr37fjoDuCKGpJjbRm94NY');
            $bot->sendMessage($teleg->teleg_id, "Держи код для подтверждения: ".$rand." только тссс... никому его не говори, даже мне.");
        }

    }

    public static function sendCodeArr($data = []) {

        $siteId = $data['site_id'];
        $userId = $data['user_id'];

        $count = TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->count();
        if ($count) {
            $rand = rand(100000,999999);
            TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->update(['code' => $rand]);
            $teleg = TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->first();

            $bot = new \TelegramBot\Api\BotApi('1120419890:AAGWD_OA2vfJdhr37fjoDuCKGpJjbRm94NY');
            $bot->sendMessage($teleg->teleg_id, "Держи код для подтверждения: ".$rand." только тссс... никому его не говори, даже мне.");
        }

    }

    public static function sendMessageVk($peer_id, $message, $array = '') {
        self::api('messages.send', array(
            'peer_id' => $peer_id,
            'message' => $message,
            'keyboard' => $array,
        ));
    }
}