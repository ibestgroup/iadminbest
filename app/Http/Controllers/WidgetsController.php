<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\Users;
use App\Models\SiteInfo;
use App\Models\OrdersMarkInside;
use App\Models\OrdersMark;
use App\Models\MarksInside;
use App\Models\Marks;
use App\Models\Friends;
use App\Models\PaymentsOrder;
use Illuminate\Support\Facades\Auth;
use App\Libraries\Helpers;

class WidgetsController extends Controller
{
    /**
     * в массиве определяем метод который вызывается для данного виджета,
     * в ctype указывается тип React компонента на клиенте
     */
    const WIDGETS = [
        1  => [ 'method' => 'mkAvatar', 'ctype' => 'UserCard'] ,
        2  => [ 'method' => 'mkBalance', 'ctype' => 'SmallCard'] ,
        3  => [ 'method' => 'mkReferals', 'ctype' => 'UsersList'] ,
        4  => [ 'method' => 'mkLastOperations', 'ctype' => 'TableCard'] ,
        5  => [ 'method' => 'mkWallet', 'ctype' => 'SmallCard'] ,
        6  => [ 'method' => 'mkPlans', 'ctype' => 'TableCard'] ,
        7  => [ 'method' => 'mkParentReferal', 'ctype' => 'SmallCard'] ,
        8  => [ 'method' => 'mkCurators', 'ctype' => 'TableCard'] ,
        9  => [ 'method' => '', 'ctype' => 'SmallCard'] ,
        10 => [ 'method' => 'mkFriends', 'ctype' => 'UsersList'] ,
        11 => [ 'method' => 'mkNews', 'ctype' => 'SmallCard'] ,
        12 => [ 'method' => 'mkLinkVisits', 'ctype' => 'SmallCard'] ,
        13 => [ 'method' => 'mkReferalsCount', 'ctype' => 'SmallCard'] ,
        14 => [ 'method' => '', 'ctype' => 'SmallCard'] ,
    ];

    public function list($id){
        // id не пригодился, я так понимаю профайл другого юзера будет непросто достать
        // dd($id);

        $widgets = Profile::all();

        foreach ($widgets as &$widget) {
            $widget['arr_values'] = unserialize($widget['arr_values']);
            $widget['ctype'] = self::WIDGETS[$widget['id']]['ctype'];
        }

        return $widgets;
    }

    public function specific($uid, $id){

        $widget = Profile::where('id', $id)->get();
        $widgetData = false;

        if (!is_null($widget)) {
            // подтягиваем имя функции из справочника
            $funcName = self::WIDGETS[$id]['method'];

            // клеим полный массив данных юзера
            $user = Users::find($uid);

            if (!is_null($user)) {
                $userData = $user->users_data()->first()->toArray();
                unset($userData['id']);
                $user = array_merge($user->toArray(), $userData);

                // инвоким соответствующий метод
                $widgetData = self::$funcName($user);
            }
        }

        return $widgetData;
    }

    public static function mkAvatar($userData) {
        $siteInfo = SiteInfo::first()['mark_type'];

        $uid = $userData['id'];

        if ($siteInfo === 3) {
            // количество маркетингов без автореинвеста
            $mark = OrdersMarkInside::where('id_user', $uid)->count();
        }
        elseif($siteInfo === 4){
            // количество маркетингов с автореинвестом
            $mark = OrdersMarkInside::where('id_user', $uid)->count();
        }

        // дергаем количество друзей пользователя
        $friends = Friends::where('fromid' , $uid)->where('status', 1)->count();

        //
        $payouts = PaymentsOrder::getPayments($uid);
        $payouts = round($payouts);

        $widgetData = [
            'name' => join([$userData['name'], $userData['lastname']], ' '),
            'avatar' => '/laravel/storage/app/'.$userData['avatar'],
            'marketings' => $mark,
            'friends' => $friends,
            'payouts' => $payouts,
            'current' => Helpers::getCurrent(SiteInfo::find(1)['value'], 'iconClass'),
        ];

        return $widgetData;
    }

    public static function mkBalance($userData) {
        
        $widgetData = [
            'title' => 'Текущий баланс', // поле не используется на клиенте
            'value' => $userData['balance'],
            'link'  => '/profile/balance', // TODO: сменить на актуальную ссылку на пополнение
            'link_text' => 'пополнить баланс',
            'color' => 'bg-green',
            'current' => Helpers::getCurrent(SiteInfo::find(1)['value'], 'iconClass'),
        ];

        return $widgetData;
    }

    public static function mkEarned($userData) {

        $payouts = PaymentsOrder::getPayments($userData['id']);

        $widgetData = [
            'title' => 'Заработано', // fallback - значение
            'value' => $payouts,
            'link'  => '/link/', // TODO: сменить на актуальную ссылку на пополнение
            // 'link_text' => '',
            'color' => 'bg-green',
            'current' => Helpers::getCurrent(SiteInfo::find(1)['value'], 'iconClass')
        ];

        return $widgetData;
    }

    public static function mkLastOperations($userData) {
        $uid = $userData['id'];
        $payments = PaymentsOrder::select('*')->where('to_user', $uid)->orWhere('from_user', $uid)->take(10)->get();

        $rows = [];
        foreach ($payments as $payment) {
            $rows[] = [
                'sender' => [
                    'link' => '/profile/' . $payment['from_user'],
                    'title' => $payment->sender['login'],
                ],
                'reciever' => [
                    'link' => '/profile/' . $payment['to_user'],
                    'title' => $payment->reciever['login'],
                ],
                'amount' => $payment['amount'],
                'date' => $payment['date'],
            ];
        }

        $widgetData = [
            'title' => 'Последние операции', // fallback - значение
            'rows' => $rows,
            'headers' => ['Отправитель', 'Получатель', 'Сумма', 'Дата'],
            'link'  => '/profile/transaction', // TODO: сменить на актуальную ссылку на пополнение
            // 'link_text' => '',
            'color' => 'warning',
            'current' => Helpers::getCurrent(SiteInfo::find(1)['value'], 'iconClass'),
        ];

        return $widgetData;
    }


    public static function mkCurators($userData) {
        $siteInfo = SiteInfo::first()['mark_type'];

        $uid = $userData['id'];

        if ($siteInfo === 3) {
            // количество маркетингов без автореинвеста
            $marks = OrdersMarkInside::where('id_user', $uid)->where('id', '>', 9)->get();
        }
        elseif($siteInfo === 4){
            // количество маркетингов с автореинвестом
            $marks = OrdersMarkInside::where('id_user', $uid)->where('id', '>', 9)->get();
        }

        $rows = [];

        foreach ($marks as $mark) {
            $rows[] = [
                'name' => \App\Helpers\MarksHelper::markName($mark->mark),
                'curator' => [
                    'title' => $mark->curator['login'],
                    'link' => 'profile/' . $mark->curator['id'],
                ]
            ];
        }


        $widgetData = [
            'title' => 'Кураторы', // поле не используется на клиенте
            'rows' => $rows,
            'headers' => ['Тариф', 'Куратор'],
            'link'  => '/profile/referalTree', // TODO: сменить на актуальную ссылку на пополнение
            // 'link_text' => 'пополнить баланс',
            'color' => 'info'
        ];

        return $widgetData;
    }




    public static function mkPlans($userData) {
        $siteInfo = SiteInfo::first()['mark_type'];

        $uid = $userData['id'];

        if ($siteInfo === 3) {
            // количество маркетингов без автореинвеста
            $marks = MarksInside::where('status', 1)->get();
        }
        elseif($siteInfo === 4){
            // количество маркетингов с автореинвестом
            $marks = MarksInside::where('status', 1)->where('only_reinvest', 0)->get();
        }

        $rows = [];

        foreach ($marks as $mark) {
            $rows[] = [
                'name' => \App\Helpers\MarksHelper::markName($mark->id),
                'button' => [
                    'title' => 'Активировать',
                    'link' => 'javascript:activate('.$mark->id.');',
                    'classes' => 'js-activate btn btn-block btn-primary'
                ]
            ];
        }

        $widgetData = [
            'title' => 'Тарифы', // поле не используется на клиенте
            'rows' => $rows,
            'headers' => ['Тариф', ''],
            'link'  => '', // TODO: сменить на актуальную ссылку на пополнение
            // 'link_text' => 'пополнить баланс',
            'color' => 'info'
        ];

        return $widgetData;
    }





    public static function mkParentReferal($userData) {

        $ref = Users::find($userData['refer']);

        $widgetData = [
            // 'parent_referal' => $userData['refer'],
            'value' => $ref['login'],
            'picture' => '/laravel/storage/app/'.$ref['avatar'],
            'link_text' => 'посмотреть профиль',
            'link' => '/profile/' . $ref['id'],
            'color' => 'bg-red'
        ];

        return $widgetData;
    }

    public static function mkReferalsCount($userData) {

        $refCount = Users::where('refer', $userData['id'])->count();

        $widgetData = [
            // 'parent_referal' => $userData['refer'],
            'value' => $refCount,
            // 'picture' => '/'.$ref['avatar'],
            // 'link_text' => 'посмотреть профиль',
            // 'link' => '/profile/' . $ref['id'],
            'color' => 'bg-yellow'
        ];

        return $widgetData;
    }


    public static function mkReferals($userData) {

        $refCount = Users::where('refer', $userData['id'])->count();
        $referals = Users::select('id', 'avatar', 'login', 'last_active')->where('refer', $userData['id'])->take(8)->get()->toArray();

        foreach ($referals as &$referal) {
            $referal['avatar'] = '/laravel/storage/app/'.$referal['avatar'];
            $referal['last_active'] = showDate(strtotime($referal['last_active']));
        }

        $widgetData = [
            // 'parent_referal' => $userData['refer'],
            'users' => $referals,
            'total' => $refCount,
            // 'picture' => '/'.$ref['avatar'],
            // 'link_text' => 'посмотреть профиль',
            'link' => '/profile/referral-list/',
            'color' => 'success'
        ];

        return $widgetData;
    }

    public static function mkLinkVisits($userData) {
        $widgetData = [
            // 'title' => 'Визиты страницы', // не используется на клиенте, оставил для красоты
            'value' => $userData['link_visit'],
            'color' => 'bg-aqua'
        ];
        return $widgetData;
    }


    public static function mkFriends($userData) {
        // дергаем друзей пользователя
        $friendsCount = Friends::where('fromid' , $userData['id'])->where('status', 1)->count();

        $friends = Friends::select('id')->where('fromid' , $userData['id'])->where('status', 1)->take(8)->get()->toArray();

        $friendsOut = [];
        foreach ($friends as &$friend) {
            $friendTemp = Users::select('avatar', 'id', 'login', 'last_active')->find($friend['id']);
            if (!empty($friendTemp)) {
                $friendsOut[] = [
                    'id' => $friendTemp['id'],
                    'avatar' => '/laravel/storage/app/' . $friendTemp['avatar'],
                    'last_active' => showDate(strtotime($friendTemp['last_active'])),
                ];
            }

        }

        $widgetData = [
            'users' => $friendsOut,
            'total' => $friendsCount,
            'link' => '/friends/',
            'color' => 'warning'
        ];
        return $widgetData;
    }

}


/**
  * Перенести в другое место
 */
function showDate( $date ) // $date --> время в формате Unix time
{
    $stf      = 0;
    $cur_time = time();
    $diff     = $cur_time - $date;

    $seconds = array( 'секунда', 'секунды', 'секунд' );
    $minutes = array( 'минута', 'минуты', 'минут' );
    $hours   = array( 'час', 'часа', 'часов' );
    $days    = array( 'день', 'дня', 'дней' );
    $weeks   = array( 'неделя', 'недели', 'недель' );
    $months  = array( 'месяц', 'месяца', 'месяцев' );
    $years   = array( 'год', 'года', 'лет' );
    $decades = array( 'десятилетие', 'десятилетия', 'десятилетий' );

    $phrase = array( $seconds, $minutes, $hours, $days, $weeks, $months, $years, $decades );
    $length = array( 1, 60, 3600, 86400, 604800, 2630880, 31570560, 315705600 );

    for ( $i = sizeof( $length ) - 1; ( $i >= 0 ) && ( ( $no = $diff / $length[ $i ] ) <= 1 ); $i -- ) {
        ;
    }
    if ( $i < 0 ) {
        $i = 0;
    }
    $_time = $cur_time - ( $diff % $length[ $i ] );
    $no    = floor( $no );
    $value = sprintf( "%d %s ", $no, getPhrase( $no, $phrase[ $i ] ) );

    if ( ( $stf == 1 ) && ( $i >= 1 ) && ( ( $cur_time - $_time ) > 0 ) ) {
        $value .= time_ago( $_time );
    }

    return $value . ' назад';
}

function getPhrase( $number, $titles ) {
    $cases = array( 2, 0, 1, 1, 1, 2 );

    return $titles[ ( $number % 100 > 4 && $number % 100 < 20 ) ? 2 : $cases[ min( $number % 10, 5 ) ] ];
}





