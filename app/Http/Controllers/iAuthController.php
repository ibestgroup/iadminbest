<?php

namespace App\Http\Controllers;

use App\Libraries\Domain;
use App\Libraries\Helpers;
use App\Models\Ad;
use App\Models\CoreSiteList;
use App\Models\SiteInfo;
use App\Models\TelegramCore;
use App\Models\TelegramMenu;
use App\Models\Users;
use App\Models\UsersData;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Nexmo\User\User;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\Debug\Exception\DummyException;
use Telegram\Bot\Api;
use Telegram\Bot\Keyboard\Keyboard;
use Telegram\Bot\Laravel\Facades\Telegram;
use Telegram\Bot\Objects\Update;
use Telegram\Bot\TelegramClient;
use Telegram\Bot\Exceptions\TelegramSDKException;

class iAuthController extends Controller
{

    public function __construct() {
        define('CALLBACK_API_CONFIRMATION_TOKEN', '071924d7'); // Строка, которую должен вернуть сервер
        define('VK_API_ACCESS_TOKEN', '4cc0f0fa2ce8ab9b04395cce19e6a1ecfbc76f68c678edc847f9bf3e073372f5eb00f90fd204deaf8c6a6'); // Ключ доступа сообщества

        define('CALLBACK_API_EVENT_CONFIRMATION', 'confirmation'); // Тип события о подтверждении сервера
        define('CALLBACK_API_EVENT_MESSAGE_NEW', 'message_new'); // Тип события о новом сообщении
        define('CALLBACK_API_EVENT_MESSAGE_REPLY', 'message_reply'); // Тип события о новом сообщении
        define('VK_API_ENDPOINT', 'https://api.vk.com/method/'); // Адрес обращения к API
        define('VK_API_VERSION', '5.89'); // Используемая версия API

    }

    public static function iAuth()
    {

        $domain = new Domain();

        $bot = new \TelegramBot\Api\BotApi('1120419890:AAGWD_OA2vfJdhr37fjoDuCKGpJjbRm94NY');

        http_response_code(200);
        $data = json_decode(file_get_contents('php://input'), TRUE);


        if (isset($data['callback_query'])) {

            $arResult = $data;
            $userId = $arResult['callback_query']['from']['id'];
            $userLogin = $arResult['callback_query']['from']['username'];
            $dataCB = $arResult['callback_query']['data'];

            if ($dataCB == 'testtest_iadminbest') {

                $buttons = [];

                array_push($buttons, ["text" => "back", "callback_data" => "/start"]);
                array_push($buttons, ["text" => "newbutton", "callback_data" => "qweewq"]);

                $sendData['text'] = 'Я нашел тебя на этих сайтах';

                $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                    [
                        $buttons
                    ]
                );

                $bot->editMessageText($userId, $arResult['callback_query']['message']['message_id'], 'newtext', null, true, $keyboard);


                return Response::json("OK", 200);
            } else {

                preg_match_all('/site\_id\:(.*)/',$dataCB, $pos);
                $siteId = $pos[1][0];

                $rand = rand(100000, 999999);
                TelegramCore::where('site_id', $siteId)->where('teleg_login', '@'.$userLogin)->update([
                    'code' => $rand,
                    'date_last_code' => strtotime(now()),
                    'updated_at' => strtotime(now())
                ]);

                $bot->sendMessage($userId, "Ваш код для подтверждения: ".$rand);
                return Response::json("OK", 200);
            }

        } else {

            $sendData['chat_id'] = @$data['message']['chat']['id'];
            if ($sendData['chat_id']) {
                $apiType = 'teleg';
            } else {
                $apiType = 'vk';
            }

            if ($apiType == 'teleg') {

                /*
                 * Telegram api
                 */

                $message = $data['message']['text'];
                $info = $data['message'];

                $count = TelegramCore::where('teleg_login', '@' . $info['chat']['username'])->count();

                $sendData['chat_id'] = $data['message']['chat']['id'];

                if ($message == '/start' or $message == '/code' or $message == 'код' or $message == 'дай код') {

                    if ($count == 1) {

                        $telegInfo = TelegramCore::where('teleg_login', '@' . $info['chat']['username'])->first();
                        TelegramCore::where('teleg_login', '@' . $info['chat']['username'])->update([
                            'teleg_id' => $sendData['chat_id'],
                            'teleg_chat_id' => $sendData['chat_id'],
                        ]);


                        $siteDB = CoreSiteList::where('id', $telegInfo->site_id)->first()['subdomain'];
                        $siteDomain = $domain->resultDomain($telegInfo->site_id);

                        Config::set("database.connections.other", [
                            'driver' => 'mysql',
                            "host" => "localhost",
                            "database" => $siteDB,
                            "username" => "root",
                            "password" => "DrinkVodkaPlayDotka228"
                        ]);

                        $user = Users::on('other')->where('id', $telegInfo->user_id)->first();

                        if ($message == '/start') {
                            $sendData['text'] = "Привет! Кажется я тебя знаю! Так-так-так... посмотрим... Да, точно! Ты " . $user->login . ", на сайте $siteDomain правильно?";
                            $bot->sendMessage($sendData['chat_id'], $sendData['text']);
                        }

                        TelegramController::sendCodeArr(['site_id' => $telegInfo->site_id, 'user_id' => $user->id]);

                        return Response::json("OK", 200);


                    } elseif ($count > 1) {

                        $telegInfo = TelegramCore::where('teleg_login', '@' . $info['chat']['username'])->get();

                        TelegramCore::where('teleg_login', '@' . $info['chat']['username'])->update([
                            'teleg_id' => $sendData['chat_id'],
                            'teleg_chat_id' => $sendData['chat_id'],
                        ]);

                        $buttons = [];

                        foreach ($telegInfo as $key => $ti) {

                            $siteDB = CoreSiteList::where('id', $ti->site_id)->first()['subdomain'];
                            $siteDomain = $domain->resultDomain($ti->site_id);

                            Config::set("database.connections.other", [
                                'driver' => 'mysql',
                                "host" => "localhost",
                                "database" => $siteDB,
                                "username" => "root",
                                "password" => "DrinkVodkaPlayDotka228"
                            ]);

                            $siteInfo = substr(SiteInfo::on('other')->first()->name, 0, 20);

                            $user = Users::on('other')->where('id', $ti->user_id)->first();

                            array_push($buttons, ["text" => "$siteInfo", "callback_data" => "site_id:" . $ti->site_id]);

                            DB::disconnect('other');

                        }

                        $sendData['text'] = 'Я нашел тебя на этих сайтах';

                        $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup(
                            [
                                $buttons
                            ]
                        );

                        $bot->sendMessage($sendData['chat_id'], $sendData['text'], null, false, null, $keyboard);

                        return Response::json("OK", 200);


                    } else {

                        $sendData['text'] = 'Я не нашел тебя ни на одном подвластном мне сервисе';
                        $bot->sendMessage($sendData['chat_id'], $sendData['text']);

                        return Response::json("OK", 200);

                    }


                } else {

                    $sendData['text'] = 'Bye bro';
                    $bot->sendMessage($sendData['chat_id'], $sendData['text']);

                    return Response::json("OK", 200);
                }

            } elseif ($apiType == 'vk') {

                /*
                 * VK Api
                 */

                $event = json_decode(file_get_contents('php://input'), true);

                switch ($event['type']) {
                    // Подтверждение сервера
                    case CALLBACK_API_EVENT_CONFIRMATION:
                        echo(CALLBACK_API_CONFIRMATION_TOKEN);
                        break;
                    // Получение нового сообщения
                    case CALLBACK_API_EVENT_MESSAGE_NEW:
                        $message = @$event['object'];
                        $peer_id = @$message['peer_id'] ?@$message['peer_id']: @$message['user_id'];

                        $count = TelegramCore::where('vk_id', $peer_id)->where('vk_status', '!=', 2)->count();
                        $rand = rand(100000, 999999);

                        if ($count == 1 or @$message['body'] == '/start') {
                            $telegInfo = TelegramCore::where('vk_id', $peer_id)->first();
                            TelegramCore::where('vk_id', $peer_id)->update([
                                'vk_code' => $rand,
                            ]);

                            $siteDB = CoreSiteList::where('id', $telegInfo->site_id)->first()['subdomain'];

                            Config::set("database.connections.other", [
                                'driver' => 'mysql',
                                "host" => "localhost",
                                "database" => $siteDB,
                                "username" => "root",
                                "password" => "DrinkVodkaPlayDotka228"
                            ]);

                            $user = Users::on('other')->where('id', $telegInfo->user_id)->first();


                            $sendData['text'] = "Привет, $user->login! Твой код подтверждения: $rand";
                            self::sendMessageVk($peer_id, $sendData['text']);

                        } elseif ($count > 1) {
                            $text = @$message['text'];
                            $ch = curl_init();
//
                            curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
                            curl_setopt($ch, CURLOPT_HEADER, 0);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_URL, 'https://'.$text.'/get-site-info');
                            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

                            $data = curl_exec($ch);
                            curl_close($ch);

                            $siteInfo = (array)json_decode($data);


                            if (isset($siteInfo['site_id'])) {

                                $iAuth = TelegramCore::where('site_id', $siteInfo['site_id'])->where('vk_id', $peer_id);
                                $iAuthCount = $iAuth->count();
                                $iAuthRec = $iAuth->first();
                                if ($iAuthCount) {

                                    $ch = curl_init();

                                    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
                                    curl_setopt($ch, CURLOPT_HEADER, 0);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                    curl_setopt($ch, CURLOPT_URL, 'https://'.$text.'/get-user-info/'.$iAuthRec->user_id);
                                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

                                    $data = curl_exec($ch);
                                    curl_close($ch);

                                    $user = (array)json_decode($data);

                                    $iAuth->update([
                                        'vk_code' => $rand,
                                        'date_last_code' => strtotime(now()),
                                        'updated_at' => strtotime(now())
                                    ]);

                                    $sendData['text'] = "Привет, $user[login]! Твой код подтверждения: $rand для сайта $siteInfo[name]";
                                    self::sendMessageVk($peer_id, $sendData['text']);
                                }

                            } else {

                                $buttonsStart = '{"one_time": false,"inline": true,"buttons": [[';
                                $iAuthArray = TelegramCore::where('vk_id', $peer_id)->get();
                                $buttons = '';
                                foreach ($iAuthArray as $item) {
                                    $domain = new Domain();
                                    $siteUrl = $domain->resultDomain($item->site_id);
                                    $buttons .= '{"action": {"type": "text","payload": "{\"site_id\":\"'.$siteUrl.'\"}","label": "'.$siteUrl.'"},"color": "secondary"},';
                                }

                                $buttons = substr($buttons, 0, -1);
                                $buttonsEnd = ']]}';
                                $buttons = $buttonsStart.$buttons.$buttonsEnd;
                                $sendData['text'] = "Привет! Я нашел тебя на нескольких сайтах. Выбери нужный.";
                                self::sendMessageVk($peer_id, $sendData['text'], $buttons);

                            }
                        } else {
                            self::sendMessageVk($peer_id, "Я тщательно проверил все сайты, к которым я подключен, но нигде тебя не нашел =(");
                        }

                        echo('ok');
                        break;
                    default:
                        echo('ok');
                        break;
                }

            }
        }
    }

    public function iAuthAdd(Request $request) {


        $site = Helpers::getSiteId();
        $user = Auth::id();

        $telegCount = TelegramCore::where('site_id', $site)->where('user_id', $user)->where('status', 1)->count();
        $vkCount = TelegramCore::where('site_id', $site)->where('user_id', $user)->where('vk_status', 1)->count();


        if (!$telegCount and !$vkCount and isset($request->login)) {
            TelegramCore::where('site_id', $site)
                ->where('user_id', $user)
                ->delete();

            if ($request->messanger == 'vk') $vkId = $this->getVkId($request->login);

//            dd($request->login);
            TelegramCore::create([
                'status' => 0,
                'teleg_login' => ($request->messanger == 'teleg')?$request->login:'',
                'teleg_id' => 0,
                'teleg_chat_id' => 0,
                'user_id' => Auth::id(),
                'site_id' => Helpers::getSiteId(),
                'code' => rand(100000, 999999),
                'date_last_code' => strtotime(now()),
                'notice' => 1,
                'system_notice' => 1,
                'created_at' => strtotime(now()),
                'updated_at' => strtotime(now()),
                'vk_status' => 0,
                'vk_link' => ($request->messanger == 'vk')?$request->login:0,
                'vk_id' => ($request->messanger == 'vk')?$vkId:0,
                'vk_notice' => 0,
                'vk_system_notice' => 0,
                'vk_code' => 0,
            ]);
        }

        if ($request->messenger == 'teleg') {

            if ($vkCount) {
//                $vkForThisTelegram = TelegramCore::where('teleg_login', $request->login)->where('status', 1)->where('vk_status', 1)->first();
//                $thisVk = TelegramCore::where('user_id', $user)->where('site_id', $site)->where('vk_status', 1)->first();
//
//                if ($thisVk->vk_id != $vkForThisTelegram->vk_id) {
//                    return Response::json([
//                        'type' => 'error',
//                        'message' => 'В паре с этим телеграм используется другой аккаунт Вконтакте.',
//                    ], 200);
//                }

            }

            if (!$telegCount and isset($request->login)) {
                TelegramCore::where('user_id', $user)->where('site_id', $site)->update([
                    'status' => 0,
                    'teleg_login' => $request->login,
                    'teleg_id' => 0,
                    'teleg_chat_id' => 0,
                    'code' => rand(100000, 999999),
                ]);
            }

        } elseif ($request->messenger == 'vk') {

            if (!preg_match("~^https\:\/\/vk\.com\/~", $request->login)) {
                return Response::json([
                    'type' => 'error',
                    'message' => 'Неправильная ссылка. Должна начинаться на https://vk.com/',
                ], 200);
            }

            $vkId = $this->getVkId($request->login);
            if (!$vkCount and isset($request->login)) {
                TelegramCore::where('user_id', $user)->where('site_id', $site)->update([
                    'vk_status' => 0,
                    'vk_link' => $request->login,
                    'vk_id' => $vkId,
                    'vk_code' => rand(100000, 999999),
                ]);
            }

        }

    }

    public static function iAuthConfirm(Request $request) {

        $site = Helpers::getSiteId();
        $user = Auth::id();

        $iAuth = TelegramCore::where('site_id', $site)->where('user_id', $user);
        $iAuthRec = $iAuth->first();


        $code = ($request->messenger == 'teleg')?$iAuthRec->code:$iAuthRec->vk_code;

        if ($request->iauth_code == $code and $request->iauth_code != 0) {

            if ($request->messenger == 'teleg') {

                $iAuth->update([
                    'status' => 1,
                ]);

                $iAuth = TelegramCore::where('site_id', $site)->where('user_id', $user)->first();
                $findFull = TelegramCore::where('teleg_login', $iAuth->teleg_login)
                    ->where('status', 1)
                    ->where('vk_status', 1);

                if ($findFull->count()) {
                    $findFull = $findFull->first();
                    TelegramCore::where('teleg_login', $findFull->teleg_login)->update([
                        'vk_status' => 1,
                        'vk_link' => $findFull->vk_link,
                        'vk_id' => $findFull->vk_id,
                    ]);

                    TelegramCore::where('vk_id', $findFull->vk_id)->update([
                        'status' => 1,
                        'teleg_login' => $findFull->teleg_login,
                        'teleg_id' => $findFull->teleg_id,
                        'teleg_chat_id' => $findFull->teleg_chat_id,
                    ]);
                    $mess = "и вконтакте $findFull->vk_link";
                }

                UsersData::where('id_user', $user)->update([
                    'telegram' => $iAuthRec->teleg_login,
                ]);

                $mess = 'Телеграм '.$mess.' аккаунт успешно привязан';

            } elseif ($request->messenger == 'vk') {

                $iAuth->update([
                    'vk_status' => 1,
                ]);


                $iAuth = TelegramCore::where('site_id', $site)->where('user_id', $user)->first();
                $findFull = TelegramCore::where('vk_id', $iAuth->vk_id)
                    ->where('status', 1)
                    ->where('vk_status', 1);

                if ($findFull->count()) {
                    $findFull = $findFull->first();
                    TelegramCore::where('vk_id', $findFull->vk_id)->update([
                        'status' => 1,
                        'teleg_login' => $findFull->teleg_login,
                        'teleg_id' => $findFull->teleg_id,
                        'teleg_chat_id' => $findFull->teleg_chat_id,
                    ]);

                    TelegramCore::where('teleg_login', $findFull->teleg_login)->update([
                        'vk_status' => 1,
                        'vk_link' => $findFull->vk_link,
                        'vk_id' => $findFull->vk_id,
                    ]);
                    $mess = "и телеграмм $findFull->teleg_login";
                }

                UsersData::where('id_user', $user)->update([
                    'vk' => $iAuthRec->vk_link,
                ]);

                $mess = 'Страница '.$mess.' ВКонтакте успешно привязана';
            }

            return Response::json([
                'type' => 'success',
                'message' => $mess,
            ], 200);

        } else {
            Helpers::iAuth('reset');
            return Response::json([
                'type' => 'error',
                'message' => 'Код введен неверно. Получите новый код.',
            ], 200);
        }
    }

    public static function iAuthUnlink(Request $request) {

        $messenger = $request->messenger;
        $iAuthError = Helpers::iAuth('check_code', $request->iauth_code, $request->messenger);
        if ($iAuthError) {
            return Response::json([
                "type" => "error",
                "message" => $iAuthError,
            ], 200);
        }

        $site = Helpers::getSiteId();
        $user = Auth::id();

        $teleg = TelegramCore::where('site_id', $site)->where('user_id', $user)->first();


        if ($messenger == 'teleg') {

            TelegramCore::where('teleg_login', $teleg->teleg_login)->update([
                'status' => 2,
//                'teleg_login' => '',
//                'teleg_id' => 0,
//                'teleg_chat_id' => 0,
//                'notice' => 0,
//                'system_notice' => 0,
//                'code' => 0,
            ]);
            UsersData::where('id_user', $user)->update(['telegram' => '']);

            return Response::json([
                'type' => 'warning',
                'message' => "Телеграм $teleg->teleg_login отключен от вашего аккаунта",
            ], 200);

        } elseif ($messenger == 'vk') {

            TelegramCore::where('vk_id', $teleg->vk_id)->update([
                'vk_status' => 2,
//                'vk_link' => '',
//                'vk_id' => 0,
//                'vk_notice' => 0,
//                'vk_system_notice' => 0,
//                'vk_code' => 0,
            ]);

            UsersData::where('id_user', $user)->update(['vk' => '']);

            return Response::json([
                'type' => 'warning',
                'message' => "Вконтакте $teleg->vk_link отключен от вашего аккаунта",
            ], 200);
        }


    }

    public static function iAuthNoticeEdit(Request $request) {


        $messenger = $request->messenger;
        $prefix = ($messenger == 'vk')?'vk_':'';

        $iAuthError = Helpers::iAuth('check_code', $request->iauth_code, $messenger);
        if ($iAuthError) {
            return Response::json([
                "type" => "error",
                "message" => $iAuthError,
            ], 200);
        }

        $site = Helpers::getSiteId();
        $user = Auth::id();

        $notice = $request->notice;
        $systemNotice = $request->system_notice;


        TelegramCore::where('site_id', $site)->where('user_id', $user)->update([
            $prefix.'notice' => $notice,
            $prefix.'system_notice' => $systemNotice,
        ]);

        return Response::json([
            'type' => 'success',
            'message' => "Настройки оповещений изменены",
        ], 200);

    }

    public static function sendCode(Request $request) {

        $siteId = $request->site_id;
        $userId = $request->user_id;
        $messenger = $request->messenger;

        $prefix = ($messenger == 'teleg')?'':'vk_';

        $iAuth = TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->where($prefix.'status', 1);

        if ($iAuth->count()) {

            $rand = rand(100000,999999);
            $iAuth->update([$prefix.'code' => $rand]);
            $iAuth = $iAuth->first();

            if ($messenger == 'teleg') {
                $bot = new \TelegramBot\Api\BotApi('1120419890:AAGWD_OA2vfJdhr37fjoDuCKGpJjbRm94NY');
                $bot->sendMessage($iAuth->teleg_id, "Держи код для подтверждения: ".$rand." только тссс... никому его не говори, даже мне.");
            } else if ($messenger == 'vk') {
                self::sendMessageVk($iAuth->vk_id, "Держи код для подтверждения: ".$rand." только тссс... никому его не говори, даже мне.");
            }
        }

    }

    public static function sendCodeArr($data = ['messenger' => 'teleg']) {

        $siteId = $data['site_id'];
        $userId = $data['user_id'];
        $messenger = $data['messenger'];

        $prefix = ($messenger == 'teleg')?'':'vk_';

        $iAuth = TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->where($prefix.'status', 1);

        if ($iAuth->count()) {

            $rand = rand(100000,999999);
            $iAuth->update([$prefix.'code' => $rand]);
            $iAuth = $iAuth->first();

            if ($messenger == 'teleg') {
                $bot = new \TelegramBot\Api\BotApi('1120419890:AAGWD_OA2vfJdhr37fjoDuCKGpJjbRm94NY');
                $bot->sendMessage($iAuth->teleg_id, "Держи код для подтверждения: ".$rand." только тссс... никому его не говори, даже мне.");
            } else if ($messenger == 'vk') {
                self::sendMessageVk($iAuth->vk_id, "Привет: {$userId}");
            }
        }

    }

    public static function sendMessageVk($peer_id, $message, $array = '') {
        self::api('messages.send', array(
            'peer_id' => $peer_id,
            'message' => $message,
            'keyboard' => $array,
        ));
    }

    public static function api($method, $params) {
        $params['access_token'] = VK_API_ACCESS_TOKEN;
        $params['v'] = VK_API_VERSION;
        $query = http_build_query($params);
        $url = VK_API_ENDPOINT . $method . '?' . $query;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_exec($curl);
        curl_close($curl);
    }

    public function getVkId($url) {
        $pageVk = file_get_contents($url);
        preg_match('~<a href="/photo(.*?)_~', $pageVk, $matches);
        $result = [
            'vk_id' => $matches[1]
        ];
        return $result['vk_id'];
    }



    public function iAuthCheckPair(Request $request) {
        $site = Helpers::getSiteId();
        $user = Auth::id();
        $messenger = $request->messenger;

        if ($messenger == 'teleg') {
            $nameColumn = 'teleg_login';
            $prefix = 'vk_';
            $message = 'В паре с этим телеграм используется другой аккаунт Вконтакте.';
            $equal = 'vk_id';
            $login = $request->login;
        } elseif ($messenger == 'vk') {
            $nameColumn = 'vk_id';
            $prefix = '';
            $message = 'В паре с этой страницей вк уже используется другой аккаунт Телеграм.';
            $equal = 'teleg_id';
            $login = self::getVkId($request->login);
        }

        $forThisMessenger = TelegramCore::where($nameColumn, $login)->where('status', 1)->where('vk_status', 1);
        $forThisMessengerRec = $forThisMessenger->first();
        $forThisMessengerCount = $forThisMessenger->count();
        $thisMessenger = TelegramCore::where('user_id', $user)->where('site_id', $site)->where($prefix.'status', 1);
        $thisMessengerRec = $thisMessenger->first();
        $thisMessengerCount = $thisMessenger->count();

//        dd($thisMessengerRec, $forThisMessengerRec);



        $result['message'] = '';

        if ($thisMessengerCount and $forThisMessengerCount) {
            if ($thisMessengerRec->{$equal} != $forThisMessengerRec->{$equal}) {
                $result['message'] = $message;
            }
        }

        return $result;

    }

    public function jsonPair(Request $request) {
        $site = Helpers::getSiteId();
        $user = Auth::id();
        $messenger = $request->messenger;

        if ($messenger == 'teleg') {
            $nameColumn = 'teleg_login';
            $prefix = '';
            $message = 'В паре с этим телеграм используется другой аккаунт Вконтакте.';
            $equal = 'vk_id';
        } elseif ($messenger == 'vk') {
            $nameColumn = 'vk_id';
            $prefix = 'vk_';
            $message = 'В паре с этой страницей вк уже используется другой аккаунт Телеграм.';
            $equal = 'teleg_id';
        }

        $forThisMessenger = TelegramCore::where($nameColumn, $request->login)->where('status', 1)->where('vk_status', 1)->first();
        $thisMessenger = TelegramCore::where('user_id', $user)->where('site_id', $site)->where($prefix.'status', 1)->first();

//        if ($thisMessenger->{$equal} != $forThisMessenger->$equal) {
//            return $m
//        }

//        if ($messenger == 'teleg') {
//
//        } elseif ($messenger == 'vk') {
//
//            $telegForThisVk = TelegramCore::where('vk_id', $request->login)->where('status', 1)->where('vk_status', 1)->first();
//            $thisTeleg = TelegramCore::where('user_id', $user)->where('site_id', $site)->where('status', 1)->first();
//
//            if ($thisTeleg->teleg_id != $telegForThisVk->teleg_id) {
//                return Response::json([
//                    'type' => 'error',
//                    'message' => 'В паре с этой страницей вк уже используется другой аккаунт Телеграм.',
//                ], 200);
//            }
//
//        }


    }

    public function telegBot()
    {
        $bot = new \TelegramBot\Api\BotApi('1855024292:AAFnPS7OWt7aylnNgFrOUDvq_5l-WLOlsvo');
        $bot->sendMessage(370309867, "Понг");
        echo 'OK';
    }

    public function telegBotq(Request $request)
    {
        $typeMessage = (isset($request->input()['callback_query'])) ? 'callback_query' : 'message';
        $userId = $request->input()[$typeMessage]['from']['id'];

        Log::info($userId);

        Config::set("database.connections.telegram", [
            "host" => "127.0.0.1",
            "database" => 'newsite_iadminbest',
            "username" => "root",
            "password" => "DrinkVodkaPlayDotka228",
            'driver' => 'mysql',
            'strict' => false,
        ]);



        $siteInfo = SiteInfo::on('telegram')->find(1);
        $subDom = $siteInfo->subdom;
        $bot = new \TelegramBot\Api\BotApi('1855024292:AAFnPS7OWt7aylnNgFrOUDvq_5l-WLOlsvo');

        if (isset($request->input()['callback_query'])) {
            $answer = explode('_', $request->input()['callback_query']['data'])[1] ?? 1;

            $resultMenu = self::getMenu($answer);

            $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup($resultMenu['keyboard']);
            $bot->editMessageText(
                $request->input()['callback_query']['message']['chat']['id'],
                $request->input()['callback_query']['message']['message_id'],
                $resultMenu['text'],
                null,
                false,
                $keyboard
            );
        } else {

            $resultMenu = self::getMenu();
            $keyboard = new \TelegramBot\Api\Types\Inline\InlineKeyboardMarkup($resultMenu['keyboard']);
            if (isset($request->input()['message']['chat']['id'])) {

                if (explode('_', $request->input()['message']['text'])[0] == 'balance') {
                    $bot->sendMessage(
                        $request->input()['message']['chat']['id'],
                        '<b style="color: red;">Пополнение</b> баланса /balance_200 на ' . explode('_', $request->input()['message']['text'])[1],
                        'HTML',
                        false,
                        $request->input()['message']['message_id'],
                        $keyboard
                    );
                } else {
                    $bot->sendMessage(
                        $request->input()['message']['chat']['id'],
                        $resultMenu['text'],
                        null,
                        false,
                        null,
                        $keyboard
                    );
                }
            }
        }

        die('OK');

    }

    public static function getMenu($answer = 1) {
        $telegramMenu = TelegramMenu::on('telegram')->where('id', $answer)->where('type', 'menu')->first();
        if (!$telegramMenu) {
            $telegramMenu = TelegramMenu::on('telegram')->find(1);
        }
        $telegramButtons = TelegramMenu::on('telegram')->where('parent_id', $telegramMenu->id)->where('id', '!=', 1)->orderBy('sort')->get();

        $text = $telegramMenu->text;
        $keyboard = [];
        $itemButton = [];
        foreach ($telegramButtons as $item) {
            $itemButton[] = [
                'text' => $item->name,
                'callback_data' => 'item_' . $item->id,
            ];
        }

        $keyboard[] = $itemButton;
        if ($telegramMenu->id != 1) {
            $backMenu = TelegramMenu::on('telegram')->where('id', $telegramMenu->parent_id)->first();
            $keyboard[] = [
                [
                    'text' => '<<<- Главное меню',
                    'callback_data' => 'main',
                ],
                [
                    'text' => '<- Назад',
                    'callback_data' => $backMenu->callback,
                ],
            ];
        }

        $header = '';
        $footer = '';
        $result = $header . $text . $footer;
        return ['text' => $result, 'keyboard' => $keyboard];
    }

}