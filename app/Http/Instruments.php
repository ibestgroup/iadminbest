<?php
namespace App\Http\Controllers;

use App\Models\Users;
use Auth;

class Instruments extends Controller {

    public static function getLogin($id) {
        $user = Users::where('id', '=', $id);
        return $user['login'];
    }

    public static function getId($login) {
        $user = Users::where('login', '=', $login);
        return $user['id'];
    }

}
