<?php

namespace App\Http\Middleware;

use App\Models\StatisticVisit;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckVisit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $toDay = date("Y-m-d");

        if (StatisticVisit::where('day', $toDay)->count() == 0) {
            StatisticVisit::create([
                'day' => $toDay,
                'view' => 0,
                'visit' => 0,
                'view_prof' => 0,
                'visit_prof' => 0,
                'registration' => 0,
                'payments' => 0,
                'payments_sum' => 0,
                'orders' => 0
            ]);
        }

        StatisticVisit::where('day', $toDay)->increment('visit', 1);


    }
}
