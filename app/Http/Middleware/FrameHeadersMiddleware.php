<?php

namespace App\Http\Middleware;

use App\Models\StatisticVisit;
use Closure;
use Illuminate\Support\Facades\Auth;

class FrameHeadersMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $response->header('X-Frame-Options', 'ALLOW FROM https://ibux.best/');
        return $response;
    }
}
