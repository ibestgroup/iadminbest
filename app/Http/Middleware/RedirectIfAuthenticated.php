<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        $ajax = $request->ajax ?? 0;
        if (Auth::guard($guard)->check()) {
            if($ajax == 0) {
                return redirect('/profile');
            } else {
                return Response::json([
                    "type" => "success",
                    "url" => "/profile",
                ], 200);
            }
        }

        return $next($request);
    }
}
