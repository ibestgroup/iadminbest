<?php

namespace App\Http\Middleware;

use App\Models\AuthPages;
use Closure;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;

class RedirectIfNotAuthenticated extends Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, ...$guards)
    {
        $authPage = AuthPages::first()->login ?? '/login';

        if ($authPage == '/login') {
            $this->authenticate($request, $guards);
        } else {
            if (Auth::id() == 0)
                return redirect($authPage);
        }


        return $next($request);
    }

}
