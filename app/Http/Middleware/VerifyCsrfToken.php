<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/profile/balance',
        '/checkpay',
        '/checkpayfk',
        '/successpayfk',

        '/telegrambot',
        '/telegram-add',
        '/telegramconfirm',
        '/telegram-unlink',
        '/telegram-sendcode',

        '/i-auth',
        '/i-auth-add',
        '/i-auth-confirm',
        '/i-auth-unlink',
        '/i-auth-sendcode',

        '/i-auth-check-pair',

        '/vkbot',

        '/checkpaybeta',
        '/checkcashoutbeta',
        '/checkpayfinest',
        '/checkpaypm',
        '/checkpayobm',
        '/checkpaycrypto',
        '/successpay',
        '/failspay',
        '/landing-edit',
        '/landing-edit/*',
        '/get-site-info',
        '/get-user-info',
        'stripe/*',





        '/telegbot',
    ];
}
