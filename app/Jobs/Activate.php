<?php

namespace App\Jobs;

use App\Helpers\MarksHelper;
use App\Libraries\Domain;
use App\Models\Users;
use App\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class Activate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $mark;
    protected $typeBalance;
    protected $db;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $mark, $db)
    {
        $this->user = $user;
        $this->mark = $mark;
        $this->db = $db;
    }

    /**
     * @throws \Exception
     */
    public function handle()
    {
        $activate = new \App\Libraries\Activate();
        $activate->activate($this->user, $this->mark, 0, 'activate', 0, 0, $this->db);
    }

    public function failed()
    {
        $resultPrice = MarksHelper::calcMarkPrice($this->mark);
        Users::whereId($this->user)->increment('balance', $resultPrice);
    }
}
