<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Ad extends Model {
    public $timestamps = false;

    protected $table = 'ad';
    protected $fillable = [
        'state',
        'type',
        'head',
        'text'
    ];
}