<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class AdsCore extends Model {
    public $timestamps = false;

    protected $connection = 'core';
    protected $table = 'ads_core';

    protected $fillable = [
        'name',
        'status',
        'link',
        'image',
        'type',
        'target',
        'view',
        'click',
    ];
}