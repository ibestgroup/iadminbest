<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class AdsExeption extends Model {
    public $timestamps = false;

    protected $connection = 'core';
    protected $table = 'ads_exeption';

    protected $fillable = [
        'subdomain',
    ];
}