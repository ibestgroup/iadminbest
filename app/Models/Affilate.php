<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Affilate extends Model
{
    protected $table = 'affilate';

    protected $fillable = [
        'level_1',
        'level_2',
        'level_3',
        'level_4',
        'level_5',
        'level_6',
        'level_7',
        'level_8'
    ];

    public $timestamps = false;
}