<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class AuthLanding extends Model {
    public $timestamps = false;

    protected $table = 'auth_landing';
    protected $fillable = [
        'auth',
        'register',
        'forgot',
        'reset',
    ];
}