<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AuthPages extends Model
{
    protected $table = 'auth_pages';
    public $timestamps = false;

    protected $fillable = [
        'login',
        'register',
        'password',
        'reset_password',
    ];

}