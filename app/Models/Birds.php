<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Birds extends Model
{
    protected $table = 'birds';

    protected $fillable = [
        'name',
        'type',
        'range',
        'price',
        'max_price',
        'status',
        'image',
        'profit',
        'second',
        'end',
        'only_end',
        'buy_btn',
        'now_activate',
    ];

    public function typesOrder()
    {
        return $this->belongsTo(TypesOrder::class,'type','id');
    }

    public function bird()
    {
        return $this->belongsTo(BirdsOrder::class,'id','bird_id');
    }
    public $timestamps = false;
}