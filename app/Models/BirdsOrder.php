<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BirdsOrder extends Model
{
    protected $table = 'birds_egg';

    protected $fillable = [
        'user_id',
        'bird_id',
        'balance',
        'created_at',
        'updated_at',
        'stock',
        'number',
        'status',
    ];

    public function bird() {
        return $this->belongsTo(Birds::class, 'bird_id', 'id');
    }

    public $timestamps = false;
}