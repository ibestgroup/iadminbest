<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BonusActivate extends Model
{
    protected $table = 'bonus_activate';

    protected $fillable = [
        'user_id',
        'amount',
        'created_at'
    ];

    public $timestamps = false;
}