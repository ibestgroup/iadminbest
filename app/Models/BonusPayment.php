<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BonusPayment extends Model
{
    protected $table = 'bonus_payment';

    protected $fillable = [
        'more',
        'less',
        'type',
        'amount'
    ];

    public $timestamps = false;
}