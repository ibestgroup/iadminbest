<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BonusSetting extends Model
{
    protected $table = 'bonus_setting';

    protected $fillable = [
        'status',
        'more',
        'less',
        'second',
        'cashout_for_buy'
    ];

    public $timestamps = false;
}