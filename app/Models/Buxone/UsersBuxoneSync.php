<?php
namespace App\Models\Buxone;
use \Illuminate\Database\Eloquent\Model;

class UsersBuxoneSync extends Model {
    public $timestamps = false;
    protected $connection = 'buxone';
    protected $table = 'sync_out_sites';
    protected $fillable = [
        'user_buxone',
        'user_out_site',
        'site_type',
        'site_id',
    ];
}