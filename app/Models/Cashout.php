<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Cashout extends Model
{
    protected $table = 'cashout';

    protected $fillable = [
        'id_user',
        'payment_system',
        'id_payment',
        'currency',
        'amount',
        'status',
        'to',
        'date',
    ];

    public $timestamps = false;
}