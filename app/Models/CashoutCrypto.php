<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CashoutCrypto extends Model
{
    protected $table = 'cashout_crypto';

    protected $fillable = [
        'address',
        'tx_id',
        'date',
        'confirm',
        'status',
        'amount',
        'id_user',
        'cashout_id',
        'tag'
    ];

    public $timestamps = false;
}