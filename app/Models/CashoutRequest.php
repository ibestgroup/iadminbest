<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CashoutRequest extends Model {
    public $timestamps = false;

    protected $table = 'cashout_request';
    protected $fillable = [
        'user_id',
        'payment_system',
        'wallet',
        'amount',
        'date',
        'status',
        'token',
    ];
}