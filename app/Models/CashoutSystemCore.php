<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CashoutSystemCore extends Model {
    public $timestamps = false;

    protected $connection = 'core';
    protected $table = 'cashout_system';

    protected $fillable = [
        'id',
        'name',
        'code',
        'currency',
        'min',
        'max',
        'fee',
        'custom_fee',
        'example',
        'pattern',
        'status',
    ];
}