<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CoreBotWidgets extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'telegram_widgets';
    protected $fillable = [
        'name',
        'name_ru',
    ];
}