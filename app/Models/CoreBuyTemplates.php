<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CoreBuyTemplates extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'buy_templates';
    protected $fillable = [
        'user_id',
        'template_id',
        'date',
    ];

    public function templateInfo()
    {
        return $this->belongsTo(CoreTemplates::class, 'template_id', 'id');
    }

}