<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CoreCurrencyRate extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'currency_rate';
    protected $fillable = [
        'usd',
        'eur',
        'updated_at',
    ];
}