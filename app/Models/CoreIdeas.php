<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CoreIdeas extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'ideas';
    protected $fillable = [
        'name',
        'text',
        'rate',
        'development',
        'date',
    ];
}