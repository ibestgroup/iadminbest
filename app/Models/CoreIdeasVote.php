<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CoreIdeasVote extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'ideas_vote';
    protected $fillable = [
        'idea_id',
        'user_id',
        'vote',
    ];
}