<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CorePages extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'store_pages';
    protected $fillable = [
        'status',
        'native',
        'name',
        'name_ru',
        'template',
    ];
}