<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CoreParking extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'parking';
    protected $fillable = [
        'site_id',
        'domain',
        'delegate',
        'code',
    ];
}