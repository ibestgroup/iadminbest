<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CorePartnerBalance extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'partner_balance';
    protected $fillable = [
        'user_id',
        'rub',
        'usd',
        'eur',
        'btc',
        'eth',
        'xrp',
        'ltc',
        'doge',
        'ltct',
    ];
}