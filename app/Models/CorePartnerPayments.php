<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CorePartnerPayments extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'partner_payments';
    protected $fillable = [
        'transaction_id',
        'site_id',
        'site_admin',
        'to_user',
        'amount',
        'parent_amount',
        'privilege',
        'payment_system',
        'currency',
        'date',
    ];
}