<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CoreProfit extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'profit';
    protected $fillable = [
        'rub',
        'usd',
        'eur',
        'btc',
        'eth',
        'xpr',
        'ltc',
        'doge',
        'totalrub'
    ];
}