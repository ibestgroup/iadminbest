<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CoreSiteElements extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'store_site_elements';
    protected $fillable = [
        'id_in_system',
        'type',
        'name',
        'name_ru',
        'parent_id',
        'option_value',
        'page',
        'sort',
        'template',
    ];

    public function children() {

    }
}