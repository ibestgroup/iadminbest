<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CoreSitesBalance extends Model {

    public $timestamps = false;

    protected $connection = 'core';
    protected $table = 'sites_balance';
    protected $fillable = [
        'site_id',
        'admin_id',
        'parent_id',
        'all_profit',
        'cashout',
        'updated_at',
    ];
}