<?php
namespace App\Models;
use App\Libraries\Helpers;
use \Illuminate\Database\Eloquent\Model;

class CoreTemplates extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'store_templates';
    protected $fillable = [
        'user_id',
        'status',
        'type',
        'name',
        'img',
        'price',
        'date',
    ];

    public function checkTemplate()
    {
        return $this->belongsTo(CoreBuyTemplates::class, 'id', 'template_id')->where('user_id', Helpers::getSiteAdmin())->count();
    }

    public function checkOrder()
    {
        return $this->belongsTo(CoreTemplatesOrder::class, 'id', 'template_id')->where('user_id', Helpers::getSiteAdmin())->count();
    }
}