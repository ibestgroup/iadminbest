<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CoreTemplatesOrder extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'templates_order';
    protected $fillable = [
        'template_id',
        'user_id',
        'to_user',
        'amount',
        'date',
    ];

}