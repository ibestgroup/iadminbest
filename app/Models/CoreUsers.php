<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class CoreUsers extends Model {
    public $timestamps = false;


    protected $connection = 'core';
    protected $table = 'users';
    protected $fillable = [
        'login',
        'password',
        'regdate',
        'email',
        'avatar',
        'active',
        'parent',
        'refer',
        'ref_num',
        'last_active',
        'comet_last_active',
        'privelege',
        'status',
        'payeer_wallet',
        'balance',
        'type_follow'
    ];
}