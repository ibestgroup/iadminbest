<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Currency extends Model {
    public $timestamps = false;

    protected $table = 'currency';
    protected $fillable = [
        'status',
        'name',
        'short_name',
        'price',
        'type_price',
        'img',
    ];
}