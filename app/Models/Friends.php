<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Friends extends Model
{
    protected $table = 'friends';

    protected $fillable = [
        'toid',
        'fromid',
        'status'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(Users::class,'toid','id');
    }

    public function userOut()
    {
        return $this->belongsTo(Users::class,'fromid','id');
    }

}