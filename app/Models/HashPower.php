<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class HashPower extends Model {
    public $timestamps = false;

    protected $table = 'hash_power';
    protected $fillable = [
        'status',
        'name',
        'price',
        'payment',
        'buy_order',
        'level_1',
        'level_2',
        'level_3',
        'level_4',
        'level_5',
        'level_6',
        'level_7',
        'level_8',
        'level_buy_1',
        'level_buy_2',
        'level_buy_3',
        'level_buy_4',
        'level_buy_5',
        'level_buy_6',
        'level_buy_7',
        'level_buy_8',
    ];
}