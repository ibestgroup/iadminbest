<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marks extends Model
{
    protected $table = 'marks';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'status',
        'level',
        'width',
        'type',
        'level1',
        'level2',
        'level3',
        'level4',
        'level5',
        'level6',
        'level7',
        'level8',
        'overflow'
    ];
}
