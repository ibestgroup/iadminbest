<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarksInside extends Model
{
    protected $table = 'marks_inside';
    public $timestamps = false;


    protected $fillable = [
        'name',
        'status',
        'level',
        'width',
        'reinvest',
        'overflow',
        'type',
    ];

    public function orders() {
        return $this->hasMany('\App\Models\OrdersMarkInside', 'id', 'mark');
    }
}
