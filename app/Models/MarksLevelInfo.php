<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarksLevelInfo extends Model
{
    protected $table = 'marks_levels_info';
    public $timestamps = false;


    protected $fillable = [
        'to_par_1',
        'to_par_2',
        'to_par_3',
        'to_par_4',
        'to_par_5',
        'to_par_6',
        'to_par_7',
        'to_par_8',
        'to_par_9',
        'to_par_10',
        'to_admin_1',
        'to_admin_2',
        'to_admin_3',
        'to_admin_4',
        'to_admin_5',
        'to_admin_6',
        'to_admin_7',
        'to_admin_8',
        'to_admin_9',
        'to_admin_10',
        'reinvest_type_1',
        'reinvest_type_2',
        'reinvest_type_3',
        'reinvest_type_4',
        'reinvest_type_5',
        'reinvest_type_6',
        'reinvest_type_7',
        'reinvest_type_8',
        'reinvest_type_9',
        'reinvest_type_10',
        'reinvest_1',
        'reinvest_2',
        'reinvest_3',
        'reinvest_4',
        'reinvest_5',
        'reinvest_6',
        'reinvest_7',
        'reinvest_8',
        'reinvest_9',
        'reinvest_10',
        'reinvest_first_1',
        'reinvest_first_2',
        'reinvest_first_3',
        'reinvest_first_4',
        'reinvest_first_5',
        'reinvest_first_6',
        'reinvest_first_7',
        'reinvest_first_8',
        'reinvest_first_9',
        'reinvest_first_10',
        'priority_1',
        'priority_2',
        'priority_3',
        'priority_4',
        'priority_5',
        'priority_6',
        'priority_7',
        'priority_8',
        'priority_9',
        'priority_10',
        'to_inviter_1',
        'to_inviter_2',
        'to_inviter_3',
        'to_inviter_4',
        'to_inviter_5',
        'to_inviter_6',
        'to_inviter_7',
        'to_inviter_8',
        'to_inviter_9',
        'to_inviter_10',
        'reinvest_first_type_1',
        'reinvest_first_type_2',
        'reinvest_first_type_3',
        'reinvest_first_type_4',
        'reinvest_first_type_5',
        'reinvest_first_type_6',
        'reinvest_first_type_7',
        'reinvest_first_type_8',
        'reinvest_first_type_9',
        'reinvest_first_type_10',
    ];

    public function orders() {
        return $this->hasMany('\App\Models\OrdersMarkInside', 'id', 'mark');
    }
}
