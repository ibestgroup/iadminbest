<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Notice extends Model {
    public $timestamps = false;

    protected $table = 'notice';
    protected $fillable = [
        'user_id',
        'type',
        'subtype',
        'head',
        'text',
        'view',
        'date'
    ];
}