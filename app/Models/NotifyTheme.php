<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class NotifyTheme extends Model {
    public $timestamps = false;

    protected $table = 'theme_notify';
    protected $fillable = [
        'theme',
        'position',
        'time_delay',
    ];
}