<?php

namespace App\Models;

use App\Models\Users;
use Illuminate\Database\Eloquent\Model;

class OrdersMark extends Model
{
    protected $table = 'orders_mark';
    public $timestamps = false;

    protected $fillable = [
        'id_user',
        'parent',
        'mark',
        'level',
        'par_or',
        'date',
    ];

    // public function marks() {
    //     return $this->belongsTo(MarksInside::class, 'mark', 'id');
    // }

    // public function id_user() {
    //     return $this->belongsTo(Users::class, 'id_user', 'id');
    // }

    public function curator() {
        return $this->belongsTo(Users::class, 'parent', 'id');
    }
}
