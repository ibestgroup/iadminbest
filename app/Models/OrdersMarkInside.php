<?php

namespace App\Models;

use App\Models\Users;
use Illuminate\Database\Eloquent\Model;

class OrdersMarkInside extends Model
{
    protected $table = 'orders_mark_inside';
    public $timestamps = false;

    protected $fillable = [
        'id_user',
        'parent',
        'mark',
        'level',
        'balance',
        'pay_people',
        'status',
        'par_or',
        'date',
        'reinvest',
        'cashout'
    ];

    public function marks() {
        return $this->belongsTo(MarksInside::class, 'mark', 'id');
    }

    public function id_user() {
        return $this->belongsTo(Users::class, 'id_user', 'id');
    }

    public function curator() {
        return $this->belongsTo(Users::class, 'parent', 'id');
    }

    public function marksInfo() {
        return $this->hasMany(MarksInside::class, 'id', 'mark');
    }
}
