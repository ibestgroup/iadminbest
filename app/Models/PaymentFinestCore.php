<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class PaymentFinestCore extends Model {
    public $timestamps = false;

    protected $connection = 'core';
    protected $table = 'payment_finest';

    protected $fillable = [
        'payment_id',
        'site_id',
    ];
}