<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PaymentSystems extends Model
{

    public $timestamps = false;

    protected $connection = 'core';
    protected $table = 'payment_systems';

    protected $fillable = [
        'name',
        'code',
        'min',
        'commission',
        'examples',
        'currency',
        'pattern',
        'input_fee',
        'status',
    ];
}