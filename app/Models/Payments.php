<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Payments extends Model {
    protected $table = 'payment';
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'payment_id',
        'amount',
        'date',
        'payment_system',
    ];
}