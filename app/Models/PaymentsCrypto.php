<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class PaymentsCrypto extends Model {

    public $timestamps = false;
    protected $table = 'payment_crypto';
    protected $fillable = [
        'address',
        'tx_id',
        'date',
        'confirm',
        'status',
        'amount',
        'id_user',
        'tag'
    ];
}