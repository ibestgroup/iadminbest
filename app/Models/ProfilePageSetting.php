<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProfilePageSetting extends Model
{
    protected $table = 'profile_page_setting';

    protected $fillable = [
        'width_full',
        'width',
        'bg_color',
        'bg_img',
        'sidebar_bg_color',
        'sidebar_bg_img',
        'menu_bg_color',
        'menu_bg_img',
        'header_width_full',
        'header_width',
        'header_img'
    ];

    public $timestamps = false;
}