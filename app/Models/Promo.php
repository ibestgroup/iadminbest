<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Promo extends Model {
    public $timestamps = false;

    protected $table = 'promo';
    protected $fillable = [
        'name',
        'image_link',
        'width',
        'height'
    ];
}