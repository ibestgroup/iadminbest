<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Roulette extends Model {
    public $timestamps = false;

    protected $table = 'roulette';
    protected $fillable = [
        'user_id',
        'amount',
        'ref_num',
        'result',
        'date'
    ];
}