<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class SiteElements extends Model {
    public $timestamps = false;

    protected $table = 'site_elements';
    protected $fillable = [
        'id_in_system',
        'type',
        'name',
        'name_ru',
        'parent_id',
        'option_value',
        'page',
        'sort',
        'template',
    ];

    public function children() {

    }
}