<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class SiteInfo extends Model {
    public $timestamps = false;

    protected $table = 'setting_site';
    protected $fillable = [
        'subdom',
        'name',
        'color',
        'logo',
        'value',
        'admin_id',
        'chat_main',
        'chat_private',
        'start',
        'chat_on',
        'no_image',
        'cashout_confirm',
        'min_cashout',
        'max_cashout',
        'cashout_before_start',
        'payment_to',
        'payment_pay',
        'cashout_limit_day',
        'cashout_limit_day_all',
        'sandbox',
        'alter_fee',
    ];
}