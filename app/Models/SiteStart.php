<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class SiteStart extends Model {
    public $timestamps = false;

    protected $table = 'start';
    protected $fillable = [
        'start_reg',
        'start_pay'
    ];
}