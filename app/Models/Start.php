<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Start extends Model
{
    protected $table = 'start';

    protected $fillable = [
        'start_reg',
        'start_pay'
    ];
    public $timestamps = false;
}
