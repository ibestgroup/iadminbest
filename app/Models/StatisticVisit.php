<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class StatisticVisit extends Model {
    protected $table = 'statistic';
    public $timestamps = false;

    protected $fillable = [
        'day',
        'view',
        'visit',
        'view_prof',
        'visit_prof',
        'registration',
        'payments',
        'payments_sum',
        'orders'
    ];
}