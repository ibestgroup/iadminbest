<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Stock extends Model {
    public $timestamps = false;

    protected $table = 'stock';
    protected $fillable = [
        'user_id',
        'type_order',
        'amount',
        'last_cashout'
    ];
}