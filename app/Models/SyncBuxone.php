<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class SyncBuxone extends Model {
    public $timestamps = false;

    protected $table = 'sync_buxone';
    protected $fillable = [
        'user_id',
        'user_buxone'
    ];
}