<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class TelegramBotSettings extends Model {
    public $timestamps = false;

    protected $table = 'telegram_bot_settings';
    protected $fillable = [
        'header',
        'footer',
        'separator',
    ];
}