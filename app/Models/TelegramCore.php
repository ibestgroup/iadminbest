<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class TelegramCore extends Model {
    public $timestamps = false;

    protected $connection = 'core';
    protected $table = 'telegram_core';

    protected $fillable = [
        'status',
        'teleg_login',
        'teleg_id',
        'teleg_chat_id',
        'user_id',
        'site_id',
        'code',
        'date_last_code',
        'notice',
        'system_notice',
        'created_at',
        'updated_at',
        'vk_status',
        'vk_link',
        'vk_id',
        'vk_notice',
        'vk_system_notice',
        'vk_code',
    ];
}