<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class TelegramMenu extends Model {
    public $timestamps = false;

    protected $table = 'telegram_menu';
    protected $fillable = [
        'name',
        'type',
        'text',
        'parent_id',
        'sort',
        'widget',
        'widget_settings',
    ];
}