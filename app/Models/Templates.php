<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Templates extends Model {
    public $timestamps = false;

    protected $table = 'templates';
    protected $fillable = [
        'status',
        'name',
        'name_ru',
        'store',
    ];
}