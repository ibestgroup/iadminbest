<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Tickets extends Model {
    public $timestamps = false;

    protected $table = 'tickets';
    protected $fillable = [
        'id_ticket',
        'name_ticket',
        'id_user',
        'open',
        'hide',
        'text',
        'rating',
        'date',
        'ticket_view'
    ];
}