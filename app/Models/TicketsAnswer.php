<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class TicketsAnswer extends Model {
    public $timestamps = false;

    protected $table = 'ticket_answer';
    protected $fillable = [
        'id_answer',
        'id_user',
        'id_ticket',
        'text',
        'rating',
        'date'
    ];
}