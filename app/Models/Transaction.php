<?php
namespace App\Models;
use \Illuminate\Database\Eloquent\Model;

class Transaction extends Model {

    public $timestamps = false;


    protected $table = 'transaction';
    protected $fillable = [
        'transaction_id',
        'from_user',
        'to_user',
        'type',
        'date',
    ];

    public function info($type = 'payment')
    {
        $table = new Payments();
        if ($type == 'payment') {
            $table = new Payments();
        } elseif ($type == 'payments_order') {
            $table = new PaymentsOrder();
        } elseif ($type == 'cashout') {
            $table = new Cashout();
        } elseif ($type == 'payments_crypto') {
            $table = new PaymentsCrypto();
        }
        return $this->belongsTo($table,'transaction_id','id')->first();
    }

}