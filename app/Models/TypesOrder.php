<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class TypesOrder extends Model
{
    protected $table = 'types_order';

    protected $fillable = [
        'status',
        'bird',
        'egg',
        'image',
        'price',
        'end',
        'only_end',
        'percent_cashout',
    ];

    public function birds()
    {
        return $this->hasMany(Birds::class,'type','id');
    }

    public $timestamps = false;
}