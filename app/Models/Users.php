<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class Users extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $table = 'users';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login',
        'regdate',
        'password',
        'email',
        'avatar',
        'active',
        'parent',
        'refer',
        'ref_num',
        'ref_quan',
        'last_active',
        'comet_last_active',
        'privelege',
        'status',
        'payeer_wallet',
        'balance',
        'balance_cashout',
        'balance_hash_power',
        'type_follow',
        'balance_cashout',
        'balance_hash_power',
        'telegram_id',
        'telegram_login',
        'telegram_bind_code',
        'telegram_state',
        'telegram_state_value',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function users_data()
    {
        return $this->belongsTo(UsersData::class, 'id', 'id_user');
    }

    public function user_marks()
    {
        return $this->hasMany(OrdersMarkInside::class, 'id_user', 'id');
    }

    public function userMarks() {
        return $this->hasMany('App\Models\OrdersMarkInside', 'id_user', 'id');
    }

    public  function  getIsAdminAttribute ()
    {
        return true;
    }

    public function sendPasswordResetNotification($token)
    {
//        $log = new Logger('name');
//        $log->pushHandler(new StreamHandler('log.log', Logger::WARNING));
//        $log->warning('------------ USERS ---------', array($_SERVER));


        $email = self::getEmailForPasswordReset();
        $user = self::where('email', $email)->get()[0];
        $siteInfo = SiteInfo::find(1)->get()[0];
        $url = URL::to('/');

        PasswordReset::where('email', $email)->delete();
        PasswordReset::create([
            'email' => $email,
            'token' => $token,
            'created_at' => now()
        ]);

        Mail::send('mail.restore', ['token' => $token, 'user' => $user, 'siteInfo' => $siteInfo, 'url' => $url], function ($m) use ($user, $siteInfo) {
            $m->from('info@iadmin.work', $siteInfo['name']);
            $m->to($user->email)->subject('Восстановление пароля от '.$siteInfo['name']);
        });
    }
}
