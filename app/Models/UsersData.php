<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UsersData extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;

    protected $table = 'users_data';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user',
        'lastname',
        'name',
        'fathername',
        'vk',
        'twitter',
        'facebook',
        'instagram',
        'skype',
        'telegram',
        'hide_page',
        'open_wall',
        'sex',
        'telephone',
        'rang',
        'link_visit',
        'text_status',
    ];

    public function users()
    {
        return $this->belongsTo(Users::class,'id','id_user');
    }
}
