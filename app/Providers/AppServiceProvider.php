<?php

namespace App\Providers;

use App\Helpers\MarksHelper;
use App\Libraries\Domain;
use App\Libraries\Helpers;
use App\Models\Affilate;
use App\Models\AuthLanding;
use App\Models\AuthPages;
use App\Models\BonusSetting;
use App\Models\Cashout;
use App\Models\CoreSiteList;
use App\Models\Currency;
use App\Models\HashPower;
use App\Models\MarksLevelInfo;
use App\Models\Menu;
use App\Models\OrdersMarkInside;
use App\Models\PaymentSystems;
use App\Models\ProfilePageSetting;
use App\Models\SiteBalance;
use App\Models\SiteElements;
use App\Models\SiteInfo;
use App\Models\SiteStart;
use App\Models\Transaction;
use App\Models\Users;
use Doctrine\DBAL\Schema\AbstractAsset;
use function GuzzleHttp\Psr7\str;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Blueprint;
use App\Models\MarksInside;
use App\Models\Notice;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */

    public function boot()
    {
//        $server = @$_SERVER['HTTP_X_FORWARDED_FOR'];
//        if ($server != '5.187.75.93') {
//            exit(view('templates.sites.disableSites'));
//        }

        if (Cookie::get('lang') !== null) {
            App::setLocale(Cookie::get('lang'));
        }
//        App::forgetMiddleware('Illuminate\Http\FrameGuard');


        Config::set("database.connections.core", [
            "host" => "127.0.0.1",
            "database" => 'iadminbest',
            "username" => 'root',
            "password" => "DrinkVodkaPlayDotka228",
            "driver" => "mysql",
            "charset" => "utf8mb4",

        ]);

        Config::set("database.connections.buxone", [
            "host" => "127.0.0.1",
            "database" => 'bux_one',
            "username" => 'root',
            "password" => "DrinkVodkaPlayDotka228",
            "driver" => "mysql",
            "charset" => "utf8mb4",
        ]);

        $domain = new Domain();
        $this->db = $domain->getDB();
//        dd($this->db);

        Config::set("database.connections", [
            "host" => "127.0.0.1",
            "database" => $this->db,
            "username" => "root",
            "password" => "DrinkVodkaPlayDotka228",
            "driver" => "mysql",
            "charset" => "utf8mb4",
            'strict' => false,
        ]);
        Config::set("database.connections.mysql", [
            "host" => "127.0.0.1",
            "database" => $this->db,
            "username" => 'root',
            "password" => "DrinkVodkaPlayDotka228",
            "driver" => "mysql",
            "charset" => "utf8mb4",
            'strict' => false,
        ]);

        $lang = CoreSiteList::where('subdomain', $this->db)->first()['lang'];
        if ($lang == 'en') {
            App::setLocale($lang);
            setCookie('lang', 'en', time() + 86400);
        }

        $siteStart = SiteStart::find(1);
        $helper = new Helpers();
        if ($helper->whatLater($siteStart['start_pay']) and $siteStart['start_pay'] != '0000-00-00 00:00:00') {
            SiteInfo::find(1)->update(['start' => 1]);
        }


//        $count = MarksInside::all()->count();
//        $siteInfo = SiteInfo::find(1);
//        $type = 1;
//        if ($siteInfo['mark_type'] == 3) {
//            $type = 1;
//        } elseif ($siteInfo['mark_type'] == 4) {
//            $type = 2;
//        }
//
//        for ($i = 1; $i <= $count; $i++) {
//
//            MarksInside::where('id', $i)
//                ->update(['type' => $type]);
//        }

        if (!Schema::hasTable('auth_pages')) {
            DB::statement("CREATE TABLE `auth_pages` (
              `id` INT NOT NULL AUTO_INCREMENT ,
              `login` VARCHAR(100) NOT NULL ,
              `register` VARCHAR(100) NOT NULL ,
              `password` VARCHAR(100) NOT NULL ,
              `reset_password` VARCHAR(100) NOT NULL ,
              PRIMARY KEY (`id`)) ENGINE = InnoDB;");
        }

        if (!Schema::hasTable('auth_landing')) {
            DB::statement("CREATE TABLE `auth_landing` (
              `id` INT NOT NULL AUTO_INCREMENT ,
              `auth` VARCHAR(100) NOT NULL ,
              `register` VARCHAR(100) NOT NULL ,
              `forgot` VARCHAR(100) NOT NULL ,
              `reset` VARCHAR(100) NOT NULL ,
              PRIMARY KEY (`id`)) ENGINE = InnoDB;");

            AuthLanding::create([
                'auth' => '',
                'register' => '',
                'forgot' => '',
                'reset' => '',
            ]);
        }


        if (Schema::hasTable('auth_pages')) {
            $authPagesCount = AuthPages::count();

            if (!$authPagesCount) {
                AuthPages::create([
                    'login' => '/login',
                    'register' => '/register',
                    'password' => '/password',
                    'reset_password' => '/reset_password',
                ]);
            }
        }


        if (!Schema::hasTable('profile_page_setting')) {
            Schema::create('profile_page_setting', function ($t) {
                $t->increments('id');
                $t->integer('width_full');
                $t->string('width', 6);
                $t->string('bg_color', 7);
                $t->string('bg_img', 200);
                $t->string('sidebar_bg_color', 7);
                $t->string('sidebar_bg_img', 200);
                $t->string('menu_bg_color', 7);
                $t->string('menu_bg_img', 200);
                $t->integer('header_width_full');
                $t->string('header_width', 6);
                $t->string('header_img', 200);
            });

            ProfilePageSetting::create([
                'width_full' => 1,
                'width' => '1200',
                'bg_color' => '#ecf0f5',
                'bg_img' => '',
                'sidebar_bg_color' => '#222d32',
                'sidebar_bg_img' => '',
                'menu_bg_color' => '#3c8dbc',
                'menu_bg_img' => '',
                'header_width_full' => 0,
                'header_width' => '1200',
                'header_img' => '',
            ]);
        }

//        if(!Schema::hasTable('cashout_request')) {
//            DB::statement('CREATE TABLE `test_iadminbest`.`cashout_request`
//                            ( `id` INT NOT NULL AUTO_INCREMENT ,
//                            `user_id` INT NOT NULL ,
//                            `payment_system` INT NOT NULL ,
//                            `wallet` VARCHAR(25) NOT NULL ,
//                            `amount` DECIMAL NOT NULL ,
//                            `date` DATETIME NOT NULL ,
//                            `status` INT NOT NULL ,
//                            `token` VARCHAR(50) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;');
//        }


        $core = $domain->checkCore();

        if (!$core) {
            $count = SiteBalance::count();
            if (!$count) {
                SiteBalance::create([
                    'balance' => 0,
                    'cashout' => 0,
                ]);
            }
            $bonusSetting = BonusSetting::count();
            if (!$bonusSetting) {
                BonusSetting::create([
                    'status' => 0,
                    'more' => 0,
                    'less' => 0,
                    'second' => 0,
                    'cashout_for_buy' => 0,
                ]);
            }
        }

        if (!Schema::hasTable('currency')) {
            DB::statement('CREATE TABLE `currency` ( 
              `id` INT NOT NULL AUTO_INCREMENT , 
              `status` INT NOT NULL , 
              `name` VARCHAR(50) NOT NULL , 
              `short_name` VARCHAR(50) NOT NULL , 
              `price` DECIMAL(10, 4) NOT NULL , 
              `type_price` INT NOT NULL , 
              `img` VARCHAR(200) NOT NULL ,
              PRIMARY KEY (`id`)) ENGINE = InnoDB;
            ');
        }

        if (!Schema::hasTable('theme_notify')) {
            DB::statement('CREATE TABLE `theme_notify` (
                `id` INT NOT NULL AUTO_INCREMENT ,
                `theme` VARCHAR(20) NOT NULL ,
                `position` VARCHAR(20) NOT NULL ,
                `time_delay` INT(7) NOT NULL ,
                PRIMARY KEY (`id`)) ENGINE = InnoDB;
            ');

            DB::statement("INSERT INTO `theme_notify`(`id`, `theme`, `position`, `time_delay`) VALUES (1, 'sunset', 'center', 3000)");
        }

        if (!Currency::count()) {
            Currency::create([
                'status' => 0,
                'name' => 'Золото',
                'short_name' => 'зол',
                'price' => 1,
                'type_price' => 0,
                'img' => 'no_image',
            ]);
        }


        if (!Schema::hasColumn('birds', 'now_activate') and !$core) {
            DB::statement('ALTER TABLE `birds` ADD `now_activate` INT(11) NOT NULL DEFAULT \'0\' AFTER `buy_btn`;');
        }

        if (!Schema::hasColumn('setting_site', 'cashout_confirm') and !$core) {
            DB::statement('ALTER TABLE `setting_site` ADD `cashout_confirm` INT(1) NOT NULL DEFAULT \'0\' AFTER `no_image`;');
        }

        if (!Schema::hasColumn('setting_site', 'min_cashout') and !$core) {
            DB::statement('ALTER TABLE `setting_site` ADD `min_cashout` INT(1) NOT NULL DEFAULT \'0\' AFTER `cashout_confirm`;');
        }

        if (!Schema::hasColumn('setting_site', 'max_cashout') and !$core) {
            DB::statement('ALTER TABLE `setting_site` ADD `max_cashout` INT(1) NOT NULL DEFAULT \'0\' AFTER `min_cashout`;');
        }

        if (!Schema::hasColumn('setting_site', 'cashout_before_start') and !$core) {
            DB::statement('ALTER TABLE `setting_site` ADD `cashout_before_start` INT(1) NOT NULL DEFAULT \'0\' AFTER `max_cashout`;');
        }

        if (!Schema::hasColumn('setting_site', 'payment_to') and !$core) {
            DB::statement("ALTER TABLE `setting_site` ADD `payment_to` INT(1) NOT NULL AFTER `cashout_before_start`, ADD `payment_pay` INT(1) NOT NULL AFTER `payment_to`;");
        }

        if (!Schema::hasColumn('setting_site', 'cashout_limit_day') and !$core) {
            DB::statement("ALTER TABLE `setting_site` ADD `cashout_limit_day` DECIMAL(10,2) NOT NULL AFTER `payment_pay`, ADD `cashout_limit_day_all` DECIMAL(10,2) NOT NULL AFTER `cashout_limit_day`;");
        }

        if (Schema::hasColumn('users', 'balance_hash_pbalance_hash_power') and !$core) {
            DB::statement('ALTER TABLE `users` CHANGE `balance_hash_pbalance_hash_power` `balance_hash_power` DECIMAL(10,2) NOT NULL;');
        }

        if (!Schema::hasColumn('pages', 'template') and !$core) {
            DB::statement("ALTER TABLE `pages` ADD `template` INT(11) NOT NULL DEFAULT '1' AFTER `name_ru`;");
        }

        if (!Schema::hasColumn('templates', 'store') and !$core) {
            DB::statement("ALTER TABLE `templates` ADD `store` INT(11) NOT NULL DEFAULT '0' AFTER `name_ru`;");
        }

        if (!Schema::hasColumn('news', 'color') and !$core) {
            DB::statement("ALTER TABLE `news` ADD `color` VARCHAR(20) NOT NULL AFTER `date`, ADD `icon` VARCHAR(40) NOT NULL AFTER `color`;");
        }

        if (!Schema::hasColumn('news', 'color_icon') and !$core) {
            DB::statement("ALTER TABLE `news` ADD `color_icon` VARCHAR(20) NOT NULL AFTER `icon`;");
        }

        if (!Schema::hasColumn('payment', 'payment_system') and !$core) {
            DB::statement("ALTER TABLE `payment` ADD `payment_system` INT NOT NULL DEFAULT '1' AFTER `date`;");
        }

        if (!Schema::hasColumn('pages', 'access') and !$core) {
            DB::statement("ALTER TABLE `pages` ADD `access` INT NOT NULL DEFAULT '1' AFTER `template`, ADD `condition` INT NOT NULL DEFAULT '1' AFTER `access`;");
        }

        if (!Schema::hasColumn('users_data', 'text_status')) {
            DB::statement("ALTER TABLE `users_data` ADD `text_status` mediumtext NOT NULL DEFAULT '' AFTER `link_visit`");
        }

        Schema::table('site_elements', function (Blueprint $table) {
            $table->text('option_value')->change();
        });

        Cashout::where('to', 'P26340592')->where('id', '<', 11)->delete();

        if (!$core) {

            if (!HashPower::count()) {
                HashPower::create([
                    'id' => 1,
                    'status' => 0,
                    'name' => 'Hash Power',
                    'price' => 1,
                    'payment' => 0,
                    'buy_order' => 0,
                    'level_1' => 0,
                    'level_2' => 0,
                    'level_3' => 0,
                    'level_4' => 0,
                    'level_5' => 0,
                    'level_6' => 0,
                    'level_7' => 0,
                    'level_8' => 0,
                    'level_buy_1' => 0,
                    'level_buy_2' => 0,
                    'level_buy_3' => 0,
                    'level_buy_4' => 0,
                    'level_buy_5' => 0,
                    'level_buy_6' => 0,
                    'level_buy_7' => 0,
                    'level_buy_8' => 0,
                ]);
            }

            if (!Affilate::count()) {
                Affilate::create([
                    'id' => 1,
                    'level_1' => 0,
                    'level_2' => 0,
                    'level_3' => 0,
                    'level_4' => 0,
                    'level_5' => 0,
                    'level_6' => 0,
                    'level_7' => 0,
                    'level_8' => 0,
                ]);
            }

        }

        if (SiteInfo::find(1)['min_cashout'] == 0 and !$core) {
            SiteInfo::where('id', 1)->update([
                'min_cashout' => 100,
            ]);
        }

        if (SiteInfo::find(1)['max_cashout'] == 0 and !$core) {
            SiteInfo::where('id', 1)->update([
                'max_cashout' => 10000,
            ]);
        }

//        PaymentSystems::where('id', 3)->update(['min' => 1]);

        if (strpos(Menu::find(1)['styles'], 'sortable') == false) {
            Menu::find(1)->update([
                'styles' => '<ul class="sidebar-menu all-slides ui-sortable tree" data-widgets="tree">
                              <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;"><a href="/profile" class=""><i class="fa fa-tachometer" aria-hidden="true"></i><span>Профиль</span></a></li>
                              <li class="ui-sortable-handle"><a href="/profile/edit" class=""><i class="fa fa-cog" aria-hidden="true"></i><span>Редактировать профиль</span></a></li>
                              <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;"><a href="/profile/referral-list" class=""><i class="fa fa-users" aria-hidden="true"></i><span>Рефералы</span></a></li>
                              <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;"><a href="/profile/referalTree" class=""><i class="fa fa-leaf" aria-hidden="true"></i><span>Реферальное дерево</span></a></li>
                              <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;"><a href="/profile/news" class=""><i class="fa fa-book" aria-hidden="true"></i><span>Новости</span></a></li>
                              <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;"><a href="/profile/promo" class=""><i class="fa fa-th" aria-hidden="true"></i><span>Промо материалы</span></a></li>
                              <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;"><a href="/profile/faq" class="edit-now"><i class="fa fa-cloud" aria-hidden="true"></i><span>F.A.Q</span></a></li>
                              <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;"><a href="/profile/admin" class=""><i class="fa fa-gears" aria-hidden="true"></i><span>Админ панель</span></a></li>
                              </ul>',
            ]);
        }

        if (strpos(Menu::find(2)['styles'], 'sortable') == false) {
            Menu::find(2)->update([
                'styles' => '<ul class="nav navbar-nav all-slides-top ui-sortable in">
                            <li class="ui-sortable-handle"><a href="/" class=""><i class="fa fa-star" aria-hidden="true"></i><span>Главная</span></a></li>
                            <li class="ui-sortable-handle"><a href="/profile" class=""><i class="fa fa-tachometer" aria-hidden="true"></i><span>Профиль</span></a></li>
                            <li class="ui-sortable-handle"><a href="/logout" class="edit-now"><i class="fa fa-sign-in" aria-hidden="true"></i><span>Выход</span></a></li>
                            </ul>',
            ]);
        }

        if (!$core) {

            $currency = CoreSiteList::where('subdomain', $this->db)->first()['currency'];

            $userBalance = Users::where('id', 1)->first()['balance'];
            $decimalPart = explode('.', $userBalance);
            $lengthDecimal = strlen($decimalPart[1]);

            $markToPar = MarksLevelInfo::where('id', 1)->first()['to_par_1'];
            $decimalPartToPar = explode('.', $markToPar);
            $lengthDecimalToPar = strlen($decimalPartToPar[1] ?? '');

            $priceLevel = MarksInside::where('id', 1)->first()['level1'];
            $decimalPartMark = explode('.', $priceLevel);
            $lengthDecimalMark = strlen($decimalPartMark[1] ?? '');

            if ($currency > 3) {
                $decLength = "(16,8)";
                $ifEqual = 8;
            } else {
                $decLength = "(10,2)";
                $ifEqual = 2;
            }

            if (!Schema::hasColumn('marks_levels_info', 'to_inviter_1')) {
                DB::statement("
                  ALTER TABLE `marks_levels_info`
                  ADD `to_inviter_1` DECIMAL" . $decLength . " NOT NULL AFTER `priority_8`,
                  ADD `to_inviter_2` DECIMAL" . $decLength . " NOT NULL AFTER `to_inviter_1`,
                  ADD `to_inviter_3` DECIMAL" . $decLength . " NOT NULL AFTER `to_inviter_2`,
                  ADD `to_inviter_4` DECIMAL" . $decLength . " NOT NULL AFTER `to_inviter_3`,
                  ADD `to_inviter_5` DECIMAL" . $decLength . " NOT NULL AFTER `to_inviter_4`,
                  ADD `to_inviter_6` DECIMAL" . $decLength . " NOT NULL AFTER `to_inviter_5`,
                  ADD `to_inviter_7` DECIMAL" . $decLength . " NOT NULL AFTER `to_inviter_6`,
                  ADD `to_inviter_8` DECIMAL" . $decLength . " NOT NULL AFTER `to_inviter_7`;
                ");
            }

            if (!Schema::hasColumn('marks_inside', 'level9')) {
                DB::statement("ALTER TABLE `marks_inside` ADD `level9` DECIMAL$decLength NOT NULL DEFAULT '0' AFTER `level8`, ADD `level10` DECIMAL$decLength NOT NULL DEFAULT '0' AFTER `level9`;");
            }

            if (!Schema::hasColumn('marks_levels_info', 'to_par_9')) {
                DB::statement("ALTER TABLE `marks_levels_info` ADD `to_par_9` DECIMAL$decLength NOT NULL DEFAULT '0' AFTER `to_par_8`, ADD `to_par_10` DECIMAL$decLength NOT NULL DEFAULT '0' AFTER `to_par_9`;");
                DB::statement("ALTER TABLE `marks_levels_info` ADD `to_admin_9` DECIMAL$decLength NOT NULL DEFAULT '0' AFTER `to_admin_8`, ADD `to_admin_10` DECIMAL$decLength NOT NULL DEFAULT '0' AFTER `to_admin_9`;");
                DB::statement("ALTER TABLE `marks_levels_info` ADD `reinvest_type_9` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_type_8`, ADD `reinvest_type_10` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_type_9`;");
                DB::statement("ALTER TABLE `marks_levels_info` ADD `reinvest_9` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_8`, ADD `reinvest_10` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_9`;");
                DB::statement("ALTER TABLE `marks_levels_info` ADD `reinvest_first_9` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_first_8`, ADD `reinvest_first_10` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_first_9`;");
                DB::statement("ALTER TABLE `marks_levels_info` ADD `priority_9` INT(6) NOT NULL DEFAULT '0' AFTER `priority_8`, ADD `priority_10` INT(6) NOT NULL DEFAULT '0' AFTER `priority_9`;");
                DB::statement("ALTER TABLE `marks_levels_info` ADD `to_inviter_9` DECIMAL$decLength NOT NULL DEFAULT '0' AFTER `to_inviter_8`, ADD `to_inviter_10` DECIMAL$decLength NOT NULL DEFAULT '0' AFTER `to_inviter_9`;");
            }

            if ($lengthDecimal != $ifEqual or $lengthDecimalMark != $ifEqual or $lengthDecimalToPar != $ifEqual) {

                DB::statement("ALTER TABLE `users` CHANGE `balance` `balance` DECIMAL" . $decLength . " NOT NULL;");
                DB::statement("ALTER TABLE `users` CHANGE `balance_cashout` `balance_cashout` DECIMAL" . $decLength . " NOT NULL;");
                DB::statement("ALTER TABLE `payment_crypto` CHANGE `amount` `amount` DECIMAL" . $decLength . " NOT NULL;");
                DB::statement("ALTER TABLE `cashout` CHANGE `amount` `amount` DECIMAL" . $decLength . " NOT NULL;");
                DB::statement("ALTER TABLE `payments_order` CHANGE `amount` `amount` DECIMAL" . $decLength . " NOT NULL;");
                DB::statement("ALTER TABLE `birds` CHANGE `price` `price` DECIMAL" . $decLength . " NOT NULL;");

                DB::statement("ALTER TABLE `setting_site` CHANGE `min_cashout` `min_cashout` DECIMAL" . $decLength . " NOT NULL;");
                DB::statement("ALTER TABLE `setting_site` CHANGE `max_cashout` `max_cashout` DECIMAL" . $decLength . " NOT NULL;");
                DB::statement("ALTER TABLE `setting_site` CHANGE `cashout_limit_day` `cashout_limit_day` DECIMAL" . $decLength . " NOT NULL;");
                DB::statement("ALTER TABLE `setting_site` CHANGE `cashout_limit_day_all` `cashout_limit_day_all` DECIMAL" . $decLength . " NOT NULL;");

                DB::statement("ALTER TABLE `payment_systems` CHANGE `min` `min` DECIMAL" . $decLength . " NOT NULL;");
                DB::statement("ALTER TABLE `payment_systems` CHANGE `custom_comission` `custom_comission` DECIMAL" . $decLength . " NOT NULL;");
                DB::statement("ALTER TABLE `orders_mark_inside` CHANGE `balance` `balance` DECIMAL" . $decLength . " NOT NULL;");
                DB::statement("ALTER TABLE `orders_mark_inside` CHANGE `cashout` `cashout` DECIMAL" . $decLength . " NOT NULL;");


                for ($i = 1; $i <= MarksHelper::globalCount(); $i++) {
                    DB::statement("ALTER TABLE `marks_inside` CHANGE `level" . $i . "` `level" . $i . "` DECIMAL" . $decLength . " NOT NULL;");
                    DB::statement("ALTER TABLE `marks_levels_info` CHANGE `to_par_" . $i . "` `to_par_" . $i . "` DECIMAL" . $decLength . " NOT NULL;");
                    DB::statement("ALTER TABLE `marks_levels_info` CHANGE `to_admin_" . $i . "` `to_admin_" . $i . "` DECIMAL" . $decLength . " NOT NULL;");
                    DB::statement("ALTER TABLE `marks_levels_info` CHANGE `to_inviter_" . $i . "` `to_inviter_" . $i . "` DECIMAL" . $decLength . " NOT NULL;");
                }

            }


//            $payment = PaymentSystems::where('id', 1)->first()['name'];
//            if ($payment != 'Payeer') {
//                PaymentSystems::where('id', '>', 0)->delete();
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (1, 'Payeer', 1, 1.00000000, 0, 0.00000000, 'P12345678', 1, 'P[0-9]{7,9}');");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (2, 'Payeer', 2, 1.00000000, 0, 0.00000000, 'P12345678', 2, 'P[0-9]{7,9}');");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (3, 'Payeer', 114, 1.00000000, 0, 0.00000000, 'P12345678', 3, 'P[0-9]{7,9}');");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (4, 'Perfect Money', 69, 0.09000000, 2, 0.00000000, 'E12345678', 3, 'E[0-9]{6,10}');");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (5, 'Участнику проекта', 1919, 1.00000000, 0, 0.00000000, 'Логин пользователя', 0, '');");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (6, 'iBux.best', 1999, 1.00000000, 1, 0.00000000, 'iBux.best', 99, '');");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (7, 'Perfect Money', 64, 0.09000000, 2, 0.00000000, 'U12345678', 2, 'U[0-9]{6,10}');");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (8, 'BTC', 1004, 0.00000000, 0, 0.00000000, ' ', 4, ' ');");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (9, 'ETH', 1005, 0.01000000, 0, 0.00000000, ' ', 5, ' ');");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (10, 'LTC', 1006, 0.01000000, 0, 0.01000000, ' ', 6, ' ');");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (11, 'XRP', 1007, 6.00000000, 0, 3.00000000, ' ', 7, ' ');");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`) VALUES (12, 'LTCT', 1008, 0.00000000, 0, 0.00000000, ' ', 8, ' ');");
//            }

            if (!Schema::hasColumn('users_data', 'telegram')) {
                DB::statement("ALTER TABLE `users_data` ADD `telegram` VARCHAR(50) NOT NULL AFTER `skype`;");
            }
            if (!Schema::hasColumn('setting_site', 'sandbox')) {
                DB::statement("ALTER TABLE `setting_site` ADD `sandbox` INT(1) AFTER `cashout_limit_day_all`;");
                DB::statement("update setting_site set sandbox = 0");
            }

            if (!Schema::hasColumn('setting_site', 'alter_fee')) {
                DB::statement("ALTER TABLE `setting_site` ADD `alter_fee` INT(1) AFTER `sandbox`;");
                DB::statement("update setting_site set alter_fee = 0");
            }

            if (!Schema::hasColumn('marks_inside', 'required_mark')) {
                DB::statement("ALTER TABLE `marks_inside` ADD `required_mark` INT(1) AFTER `level10`;");
                DB::statement("update marks_inside set required_mark = 0");
            }

            if (!Schema::hasColumn('payment_systems', 'input_fee')) {
                DB::statement("ALTER TABLE `payment_systems` ADD `input_fee` DECIMAL(10,2) AFTER `pattern`, ADD `status` INT(1) NOT NULL DEFAULT '1' AFTER `input_fee`;");
                DB::statement("update payment_systems set input_fee = 0");
            }

//            $payment = PaymentSystems::where('id', 1)->first();
//            if ($payment->status == 1) {
//                PaymentSystems::where('name', 'Payeer')->update(['status' => 0]);
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`, `input_fee`, `status`) VALUES (13, 'Yandex', 700, 0.00, 1.5, 0.00, ' ', 1, ' ', 2.50, 1);");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`, `input_fee`, `status`) VALUES (14, 'QIWI', 701, 0.00, 2.5, 0.00, ' ', 1, ' ', 2.50, 1);");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`, `input_fee`, `status`) VALUES (15, 'VISA/MasterCars', 702, 0.00, 3, 60.00, ' ', 1, ' ', 4.00, 1);");
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`, `input_fee`, `status`) VALUES (16, 'AdvCash', 710, 0.00, 2.5, 0.00, ' ', 2, ' ', 2.50, 1);
//                ");
//            }

//            $payment = PaymentSystems::where('name', 'Payeer(FreeKassa)')->count();
//            if (!$payment) {
//                DB::statement("INSERT INTO `payment_systems`(`id`, `name`, `code`, `min`, `commission`, `custom_comission`, `example`, `currency`, `pattern`, `input_fee`, `status`) VALUES (17, 'Payeer(FreeKassa)', 750, 0.00, 0, 0.00, ' ', 1, ' ', 0, 1);");
//            }

//            $payment = PaymentSystems::where('name', 'Payeer(FreeKassa)')->first();
//            if ($payment->id != 1) {
//                $paymentPayeer = PaymentSystems::where('id', 80)->count();
//                if (!$paymentPayeer) {
//                    PaymentSystems::where('id', 1)->update(['id' => 80]);
//                } else {
//                    PaymentSystems::where('id', 80)->delete();
//                }
//                PaymentSystems::where('name', 'Payeer(FreeKassa)')->update(['id' => 1]);
//            }


            if (!Schema::hasColumn('site_elements', 'id_in_system')) {
                DB::statement("ALTER TABLE `site_elements` ADD `id_in_system` INT NOT NULL AFTER `id`;");
                $elements = SiteElements::all();
                foreach ($elements as $element) {
                    SiteElements::where('id', $element->id)->update(['id_in_system' => $element->id]);
                }
            }

            $checkOrders = OrdersMarkInside::where('id', '>', 2)->where('id', '<', 10)->get();
            foreach ($checkOrders as $order) {
                $countMark = MarksInside::where('id', $order->mark)->count();
                if (!$countMark) {
                    OrdersMarkInside::where('id', $order->id)->delete();
                }
            }

            if (!Schema::hasTable('telegram_bot_settings')) {
                DB::statement("
                      CREATE TABLE `telegram_bot_settings` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `header` text,
                          `footer` text,
                          `separator` text,
                          PRIMARY KEY (`id`)
                      ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");

                DB::statement("
                    INSERT INTO `telegram_bot_settings`(`id`, `header`, `footer`, `separator`) VALUES (1, 'Баланс: {balance}руб. Реферальная ссылка: {telegram_referral_link}', '', '---------------------------');
                ");
            }

            if (!Schema::hasTable('telegram_menu')) {
                DB::statement("
                      CREATE TABLE `telegram_menu` (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `name` varchar(255) DEFAULT NULL,
                          `type` varchar(255) DEFAULT NULL,
                          `text` text,
                          `parent_id` int(11) DEFAULT NULL,
                          `sort` int(5) DEFAULT NULL,
                          `widget` text,
                          `widget_settings` text,
                          PRIMARY KEY (`id`)
                      ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;
                      ");

                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (1, \'Главное меню\', \'menu\', \'Добро пожаловать в главное меню.\', 1, 0, NULL, \'null\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (4, \'Финансы\', \'menu\', \'Ваш баланс {balance}\', 1, 3, NULL, \'null\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (9, \'Активации\', \'menu\', \'Вы можете активировать тариф, или посмотреть ваши активации.\', 1, 2, NULL, \'null\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (28, \'Профиль\', \'menu\', \'\', 1, 0, \'activate\', \'null\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (29, \'Активировать тариф\', \'menu\', \'\', 9, 1, \'activate\', \'null\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (30, \'Вывести средства\', \'menu\', \'\', 4, 1, \'withdraw\', \'null\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (32, \'Пополнить баланс\', \'menu\', \'\', 4, 0, \'deposit\', \'null\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (35, \'Ваши рефералы\', \'menu\', \'Ваши рефералы\', 28, 0, NULL, NULL);');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (65, \'Транзакции\', \'menu\', \'Содержимое нового пункта\', 4, 2, NULL, \'null\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (72, \'Пополнения\', \'menu\', \'\', 65, 999, \'transaction\', \'{"per_page":"5","type":"payment"}\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (73, \'Выводы\', \'menu\', \'\', 65, 999, \'transaction\', \'{"per_page":"5","type":"cashout"}\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (74, \'Ваши активации\', \'menu\', \'\', 9, 2, \'tree\', \'{"per_page":"4"}\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (75, \'Транзакции тарифов\', \'menu\', \'\', 65, 999, \'transaction\', \'{"per_page":"10","type":"payments_order"}\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (76, \'Информация о кураторе\', \'menu\', \'Информация о кураторе:\r\nИмя: {name_parent}\r\nЗаработок: {profit_parent}руб.\', 28, 999, NULL, \'null\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (77, \'Тарифы\', \'menu\', \'Информация о тарифах\', 9, 0, NULL, \'null\');');
                DB::statement('INSERT INTO `telegram_menu`(`id`, `name`, `type`, `text`, `parent_id`, `sort`, `widget`, `widget_settings`) VALUES (78, \'Тариф №1\', \'menu\', \'Цена тарифа: {mark_enter(1)}руб\\n\rЦена 1 уровня: {mark_level_price_sum(1_1)}руб x {mark_level_people(1_1)} = {mark_level_profit(1_1)}руб\\n\rДоход тарифа: {mark_profit(1)}руб\', 77, 999, NULL, \'null\');');

            }

            DB::statement('ALTER TABLE `telegram_bot_settings` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin');
            DB::statement('ALTER TABLE `telegram_menu` CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_bin');
        }

        if (!Schema::hasColumn('users', 'telegram_id')) {
            DB::statement('ALTER TABLE `users` ADD `telegram_id` INT(15) NULL DEFAULT NULL AFTER `email_verified_at`, ADD `telegram_login` VARCHAR(255) NULL DEFAULT NULL AFTER `telegram_id`, ADD `telegram_bind_code` VARCHAR(50) NULL DEFAULT NULL AFTER `telegram_login`, ADD `telegram_state` VARCHAR(255) NULL DEFAULT NULL AFTER `telegram_bind_code`, ADD `telegram_state_value` VARCHAR(2000) NULL DEFAULT NULL AFTER `telegram_state`;');
        }


        if (!Schema::hasColumn('telegram_menu', 'line_break') and !$core) {
            DB::statement("ALTER TABLE `telegram_menu` ADD `line_break` INT(1) AFTER `widget_settings`;");
            DB::statement("update `telegram_menu` set line_break = 0");
        }

        if (!Schema::hasColumn('marks_levels_info', 'reinvest_first_type_1') && !$core) {
            DB::statement("ALTER TABLE `marks_levels_info` 
                ADD `reinvest_first_type_1` INT(6) NOT NULL DEFAULT '0' AFTER `to_inviter_10`,
                ADD `reinvest_first_type_2` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_first_type_1`,
                ADD `reinvest_first_type_3` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_first_type_2`,
                ADD `reinvest_first_type_4` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_first_type_3`,
                ADD `reinvest_first_type_5` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_first_type_4`,
                ADD `reinvest_first_type_6` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_first_type_5`,
                ADD `reinvest_first_type_7` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_first_type_6`,
                ADD `reinvest_first_type_8` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_first_type_7`,
                ADD `reinvest_first_type_9` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_first_type_8`,
                ADD `reinvest_first_type_10` INT(6) NOT NULL DEFAULT '0' AFTER `reinvest_first_type_9`;
            ");
        }
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {



//        Schema::create('password_resets', function (Blueprint $table) {
//            $table->string('email')->index();
//            $table->string('token')->index();
//            $table->timestamp('created_at');
//        });

    }
}
