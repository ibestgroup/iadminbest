<?php

namespace App\Providers;

use App\Models\Users;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;

class ReferralLink extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        if (isset($_GET['id'])) {
            Cookie::queue('refer_id', $_GET['id'], 86000);
        }


        if (isset($_GET['ref'])) {
            Cookie::queue('refer_login', $_GET['ref'], 86000);
        }
    }
}
