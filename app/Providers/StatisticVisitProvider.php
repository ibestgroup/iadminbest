<?php

namespace App\Providers;

use App\Libraries\Domain;
use App\Models\CoreSiteList;
use App\Models\StatisticVisit;
use App\Models\Users;
use App\Models\UsersData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Illuminate\Support\Facades\Schema;


class StatisticVisitProvider extends ServiceProvider
{
    public $toDay = '';
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        if (!isset($_SERVER['REQUEST_URI'])) {
            $_SERVER['REQUEST_URI'] = '';
        }
        $getLogin = $request->input('ref');

        if (isset($getLogin)) {
            $getId = Users::where('login', $getLogin)->first()['id'];
        } else {
            $getId = $request->input('id');
        }


        if (isset($getId)) {
            UsersData::where('id_user', $getId)->increment('link_visit');
        }

        $path = $request->path();
        $this->toDay = date("Y-m-d");
        if(Schema::hasTable('statistic')) {

            if (StatisticVisit::where('day', $this->toDay)->count() == 0) {
                StatisticVisit::create([
                    'day' => $this->toDay,
                    'view' => 0,
                    'visit' => 0,
                    'view_prof' => 0,
                    'visit_prof' => 0,
                    'registration' => 0,
                    'payments' => 0,
                    'payments_sum' => 0,
                    'orders' => 0
                ]);
            }


            $checkExtension = preg_match("/.+\.(.*?)/", $_SERVER['REQUEST_URI'], $checkExtension);
            $checkWidget = preg_match("~widgets~", $_SERVER['REQUEST_URI'], $checkWidget);
            $checkGet = preg_match("~\?.+\=~", $_SERVER['REQUEST_URI'], $checkGet);

//        $log = new Logger('name');
//        $log->pushHandler(new StreamHandler('log.log', Logger::WARNING));
//        $log->warning('------------ ЛОГ СЕРВЕРА ---------', array($_SERVER['REQUEST_URI']));

            /**
             * view
             */

            if (!$checkExtension AND !$checkWidget AND !$checkGet) {
                StatisticVisit::where('day', $this->toDay)->increment('view', 1);

                /**
                 * visit
                 */

                if (isset($_COOKIE['visit'])) {

                    if ($_COOKIE['visit'] != $this->toDay) {
                        setcookie("visit", $this->toDay, time() + 86400);
                        StatisticVisit::where('day', $this->toDay)->increment('visit', 1);
                    }
                } else {
                    setcookie("visit", $this->toDay, time() + 86400);
                    StatisticVisit::where('day', $this->toDay)->increment('visit', 1);
                }

                /**
                 * view_prof and visit_prof
                 */

                if (preg_match('~profile~', $path)) {
                    StatisticVisit::where('day', $this->toDay)->increment('view_prof', 1);

                    if (isset($_COOKIE['visit_prof'])) {

                        if ($_COOKIE['visit_prof'] != $this->toDay) {
                            setcookie("visit_prof", $this->toDay, time() + 86400);
                            StatisticVisit::where('day', $this->toDay)->increment('visit_prof', 1);
                        }

                    } else {
                        setcookie("visit_prof", $this->toDay, time() + 86400);
                        StatisticVisit::where('day', $this->toDay)->increment('visit_prof', 1);
                    }


                }


            }
        }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
