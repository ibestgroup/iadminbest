<?php

namespace App\Libraries;

use App\Http\Controllers\TelegramBotController;
use App\Libraries\Multimark;
use App\Models\CoreProfit;
use App\Helpers\MarksHelper;
use App\Models\MarksInside;
use App\Models\MarksLevelInfo;
use App\Models\Notice;
use App\Models\OrdersMarkInside;
use App\Models\Transaction;
use App\Models\Users;
use App\Models\PaymentsOrder;
use App\Models\SiteInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Activate
{
    public $fee = 10;
    public $partnersPrice = 70;

    /**
     * $type - 'activate', 'reinvest', 'levelUp', 'reinvest_first'
     */
    public function activate($us_id, $tarif, $tarifId, $type = 'activate', $reinvestTypePrev = 0, $fromLevel = 0, $db = '') {
        if ($db) {
            try {
                if (Config::get("database.connections.activate")) {
                    DB::disconnect('activate');
                }
                Config::set("database.connections.activate", [
                    "host" => "127.0.0.1",
                    "database" => $db,
                    "username" => "root",
                    "password" => "DrinkVodkaPlayDotka228",
                    'driver' => 'mysql',
                    'strict' => false,
                ]);

                DB::setDefaultConnection('activate');
            } catch (\Exception $e) {
                Log::info($e);
            }
        }

        if (SiteInfo::on('activate')->find(1)->alter_fee) {
            $this->fee = 0;
        }
//        DB::beginTransaction();
//        OrdersMarkInside::raw('SET TRANSACTION ISOLATION LEVEL REPEATABLE READ');

        $levelsGlob = MarksHelper::globalCount();

        $fromWallet = 0;
        if ($tarifId) $fromWallet = $tarifId;

//        if ($tarifId) {
//            $checkAdminOrder = OrdersMarkInside::where('id', $tarifId)->first();
//            if ($checkAdminOrder->par_or == $tarifId and $type = 'reinvest') {
//                DB::commit();
//                goto nextAdmin;
//            }
//        }


        $ordersMark = OrdersMarkInside::class;

        $markInfo = MarksInside::find($tarif);
        $markLevelInfo = MarksLevelInfo::find($tarif);
        $overflow = $markInfo->overflow;

        $user = Users::find($us_id);

//        $markProfit = MarksHelper::markProfit($tarif);
//        $countOrder = $ordersMark::where('id_user', $us_id)->where('mark', $tarif)->count();

        $orderItem['level'] = 0;

        if ($tarifId) { // Если указан id тарифа, то берем запись тарифа
            $orderItem = $ordersMark::where('id', $tarifId)->first();
        }

        switch($overflow) {
            case 1:
                $overflowObj = new Multimark($user['id'], $tarif, true);
                $overflowRes = $overflowObj->overflowMulti();
                break;
            case 2:
                $overflowObj = new Multimark(1, $tarif, true);
                $overflowRes = $overflowObj->overflowMulti();
                break;
            default:
                $countOrders = $ordersMark::where('id_user', $user['refer'])->where('mark', $tarif)->where('status', 0)->count();
                if ($countOrders) {
                    $overflowRes = $ordersMark::where('id_user', $user['refer'])->where('mark', $tarif)->where('status', 0)->first()['id'];
                } else {
                    $overflowRes = $ordersMark::where('id_user', 1)->where('mark', $tarif)->where('status', 0)->first()['id'];
                }
        }

        for ($i = 0; $i < MarksHelper::globalCount(); $i++) {
            $l = $i+1;
            $priceLevels[$i] = $markInfo['level' . $l];
        }

        // Определяем от кого считать вышестоящих
        // Если указан $tarifId который не закрыт, берем его, если нет, то куда сработал перелив


        if ($orderItem['level'] < $markInfo['level'] and $tarifId and $type != 'reinvest' and $type != 'reinvest_first') {
            $massive[] = $ordersMark::where('id', $orderItem['par_or'])->first();
        } else {
            $massive[] = $ordersMark::where('id', $overflowRes)->first();
        }

        for ($i = 0; $i < $markInfo['level']; $i++) {
            $parentOrders = $massive[$i];
            $massive[$i] = $parentOrders;

            $referOrder = $massive[$i]['par_or'];

            $parentOrders['full_price'] = $priceLevels[$i];
            $parentOrders['price'] =  $priceLevels[$i]/100*(100 - $this->fee);

            if ($i + 1 < $markInfo['level']) {
                $massive[] = $ordersMark::where('id', $referOrder)->first();
            }
        }

        // Если максимальный уровень, то начинаем активировать первый.
        if ($orderItem['level'] >= $markInfo['level'] or $type == 'reinvest' or $type == 'reinvest_first') {
            $level = 1; $orderItem['level'] = 0;
        } else {
            $level = $orderItem['level'] + 1;
        }

        // Считаем общую цену за уровень
        $priceToPar = Helpers::myRound($markLevelInfo['to_par_'.$level]);
        $priceToAdmin = Helpers::myRound($markLevelInfo['to_admin_'.$level]);

        $pricesToInviter = 0;
        $l = 1;
        while ($l <= $levelsGlob) {
            $pricesToInviter += $markLevelInfo['to_inviter_'.$l];
            $l++;
        }

        $resultPrice = $priceLevels[$orderItem['level']] + $priceToPar + $priceToAdmin + $pricesToInviter;


        if ($markInfo['type'] == 2) {
            $resultPrice = MarksHelper::markEnter($tarif);
        }

        if ($tarifId == 0) {
            $balance = $user['balance'];
        } else {
            $balance = $orderItem['balance'] - $orderItem['cashout'];
        }

        if ($priceToPar < 0 or $priceToAdmin < 0 or $priceLevels[$orderItem['level']] < 0) {
            DB::rollBack();
            throw new \Exception("Что-то пошло не так.");
        }

        $balance = Helpers::myRound($balance);
        $resultPrice = Helpers::myRound($resultPrice);

        if ($balance < $resultPrice and $tarifId != 0) {
            DB::commit();
        } else {

            $levelOrder = $orderItem['level'];
            $parentOrders = $massive[$levelOrder];
            $toUser = $parentOrders['id_user'];
            $firstParentOrder = $massive[0]['id'];

            Users::find($us_id)->update(['type_follow' => 1]);

            if (in_array($type, ['activate', 'reinvest', 'reinvest_first'])) {

                $priceReinvest = MarksHelper::calcMarkPrice($tarif);

                $countLevel = 1;
                if ($markInfo['type'] == 2) {
                    $countLevel = $markInfo['level'];
                }

                if ($type != 'activate') {

                    $ordersMark::where('id', $tarifId)->increment('cashout', $priceReinvest);
                    $ordersMark::where('id', $tarifId)->update(['reinvest' => 1]);

                }

                $ordersMark::create([
                    'id_user' => $us_id,
                    'parent' => $parentOrders['id_user'],
                    'mark' => $tarif,
                    'level' => 1,
                    'balance' => 0,
                    'pay_people' => 0,
                    'status' => 0,
                    'par_or' => $firstParentOrder,
                    'date' => now(),
                    'reinvest' => 0
                ]);
                for ($i = 1; $i <= $countLevel; $i++) {

                    $priceToPar = $markLevelInfo['to_par_'.$i];
                    $priceToAdmin = $markLevelInfo['to_admin_'.$i];

                    $parentOrders = $massive[$i-1];
                    $parentOrders['price'] = Helpers::myRound($parentOrders['price']);
                    $ordersMark::where('id', $parentOrders['id'])->increment('balance', $parentOrders['price']);

                    $transaction = PaymentsOrder::create([
                        'from_user' => $us_id,
                        'to_user' => $toUser,
                        'from_wallet' => $fromWallet,
                        'wallet' => $parentOrders['id'],
                        'from_level' => $fromLevel,
                        'level' => $i,
                        'amount' => $parentOrders['price'],
                        'date' => now(),
                        'direct' => 0,
                        'mark' => $tarif,
                        'type' => $type,
                    ]);

                    Transaction::create([
                        'transaction_id' => $transaction->id,
                        'from_user' => $us_id,
                        'to_user' => $toUser,
                        'type' => 'payments_order',
                        'date' => now(),
                    ]);

                    if ($priceToAdmin > 0) {

                        $priceToAdmin = Helpers::myRound($priceToAdmin / 100 * (100 - $this->fee));

                        Users::where('id', 1)->increment('balance', $priceToAdmin);
                        $transaction = PaymentsOrder::create([
                            'from_user' => $us_id,
                            'to_user' => 1,
                            'from_wallet' => $fromWallet,
                            'wallet' => 'to_admin',
                            'from_level' => $fromLevel,
                            'level' => $i,
                            'amount' => $priceToAdmin,
                            'date' => now(),
                            'direct' => 0,
                            'mark' => $tarif,
                            'type' => $type,
                        ]);

                        Transaction::create([
                            'transaction_id' => $transaction->id,
                            'from_user' => $us_id,
                            'to_user' => 1,
                            'type' => 'payments_order',
                            'date' => now(),
                        ]);
                    }

                    if ($priceToPar > 0) {

                        $priceToPar = Helpers::myRound($priceToPar / 100  * (100 - $this->fee));

                        Users::where('id', $user['refer'])->increment('balance', $priceToPar);
                        $transaction = PaymentsOrder::create([
                            'from_user' => $us_id,
                            'to_user' => $user['refer'],
                            'from_wallet' => $fromWallet,
                            'wallet' => 'to_partner',
                            'from_level' => $fromLevel,
                            'level' => $i,
                            'amount' => $priceToPar,
                            'date' => now(),
                            'direct' => 0,
                            'mark' => $tarif,
                            'type' => $type,
                        ]);

                        Transaction::create([
                            'transaction_id' => $transaction->id,
                            'from_user' => $us_id,
                            'to_user' => $user['refer'],
                            'type' => 'payments_order',
                            'date' => now(),
                        ]);

                        Notice::create([
                            'user_id' => $user['refer'],
                            'type' => 'finance',
                            'subtype' => 'payment_order_parent',
                            'head' => $priceToPar.' руб. от реферала',
                            'text' => 'Вы получили '.$priceToPar.' руб. от прямого реферала! Продолжайте в том же духе.',
                            'view' => 0,
                            'date' => now(),
                        ]);
                        TelegramBotController::sendNotice(
                            $us_id,
                            'Вы получили '.$priceToPar.' руб. от прямого реферала! Продолжайте в том же духе.',
                            $db
                        );
                    }

                    if ($i == 1) {

                        $pricesToInviter = [];
                        $l = 1;
                        $inviters[1] = $user['refer'];
                        while ($l <= MarksHelper::globalCount()) {
                            $pricesToInviter[$l] = Helpers::myRound($markLevelInfo['to_inviter_'.$l]);
                            $inviters[$l + 1] = Users::where('id', $inviters[$l])->first()->refer;
                            $l++;
                        }


                        foreach ($pricesToInviter as $key => $priceToInviter) {

                            if ($priceToInviter > 0) {
//                                    $priceToInviter = round($priceToInviter / 100, Helpers::siteCurrency('round'), PHP_ROUND_HALF_DOWN) * (100 - $this->fee);
                                $priceToInviter = Helpers::myRound($priceToInviter / 100  * (100 - $this->fee));
                                Users::where('id', $inviters[$key])->increment('balance', $priceToInviter);
                                $transaction = PaymentsOrder::create([
                                    'from_user' => $us_id,
                                    'to_user' => $inviters[$key],
                                    'from_wallet' => $fromWallet,
                                    'wallet' => 'to_inviter',
                                    'from_level' => $fromLevel,
                                    'level' => $key,
                                    'amount' => $priceToInviter,
                                    'date' => now(),
                                    'direct' => 0,
                                    'mark' => $tarif,
                                    'type' => $type,
                                ]);

                                Transaction::create([
                                    'transaction_id' => $transaction->id,
                                    'from_user' => $us_id,
                                    'to_user' => $inviters[$key],
                                    'type' => 'payments_order',
                                    'date' => now(),
                                ]);

//                                    Notice::create([
//                                        'user_id' => $inviters[$key],
//                                        'type' => 'finance',
//                                        'subtype' => 'payment_order_parent',
//                                        'head' => $priceToPar.' руб. от реферала',
//                                        'text' => 'Вы получили '.$priceToPar.' руб. от прямого реферала! Продолжайте в том же духе.',
//                                        'view' => 0,
//                                        'date' => now(),
//                                    ]);
                            }
                        }
                    }


                    $nextAction = new Activate();
                    $nextAction->nextAction($parentOrders['id'], $i);

                }


            }

            if ($type == 'level_up' and $markInfo['type'] == 1) {

                if ($balance >= $resultPrice) {
                    $parentOrders['price'] = Helpers::myRound($parentOrders['price']);
                    $ordersMark::where('id', $tarifId)->increment('cashout', $resultPrice);
                    $ordersMark::where('id', $parentOrders['id'])->increment('balance', $parentOrders['price']);

                    $transaction = PaymentsOrder::create([
                        'from_user' => $us_id,
                        'to_user' => $toUser,
                        'from_wallet' => $fromWallet,
                        'wallet' => $parentOrders['id'],
                        'from_level' => $fromLevel,
                        'level' => $level,
                        'amount' => $parentOrders['price'],
                        'date' => now(),
                        'direct' => 0,
                        'mark' => $tarif,
                        'type' => 'level_up',
                    ]);

                    Transaction::create([
                        'transaction_id' => $transaction->id,
                        'from_user' => $us_id,
                        'to_user' => $toUser,
                        'type' => 'payments_order',
                        'date' => now(),
                    ]);
                }

                $ordersMark::where('id', $tarifId)->update(['level' => $level]);

                Notice::create([
                    'user_id' => $us_id,
                    'type' => 'account',
                    'subtype' => 'level_up',
                    'head' => 'Ваш уровень в тарифе повышен до '.$level.'!',
                    'text' => 'Сработало автоматическое повышение уровня! Так держать!',
                    'view' => 0,
                    'date' => now()
                ]);

                TelegramBotController::sendNotice(
                    $us_id,
                    'Сработало автоматическое повышение уровня! Так держать!',
                    $db
                );

//                $priceToAdmin = round($priceToAdmin / 100 * (100 - $this->fee), Helpers::siteCurrency('round'), PHP_ROUND_HALF_DOWN);
//
//                if ($priceToAdmin) {
//                    Users::where('id', 1)->increment('balance', $priceToAdmin);
//                    $transaction = PaymentsOrder::create([
//                        'from_user' => $us_id,
//                        'to_user' => 1,
//                        'from_wallet' => $fromWallet,
//                        'wallet' => 'to_partner',
//                        'from_level' => $fromLevel,
//                        'level' => $level,
//                        'amount' => $priceToAdmin,
//                        'date' => now(),
//                        'direct' => 0,
//                        'mark' => $tarif,
//                        'type' => $type,
//                    ]);
//
//                    Transaction::create([
//                        'transaction_id' => $transaction->id,
//                        'from_user' => $us_id,
//                        'to_user' => 1,
//                        'type' => 'payments_order',
//                        'date' => now(),
//                    ]);
//                }

                if ($priceToAdmin) {
                    $priceToAdmin = Helpers::myRound($priceToAdmin / 100 * (100 - $this->fee));

                    Users::where('id', 1)->increment('balance', $priceToAdmin);
                    $transaction = PaymentsOrder::create([
                        'from_user' => $us_id,
                        'to_user' => 1,
                        'from_wallet' => $fromWallet,
                        'wallet' => 'to_admin',
                        'from_level' => $fromLevel,
                        'level' => $i,
                        'amount' => $priceToAdmin,
                        'date' => now(),
                        'direct' => 0,
                        'mark' => $tarif,
                        'type' => $type,
                    ]);

                    Transaction::create([
                        'transaction_id' => $transaction->id,
                        'from_user' => $us_id,
                        'to_user' => 1,
                        'type' => 'payments_order',
                        'date' => now(),
                    ]);
                }

                if ($priceToPar) {
                    $priceToPar = Helpers::myRound($priceToPar / 100  * (100 - $this->fee));

                    Users::where('id', $user['refer'])->increment('balance', $priceToPar);
                    $transaction = PaymentsOrder::create([
                        'from_user' => $us_id,
                        'to_user' => $user['refer'],
                        'from_wallet' => $fromWallet,
                        'wallet' => 'to_partner',
                        'from_level' => $fromLevel,
                        'level' => $level,
                        'amount' => $priceToPar,
                        'date' => now(),
                        'direct' => 0,
                        'mark' => $tarif,
                        'type' => $type,
                    ]);

                    Transaction::create([
                        'transaction_id' => $transaction->id,
                        'from_user' => $us_id,
                        'to_user' => $user['refer'],
                        'type' => 'payments_order',
                        'date' => now(),
                    ]);

                    TelegramBotController::sendNotice(
                        $us_id,
                        'Вы получили '.$priceToPar.' руб. от прямого реферала! Продолжайте в том же духе.',
                        $db
                    );

                    Notice::create([
                        'user_id' => $user['refer'],
                        'type' => 'finance',
                        'subtype' => 'payment_order_parent',
                        'head' => $priceToPar.' руб. от реферала',
                        'text' => 'Вы получили '.$priceToPar.' руб. от прямого реферала! Продолжайте в том же духе.',
                        'view' => 0,
                        'date' => now(),
                    ]);
                }

                $nextAction = new Activate();
                $nextAction->nextAction($parentOrders['id'], $level);

            }

            DB::commit();
        }

    }

    public function orderInfo($tarifId, $level) {
        if (SiteInfo::on('activate')->find(1)->alter_fee) {
            $this->fee = 0;
        }
        $fee = $this->fee;

        $order = OrdersMarkInside::where('id', $tarifId)->first();
        $markId = $order->mark;
        $mark = MarksInside::find($order->mark);
        $markLevelInfo = MarksLevelInfo::where('id', $order->mark)->first();
        $admin = 0;
        if ($order->id == $order->par_or) {
            $admin = 1;
        }

        $balanceTarif = $order->balance - $order->cashout;                      //баланс тарифа

        $profit = PaymentsOrder::where('wallet', $tarifId)                      //всего заработали
        ->where('level', $level)
            ->sum('amount');
//        dd($profit);
        $profit = Helpers::myRound($profit);

        $cashoutAll = PaymentsOrder::where('from_wallet', $tarifId)             //всего выведено
        ->where('from_level', $level)->where('wallet', '!=', 'to_balance')
            ->sum('amount');
        $cashoutAll = Helpers::myRound($cashoutAll);

        $cashoutToBalance = PaymentsOrder::where('from_wallet', $tarifId)       //выведено на баланс
        ->where('from_level', $level)
            ->where('wallet', 'to_balance')
            ->sum('amount');
        $cashoutToBalance= Helpers::myRound($cashoutToBalance);

        $cashoutAll = ($cashoutAll/(100 - $fee)*100) + $cashoutToBalance;
        $cashoutAll = Helpers::myRound($cashoutAll);

        $cashoutLevelUp = PaymentsOrder::where('from_wallet', $tarifId)         //выведено на повышение уровня
        ->where('from_level', $level)
            ->where('type', 'level_up')
            ->sum('amount');
        $cashoutLevelUp = $cashoutLevelUp/(100 - $fee)*100;
        $cashoutLevelUp = Helpers::myRound($cashoutLevelUp);



        $cashoutReinvest = PaymentsOrder::where('from_wallet', $tarifId)        //выведено на реинвесты
        ->where('from_level', $level)
            ->where('type', 'reinvest')
            ->sum('amount');
        $cashoutReinvest = $cashoutReinvest/(100 - $fee)*100;
        $cashoutReinvest = Helpers::myRound($cashoutReinvest);


        $cashoutReinvestFirst = PaymentsOrder::where('from_wallet', $tarifId)   //выведено на реинвесты в первый тариф
        ->where('from_level', $level)
            ->where('type', 'reinvest_first')
            ->sum('amount');
        $cashoutReinvestFirst = $cashoutReinvestFirst/(100 - $fee)*100;
        $cashoutReinvestFirst = Helpers::myRound($cashoutReinvestFirst);


        $balanceLevel = $profit - $cashoutAll;                                  //баланс тарифа на уровне
        $balanceLevel = Helpers::myRound($balanceLevel);

        $countReinvest = PaymentsOrder::where('from_wallet', $tarifId)          //сколько сделано реинвестов
        ->where('from_level', $level)
            ->where('wallet', '!=', 'to_partner')
            ->where('wallet', '!=', 'to_inviter')
            ->where('wallet', '!=', 'to_admin')
            ->where('type', 'reinvest')
            ->where('level', 1)
            ->count();

        $countReinvestFirst = PaymentsOrder::where('from_wallet', $tarifId)     //сколько сделано реинвестов в первый тариф
        ->where('from_level', $level)
            ->where('wallet', '!=', 'to_partner')
            ->where('wallet', '!=', 'to_inviter')
            ->where('wallet', '!=', 'to_admin')
            ->where('type', 'reinvest_first')
            ->count();



        $markProfit = MarksHelper::profitLevel($markId, $level, $admin);                                            // должно быть выведено
        $markProfit = Helpers::myRound($markProfit);
        $markCashout = MarksHelper::cashoutLevel($markId, $level, $admin);                                          // должно быть выведено
        $markCashout = Helpers::myRound($markCashout);                                    // зарезервировано всего(повышение, реинвест)
        if ($order->level <= $level) {
            $markPriceNextLevel = MarksHelper::priceNextLevel($markId, $level);                             // цена на след уровень
        } else {
            $markPriceNextLevel = 0;
        }
        $markPriceReinvest = MarksHelper::priceReinvest($markId, $level);                                   // цена на реинвест
        $markPriceReinvest = Helpers::myRound($markPriceReinvest);
        $markPriceReinvestFirst = MarksHelper::markEnter($markLevelInfo['reinvest_first_type_' . $level]);                                                // цена на реинвест в первый тариф
        $markPriceReinvestFirst = Helpers::myRound($markPriceReinvestFirst);
        $markPriceReinvestFirstAll = $markPriceReinvestFirst * $markLevelInfo['reinvest_first_' . $level];    // цена всех реинвестов в первый тариф
        $markPriceReinvestFirstAll = Helpers::myRound($markPriceReinvestFirstAll);
        $markPriceReinvestAll = $markPriceReinvest * $markLevelInfo['reinvest_type_' . $level];               // цена всех реинвестов
        $markPriceReinvestAll = Helpers::myRound($markPriceReinvestAll);
        $markCountReinvest = $markLevelInfo['reinvest_type_' . $level];                                       // должно быть реинвестов
        $markCountReinvestFirst = $markLevelInfo['reinvest_first_' . $level];                                 // должно быть реинвестов в первый тариф

        $markReserved = $markPriceReinvestAll + $markPriceReinvestFirstAll + $markPriceNextLevel;           // зарезервировано всего(повышение, реинвест)
        $markReserved = Helpers::myRound($markReserved);

        $markCashout = Helpers::myRound($markCashout);
        $cashoutToBalance = Helpers::myRound($cashoutToBalance);
        $canCashout = $markCashout - $cashoutToBalance;
        if ($admin) {
            $canCashout = $markProfit - $cashoutAll - $markReserved;
        }
        $canCashout = Helpers::myRound($canCashout);

        if ($canCashout > 0) {
            if ($canCashout >= $balanceLevel) {
                $toWithdraw = $balanceLevel;
            } else {
                $toWithdraw = $canCashout;
            }
        } else {
            $toWithdraw = 0;
        }
        $toWithdraw = Helpers::myRound($toWithdraw);


        $orderInfo = [
            'profit' => $profit,                                                // всего заработали
            'cashout_all' => $cashoutAll,                                       // всего выведено
            'cashout_to_balance' => $cashoutToBalance,                          // выведено на баланс
            'cashout_level_up' => $cashoutLevelUp,                              // выведено на повышение уровня
            'balance_level' => $balanceLevel,                                   // баланс тарифа на уровне
            'balance_tarif' => $balanceTarif,                                   // баланс тарифа
            'cashout_reinvest' => $cashoutReinvest,                             // выведено на реинвесты
            'cashout_reinvest_first' => $cashoutReinvestFirst,                  // выведено на реинвесты в первый тариф
            'count_reinvest' => $countReinvest,                                 // сколько реинвестов сделано
            'count_reinvest_first' => $countReinvestFirst,                      // сколько реинвестов сделано в первый тариф

            'mark_profit' => $markProfit,                                       // должно быть выведено
            'mark_cashout' => $markCashout,                                     // должно быть выведено
            'mark_reserved' => $markReserved,                                   // зарезервировано всего(повышение, реинвест)
            'mark_price_next_level' => $markPriceNextLevel,                     // цена на след уровень

            'mark_price_reinvest' => $markPriceReinvest,                        // цена на реинвест
            'mark_price_reinvest_all' => $markPriceReinvestAll,                 // цена всех реинвестов
            'mark_count_reinvest' => $markCountReinvest,                        // должно быть реинвестов

            'mark_price_reinvest_first' => $markPriceReinvestFirst,             // цена на реинвест в первый тариф
            'mark_price_reinvest_first_all' => $markPriceReinvestFirstAll,      // цена всех реинвестов в первый тариф
            'mark_count_reinvest_first' => $markCountReinvestFirst,             // должно быть реинвестов в первый тариф

            'to_withdraw' => $toWithdraw,                                       // доступно для вывода
        ];

        foreach ($orderInfo as $key => $val) {
            $orderInfo[$key] = Helpers::myRound($orderInfo[$key]);
        }

        return $orderInfo;
    }

    public function nextAction($tarifId, $level) {

        $order = OrdersMarkInside::where('id', $tarifId)->first();
        $markId = $order->mark;
        $overflow = $order->marks->overflow;

        $markInfo = MarksInside::where('id', $markId)->first();
        $markLevelInfo = MarksLevelInfo::where('id', $markId)->first();


        /**
         * priority
         * 1 - cashout
         * 2 - level_up
         * 3 - reinvest
         * 4 - reinvest_first
         *
         * 1234 - cashout -> level_up -> reinvest -> reinvest_first
         * 4321 - reinvest_first -> reinvest -> level_up -> cashout
         * 3421 - reinvest -> reinvest_first -> level_up -> cashout
         */

        $priorityArray = Helpers::priorityArray($markLevelInfo['priority_'.$level]);

        $orderInfo = self::orderInfo($tarifId, $level);

        foreach ($priorityArray as $key => $priority) {

            if ($priority == 'cashout') {

                $orderInfo = self::orderInfo($tarifId, $level);
                Log::info('withdraw');
                Log::info($orderInfo['to_withdraw']);

                if ($orderInfo['to_withdraw'] > 0) {

    //                echo "Выводим ".$orderInfo['to_withdraw']." руб. на баланс<br>";

                    PaymentsOrder::create([
                        'from_user' => '9999999',
                        'to_user' => $order->id_user,
                        'from_wallet' => $order->id,
                        'wallet' => 'to_balance',
                        'from_level' => $level,
                        'level' => 0,
                        'amount' => $orderInfo['to_withdraw'],
                        'date' => now(),
                        'direct' => 0,
                        'mark' => $markId,
                        'type' => 'cashout_balance',
                    ]);
                    OrdersMarkInside::where('id', $order->id)->increment('cashout', $orderInfo['to_withdraw']);
                    Users::where('id', $order->id_user)->increment('balance', $orderInfo['to_withdraw']);

                    $orderInfo = self::orderInfo($tarifId, $level);
                }

            } elseif ($priority == 'level_up') {

                if ($orderInfo['mark_price_next_level'] and $order->level < $markInfo->level) {
                    if ($orderInfo['balance_level'] >= $orderInfo['mark_price_next_level'] and $orderInfo['balance_tarif'] >= $orderInfo['mark_price_next_level']) {
                        $reinvestObj = new Activate();
                        $reinvestObj->activate($order->id_user, $order->mark, $order->id, 'level_up', 0, $level);
                    } else {
                        return false;
                    }

//                echo "Повысили уровень<br>";

                } elseif ($orderInfo['mark_reserved'] == 0 and $orderInfo['balance_level']) {
                    if ($order->level >= $level) {
                        PaymentsOrder::create([
                            'from_user' => '9999999',
                            'to_user' => $order->id_user,
                            'from_wallet' => $order->id,
                            'wallet' => 'to_balance',
                            'from_level' => $level,
                            'level' => 0,
                            'amount' => $orderInfo['balance_level'],
                            'date' => now(),
                            'direct' => 0,
                            'mark' => $markId,
                            'type' => 'cashout_balance',
                        ]);
                        Users::where('id', $order->id_user)->increment('balance', $orderInfo['balance_level']);
                        OrdersMarkInside::where('id', $order->id)->increment('cashout', $orderInfo['balance_level']);

//                    echo "Уже и так уровень активирован, поэтому вывели на баланс<br>";

                    }
                }

                $orderInfo = self::orderInfo($tarifId, $level);

            } elseif ($priority == 'reinvest' and $overflow != 0) {

                if ($orderInfo['cashout_reinvest'] < $orderInfo['mark_price_reinvest_all']) {

                    if ($orderInfo['count_reinvest'] < $orderInfo['mark_count_reinvest']) {

                        for ($i = $orderInfo['count_reinvest']; $i < $orderInfo['mark_count_reinvest']; $i++) {
                            if ($orderInfo['balance_level'] >= $orderInfo['mark_price_reinvest'] and $orderInfo['balance_tarif'] >= $orderInfo['mark_price_reinvest']) {
                                $reinvestObj = new Activate();
                                $reinvestObj->activate($order->id_user, $markLevelInfo['reinvest_'.$level], $order->id, 'reinvest', 0, $level);
                                $orderInfo = self::orderInfo($tarifId, $level);

//                            echo "Сделали реинвест $i-й по счету<br>";

                            } else {
                                return false;
                            }
                        }
                    }

                }

            } elseif ($priority == 'reinvest_first' and $overflow != 0) {

                if ($orderInfo['cashout_reinvest_first'] < $orderInfo['mark_price_reinvest_first_all']) {

                    if ($orderInfo['count_reinvest_first'] < $orderInfo['mark_count_reinvest_first']) {

                        for ($i = $orderInfo['count_reinvest_first']; $i < $orderInfo['mark_count_reinvest_first']; $i++) {

                            if ($orderInfo['balance_level'] >= $orderInfo['mark_price_reinvest_first']) {
                                $reinvestObj = new Activate();
                                $reinvestObj->activate($order->id_user, $markLevelInfo['reinvest_first_type_' . $level], $order->id, 'reinvest_first', 0, $level);
                                $orderInfo = self::orderInfo($tarifId, $level);
                            } else {
                                return false;
                            }
                        }
                    }

                }

            }
        }

    }

}
