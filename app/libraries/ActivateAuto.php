<?php

namespace App\Libraries;

use App\Libraries\Multimark;
use App\Models\CoreProfit;
use App\Helpers\MarksHelper;
use App\Models\MarksInside;
use App\Models\Users;
use App\Models\OrdersMarkInside;
use App\Models\PaymentsOrder;
use App\Models\SiteInfo;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class ActivateAuto
{
    public $comission = 10;
    public $partnersPrice = 70;


    public function activateAuto($userId, $tarifId, $reinvest = false, $firstReinvest = false, $fromTarif = false) {

        if (SiteInfo::find(1)->alter_fee) {
            $this->comission = 0;
        }

        DB::beginTransaction();
        OrdersMarkInside::raw('SET TRANSACTION ISOLATION LEVEL REPEATABLE READ');

        /**
         * Сделать хелперс для определения валюты
         */
//        $db_row_cur = strtolower($m_curr);
        $today = date('Y-m-d');

        $site_info = SiteInfo::find(1);

        $tar = MarksInside::find($tarifId);
        $admin = Users::find(1);

        $crypto = (Helpers::typeCurrent($site_info['value']) == 'fiat')?false:true;
        $curecho = Helpers::getCurrent($site_info['value'], 'icon');


        /**
         * 00 - админу
         * 000 - Паренту
         * 0000 - Перевод с тарифа на баланс
         */

        /**
         * Комисссия конструктору
         */
        $half = 100 - $this->comission;
        $partnerHalf = 100 - $this->partnersPrice;
        if ($tarifId < 1 OR $tarifId > 9) {
            DB::rollBack();
            throw new \Exception('Такой тариф не существует');
        } else {
            $tarif = MarksInside::find($tarifId);

            if (!$tarif['status'] and !$reinvest) {
                DB::rollBack();
                throw new \Exception('Такой тариф не существует');
            }
        }

        $user = Users::where('id', $userId)->first();

        /**
         * Получаем кол-во активаций для определения overflow
         */
        $userMarksCount = OrdersMarkInside::where('mark', $tarifId)->count();

        $overflow = $site_info['overflow'];

        if (!$userMarksCount && $overflow == 0) {
            $overflow = 1;
        }

        switch($overflow) {
            case 1:
                $overflowObj = new Multimark($user['refer'], $tarifId, true);
                $overflowRes = $overflowObj->overflowMulti();
                break;
            case 2:
                $overflowObj = new Multimark(1, $tarifId, true);
                $overflowRes = $overflowObj->overflowMulti();
                break;
            case 3:
                $overflowObj = new Multimark($userId, $tarifId, true);
                $overflowRes = $overflowObj->overflowMulti();
                break;
            default:
                $overflowObj = new Multimark($userId, $tarifId, true);
                $overflowRes = $overflowObj->overflowMulti();
        }

        $priceLevels = [$tarif['level1'],$tarif['level2'], $tarif['level3'], $tarif['level4'], $tarif['level5'], $tarif['level6'], $tarif['level7'], $tarif['level8']];

        $levels = $tarif['level'];

        $massive[] = OrdersMarkInside::where('id', $overflowRes)->first();

        //Переводим на баланс
        if ($reinvest && !$firstReinvest) {
            $priceEnter = MarksHelper::calcMarkPrice($tarifId);
            $tarifBalance = OrdersMarkInside::find($reinvest)['balance'];
            $cashoutOrder = OrdersMarkInside::find($reinvest)['cashout'];
            $toUserSum = $tarifBalance - $priceEnter;
            $toUserSum = $toUserSum - $cashoutOrder;

            if ($crypto) {
                $toUserSum = Helpers::in_crypto($toUserSum);
            }

            PaymentsOrder::create([
                'from_user' => '9999999',
                'to_user' => $userId,
                'wallet' => '0000',
                'amount' => $toUserSum,
                'id_order' => '0',
                'id_pay_payeer' => 0,
                'date' => now(),
                'direct' => 0,
                'mark' => $tarifId
            ]);

            OrdersMarkInside::where('id', $reinvest)
                            ->update(['status' => 2]);
            OrdersMarkInside::where('id', $reinvest)
                            ->increment('cashout', $toUserSum);
            Users::where('id', $userId)
                ->increment('balance', $toUserSum);
        }

        for ($i = 0; $i < $levels; $i++) {
            $mark = $massive[$i];

            /**
             * Комиссия конструктору
             */
            $mark['full_price'] = $priceLevels[$i];
            $mark['price'] = round($priceLevels[$i]/100, 2, PHP_ROUND_HALF_DOWN) * $half;
            $mark['comission'] = round($priceLevels[$i]/100, 2, PHP_ROUND_HALF_DOWN) * $this->comission;
            /**
             * Партнерские
             */
//            if ($this->getWalletParCore()) {
//                $mark['partner_price'] = round($mark['comission'] / 100, 2, PHP_ROUND_HALF_DOWN) * $partnerHalf;
//                $mark['comission'] = round($mark['comission'] / 100, 2, PHP_ROUND_HALF_DOWN) * $this->partnersPrice;
//            }

            $massive[$i] = $mark;

            $refer = $massive[$i]['par_or'];

            /**
             * Если итерация последняя не записываем
             */
            if ($i + 1 < $levels)
                $massive[] = OrdersMarkInside::where('id', $refer)->first();
        }

        $markProfit = MarksHelper::markProfit($tarifId);
        $markEnter = MarksHelper::calcMarkPrice($tarifId);


        $balanceForWithdrow = $user['balance'];
        if ($reinvest) {
            $balanceForWithdrow = OrdersMarkInside::find($reinvest)['balance'];
        }

        if ($markEnter < 0) {
            DB::rollBack();
            throw new \Exception("Что-то пошло не так.");
        }

        if ($balanceForWithdrow < $markEnter) {
            DB::rollBack();
            throw new \Exception("Недостаточно средств на балансе. Необходимо ".$markEnter.$curecho.". Ваш баланс: ".$user['balance']." ".$curecho.".");
        }

        //Активация маркетинга
        $mark = $massive[0];
        if ($reinvest) {
            OrdersMarkInside::create([
                'id_user' => $userId,
                'parent' => $mark['id_user'],
                'mark' => $tarifId,
                'level' => 1,
                'balance' => 0,
                'pay_people' => 0,
                'status' => 0,
                'par_or' => $mark['id'],
                'date' => now(),
                'reinvest' => 1
            ]);
        } else {
            OrdersMarkInside::create([
                'id_user' => $userId,
                'parent' => $mark['id_user'],
                'mark' => $tarifId,
                'level' => 1,
                'balance' => 0,
                'pay_people' => 0,
                'status' => 0,
                'par_or' => $mark['id'],
                'date' => now(),
                'reinvest' => 0
            ]);
        }

        foreach ($massive as $i => $mark) {
            //Списываем с баланса
            if ($reinvest) {
//                OrdersMarkInside::where('id', $reinvest)
//                    ->decrement('balance', $mark['full_price']);
            } else {
                Users::where('id', $userId)
                    ->decrement('balance', $mark['full_price']);
            }

            //Прибавляем на баланс
            OrdersMarkInside::where('id', $mark['id'])
                ->increment('pay_people', 1);
            OrdersMarkInside::where('id', $mark['id'])
                ->increment('balance', $mark['price']);

            PaymentsOrder::create([
                'from_user' => $userId,
                'to_user' => $mark['id_user'],
                'wallet' => $mark['id'],
                'amount' => $mark['price'],
                'id_order' => '0',
                'id_pay_payeer' => 0,
                'date' => now(),
                'direct' => 0,
                'mark' => $tarifId
            ]);


            /**
             * TODO: Не забыть доделать комиссионные
             */
//            DB::query("UPDATE `profit` SET `$db_row_cur` = `$db_row_cur` + $mark[comission] WHERE `date` = '$today'", $this->connectCore);


            $mark['status'] = OrdersMarkInside::find($mark['id'])['status'];
            if ($tarif['reinvest_first'] && !$firstReinvest && (($mark['balance'] + $mark['price']) >= $markProfit) && $mark['status'] < 2) {
                $reinvestObj = new ActivateAuto();
                $reinvestObj->activateAuto($mark['id_user'], 1, $mark['id'], true);
            }

            $sumBalance =  round($mark['balance'] + $mark['price'], 2);
            $markProfit =  round($markProfit, 2);

            //Автореинвест


            if ($tarif['reinvest'] && ($sumBalance >= $markProfit) && !$firstReinvest && $mark['status'] < 2) {
                $reinvestObj = new ActivateAuto();
                $reinvestObj->activateAuto($mark['id_user'], $tarif['reinvest'], $mark['id'], false, $tarifId);
            }

            if (!$tarif['reinvest'] && ($sumBalance >= $markProfit)) {
                $tarifBalance = OrdersMarkInside::where('id', $mark['id'])->get('balance');

                PaymentsOrder::create([
                    'from_user' => '9999999',
                    'to_user' => $mark['id_user'],
                    'wallet' => '0000',
                    'amount' => $tarifBalance,
                    'id_order' => '0',
                    'id_pay_payeer' => 0,
                    'date' => now(),
                    'direct' => 0,
                    'mark' => $tarifId
                ]);

                OrdersMarkInside::where('id', $mark['id'])
                    ->update(['status' => 2]);

                Users::where('id', $mark['id_user'])
                    ->increment('balance', $tarifBalance);
            }
        }

        if ($tarif['to_admin']) {
            $toAdminFee = round($tarif['to_admin']/100, 2, PHP_ROUND_HALF_DOWN) * $this->comission;
            $toAdminSum = round($tarif['to_admin']/100, 2, PHP_ROUND_HALF_DOWN) * $half;

            Users::where('id', 1)
                ->increment('balance', $toAdminSum);

            PaymentsOrder::create([
                'from_user' => $userId,
                'to_user' => 1,
                'wallet' => '00',
                'amount' => $toAdminSum,
                'id_order' => '0',
                'id_pay_payeer' => 0,
                'date' => now(),
                'direct' => 0,
                'mark' => $tarifId
            ]);

//            DB::query("UPDATE `profit` SET `$db_row_cur` = `$db_row_cur` + $toAdminFee WHERE `date` = '$today'", $this->connectCore);
        }
        if ($tarif['to_par']) {
            $toParFee = round($tarif['to_par']/100, 2, PHP_ROUND_HALF_DOWN) * $this->comission;
            $domain = new Domain();

            if ($domain->getDB() == 'monetary') {
                $toParSum = $tarif['to_par'];
            } else {
                if ($domain->getDB() == 'aquarium_4' and $tarifId == 1) {
                    $toParSum = $tarif['to_par'];
                } else {
                    $toParSum = round($tarif['to_par'] / 100, 2, PHP_ROUND_HALF_DOWN) * $half;
                }
            }

            Users::where('id', $user['refer'])
                ->increment('balance', $toParSum);

            PaymentsOrder::create([
                'from_user' => $userId,
                'to_user' => $user['refer'],
                'wallet' => '000',
                'amount' => $toParSum,
                'id_order' => '0',
                'id_pay_payeer' => 0,
                'date' => now(),
                'direct' => 0,
                'mark' => $tarifId
            ]);

//            DB::query("UPDATE `profit` SET `$db_row_cur` = `$db_row_cur` + $toParFee WHERE `date` = '$today'", $this->connectCore);
        }

        $fee = $tarif['to_admin'] + $tarif['to_par'];

        if (!$firstReinvest && !$reinvest) {
            Users::where('id', $userId)
                ->decrement('balance', $fee, ['type_follow' => 1]);
        }


        DB::commit();
//        $partnerProgramm = new PartnerCore();
//        $partnerProgramm->sendPercent($markEnter, $userId, $tarifId);

//        $partnerProgramm = new PartnerCore();
//        $partnerProgramm->sendPercent($price_all, $us_id, $tarif);
    }

}
