<?php


namespace App\libraries;


use App\Models\Marks;
use App\Models\MarksInside;
use PhpQuery\PhpQuery;

class Constructor
{
    public static $sitesFolder;
    public static $site;

    public function __construct()
    {
//        include_once $_SERVER['DOCUMENT_ROOT'].'/classes/phpQueryCl/phpQuery/phpQuery.php';
        $landing = new Landing();

        self::$sitesFolder = $landing->sitePath;
    }

    public function addFile($file, $content) {
        if (!is_dir(self::$sitesFolder))
            mkdir(self::$sitesFolder);

        $fp = fopen($file, "w");
//		echo $content;
        fwrite($fp, $content);

        fclose($fp);
    }

    public function loadProject() {
        return file_get_contents(self::$sitesFolder.'/project.pixbuilder');
    }

    public function updateUserPages() {

        //Получаем все страницы сайта
        $pages = glob(self::$sitesFolder."/*.html");

        foreach ($pages as $page) {
            $html = $this->updateTarif($page);

            $this->addFile($page, $html);
        }

        $this->deleteOldFiles($pages);
    }

    public function updateTarif($page) {
        $doc = phpQuery::newDocumentFileHTML($page);
//        phpQuery::selectDocument($doc);

        $marksContainer = pq('.p-mark-container');
        if ($doc['.p-mark-container']) {
            $marks = array_filter(MarksInside::all(), function ($ar) {
                return $ar['status'] == 1;
            });

            $activeMarksID = array();

            foreach ($marks as $c => $mark) {
                $price_level = [$mark['level1'], $mark['level2'], $mark['level3'], $mark['level4'], $mark['level5'], $mark['level6'], $mark['level7'], $mark['level8']];
                $markHtml = pq('.p-mark-'.$mark['id']);

                $activeMarksID[] = $mark['id'];

                if (!$markHtml->hasClass('p-mark-'.$mark['id'])) {
                    $markHtml = pq('.p-mark-1')->clone()->removeClass('p-mark-1')->addClass('p-mark-'.$mark['id']);
                    $marksContainer->append($markHtml);
                }

                $markHtml->find('.p-mark-name')->text($mark['name']);
                $markHtml->find('.p-money-mark-auto')->text(Main::markProfit($mark['id']));
                $level = $markHtml->find('.p-mark-levels .p-marks-level-line:first')->clone();
                $markContainer = $markHtml->find('.p-mark-levels')->empty();
                for ($i = 0; $i < $mark['level']; $i++) {
                    $level = $level->clone();
                    pq($level)->find('.p-mark-level-n')->text($i+1);
                    pq($level)->find('.p-mark-level-price')->text($price_level[$i]);
                    pq($level)->find('.p-mark-level-user')->text(pow($mark['width'], $i+1));
                    pq($level)->find('.p-mark-level-profit')->text(pow($mark['width'], $i+1) * $price_level[$i] * 0.9);
                    $markContainer->append($level);
                }

            }

            foreach ($marksContainer['.p-mark'] as $item) {

                if (!in_array(pq($item)->attr('data-id'), $activeMarksID)) {
                    pq($item)->remove();
                }
            }

        }

        return $doc->htmlOuter();
    }

    public function deleteOldFiles($pages) {
        $files = array_slice(scandir(self::$sitesFolder.'/uploads'), 2);
        $src = array();

        foreach ($pages as $page) {
            $doc = phpQuery::newDocumentFileHTML($page);

            $images = pq('img');
            foreach ($images as $image) {
                $src[] = pq($image)->attr('src');
            }

            $elWidthBackground = pq('.pix_builder_bg');

            foreach ($elWidthBackground as $item) {
                $style = pq($item)->attr('style');
                if (strpos($style, 'url("') !== false) {
                    $pos1 = strpos($style, 'url("') + 5;
                    $pos2 = strpos($style, '");');
                    $src[] = $_SERVER['DOCUMENT_ROOT'].substr($style, $pos1, $pos2 - $pos1);
                }
            }
        }

        foreach ($files as $file) {
            $filePath = self::$sitesFolder.'/uploads/'.$file;

            if (array_search($filePath, $src) === false) {
                unlink($filePath);
            }
        }
    }

}