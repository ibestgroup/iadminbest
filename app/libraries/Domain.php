<?php

namespace App\Libraries;


use App\Models\CoreParking;
use App\Models\CoreSiteList;

class Domain {

    /**
     * Domain constructor.
     */

    function __construct()
    {
        if (!isset($_SERVER['HTTP_HOST'])) {
            $this->domain = 'dohodmlm.iadminlocal.best';
        } else {
            $this->domain = $_SERVER['HTTP_HOST'];
        }
    }

    /**
     * @return mixed
     */

    public function domain()
    {
        preg_match_all('/(.*?)\.(iadmin.best|iadminlocal.best|icash.best|icashlocal.best|icash.work|iadmin.work|piramidayspeha.ru|moneytree.best)/', $this->domain, $this->mainDomain);


        if (count($this->mainDomain[1]) == 0) {
            return "iadminbest";
        } else {
            return $this->mainDomain[1][0];
        }
    }


    /**
     * @param $db
     * @return bool
     */

    public function checkCore() {
        $db = $this->domain();

        $countParking = CoreParking::where('domain', $this->domain)->count();

        if ($db === 'iadminbest' and !$countParking) {
            return true;
        } else {
            return false;
        }
    }

    public function dirTemplates() {
        if ($this->checkCore()) {
            return 'core';
        } else {
            return 'sites';
        }
    }

    /**
     * @return string
     */

    public function getDB () {

        if ($this->checkCore()) {
            $result = 'iadminbest';
        } else {

            $result = $this->domain()."_iadminbest";

            $countParking = CoreParking::where('domain', $this->domain)->count();
            if ($countParking) {
                $parking = CoreParking::where('domain', $this->domain)->first();
                $siteList = CoreSiteList::where('id', $parking->site_id)->first();
                $result = $siteList->subdomain;
            }

        }

        return $result;
    }

    public function parking() {

        $idSite = Helpers::getSiteId();
        if (CoreParking::where('site_id', $idSite)->count()) {
            $domain = CoreParking::where('site_id', $idSite)->first()['domain'];
        } else {
            $domain = false;
        }
        return $domain;

    }

    public function parkingDomain() {
        $parkingCount = CoreParking::where('domain', $this->domain)->count();
        if ($parkingCount) {
            $parking = CoreParking::where('domain', $this->domain)->get()[0];
            $this->mainDomain[1][0] = CoreSiteList::where('id', $parking['site_id'])->get()[0]['subdomain'];
        }

        if ($method = 'get_db') {

        }
    }

    public function nativeDomain() {
        return 'https://' . $this->domain() . '.iadmin.work';
    }

    public static function checkParking() {

        $idSite = Helpers::getSiteId();
        if (CoreParking::where('site_id', $idSite)->where('delegate', 1)->count()) {
            $domain = true;
        } else {
            $domain = false;
        }
        return $domain;

    }

    public function resultDomain($siteId = '') {

        $domain = new Domain();

        if ($siteId == '') {
            $siteId = Helpers::getSiteId();
        }

        $site = CoreSiteList::whereId($siteId)->first();
        $parkingCount = CoreParking::where('site_id', $site['id'])->where('delegate', 1)->count();

        if ($parkingCount) {
            $parking = CoreParking::where('site_id', $site['id'])->where('delegate', 1)->first();
            $result = $parking->domain;
        } else {
            $siteName = $site->subdomain;
            $siteName = preg_replace('/_iadminbest/', '', $siteName);
            $result = $siteName.'.iadmin.work';
        }

        return $result;

    }

}