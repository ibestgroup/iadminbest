<?php

namespace App\Libraries;
use App\Helpers\MarksHelper;
use App\Models\AdsCore;
use App\Models\AdsExeption;
use App\Models\BirdsOrder;
use App\Models\CoreCurrencyRate;
use App\Models\CoreSiteList;
use App\Models\CoreUsers;
use App\Models\Currency;
use App\Models\Friends;
use App\Models\MarksInside;
use App\Models\Notice;
use App\Models\OrdersMarkInside;
use App\Models\Pages;
use App\Models\Payments;
use App\Models\PaymentsOrder;
use App\Models\SiteElements;
use App\Models\SiteInfo;
use App\Models\TelegramCore;
use App\Models\Templates;
use App\Models\TypesOrder;
use App\Models\UsersData;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Auth;
use App\Models\Users;
use App\Models\Ad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;


class Helpers {

    const MARKS_INFO_FIELDS = [
        'mark_level_price_sum',
        'mark_level_price',
        'mark_level_to_par',
        'mark_level_to_admin',
        'mark_level_reinvest',
        'mark_level_reinvest_type',
        'mark_level_reinvest_first',
        'mark_level_to_inviter',
        'mark_level_profit',
        'mark_level_people',
    ];

    /**
     * @param $meth
     * @param $data
     * @return mixed
     */
    public static function convertUs($meth, $data) {
        if ($meth == 'id') {
            $user = Users::where('login', $data)->get();
            return $user['id'];
        } elseif($meth == 'login') {
            $user = Users::find($data);
            return $user['login'];
        } elseif($meth == 'telegram_login') {
            $user = Users::find($data);
            return $user['telegram_login'];
        }
    }

    public static function convertUsCore($meth, $data) {
        if ($meth == 'id') {
            $user = CoreUsers::where('login', $data)->get();
            return $user['id'];
        } elseif($meth == 'login') {
            $user = CoreUsers::find($data);
            return $user['login'];
        }
    }

    /**
     * @param $num
     * @return string
     */
    public static function typeAd($num) {
        if ($num == 1) {
            return 'success';
        } elseif($num == 2) {
            return 'danger';
        } elseif($num == 3) {
            return 'warning';
        } elseif($num == 4) {
            return 'info';
        }
    }

    public static function in_crypto($sum) {
        $sum = number_format($sum,10);
        $sum = rtrim(rtrim($sum, '0'), '.');
        $sum = str_replace(',', "", $sum);
        return (float)$sum;
    }

    public function balanceOrder($id) {
        $order = OrdersMarkInside::find($id);

        $balance = $order['balance'] - $order['cashout'];

        return $balance;
    }

    public static function walletCLK() {

        $domain = new Domain();
        $domain = $domain->getDB();
        $siteId = CoreSiteList::where('subdomain', $domain)->first()['id'];
        $siteId = str_pad($siteId, 6, "0", STR_PAD_LEFT);
        $userId = str_pad(Auth::id(), 6, "0", STR_PAD_LEFT);
        return 'CLK'.$siteId.$userId;

    }

    public static function infoWalletCLK($wallet, $method = 'site') {

        preg_match_all('~CLK([0-9]{6})([0-9]{6})~', $wallet, $result);
        $idSite = (int)$result[1][0];
        $idUser = (int)$result[2][0];
        if ($method == 'site') {
            return $idSite;
        } elseif ($method == 'user') {
            return $idUser;
        }

    }

    /**
     * @param $firstTime
     * @param string $secondTime
     * @return bool
     */
    public function whatLater($firstTime, $secondTime = '') {
        if ($secondTime == '') {
            $secondTime = strtotime(date('Y-m-d H:i:s'));
        }
        $firstTime = strtotime($firstTime);

        if ($firstTime < $secondTime) {
            return true;
        } else {
            return false;
        }
    }

    public static function getMarkType() {
        $siteInfo = SiteInfo::find(1);

        if ($siteInfo['mark_type'] == 3) {
            return "Линейные";
        } elseif ($siteInfo['mark_type'] == 4) {
            return "Мульти";
        }
    }

    public static function getCurrent($num, $method = 'name') {
        if ($method == 'name') {
            switch ($num) {
                case 1:
                    $current = 'Рубль';
                    break;
                case 2:
                    $current = 'Bitcoin';
                    break;
                case 3:
                    $current = 'Доллар';
                    break;
                case 4:
                    $current = 'Евро';
                    break;
                case 5:
                    $current = 'Ethereum';
                    break;
                case 6:
                    $current = 'LiteCoin';
                    break;
                case 7:
                    $current = 'DogeCoin';
                    break;
                case 8:
                    $current = 'Ripple';
                    break;
                case 9:
                    $current = 'LiteCoinTest';
                    break;
            }
        } elseif ($method == 'code') {
            switch ($num) {
                case 1:
                    $current = 'RUB';
                    break;
                case 2:
                    $current = 'BTC';
                    break;
                case 3:
                    $current = 'USD';
                    break;
                case 4:
                    $current = 'EUR';
                    break;
                case 5:
                    $current = 'ETH';
                    break;
                case 6:
                    $current = 'LTC';
                    break;
                case 7:
                    $current = 'DOGE';
                    break;
                case 8:
                    $current = 'XRP';
                    break;
                case 9:
                    $current = 'LTCT';
                    break;
            }
        } elseif ($method == 'icon') {
            switch ($num) {
                case 1:
                    $current = '<i class="fa fa-rub" aria-hidden="true"></i>';
                    break;
                case 2:
                    $current = '<i class="cc BTC-alt" aria-hidden="true"></i>';
                    break;
                case 3:
                    $current = '<i class="fa fa-usd" aria-hidden="true"></i>';
                    break;
                case 4:
                    $current = '<i class="fa fa-eur" aria-hidden="true"></i>';
                    break;
                case 5:
                    $current = '<i class="cc ETH-alt" aria-hidden="true"></i>';
                    break;
                case 6:
                    $current = '<i class="cc LTC-alt" aria-hidden="true"></i>';
                    break;
                case 7:
                    $current = '<i class="cc DOGE-alt" aria-hidden="true"></i>';
                    break;
                case 8:
                    $current = '<i class="cc XRP-alt" aria-hidden="true"></i>';
                    break;
                case 9:
                    $current = '<i class="cc LTCT-alt" aria-hidden="true"></i>';
                    break;
            }
        } elseif ($method == 'iconClass') {
            switch ($num) {
                case 1:
                    $current = 'fa fa-rub';
                    break;
                case 2:
                    $current = 'cc BTC-alt';
                    break;
                case 3:
                    $current = 'fa fa-usd';
                    break;
                case 4:
                    $current = 'fa fa-eur';
                    break;
                case 5:
                    $current = 'cc ETH-alt';
                    break;
                case 6:
                    $current = 'cc LTC-alt';
                    break;
                case 7:
                    $current = 'cc DOGE-alt';
                    break;
                case 8:
                    $current = 'cc XRP-alt';
                    break;
                case 9:
                    $current = 'cc LTC-alt';
                    break;
            }
        }

        return $current;
    }

    public static function typeCurrent($num) {
        if ($num == 1 or $num == 3 or $num == 4) {
            return 'fiat';
        } else {
            return 'crypto';
        }
    }

    public static function privelegeName($num) {
        switch ($num) {
            case 0:
                $privelege = __('main.privilege_banned');
                break;
            case 1:
                $privelege = __('main.privilege_custom');
                break;
            case 2:
                $privelege = __('main.privilege_before_start');
                break;

        }

        return $privelege;
    }

    public static function getSiteId($subdomain = '') {

        if ($subdomain == '') {
            $siteDB = new Domain();
//            dd($siteDB->getDB());
            $site = CoreSiteList::where('subdomain', $siteDB->getDB())->first();
        } else {
            $site = CoreSiteList::where('subdomain', $subdomain)->first();
        }

        return $site['id'];
    }

    public static function getSiteDB($siteId) {
        return CoreSiteList::find($siteId)->subdomain;
    }

    public static function getBotUsername() {
        return CoreSiteList::find(self::getSiteId())->telegram_bot_username ?? '';
    }

    public static function getSiteAdmin() {
        $siteDB = new Domain();

        $site = CoreSiteList::where('subdomain', $siteDB->getDB())->first();

        return $site['user_id'];
    }

    /**
     * @param $from
     * @param $to
     * @param int $amount
     * @return float|int
     */

    public static function convertCurrency($from, $to, $amount = 1) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.cryptonator.com/api/full/'.$from.'-'.$to);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);

        $result = trim(curl_exec($ch));
        curl_close($ch);
        $result = json_decode($result);

        $price = $result->ticker->price;
        $resultPrice = $price * $amount;

        return $resultPrice;

    }

    public static function idTransaction($id, $table) {

        if ($table == 'payment') {
            $prefix = 19;
        } elseif ($table == 'payments_order') {
            $prefix = 91;
        } elseif ($table == 'cashout') {
            $prefix = 48;
        }

        $idTransaction = $prefix.str_pad($id, 5, "0", STR_PAD_LEFT);
        return $idTransaction;
    }

    /**
     * @param $toUser
     * @return string
     */

    public static function friendsButton($toUser) {

        $friends = Friends::where('toid', $toUser)->where('fromid', Auth::id());
        $friendToUser = Friends::where('toid', Auth::id())->where('fromid', $toUser)->where('status', 0)->count();

        $friendCount = $friends->count();
        if ($friendCount) {
            $friend = $friends->get()[0];
        }

        if ($friendToUser) {
            return '
                <form action="'.route('confirmFriend').'" class="js-friends" method="post" data-id="0">
                    <input type="hidden" name="id_user" value="'.$toUser.'"/>
                    <button class="btn btn-warning" style="display: block; width: 100%; font-size: 25px;">Добавил вас, подтвердить заявку в друзья</button>
                </form>';
        }

        if ($friendCount == 0) {
            return '
                <form action="'.route('addFriend').'" class="js-friends" method="post" data-id="0">
                    <input type="hidden" name="id_user" value="'.$toUser.'"/>
                    <button class="btn btn-primary" style="display: block; width: 100%; font-size: 25px;">Добавить в друзья</button>
                </form>';
        } elseif ($friend['status'] == 0) {
            return '
                <form action="'.route('deleteFriend').'" class="js-friends" method="post" data-id="0">
                    <input type="hidden" name="id_user" value="'.$toUser.'"/>
                    <button class="btn btn-danger" style="display: block; width: 100%; font-size: 25px;">Вы отправили запрос(отменить запрос)</button>
                </form>';
        } else {
            return '
                <form action="'.route('deleteFriend').'" class="js-friends" method="post" data-id="0">
                    <input type="hidden" name="id_user" value="'.$toUser.'"/>
                    <button class="btn btn-success" style="display: block; width: 100%; font-size: 25px;">Ваш друг(удалить из друзей)</button>
                </form>';
        }

    }

    /**
     * @param $id
     * @param string $get
     * @return int|string
     */

    public static function notice($userID) {
        $myNotice = Notice::where('user_id', $userID);
        $subType = 'account';

        $resultView = '';

        $types =
            [
            'account' => [
                'register', 'password', 'activate', 'confirm_email', 'reinvest', 'level_up'
            ],
            'finance' => [
                'payment', 'cashout', 'payment_order', 'payment_order_parent'
            ],
            'referrals' => [
                'register_ref', 'activate_ref'
            ]];

        foreach ($types as $nameType => $arrType) {

            if (Notice::where('user_id', Auth::id())->where('type', $nameType)->where('view', 0)->count()) {

                $resultView .= '<li class="header">'.self::noticeGet($nameType).' - '.Notice::where('user_id', Auth::id())->where('type', $nameType)->where('view', 0)->count().'</li>';

                foreach ($arrType as $subType) {
                    $count = Notice::where('user_id', Auth::id())->where('subtype', $subType)->where('view', 0)->count();
                    if ($count) {
                        if ($count > 1) {
                            $resultView .=
                                '<li>
                                        <a href="'.route('notice').'" style="line-height: 25px;">
                                            <i class="fa fa-'.self::noticeGet($subType, 'icon').' text-'.self::noticeGet($subType, 'color').'" style="font-size: 20px;"></i> 
                                            <b>'.self::noticeGet($subType).'</b> - '.$count.' уведомления(й) 
                                    </a>
                                </li>';
                        } else {

                            $thisNotice = Notice::where('user_id', Auth::id())->where('subtype', $subType)->where('view', 0)->first();
                            $resultView .=
                            '<li>
                                    <a href="'.route('notice').'" style="line-height: 25px;">
                                        <i class="fa fa-'.self::noticeGet($subType, 'icon').' text-'.self::noticeGet($subType, 'color').'" style="font-size: 20px;"></i> 
                                        <b>'.self::noticeGet($subType).'</b> - '.$thisNotice->head.'
                                </a>
                            </li>';
                        }
                    }
                }
            }


//            if (in_array($item->type, $value)) {
//                $subType = $key;
//            }
        }

        return $resultView;

    }

    public static function noticeGet($type, $get = 'name') {
        $array = [
            'account' => [
                'name' => 'Профиль', 'icon' =>'user', 'color' => 'aqua'],
            'finance' => [
                'name' => 'Деньги', 'icon' =>'money', 'color' => 'green'],
            'referrals' => [
                'name' => 'Рефералы', 'icon' =>'users', 'color' => 'yellow'],

            'register' => [
                'name' => 'Регистрация', 'icon' =>'user', 'color' => 'aqua'],
            'password' => [
                'name' => 'Изменение пароля', 'icon' =>'key', 'color' => 'aqua'],
            'profile_edit' => [
                'name' => 'Изменение данных', 'icon' =>'key', 'color' => 'aqua'],
            'activate' => [
                'name' => 'Активация', 'icon' =>'plus-square', 'color' => 'green'],
            'reinvest' => [
                'name' => 'Реинвест', 'icon' =>'plus-square', 'color' => 'green'],
            'level_up' => [
                'name' => 'Повышение уровня', 'icon' =>'plus-square', 'color' => 'green'],
            'confirm_email' => [
                'name' => 'Подтверждение почты', 'icon' =>'envelope', 'color' => 'aqua'],

            'payment' => [
                'name' => 'Пополнение баланса', 'icon' =>'shopping-cart', 'color' => 'aqua'],
            'cashout' => [
                'name' => 'Вывод', 'icon' =>'money', 'color' => 'red'],
            'payment_order' => [
                'name' => 'Прибыль с тарифов', 'icon' =>'comments-dollar', 'color' => 'green'],
            'payment_order_parent' => [
                'name' => 'Прибыль от рефералов', 'icon' =>'hand-holding-usd', 'color' => 'green'],

            'register_ref' => [
                'name' => 'Регистрация реферала', 'icon' =>'user-plus', 'color' => 'yellow'],
            'activate_ref' => [
                'name' => 'Активация реферала', 'icon' =>'user-check', 'color' => 'green'],
        ];

        return $array[$type][$get];
    }

    public static function userProfit($id = '', $meth = '') {

        if ($id == '') {
            $id = Auth::id();
        }

        if ($meth == 'to_inviter') {
            $toInviter = PaymentsOrder::where('to_user', $id)->where('wallet', 'to_inviter')->sum('amount');
            $parner = PaymentsOrder::where('to_user', $id)->where('wallet', 'to_partner')->sum('amount');
            $result = $toInviter + $parner;
            return $result;
        }

        $onBalance = PaymentsOrder::where('to_user', $id)->where('wallet', 'to_balance')->sum('amount');
        $parner = PaymentsOrder::where('to_user', $id)->where('wallet', 'to_partner')->sum('amount');
        $result = $onBalance + $parner;
        return $result;
    }

    public static function translit($string) {

        $string = str_replace(' ', '-', $string);
        $converter = array(
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'c',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'sch',
            'ь' => '\'',  'ы' => 'y',   'ъ' => '\'',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'C',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Sch',
            'Ь' => '\'',  'Ы' => 'Y',   'Ъ' => '\'',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',
        );


        return strtolower(strtr($string, $converter));

    }

    public static function valueArray() {
        $array = [
            'login',
            'login_parent',
            'profit',
            'profit_to_inviter',
            'link_visit',
            'count_ref',
            'balance',
            'avatar',
            'avatar_parent',
            'avatar_link',
            'avatar_parent_link',
            'last_user_login',
            'mark_enter',
            'mark_profit',
            'mark_next_user',
            'count_refs_level',
            'count_refs_active_level',
            'user_count',
            'user_activate_count',
            'all_cash',
            'hash_power',
            'activate_mark_count',
            'activate_mark_count_parent',


            'email',
            'balance_cashout',
            'affiliate_profit',
            'affiliate_profit_all',
            'last_active',

            'link_visit_parent',
            'count_ref_parent',
            'profit_parent',
            'profit_to_inviter_parent',
            'users_count',
            'activate_count',
            'transaction_sum',
            'percent_sum',
            'percent_sum_user',

            'site_logo',

            'skype',
            'telegram',
            'vk',
            'facebook',
            'twitter',
            'instagram',
            'text_status',

            'lastname',
            'name',
            'fathername',

            'skype_parent',
            'telegram_parent',
            'vk_parent',
            'facebook_parent',
            'twitter_parent',
            'instagram_parent',
            'text_status_parent',

            'lastname_parent',
            'name_parent',
            'fathername_parent',

            'referral_link',
            'telegram_referral_link',
            'telegram_bind_link',
            'telegram_id',
            'telegram_login',
        ];

        $array = array_merge($array, self::MARKS_INFO_FIELDS);

        return $array;
    }

    public static function getResultPage($page, $userID = 0)
    {
        $arRes = [];

        foreach (self::valueArray() as $val) {


            if (stripos($page, '{'.$val.'}') !== false or preg_match("~\{$val\(.*?\)\}~", $page)) {
                $result = Helpers::convertValue($val, $userID);
                $arRes['{'.$val.'}'] = $result;
            }

            if (preg_match_all("~\{$val\((.*?)\)\}~", $page, $matches)) {

                foreach ($matches[1] as $match) {
                    $data = [];
                    if ($val == 'avatar' or $val == 'avatar_parent') {
                        $imgSize = explode(':', $match);
                        $data = [];
                        if (count($imgSize)) {
                            $data['height'] = $imgSize[0];
                            $data['width'] = (isset($imgSize[1])) ? $imgSize[1] : $imgSize[0];
                        }

                        if ($data['width']) {
                            $result = self::convertValue($val, $userID, $data);
                            $arRes['{' . $val . '(' . $data['height'] . ':' . $data['width'] . ')}'] = $result;
                        }
                    }

                    if ($val == 'mark_enter' or $val == 'mark_profit' or $val == 'mark_next_user') {
                        $data['mark'] = $match;
                        $result = self::convertValue($val, $userID, $data);
                        $arRes['{' . $val . '(' . $data['mark'] . ')}'] = $result;
                    }

                    if (in_array($val, self::MARKS_INFO_FIELDS)) {
                        $dataExplode = explode('_', $match);
                        $data['mark_info']['id'] = $dataExplode[0];
                        $data['mark_info']['level'] = $dataExplode[1];
                        $result = self::convertValue($val, $userID, $data);
                        $arRes['{' . $val . '(' . $data['mark_info']['id'] . '_' . $data['mark_info']['level'] . ')}'] = $result;
                    }

                    if ($val == 'count_refs_level') {
                        $data['count_refs_level'] = $match;
                        $result = self::convertValue($val, $userID, $data);
                        $arRes['{' . $val . '(' . $data['count_refs_level'] . ')}'] = $result;
                    }

                    if ($val == 'count_refs_active_level') {
                        $data['count_refs_active_level'] = $match;
                        $result = self::convertValue($val, $userID, $data);
                        $arRes['{' . $val . '(' . $data['count_refs_active_level'] . ')}'] = $result;
                    }

                }
            }

        }

        $page = strtr($page, $arRes);

        return $page;
    }

    public static function convertValue($name, $userID = '', $data = []) {

        if ($userID == '') {
            $userID = Auth::id();
        }

        $user = Users::where('id', $userID)->first();

        $usersData = UsersData::where('id_user', $userID)->first();

        $parentCount = Users::where('id', $user->refer ?? 1)->count();
        if ($parentCount == 0) {
            Users::where('id', $userID)->update([
                'refer' => 1
            ]);
            $user = Users::where('id', $userID)->first();
        }

        $parent = Users::where('id', $user->refer ?? 1)->first();

        if (!isset($parent->users_data->link_visit)) {
            UsersData::where('id_user', $user->refer ?? 1)->update(['link_visit' => 0]);
        }

        $siteInfo = SiteInfo::find(1);
        $transactionSum = PaymentsOrder::where('type', '!=', 'inside_transaction')->where('type', '!=', 'to_balance')->sum('amount');
        $transactionSumUser = PaymentsOrder::where('type', '!=', 'inside_transaction')->where('type', '!=', 'to_balance')->where('to_user', Auth::id())->sum('amount');
        $affiliateProfit = PaymentsOrder::where('to_user', Auth::id())->where('from_user', $userID)->where('type', 'affiliate')->sum('amount');
        $affiliateProfitAll = PaymentsOrder::where('to_user', $userID)->where('type', 'affiliate')->sum('amount');
        $botUsername = Helpers::getBotUsername();

        $protocol = (isset($_SERVER['HTTPS'])) ? 'https' : 'http';

        $arrayValue = [
            'login' => $user->login,
            'balance' => $user->balance,
            'email' => $user->email,
            'balance_cashout' => $user->balance_cashout,
            'count_ref' => Users::where('refer', $user->id)->count(),
            'link_visit' => $usersData['link_visit'],
            'profit' => Helpers::userProfit($userID),
            'profit_to_inviter' => Helpers::userProfit($userID, 'to_inviter'),
            'affiliate_profit' => $affiliateProfit,
            'affiliate_profit_all' => $affiliateProfitAll,
            'last_active' => $user->last_active,
            'hash_power' => $user->balance_hash_power,
            'activate_mark_count' => $user->user_marks()->count(),

            'login_parent' => Helpers::convertUs('login', $user->refer),
            'link_visit_parent' => $parent->users_data->link_visit,
            'count_ref_parent' => Users::where('refer', $parent->id)->count(),
            'profit_parent' => Helpers::userProfit($parent->id),
            'profit_to_inviter_parent' => Helpers::userProfit($parent->id, 'to_inviter'),
            'users_count' => Users::count(),
            'activate_count' => OrdersMarkInside::count() - 9,
            'transaction_sum' => $transactionSum,
            'percent_sum' => $transactionSum/10,
            'percent_sum_user' => $transactionSumUser/10,
            'activate_mark_count_parent' => $parent->user_marks()->count(),

            'avatar' => self::getAvatar($userID, 'tag', $data),
            'avatar_link' => self::getAvatar($userID, 'link'),
            'avatar_parent_link' => self::getAvatar($parent->id, 'link'),
            'avatar_parent' => self::getAvatar($parent->id, 'tag', $data),
            'site_logo' => '<img src="/iadminbest/storage/app/'.$siteInfo->logo.'">',
            'user_count' => Users::count(),
            'user_activate_count' => Users::where('type_follow', 1)->count(),
            'all_cash' => Payments::where('id', '>', 1)->sum('amount'),

            'skype' => $user->users_data['skype'],
            'telegram' => $user->users_data['telegram'],
            'vk' => $user->users_data['vk'],
            'facebook' => $user->users_data['facebook'],
            'twitter' => $user->users_data['twitter'],
            'instagram' => $user->users_data['instagram'],
            'text_status' => $user->users_data['text_status'],

            'lastname' => $user->users_data['lastname'],
            'name' => $user->users_data['name'],
            'fathername' => $user->users_data['fathername'],


            'skype_parent' => $parent->users_data['skype'],
            'telegram_parent' => $parent->users_data['telegram'],
            'vk_parent' => $parent->users_data['vk'],
            'facebook_parent' => $parent->users_data['facebook'],
            'twitter_parent' => $parent->users_data['twitter'],
            'instagram_parent' => $parent->users_data['instagram'],
            'text_status_parent' => $parent->users_data['text_status'],

            'lastname_parent' => $parent->users_data['lastname'],
            'name_parent' => $parent->users_data['name'],
            'fathername_parent' => $parent->users_data['fathername'],

            'last_user_login' => Users::latest('regdate')->first()->login,

            'referral_link' => $protocol . '://' . $_SERVER['HTTP_HOST'] . '/?ref=' . $user->login,
            'telegram_referral_link' => 'https://t.me/' . $botUsername . '?start=refcode_' . $user->telegram_id,
            'telegram_bind_link' => 'https://t.me/' . $botUsername . '?start=bind_' . $user->login . '_' . $user->telegram_bind_code,
            'telegram_id' => $user->telegram_id,
            'telegram_login' => $user->telegram_login,
        ];

        if (isset($data['count_refs_level'])) {
            $arrayValue['count_refs_level'] = Helpers::refCountLevel($userID, $data['count_refs_level'], 0);
        }

        if (isset($data['count_refs_active_level'])) {
            $arrayValue['count_refs_active_level'] = Helpers::refCountLevel($userID, $data['count_refs_active_level'], 0, ['type' => 'active']);
        }

        if (isset($data['mark_id'])) {
            $order = OrdersMarkInside::where('id', $data['mark_id'])->first();
            $markBalance = $order->balance - $order->cashout;
            $mark = MarksInside::where('id', $order->mark)->first();
            $markProfit = PaymentsOrder::where('wallet', $data['mark_id'])
                ->sum('amount');

            $markEnter = MarksHelper::markEnter($data['mark_id']);

            $arrayValue = [
                'mark_name' => $mark->name,
                'mark_profit' => $markProfit,
                'mark_profit_all' => MarksHelper::markProfit($mark->id),
                'mark_balance' => $markBalance,
                'mark_enter' => $markEnter,
            ];
        }

        if (isset($data['mark'])) {
            $markEnter = MarksHelper::markEnter($data['mark']);
            $markProfit = MarksHelper::markProfit($data['mark']);

            $arrayValue = [
                'mark_enter' => $markEnter,
                'mark_profit' => $markProfit,
                'mark_next_user' => self::getNextUser($data['mark']),
            ];
        }

        if (isset($data['mark_info'])) {
            $data = $data['mark_info'];
            foreach (self::MARKS_INFO_FIELDS as $item) {
                $arrayValue[$item] = MarksHelper::markInfo($data['id'], $data['level'], $item);
            }
        }

        if (isset($data['deposit_id'])) {

            $bird = BirdsOrder::where('id', $data['deposit_id'])->first();
            $number = $bird->number;
            $stock = $bird->stock;
            $resource = $bird->bird->typesOrder->egg;
            $depositName = $bird->bird->name;

            $arrayValue = [
                'deposit_name' => $depositName,
                'deposit_number' => $number,
                'deposit_stock' => $stock,
                'deposit_resource' => $resource,
            ];
        }
        if (isset($arrayValue[$name])) {
            return $arrayValue[$name];
        }

    }

    public static function getInput($array) {

        $postfixLink = ($array['value'] == 'avatar') ? '_link' : '';

        $name = ($array['name'] != '')?'<label for="'.$array['name'].'">'.$array['name'].'</label>':'';
        $classForIcon = ($array['icon'] != 'none')?'input-group':'';
        $icon = ($array['icon'] != 'none')?'<span class="input-group-addon"><i class="'.$array['icon'].'"></i></span>':'';
        $value = self::convertValue($array['value'].$postfixLink);
        $input = '<input autocomplete="off" type="text" name="'.$array['value'].'" class="form-control" value="'.$value.'" placeholder="'.$array['name'].'">';

        if ($array['value'] == 'avatar') {
            $result = '<div class="kv-avatar text-center">
                            <div class="file-loading">
                                <input id="avatar" name="avatar" type="file" data-default="'.$value.'">
                            </div>
                        </div>';

        } elseif ($array['value'] == 'password') {

            $result = '
                        <label for="password">'.__('main.new_password').'</label>
                        <div class="form-group '.$classForIcon.'">
                            '.$icon.'
                            <input autocomplete="off" type="password" class="form-control" name="password" id="password" placeholder="'.__('main.new_password').'">
                        </div>
                        <label for="password_confirmation">'.__('main.confirm_new_password').'</label>
                        <div class="form-group '.$classForIcon.'">
                            '.$icon.'
                            <input autocomplete="off" type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="'.__('main.confirm_new_password').'">
                        </div>';

        } elseif ($array['value'] == 'confirm_cashout') {

            $result = $name.'
                        <select class="form-group '.$classForIcon.'" name="confirm_cashout">
                            <option value="0">Без подтверждения</option>
                            <option value="1">Подтверждение по почте</option>
                        </select>';

        } elseif ($array['value'] == 'telegram') {

            $domain = new Domain();
            $getDB = $domain->getDB();
            if ($getDB == 'testtest_iadminbest' or $getDB == 'dohodmlm_iadminbest') {

                $result = $name.'
                    <div class="js-telegram-confirm" style="">
                
                        <div class="form-group '.$classForIcon.'">
                            '.$icon.'
                            '.$input.'    
                        </div>   
                    </div>
                    ';
//                        </div>
//                        <div class="telegram-confirm-message iauth-protected" style="min-height: 60px; background: #fff; display: none; text-align: center;">
//                            <h3 class="ta-center">
//                                <span class="iauth-color">iAuth</span> защита
//                                <i class="fa fa-times teleg-close" style="float: right; cursor: pointer;"></i>
//                            </h3>
//                            <div class="telegram-for-content" style="border-bottom: 1px solid #efefef; padding: 2px;">
//
//                            </div>
//                        </div>

            } else {

                $result = $name.'
                            <div class="form-group '.$classForIcon.'">
                                '.$icon.'
                                '.$input.'
                            </div>';

            }

        } else {

            $result = $name.'
                        <div class="form-group '.$classForIcon.'">
                            '.$icon.'
                            '.$input.'
                        </div>';

        }



        return $result;

    }

    public static function getAvatar($userId = 0, $meth = 'tag', $data = []) {


        if ($userId == 0) {
            $userId = Auth::id();
        }

        $user = Users::where('id', $userId)->first();
        $avatar = $user['avatar'];

        if ($avatar == 'resource/avatars/0.jpg' or $avatar == 'no_image') {
            $siteInfo = SiteInfo::find(1);
            $result = '/iadminbest/storage/app/'.$siteInfo['no_image'];
        } else {
            $result = '/iadminbest/storage/app/'.$avatar;
        }


        if ($meth == 'tag') {
            $stylesImg = "";
            if (!empty($data['height'])) {
                $stylesImg = "style='height: $data[height]px; width: $data[width]px; display: inline-block;'";
                $stylesImg = "style='height: $data[height]px; width: $data[width]px; display: inline-block;'";
            }
            return "<img src='$result' alt='$user->login' $stylesImg>";

        } elseif ($meth == 'link') {

            return $result;

        }

    }

    public static function getNextUser($mark) {
        $mark = MarksInside::whereId($mark)->first();

        if ($mark->overflow != 2) {
            return "Ошибка: Значение только для переливов \"Под админа\"";
        }

        $overflowObj = new Multimark(1, $mark->id, true);
        $overflowRes = self::convertUs('login', $overflowObj->overflowMulti());
        return $overflowRes;

    }

    public static function selectOptionPost() {

        return Response::json([
            "result_value" => self::selectOption('value', 'login'),
            "result_icon" => self::selectOption('icon', 'users'),
            "result_value_transaction" => self::selectOption('value_transaction', 'id'),
            "result_value_input" => self::selectOption('value_input', 'avatar'),
            "result_value_links" => self::selectOption('value_links', 'avatar'),
            "result_image" => self::selectOption('image', 'avatar'),
        ], 200);

    }

    public static function selectOption($type, $value = '', $data = []) {

        $result = '';

        if ($type == 'value') {
            $siteType = self::siteType();
            $forInvest = ($siteType > 1)?'<option value="balance_cashout">'.__('main.value_balance_cashout').'</option><option value="affiliate_profit">'.__('main.value_affiliate_profit').'</option><option value="affiliate_profit_all">'.__('main.value_affiliate_profit_all').'</option>':'';

            $result = '
                <optgroup label="'.__('main.information_user').'">
                    <option value="login">'.__('main.value_login').'</option> 
                    <option value="balance">'.__('main.value_balance').'>'.
                    $forInvest
                    .'
                    <option value="count_ref">'.__('main.value_count_ref').'</option>
                    <option value="email">'.__('main.value_email').')</option>
                    <option value="link_visit">'.__('main.value_link_visit').'</option>  
                    <option value="profit">'.__('main.value_profit').'</option>
                    <option value="profit">'.__('main.value_profit_to_inviter').'</option>
                    <option value="percent_sum_user">'.__('main.value_percent_sum_user').'</option>
                    <option value="last_active">'.__('main.value_last_active').'</option>
                </optgroup>
                <optgroup label="'.__('main.social_network').'">
                    <option value="vk">'.__('main.value_vk').'</option>
                    <option value="twitter">'.__('main.value_twitter').'</option> 
                    <option value="facebook">'.__('main.value_facebook').'</option>
                    <option value="instagram">'.__('main.value_instagram').'</option>
                    <option value="skype">'.__('main.value_skype').'</option>
                    <option value="telegram">'.__('main.value_telegram').'</option>
                    <option value="text_status">'.__('main.value_text_status').'</option>
                </optgroup>
                <optgroup label="'.__('main.information_referer').'">
                    <option value="login_parent">'.__('main.value_login_parent').'</option>
                    <option value="link_visit_parent">'.__('main.value_link_visit_parent').'</option> 
                    <option value="count_ref_parent">'.__('main.value_count_ref_parent').'</option>
                    <option value="profit_parent">'.__('main.value_profit_parent').'</option>
                    <option value="profit_to_inviter_parent">'.__('main.value_profit_to_inviter_parent').'</option>
                </optgroup>
                <optgroup label="'.__('main.information_site').'">
                    <option value="users_count">'.__('main.value_users_count').'</option>
                    <option value="activate_count">'.__('main.value_activate_count').'</option>
                    <option value="transaction_sum">'.__('main.value_transaction_sum').'</option> 
                    <option value="percent_sum">'.__('main.value_percent_sum').'</option>
                </optgroup>
            ';

        } elseif ($type == 'value_input') {

            $result = '
                <optgroup label="'.__('main.information_main').'">
                    <option value="avatar">'.__('main.value_avatar').'</option> 
                    <option value="name">'.__('main.value_name').'</option> 
                    <option value="lastname">'.__('main.value_lastname').'</option>
                    <option value="fathername">'.__('main.value_fathername').'</option>
                    <option value="password">'.__('main.value_password').'</option>
                    <option value="telephone">'.__('main.value_telephone').'</option>
                </optgroup>
                <optgroup label="'.__('main.social_network').'">
                    <option value="vk">'.__('main.value_vk').'</option>
                    <option value="twitter">'.__('main.value_twitter').'</option> 
                    <option value="facebook">'.__('main.value_facebook').'</option>
                    <option value="instagram">'.__('main.value_instagram').'</option>
                    <option value="skype">'.__('main.value_skype').'</option>
                    <option value="telegram">'.__('main.value_telegram').'</option>
                    <option value="text_status">'.__('main.value_text_status').'</option>
                </optgroup>
            ';

        } elseif ($type == 'value_marks') {

            $result = '
                <optgroup label="'.__('main.information_tariff').'">
                    <option value="mark_name">'.__('main.value_mark_name').'</option> 
                    <option value="mark_profit">'.__('main.value_mark_profit').'</option> 
                    <option value="mark_profit_all">'.__('main.value_mark_profit_all').'</option> 
                    <option value="mark_balance">'.__('main.value_mark_balance').'</option>
                    <option value="mark_more">'.__('main.value_mark_more').'</option>
                </optgroup>
            ';

        } elseif ($type == 'image') {

            $result = '
                <optgroup label="'.__('main.value_images').'">
                    <option value="avatar">'.__('main.value_avatar').'</option> 
                    <option value="avatar_parent">'.__('main.value_avatar_parent').'</option>
                    <option value="site_logo">'.__('main.value_site_logo').'</option>
                </optgroup>
            ';

        } elseif ($type == 'icon') {

            $result = '
                <option value="none">'.__('main.without_icon').'</option>
                <option data-content="<i class=\'fa fa-tachometer\' aria-hidden=\'true\'></i>" value="fa fa-tachometer"></option>
                <option data-content="<i class=\'fa fa-users\' aria-hidden=\'true\'></i>" value="fa fa-users"></option>
                <option data-content="<i class=\'fa fa-sliders\' aria-hidden=\'true\'></i>" value="fa fa-sliders"></option>
                <option data-content="<i class=\'fa fa-life-ring\' aria-hidden=\'true\'></i>" value="fa fa-life-ring"></option>
                <option data-content="<i class=\'fa fa-star\' aria-hidden=\'true\'></i>" value="fa fa-star"></option>
                <option data-content="<i class=\'fa fa-bell\' aria-hidden=\'true\'></i>" value="fa fa-bell"></option>
                <option data-content="<i class=\'fa fa-bed\' aria-hidden=\'true\'></i>" value="fa fa-bed"></option>
                <option data-content="<i class=\'fa fa-bullseye\' aria-hidden=\'true\'></i>" value="fa fa-bullseye"></option>
                <option data-content="<i class=\'fa fa-calendar\' aria-hidden=\'true\'></i>" value="fa fa-calendar"></option>
                <option data-content="<i class=\'fa fa-cloud\' aria-hidden=\'true\'></i>" value="fa fa-cloud"></option>
                <option data-content="<i class=\'fa fa-comments\' aria-hidden=\'true\'></i>" value="fa fa-comments"></option>
                <option data-content="<i class=\'fa fa-cog\' aria-hidden=\'true\'></i>" value="fa fa-cog"></option>
                <option data-content="<i class=\'fa fa-gift\' aria-hidden=\'true\'></i>" value="fa fa-gift"></option>
                <option data-content="<i class=\'fa fa-envelope\' aria-hidden=\'true\'></i>" value="fa fa-envelope"></option>
                <option data-content="<i class=\'fa fa-desktop\' aria-hidden=\'true\'></i>" value="fa fa-desktop"></option>
                <option data-content="<i class=\'fa fa-glass\' aria-hidden=\'true\'></i>" value="fa fa-glass"></option>
                <option data-content="<i class=\'fa fa-leaf\' aria-hidden=\'true\'></i>" value="fa fa-leaf"></option>
                <option data-content="<i class=\'fa fa-male\' aria-hidden=\'true\'></i>" value="fa fa-male"></option>
                <option data-content="<i class=\'fa fa-plane\' aria-hidden=\'true\'></i>" value="fa fa-plane"></option>
                <option data-content="<i class=\'fa fa-sun-o\' aria-hidden=\'true\'></i>" value="fa fa-sun-o"></option>
                <option data-content="<i class=\'fa fa-sign-in\' aria-hidden=\'true\'></i>" value="fa fa-sign-in"></option>
                <option data-content="<i class=\'fa fa-rocket\' aria-hidden=\'true\'></i>" value="fa fa-rocket"></option>
                <option data-content="<i class=\'fa fa-trophy\' aria-hidden=\'true\'></i>" value="fa fa-trophy"></option>
                <option data-content="<i class=\'fa fa-venus-mars\' aria-hidden=\'true\'></i>" value="fa fa-venus-mars"></option>
                <option data-content="<i class=\'fa fa-file\' aria-hidden=\'true\'></i>" value="fa fa-file"></option>
                <option data-content="<i class=\'fa fa-area-chart\' aria-hidden=\'true\'></i>" value="fa fa-area-chart"></option>
                <option data-content="<i class=\'fa fa-bar-chart\' aria-hidden=\'true\'></i>" value="fa fa-bar-chart"></option>
                <option data-content="<i class=\'fa fa-line-chart\' aria-hidden=\'true\'></i>" value="fa fa-line-chart"></option>
                <option data-content="<i class=\'fa fa-rub\' aria-hidden=\'true\'></i>" value="fa fa-rub"></option>
                <option data-content="<i class=\'fa fa-money\' aria-hidden=\'true\'></i>" value="fa fa-money"></option>
                <option data-content="<i class=\'fa fa-eur\' aria-hidden=\'true\'></i>" value="fa fa-eur"></option>
                <option data-content="<i class=\'fa fa-btc\' aria-hidden=\'true\'></i>" value="fa fa-btc"></option>
                <option data-content="<i class=\'fa fa-usd\' aria-hidden=\'true\'></i>" value="fa fa-usd"></option>
                <option data-content="<i class=\'fab fa-ethereum\' aria-hidden=\'true\'></i>" value="fa fa-usd"></option>
                <option data-content="<i class=\'cc LTC-alt\' aria-hidden=\'true\'></i>" value="fa fa-usd"></option>
                <option data-content="<i class=\'cc XRP-alt\' aria-hidden=\'true\'></i>" value="fa fa-usd"></option>
                <option data-content="<i class=\'cc LTC-alt\' aria-hidden=\'true\'></i>" value="fa fa-usd"></option>
                <option data-content="<i class=\'fa fa-skype\' aria-hidden=\'true\'></i>" value="fa fa-skype"></option>
                <option data-content="<i class=\'fa fa-odnoklassniki\' aria-hidden=\'true\'></i>" value="fa fa-odnoklassniki"></option>
                <option data-content="<i class=\'fa fa-vk\' aria-hidden=\'true\'></i>" value="fa fa-vk"></option>
                <option data-content="<i class=\'fa fa-microchip\' aria-hidden=\'true\'></i>" value="fa fa-microchip"></option>
                <option data-content="<i class=\'fa fa-book\' aria-hidden=\'true\'></i>" value="fa fa-book"></option>
                <option data-content="<i class=\'fa fa-gear\' aria-hidden=\'true\'></i>" value="fa fa-gear"></option>
                <option data-content="<i class=\'fa fa-th\' aria-hidden=\'true\'></i>" value="fa fa-th"></option>
                <option data-content="<i class=\'fa fa-gears\' aria-hidden=\'true\'></i>" value="fa fa-gears"></option>
                <option data-content="<i class=\'fa fa-user-plus\' aria-hidden=\'true\'></i>" value="fa fa-user-plus"></option>
                <option data-content="<i class=\'fa fa-user\' aria-hidden=\'true\'></i>" value="fa fa-user"></option>
            ';

        } elseif ($type == 'value_transaction') {

            $result = '
                <option value="id_transaction">'.__('main.id_transaction').'</option>
                <option value="amount">'.__('main.amount').'</option>
                <option value="from_user">'.__('main.from_user').'</option>
                <option value="to_user">'.__('main.to_user').'</option>  
                <option value="type">'.__('main.type').'</option>
                <option value="date">'.__('main.date').'</option>
            ';

        } elseif ($type == 'value_deposit') {

            $result = '
                <option value="deposit_name">'.__('main.deposit_name').'</option>
                <option value="deposit_number">'.__('main.deposit_number').'</option>
                <option value="deposit_resource">'.__('main.deposit_resource').'</option>
                <option value="deposit_seconds">'.__('main.deposit_seconds').'</option> 
                <option value="deposit_balance">'.__('main.deposit_balance').'</option>
                <option value="deposit_stock">'.__('main.deposit_stock').'</option>
                <option value="deposit_btn">'.__('main.deposit_btn').'</option>
            ';

        } elseif ($type == 'value_store') {

            $result = '
                
                
                <option value="name">'.__('main.invest_name').'</option> 
                <option value="price">'.__('main.invest_price').'</option> 
                <optgroup label="'.__('main.time').'">
                <option value="second">'.__('main.invest_second').'</option> 
                <option value="second_to_minute">'.__('main.invest_second_to_minute').'</option>
                <option value="second_to_hour">'.__('main.invest_second_to_hour').'</option>  
                <option value="second_to_day">'.__('main.invest_second_to_day').'</option>
                
                <optgroup label="'.__('main.profit').'">
                    <option value="store_profit_second">'.__('main.invest_store_profit_second').'</option>
                    <option value="store_profit_minute">'.__('main.invest_store_profit_minute').'</option>
                    <option value="store_profit_hour">'.__('main.invest_store_profit_hour').'</option>
                    <option value="store_profit_day">'.__('main.invest_store_profit_day').'</option>
                    <option value="store_profit_all">'.__('main.invest_store_profit_all').'</option>
                </optgroup>
                <option value="number">'.__('main.invest_number').'</option>
                <option value="store_btn">'.__('main.invest_store_btn').'</option>
            ';

        } elseif ($type == 'text_align') {

            $result = '
                <option value="left">'.__('main.align_left').'</option>
                <option value="center">'.__('main.align_center').'</option>
                <option value="right">'.__('main.align_right').'</option>
            ';

        } elseif ($type == 'text_family') {

            $result = '
                <option style="font-family: Arial;" value="Arial">Arial</option>
                <option style="font-family: \'Times New Roman\';" value="Times New Roman">Times New Roman</option>
                <option style="font-family: Georgia;" value="Georgia">Georgia</option>
                <option style="font-family: \'PT Sans\';" value="PT Sans">PT Sans</option>
                <option style="font-family: \'Arial Black\';" value="Arial Black">Arial Black</option>
                <option style="font-family: \'Montserrat\';" value="Montserrat">Montserrat</option>
            ';

        } elseif ($type == 'float') {

            $result = '
                <option value="left">'.__('main.align_left').'</option>
                <option value="right">'.__('main.align_right').'</option>
            ';

        } elseif ($type == 'type_border') {

            $result = '
                <option value="solid">'.__('main.border_solid').'</option>
                <option value="double">'.__('main.border_double').'</option>
                <option value="dashed">'.__('main.border_dashed').'</option>
                <option value="dotted">'.__('main.border_dotted').'</option>
                <option value="inset">'.__('main.border_inset').'</option>
                <option value="outset">3D</option>
                <option value="groove">'.__('main.border_unset').'</option>
            ';

        } elseif ($type == 'value_links') {

            $template = Helpers::siteTemplate();
            $pages = Pages::where('name', '!=', 'profile')->where('template', $template)->get();

            $result = '
                <option value="/">'.__('main.main_page').'</option>
                <option value="/logout">'.__('main.logout_page').'</option>
                <option value="/profile">'.__('main.profile_page').'</option>
                <option value="/profile/ibux">'.__('main.connect_ibux_page').'</option>
            ';

            foreach ($pages as $page) {
                $result = $result.'<option value="/profile/'.$page['name'].'">'.$page['name_ru'].'</option>';
            }

        } elseif ($type == 'value_referrals') {

            $result = '
                <optgroup label="'.__('main.information_user').'">
                    <option value="login">'.__('main.value_login').'</option>
                    <option value="avatar">'.__('main.value_avatar').'</option>
              <option value="count_ref">'.__('main.value_count_ref').'</option>
              <option value="link_visit">'.__('main.value_link_visit').'</option>  
              <option value="profit">'.__('main.value_profit').'</option>
              <option value="last_active">'.__('main.value_last_active').'</option>
          </optgroup>
            ';

        } elseif ($type == 'column') {

            $result = '
                <option value="12">1</option>
                <option value="6">1/2</option>
                <optgroup label="3/4 + 1/4">
                    <option value="9">3/4</option>
                    <option value="3">1/4</option>
                </optgroup>
                <optgroup label="2/3 + 1/3">
                    <option value="8">2/3</option>
                    <option value="4">1/3</option>
                </optgroup>
                <optgroup label="5/6 + 1/6">
                    <option value="10">5/6</option>
                    <option value="2">1/6</option>
                </optgroup>
            ';

        } elseif ($type == 'widgets') {

            $resultMlm = '';
            $resultInvest = '';

            if (self::siteType() == 1) {
                $resultMlm = '
                <option value="activate">'.__('main.widg_activate').'</option>
                <option value="tarifs">'.__('main.widg_tarifs').'</option>';
            } else {
                $resultInvest = '
                    <option value="store">'.__('main.widg_store').'</option>
                    <option value="mydeposits">'.__('main.widg_mydeposits').'</option>
                    <option value="bonusclick">'.__('main.widg_bonusclick').'</option>
                    <option value="exchange">'.__('main.widg_exchange').'</option>
                    ';
            }

            $result = '
                <option value="profilebox">'.__('main.widg_profilebox').'</option>
                <option value="transaction">'.__('main.widg_transaction').'</option>
                <option value="balance">'.__('main.widg_balance').'</option>
                <option value="referrals">'.__('main.widg_referrals').'</option>
                <option value="news">'.__('main.widg_news').'</option>
                <option value="faq">'.__('main.widg_faq').'</option>
                <option value="promo">'.__('main.widg_promo').'</option>
                <option value="profileedit">'.__('main.widg_profileedit').'</option>
                <option value="cashout">'.__('main.widg_cashout').'</option>
                <option value="content">'.__('main.widg_content').'</option>
                '.$resultMlm.$resultInvest.'
                <option value="horizontalmenu">'.__('main.widg_horizontalmenu').'</option>
                <option value="verticalmenu">'.__('main.widg_verticalmenu').'</option>
            ';



        } elseif ($type == 'boxes') {

            if (in_array($data['widget'], ['horizontalmenu', 'verticalmenu'])) {
                $result = '
                    <option value="sidebar">'.__('main.block_for_menu').'</option>
                ';
            } else {
                $result = '
                    <option value="mainbox">'.__('main.block_for_custom').'</option>
                ';
            }

        } elseif ($type == 'marks') {
            $marks = MarksInside::select('id', 'name')->get();
            $result .= '<option value="0">Все</option>';
            foreach ($marks as $mark) {

                $result .= '<option value="' . $mark['id'] . '">' . $mark['name'] . '</option>';
            }
        }

        if ($value != '')
            $result = str_replace('value="'.$value.'"', 'value="'.$value.'" selected="selected"', $result);

        return $result;
    }

    public static function getForm($typeForm, $typeBox, $name = '', $value = [], $prefix = '') {

        $result = '';
        if ($name != '') {
            $name = '_'.$name;
        }

        if ($prefix != '') {
            $prefix = $prefix.'_';
        }

        if ($typeForm == 'border') {

            $borderWidth = (isset($value[$typeBox.'_style']['border'.$name]['border_width']))?$value[$typeBox.'_style']['border'.$name]['border_width']:'';
            $borderColor = (isset($value[$typeBox.'_style']['border'.$name]['border_color']))?$value[$typeBox.'_style']['border'.$name]['border_color']:'';
            $select = self::selectOption('type_border', (isset($value[$typeBox.'_style']['border'.$name]['border_style'])?$value[$typeBox.'_style']['border'.$name]['border_style']:''));

            $result = '
                <div class="col-lg-12 p-0">
                    <div class="col-lg-4 pl-0">
                        <label>'.__('main.width').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[border'.$name.'][border_width]" value="'.$borderWidth.'">
                    </div>
                    <div class="col-lg-4">
                        <label>'.__('main.color').'</label>
                        <input type="color" class="form-control" name="'.$typeBox.'_style[border'.$name.'][border_color]" placeholder="Цвет фона" value="'.$borderColor.'">
                    </div>
                    <div class="col-lg-4 pr-0">
                        <label>'.__('main.type').'</label>
                        <select class="form-control" name="'.$typeBox.'_style[border'.$name.'][border_style]">'.
                            $select
                        .'</select>
                    </div>
                </div>
            ';
        } elseif ($typeForm == 'border_radius') {
            $borderRadiusTL = (isset($value[$typeBox.'_style']['border_radius'.$name]['tl'])?$value[$typeBox.'_style']['border_radius'.$name]['tl']:'');
            $borderRadiusTR = (isset($value[$typeBox.'_style']['border_radius'.$name]['tr'])?$value[$typeBox.'_style']['border_radius'.$name]['tr']:'');
            $borderRadiusBR = (isset($value[$typeBox.'_style']['border_radius'.$name]['br'])?$value[$typeBox.'_style']['border_radius'.$name]['br']:'');
            $borderRadiusBL = (isset($value[$typeBox.'_style']['border_radius'.$name]['bl'])?$value[$typeBox.'_style']['border_radius'.$name]['bl']:'');

            $result = '
                <div class="form-group col-lg-12 p-0">
                    <div class="col-lg-6 pl-0">
                        <label>'.__('main.top_left').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[border_radius'.$name.'][tl]" value="'.$borderRadiusTL.'">
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>'.__('main.top_right').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[border_radius'.$name.'][tr]" value="'.$borderRadiusTR.'">
                    </div>
                    <div class="col-lg-6 pl-0">
                        <label>'.__('main.bottom_left').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[border_radius'.$name.'][bl]" value="'.$borderRadiusBL.'">
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>'.__('main.bottom_right').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[border_radius'.$name.'][br]" value="'.$borderRadiusBR.'">
                    </div>
                </div>
            ';

        } elseif ($typeForm == 'padding') {
            $paddingT = (isset($value[$typeBox.'_style']['padding'.$name]['t'])?$value[$typeBox.'_style']['padding'.$name]['t']:'');
            $paddingR = (isset($value[$typeBox.'_style']['padding'.$name]['r'])?$value[$typeBox.'_style']['padding'.$name]['r']:'');
            $paddingL = (isset($value[$typeBox.'_style']['padding'.$name]['l'])?$value[$typeBox.'_style']['padding'.$name]['l']:'');
            $paddingB = (isset($value[$typeBox.'_style']['padding'.$name]['b'])?$value[$typeBox.'_style']['padding'.$name]['b']:'');

            $result = '
                <div class="form-group col-lg-12 p-0">
                    <div class="col-lg-6 pl-0">
                        <label>'.__('main.top').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[padding'.$name.'][t]" value="'.$paddingT.'">
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>'.__('main.bottom').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[padding'.$name.'][b]" value="'.$paddingB.'">
                    </div>
                    <div class="col-lg-6 pl-0">
                        <label>'.__('main.left').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[padding'.$name.'][l]" value="'.$paddingL.'">
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>'.__('main.right').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[padding'.$name.'][r]" value="'.$paddingR.'">
                    </div>
                </div>
            ';

        } elseif ($typeForm == 'margin') {
            $marginT = (isset($value[$typeBox.'_style']['margin'.$name]['t'])?$value[$typeBox.'_style']['margin'.$name]['t']:'');
            $marginR = (isset($value[$typeBox.'_style']['margin'.$name]['r'])?$value[$typeBox.'_style']['margin'.$name]['r']:'');
            $marginL = (isset($value[$typeBox.'_style']['margin'.$name]['l'])?$value[$typeBox.'_style']['margin'.$name]['l']:'');
            $marginB = (isset($value[$typeBox.'_style']['margin'.$name]['b'])?$value[$typeBox.'_style']['margin'.$name]['b']:'');

            $result = '
                <div class="form-group col-lg-12 p-0">
                    <div class="col-lg-6 pl-0">
                        <label>'.__('main.top').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[margin'.$name.'][t]" value="'.$marginT.'">
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>'.__('main.bottom').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[margin'.$name.'][b]" value="'.$marginB.'">
                    </div>
                    <div class="col-lg-6 pl-0">
                        <label>'.__('main.left').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[margin'.$name.'][l]" value="'.$marginL.'">
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>'.__('main.right').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style[margin'.$name.'][r]" value="'.$marginR.'">
                    </div>
                </div>
            ';

        } elseif ($typeForm == 'color') {

            $color = (isset($value[$typeBox.'_style'][$prefix.'color'.$name]))?$value[$typeBox.'_style'][$prefix.'color'.$name]:'';

            $transparent = '';
            if ($prefix == 'bg_') {
                $transparentValue = (isset($value[$typeBox.'_style'][$prefix.'color'.$name.'_transparent']))?$value[$typeBox.'_style'][$prefix.'color'.$name.'_transparent']:'';
                $transparent = '<label>'.__('main.opacity').'</label><input type="number" min="0" max="1" step="0.1" class="form-control" name="'.$typeBox.'_style['.$prefix.'color'.$name.'_transparent]"value="'.$transparentValue.'">';
            }
            $result = '
                <input type="color" class="form-control" name="'.$typeBox.'_style['.$prefix.'color'.$name.']"
                value="'.$color.'">'.$transparent.'
            ';

        } elseif ($typeForm == 'width') {

            $width = (isset($value[$typeBox.'_style']['width'.$name]))?$value[$typeBox.'_style']['width'.$name]:'';

            $result = '
                <input type="number" class="form-control" name="'.$typeBox.'_style['.$prefix.'width'.$name.']"
                value="'.$width.'">
            ';

        } elseif ($typeForm == 'height') {

            $height = (isset($value[$typeBox.'_style']['height'.$name]))?$value[$typeBox.'_style']['height'.$name]:'';

            $result = '
                <input type="number" class="form-control" name="'.$typeBox.'_style['.$prefix.'height'.$name.']"
                value="'.$height.'">
            ';

        } elseif ($typeForm == 'margin') {

//            $margin = (isset($value[$typeBox.'_style']['margin'.$name]))?$value[$typeBox.'_style']['margin'.$name]:'';
//
//            $result = '
//                <input type="number" class="form-control" name="'.$typeBox.'_style['.$prefix.'margin'.$name.']"
//                value="'.$margin.'">
//            ';


        } elseif ($typeForm == 'align') {

            $align = (isset($value[$typeBox.'_style'][$prefix.'text_align'.$name]))?$value[$typeBox.'_style'][$prefix.'text_align'.$name]:'';
            $family = (isset($value[$typeBox.'_style'][$prefix.'font'.$name]['family']))?$value[$typeBox.'_style'][$prefix.'font'.$name]['family']:'';
            $select = self::selectOption('text_align', $align);
            $selectFontFamily = self::selectOption('text_family', $family);
            $bold = (isset($value[$typeBox.'_style'][$prefix.'font'.$name]['weight']))?($value[$typeBox.'_style'][$prefix.'font'.$name]['weight'] == '1')?'1':'0':'0';
            $italic = (isset($value[$typeBox.'_style'][$prefix.'font'.$name]['style']))?($value[$typeBox.'_style'][$prefix.'font'.$name]['style'] == '1')?'1':'0':'0';
            $size = (isset($value[$typeBox.'_style'][$prefix.'font'.$name]['size']))?$value[$typeBox.'_style'][$prefix.'font'.$name]['size']:'0';

            $boldChecked = ($bold == 1)?'checked="checked"':'';
            $italicChecked = ($italic == 1)?'checked="checked"':'';

            $result = '
                <div class="text-form-edit mt-5">
                    <div class="col-lg-6">
                        <select class="form-control text-align-select" name="'.$typeBox.'_style['.$prefix.'text_align'.$name.']" style="display:none;">'.
                            $select
                        .'</select>
                    </div>
                    <div class="col-lg-12 p-0">
                        <select class="form-control selectpicker" name="'.$typeBox.'_style['.$prefix.'font'.$name.'][family]" style="">'.
                            $selectFontFamily
                        .'</select>
                    </div>
                    <div class="col-lg-4 pl-0">
                        <label>'.__('main.font_size').'</label>
                        <input type="number" class="form-control" name="'.$typeBox.'_style['.$prefix.'font'.$name.'][size]" value="'.$size.'">
                    </div>
                    <div class="col-lg-8 pr-0">
                        <label>'.__('main.font_setting').'</label>
                        <br>
                        <div class="btn-group">
                          <button type="button" class="btn btn-default text-align-edit" data-align="left"><i class="fa fa-align-left"></i></button>
                          <button type="button" class="btn btn-default text-align-edit" data-align="center"><i class="fa fa-align-center"></i></button>
                          <button type="button" class="btn btn-default text-align-edit" data-align="right"><i class="fa fa-align-right"></i></button>
                        </div>
                        <div class="btn-group">
                          <button type="button" class="btn btn-default font-weight" data-align="bold"><i class="fa fa-bold"></i></button>
                          <button type="button" class="btn btn-default font-style" data-align="italic"><i class="fa fa-italic"></i></button>
                        </div>
                        <input type="checkbox" name="'.$typeBox.'_style['.$prefix.'font'.$name.'][weight]" class="js-edit-bold" style="display: none;" value="1" '.$boldChecked.'>
                        <input type="checkbox" name="'.$typeBox.'_style['.$prefix.'font'.$name.'][style]" class="js-edit-italic" style="display: none;" value="1" '.$italicChecked.'>
                    </div>
                </div>
            ';

        } elseif ($typeForm == 'float') {

            $align = (isset($value[$typeBox.'_style']['float'.$name]))?$value[$typeBox.'_style']['float'.$name]:'';
            $select = self::selectOption('float', $align);

            $result = '
                <select class="form-control" name="'.$typeBox.'_style['.$prefix.'float'.$name.']">'.
                    $select
                .'</select>
            ';

        } elseif ($typeForm == 'btn') {

            $width = self::getForm('width', $typeBox, 'btn'.$name, $value);

            $borderRadius = self::getForm('border_radius', $typeBox, 'btn'.$name, $value);
            $borderRadiusH = self::getForm('border_radius', $typeBox, 'btn_h'.$name, $value);

            $border = self::getForm('border', $typeBox, 'btn'.$name, $value);
            $borderH = self::getForm('border', $typeBox, 'btn_h'.$name, $value);

            $bgColor = self::getForm('color', $typeBox, 'btn'.$name, $value, 'bg');
            $bgColorH = self::getForm('color', $typeBox, 'btn_h'.$name, $value, 'bg');

            $color = self::getForm('color', $typeBox, 'btn'.$name, $value);
            $colorH = self::getForm('color', $typeBox, 'btn_h'.$name, $value);


            $result = '
                <h5>'.__('main.decor').'</h5>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#style-btn'.$name.'-'.$typeBox.'" data-toggle="tab" aria-expanded="true">'.__('main.btn_static').'</a></li>
                        <li><a href="#style-btn-hover'.$name.'-'.$typeBox.'" data-toggle="tab" aria-expanded="true">'.__('main.btn_hover').'</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="style-btn'.$name.'-'.$typeBox.'">
                            <div class="col-lg-4 pl-0">
                                <h5>'.__('main.width').'</h5>
                                <label>'.__('main.width').'</label>
                                '.$width.'
                            </div>
                            <div class="col-lg-4 pl-0">
                                <h5>'.__('main.color').'</h5>
                                <label>'.__('main.bg_color').'</label>
                                '.$bgColor.'
                            </div>
                            <div class="col-lg-4 pr-0">
                                <label>'.__('main.text_color').'</label>
                                '.$color.'
                            </div>
                            <div class="clearfix"></div>

                            <h5>'.__('main.border_radius').'</h5>
                            '.$borderRadius.'
                            <div class="clearfix"></div>

                            <h5>'.__('main.border').'</h5>
                            '.$border.'
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-pane" id="style-btn-hover'.$name.'-'.$typeBox.'">
                            <h5>'.__('main.color').'</h5>
                            <div class="col-lg-4 pl-0">
                                <label>'.__('main.bg_color').'</label>
                                '.$bgColorH.'
                            </div>
                            <div class="col-lg-4 pr-0">
                                <label>'.__('main.text_color').'</label>
                                '.$colorH.'
                            </div>
                            <div class="clearfix"></div>

                            <h5>'.__('main.border_radius').'</h5>
                            '.$borderRadiusH.'
                            <div class="clearfix"></div>

                            <h5>'.__('main.border').'</h5>
                            '.$borderH.'
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            ';

        } elseif ($typeForm == 'pagination') {


            $width = self::getForm('width', $typeBox, 'btn'.$name, $value);

            $borderRadius = self::getForm('border_radius', $typeBox, 'btn'.$name, $value);
            $borderRadiusH = self::getForm('border_radius', $typeBox, 'btn_h'.$name, $value);
            $borderRadiusA = self::getForm('border_radius', $typeBox, 'btn_a'.$name, $value);

            $border = self::getForm('border', $typeBox, 'btn'.$name, $value);
            $borderH = self::getForm('border', $typeBox, 'btn_h'.$name, $value);
            $borderA = self::getForm('border', $typeBox, 'btn_a'.$name, $value);

            $bgColor = self::getForm('color', $typeBox, 'btn'.$name, $value, 'bg');
            $bgColorH = self::getForm('color', $typeBox, 'btn_h'.$name, $value, 'bg');
            $bgColorA = self::getForm('color', $typeBox, 'btn_a'.$name, $value, 'bg');

            $color = self::getForm('color', $typeBox, 'btn'.$name, $value);
            $colorH = self::getForm('color', $typeBox, 'btn_h'.$name, $value);
            $colorA = self::getForm('color', $typeBox, 'btn_a'.$name, $value);


            $result = '
                <h5>'.__('main.pagination').'</h5>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#paginate-btn'.$name.'" data-toggle="tab" aria-expanded="true">'.__('main.btn_static').'</a></li>
                        <li><a href="#paginate-btn-hover'.$name.'" data-toggle="tab" aria-expanded="true">'.__('main.btn_hover').'</a></li>
                        <li><a href="#paginate-btn-active'.$name.'" data-toggle="tab" aria-expanded="true">'.__('main.btn_active').'</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="paginate-btn'.$name.'">
                            <h5>'.__('main.color').'</h5>
                            <div class="col-lg-4 pl-0">
                                <label>'.__('main.bg_color').'</label>
                                '.$bgColor.'
                            </div>
                            <div class="col-lg-4 pr-0">
                                <label>'.__('main.text_color').'</label>
                                '.$color.'
                            </div>
                            <div class="clearfix"></div>

                            <h5>'.__('main.border_radius').'</h5>
                            '.$borderRadius.'
                            <div class="clearfix"></div>
                            <h5>'.__('main.border').'</h5>
                            '.$border.'
                            <div class="clearfix"></div>
                        </div>
                        <div class="tab-pane" id="paginate-btn-hover'.$name.'">
                            <h5>'.__('main.color').'</h5>
                            <div class="col-lg-4 pl-0">
                                <label>'.__('main.bg_color').'</label>
                                '.$bgColorH.'
                            </div>
                            <div class="col-lg-4 pr-0">
                                <label>'.__('main.text_color').'</label>
                                '.$colorH.'
                            </div>
                            <div class="clearfix"></div>

                            <h5>'.__('main.border_radius').'</h5>
                            '.$borderRadiusH.'
                            <div class="clearfix"></div>

                            <h5>'.__('main.border').'</h5>
                            '.$borderH.'
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="tab-pane" id="paginate-btn-active'.$name.'">
                            <h5>'.__('main.color').'</h5>
                            <div class="col-lg-4 pl-0">
                                <label>'.__('main.bg_color').'</label>
                                '.$bgColorA.'
                            </div>
                            <div class="col-lg-4 pr-0">
                                <label>'.__('main.text_color').'</label>
                                '.$colorA.'
                            </div>
                            <div class="clearfix"></div>

                            <h5>'.__('main.border_radius').'</h5>
                            '.$borderRadiusA.'
                            <div class="clearfix"></div>

                            <h5>'.__('main.border').'</h5>
                            '.$borderA.'
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            ';

        }

        return $result;
    }

    public static function getStyle($blockId, $demo = 0)
    {

        if ($demo) {
            $classSiteElements = \App\Models\CoreSiteElements::query();
            $template = $demo;
        } else {
            $classSiteElements = SiteElements::query();
            $template = Helpers::siteTemplate();
        }

        $box = $classSiteElements->where('id_in_system', $blockId)->where('template',$template)->first();
        $optionValue = self::serialize_corrector($box['option_value']);
        $widget = $optionValue['widget'];
        $classBox = '#box-' . $box['id_in_system'];

        $result = '';
        $style = '';



        $typeStyle = (isset($optionValue['box_style']))?$optionValue['box_style']:null;

        if (isset($typeStyle)) {
            $typeStyle = self::ifIsset($optionValue['box_style']);

            if ($box['type'] == 'sidebar') {
                $mt = (isset($typeStyle['margin']['top']))?$typeStyle['margin']['top']:'';
                $mb = (isset($typeStyle['margin']['bottom']))?$typeStyle['margin']['bottom']:'';
                $style =
                    $classBox . ' {
                        margin-top: ' . $mt . 'px;
                        margin-bottom: ' . $mb . 'px;
                        float: ' . $typeStyle['float'] .';
                        
                    }
                ';
                $result = $result.$style;

            }
            if ($box['type'] == 'mainbox') {

                $border = self::getProperty('border', $typeStyle['border']);
                $font = self::getProperty('font', (isset($typeStyle['font_head']))?$typeStyle['font_head']:'');

                $headAlign =  $typeStyle['text_align_head'];
                $background = $typeStyle['bg_color'] ?? '';
                $transparent = self::getProperty('transparent', [ 'transparent' => (isset($typeStyle['bg_color_transparent']))?$typeStyle['bg_color_transparent'] : '']);
                $backgroundHead = $typeStyle['bg_color_head'] ?? '';
                $transparentHead = self::getProperty('transparent', [ 'transparent' => (isset($typeStyle['bg_color_head_transparent']))?$typeStyle['bg_color_head_transparent'] : '']);
                $colorHead = $typeStyle['color_head'];
                $pl = (isset($typeStyle['padding']['left']))?$typeStyle['padding']['left']:'';
                $pr = (isset($typeStyle['padding']['right']))?$typeStyle['padding']['right']:'';

                $borderRadius = self::getProperty('border_radius', $typeStyle['border_radius']);

                $style =
                    $classBox . ' {
                        padding-left: ' . $pl . 'px;
                        padding-right: ' . $pr . 'px;
                        margin-left: 0px;
                        margin-right: 0px;
                        
                    }'.
                    $classBox . ' .content {
                        margin-left: 0px;
                        margin-right: 0px;
                        
                    }
                    '.$classBox . ' .box-footer {
                        background-color: ' . $background . ';
                    }
                    '.$classBox . ' .box-footer {
                        background-color: ' . $background . ';
                    }
                    '.$classBox . ' .box {
                        background-color: ' . $background . $transparent . ';
                        border: '.$border.';
                        border-radius: '.$borderRadius.';
                    }' .
                    $classBox . ' .box-header {
                        text-align: ' . $headAlign . ';
                        color: ' . $colorHead . ';
                        background: ' . $backgroundHead . $transparentHead . ';
                    }' .
                    $classBox . ' .box-header h3 {
                        font: ' . $font .';
                    }
                    
                ';
                $result = $result.$style;

            } elseif ($box['type'] == 'smallbox') {

            }
        }

        $typeStyle = (isset($optionValue['widget_style']))?$optionValue['widget_style']:null;

        if (isset($typeStyle)) {
            $typeStyle = self::ifIsset($optionValue['widget_style']);

            if ($widget == 'profilebox') {

                $borderImg = self::getProperty('border', $typeStyle['border_img']);
                $borderLine = self::getProperty('border', $typeStyle['border_line']);

                $bgColorElements =  $typeStyle['bg_color_elements'];

                $colorTextLeft = $typeStyle['color_tl'];
                $colorTextRight = $typeStyle['color_tr'];

                $borderRadius = self::getProperty('border_radius', $typeStyle['border_radius']);

                $widthImg = $typeStyle['width_img'];
                $colorLogin = $typeStyle['color_login'];

                $bgColorBtn = $typeStyle['bg_color_btn'];
                $colorBtn = $typeStyle['color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $colorBtnH = $typeStyle['color_btn_h'];
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);
                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);

                $style =
                    $classBox . ' img {
                        width: ' . $widthImg . 'px;
                        border: ' . $borderImg . ';
                        border-radius: '.$borderRadius.';
                        
                    }
                    '.$classBox . ' .list-group-item {
                        border: ' . $borderLine . ';
                        background-color: ' . $bgColorElements.';
                        padding-right: 10px;
                        padding-left: 10px;
                    }
                    '.$classBox . ' .list-group-item b {
                        color: ' . $colorTextLeft . ';
                    }
                    '.$classBox . ' .list-group-item a {
                        color: ' . $colorTextRight . ';
                    }
                    '.$classBox . ' .profile-username {
                        color: ' . $colorLogin . ';
                    }'.
                    $classBox . ' .btn-primary {
                        background-color: '.$bgColorBtn.';
                        color: '.$colorBtn.';
                        border-radius: '.$borderRadiusBtn.';
                        border: ' . $borderBtn . ';
                    }'.
                    $classBox . ' .btn-primary:hover {
                        background-color: '.$bgColorBtnH.';
                        color: '.$colorBtnH.';
                        border-radius: '.$borderRadiusBtnH.';
                        border: ' . $borderBtnH . ';
                    }';

            } elseif ($widget == 'balance') {

                $labelTextAlign = $typeStyle['text_align_label'];
                $labelTextFont = $borderInput = self::getProperty('font', (isset($typeStyle['font_label']))?$typeStyle['font_label']:'');
                $labelColor = $typeStyle['label_color_text'];

                $bgColorInput = $typeStyle['bg_color_input'] ?? '';
                $colorInput = $typeStyle['color_input'] ?? '';

                $borderInput = self::getProperty('border', $typeStyle['border_input']);
                $borderRadiusInput = self::getProperty('border_radius', $typeStyle['border_radius_input']);

                $widthBtn = $typeStyle['width_btn'];

                $bgColorBtn = $typeStyle['bg_color_btn'];
                $colorBtn = $typeStyle['color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $colorBtnH = $typeStyle['color_btn_h'];

                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);


                $style =
                    $classBox . ' .label-sum {
                        text-align: '.$labelTextAlign.';
                        color: '.$labelColor.';
                        font: '.$labelTextFont.';
                    }'.
                    $classBox . ' input {
                        border: ' . $borderInput . ';
                        background: '.$bgColorInput.';
                        color: '.$colorInput.';
                        border-radius: '.$borderRadiusInput.';
                    }'.
                    $classBox . ' .btn-primary {
                        display: block;
                        margin: auto;
                        width: '.$widthBtn.'px;
                        background-color: '.$bgColorBtn.';
                        color: '.$colorBtn.';
                        border-radius: '.$borderRadiusBtn.';
                        border: ' . $borderBtn . ';
                    }'.
                    $classBox . ' .btn-primary:hover {
                        background-color: '.$bgColorBtnH.';
                        color: '.$colorBtnH.';
                        border-radius: '.$borderRadiusBtnH.';
                        border: ' . $borderBtnH . ';

                    }
                ';

            } elseif ($widget == 'exchange') {

                $labelTextAlign = $typeStyle['text_align_label'];
                $labelTextFont = $borderInput = self::getProperty('font', (isset($typeStyle['font_label']))?$typeStyle['font_label']:'');
                $labelColor = $typeStyle['label_color_text'];

                $bgColorInput = $typeStyle['bg_color_input'] ?? '';
                $colorInput = $typeStyle['color_input'] ?? '';

                $borderInput = self::getProperty('border', $typeStyle['border_input']);
                $borderRadiusInput = self::getProperty('border_radius', $typeStyle['border_radius_input']);

                $widthBtn = $typeStyle['width_btn'];

                $bgColorBtn = $typeStyle['bg_color_btn'];
                $colorBtn = $typeStyle['color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $colorBtnH = $typeStyle['color_btn_h'];

                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);


                $style =
                    $classBox . ' .label-sum {
                        text-align: '.$labelTextAlign.';
                        color: '.$labelColor.';
                        font: '.$labelTextFont.';
                    }'.
                    $classBox . ' input {
                        border: ' . $borderInput . ';
                        background: '.$bgColorInput.';
                        color: '.$colorInput.';
                        border-radius: '.$borderRadiusInput.';
                    }'.
                    $classBox . ' .btn-primary {
                        display: block;
                        margin: auto;
                        width: '.$widthBtn.'px;
                        background-color: '.$bgColorBtn.';
                        color: '.$colorBtn.';
                        border-radius: '.$borderRadiusBtn.';
                        border: ' . $borderBtn . ';
                    }'.
                    $classBox . ' .btn-primary:hover {
                        background-color: '.$bgColorBtnH.';
                        color: '.$colorBtnH.';
                        border-radius: '.$borderRadiusBtnH.';
                        border: ' . $borderBtnH . ';

                    }
                ';

            } elseif ($widget == 'bonusclick') {

                $textAlign = $typeStyle['text_align_text_area'] ?? '';
                $textFont = $borderInput = self::getProperty('font', (isset($typeStyle['font_text_area']))?$typeStyle['font_text_area']:'');
                $textColor = $typeStyle['color_text_area'] ?? '';

                $bgColorInput = $typeStyle['bg_color_input'] ?? '';
                $colorInput = $typeStyle['color_input'] ?? '';

                $borderInput = self::getProperty('border', $typeStyle['border_input']);
                $borderRadiusInput = self::getProperty('border_radius', $typeStyle['border_radius_input']);

                $widthBtn = $typeStyle['width_btn'];

                $bgColorBtn = $typeStyle['bg_color_btn'];
                $colorBtn = $typeStyle['color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $colorBtnH = $typeStyle['color_btn_h'];

                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);


                $style =
                    $classBox . ' .text-bonus {
                        text-align: '.$textAlign.';
                        color: '.$textColor.';
                        font: '.$textFont.';
                    }'.
                    $classBox . ' input {
                        border: ' . $borderInput . ';
                        background: '.$bgColorInput.';
                        color: '.$colorInput.';
                        border-radius: '.$borderRadiusInput.';
                    }'.
                    $classBox . ' .btn-primary {
                        display: block;
                        margin: auto;
                        width: '.$widthBtn.'px;
                        background-color: '.$bgColorBtn.';
                        color: '.$colorBtn.';
                        border-radius: '.$borderRadiusBtn.';
                        border: ' . $borderBtn . ';
                    }'.
                    $classBox . ' .btn-primary:hover {
                        background-color: '.$bgColorBtnH.';
                        color: '.$colorBtnH.';
                        border-radius: '.$borderRadiusBtnH.';
                        border: ' . $borderBtnH . ';

                    }
                ';

            } elseif ($widget == 'profileedit') {

                $borderAvatar = self::getProperty('border', (isset($typeStyle['border_avatar']))?$typeStyle['border_avatar']:'');
                $borderRadiusAvatar = self::getProperty('border_radius', (isset($typeStyle['border_radius_avatar']))?$typeStyle['border_radius_avatar']:'');

                $labelTextAlign = $typeStyle['text_align_label'];
                $labelTextFont = self::getProperty('font', (isset($typeStyle['label_font_text']))?$typeStyle['label_font_text']:'');
                $labelColor = $typeStyle['label_color_text'];

                $bgColorInput = $typeStyle['bg_color_input'];
                $colorInput = $typeStyle['color_input'];

                $borderInput = self::getProperty('border', $typeStyle['border_input']);
                $borderRadiusInput = self::getProperty('border_radius', $typeStyle['border_radius_input']);

                $widthBtn = $typeStyle['width_btn'];

                $bgColorBtn = $typeStyle['bg_color_btn'];
                $colorBtn = $typeStyle['color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $colorBtnH = $typeStyle['color_btn_h'];

                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);




                $style =
                    $classBox . ' {
                        font: ' . $labelTextFont . '
                    } ' .
                    $classBox . ' label {
                        text-align: '.$labelTextAlign.';
                        color: '.$labelColor.';
                        font: '.$labelTextFont.';
                    }
                    '.$classBox . ' .btn.btn-file {
                        margin-top: 10px;
                    }
                    '.$classBox . ' .file-drop-zone {
                        border: none;
                    }
                    '.$classBox . ' .file-preview img {
                        border-radius: '.$borderRadiusAvatar.';
                    }
                    '.$classBox . ' .file-preview {
                        border: '.$borderAvatar.';
                        border-radius: '.$borderRadiusAvatar.';
                    }
                    '.$classBox . ' input{
                        background: '.$bgColorInput.';
                        color: '.$colorInput.';
                        border: ' . $borderInput . ';
                        border-radius: '.$borderRadiusInput.';
                    }
                    '.$classBox . ' .input-group {
                        overflow: hidden;
                        border: ' . $borderInput . ';
                        border-radius: '.$borderRadiusInput.';
                    }
                    '.$classBox . ' .input-group > input {
                        border-radius: 0px;
                        border: none;
                    }
                    '.$classBox.' .input-group .input-group-addon {
                        background: '.$bgColorInput.';
                        color: '.$colorInput.';
                        border: none;
                    }
                    
                    '.$classBox . ' .btn-primary {
                        display: block;
                        margin: auto;
                        width: '.$widthBtn.'px;
                        background-color: '.$bgColorBtn.';
                        color: '.$colorBtn.';
                        border-radius: '.$borderRadiusBtn.';
                        border: ' . $borderBtn . ';
                    }
                    '.$classBox . ' .btn-primary:hover {
                        background-color: '.$bgColorBtnH.';
                        color: '.$colorBtnH.';
                        border-radius: '.$borderRadiusBtnH.';
                        border: ' . $borderBtnH . ';

                    }
                ';

            } elseif ($widget == 'cashout') {

                $labelTextAlign = $typeStyle['text_align_label'];
                $labelColor = $typeStyle['label_color_text'];
                $labelFont = self::getProperty('font', $typeStyle['font_label']);

                $bgColorInput = $typeStyle['bg_color_input'];
                $colorInput = $typeStyle['color_input'];

                $borderInput = self::getProperty('border', $typeStyle['border_input']);
                $borderRadiusInput = self::getProperty('border_radius', $typeStyle['border_radius_input']);

                $widthBtn = $typeStyle['width_btn'];

                $bgColorBtn = $typeStyle['bg_color_btn'];
                $colorBtn = $typeStyle['color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $colorBtnH = $typeStyle['color_btn_h'];

                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);




                $style =
                    $classBox . ' {
                        font: ' . $labelFont . '
                    } ' .
                    $classBox . ' label {
                        width: 100%;
                        text-align: '.$labelTextAlign.';
                        color: '.$labelColor.';
                        font: '.$labelFont.';
                    }'.
                    $classBox . ' input {
                        border: ' . $borderInput . ';
                        background: '.$bgColorInput.';
                        color: '.$colorInput.';
                        border-radius: '.$borderRadiusInput.';
                    }'.
                    $classBox . ' .btn-primary {
                        display: block;
                        margin: auto;
                        width: '.$widthBtn.'px;
                        background-color: '.$bgColorBtn.';
                        color: '.$colorBtn.';
                        border-radius: '.$borderRadiusBtn.';
                        border: ' . $borderBtn . ';
                    }'.
                    $classBox . ' .btn-primary:hover {
                        background-color: '.$bgColorBtnH.';
                        color: '.$colorBtnH.';
                        border-radius: '.$borderRadiusBtnH.';
                        border: ' . $borderBtnH . ';

                    }
                ';

            } elseif ($widget == 'news') {

                $bgColorHead = $typeStyle['bg_color_head'];
                $colorHead = $typeStyle['color_head'];
                $colorDate = $typeStyle['color_date'];
                $alignHead = $typeStyle['text_align_head'];

                $borderHead = self::getProperty('border', $typeStyle['border_head']);

                $bgColorTimelineDate = $typeStyle['bg_color_timeline_date'];
                $bgColorTimelineLine = $typeStyle['bg_color_timeline_line'];
                $colorTimeline = $typeStyle['color_timeline'];

                $bgColorText = $typeStyle['bg_color_text'];
                $colorText = $typeStyle['color_text'];
                $alignText = $typeStyle['text_align_text'];

                $bgColorFooter = $typeStyle['bg_color_footer'];
                $alignFooter = $typeStyle['text_align_footer'];

                $borderRadiusTimeline = self::getProperty('border_radius', $typeStyle['border_radius_timeline']);
                $borderRadiusTimelineIcon = self::getProperty('border_radius', $typeStyle['border_radius_timeline_icon']);



                $widthBtn = $typeStyle['width_btn'];

                $bgColorBtn = $typeStyle['bg_color_btn'];
                $colorBtn = $typeStyle['color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $colorBtnH = $typeStyle['color_btn_h'];

                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);

                $typeStyle = array_filter($typeStyle, function($element) {
                    return !empty($element);
                });

                $style =
                    $classBox . ' .timeline-header {
                        border-bottom: ' . $borderHead . ';
                        background: ' . $bgColorHead . ';
                        color: ' . $colorHead . ';
                        text-align: ' . $alignHead . ';
                    }
                    '.$classBox . ' .time {
                        color: ' . $colorDate . ';
                    }
                    '.$classBox . ' .timeline:before {
                        background: ' . $bgColorTimelineLine . ';
                    }
                    '.$classBox . ' .time-label span {
                        background: ' . $bgColorTimelineDate . ';
                        color: ' . $colorTimeline . ';
                        border-radius: '.$borderRadiusTimeline.';
                    }
                    '.$classBox . ' .timeline-body {
                        background: ' . $bgColorText . ';
                        color: ' . $colorText . ';
                        text-align: ' . $alignText . ';
                    }
                    '.$classBox . ' .timeline-footer {
                        background: ' . $bgColorFooter . ';
                        text-align: ' . $alignFooter . ';
                    }
                    '.$classBox . ' .fa {
                        border-radius: '.$borderRadiusTimelineIcon.';
                    }'.
                    $classBox . ' .btn-primary {
                        width: '.$widthBtn.'px;
                        background-color: '.$bgColorBtn.';
                        color: '.$colorBtn.';
                        border-radius: '.$borderRadiusBtn.';
                        border: ' . $borderBtn . ';
                    }'.
                    $classBox . ' .btn-primary:hover {
                        background-color: '.$bgColorBtnH.';
                        color: '.$colorBtnH.';
                        border-radius: '.$borderRadiusBtnH.';
                        border: ' . $borderBtnH . ';
                    }
                ';

            } elseif ($widget == 'transaction') {

                $borderLine = self::getProperty('border', $typeStyle['border_line']);

                $bgColorTH = $typeStyle['bg_color_th'];
                $colorTH = $typeStyle['color_th'];

                $alignTable = $typeStyle['text_align_table'];
                $fontTable = self::getProperty('font', $typeStyle['font_table']);

                $bgColorFrom = $typeStyle['bg_color_from'];
                $colorFrom = $typeStyle['color_from'];

                $bgColorTo = $typeStyle['bg_color_to'];
                $colorTo = $typeStyle['color_to'];

                $bgColorTDH = $typeStyle['bg_color_td_h'];
                $colorTDH = $typeStyle['color_td_h'];


                $bgColorBtn = $typeStyle['bg_color_btn'];
                $colorBtn = $typeStyle['color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $bgColorBtnA = $typeStyle['bg_color_btn_a'];
                $colorBtnH = $typeStyle['color_btn_h'];
                $colorBtnA = $typeStyle['color_btn_a'];
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);
                $borderBtnA = self::getProperty('border', $typeStyle['border_btn_a']);
                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);
                $borderRadiusBtnA = self::getProperty('border_radius', $typeStyle['border_radius_btn_a']);

                $style =
                    $classBox . ' td {
                        border: ' . $borderLine . ';
                        font: ' . $fontTable . ';
                        text-align: ' . $alignTable . ';
                    }
                    '.$classBox . ' table {
                        text-align: ' . $alignTable . ';
                        font: ' . $fontTable . ';
                    }
                    '.$classBox . ' th {
                        background: ' . $bgColorTH.';
                        color: ' . $colorTH.';
                        font: ' . $fontTable . ';
                        text-align: ' . $alignTable . ';
                        
                    }
                    '.$classBox . ' .to-transaction {
                        background: ' . $bgColorTo.';
                        color: ' . $colorTo.';
                    }
                    '.$classBox . ' .from-transaction {
                        background: ' . $bgColorFrom.';
                        color: ' . $colorFrom.';
                    }
                    '.$classBox . ' tr:hover {
                        background: ' . $bgColorTDH.';
                        color: ' . $colorTDH.';
                    }
                    '.$classBox . ' .pagination > li > a,
                    '.$classBox . ' .pagination > .disabled > span
                    {
                        background: ' . $bgColorBtn.';
                        color: ' . $colorBtn.';
                        border: ' . $borderBtn.';
                        border-radius: ' . $borderRadiusBtn.';
                    }
                    '.$classBox . ' .pagination > li > a:hover,
                    '.$classBox . ' .pagination > li > span:hover {
                        background: ' . $bgColorBtnH.';
                        color: ' . $colorBtnH.';
                        border: ' . $borderBtnH.';
                        border-radius: ' . $borderRadiusBtnH.';
                    }
                    '.$classBox . ' .pagination > .active > a,
                    '.$classBox . ' .pagination > .active > a:hover,
                    '.$classBox . ' .pagination > .active > span,
                    '.$classBox . ' .pagination > .active > span:hover,
                    '.$classBox . ' .pagination > li > span
                    {
                        background: ' . $bgColorBtnA.';
                        color: ' . $colorBtnA.';
                        border: ' . $borderBtnA.';
                        border-radius: ' . $borderRadiusBtnA.';
                    }
                    
                    '.$classBox . ' .pagination > .disabled > span,
                    '.$classBox . ' .pagination > .disabled > span:hover
                    {
                        background: ' . $bgColorBtn.'aa;
                        border: ' . $borderBtn.';
                        border-radius: ' . $borderRadiusBtn.';
                        color: ' . $colorBtn.';
                    }
                ';

            } elseif ($widget == 'referrals') {

                $borderLine = self::getProperty('border', $typeStyle['border_line']);

                $bgColorTH = $typeStyle['bg_color_th'];
                $colorTH = $typeStyle['color_th'];

                $alignTable = $typeStyle['text_align_table'];
                $fontTable = self::getProperty('font', $typeStyle['font_table']);

                $bgColorTDH = $typeStyle['bg_color_td_h'];
                $colorTDH = $typeStyle['color_td_h'];

                $bgColorActivate = $typeStyle['bg_color_activate'];
                $colorActivate = $typeStyle['color_activate'];

                $bgColorNotActivate = $typeStyle['bg_color_not_activate'];
                $colorNotActivate = $typeStyle['color_not_activate'];


                $bgColorBtn = $typeStyle['bg_color_btn'];
                $colorBtn = $typeStyle['color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $bgColorBtnA = $typeStyle['bg_color_btn_a'];
                $colorBtnH = $typeStyle['color_btn_h'];
                $colorBtnA = $typeStyle['color_btn_a'];
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);
                $borderBtnA = self::getProperty('border', $typeStyle['border_btn_a']);
                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);
                $borderRadiusBtnA = self::getProperty('border_radius', $typeStyle['border_radius_btn_a']);

                $style =
                    $classBox . ' td {
                        text-align: ' . $alignTable . ';
                        font: ' . $fontTable . ';
                        border: none;
                    }
                    '.$classBox . ' tr {
                        transition: all .3s linear;
                        border: ' . $borderLine . ';
                    }
                    '.$classBox . ' table {
                        text-align: ' . $alignTable . ';
                        font: ' . $fontTable . ';
                    }
                    '.$classBox . ' th {
                        background: ' . $bgColorTH.';
                        color: ' . $colorTH.';
                        text-align: ' . $alignTable . ';
                        font: ' . $fontTable . ';
                    }
                    '.$classBox . ' tr:hover {
                        background: ' . $bgColorTDH.';
                        color: ' . $colorTDH.';
                    }
                    
                    '.$classBox . ' .activate-user {
                        background: ' . $bgColorActivate.';
                        color: ' . $colorActivate.';
                    }
                    '.$classBox . ' .not-activate-user {
                        background: ' . $bgColorNotActivate.';
                        color: ' . $colorNotActivate.';
                    }
                    
                    '.$classBox . ' .pagination > li > a,
                    '.$classBox . ' .pagination > .disabled > span
                    {
                        background: ' . $bgColorBtn.';
                        color: ' . $colorBtn.';
                        border: ' . $borderBtn.';
                        border-radius: ' . $borderRadiusBtn.';
                    }
                    '.$classBox . ' .pagination > li > a:hover,
                    '.$classBox . ' .pagination > li > span:hover {
                        background: ' . $bgColorBtnH.';
                        color: ' . $colorBtnH.';
                        border: ' . $borderBtnH.';
                        border-radius: ' . $borderRadiusBtnH.';
                    }
                    '.$classBox . ' .pagination > .active > a,
                    '.$classBox . ' .pagination > .active > a:hover,
                    '.$classBox . ' .pagination > .active > span,
                    '.$classBox . ' .pagination > .active > span:hover,
                    '.$classBox . ' .pagination > li > span
                    {
                        background: ' . $bgColorBtnA.';
                        color: ' . $colorBtnA.';
                        border: ' . $borderBtnA.';
                        border-radius: ' . $borderRadiusBtnA.';
                    }
                    
                    '.$classBox . ' .pagination > .disabled > span,
                    '.$classBox . ' .pagination > .disabled > span:hover
                    {
                        background: ' . $bgColorBtn.'aa;
                        border: ' . $borderBtn.';
                        border-radius: ' . $borderRadiusBtn.';
                        color: ' . $colorBtn.';
                    }
                ';



            } elseif ($widget == 'tarifs') {

                $borderLine = self::getProperty('border', $typeStyle['border_line']);
                $borderLineTree = self::getProperty('border', $typeStyle['border_line_tree']);

                $bgColorTH = $typeStyle['bg_color_th'];
                $colorTH = $typeStyle['color_th'];
                $bgColorTR = $typeStyle['bg_color_tr'];
                $colorTR = $typeStyle['color_tr'];
                $bgColorTRH = $typeStyle['bg_color_tr_h'];
                $colorTRH = $typeStyle['color_tr_h'];

                $colorH3 = $typeStyle['color_h3'];
                $fontH3 = self::getProperty('font', (isset($typeStyle['font_h3']))?$typeStyle['font_h3']:'');
                $alignH3 = $typeStyle['text_align_h3'];

                $colorH3Tree = $typeStyle['color_h3_tree'];
                $fontH3Tree = self::getProperty('font', (isset($typeStyle['font_h3_tree']))?$typeStyle['font_h3_tree']:'');
                $alignH3Tree = $typeStyle['text_align_h3_tree'];

                $alignTable = $typeStyle['text_align'];
                $fontTable = self::getProperty('font', $typeStyle['font']);
                $alignTableTree = (isset($typeStyle['text_align_tree']))?$typeStyle['text_align_tree']:'';
                $fontTableTree = self::getProperty('font', $typeStyle['font_table_tree']);


                $bgColorBtnMore = $typeStyle['bg_color_btn_more'];
                $bgColorBtnHMore = $typeStyle['bg_color_btn_h_more'];
                $colorBtnMore = $typeStyle['color_btn_more'];
                $colorBtnHMore = $typeStyle['color_btn_h_more'];
                $borderBtnMore = self::getProperty('border', $typeStyle['border_btn_more']);
                $borderBtnHMore = self::getProperty('border', $typeStyle['border_btn_h_more']);
                $borderRadiusBtnMore = self::getProperty('border_radius', $typeStyle['border_radius_btn_more']);
                $borderRadiusBtnHMore = self::getProperty('border_radius', $typeStyle['border_radius_btn_h_more']);

                $bgColorBtnTop = $typeStyle['bg_color_btn_top'];
                $bgColorBtnHTop = $typeStyle['bg_color_btn_h_top'];
                $colorBtnTop = $typeStyle['color_btn_top'];
                $colorBtnHTop = $typeStyle['color_btn_h_top'];
                $borderBtnTop = self::getProperty('border', $typeStyle['border_btn_top']);
                $borderBtnHTop = self::getProperty('border', $typeStyle['border_btn_h_top']);
                $borderRadiusBtnTop = self::getProperty('border_radius', $typeStyle['border_radius_btn_top']);
                $borderRadiusBtnHTop = self::getProperty('border_radius', $typeStyle['border_radius_btn_h_top']);

                $bgColorBtn = $typeStyle['bg_color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $bgColorBtnA = $typeStyle['bg_color_btn_a'];
                $colorBtn = $typeStyle['color_btn'];
                $colorBtnH = $typeStyle['color_btn_h'];
                $colorBtnA = $typeStyle['color_btn_a'];
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);
                $borderBtnA = self::getProperty('border', $typeStyle['border_btn_a']);
                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);
                $borderRadiusBtnA = self::getProperty('border_radius', $typeStyle['border_radius_btn_a']);


                $borderImg = self::getProperty('border', (isset($typeStyle['border_img']))?$typeStyle['border_img']:'');
                $borderRadiusImg = self::getProperty('border_radius', (isset($typeStyle['border_radius_img']))?$typeStyle['border_radius_img']:'');
                $fontImg = self::getProperty('font', (isset($typeStyle['font_img']))?$typeStyle['font_img']:'');
                $textAlignImg = (isset($typeStyle['text_align_img']))?$typeStyle['text_align_img']:'';
                $colorImg = (isset($typeStyle['color_img']))?$typeStyle['color_img']:'';

                $borderImgFree = self::getProperty('border', (isset($typeStyle['border_img_free']))?$typeStyle['border_img_free']:'');
                $borderRadiusImgFree = self::getProperty('border_radius', (isset($typeStyle['border_radius_img_free']))?$typeStyle['border_radius_img_free']:'');
                $fontImgFree = self::getProperty('font', (isset($typeStyle['font_img_free']))?$typeStyle['font_img_free']:'');
                $textAlignImgFree = (isset($typeStyle['text_align_img_free']))?$typeStyle['text_align_img_free']:'';
                $colorImgFree = (isset($typeStyle['color_img_free']))?$typeStyle['color_img_free']:'';

                $bgColorFrom = (isset($typeStyle['bg_color_from']))?$typeStyle['bg_color_from']:'';
                $colorFrom = (isset($typeStyle['color_from']))?$typeStyle['color_from']:'';

                $bgColorTo = (isset($typeStyle['bg_color_to']))?$typeStyle['bg_color_to']:'';
                $colorTo = (isset($typeStyle['color_to']))?$typeStyle['color_to']:'';

                $bgColorTDHTree = (isset($typeStyle['bg_color_td_h_tree']))?$typeStyle['bg_color_td_h_tree']:'';
                $colorTDHTree = (isset($typeStyle['color_td_h_tree']))?$typeStyle['color_td_h_tree']:'';
                $bgColorTHTree = (isset($typeStyle['bg_color_td_h_tree']))?$typeStyle['bg_color_th_tree']:'';
                $colorTHTree = (isset($typeStyle['color_td_h_tree']))?$typeStyle['color_th_tree']:'';

                $style =
                    $classBox . ' td,'.$classBox . ' tr,'.$classBox . ' th {
                        text-align: ' . $alignTable . ';
                        vertical-align: middle;
                        font: ' . $fontTable . ';
                        border: none;
                    }
                    '.$classBox . ' .tarif-tree td,'.$classBox . ' .tarif-tree tr,'.$classBox . ' .tarif-tree th {
                        text-align: ' . $alignTableTree . ';
                        vertical-align: middle;
                        font: ' . $fontTableTree . ';
                        border: none;
                    }
                    '.$classBox . ' h3 {
                        color: ' . $colorH3 . ';
                        font: ' . $fontH3 . ';
                        text-align: ' . $alignH3 . ';
                    }
                    '.$classBox . ' .tarif-tree h3 {
                        color: ' . $colorH3Tree . ';
                        font: ' . $fontH3Tree . ';
                        text-align: ' . $alignH3Tree . ';
                    }
                    '.$classBox . ' .tarif-tree tr {
                        border: ' . $borderLineTree . ';
                    }
                    '.$classBox . ' tr {
                        transition: all .3s linear;
                        border: ' . $borderLine . ';
                        background: ' . $bgColorTR.';
                        color: ' . $colorTR.';
                    }
                    '.$classBox . ' tr:hover {
                        background: ' . $bgColorTRH.';
                        color: ' . $colorTRH.';
                    }
                    '.$classBox . ' table {
                        text-align: ' . $alignTable . ';
                        font: ' . $fontTable . ';
                    }
                    '.$classBox . ' th, '.$classBox . ' th:hover {
                        background: ' . $bgColorTH.';
                        color: ' . $colorTH.';
                        text-align: ' . $alignTable . ';
                        font: ' . $fontTable . ';
                    }
                    '.$classBox . ' .tarif-tree th, '.$classBox . ' .tarif-tree th:hover {
                        background: ' . $bgColorTHTree.';
                        color: ' . $colorTHTree.';
                        text-align: ' . $alignTableTree . ';
                        font: ' . $fontTableTree . ';
                    }
                    '.$classBox . ' .img-circle-my {
                        border: ' . $borderImg . ';
                        border-radius: ' . $borderRadiusImg . ';
                    }'.$classBox . ' .img-tree {
                        text-align: ' . $textAlignImg . ';
                        font: ' . $fontImg . ';
                        color: ' . $colorImg .';
                    }
                    '.$classBox . ' .img-circle-my.circle-free {
                        border: ' . $borderImgFree . ';
                        border-radius: ' . $borderRadiusImgFree . ';
                    }
                    '.$classBox . ' .img-tree-free {
                        text-align: ' . $textAlignImgFree . ';
                        font: ' . $fontImgFree . ';
                        color: ' . $colorImgFree .';
                    }
                    '.$classBox . ' .tarif-tree .to-transaction {
                        background: ' . $bgColorTo.';
                        color: ' . $colorTo.';
                    }
                    '.$classBox . ' .tarif-tree .from-transaction {
                        background: ' . $bgColorFrom.';
                        color: ' . $colorFrom.';
                    }
                    '.$classBox . ' .tarif-tree tr:hover {
                        background: ' . $bgColorTDHTree.';
                        color: ' . $colorTDHTree.';
                    }'.
                    $classBox . ' .top-btn .btn-primary {
                        background-color: '.$bgColorBtnTop.';
                        color: '.$colorBtnTop.';
                        border-radius: '.$borderRadiusBtnTop.';
                        border: ' . $borderBtnTop . ';
                    }'.
                    $classBox . ' .top-btn .btn-primary:hover {
                        background-color: '.$bgColorBtnHTop.';
                        color: '.$colorBtnHTop.';
                        border-radius: '.$borderRadiusBtnHTop.';
                        border: ' . $borderBtnHTop . ';
                    }
                    '.
                    $classBox . ' .btn-primary.more {
                        background-color: '.$bgColorBtnMore.';
                        color: '.$colorBtnMore.';
                        border-radius: '.$borderRadiusBtnMore.';
                        border: ' . $borderBtnMore . ';
                    }'.
                    $classBox . ' .btn-primary.more:hover {
                        background-color: '.$bgColorBtnHMore.';
                        color: '.$colorBtnHMore.';
                        border-radius: '.$borderRadiusBtnHMore.';
                        border: ' . $borderBtnHMore . ';
                    }
                    '.$classBox . ' .pagination > li > a,
                    '.$classBox . ' .pagination > .disabled > span
                    {
                        background: ' . $bgColorBtn.';
                        color: ' . $colorBtn.';
                        border: ' . $borderBtn.';
                        border-radius: ' . $borderRadiusBtn.';
                    }
                    '.$classBox . ' .pagination > li > a:hover,
                    '.$classBox . ' .pagination > li > span:hover {
                        background: ' . $bgColorBtnH.';
                        color: ' . $colorBtnH.';
                        border: ' . $borderBtnH.';
                        border-radius: ' . $borderRadiusBtnH.';
                    }
                    '.$classBox . ' .pagination > .active > a,
                    '.$classBox . ' .pagination > .active > a:hover,
                    '.$classBox . ' .pagination > .active > span,
                    '.$classBox . ' .pagination > .active > span:hover,
                    '.$classBox . ' .pagination > li > span
                    {
                        background: ' . $bgColorBtnA.';
                        color: ' . $colorBtnA.';
                        border: ' . $borderBtnA.';
                        border-radius: ' . $borderRadiusBtnA.';
                    }
                    
                    '.$classBox . ' .pagination > .disabled > span,
                    '.$classBox . ' .pagination > .disabled > span:hover
                    {
                        background: ' . $bgColorBtn.'aa;
                        border: ' . $borderBtn.';
                        border-radius: ' . $borderRadiusBtn.';
                        color: ' . $colorBtn.';
                    }
                ';

            } elseif ($widget == 'mydeposits') {

                $borderLine = self::getProperty('border', $typeStyle['border_line']);

                $bgColorTH = $typeStyle['bg_color_th'];
                $colorTH = $typeStyle['color_th'];
                $bgColorTR = $typeStyle['bg_color_tr'];
                $colorTR = $typeStyle['color_tr'];
                $bgColorTRH = $typeStyle['bg_color_tr_h'];
                $colorTRH = $typeStyle['color_tr_h'];

                $colorH3 = $typeStyle['color_h3'];
                $fontH3 = self::getProperty('font', (isset($typeStyle['font_h3']))?$typeStyle['font_h3']:'');
                $alignH3 = $typeStyle['text_align_h3'];

                $alignTable = $typeStyle['text_align'];
                $fontTable = self::getProperty('font', $typeStyle['font']);
//                $alignTableTree = (isset($typeStyle['text_align_tree']))?$typeStyle['text_align_tree']:'';
//                $fontTableTree = self::getProperty('font', $typeStyle['font_table_tree']);


                $bgColorBtnMore = $typeStyle['bg_color_btn_more'];
                $bgColorBtnHMore = $typeStyle['bg_color_btn_h_more'];
                $colorBtnMore = $typeStyle['color_btn_more'];
                $colorBtnHMore = $typeStyle['color_btn_h_more'];
                $borderBtnMore = self::getProperty('border', $typeStyle['border_btn_more']);
                $borderBtnHMore = self::getProperty('border', $typeStyle['border_btn_h_more']);
                $borderRadiusBtnMore = self::getProperty('border_radius', $typeStyle['border_radius_btn_more']);
                $borderRadiusBtnHMore = self::getProperty('border_radius', $typeStyle['border_radius_btn_h_more']);

                $bgColorBtnTop = $typeStyle['bg_color_btn_top'];
                $bgColorBtnHTop = $typeStyle['bg_color_btn_h_top'];
                $colorBtnTop = $typeStyle['color_btn_top'];
                $colorBtnHTop = $typeStyle['color_btn_h_top'];
                $borderBtnTop = self::getProperty('border', $typeStyle['border_btn_top']);
                $borderBtnHTop = self::getProperty('border', $typeStyle['border_btn_h_top']);
                $borderRadiusBtnTop = self::getProperty('border_radius', $typeStyle['border_radius_btn_top']);
                $borderRadiusBtnHTop = self::getProperty('border_radius', $typeStyle['border_radius_btn_h_top']);

                $bgColorBtn = $typeStyle['bg_color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $bgColorBtnA = $typeStyle['bg_color_btn_a'];
                $colorBtn = $typeStyle['color_btn'];
                $colorBtnH = $typeStyle['color_btn_h'];
                $colorBtnA = $typeStyle['color_btn_a'];
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);
                $borderBtnA = self::getProperty('border', $typeStyle['border_btn_a']);
                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);
                $borderRadiusBtnA = self::getProperty('border_radius', $typeStyle['border_radius_btn_a']);


                $borderImg = self::getProperty('border', (isset($typeStyle['border_img']))?$typeStyle['border_img']:'');
                $borderRadiusImg = self::getProperty('border_radius', (isset($typeStyle['border_radius_img']))?$typeStyle['border_radius_img']:'');
                $fontImg = self::getProperty('font', (isset($typeStyle['font_img']))?$typeStyle['font_img']:'');
                $textAlignImg = (isset($typeStyle['text_align_img']))?$typeStyle['text_align_img']:'';
                $colorImg = (isset($typeStyle['color_img']))?$typeStyle['color_img']:'';

                $borderImgFree = self::getProperty('border', (isset($typeStyle['border_img_free']))?$typeStyle['border_img_free']:'');
                $borderRadiusImgFree = self::getProperty('border_radius', (isset($typeStyle['border_radius_img_free']))?$typeStyle['border_radius_img_free']:'');
                $fontImgFree = self::getProperty('font', (isset($typeStyle['font_img_free']))?$typeStyle['font_img_free']:'');
                $textAlignImgFree = (isset($typeStyle['text_align_img_free']))?$typeStyle['text_align_img_free']:'';
                $colorImgFree = (isset($typeStyle['color_img_free']))?$typeStyle['color_img_free']:'';

                $bgColorFrom = (isset($typeStyle['bg_color_from']))?$typeStyle['bg_color_from']:'';
                $colorFrom = (isset($typeStyle['color_from']))?$typeStyle['color_from']:'';

                $bgColorTo = (isset($typeStyle['bg_color_to']))?$typeStyle['bg_color_to']:'';
                $colorTo = (isset($typeStyle['color_to']))?$typeStyle['color_to']:'';

                $bgColorTDHTree = (isset($typeStyle['bg_color_td_h_tree']))?$typeStyle['bg_color_td_h_tree']:'';
                $colorTDHTree = (isset($typeStyle['color_td_h_tree']))?$typeStyle['color_td_h_tree']:'';

                $style =
                    $classBox . ' td,'.$classBox . ' tr,'.$classBox . ' th {
                        text-align: ' . $alignTable . ';
                        vertical-align: middle;
                        font: ' . $fontTable . ';
                        border: none;
                    }
                    '.$classBox . ' h3 {
                        color: ' . $colorH3 . ';
                        font: ' . $fontH3 . ';
                        text-align: ' . $alignH3 . ';
                    }
                    '.$classBox . ' tr {
                        transition: all .3s linear;
                        border: ' . $borderLine . ';
                        background: ' . $bgColorTR.';
                        color: ' . $colorTR.';
                    }
                    '.$classBox . ' tr:hover {
                        background: ' . $bgColorTRH.';
                        color: ' . $colorTRH.';
                    }
                    '.$classBox . ' table {
                        text-align: ' . $alignTable . ';
                        font: ' . $fontTable . ';
                    }
                    '.$classBox . ' th, '.$classBox . ' th:hover {
                        background: ' . $bgColorTH.';
                        color: ' . $colorTH.';
                        text-align: ' . $alignTable . ';
                        font: ' . $fontTable . ';
                    }
                    '.$classBox . ' .img-circle-my {
                        border: ' . $borderImg . ';
                        border-radius: ' . $borderRadiusImg . ';
                    }'.$classBox . ' .img-tree {
                        text-align: ' . $textAlignImg . ';
                        font: ' . $fontImg . ';
                        color: ' . $colorImg .';
                    }
                    '.$classBox . ' .img-circle-my.circle-free {
                        border: ' . $borderImgFree . ';
                        border-radius: ' . $borderRadiusImgFree . ';
                    }
                    '.$classBox . ' .img-tree-free {
                        text-align: ' . $textAlignImgFree . ';
                        font: ' . $fontImgFree . ';
                        color: ' . $colorImgFree .';
                    }
                    '.$classBox . ' .tarif-tree .to-transaction {
                        background: ' . $bgColorTo.';
                        color: ' . $colorTo.';
                    }
                    '.$classBox . ' .tarif-tree .from-transaction {
                        background: ' . $bgColorFrom.';
                        color: ' . $colorFrom.';
                    }
                    '.$classBox . ' .tarif-tree tr:hover {
                        background: ' . $bgColorTDHTree.';
                        color: ' . $colorTDHTree.';
                    }'.
                    $classBox . ' .top-btn .btn-primary {
                        background-color: '.$bgColorBtnTop.';
                        color: '.$colorBtnTop.';
                        border-radius: '.$borderRadiusBtnTop.';
                        border: ' . $borderBtnTop . ';
                    }'.
                    $classBox . ' .top-btn .btn-primary:hover {
                        background-color: '.$bgColorBtnHTop.';
                        color: '.$colorBtnHTop.';
                        border-radius: '.$borderRadiusBtnHTop.';
                        border: ' . $borderBtnHTop . ';
                    }
                    '.
                    $classBox . ' .box-body .btn-primary.pick-up {
                        background-color: '.$bgColorBtnMore.';
                        color: '.$colorBtnMore.';
                        border-radius: '.$borderRadiusBtnMore.';
                        border: ' . $borderBtnMore . ';
                    }'.
                    $classBox . ' .box-body .btn-primary.pick-up:hover {
                        background-color: '.$bgColorBtnHMore.';
                        color: '.$colorBtnHMore.';
                        border-radius: '.$borderRadiusBtnHMore.';
                        border: ' . $borderBtnHMore . ';
                    }
                    '.$classBox . ' .pagination > li > a,
                    '.$classBox . ' .pagination > .disabled > span
                    {
                        background: ' . $bgColorBtn.';
                        color: ' . $colorBtn.';
                        border: ' . $borderBtn.';
                        border-radius: ' . $borderRadiusBtn.';
                    }
                    '.$classBox . ' .pagination > li > a:hover,
                    '.$classBox . ' .pagination > li > span:hover {
                        background: ' . $bgColorBtnH.';
                        color: ' . $colorBtnH.';
                        border: ' . $borderBtnH.';
                        border-radius: ' . $borderRadiusBtnH.';
                    }
                    '.$classBox . ' .pagination > .active > a,
                    '.$classBox . ' .pagination > .active > a:hover,
                    '.$classBox . ' .pagination > .active > span,
                    '.$classBox . ' .pagination > .active > span:hover,
                    '.$classBox . ' .pagination > li > span
                    {
                        background: ' . $bgColorBtnA.';
                        color: ' . $colorBtnA.';
                        border: ' . $borderBtnA.';
                        border-radius: ' . $borderRadiusBtnA.';
                    }
                    
                    '.$classBox . ' .pagination > .disabled > span,
                    '.$classBox . ' .pagination > .disabled > span:hover
                    {
                        background: ' . $bgColorBtn.'aa;
                        border: ' . $borderBtn.';
                        border-radius: ' . $borderRadiusBtn.';
                        color: ' . $colorBtn.';
                    }
                ';

            } elseif ($widget == 'store') {

                $borderLine = self::getProperty('border', $typeStyle['border_line']);

                $bgColorTH = $typeStyle['bg_color_th'];
                $colorTH = $typeStyle['color_th'];
                $bgColorTR = $typeStyle['bg_color_tr'];
                $colorTR = $typeStyle['color_tr'];
                $bgColorTRH = $typeStyle['bg_color_tr_h'];
                $colorTRH = $typeStyle['color_tr_h'];

                $colorH3 = $typeStyle['color_h3'];
                $fontH3 = self::getProperty('font', (isset($typeStyle['font_h3']))?$typeStyle['font_h3']:'');
                $alignH3 = $typeStyle['text_align_h3'];

                $alignTable = $typeStyle['text_align'];
                $fontTable = self::getProperty('font', $typeStyle['font']);


                $bgColorBtnMore = $typeStyle['bg_color_btn_more'];
                $bgColorBtnHMore = $typeStyle['bg_color_btn_h_more'];
                $colorBtnMore = $typeStyle['color_btn_more'];
                $colorBtnHMore = $typeStyle['color_btn_h_more'];
                $borderBtnMore = self::getProperty('border', $typeStyle['border_btn_more']);
                $borderBtnHMore = self::getProperty('border', $typeStyle['border_btn_h_more']);
                $borderRadiusBtnMore = self::getProperty('border_radius', $typeStyle['border_radius_btn_more']);
                $borderRadiusBtnHMore = self::getProperty('border_radius', $typeStyle['border_radius_btn_h_more']);

                $bgColorBtnTop = $typeStyle['bg_color_btn_top'];
                $bgColorBtnHTop = $typeStyle['bg_color_btn_h_top'];
                $colorBtnTop = $typeStyle['color_btn_top'];
                $colorBtnHTop = $typeStyle['color_btn_h_top'];
                $borderBtnTop = self::getProperty('border', $typeStyle['border_btn_top']);
                $borderBtnHTop = self::getProperty('border', $typeStyle['border_btn_h_top']);
                $borderRadiusBtnTop = self::getProperty('border_radius', $typeStyle['border_radius_btn_top']);
                $borderRadiusBtnHTop = self::getProperty('border_radius', $typeStyle['border_radius_btn_h_top']);

                $bgColorFrom = (isset($typeStyle['bg_color_from']))?$typeStyle['bg_color_from']:'';
                $colorFrom = (isset($typeStyle['color_from']))?$typeStyle['color_from']:'';

                $bgColorTo = (isset($typeStyle['bg_color_to']))?$typeStyle['bg_color_to']:'';
                $colorTo = (isset($typeStyle['color_to']))?$typeStyle['color_to']:'';

                $bgColorTDHTree = (isset($typeStyle['bg_color_td_h_tree']))?$typeStyle['bg_color_td_h_tree']:'';
                $colorTDHTree = (isset($typeStyle['color_td_h_tree']))?$typeStyle['color_td_h_tree']:'';

                $style =
                    $classBox . ' td,'.$classBox . ' tr,'.$classBox . ' th {
                        text-align: ' . $alignTable . ';
                        vertical-align: middle;
                        font: ' . $fontTable . ';
                        border: none;
                    }
                    '.$classBox . ' h3 {
                        color: ' . $colorH3 . ';
                        font: ' . $fontH3 . ';
                        text-align: ' . $alignH3 . ';
                    }
                    '.$classBox . ' tr {
                        transition: all .3s linear;
                        border: ' . $borderLine . ';
                        background: ' . $bgColorTR.';
                        color: ' . $colorTR.';
                    }
                    '.$classBox . ' tr:hover {
                        background: ' . $bgColorTRH.';
                        color: ' . $colorTRH.';
                    }
                    '.$classBox . ' table {
                        text-align: ' . $alignTable . ';
                        font: ' . $fontTable . ';
                    }
                    '.$classBox . ' th, '.$classBox . ' th:hover {
                        background: ' . $bgColorTH.';
                        color: ' . $colorTH.';
                        text-align: ' . $alignTable . ';
                        font: ' . $fontTable . ';
                    }
                    '.$classBox . ' .tarif-tree .to-transaction {
                        background: ' . $bgColorTo.';
                        color: ' . $colorTo.';
                    }
                    '.$classBox . ' .tarif-tree .from-transaction {
                        background: ' . $bgColorFrom.';
                        color: ' . $colorFrom.';
                    }
                    '.$classBox . ' .tarif-tree tr:hover {
                        background: ' . $bgColorTDHTree.';
                        color: ' . $colorTDHTree.';
                    }'.
                    $classBox . ' .top-btn .btn-primary {
                        background-color: '.$bgColorBtnTop.';
                        color: '.$colorBtnTop.';
                        border-radius: '.$borderRadiusBtnTop.';
                        border: ' . $borderBtnTop . ';
                    }'.
                    $classBox . ' .top-btn .btn-primary:hover {
                        background-color: '.$bgColorBtnHTop.';
                        color: '.$colorBtnHTop.';
                        border-radius: '.$borderRadiusBtnHTop.';
                        border: ' . $borderBtnHTop . ';
                    }'.
                    $classBox . ' .box-body .btn-primary.pick-up {
                        background-color: '.$bgColorBtnMore.';
                        color: '.$colorBtnMore.';
                        border-radius: '.$borderRadiusBtnMore.';
                        border: ' . $borderBtnMore . ';
                    }'.
                    $classBox . ' .box-body .btn-primary.pick-up:hover {
                        background-color: '.$bgColorBtnHMore.';
                        color: '.$colorBtnHMore.';
                        border-radius: '.$borderRadiusBtnHMore.';
                        border: ' . $borderBtnHMore . ';
                    }
                ';

            } elseif ($widget == 'faq') {

                $bgColorHead = $typeStyle['bg_color_head'];
                $colorHead = $typeStyle['color_head'];
                $alignHead = $typeStyle['text_align_head'];
                $borderHead = self::getProperty('border', $typeStyle['border_head']);

                $bgColorText = $typeStyle['bg_color_text'];
                $colorText = $typeStyle['color_text'];
                $alignText = $typeStyle['text_align_text'];
                $borderText = self::getProperty('border', $typeStyle['border_text']);

                $borderRadius = self::getProperty('border_radius', $typeStyle['border_radius_body']);

                $style =
                    $classBox . ' .box .box .box-header {
                        border-top: ' . $borderHead . ';
                        background: ' . $bgColorHead.';
                        text-align: ' . $alignHead . ';
                    }
                    '.$classBox.' .box .box {
                        border: none;
                    }
                    '.$classBox . ' .box .box .box-header a {
                        color: ' . $colorHead.';
                    }
                    '.$classBox . ' .box .box .box-body {
                        border-top: ' . $borderText . ';
                        background: ' . $bgColorText.';
                        color: ' . $colorText.';
                        text-align: ' . $alignText . ';
                        border-radius: ' . $borderRadius . ';
                    }
                ';

            } elseif ($widget == 'promo') {

                $bgColorHead = $typeStyle['bg_color_head'];
                $colorHead = $typeStyle['color_head'];
                $fontHead = self::getProperty('font', (isset($typeStyle['font_head']))?$typeStyle['font_head']:'');
                $alignHead = $typeStyle['text_align_head'];

                $bgColorRef = $typeStyle['bg_color_reflink'] ?? '';
                $colorRef = $typeStyle['color_reflink'] ?? '';
                $fontRef = self::getProperty('font', (isset($typeStyle['font_reflink']))?$typeStyle['font_reflink']:'');
                $alignRef = $typeStyle['text_align_reflink'] ?? '';

                $labelTextAlign = $typeStyle['label_text_align'];
                $labelColor = $typeStyle['label_color_text'];

                $bgColorInput = $typeStyle['bg_color_input'];
                $colorInput = $typeStyle['color_input'];

                $borderInput = self::getProperty('border', $typeStyle['border_input']);
                $borderRadiusInput = self::getProperty('border_radius', $typeStyle['border_radius_input']);

                $widthBtn = $typeStyle['width_btn'];

                $bgColorBtn = $typeStyle['bg_color_btn'];
                $colorBtn = $typeStyle['color_btn'];
                $bgColorBtnH = $typeStyle['bg_color_btn_h'];
                $colorBtnH = $typeStyle['color_btn_h'];

                $borderRadiusPromo = self::getProperty('border_radius', $typeStyle['border_radius_promo'] ?? '');
                $borderPromo = self::getProperty('border', $typeStyle['border_promo'] ?? '');
                $bgColorPromo = $typeStyle['bg_color_promo'] ?? '';

                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);
                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);

                $paddingRef = self::getProperty('padding', $typeStyle['padding_reflink'] ?? '');
                $borderRef = self::getProperty('border', $typeStyle['border_reflink'] ?? '');
                $borderRadiusRef = self::getProperty('border_radius', $typeStyle['border_radius_reflink'] ?? '');

                $style =
                    $classBox . ' textarea {
                        background: '.$bgColorInput.';
                        color: '.$colorInput.';
                        border: ' . $borderInput . ';
                        border-radius: '.$borderRadiusInput.';
                    }'.
                    $classBox . ' .refresh {
                        padding: 10px;
                    }
                    '.$classBox . ' .element-promo {
                        border: ' . $borderPromo . ';
                        border-radius: '.$borderRadiusPromo.';
                        background: '.$bgColorPromo.';
                    }
                    '.$classBox . ' .reflink {
                        border: ' . $borderRef . ';
                        border-radius: '.$borderRadiusRef.';
                        padding: '.$paddingRef.';
                        font: '.$fontRef.';
                        text-align: '.$alignRef.';
                        color: '.$colorRef.';
                        background: '.$bgColorRef.';
                    }
                    '.$classBox . ' .promo-head h3 {
                        font: '.$fontHead.';
                    }
                    '.$classBox . ' .promo-head {
                        text-align: '.$alignHead.';
                        color: '.$colorHead.';
                        background: '.$bgColorHead.';
                    }
                    '.$classBox . ' img {
                        display: block;
                        margin: auto;
                        width: '.$widthBtn.'px;
                        background-color: '.$bgColorBtn.';
                        color: '.$colorBtn.';
                        border-radius: '.$borderRadiusBtn.';
                        border: ' . $borderBtn . ';
                    }
                ';

            } elseif ($widget == 'horizontalmenu') {

                $mainStyle = self::serialize_corrector(SiteElements::where('type', 'style')->first()['option_value']);
                $float = $typeStyle['float'];

                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);
                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);

                $style =
                    $classBox . ' {
                        display: inline-block
                    }'.
                    $classBox . ' .navbar-custom-menu {
                        float: ' . $float . ';
                    }'.$classBox . ' .navbar-custom-menu a {
                        color: ' . $typeStyle['color_btn'] . ';
                        background-color: ' . $typeStyle['bg_color_btn'] . ';
                        border: '.$borderBtn.';
                        border-radius: '.$borderRadiusBtn.';
                    }
                    '.$classBox . ' .navbar-custom-menu a:hover {
                        color: ' . $typeStyle['color_btn_h'] . ';
                        background-color: ' . $typeStyle['bg_color_btn_h'] . ';
                        border: '.$borderBtnH.';
                        border-radius: '.$borderRadiusBtnH.';
                    }'.$classBox . ' .navbar-custom-menu a i {
                        color: ' . $typeStyle['color_icon_btn'] . ';
                    }'.$classBox . ' .navbar-custom-menu a:hover i {
                        color: ' . $typeStyle['color_icon_btn_h'] . ';
                    }';

            } elseif ($widget == 'activate') {

                $bgColorBtn = (isset($typeStyle['bg_color_btn_tarif']))?$typeStyle['bg_color_btn_tarif']:'';
                $colorBtn = (isset($typeStyle['color_btn_tarif']))?$typeStyle['color_btn_tarif']:'';
                $bgColorBtnH = (isset($typeStyle['bg_color_btn_tarif']))?$typeStyle['bg_color_btn_h_tarif']:'';
                $colorBtnH = (isset($typeStyle['color_btn_tarif']))?$typeStyle['color_btn_h_tarif']:'';
                $font = self::getProperty('font', (isset($typeStyle['font_tarif']))?$typeStyle['font_tarif']:'');
                $alignTarif = $typeStyle['text_align_tarif'];

                $borderRadiusBtn = self::getProperty('border_radius', (isset($typeStyle['border_radius_btn_tarif']))?$typeStyle['border_radius_btn_tarif']:'');
                $borderRadiusBtnH = self::getProperty('border_radius', (isset($typeStyle['border_radius_btn_h_tarif']))?$typeStyle['border_radius_btn_h_tarif']:'');
                $borderBtn = self::getProperty('border', (isset($typeStyle['border_btn_tarif']))?$typeStyle['border_btn_tarif']:'');
                $borderBtnH = self::getProperty('border', (isset($typeStyle['border_btn_h_tarif']))?$typeStyle['border_btn_h_tarif']:'');

                $padding = self::getProperty('padding', (isset($typeStyle['padding_tarif']))?$typeStyle['padding_tarif']:'');

                $style =
                    $classBox . ' {
                    }
                    '.$classBox . ' .btn-activate-tarif {
                        width: 100%;
                        text-align: ' . $alignTarif . ';
                        color: ' . $colorBtn . ';
                        background-color: ' . $bgColorBtn . ';
                        border: '.$borderBtn.';
                        border-radius: '.$borderRadiusBtn.';
                        padding: '.$padding.';
                        font: '.$font.';
                        white-space: normal;
                    }
                    '.$classBox . ' .btn-activate-tarif:hover {
                        color: ' . $colorBtnH . ';
                        background-color: ' . $bgColorBtnH . ';
                        border: '.$borderBtnH.';
                        border-radius: '.$borderRadiusBtnH.';
                    }';

            } elseif ($widget == 'verticalmenu') {

                $mainStyle = self::serialize_corrector(SiteElements::where('type', 'style')->first()['option_value']);

                $borderBtn = self::getProperty('border', $typeStyle['border_btn']);
                $borderBtnH = self::getProperty('border', $typeStyle['border_btn_h']);
                $borderRadiusBtn = self::getProperty('border_radius', $typeStyle['border_radius_btn']);
                $borderRadiusBtnH = self::getProperty('border_radius', $typeStyle['border_radius_btn_h']);

                $style =
                    $classBox . ' .sidebar-menu a {
                        color: ' . $typeStyle['color_btn'] . ';
                        background-color: ' . $typeStyle['bg_color_btn'] . ';
                        border: '.$borderBtn.';
                        border-radius: '.$borderRadiusBtn.';
                    }
                    '.$classBox . ' {
                        width: 100%;
                    }
                    '.$classBox . ' .sidebar-menu a:hover {
                        color: ' . $typeStyle['color_btn_h'] . ';
                        background-color: ' . $typeStyle['bg_color_btn_h'] . ';
                        border: '.$borderBtnH.';
                        border-radius: '.$borderRadiusBtnH.';
                    }'.$classBox . ' .sidebar-menu a i {
                        color: ' . $typeStyle['color_icon_btn'] . ';
                    }'.$classBox . ' .sidebar-menu a:hover i {
                        color: ' . $typeStyle['color_icon_btn_h'] . ';
                    }';
            }

            $result = $result . $style;
        }


        return $result;
    }

    public static function ifIsset($array) {
        $arValue = [
            'border_color', 'border_style', 'border_width', 'border', 'border_radius', 'border_img', 'border_line', 'border_head', 'border_btn', 'border_btn_h', 'border_input',
            'border_color_img', 'border_style_img', 'border_width_img', 'label_sum',
            'border_color_line', 'border_style_line', 'border_width_line',
            'border_color_input', 'border_style_input', 'border_width_input',
            'border_color_btn', 'border_style_btn', 'border_width_btn',
            'border_color_btn_h', 'border_style_btn_h', 'border_width_btn_h',
            'border_color_head', 'border_style_head', 'border_width_head',
            'bg_color_from', 'color_from', 'bg_color_to', 'color_to', 'bg_color_td_h', 'color_td_h',
            'border_radius_tl', 'border_radius_tr', 'border_radius_br', 'border_radius_bl',
            'border_radius_timeline', 'border_radius_timeline_icon', 'border_radius_btn', 'border_radius_btn_h', 'border_radius_btn_a', 'border_radius_input',
            'border_radius_tl_btn', 'border_radius_tr_btn', 'border_radius_br_btn', 'border_radius_bl_btn',
            'border_radius_tl_btn_h', 'border_radius_tr_btn_h', 'border_radius_br_btn_h', 'border_radius_bl_btn_h',
            'border_radius_tl_input', 'border_radius_tr_input', 'border_radius_br_input', 'border_radius_bl_input',
            'border_radius_tl_timeline', 'border_radius_tr_timeline', 'border_radius_br_timeline', 'border_radius_bl_timeline',
            'border_radius_tl_timeline_icon', 'border_radius_tr_timeline_icon', 'border_radius_br_timeline_icon', 'border_radius_bl_timeline_icon',
            'bg', 'bg_head', 'bg_color_elements', 'bg_color_input', 'bg_color_btn', 'bg_color_btn_h', 'bg_color_head', 'bg_color_footer',
            'color_head', 'color_tl', 'color_tr', 'color_login', 'color_input', 'color_btn', 'color_btn_h', 'color_date',
            'color_icon_btn', 'color_icon_btn_h', 'float', 'margin', 'top', 'bottom', 'link', 'text', 'label_sum', 'padding', 'text_align_label',
            'bg_color_btn_a', 'color_btn_a', 'border_btn_a', 'font_table', 'bg_color_activate', 'bg_color_not_activate', 'color_activate', 'color_not_activate',

            'text_align_head', 'label_text_align', 'label_color_text', 'text_align_footer', 'last_referrals', 'random_referrals',
            'bg_color_btn_more', 'bg_color_btn_h_more', 'color_btn_more', 'color_btn_h_more', 'border_btn_more', 'border_btn_h_more', 'border_radius_btn_more', 'border_radius_btn_h_more',
            'bg_color_btn_top', 'bg_color_btn_h_top', 'color_btn_top', 'color_btn_h_top', 'border_btn_top', 'border_btn_h_top', 'border_radius_btn_top', 'border_radius_btn_h_top',
            'bg_color_th', 'color_th', 'color_h3', 'text_align_table',
            'width_img', 'width_btn', 'text_align_text', 'color_text', 'text_align_h3', 'font_h3',
        ];

        foreach ($arValue as $k => $v) {

            $array[$v] = (isset($array[$v]))?$array[$v]:'';
            if (is_array($array[$v])) {
                $array[$v] = self::ifIsset($array[$v]);
            }

        }

        return $array;
    }

    public static function getProperty($type = 'border', $data = []) {

        $result = '';
        if ($type == 'border') {
            $width = (isset($data['border_width']))?$data['border_width']:'';
            $color = (isset($data['border_color']))?$data['border_color']:'';
            $style = (isset($data['border_style']))?$data['border_style']:'';
            $result = $width.'px '.$color.' '.$style;

        } elseif($type == 'border_radius') {

            $tl = (isset($data['tl']))?$data['tl']:'';
            $tr = (isset($data['tr']))?$data['tr']:'';
            $br = (isset($data['br']))?$data['br']:'';
            $bl = (isset($data['bl']))?$data['bl']:'';

            $result = $tl.'px '.$tr.'px '.$br.'px '.$bl.'px';

        } elseif($type == 'font') {
            $weight = (isset($data['weight']))?($data['weight'] == 1)?'bold':'':'';
            $style = (isset($data['style']))?($data['style'] == 1)?'italic':'':'';
            $size = (isset($data['size']))?$data['size']:'';
            $family = (isset($data['family']))?$data['family']:'Source Sans Pro';

            $result = $weight.' '.$style.' '.$size.'px "'.$family.'"';

        } elseif($type == 'transparent') {

            $transparent = $data['transparent'];

            if ($transparent != '')
                if ($transparent == 0) {
                    $transparent = '00';
                } else {
                    $transparent = dechex($transparent * 255);
                }
            else
                $transparent = 'ff';

            $result = $transparent;

        } elseif($type == 'margin' or $type == 'padding') {

            $t = (isset($data['t']))?$data['t']:'0';
            $b = (isset($data['b']))?$data['b']:'0';
            $l = (isset($data['l']))?$data['l']:'0';
            $r = (isset($data['r']))?$data['r']:'0';

            $result = $t.'px '.$r.'px '.$b.'px '.$l.'px';
        }

        return $result;

    }

    public static function getPages($id = 0) {

        if ($id == 0) {
            $template = Helpers::siteTemplate();
        } else {
            $template = $id;
        }

        $pages = [
            ['link' => 'all', 'name' => 'На всех страницах'],
            ['link' => 'profile', 'name' => 'Профиль'],
        ];
        $pagesSite = Pages::where('template', $template)->get();
        foreach ($pagesSite as $key => $pageSite) {
            array_push($pages, ['link' => $pageSite['name'], 'name' => $pageSite['name_ru']]);
        }

        return $pages;
    }

    public static function widgetName($type) {
        $arr = [
            "sidebar" => "Меню",
            "mainbox" => "Обычный блок",
            "smallbox" => "Маленький блок",
            "profilebox" => "Аватар с информацией",
            "transaction" => "Блок с операциями",
            "balance" => "Форма пополнения",
            "referrals" => "Список рефералов",
            "news" => "Новости",
            "faq" => "Частозадаваемые вопросы",
            "promo" => "Промоматериалы",
            "profileedit" => "Редактирование профиля",
            "cashout" => "Форма вывода",
            "activate" => "Активация тарифов",
            "tarifs" => "Мои тарифы",
            "verticalmenu" => "Вертикальное меню",
            "horizontalmenu" => "Горизонтальное меню",
            "content" => "Блок с контентом",
            "mydeposits" => "Мои вклады",
            "store" => "Магазин вкладов",
            "bonusclick" => "Бонус за клик",
//            "grid" => "Область",
        ];



        return @$arr[$type];
    }

    public static function siteTemplate() {
        $template = Templates::where('status', 1)->first()['id'];

        return $template;
    }

    public static function priorityArray($num) {
        $num = (string)$num;
        $int = preg_split('//', $num);
        array_pop($int);
        array_shift($int);

        foreach ($int as $key => $value) {
            if ($value == 1) {
                $int[$key] = 'cashout';
            } elseif ($value == 2) {
                $int[$key] = 'level_up';
            } elseif ($value == 3) {
                $int[$key] = 'reinvest';
            } elseif ($value == 4) {
                $int[$key] = 'reinvest_first';
            }
        }

        return $int;
    }

    public static function pathAvatar($id = 0) {
        if ($id == 0) {
            $id = Auth::id();
        }

        $user = Users::where('id', $id)->first();
        $avatar = $user['avatar'];

        if ($avatar == 'resource/avatars/0.jpg') {
            $siteInfo = SiteInfo::find(1);
            $result = '/iadminbest/storage/app/'.$siteInfo['no_image'];
        } else {
            $result = '/iadminbest/storage/app/'.$avatar;
        }

        return $result;
    }

    public static function getTypeOrder($type, $get) {
        return TypesOrder::where('id', $type)->first()[$get];
    }

    public static function siteType() {

        $siteId = Helpers::getSiteId();
        return CoreSiteList::find($siteId)['type'];

    }

    public static function checkCurrency() {
        $currency = Currency::first();

        if ($currency->status == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static function adsense($type = 'sidebar') {

        $domain = new Domain();

        $exeptionCount = AdsExeption::where('subdomain', $domain->getDB())->count();
        if ($exeptionCount) {
            return false;
        }

        $result = '';
        $marks = MarksInside::where('status', '!=', 0)->get();
        $show = false;

        foreach ($marks as $mark) {
            if ($mark->level1 < 50) {
                $show = true;
            }
        }

        $sum = Payments::sum('amount');
        if ($sum > 75000) {
            $show = false;
        }


        $adsAll = AdsCore::where('status', 1)->where('type', $type)->get();
        $adsCount = AdsCore::where('status', 1)->where('type', $type)->count() - 1;

        $rand = rand(0, $adsCount);

        AdsCore::where('id', $adsAll[$rand]->id)->increment('view');

        if ($show) {
            $result = '<a href="'.$adsAll[$rand]->link.'" target="_blank">
                        <img src="'.$adsAll[$rand]->image.'" width="100%" alt="">
                    </a>';
        }

//        $result = '<script type=\'text/javascript\'>(function() {
//                  /* Optional settings (these lines can be removed): */
//                   subID = "";  // - local banner key;
//                   injectTo = "";  // - #id of html element (ex., "top-banner").
//                  /* End settings block */
//
//                if(injectTo=="")injectTo="admitad_shuffle"+subID+Math.round(Math.random()*100000000);
//                if(subID==\'\')subid_block=\'\'; else subid_block=\'subid/\'+subID+\'/\';
//                document.write(\'<div id="\'+injectTo+\'"></div>\');
//                var s = document.createElement(\'script\');
//                s.type = \'text/javascript\'; s.async = true;
//                s.src = \'https://ad.admitad.com/shuffle/bfcfda0a3e/\'+subid_block+\'?inject_to=\'+injectTo;
//                var x = document.getElementsByTagName(\'script\')[0];
//                x.parentNode.insertBefore(s, x);
//                })();</script>
//                ';

        return $result;

    }

    public static function siteCurrency($type = 'number') {

        $domain = new Domain();
        $currency = CoreSiteList::where('subdomain', $domain->getDB())->first()['currency'];

        if ($type == 'number') {

            return $currency;

        } elseif ($type == 'code') {

            switch ($currency) {
                case 1:
                    $currencyCode = 'RUB';
                    break;
                case 2:
                    $currencyCode = 'USD';
                    break;
                case 3:
                    $currencyCode = 'EUR';
                    break;
                case 4:
                    $currencyCode = 'BTC';
                    break;
                case 5:
                    $currencyCode = 'ETH';
                    break;
                case 6:
                    $currencyCode = 'LTC';
                    break;
                case 7:
                    $currencyCode = 'XRP';
                    break;
                case 8:
                    $currencyCode = 'LTCT';
                    break;
                default:
                    $currencyCode = 'RUB';
            }

            return $currencyCode;

        } elseif ($type == 'type') {

            if ($currency < 4) {
                return 'fiat';
            } else {
                return 'crypto';
            }

        } elseif ($type == 'icon') {

            switch ($currency) {
                case 1:
                    $cur_ico = 'fa fa-rub';
                    break;
                case 2:
                    $cur_ico = 'fa fa-usd';
                    break;
                case 3:
                    $cur_ico = 'fa fa-eur';
                    break;
                case 4:
                    $cur_ico = 'fa fa-bitcoin';
                    break;
                case 5:
                    $cur_ico = 'fab fa-ethereum';
                    break;
                case 6:
                    $cur_ico = 'cc LTC-alt';
                    break;
                case 7:
                    $cur_ico = 'cc XRP-alt';
                    break;
                case 8:
                    $cur_ico = 'cc LTC-alt';
                    break;
                default:
                    $cur_ico = 'fa fa-rub';
            }
            return '<i class="'.$cur_ico.'" aria-hidden="true"></i>';

        } elseif ($type == 'round') {
            if ($currency < 4) {
                return 2;
            } else {
                return 8;
            }
        }

    }

    public static function access($page, $type = 'check') {

        $pageInfo = Pages::where('name', $page)->first();
        $message = '';

        switch ($pageInfo->access) {
            case 1:
                $result = true;
                break;
            case 2:
                $count = Users::where('refer', Auth::id())->count();

                if ($count >= $pageInfo->condition) {
                    $result = true;
                    break;
                }
                $message = __('main.access_refer', ['condition' => $pageInfo->condition, 'count' => $count]);
                $result = false;
                break;
            case 3:
                $count = UsersData::where('id_user', Auth::id())->first()->link_visit;

                if ($count >= $pageInfo->condition) {
                    $result = true;
                    break;
                }

                $message = __('main.access_link_visit', ['condition' => $pageInfo->condition, 'count' => $count]);
                $result = false;
                break;
            case 4:
                $count = OrdersMarkInside::where('id_user', Auth::id())->count();

                if ($count) {
                    $result = true;
                    break;
                }

                $message = __('main.access_activate', ['condition' => $pageInfo->condition, 'count' => $count]);
                $result = false;
                break;
            case 5:
                $count = OrdersMarkInside::where('id_user', Auth::id())->where('mark', $pageInfo->condition)->count();

                if ($count) {
                    $result = true;
                    break;
                }

                $message = __('main.access_activate_number', ['condition' => MarksHelper::markName($pageInfo->condition), 'count' => $count]);
                $result = false;
                break;
            default:
                $result = true;
                break;
        }


        if ($type == 'message') {
            return $message;
        } else {
            return $result;
        }

    }

    public static function myRound($number, $type = 'low') {

//        $number = 1;
//        $stringNumber = (string)$number;
//        $arrString = explode('.', $stringNumber);
//        $arrString = (!empty($arrString[1]))?$arrString[1]:'';
//
//        $diff = strlen($arrString[1]) - Helpers::siteCurrency('round');
//        $decPart = $arrString[1];
//
//
//        if ($diff > 0) {
//            $decPart = substr($decPart, 0, Helpers::siteCurrency('round'));
//        } else {
//            $diff *= -1;
//            for ($i = 0; $i < $diff; $i++) {
//                $decPart .= '0';
//            }
//        }
//
//        dd($decPart, $diff);
//
//        $substr = substr($arrString[1], 0, Helpers::siteCurrency('round'));
//        $getResult = $arrString[0];
//
//        return (string)$number;
//        $result = intval($number*100)/100;
//        return $result;

//        if ($type == 'low') {php
            return bcdiv($number, 1, Helpers::siteCurrency('round')) * 1;
//        } else {
//            return round($number, Helpers::siteCurrency('round'));
//        }
//        return number_format($number, 1, Helpers::siteCurrency('round')) * 1;
        //1182
    }

    public static function stepForInput() {
        $currency = self::siteCurrency();
        $step = ($currency > 3)?'0.00000001':'0.01';
        $string = 'step="'.$step.'"';
        return $string;
    }

    public static function iAuth($meth = 'check', $code = 0, $messenger = 'teleg') {
        $domain = new Domain();

        if (AdsExeption::where('subdomain', $domain->getDB())->count()) {
            return false;
        }
        $site = Helpers::getSiteId();
        $user = Auth::id();
        $prefix = ($messenger == 'vk')?'vk_':'';

        $telegCount = TelegramCore::where('site_id', $site)->where('user_id', $user)->where(function ($q) {
            $q->where('status', 1)->orWhere('vk_status', 1);
        })->count();

        $teleg = TelegramCore::where('site_id', $site)->where('user_id', $user)->where($prefix.'status', 1);
        $telegInfo = $teleg->first();


        if ($meth == 'check') {

            if ($telegCount) {
                return true;
            }
            return false;

        } elseif ($meth == 'code') {

            return @$telegInfo->{$prefix.'code'}??0;

        } elseif ($meth == 'check_code') {

            if ($telegCount) {
                if (empty($code) or $code == 0 or $code == null) {
                    $message = "Вы должны ввести код подтверждения";
                    return $message;
                } else {
                    if (Helpers::iAuth('code', 0, $messenger) != $code) {
                        $message = "Код подтверждения неверный. Получите новый код.";
                        Helpers::iAuth('reset');
                        return $message;
                    }
                }
            }

            Helpers::iAuth('reset');
            return '';
        } elseif ($meth == 'form') {
            echo '
                <div class="iauth-confirm js-iauth-protection" style="display: none;">
                    <div class="iauth-protected" style="min-height: 60px; background: #fff; text-align: center; /*display: none;">
                        <h3 class="ta-center">
                            <span class="iauth-color">iAuth</span> защита
                            <i class="fa fa-times iauth-close" style="float: right; cursor: pointer;"></i>
                        </h3>
                        <h4>Обезопасьте свои средства, с помощью социальных сетей</h4>
                        
                        <div class="col-lg-12 p-10">
                            <div class="col-lg-6 iauth-toggle p-10" data-mess="teleg">
                                <div>
                                    <img src="/iadminbest/storage/app/resource/telegram_icon_64.png" width="50px" height="50px" alt="">
                                </div>
                            </div>
                            <div class="col-lg-6 iauth-toggle p-10" data-mess="vk">
                                <div>
                                    <img src="/iadminbest/storage/app/resource/vk_icon_64.png" width="50px" height="50px" alt="">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="iauth-for-content">
                            
                            </div>
                        </div>
                    </div>
                </div>
            ';
        } elseif ($meth == 'reset') {
            $teleg = TelegramCore::where('site_id', $site)->where('user_id', $user);
            $teleg->update(['code' => 0, 'vk_code' => 0]);
        }
    }

    public static function currencyConverter($from,$to,$amount)
    {
        if ($from == $to) {
            return $amount;
        }

        $count = CoreCurrencyRate::query()->count();
        $toLower = strtolower($to);
        if (!$count) {
            CoreCurrencyRate::create([
                'usd' => 0,
                'eur' => 0,
                'updated_at' => 0,
            ]);
        }

        $now = strtotime(now());
        $lastUpdate = CoreCurrencyRate::query()->first();
        if ($now - $lastUpdate->updated_at > 3600 or $lastUpdate->{$toLower} == 0) {
            $key = '449787632953597663af';
            $pair = $from.'_'.$to;
            $pairTurn = $to.'_'.$from;
            $url = 'https://free.currconv.com/api/v7/convert?q='.$pair.','.$pairTurn.'&compact=ultra&apiKey='.$key;

            $request = curl_init();
            $timeOut = 0;
            curl_setopt ($request, CURLOPT_URL, $url);
            curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut);
            $response = json_decode(curl_exec($request));
            curl_close($request);

            $result = $response->{$pair} * $amount;
            $result = self::myRound($result);


            CoreCurrencyRate::query()->first()->update([
                $toLower => $response->{$pairTurn},
                'updated_at' => strtotime(now()),
            ]);
        } else {
            $result = $amount / CoreCurrencyRate::query()->first()->{$toLower};
        }


        return self::myRound($result * 1);


    }

    public static function checkSandBox() {
        $checkSandBox = SiteInfo::find(1)->sandbox;

        if ($checkSandBox) {
            return true;
        }
    }

    public static function refCountLevel($userId, $level, $levelNow = 0, $data = []) {

        $referals = Users::where('refer', $userId)->where('id', '!=', 1);
        if (@$data['type'] == 'active') {
            $referals = $referals->where('type_follow', 1);
        } elseif (@$data['type'] == 'inactive') {
            $referals = $referals->where('type_follow', 0);
        }

        $referals = $referals->get();

        $levelNow += 1;
        $count = 0;

        foreach ($referals as $referal) {
            if ($levelNow < $level) {
                $newReferalsCount = Users::where('refer', $referal->id)->count();
                if ($newReferalsCount) {
                    $count += self::refCountLevel($referal->id, $level, $levelNow, ['type' => @$data['type']]);
                }
            } else {
                $count++;
            }
        }
        return $count;

    }

//    public static function refCountLevel($userId, $level, $active = 0, $mark = 0) {
//
//        $referals = Users::where('refer', $userId)->where('id', '!=', 1)->get();
//        return self::getRefs($referals, $level, 0);
//
//    }
//
//    public static function getRefs($referals, $level, $levelNow = 0) {
//        $levelNow += 1;
//        echo $levelNow.'<br>';
//        $count = 0;
//
//        foreach ($referals as $referal) {
////            dd($referals, $referal->id, $levelNow);
//            if ($levelNow < $level) {
//                $newReferalsCount = Users::where('refer', $referal->id)->count();
//                if ($newReferalsCount) {
//                    echo '-'.$referal->id.'-'.$newReferalsCount.'-'.$levelNow.'-'.$level.'<br>';
//                    $newReferals = Users::where('refer', $referal->id)->get();
//                    $count += self::getRefs($newReferals, $level, $levelNow);
//                }
//            } else {
//                $count++;
//            }
//        }
//        return $count;
//    }

    public static function serialize_corrector($serialized_string){
        // at first, check if "fixing" is really needed at all. After that, security checkup.
//        if ( @unserialize($serialized_string) !== true &&  preg_match('/^[aOs]:/', $serialized_string) ) {
//            $serialized_string = preg_replace_callback( '/s\:(\d+)\:\"(.*?)\";/s',    function($matches){return 's:'.strlen($matches[2]).':"'.$matches[2].'";'; },   $serialized_string );
//        }

        return unserialize($serialized_string);
    }

    public static function abbreviation($value, $needleSymbols, $symbol = '-')
    {
        $value = (string)$value;

        if (mb_strlen($value, 'utf-8') >= $needleSymbols) {
            return mb_substr($value, 0, $needleSymbols - 3, 'utf-8') . '.' . str_repeat('-', 2);
        } else {
            return self::mb_str_pad($value, $needleSymbols, $symbol);
        }
    }

    public static function mb_str_pad($input, $pad_length, $pad_string = ' ', $pad_type = STR_PAD_RIGHT, $encoding = 'UTF-8')
    {
        $input_length = mb_strlen($input, $encoding);
        $pad_string_length = mb_strlen($pad_string, $encoding);

        if ($pad_length <= 0 || ($pad_length - $input_length) <= 0) {
            return $input;
        }

        $num_pad_chars = $pad_length - $input_length;

        switch ($pad_type) {
            case STR_PAD_RIGHT:
                $left_pad = 0;
                $right_pad = $num_pad_chars;
                break;

            case STR_PAD_LEFT:
                $left_pad = $num_pad_chars;
                $right_pad = 0;
                break;

            case STR_PAD_BOTH:
                $left_pad = floor($num_pad_chars / 2);
                $right_pad = $num_pad_chars - $left_pad;
                break;
        }

        $result = '';
        for ($i = 0; $i < $left_pad; ++$i) {
            $result .= mb_substr($pad_string, $i % $pad_string_length, 1, $encoding);
        }
        $result .= $input;
        for ($i = 0; $i < $right_pad; ++$i) {
            $result .= mb_substr($pad_string, $i % $pad_string_length, 1, $encoding);
        }

        return $result;
    }

    public static function getIP()
    {
        return $_SERVER['HTTP_X_REAL_IP'] ?? $_SERVER['REMOTE_ADDR'];
    }

    public static function checkWhitelist($system)
    {
        return in_array(self::getIP(), config("paysystems.{$system}.whitelist"));
    }

}
