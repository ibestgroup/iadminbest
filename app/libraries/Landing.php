<?php


namespace App\libraries;


use App\Models\Cashout;
use App\Models\Payments;
use App\Models\PaymentsOrder;
use App\Models\Users;
use App\Models\Start;
use App\Libraries\Domain;

class Landing
{
    public $sitePath;

    public function __construct()
    {
        $domain = new Domain();

        $this->sitePath = $_SERVER['DOCUMENT_ROOT'].'/sites/'.$domain->getDB();
    }

    public static function getUserPage($page) {
        $file = file_get_contents($page);

        $searchMassiveArrs = self::getUserVars();
        $replaceParams = self::getUserVarsReplace();

        foreach ($searchMassiveArrs as $k => $search) {
            $replaceMas[$k] = $replaceParams[$search];
        }

        $logo = \App\Models\SiteInfo::find(1)['logo'];
        $file = str_replace('</head>', '<link rel="icon" type="image/png" href="/iadminbest/storage/app'.$logo.'" /></head>', $file);

        $csrf = csrf_token();
        $file = str_replace('</head>', '<meta name="csrf-token" content="'.$csrf.'"></head>', $file);
        $file = str_replace('</head>', '<script data-ad-client="ca-pub-7640044855853499" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script></head>', $file);
        $file = str_replace('</form>', '<input type="hidden" name="_token" value="'.csrf_token().'"></form>', $file);


        $file = str_replace('</head>', '<script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(56723590, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/56723590" style="position:absolute; left:-9999px;" alt="" /></div></noscript></head>', $file);

        return str_ireplace($searchMassiveArrs, $replaceMas, $file);
    }

    public static function getUserVarsReplace() {
        $siteType = Helpers::siteType();

        if ($siteType == 1) {
            $payments = PaymentsOrder::where("to_user", "!=", 0);
        } else {
            $payments = Payments::where('id', '!=', 0);
        }


        $allUsers = Users::all()->count();
        $allSum = $payments->sum('amount');
        $paymentsCount = $payments->count();
        $allActiveUsers = Users::where("type_follow", ">", 0)->count();
        $allCashout = Cashout::where('payment_system', '!=', 333)->where('payment_system', '!=', 1919)->where('id_user', '!=', 1)->where('status', 2)->sum('amount');
        $projectTime = Start::find(1)->start_pay;
        $datetime1 = new \DateTime($projectTime);
        $datetime2 = new \DateTime('now');
        $interval = $datetime1->diff($datetime2);
        $projectTime = $interval->format('%a');

        $lastUserLogin = Users::orderBy('id', 'desc')->first()['login'];


        $varsResult = array(
            "{active_users_count}" => $allActiveUsers,
            "{payments_count}" => $paymentsCount,
            "{all_cash}" => $allSum,
            "{project_time}" => $projectTime,
            "{all_users}" => $allUsers,
            "{all_cashout}" => $allCashout,
            "{last_user_login}" => $lastUserLogin,
        );

        return $varsResult;
    }

    public static function getUserVars() {
        return array_values((array)json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/iadminbest/constructor/vars.json")));

    }
}