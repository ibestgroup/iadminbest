<?php

namespace App\Libraries;

use App\Helpers\MarksHelper;
use App\Models\MarksInside;
use App\Models\OrdersMarkInside;
use App\Models\Users;
use App\Models\Marks;
use Illuminate\Support\Facades\Log;

class Multimark {
    public $height = 99;
    public $overflow;
    public $width;
    public $refer;
    public $parentUser;
    public $id_mark;
    public $ch_po;
    public $ms_prefix;
    public $auto;
    public $ordersTree;
    public $orders;
    public $levelsMas = array();
    public $parentMark;
    public $ordersMark;
    public $logins;

    public static $orders1;
    public static $logins1;

    public function __construct($refer, $id_mark, $auto = false)
    {
        global $dom;
        $this->refer = $refer;
        $this->id_mark = $id_mark;
        $this->ms_prefix = ($auto)?'_inside':'';
        $this->auto = $auto;

        if ($this->ms_prefix) {
            $this->ordersMark = OrdersMarkInside::class;
            $mark = MarksInside::find($this->id_mark);
        } else {
            $this->ordersMark = OrdersMarkInside::class;
            $mark = Marks::find($this->id_mark);
        }
        $this->width = $mark['width'];
        $this->overflow = $mark['overflow'];  //TODO: Здесь будет единица, только если маркетинг - 1 ( #youngoldman# Здесь может быть в обоих случаях единица)


        //Родитель из таблицы users
        $this->parentUser = Users::find($this->refer);
//        dd($this->parentUser);
        //Уровень рефера?
        $check_mark = $this->ordersMark::where('id_user', $this->refer)->where('mark', $this->id_mark)->count();
        // #youngoldman# Объявил эту переменную, так как когда вставляю $this->parentUser[refer] в SQL запрос он не находит совпадения и $check_mark соответственно всегда равен нулю. И ниже теперь if нормельно заменяется на while.
        $for_check_mark = $this->parentUser['refer'];
        while ( $check_mark == 0 ) {// Если refer нулевого уровня - ищем первого ненулевого выше в структуре.
            $check_mark = $this->ordersMark::where('id_user', $for_check_mark)->where('mark', $this->id_mark)->count();

            $this->parentUser = Users::where('id', $for_check_mark)->first();
            $for_check_mark = $this->parentUser->refer;
        }


        $this->refer = $this->parentUser['id'];
    }


    public function overflowMulti($orderIdForce = 0) {

        $orders = array();
        $users =  $this->ordersMark::where('mark', $this->id_mark)->get();

        foreach ($users as $user) {
            $orders[] = $user;
        }

        $orderId = $this->ordersMark::where('id_user', $this->refer)->where('mark', $this->id_mark)->max('id');

        if ($this->auto) {
            $countOrdersInside = $this->ordersMark::where('id_user', $this->refer)
                                                    ->where('status', 0)
                                                    ->where('mark', $this->id_mark)
                                                    ->count();

            if ($countOrdersInside > 0) {
                $order_inside = $this->ordersMark::where('id_user', $this->refer)
                                                    ->where('status', 0)
                                                    ->where('mark', $this->id_mark)->get()[0]['id'];
            } else {
                $order_inside = $this->ordersMark::where('id_user', $this->refer)
                                                    ->where('mark', $this->id_mark)->get()[0]['id'];
            }

            $orderId = $this->ordersMark::where('id_user', $this->refer)->where('mark', $this->id_mark)->first();

            if ($this->overflow == 1) {
                $ordersId = $this->ordersMark::where('id_user', $this->refer)->where('mark', $this->id_mark)->get();

                foreach ($ordersId as $ord) {
                    if (MarksHelper::checkClosed($ord->id) == false) {
                        $orderId = $this->ordersMark::where('id', $ord->id)->first();
                        break;
                    }
                }
            }

        }

        $orderId = $orderIdForce ? $this->ordersMark::where('id', $orderIdForce)->first() : $orderId;

        if ($this->overflow) {

            $this->ch_po = $orderId['id'];



            //Строим массив с зависимостями
            foreach ($orders as $key  => $order) {
                $this->orders[$order['par_or']][] = $order['id'];
            }


            //Формируем уровневый массив
            $this->outLevels($this->ch_po);
            $out = $this->searchParent();

            if (!$this->auto) {
                return $this->ordersMark::where('id', $out)->get()[0]['id_user'];
            }

            return $out;
        }

        return ($this->auto)?$order_inside:$this->refer;
    }

    function searchParent () {
        for ($i = 1; $i <= count($this->levelsMas); $i++) {
            foreach ($this->levelsMas[$i] as $id => $child) {

                $c = array_search($id, $child);
                if ($c !== false)
                    unset($child[$c]);
                if (count($child) < $this->width) {
                    return $id;
                } else {
                    foreach ($child as $child_id) {
                        if ($id != $child_id) {
                            if (count($this->levelsMas[$i+1][$child_id]) < $this->width)
                            return $child_id;
                        }
                    }
                }
            }
        }
    }

    function outLevels($parent_id = 1, $level = 1) {

        if (isset($this->orders[$parent_id]) && !empty($this->orders[$parent_id])) {
            foreach ($this->orders[$parent_id] as $k => $mark) { //Обходи
                if ($mark != 1 && $mark != $parent_id) {
                    //Рекурсивно вызываем эту же функцию, но с новым $parent_id и $level
                    $this->outLevels($mark, $level + 1);
                    $this->levelsMas[$level][$parent_id] = $this->orders[$parent_id];
                } else {
                    $this->levelsMas[$level][$parent_id] = $this->orders[$parent_id];
                }
            }
        } else {
            $this->levelsMas[$level][$parent_id] = array();
        }
    }

    function outTree($parent_id = 1) {
        $mas = array();
        if (isset($this->orders[$parent_id])) {
            foreach ($this->orders[$parent_id] as $mark) { //Обходим
                if ($mark['id'] != $parent_id) {
                    $mark['name'] = $this->logins[$mark['user']];
                    //Рекурсивно вызываем эту же функцию, но с новым $parent_id
                    $mark['children'] = $this->outTree($mark['id']);
                    $mas[] = $mark;
                }
            }
        }

        return $mas;
    }

    public function createTree($orderID) {
        $marks = $this->ordersMark::where('mark', $this->id_mark)->get();

        foreach ($marks as $key  => $order) {
            $this->orders[$order['par_or']][] = [
                'id' => $order->id,
                'user' => $order->id_user
            ];
            $users[] = $order->id_user;
        }
        $users = Users::whereIn('id', $users)->select('id', 'login')->get();
        foreach ($users as $user) {
            $this->logins[$user->id] = $user->login;
        }

        return $this->outTree($orderID);
    }

    public static function outTree1($parent_id = 1) {
        $mas = array();
        if (isset(self::$orders1[$parent_id])) {
            foreach (self::$orders1[$parent_id] as $mark) { //Обходим
                if ($mark['id'] != $parent_id) {
                    $mark['name'] = self::$logins1[$mark['user']];
                    //Рекурсивно вызываем эту же функцию, но с новым $parent_id
                    $mark['children'] = self::outTree1($mark['id']);
                    $mas[] = $mark;
                }
            }
        }

        return $mas;
    }

    public static function createTree1($orderID) {
        $mark = OrdersMarkInside::find($orderID)->mark;
        $marks = OrdersMarkInside::where('mark', $mark)->get();

        foreach ($marks as $key  => $order) {
            self::$orders1[$order['par_or']][] = [
                'id' => $order->id,
                'user' => $order->id_user
            ];
            $users[] = $order->id_user;
        }
        $users = Users::whereIn('id', $users)->select('id', 'login')->get();
        foreach ($users as $user) {
            self::$logins1[$user->id] = $user->login;
        }

        return self::outTree1($orderID);
    }
}
