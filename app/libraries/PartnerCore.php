<?php

namespace App\Libraries;

use App\Libraries\Multimark;
use App\Models\CorePartnerPayments;
use App\Models\CoreProfit;
use App\Helpers\MarksHelper;
use App\Models\CoreUsers;
use App\Models\Marks;
use App\Models\OrdersMark;
use App\Models\Users;
use App\Models\OrdersMarkInside;
use App\Models\PaymentsOrder;
use App\Models\SiteInfo;
use Illuminate\Support\Collection;

class PartnerCore
{

    /**
     * @param $amount
     * @param $fromUser
     * @param $tarif
     */

    public function sendPercent($amount, $fromUser, $tarif) {

        $siteID = Helpers::getSiteId();
        $siteAdminID = Helpers::getSiteAdmin($siteID);
        $siteAdmin = CoreUsers::where('id', Helpers::getSiteAdmin())->first();
        $siteInfo = SiteInfo::find(1);

        $comission = $amount/100*10;

        $percentSum = 0;
        $percentSumRub = 0;
        $comissionConstructor = $comission;
        $comissionConstructorRub = $comissionConstructor;


        if ($siteAdmin['refer'] != 1) {

            $parentAdmin = CoreUsers::where('id', $siteAdmin['refer'])->first();

            $percent = self::percent($parentAdmin['privelege'])/100;

            $percentSum = $comission * $percent;
            $percentSumRub = $percentSum;

            $comissionConstructor = $comission - $percentSum;
            $comissionConstructorRub = $comissionConstructor;

        }

        if ($siteInfo['value'] != 1) {
            $siteInfoValue = Helpers::getCurrent($siteInfo['value'], 'code');
            $percentSumRub = Helpers::convertCurrency($siteInfoValue, 'rub', $percentSum);
            $comissionConstructorRub = Helpers::convertCurrency($siteInfoValue, 'rub', $comissionConstructor);
        }


        if ($percentSum != 0) {
            CorePartnerPayments::create([
                'site_id' => $siteID,
                'site_admin' => $siteAdminID,
                'from_user' => $fromUser,
                'to_user' => $parentAdmin['id'],
                'amount' => $percentSum,
                'real_amount' => $amount,
                'real_amount_rub' => $percentSumRub,
                'currency' => $siteInfo['value'],
                'mark' => $tarif,
                'date' => now()
            ]);

            self::updatePrivelege($parentAdmin['id']);


        }

        CorePartnerPayments::create([
            'site_id' => $siteID,
            'site_admin' => 1,
            'from_user' => $fromUser,
            'to_user' => 1,
            'amount' => $comissionConstructor,
            'real_amount' => $amount,
            'real_amount_rub' => $comissionConstructorRub,
            'currency' => $siteInfo['value'],
            'mark' => $tarif,
            'date' => now()
        ]);

    }

    public function percent($privilege) {
        $percent = 0;

        switch ($privilege) {

            case 1:
                $percent = 5;
                break;
            case 2:
                $percent = 10;
                break;
            case 3:
                $percent = 15;
                break;
            case 4:
                $percent = 20;
                break;
            case 5:
                $percent = 25;
                break;
            case 6:
                $percent = 30;
                break;
            case 7:
                $percent = 35;
                break;
            case 8:
                $percent = 40;
                break;
            case 9:
                $percent = 50;
                break;

        }

        return $percent;

    }

    public function updatePrivelege($id) {

        $allProfit = CorePartnerPayments::where('to_user', $id)->sum('parent_amount');

        $privilege = 1;
        if ($allProfit >= 100 and $allProfit <= 500) {
            $privilege = 2;
        } elseif ($allProfit > 500 and $allProfit <= 1000) {
            $privilege = 3;
        } elseif ($allProfit > 1000 and $allProfit <= 10000) {
            $privilege = 4;
        } elseif ($allProfit > 10000 and $allProfit <= 100000) {
            $privilege = 5;
        } elseif ($allProfit > 100000 and $allProfit <= 500000) {
            $privilege = 6;
        } elseif ($allProfit > 500000 and $allProfit <= 2500000) {
            $privilege = 7;
        } elseif ($allProfit > 2500000 and $allProfit <= 10000000) {
            $privilege = 8;
        } elseif ($allProfit > 10000000) {
            $privilege = 9;
        }

        CoreUsers::where('id', $id)->update(['privelege' => $privilege]);
    }

    public function privelegeInfo() {
        $rang = [
            [
                'from' => 0,
                'to' => 100,
                'name' => __('main.rang_1'),
                'percent' => 5
            ],
            [
                'from' => 101,
                'to' => 500,
                'name' => __('main.rang_2'),
                'percent' => 10
            ],
            [
                'from' => 501,
                'to' => 1000,
                'name' => __('main.rang_3'),
                'percent' => 15
            ],
            [
                'from' => 1001,
                'to' => 10000,
                'name' => __('main.rang_4'),
                'percent' => 20
            ],
            [
                'from' => 10001,
                'to' => 100000,
                'name' => __('main.rang_5'),
                'percent' => 25
            ],
            [
                'from' => 100001,
                'to' => 500000,
                'name' => __('main.rang_6'),
                'percent' => 30
            ],
            [
                'from' => 500001,
                'to' => 2500000,
                'name' => __('main.rang_7'),
                'percent' => 35
            ],
            [
                'from' => 2500001,
                'to' => 10000000,
                'name' => __('main.rang_8'),
                'percent' => 40
            ],
            [
                'from' => 10000001,
                'to' => 100000000,
                'name' => __('main.rang_9'),
                'percent' => 50
            ],
        ];

        return $rang;
    }

}
