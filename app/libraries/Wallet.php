<?php

namespace App\Libraries;
use App\Models\Buxone\UsersBuxone;
use App\Models\Cashout;
use App\Models\CashoutSystemCore;
use App\Models\CoreSiteList;
use App\Models\Payments;
use App\Models\PaymentsOrder;
use App\Models\SiteInfo;
use App\Models\SyncBuxone;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Support\Facades\Auth;
use App\Libraries\Helpers;
use App\Models\Users;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class Wallet {

    /**
     * @int $system
     * @int $address
     * @int $sum
     * @return array
     */
    public static function send($system, $address, $sum) {

        if ($system != 1919 and $system != 1999 and $system != 221) {

            $typeCurrency = Helpers::siteCurrency('type');
            $siteCurrency = Helpers::siteCurrency();

            if ($typeCurrency == 'fiat') {

                if (in_array($system, [1,2])) {

                    if ($sum >= 2) {
                        $sum = $sum/100*98;
                    }

                    $accountNumber = 'P1008663143';
                    $apiId = '865042113';
                    $apiKey = 'DannaDiD19DannaDiD19';
                    $siteName = SiteInfo::find(1)['name'];

                    $payeer = new CPayeer($accountNumber, $apiId, $apiKey);

                    $dom = new Domain();
                    $resURL = $dom->resultDomain();

                    if ($payeer->isAuth())
                    {

                        if($payeer->checkUser(array(
                            'user' => $address,
                        ))) {
                            $arTransfer = $payeer->transfer(array(
                                'curIn' => Helpers::siteCurrency('code'),
                                'sum' => $sum,
                                'curOut' => Helpers::siteCurrency('code'),
                                'to' => $address,
                                'comment' => 'Выплата с '.$siteName.'('.$resURL.') Пользователь #'.Auth::id(),
                            ));

                            if (empty($arTransfer['errors'])) {
                                //                        echo $arTransfer['historyId'].": Перевод средств успешно выполнен";
                                $result['desc'] = 'Payment send';
                                $result['id'] = $arTransfer['historyId'];
                            } else {
                                $result['desc'] = 'Error';
                            }


                        } else {
                            $result['desc'] = 'Not found';
                        }

                    } else {
                        $result['desc'] = 'Error';
                    }

                    return json_encode($result);

                } elseif (in_array($system, [64,69])) {

                    if ($sum >= 1) {
                        $sum = $sum/100*98;
                    }

                    /*
                        This script demonstrates transfer proccess between two
                        PerfectMoney accounts using PerfectMoney API interface.
                    */

                    // trying to open URL to process PerfectMoney Spend request
                    $accountPM = '5053227';
                    $passwordPM = 'DannaDiD19';
                    $addressPM = $address;

                    if ($siteCurrency == 2) {
                        $walletPM = 'U22457714';
                    } else {
                        $walletPM = 'E22621241';
                    }

                    $f=fopen('https://perfectmoney.com/acct/confirm.asp?AccountID='.$accountPM.'&PassPhrase='.$passwordPM.'&Payer_Account='.$walletPM.'&Payee_Account='.$addressPM.'&Amount='.$sum.'&PAY_IN='.$sum.'&PAYMENT_ID=1223', 'rb');
//                    $f=fopen('https://perfectmoney.com/acct/confirm.asp?AccountID=5053227&PassPhrase=DannaDiD19&Payer_Account=U22457714&Payee_Account=U15626512&Amount=0.1&PAY_IN=0.1&PAYMENT_ID=1223', 'rb');

                    if($f === false){
                        echo 'error openning url';
                    }

                    // getting data
                    $out=array(); $out="";
                    while(!feof($f)) $out.=fgets($f);

                    fclose($f);

                    // searching for hidden fields
                    if(!preg_match_all("/<input name='(.*)' type='hidden' value='(.*)'>/", $out, $result, PREG_SET_ORDER)){
                        $result['desc'] = 'Error';
                        return json_encode($result);
                    }

                    $result['desc'] = 'Payment send';
                    return json_encode($result);

                } elseif (in_array($system, [955])) {

                    $amountResultFormat = number_format($sum, 2, '.', '');
                    $orderId = Cashout::count() + 1;

                    $params = [
                        'amount' => $amountResultFormat,
                        'currency' => Helpers::siteCurrency('code'),
                        'orderId' => $orderId,
                        'paymentSystem' => 'card',
                        'address' => $address,
                    ];
                    $sign = md5(implode('', $params) . '89ec8d252bc9a43ec1acb477ea740c05');

                    $answer = json_decode(Curl::to('https://merchant.betatransfer.io/api/withdrawal-payment?token=bcbbaeade259b067d9631b0ed714d703')
                        ->withData([
                            'amount' => $amountResultFormat,
                            'currency' => Helpers::siteCurrency('code'),
                            'orderId' => $orderId,
                            'paymentSystem' => $params['paymentSystem'],
                            'address' => $address,
                            'sign' => $sign,
                        ])
                        ->post());
//                    dd($redirectUrl, $params);

                    if ($answer->status == 'success') {
                        $result['desc'] = 'Payment send';
                        $result['id'] = $answer->id;
                        return json_encode($result);
                    } else {
                        $result['desc'] = 'Error';
                        return json_encode($result);
                    }

                } elseif (CashoutSystemCore::where('code', $system)->count()) {

                    $domain = new Domain();
                    $resultDomain = $domain->resultDomain();

                    $result = Curl::to('https://www.fkwallet.com/api_v1.php')
                        ->withData([
                            'wallet_id' => 'F105618742',
                            'purse' => $address,
                            'amount' => $sum,
                            'desc' => $resultDomain,
                            'currency' => $system,
                            'sign' => md5('F105618742' . $system . $sum . $address . '0FF87427171445B1787C426554E4F0C9'),
                            'action' => 'cashout',
                        ])
                    ->post();

                    return $result;
                }

            } else {



            }


//            return Curl::to('https://wallet.free-kassa.ru/api_v1.php')
//                ->withData([
//                    'wallet_id' => 'F102913898',
//                    'purse' => $address,
//                    'amount' => $sum,
//                    'desc' => url('/'). '[' .Auth::user()->login.']',
//                    'currency' => $system,
//                    'sign' => md5('F102913898' . $system . $sum . $address . '95F7E92211D349AC71E65899ABDFD235'),
//                    'action' => 'cashout',
//                ])
//            ->post();


        } elseif ($system == 221) {

            Config::set("database.connections.buxone", [
                "host" => "127.0.0.1",
                "database" => 'bux_one',
                "username" => 'root',
                "password" => "DrinkVodkaPlayDotka228",
                'driver' => 'mysql'
            ]);

            $result['desc'] = 'Error';
            if (Helpers::siteCurrency() != 1) {
                return json_encode($result);
            }

            $countSync = SyncBuxone::where('user_id', Auth::id())->count();

            if ($countSync) {
                $userIdBuxone = SyncBuxone::where('user_id', Auth::id())->first()['user_buxone'];
                UsersBuxone::where('id', $userIdBuxone)->increment('money', $sum);
                $result['desc'] = 'Payment send';
            }

            return json_encode($result);

        } elseif ($system == 1919) {

            $siteType = Helpers::siteType();

            $userResult = Users::where('login', $address)->count();

            if ($userResult) {
                $toUser = Users::where('login', $address)->first()['id'];
            } else {
                $toUser = 0;
            }

            if ($toUser) {
//                $resultUser->where('id', $idUser)->increment('balance', $sum)
                if ($siteType > 1) {
                    Users::where('id', $toUser)->increment('balance_cashout', $sum);
                } else {
                    Users::where('id', $toUser)->increment('balance', $sum);
                }

                $fromAddress = Auth::id();

                $payment = new PaymentsOrder();
                $payment->create([
                        'from_user' => $fromAddress,
                        'to_user' => $toUser,
                        'from_wallet' => '0',
                        'wallet' => '0',
                        'from_level' => 0,
                        'level' => 0,
                        'amount' => $sum,
                        'date' => now(),
                        'direct' => 0,
                        'mark' => 0,
                        'type' => 'inside_transaction'
                    ]);

                $result['desc'] = 'Payment send';
            } else {
                $result['desc'] = 'Error';
            }

            return json_encode($result);

        } elseif ($system == 1999) {

        }

    }

    public static function withdrawAccess($system, $amount, $userId)
    {
        $balanceSite = Payments::sum('amount');
        $cashoutAll = Cashout::where('payment_system', '!=', 1919)->where('payment_system', '!=', 333)->sum('amount') + $amount;

        if ($balanceSite < $cashoutAll) {
            return [
                "access" => false,
                "message" => "Что-то пошло не так",
            ];
        }

        $paymentFee = CashoutSystemCore::where('code', $system)->first();

        $resultFee = round(((($amount * $paymentFee['fee']) / 100) + $paymentFee['custom_fee']), 2);
        $resultSum = $amount + $resultFee;

        $balanceResult = Users::whereId($userId)->first()->balance;
        if ($balanceResult < $resultSum) {
            DB::rollBack();
            return [
                "access" => false,
                "message" => "На вашем балансе недостаточно средств. Сумма с учетом комиссии $resultSum. Ваш баланс: $balanceResult",
            ];
        }

        return [
            "access" => true,
            "message" => "OK",
            "result_sum" => $resultSum,
        ];

    }
}
