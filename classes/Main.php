<?php

/**
 * Class Main
 * TODO: Класс будет содержать часто используемые запросы. Чтобы не было дублей
 */
class Main {
	public static $instance; //Экземпляр объекта
	public static $admin;
	public static $marksInside;
    public static $marks;
    public static $icon;
    public static $siteInfo;
    public static $dom;
    public static $curUser;

	public function __construct() {}

	public static function getInstance() {
		if (self::$instance === null) {
			self::$instance = new self();
			DB::getInstance(); // Тут же готовим БД
		}
		return self::$instance;
	}

	/**
	 * @return array|null
	 */
	public static function getAdmin() {
		if (!self::$admin) {
			self::$admin = DB::fetch_array(DB::query("SELECT * FROM `users` WHERE `id` = 1"));
		}

		return self::$admin;
	}

	public static function getCurUser() {
	    if (!self::$curUser) {

        }

        return self::$curUser;
    }

	public static function getMarksInside() {
        if (!self::$marksInside) {
            $res = DB::query("SELECT * FROM `marks_inside`");
            while ($ob = DB::fetch_array($res)) {
                self::$marksInside[$ob['id']] = $ob;
            }

        }

        return self::$marksInside;
    }

    public static function getMarks($arFilter = array()) {
	    if (self::siteInfo()['mark_type'] != 3) {
	        $prefix = '_inside';
        }
        if (!self::$marks) {
            $res = DB::query("SELECT * FROM `marks$prefix`");
            while ($ob = DB::fetch_array($res)) {
                self::$marks[$ob['id']] = $ob;
            }

        }

        return self::$marks;
    }

    public static function siteInfo() {
	    if (!self::$siteInfo)
            self::$siteInfo = DB::fetch_array(DB::query("SELECT * FROM `setting_site`"));

	    return self::$siteInfo;
    }

    public static function getIcon($cur_id = false) {
	    if (!$cur_id) {
	        $cur_id = self::siteInfo()['value'];
        }
		switch ($cur_id) {
			case 1:
				$cur_ico = 'fa fa-rub';
				break;
			case 2:
				$cur_ico = 'fa fa-bitcoin';
				break;
			case 3:
				$cur_ico = 'fa fa-usd';
				break;
			case 4:
				$cur_ico = 'fa fa-eur';
				break;
			case 5:
				$cur_ico = 'cc ETH';
				break;
			case 6:
				$cur_ico = 'cc LTC-alt';
				break;
			case 7:
				$cur_ico = 'cc DOGE-alt';
				break;
			case 8:
				$cur_ico = 'cc XRP-alt';
				break;
			case 9:
				$cur_ico = 'cc LTC-alt';
				break;
			default:
				$cur_ico = 'fa fa-rub';
		}
		return '<i class="'.$cur_ico.'" aria-hidden="true"></i>';
    }

    public static function curl($url, $data) {
	    $ch = curl_init($url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	    $response = curl_exec($ch);

	    curl_close($ch);
		return $response;
    }



    public static function money_mark_auto($id, $meth = 'enter') {
        $tarif = Main::getMarksInside()[$id];
        $price_level = [$tarif['level1'],$tarif['level2'],$tarif['level3'],$tarif['level4'],$tarif['level5'],$tarif['level6'],$tarif['level7'],$tarif['level8']];
        $i = 0;
        $price_all = 0;
        $profit_all = 0;
        while ($i < $tarif['level']) {
            $price_all = $price_all + $price_level[$i];
            $i++;
            $profit_all = $profit_all + pow($tarif['width'], $i)*$price_level[$i-1] * 0.9;
        }
        $price_all = $price_all + $tarif['to_par'] + $tarif['to_admin'];
        if ($meth == 'enter')
            return $price_all;
        else if ($meth == 'profit')
            return $profit_all;
    }

    public static function moneyMark($om) {
	    global $coinsArr;
        $landing = DB::fetch_array(DB::query("SELECT * FROM `landing`"));
        $marks_level = [1, $om['level1'], $om['level2'], $om['level3'], $om['level4'], $om['level5'], $om['level6'], $om['level7'], $om['level8']];
        $q_l = 1;
        $res_profit = 0;
        $res_price = 0;
        while ( $q_l <= $om['level'] ) {
            if ( $landing['table_m'] == 1 ) {
                $res_profit = $res_profit + pow($om['width'], $q_l)*($marks_level[$q_l]*0.9);
            } elseif ( $landing['table_m'] == 2 ) {
                $res_profit = $res_profit + pow($om['width'], $q_l)*$marks_level[$q_l];
            }
            $res_price = $res_price + $marks_level[$q_l];
            $q_l++;
        }

        $out = array('res_profit' => $res_profit, 'res_price' => $res_price);

        if (array_key_exists(self::siteInfo()['value'], $coinsArr)) {
            $out['res_profit'] = in_crypto($res_profit);
            $out['res_price'] = in_crypto($res_price);
        }

        return $out;
    }

    public static function getUserPage($page) {
	    $file = file_get_contents($page);
	    $searchMassiveArrs = self::getUserVars();
	    $replaceParams = self::getUserVarsReplace();

	    foreach ($searchMassiveArrs as $k => $search) {
	        $replaceMas[$k] = $replaceParams[$search];
        }

        return str_ireplace($searchMassiveArrs, $replaceMas,$file);
    }

    public static function getUserVarsReplace() {
        $allUsers = DB::query("SELECT count(*) FROM `users`")->fetch_row()[0];
        $allSum = DB::query("SELECT sum(amount) FROM `payments_order` WHERE `to_user` != 0")->fetch_row()[0];
        $paymentsCount = DB::query("SELECT count(*) FROM `payments_order` WHERE `to_user` != 0")->fetch_row()[0];
        $allActiveUsers = DB::query("SELECT count(*) FROM `users` WHERE `type_follow` > 0")->fetch_row()[0];
        $projectTime = DB::query("SELECT `start_pay` FROM `start`")->fetch_assoc()['start_pay'];
        $datetime1 = new DateTime($projectTime);
        $datetime2 = new DateTime('now');
        $interval = $datetime1->diff($datetime2);
        $projectTime = $interval->format('%a');
        $varsResult = array(
            "{active_users_count}" => $allActiveUsers,
            "{payments_count}" => $paymentsCount,
            "{all_cash}" => $allSum,
            "{project_time}" => $projectTime
        );

        return $varsResult;
    }

    public static function getUserVars() {
	    return array_values((array)json_decode(file_get_contents($_SERVER['DOCUMENT_ROOT']."/constructor/vars.json")));

    }

    public static function dump($res, $die = false) {
		?><pre><?print_r($res)?></pre><?
	    if ($die)
	    	die;
    }

    public static function mail($to, $theme, $message) {
        $headers_mail = 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers_mail .= 'MIME-Version: 1.0' . "\r\n";
        $headers_mail .= "Content-Transfer-Encoding: 8bit \r\n";
        $headers_mail .= 'To: '.$_POST['email'] . "\r\n";
        $headers_mail .= 'From: '.Main::siteInfo()['name'].' <info@iadmin.work>' . "\r\n";
        $headers_mail .= "Date: ".date("r (T)")." \r\n";

        mail($to, $theme, $message, $headers_mail);
    }

    public static function getDom() {
        global $subdom, $db_pref;
        return FormChars($subdom[1][0].$db_pref);
    }
}

/**
 * Class DB
 *
 * TODO: По факту, наша обертка над запросами к бд.
 * Сокращаем кол-во подключений таким образом и длину строк в том числе
 */
class DB {

	protected static $_instance;  //экземпляр объекта

	public static function getInstance() { // получить экземпляр данного класса
		if (self::$_instance === null) { // если экземпляр данного класса  не создан
			self::$_instance = new self;  // создаем экземпляр данного класса
		}
		return self::$_instance; // возвращаем экземпляр данного класса
	}

	private  function __construct() { // конструктор отрабатывает один раз при вызове DB::getInstance();
		$this->connect = mysqli_connect(HOST, USER, PASS, DB);
	}

	private function __clone() { //запрещаем клонирование объекта модификатором private
	}

	private function __wakeup() {//запрещаем клонирование объекта модификатором private
	}

	public static function query($sql, $connect = false) {
		$obj=self::$_instance;

		//Если коннект кастомный
		if ($connect) {
			$result=mysqli_query($connect, $sql);
		} elseif (isset($obj->connect)) {
			$result=mysqli_query($obj->connect, $sql);
		} else {
			return false;
		}
		return $result;
	}

	//возвращает запись в виде объекта
	public static function fetch_object($object)
	{
		return @mysqli_fetch_object($object);
	}

	//возвращает запись в виде массива
	public static function fetch_array($object)
	{
		if ($object)
			return @mysqli_fetch_assoc($object);

		return array();
	}

	public static function fetch_to_array($object) {
		if ($object)
			return @mysqli_fetch_array($object);

		return array();
	}

	public static function getRows($object) {
		return @mysqli_num_rows($object);
	}

	public static function getInsertId($connect) {
		$obj=self::$_instance;
		if (!$connect)
			$connect = $obj->connect;

		return mysqli_insert_id($connect);
	}
}
