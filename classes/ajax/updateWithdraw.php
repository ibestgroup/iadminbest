<?php
@set_time_limit(0);
ULogin(1);

include $_SERVER['DOCUMENT_ROOT'].'/classes/coinpayments/coinpayments.inc.php';

$cps = new CoinPaymentsAPI();
$result = $cps->GetWithdrawalInfo($_POST['id'])['result'];

//TODO: Вообще это дибелизм как по мне, выдавать id транзакции только в статусе 2, но деваться некуда
if ($result['status'] == 2) {
	$return = array(
		'tx_id' =>  $result['send_txid'],
		'status' => 2
	);
} elseif ($result['status'] == 1) {
	$return = array(
		'status' => $result['status'],
		'date' => date('d.m.Y H:i:s', $result['time_created'])
	);
} else {
	$return = array(
		'status' => -1,
		'date' => date('d.m.Y H:i:s', $result['time_created'])
	);
}
print json_encode($return);

