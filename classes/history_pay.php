<?
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/navigation.php';


class HistoryPay {
	private $connect;
	public $allCnt;
	public $userId;
    public $curPage = 0;
    public $allPaymentsOrder = 0;
    public $allPaymentCrypto = 0;
    public $allCashoutCrypto = 0;

	/**
	 * Пока это забито жестко, но стоит дать возможность выбирать пользователю
	 * Сколько елементов фигачить на страницу
	 *
	 * @var int
	 */
	public $num = 10;


    public function __construct($userId) {
        global $curecho, $site_info, $coinsArr;

	    $this->userId = $userId;
        $this->connect = mysqli_connect(HOST, USER, PASS, DB);
        $allObj = mysqli_query($this->connect,"SELECT COUNT(*) FROM `payments_order` where to_user = $this->userId or from_user = $this->userId and to_user != 0");
        $this->allPaymentsOrder = mysqli_fetch_row($allObj)[0];
	    $this->allCnt = $this->allPaymentsOrder;

	    if (array_key_exists($site_info['value'], $coinsArr)) {
            $this->allPaymentCrypto = mysqli_fetch_row(mysqli_query($this->connect,"SELECT COUNT(*) FROM `payment_crypto` where id_user = $this->userId and status != 0"))[0];
            $this->allCashoutCrypto = mysqli_fetch_row(mysqli_query($this->connect,"SELECT COUNT(*) FROM `cashout_crypto` where id_user = $this->userId"))[0];
            $this->allCnt += $this->allPaymentCrypto;
            $this->allCnt += $this->allCashoutCrypto;
        } else {

        }
	    $this->curPage = $_GET['page']?$_GET['page']:1;
    }

    public function build() {
    	global $curecho, $site_info, $coinsArr;
		$crypto = array_key_exists($site_info['value'], $coinsArr);

        /**
         * Вычисляем начиная к какого номера
         * следует выводить сообщения
         */
        $start = $this->curPage * $this->num - $this->num;
        $sort = $_GET['sort']?$_GET['sort']:'date';
		$order = $_GET['order']?$_GET['order']:'desc';
		if ($_GET['all'] == 'Y') {
            $history = mysqli_query($this->connect, "select * from `payments_order`");
        } else {
            $history = mysqli_query($this->connect, "select * from `payments_order` where to_user = $this->userId or from_user = $this->userId and to_user != 0");
        }

		if ($crypto) {
            $payment_crypto = mysqli_query($this->connect, "select * from `payment_crypto` where id_user = $this->userId and status != 0");
            $cashout_crypto = mysqli_query($this->connect, "select * from `cashout_crypto` where id_user = $this->userId");
        }

        $result = array();

        for ($i = 0; $i < max($this->allPaymentsOrder, $this->allPaymentCrypto, $this->allCashoutCrypto); $i++) {
            if ($h = mysqli_fetch_assoc($history)) {
            	$h['type'] = 'o';
                array_push($result, $h);
            }

            if ($crypto) {
                if ($p = mysqli_fetch_assoc($payment_crypto)) {
                	$p['type'] = 'd';
                    array_push($result, $p);
                }
                if ($c = mysqli_fetch_assoc($cashout_crypto)) {
	                $c['type'] = 'w';
                    array_push($result, $c);
                }
            }
        }

        //Сортируем наш массив по времени
        if ($sort == 'date') {
            usort($result, function ($a, $b) use ($order) {
                if ($order == 'asc')
                    return strtotime($a['date']) - strtotime($b['date']);
                else
                    return strtotime($b['date']) - strtotime($a['date']);
            });
        }

        //Сортируем по сумме
        //TODO: Какое-то блядство с сортировкой чисел с плавающей точкой, нужно сделать по нормальному
        if ($sort == 'amount') {
            usort($result, function ($a, $b) use ($order) {
                if ($order == 'asc')
                    return (int)($a['amount']*100000000) - (integer)($b['amount']*100000000);
                else
                    return (integer)($b['amount']*100000000) - (integer)($a['amount']*100000000);
            });
        }
        
        $result = array_slice($result, $start, $this->num); // Для постранички

	    foreach ($result as $key => $transaction) {
	    	if (!$crypto) {
				if ( $transaction['from_user'] == $this->userId ) {
					$color = 'from-pay';
					$arrow = '<i class="fa fa-arrow-right" aria-hidden="true"></i>';
					$who = $transaction['to_user'];
				} else {
					$color = 'to-pay';
					$arrow = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
					$who = $transaction['from_user'];
				}
				?>
				<div class="<?=$color;?> pay-info-line">
					<div class="col-lg-1 col-xs-2"><?=$transaction['id']?></div>
					<div class="col-lg-2 hidden-xs"><?=id_in_login($this->userId);?></div>
					<div class="col-lg-1 col-xs-4"><?=$arrow?></div>
					<div class="col-lg-2 col-xs-4"><?=id_in_login($who);?></div>

					<div class="col-lg-2 col-xs-2"><?=$transaction['amount'];?><?=$curecho;?></div>
					<div class="col-lg-4 hidden-xs"><?=$transaction['date'];?></div>
					<div class="clearfix"></div>
				</div>
				<div class="remodal" data-remodal-id="modal-<?=$transaction['id']?>">
					<button data-remodal-action="close" class="remodal-close"></button>
					<div class="col-lg-12" id="remodal-<?=$transaction['id']?>"></div>
				</div>
			<?} else {//TODO: Тут вывод операций с криптой
			    $type = $transaction['type'];
				if ($transaction['from_user']) {
					if ( $transaction['from_user'] == $this->userId ) {
						$color = 'from-pay';
						$arrow = '<i class="fa fa-arrow-right" aria-hidden="true"></i>';
						$who = $transaction['to_user'];
					} else {
						$color = 'to-pay';
						$arrow = '<i class="fa fa-arrow-left" aria-hidden="true"></i>';
						$who = $transaction['from_user'];
					}
				} else {
					if ($type == 'w') {
						$color = 'from-pay';
						$arrow = '<i class="fa fa-arrow-up" aria-hidden="true"></i>';
						$who = $transaction['to_user'];
					} elseif ($type == 'd') {
						$color = 'to-pay';
						$arrow = '<i class="fa fa-arrow-down" aria-hidden="true"></i>';
						$who = $transaction['to_user'];
					}

					switch ($transaction['status']) {
						case 0:
							$statusText =  'Принята';
							$class =  'bg-warning';
							break;
						case 1:
							$statusText =  'В обработке';
							$class =  'bg-info';
							break;
						case 2:
							$statusText =  'Выполнена';
							$class =  'bg-success';
							break;
						case -1:
							$statusText =  'Отменена';
							$class =  'bg-danger';
							break;
					}
				}
			    ?>
				<div class="<?=$class;?> pay-info-line" <?=($type == 'd' || $type == 'w')?'role="button" data-toggle="collapse" href="#'.$transaction['id'].'_'.$type.'" aria-expanded="false" aria-controls="'.$transaction['id'].'_'.$type.'"':'';?>>
					<div class="col-lg-1 col-xs-2"><?=$transaction['id'].'_'.$type;?></div>
					<div class="col-lg-2 hidden-xs"><?=id_in_login($this->userId);?></div>
					<div class="col-lg-1 col-xs-4 <?=$color?>"><?=$arrow?></div>
					<div class="col-lg-2 col-xs-4"><?=id_in_login($who);?></div>

					<div class="col-lg-2 col-xs-2"><?=$transaction['amount'];?><?=$curecho;?></div>
					<div class="col-lg-4 hidden-xs tr_date"><?=$transaction['date'];?></div>
					<div class="clearfix"></div>
				</div>
			    <?if ($type == 'd' || $type == 'w') {?>
					<div class="collapse" id="<?=$transaction['id'].'_'.$type?>" style="padding: 0 15px">
						<div class="table-responsive">
							<table class="table">
								<tbody>
								<tr>
									<td>Аддресс</td>
									<td class="tr_address"><?=$transaction['address']?></td>
								</tr>
								<tr>
									<td>ID транзакции</td>
									<td class="tr_id"><?=$transaction['tx_id']?></td>
								</tr>
								<tr>
									<td>Статус</td>
									<td class="tr_status">
									    <?=$statusText?>
									</td>
								</tr>
							    <?if ($transaction['status'] != 2 && $type == 'w') {?>
									<tr>
										<td colspan="2">
											<a href="/classes/ajax/updateWithdraw.php" class="get-withdraw-info" data-id="<?=$transaction['cashout_id']?>">Обновить</a>
										</td>
									</tr>
							    <?}?>
								</tbody>
							</table>
						</div>
					</div>
			    <?}?>
			<?}
	    }
	    if ($_GET['order'])
	    	$path = '?order='.$_GET['order'].'&sort='.$_GET['sort'];
	    else
	    	$path = '';
	    $navigation = new Paginate('/historypay'.$path);
	    if ($path)
	    	$navigation->tpl = "&page={page}";
	    else {
		    $navigation->tpl = "?page={page}";
		}
	    $navigation->spread = 4;
	    $navigation->nextPrev = false;
	    echo $navigation->build($this->num, $this->allCnt, $this->curPage);
    }

    public static function dump($res)  {
    	?><pre style="display: none"><?print_r($res)?></pre><?
	}
}
?>
