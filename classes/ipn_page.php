<?php
/**
 * Created by PhpStorm.
 * User: sbondarenko
 * Date: 16.02.2018
 * Time: 11:57
 */

require($_SERVER['DOCUMENT_ROOT'].'/classes/coinpayments/coinpayments.inc.php');
$cp_debug_email = 'sinnochi@gmail.com';

//mail($cp_debug_email, 'Post', print_r($_REQUEST, true));

// TODO: Тут обработка колбэков от платежки

	// Fill these in with the information from your CoinPayments.net account.
//	http://mlmone.click/classes/coinpayments/ipn_page.php
	//$cp_merchant_id = 'b153f424d3c1af24c89dcf2e371e1b7f';
	$cp_merchant_id = '999975313f47d5cf4c33570cabdefd82';
	$cp_ipn_secret = 'tDjNCuEM6PbUiSu';

	function errorAndDie($error_msg) {
		global $cp_debug_email;
		if (!empty($cp_debug_email)) {
			$report = 'Error: '.$error_msg."\n\n";
			$report .= "POST Data\n\n";
			foreach ($_REQUEST as $k => $v) {
				$report .= "|$k| = |$v|\n";
			}
			mail($cp_debug_email, 'CoinPayments IPN Error', $report);
		}
		die('IPN Error: '.$error_msg);
	}

	if (!isset($_REQUEST['ipn_mode']) || $_REQUEST['ipn_mode'] != 'hmac') {
		errorAndDie('IPN Mode is not HMAC');
	}

	if (!isset($_SERVER['HTTP_HMAC']) || empty($_SERVER['HTTP_HMAC'])) {
		errorAndDie('No HMAC signature sent.');
	}

	$request = file_get_contents('php://input');
	if ($request === FALSE || empty($request)) {
		errorAndDie('Error reading POST data');
	}

	if (!isset($_POST['merchant']) || $_POST['merchant'] != trim($cp_merchant_id)) {
		errorAndDie('No or incorrect Merchant ID passed');
	}

	$hmac = hash_hmac("sha512", $request, trim($cp_ipn_secret));
	if ($hmac != $_SERVER['HTTP_HMAC']) {
		//if ($hmac != $_SERVER['HTTP_HMAC']) { <-- Use this if you are running a version of PHP below 5.6.0 without the hash_equals function
		errorAndDie('HMAC signature does not match');
	}

	// HMAC Signature verified at this point, load some variables.

	$txn_id = $_REQUEST['txn_id'];
	$address = trim($_REQUEST['address']);
	$amount = floatval($_REQUEST['amount']);
	$status = intval($_REQUEST['status']);
	$tr_id = intval($_REQUEST['tr_id']);
	$CONNECT = mysqli_connect('localhost', $_REQUEST['site_dom'], 'GenRiGeil120AntarktiDa', $_REQUEST['site_dom']);

	if ($status >= 100 || $status == 2) {
		//TODO: Обновляем баланс в таблице пользователей, следом обновляем нашу транзакцию
		if ($_REQUEST['ipn_type'] == 'deposit') {
			mysqli_query($CONNECT, "UPDATE `users`  SET `balance` = `balance` + $amount WHERE `id` = '$_REQUEST[user_id]'");
			mysqli_query($CONNECT, "UPDATE `payment_crypto` SET `status` = 2, `date` = NOW(), `confirm` = '$_REQUEST[confirms]' WHERE `address` = '$address'");
		} else {
			mysqli_query($CONNECT, "UPDATE `cashout_crypto` SET `status` = 2, `tx_id` = '$txn_id', `date` = NOW() WHERE `id` = '$tr_id'");
		}
	} else if ($status < 0) {
		if ($_REQUEST['ipn_type'] == 'deposit') {
			//TODO: Если транзакция есть,  то  обновляем таблицу payment_crypto
			mysqli_query($CONNECT, "UPDATE `payment_crypto` SET `amount` = '$amount', `tx_id` = '$txn_id',
 					`status` = -1, `date` = NOW(), `confirm` = '$_REQUEST[confirms]' WHERE `address` = '$address'");
		} else {
			mysqli_query($CONNECT, "UPDATE `cashout_crypto` SET `amount` = '$amount', `tx_id` = '$txn_id',
 					`status` = -1, `date` = NOW(), `confirm` = '0' WHERE `id` = '$tr_id'");
		}
	} else {
        if ($_REQUEST['site_dom'] && $_REQUEST['user_id']) {
	        if ($_REQUEST['ipn_type'] == 'deposit') {
	        	//TODO: Если транзакция есть,  то  обновляем таблицу payment_crypto
		        mysqli_query($CONNECT, "UPDATE `payment_crypto` SET `amount` = '$amount', `tx_id` = '$txn_id',
 					`status` = 1, `date` = NOW(), `confirm` = '$_REQUEST[confirms]' WHERE `address` = '$address'");
	        } else {
                mail($cp_debug_email, 'CoinPayments IPN Success', 'Списание');
		        mysqli_query($CONNECT, "UPDATE `cashout_crypto` SET `amount` = '$amount', `tx_id` = '$txn_id',
 					`status` = 1, `date` = NOW(), `confirm` = '0' WHERE `id` = '$tr_id'");
	        }
        }
	}
	die('IPN OK');
