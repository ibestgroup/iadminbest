<?php
/**
 * Created by PhpStorm.
 * User: sbondarenko
 * Date: 14.02.2018
 * Time: 17:22
 */
// TODO: Тут подключаем класс для работы с бд и получения основных значений
require_once $_SERVER['DOCUMENT_ROOT']."/classes/Main.php";
Main::getInstance();//Пока вызову инстанс здесь. Но в будущем перенесем в конфиг
set_time_limit(300);

class Multimark {
    public $connect;
    public $height = 99;
    public $overflow;
    public $width;
    public $refer;
    public $parentUser;
    public $id_mark;
    public $ch_po;
    public $ms_prefix;
    public $auto;
    public $ordersTree;
    public $orders;
    public $levelsMas = array();
    public $parentMark;

    public function __construct($refer, $id_mark, $auto = false)
    {
        global $dom;
        $this->connect = mysqli_connect(HOST, USER, PASS, DB);
        $this->refer = $refer;
        $this->id_mark = $id_mark;
        $this->ms_prefix = ($auto)?'_inside':'';
        $this->auto = $auto;

        //Маркетинг из таблицы marks_inside
        if ($this->ms_prefix) {
            $mark = Main::getMarksInside()[$this->id_mark];
        } else {
            $mark = Main::getMarks()[$this->id_mark];
        }
        $this->width = $mark['width'];
        $this->overflow = $mark['overflow'];  //TODO: Здесь будет ед!е!ница, только если маркетинг - 1 ( #youngoldman# Здесь может быть в обоих случаях единица)


        //Родитель из таблицы users
        $this->parentUser = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = $this->refer"));
        //Уровень рефера?
        $check_mark = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `id_user` = $this->refer AND `mark` = '$this->id_mark'"));
        // #youngoldman# Объявил эту переменную, так как когда вставляю $this->parentUser[refer] в SQL запрос он не находит совпадения и $check_mark соответственно всегда равен нулю. И ниже теперь if нормельно заменяется на while.
        $for_check_mark = $this->parentUser['refer'];

        while ( $check_mark == 0 ) {// Если refer нулевого уровня - ищем первого ненулевого выше в структуре.
            $check_mark = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `id_user` = '$for_check_mark' AND `mark` = '$this->id_mark'"));
            $this->parentUser = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = '$for_check_mark'"));
            $for_check_mark = $this->parentUser['refer'];
        }


        $this->refer = $this->parentUser['id'];
    }

    public function overflowMultiOld() {
        global $site_info, $coinsArr;
        $prUserID = $this->parentUser['id'];

        // #youngoldman# В оригинале простой мультимаркетинг возвращает id_user , а автоматический просто id поэтому сделал переменную
        $arVal = 'id_user';
        // #youngoldman# SQL запосы немного разные, можетому вынес в переменную.
        $sql_statement = "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `id`=(SELECT MAX(`id`) FROM `orders_mark` WHERE `id_user` = '$prUserID' AND `mark` = '$this->id_mark')";
        if ($this->auto) {
            $coi = mysqli_num_rows(mysqli_query($this->connect, "select * from orders_mark_inside where id_user = $this->refer and status = 0 and mark = $this->id_mark"));
            if ($coi > 0) {
                $order_inside = mysqli_fetch_assoc(mysqli_query($this->connect, "select * from orders_mark_inside where id_user = $this->refer and status = 0 and mark = $this->id_mark"))['id'];
            } else {
                $order_inside = mysqli_fetch_assoc(mysqli_query($this->connect, "select * from orders_mark_inside where id_user = $this->refer and mark = $this->id_mark"))['id'];
            }
            $arVal = 'id';
            $sql_statement = "SELECT * FROM `orders_mark_inside` WHERE `id_user` = '$prUserID' AND `mark` = '$this->id_mark'";
        }

        if ($this->overflow) {

            $this->ch_po = mysqli_fetch_array(mysqli_query($this->connect, $sql_statement))['id'];
            $referal_num = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$prUserID' AND `mark` = '$this->id_mark' AND `par_or` = '$this->ch_po'"));

            if ( $this->refer == 1 ) {
                if ($site_info['only_one'] == 0 AND $site_info['mark_type'] == 3){
                    $ch = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `id_user` = 1 AND `mark` = '$this->id_mark'"));
                    if ($ch == 1) {$referal_num = $referal_num - 1;}
                } else {
                    $referal_num = $referal_num - 1;
                }
            }
            if ( $referal_num >= $this->width ) {
                $curator = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$prUserID' AND `mark` = '$this->id_mark' AND `par_or` = '$this->ch_po'");

                while ( $change_curator = mysqli_fetch_assoc($curator) ) {
                    $referal_num = mysqli_query($this->connect, "SELECT count(*) FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator[id]'")->fetch_row()[0];
                    if ( $referal_num < $this->width ) {return $change_curator[$arVal];}
                    $curator_1 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator[id]'");

                    while ( $change_curator_1 = mysqli_fetch_assoc($curator_1) ) {
                        $referal_num_1 = mysqli_query($this->connect, "SELECT count(*) FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_1[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_1[id]'")->fetch_row()[0];
                        if ( $referal_num_1 < $this->width ) {return $change_curator_1[$arVal];}
                        $curator_2 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_1[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_1[id]'");

                        while ( $change_curator_2 = mysqli_fetch_assoc($curator_2) ) {
                            $referal_num_2 = mysqli_query($this->connect, "SELECT count(*) FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_2[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_2[id]'")->fetch_row()[0];

                            if ( $referal_num_2 < $this->width ) {return $change_curator_2[$arVal];}
                            $curator_3 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_2[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_2[id]'");

                            while ( $change_curator_3 = mysqli_fetch_assoc($curator_3) ) {
                                $referal_num_3 = mysqli_query($this->connect, "SELECT count(*) FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_3[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_3[id]'")->fetch_row()[0];
                                if ( $referal_num_3 < $this->width ) {return $change_curator_3[$arVal];}
                                $curator_4 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_3[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_3[id]'");

                                while ( $change_curator_4 = mysqli_fetch_assoc($curator_4) ) {
                                    $referal_num_4 = mysqli_query($this->connect, "SELECT count(*) FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_4[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_4[id]'")->fetch_row()[0];
                                    if ( $referal_num_4 < $this->width ) {return $change_curator_4[$arVal];}
                                    $curator_5 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_4[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_4[id]'");

                                    while ( $change_curator_5 = mysqli_fetch_assoc($curator_5) ) {
                                        $referal_num_5 = mysqli_query($this->connect, "SELECT count(*) FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_5[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_5[id]'")->fetch_row()[0];
                                        if ( $referal_num_5 < $this->width ) {return $change_curator_5[$arVal];}
                                        $curator_6 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_5[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_5[id]'");

                                        while ( $change_curator_6 = mysqli_fetch_assoc($curator_6) ) {
                                            $referal_num_6 = mysqli_query($this->connect, "SELECT count(*) FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_6[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_6[id]'")->fetch_row()[0];
                                            if ( $referal_num_6 < $this->width ) {return $change_curator_6[$arVal];}
                                            $curator_7 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_6[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_6[id]'");

                                            while ( $change_curator_7 = mysqli_fetch_assoc($curator_7) ) {
                                                $referal_num_7 = mysqli_query($this->connect, "SELECT count(*) FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_7[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_7[id]'")->fetch_row()[0];
                                                if ( $referal_num_7 < $this->width ) {return $change_curator_7[$arVal];}
                                                $curator_8 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_7[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_7[id]'");

                                                while ( $change_curator_8 = mysqli_fetch_assoc($curator_8) ) {
                                                    $referal_num_8 = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_8[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_8[id]'"));
                                                    if ( $referal_num_8 < $this->width ) {return $change_curator_8[$arVal];}
                                                    $curator_9 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_8[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_8[id]'");

                                                    while ( $change_curator_9 = mysqli_fetch_assoc($curator_9) ) {
                                                        $referal_num_9 = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_9[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_9[id]'"));
                                                        if ( $referal_num_9 < $this->width ) {return $change_curator_9[$arVal];}
                                                        $curator_10 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_9[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_9[id]'");

                                                        while ( $change_curator_10 = mysqli_fetch_assoc($curator_10) ) {
                                                            $referal_num_10 = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_10[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_10[id]'"));
                                                            if ( $referal_num_10 < $this->width ) {return $change_curator_10[$arVal];}
                                                            $curator_11 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_10[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_10[id]'");

                                                            while ( $change_curator_11 = mysqli_fetch_assoc($curator_11) ) {
                                                                $referal_num_11 = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_11[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_11[id]'"));
                                                                if ( $referal_num_11 < $this->width ) {return $change_curator_11[$arVal];}
                                                                $curator_12 = mysqli_query($this->connect, "SELECT id, id_user FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_11[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_11[id]'");

                                                                while ( $change_curator_12 = mysqli_fetch_assoc($curator_12) ) {
                                                                    $referal_num_12 = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$change_curator_12[id_user]' AND `mark` = '$this->id_mark' AND `par_or` = '$change_curator_12[id]'"));
                                                                    if ( $referal_num_12 < $this->width ) {return $change_curator_12[$arVal];}
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                return ($this->auto)?$order_inside:$this->refer;
            }
        } else {
            return ($this->auto)?$order_inside:$this->refer;
        }
    }


    public function overflowMulti() {
        global $site_info, $dom;

    	$orders = array();
	    $res = '';
    	$users =  DB::query("SELECT id, par_or FROM `orders_mark$this->ms_prefix` WHERE `mark` = '$this->id_mark'");
    	while ($user = DB::fetch_array($users)) {
    		$orders[] = $user;
		}
        // #youngoldman# В оригинале простой мультимаркетинг возвращает id_user , а автоматический просто id поэтому сделал переменную
        $arVal = 'id_user';
        // #youngoldman# SQL запосы немного разные, можетому вынес в переменную.
        $sql_statement = "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `id`=(SELECT MAX(`id`) FROM `orders_mark` WHERE `id_user` = '$this->refer' AND `mark` = '$this->id_mark')";
        if ($this->auto) {
            $coi = mysqli_num_rows(mysqli_query($this->connect, "select * from orders_mark_inside where id_user = $this->refer and status = 0 and mark = $this->id_mark"));
            if ($coi > 0) {
                $order_inside = mysqli_fetch_assoc(mysqli_query($this->connect, "select * from orders_mark_inside where id_user = $this->refer and status = 0 and mark = $this->id_mark"))['id'];
            } else {
                $order_inside = mysqli_fetch_assoc(mysqli_query($this->connect, "select * from orders_mark_inside where id_user = $this->refer and mark = $this->id_mark"))['id'];
            }
            $arVal = 'id';
            $sql_statement = "SELECT * FROM `orders_mark_inside` WHERE `id_user` = '$this->refer' AND `mark` = '$this->id_mark'";
        }

        if ($this->overflow) {

            $this->ch_po = mysqli_fetch_array(mysqli_query($this->connect, $sql_statement))['id'];
//            $referal_num = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `parent` = '$this->refer' AND `mark` = '$this->id_mark' AND `par_or` = '$this->ch_po'"));

//            if ($this->refer == 1) {
//                if ($site_info['only_one'] == 0 AND $site_info['mark_type'] >= 3) {
//                    $ch = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark$this->ms_prefix` WHERE `id_user` = 1 AND `mark` = '$this->id_mark'"));
//                    if ($ch == 1) {
//                        $referal_num = $referal_num - 1;
//                    }
//                } else {
//                    $referal_num = $referal_num - 1;
//                }
//            }

            //Строим массив с зависимостями
            foreach ($orders as $key  => $order) {
                $this->orders[$order['par_or']][] = $order['id'];
            }

            //Формируем уровневый массив
            $this->outLevels($this->ch_po);
            //Если кол-во уровней уже большое, то сокращаем их на два последних и ищем место там. выше подниматься
//            if (count($this->levelsMas) > 8) {
//                $startLevel = ;
//            }

            $out = $this->searchParent();

            if (!$this->auto) {
            	return DB::fetch_array(DB::query("SELECT `id_user` FROM `orders_mark` WHERE id = $out"))['id_user'];
			}

            return $out;
        }

        return ($this->auto)?$order_inside:$this->refer;
	}

	function searchParent () {
        for ($i = 1; $i <= count($this->levelsMas); $i++) {
            foreach ($this->levelsMas[$i] as $id => $child) {
                $c = array_search($id, $child);
                if ($c !== false)
                    unset($child[$c]);
                if (count($child) < $this->width) {
                    return $id;
                } else {
                    foreach ($child as $child_id) {
                        if ($id != $child_id) {
                            if (count($this->levelsMas[$i+1][$child_id]) < $this->width)
                                return $child_id;
                        }
                    }
                }
            }
        }
	}

	function outLevels($parent_id = 1, $level = 1) {
        foreach ($this->orders[$parent_id] as $k => $mark) { //Обходим
            if ($mark != 1 && $mark != $parent_id) {
                //Рекурсивно вызываем эту же функцию, но с новым $parent_id и $level
                $this->outLevels($mark, $level + 1);
                $this->levelsMas[$level][$parent_id] = $this->orders[$parent_id];
            } else {
                $this->levelsMas[$level][$parent_id] = $this->orders[$parent_id];
            }
        }
        if (empty($this->orders[$parent_id])) {
            $this->levelsMas[$level][$parent_id] = array();
        }
    }

    function outTree($parent_id = 1) {
        $mas = array();

        foreach ($this->orders[$parent_id] as $mark) { //Обходим
            if ($mark != $parent_id) {
                //Рекурсивно вызываем эту же функцию, но с новым $parent_id
                $mas[$parent_id][$mark] = $this->outTree($mark);
            }
        }
        return $mas;
    }

    public static function dump($res)  {
        ?><pre><?print_r($res)?></pre><?
    }
}
class ActivateMultiMark {
    public $connect;
    public $connectCore;
    public $comission = 10;
    public $partnersPrice = 70;

    public function __construct()
    {
        $this->connect = mysqli_connect(HOST, USER, PASS, DB);
        $this->connectCore = mysqli_connect(HOST, 'mlmone', 'DannaDiD19', 'mlmone');
    }

    public function checkParentBalance() {
        global $dom, $m_curr;
        $idParCore = $this->getWalletParCore();
        if ($idParCore) {
            $db_row_cur = strtolower($m_curr);

            $res = DB::query("SELECT count(*) FROM `partner_balance` WHERE `id_user` = $idParCore", $this->connectCore)->fetch_row()[0];
            if (!$res)
                DB::query("INSERT INTO `partner_balance` VALUES('', '$idParCore', 0, 0, 0, 0, 0, 0, 0, 0, 0)", $this->connectCore);
        }
    }

    public function getWalletParCore($par = 'id') {
        global $site_info;

        $par_core_query = DB::query("SELECT * FROM `users` WHERE `id` = $site_info[id_admin]", $this->connectCore);
        $par_core_ch = DB::getRows($par_core_query);
        $par_core = DB::fetch_array($par_core_query);

        if ( $par_core['refer'] > 1 && $par_core_ch ) {
            return DB::fetch_array(DB::query("SELECT * FROM `users` WHERE `id` = $par_core[refer]", $this->connectCore))[$par];
        } else {
            return 0;
        }
    }

    public function moneyTransfer($id_order, $us_id) {
        global $or_bal;
        $rb = $or_bal['balance'];
        DB::query("UPDATE `orders_mark_inside` SET `balance` = 0 WHERE `id` = $id_order");
        DB::query("UPDATE `orders_mark_inside` SET `status` = 2 WHERE `id` = $id_order");
        DB::query("UPDATE `users`  SET `balance` = balance + $rb WHERE `id` = $us_id");
        MessageSend(1, "Средства были выведены на Ваш баланс");
    }

    public function activate($us_id, $tarif) {
        global $site_info, $row_users, $balance, $cur_ech, $headers_mail, $us, $crypto, $m_curr, $dom;

        $tar = Main::getMarks()[$tarif];;

        $check_mark = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark` WHERE `id_user` = $us_id AND `mark` = $tarif"));
        if ($check_mark) {
            if ($site_info['only_one'] == 1 AND $tar['type'] == 2) {MessageSend(1, 'Вы уже активировали этот тариф. Вы не можете его активировать повторно.', '/profile');}
            $par_mark = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `orders_mark` WHERE `id` = (SELECT MAX(`id`) FROM `orders_mark` WHERE `id_user` = $us_id AND `mark` = $tarif)"));
        } else {
            $par_mark['level'] = 0;
        }

        if ($site_info['series'] == 1) {
            $t_l = $tarif - 1;
            $check_low = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark` WHERE `mark` = $t_l AND `id_user` = $us_id"));
            if (!$check_low AND $tarif != 1) {
                MessageSend(1, 'Для активации этого тарифа, Вам необходимо активировать предыдущий тариф.', '/profile');
            }
        }

        if ( $tar['type'] == 1 AND $check_mark > 0 AND $par_mark['level'] < $tar['level'] ) {
            $par_res[0] = $par_mark['parent'];
        } else {
            if ($check_mark > 0) {
                $check_ref_tar = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark` WHERE `parent` = $us_id AND `par_or` = '$par_mark[id]'"));
                if ($check_ref_tar < $tar['width']) {
                    MessageSend(1, 'Для повторной активации тарифа, у Вас должна быть заполнена первая линия.', '/profile');
                }
                $overflow = new Multimark($us_id, $tarif);
                $par_res[0] = $overflow->overflowMulti(); // Результат ID родителя, который пишется в parent. !!!!!-- Аргумент функции поле refer --!!!!!
            } else {
                $overflow = new Multimark($row_users['refer'], $tarif);
                $par_res[0] = $overflow->overflowMulti(); // Результат ID родителя, который пишется в parent. !!!!!-- Аргумент функции поле refer --!!!!!
            }
        }

        $price_one = [$tar['level1'],$tar['level2'], $tar['level3'], $tar['level4'], $tar['level5'], $tar['level6'], $tar['level7'], $tar['level8']];

        if ($par_mark['level'] < $tar['level'] AND $check_mark > 0 AND $tar['type'] == 1) {
            $last_order = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `orders_mark` WHERE `id` = $par_mark[par_or]"));
        } else {
            $last_order = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `orders_mark` WHERE `id` = (SELECT MAX(`id`) FROM `orders_mark` WHERE `id_user` = $par_res[0] AND `mark` = $tarif)"));
        }

        $l_o[0] = $last_order['id'];

        // Задание массивов для парентов и цен уровней.
        $p_i = 0;
        $this->checkParentBalance();
        $idParentCore = $this->getWalletParCore();
        while ( $p_i < 8 ) {
            $par[$p_i] = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `orders_mark` WHERE `id_user` = $par_res[$p_i] AND `id` = $l_o[$p_i]"));
            $par_res[$p_i+1] = $par[$p_i]['parent'];
            $l_o[$p_i+1] = $par[$p_i]['par_or'];
            $level[$p_i] = round($price_one[$p_i]/10, 2, PHP_ROUND_HALF_DOWN)*9;
            $level_my[$p_i] = round($price_one[$p_i]/10, 2, PHP_ROUND_HALF_DOWN);
            if ($idParentCore) {
                $level_par_cor[$p_i] = round($level_my[$p_i]/10, 2, PHP_ROUND_HALF_DOWN)*3;
                $level_my[$p_i] = round($level_my[$p_i]/10, 2, PHP_ROUND_HALF_DOWN)*7;
            } else {
                $level_my[$p_i] = round($level_my[$p_i], 2, PHP_ROUND_HALF_DOWN);
            }
            $p_i++;
        }

        if ( $tar['type'] == 2 ) {

            $type_follow = 1; //Что за переменная?
            $sum = 0;
            $price_all = 0;
            while ( $sum < $tar['level'] ) {
                $price_all = $price_all + $price_one[$sum];
                $sum++;
            }

            if ($crypto) {
                $price_all = in_crypto($price_all);
            }

            if ( $balance < $price_all  ) {
                MessageSend(1, "Недостаточно средств на балансе. Необходимо $price_all <i class=\"fa fa-$cur_ech\" aria-hidden=\"true\"></i>. Ваш баланс: $balance <i class=\"fa fa-$cur_ech\" aria-hidden=\"true\"></i>.", '/profile');
            } else {
                $balance_res = (float)$balance - $price_all;
                $i = 0;

                mysqli_query($this->connect, "UPDATE `users`  SET `balance` = $balance_res WHERE `id` = '$us_id'");
                while ( $i < $tar['level']) {

                    $orders = mysqli_query($this->connect, "SELECT * FROM `buy_order` WHERE `id_user` = $us_id");
                    $num_order = mysqli_num_rows($orders) + 1;
                    $m_p = $par[$i];
                    $mark_parent = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = '$m_p[id_user]'"));
                    $to_us = $par[$i]['id_user'];
                    $to_us_res = $par[$i]['id_user'];

                    $parent_fetch = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = '$to_us_res'"));
                    $parent_fetch_res = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = '$to_us'"));

                    if ($site_info['type_direct'] == 1) {
                        if (check_address($mark_parent['last_active'])) {
                            $wallet_send = $mark_parent['payeer_wallet'];
                        } else {
                            $ll = $i + 1;
                            if ($site_info['type_direct'] == 1 AND $site_info['quan_day_active'] != 0) {
                                $wallet_send = direct_user($par[$i]['par_or'], $ll, 2, $tar['type']);
                                $to_us = direct_user($par[$i]['par_or'],$ll,1, $tar['type']);
                            } else {
                                $wallet_send = $admin['payeer_wallet'];
                            }

                            if ($to_us == $to_us_res) {
                                $to = $parent_fetch['email'];
                                $subject = 'Начисление по тарифу '. $tar['name'] .' на сайте '. $site_info['name'];
                                $message = message_mail($subject, 'Здравствуйте, <b>' . $parent_fetch['login'] . '</b>.<br> На сайте <b>' . $site_info['name'] . '</b>, Вам поступили средства, от пользователя <b>' . $us . '</b> за уровень <b>' . $type_follow . '</b> в тарифе <b>' . $tar['name'] . '</b>.<br><br> Средства ушли на Ваш счет Payeer.<br><br> С уважением, команда ' . $site_info['name']);
                                mail($to, $subject, $message, $headers_mail);
                            } else {
                                $to = $parent_fetch['email'];
                                $subject = 'Сработал обход на сайте ' . $site_info['name'];
                                $message = message_mail($subject, 'Здравствуйте, <b>' . $parent_fetch['login'] . '</b>.<br> На сайте <b>' . $site_info['name'] . '</b>, Вам не поступили средства, от пользователя <b>' . $us . '</b> в тарифе <b>' . $tar['name'] . '</b> так как Вы не заходили на сайт более ' . $site_info['quan_day_active'] . '. Если Вы не хотите чтобы это повторилось, пожалуйста авторизуйтесь на сайте '.$site_info['name'].'.<br><br> Средства ушли на счет участника <b>' . $parent_fetch_res['email'] . '</b>.<br><br> С уважением, команда ' . $site_info['name']);
                                mail($to, $subject, $message, $headers_mail);
                                $to = $parent_fetch_res['email'];
                                $subject = 'Вы получили обход на сайте ' . $site_info['name'];
                                $message = message_mail($subject, 'Здравствуйте, <b>' . $parent_fetch_res['login'] . '</b>.<br> На сайте <b>' . $site_info['name'] . '</b>, Вам поступили средства за другого участника (от пользователя <b>' . $us . '</b> за уровень <b>' . $type_follow . '</b> в тарифе <b>' . $tar['name'] . '</b>) так как пользователь <b>' . $parent_fetch['login'] . '</b>(куратор пользователя ' . $us . ') не заходил на сайт более ' . $site_info['quan_day_active'] . ', средства обошли его стороной, и поступили на Ваш кошелек.<br><br>С уважением, команда ' . $site_info['name']);
                                mail($to, $subject, $message, $headers_mail);
                            }
                        }
                    } else {
                        $wallet_send = $mark_parent['payeer_wallet'];
                    }

                    /*
                     * Биткоин(ранее)
                     * Перевод на баланс вышестоящего участника
                     */

                    mysqli_query($this->connect, "UPDATE `users` SET `balance` = `balance` + $level[$i] WHERE id = $to_us");
                    mysqli_query($this->connect, "INSERT INTO `payments_order` VALUES('', '$us_id', '$to_us', '$wallet_send', '$level[$i]', '$num_order', 0, NOW(), 0, $tarif)");

                    $db_row_cur = strtolower($m_curr);

                    if ($idParentCore) {
                        mysqli_query($this->connectCore, "UPDATE `partner_balance` SET `$db_row_cur` = `$db_row_cur` + $level_par_cor[$i] WHERE `id_user` = '$idParentCore'");
                    }

                    mysqli_query($this->connectCore, "UPDATE `partner_balance` SET `$db_row_cur` = `$db_row_cur` + $level_my[$i] WHERE `id_user` = 1");
                    $today = date('Y-m-d');
                    mysqli_query($this->connectCore, "UPDATE `profit` SET `$db_row_cur` = `$db_row_cur` + $level_my[$i] WHERE `date` = '$today'");
                    $i++;
                }

                mysqli_query($this->connect, "INSERT INTO `buy_order` VALUES('', $us_id, $num_order, $type_follow, NOW())");
                mysqli_query($this->connect, "UPDATE `users`  SET `type_follow` = $type_follow WHERE `id` = '$us_id'");

                $paror = $par[0]['id'];
                mysqli_query($this->connect, "INSERT INTO `orders_mark` VALUES('', $us_id, $par_res[0], $tarif, $type_follow, $paror, NOW())");

                MessageSend(2, 'Ваш аккаунт активирован.<br>'.share_btns(), '/profile');
            }
        } elseif ( $tar['type'] == 1 ) {
            $type_follow = $par_mark['level'] + 1;

            if ($type_follow > $tar['level']) {$type_follow = 1; $par_mark['level'] = 0;}

            $price_all = $price_one[$par_mark['level']];

            if ($crypto) {
                $price_all = in_crypto($price_all);
            }

            if ( $balance < $price_all ) {
                MessageSend(1, "Недостаточно средств на балансе. Необходимо $price_all <i class=\"fa fa - $cur_ech\" aria-hidden=\"true\"></i>. Ваш баланс: $balance <i class=\"fa fa - $cur_ech\" aria-hidden=\"true\"></i>.", '/profile');
            } else {
                $balance_res = (float)$balance - $price_all;
                $i = $par_mark['level'];
                $i_l = $par_mark['level'] + 1;

                mysqli_query($this->connect, "UPDATE `users` SET `balance` = $balance_res WHERE id = $us_id");

                $orders = mysqli_query($this->connect, "SELECT * FROM `buy_order` WHERE `id_user` = $us_id");
                $num_order = mysqli_num_rows($orders) + 1;
                $m_p = $par[$i];
                $mark_parent = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = '$m_p[id_user]'"));
                $to_us = $par[$i]['id_user'];
                $to_us_res = $par[$i]['id_user'];

                $parent_fetch = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = '$to_us_res'"));
                $parent_fetch_res = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = '$to_us'"));

                if ($site_info['type_direct'] == 1) {
                    if ($par[$i]['level'] < $i_l) {
                        $ll = $i + 1;
                        if ($site_info['type_direct'] == 1 AND $site_info['quan_day_active'] != 0) {
                            $wallet_send = direct_user($par[$i]['par_or'], $ll, 2,$tar['type']);
                            $to_us = direct_user($par[$i]['par_or'],$ll,1,$tar['type']);
                        } else {
                            $wallet_send = $admin['payeer_wallet'];
                        }

                        if ($to_us == $to_us_res) {
                            $to = $parent_fetch['email'];
                            $subject = 'Начисление по тарифу '. $tar['name'] .' на сайте '. $site_info['name'];
                            $message = message_mail($subject, 'Здравствуйте, <b>' . $parent_fetch['login'] . '</b>.<br> На сайте <b>' . $site_info['name'] . '</b>, Вам поступили средства, от пользователя <b>' . $us . '</b> за уровень <b>' . $type_follow . '</b> в тарифе <b>' . $tar['name'] . '</b>.<br><br> Средства ушли на Ваш счет Payeer.<br><br> С уважением, команда ' . $site_info['name']);
                            mail($to, $subject, $message, $headers_mail);
                        } else {
                            $to = $parent_fetch['email'];
                            $subject = 'Сработал обход на сайте ' . $site_info['name'];
                            $message = message_mail($subject, 'Здравствуйте, <b>' . $parent_fetch['login'] . '</b>.<br> На сайте <b>' . $site_info['name'] . '</b>, Вам не поступили средства, от пользователя <b>' . $us . '</b> за уровень <b>' . $type_follow . '</b> в тарифе <b>' . $tar['name'] . '</b> так как у Вас еще не приобретен этот уровень или Вы не заходили на сайт более ' . $site_info['quan_day_active'] . '. Если Вы не хотите чтобы это повторилось, пожалуйста, повысьте Ваш уровень или авторизуйтесь на сайте '.$site_info['name'].'.<br><br> Средства ушли на счет участника <b>' . $parent_fetch_res['email'] . '</b>.<br><br> С уважением, команда ' . $site_info['name']);
                            mail($to, $subject, $message, $headers_mail);
                            $to = $parent_fetch_res['email'];
                            $subject = 'Вы получили обход на сайте ' . $site_info['name'];
                            $message = message_mail($subject, 'Здравствуйте, <b>' . $parent_fetch_res['login'] . '</b>.<br> На сайте <b>' . $site_info['name'] . '</b>, Вам поступили средства за другого участника (от пользователя <b>' . $us . '</b> за уровень <b>' . $type_follow . '</b> в тарифе <b>' . $tar['name'] . '</b>) так как пользователь <b>' . $parent_fetch['login'] . '</b>(куратор пользователя ' . $us . ') еще не приобрел этот уровень или не заходил на сайт более ' . $site_info['quan_day_active'] . ', средства обошли его стороной, и поступили на Ваш кошелек.<br><br>С уважением, команда ' . $site_info['name']);
                            mail($to, $subject, $message, $headers_mail);
                        }
                    } else {
                        $wallet_send = $mark_parent['payeer_wallet'];
                    }
                } else {
                    if ( $par[$i]['level'] < $i_l ) {
                        $wallet_send = $admin['payeer_wallet'];
                    } else {
                        $wallet_send = $mark_parent['payeer_wallet'];
                    }
                }
                $paror = $par[0]['id'];
                /*
                 * Биткоин(ранее)
                 * Перевод на баланс вышестоящего участника
                 */
                $balance_parent = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = $to_us"));
                $res_balance_parent = $balance_parent['balance'] + $level[$i];

                $db_row_cur = strtolower($m_curr);

                if ($idParentCore) {
                    mysqli_query($this->connectCore, "UPDATE `partner_balance` SET `$db_row_cur` = `$db_row_cur` + $level_par_cor[$i] WHERE `id_user` = '$idParentCore'");
                }

                mysqli_query($this->connectCore, "UPDATE `partner_balance` SET `$db_row_cur` = `$db_row_cur` + $level_my[$i] WHERE `id_user` = 1");
                $today = date('Y-m-d');
                mysqli_query($this->connectCore, "UPDATE `profit` SET `$db_row_cur` = `$db_row_cur` + $level_my[$i] WHERE `date` = '$today'");

                mysqli_query($this->connect, "UPDATE `users` SET `balance` = `balance` + $level[$i] WHERE id = $to_us");
                mysqli_query($this->connect, "INSERT INTO `payments_order` VALUES('', '$us_id', '$to_us', '$wallet_send', '$level[$i]', '$num_order', 0, NOW(), 0, $tarif)");
                mysqli_query($this->connect, "UPDATE `users`  SET `type_follow` = '1' WHERE `id` = '$us_id'");
                mysqli_query($this->connect, "INSERT INTO `buy_order` VALUES('', $us_id, $num_order, $type_follow, NOW())");
                if (!$check_mark OR $type_follow == 1) {
                    mysqli_query($this->connect, "INSERT INTO `orders_mark` VALUES('', $us_id, $par_res[0], $tarif, $type_follow, $paror, NOW())");
                } else {
                    mysqli_query($this->connect, "UPDATE `orders_mark`  SET `level` = $type_follow WHERE `id_user` = '$us_id' AND `mark` = $tarif AND `id` > 9");
                }

                MessageSend(2, "Поздравляем, Вы активировали ".$i_l." уровень.<br>".share_btns(), '/profile');

            }
        } else {
            MessageSend(1, "Что-то пошло не так.", '/profile');
        }
    }

    public function activateAutoOld($us_id, $tarif, $id_order, $showMsg = true, $markReinvest = 0) {
//    	if ($_SERVER['REMOTE_ADDR'] != '109.106.142.213' && $_SERVER['REMOTE_ADDR'] != '109.172.54.238' && $_SERVER['REMOTE_ADDR'] != '85.113.140.66' && $_SERVER['REMOTE_ADDR'] != '188.234.144.165') {
//		    MessageSend(1, 'Функция временно не доступна');
//		}
        global $site_info, $row_users, $balance, $cur_ech, $headers_mail, $us, $or_bal, $crypto, $m_curr, $dom;

        $startTarif = $tarif;

        if ($tarif < 1 OR $tarif > 9) {
            if ($showMsg) {
                MessageSend(1, 'Такой тариф не существует', '/profile');
            }
        } else {
            $tar = Main::getMarksInside()[$tarif];
        }

        //Уточнить по передаче id order
        if (!empty($id_order)) {
            $or_bal = mysqli_fetch_assoc(mysqli_query($this->connect, "select * from orders_mark_inside where id = '$id_order'"));
            if ($or_bal['status'] == 0) {
                if ($showMsg) {
                    MessageSend(1, 'Этот тариф еще не заполнен');
                }
            }

            $mark_reinvest = Main::getMarksInside()[$or_bal['mark']];
            $tarifReinvest = Main::getMarksInside()[$mark_reinvest['reinvest']];
            $tarif = $mark_reinvest['reinvest'];
            if ($markReinvest == 1) {
                $tarif = 1;
            }
//			elseif ($markReinvest == 2) {
//				$tarif = $mark_reinvest['reinvest_self'];
//			}
            $tar = Main::getMarksInside()[$tarif];
            if ($or_bal['id_user'] != $us_id) {
                if ($showMsg)
                    MessageSend(1, 'Что-то пошло не так', '/profile');
            }
        } else {
            $mark_reinvest = Main::getMarksInside()[$tarif];
            $tarifReinvest = Main::getMarksInside()[$tarif];
        }

        if ($tarif == 0) {
            $this->moneyTransfer($id_order, $us_id);
        }


        if (!$tar['status'] && $showMsg) {
            MessageSend(1, 'Такой тариф не существует', '/profile');
        }

        $admin = Main::getAdmin();

        $check_mark = mysqli_num_rows(mysqli_query($this->connect, "SELECT * FROM `orders_mark_inside` WHERE `id_user` = $us_id AND `mark` = $tarif"));
        if ($check_mark) {
            $par_mark = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `orders_mark_inside` WHERE `id_user` = $us_id AND `mark` = $tarif AND `status` = 0"));
        } else {
            $par_mark['level'] = 0;
        }

        $overflow = new Multimark($us_id, $tarif, true);
	    $paror_res = $overflow->overflowMulti();

        $par_res_fetch = mysqli_fetch_assoc(mysqli_query($this->connect, "select * from orders_mark_inside where id = $paror_res"));
        $par_res[0] = $par_res_fetch['id_user'];

        $price_one = [$tar['level1'],$tar['level2'], $tar['level3'], $tar['level4'], $tar['level5'], $tar['level6'], $tar['level7'], $tar['level8']];

//		echo $paror_res;
//		die();

        $l_o[0] = $paror_res;

        // Задание массивов для парентов и цен уровней.
        $p_i = 0;
        $this->checkParentBalance();
        $idParentCore = $this->getWalletParCore();
        while ( $p_i < 8 ) {
            $par[$p_i] = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `orders_mark_inside` WHERE `id` = $l_o[$p_i]"));
            $par_res[$p_i+1] = $par[$p_i]['parent'];
            $l_o[$p_i+1] = $par[$p_i]['par_or'];
            $level[$p_i] = round($price_one[$p_i]/10, 2, PHP_ROUND_HALF_DOWN)*9;
            $level_my[$p_i] = round($price_one[$p_i]/10, 2, PHP_ROUND_HALF_DOWN);
            if ($idParentCore) {
                $level_par_cor[$p_i] = round($level_my[$p_i]/10, 2, PHP_ROUND_HALF_DOWN)*3;
                $level_my[$p_i] = round($level_my[$p_i]/10, 2, PHP_ROUND_HALF_DOWN)*7;
            }
            $p_i++;
        }

        $type_follow = 1;
        $sum = 0;
        $price_all = 0;
        while ( $sum < $tar['level'] ) {
            $price_all = $price_all + $price_one[$sum];
            $sum++;
        }
        $price_all = $price_all + $tar['to_admin'] + $tar['to_par'];

        if ($crypto) {
            $price_all = in_crypto($price_all);
        }

        if (!empty($id_order)) {
            $balance = $or_bal['balance'];
        }

        if ( $balance < $price_all  ) {
            if ($showMsg)
                MessageSend(1, "Недостаточно средств на балансе. Необходимо $price_all <i class=\"fa fa-$cur_ech\" aria-hidden=\"true\"></i>. Ваш баланс: $balance <i class=\"fa fa-$cur_ech\" aria-hidden=\"true\"></i>.", '/profile');
        } else {

            $reinvest_first = 0;
            if ($mark_reinvest['reinvest_first'] && !$markReinvest && $id_order) {
            }

            $balance_res = (float)$balance - $price_all - $reinvest_first;
            $i = 0;


            if (!empty($id_order)) {
                if (!$markReinvest) {
//                	mysqli_query($this->connect, "UPDATE `orders_mark_inside`  SET `balance` = 0 WHERE `id` = '$id_order'");
                    mysqli_query($this->connect, "UPDATE `users` SET `balance` = balance + $balance_res WHERE `id` = $us_id");
                }

                mysqli_query($this->connect, "UPDATE `orders_mark_inside`  SET `status` = 2 WHERE `id` = '$id_order'");
            } else {
                mysqli_query($this->connect, "UPDATE `users` SET `balance` = $balance_res WHERE `id` = '$us_id'");
            }


            mysqli_query($this->connect, "INSERT INTO `orders_mark_inside` VALUES('', $us_id, $par_res[0], $tarif, $type_follow, 0, 0, 0, $paror_res, NOW())");

            while ( $i < $tar['level']) {

                $orders = mysqli_query($this->connect, "SELECT * FROM `buy_order` WHERE `id_user` = $us_id");
                $num_order = mysqli_num_rows($orders) + 1;
                $m_p = $par[$i];
                $mark_parent = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = '$m_p[id_user]'"));
                $to_us = $par[$i]['id_user'];
                $to_us_res = $par[$i]['id_user'];

                $parent_fetch = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = '$to_us_res'"));
                $parent_fetch_res = mysqli_fetch_assoc(mysqli_query($this->connect, "SELECT * FROM `users` WHERE `id` = '$to_us'"));

                if ($site_info['type_direct'] == 1) {
                    if (!check_address($mark_parent['last_active'])) {
                        $ll = $i + 1;
                        if ($site_info['type_direct'] == 1 AND $site_info['quan_day_active'] != 0) {
                            $to_us = direct_user($par[$i]['par_or'],$ll,1, $tar['type']);
                        }

                        //Шлем email
                        if ($to_us == $to_us_res) {
                            $to = $parent_fetch['email'];
                            $subject = 'Начисление по тарифу '. $tar['name'] .' на сайте '. $site_info['name'];
                            $message = message_mail($subject, 'Здравствуйте, <b>' . $parent_fetch['login'] . '</b>.<br> На сайте <b>' . $site_info['name'] . '</b>, Вам поступили средства, от пользователя <b>' . $us . '</b> за уровень <b>' . $type_follow . '</b> в тарифе <b>' . $tar['name'] . '</b>.<br><br> Средства ушли на Ваш счет Payeer.<br><br> С уважением, команда ' . $site_info['name']);
                            mail($to, $subject, $message, $headers_mail);
                        } else {
                            $to = $parent_fetch['email'];
                            $subject = 'Сработал обход на сайте ' . $site_info['name'];
                            $message = message_mail($subject, 'Здравствуйте, <b>' . $parent_fetch['login'] . '</b>.<br> На сайте <b>' . $site_info['name'] . '</b>, Вам не поступили средства, от пользователя <b>' . $us . '</b> в тарифе <b>' . $tar['name'] . '</b> так как Вы не заходили на сайт более ' . $site_info['quan_day_active'] . '. Если Вы не хотите чтобы это повторилось, пожалуйста авторизуйтесь на сайте '.$site_info['name'].'.<br><br> Средства ушли на счет участника <b>' . $parent_fetch_res['email'] . '</b>.<br><br> С уважением, команда ' . $site_info['name']);
                            mail($to, $subject, $message, $headers_mail);
                            $to = $parent_fetch_res['email'];
                            $subject = 'Вы получили обход на сайте ' . $site_info['name'];
                            $message = message_mail($subject, 'Здравствуйте, <b>' . $parent_fetch_res['login'] . '</b>.<br> На сайте <b>' . $site_info['name'] . '</b>, Вам поступили средства за другого участника (от пользователя <b>' . $us . '</b> за уровень <b>' . $type_follow . '</b> в тарифе <b>' . $tar['name'] . '</b>) так как пользователь <b>' . $parent_fetch['login'] . '</b>(куратор пользователя ' . $us . ') не заходил на сайт более ' . $site_info['quan_day_active'] . ', средства обошли его стороной, и поступили на Ваш кошелек.<br><br>С уважением, команда ' . $site_info['name']);
                            mail($to, $subject, $message, $headers_mail);
                        }
                    }
                }

                /*
                 * Биткоин(ранее)
                 * Перевод на баланс вышестоящего участника
                 */
                $to_res = $l_o[$i];
                $balance_order = mysqli_fetch_assoc(mysqli_query($this->connect, "select * from orders_mark_inside where id = $to_res"));
                $markProfit = mark_profit($tarif, 1);
                echo "overflow multi";
                ?><pre><?print_r($paror_res)?></pre><?
                echo "Вывод to_res";
                ?><pre><?print_r($to_res)?></pre><?
                $res_balance_parent = $balance_order['balance'] + $level[$i];
                $result_user = $balance_order['id_user'];

                mysqli_query($this->connect, "UPDATE `orders_mark_inside` SET `balance` = $res_balance_parent WHERE id = $to_res");
                mysqli_query($this->connect, "UPDATE `orders_mark_inside` SET `pay_people` = pay_people + 1 WHERE id = $to_res");
                if ($markProfit <= $res_balance_parent) {
                    mysqli_query($this->connect, "UPDATE `orders_mark_inside` SET `status` = 1 WHERE id = $to_res");
                }

                mysqli_query($this->connect, "INSERT INTO `payments_order` VALUES('', '$us_id', '$result_user', '$l_o[$i]', '$level[$i]', '$num_order', 0, NOW(), 0, $tarif)");

                $afterUpBalance = mysqli_fetch_assoc(mysqli_query($this->connect, "select * from orders_mark_inside where id = $to_res"));
                echo 'вывод $afterUpBalance';
                ?>
                <pre><?print_r($afterUpBalance)?></pre>
                <?echo 'вывод $markProfit';?>
                <pre><?print_r($markProfit)?></pre>
                <?echo 'Вывод $tarifReinvest'?>
                <pre><?print_r($tarifReinvest)?></pre>
                <?echo 'Вывод $markReinvest'?>
                <pre><?print_r($markReinvest)?></pre>

                <?
                if (!$markReinvest && $tarifReinvest['reinvest_first'] && $afterUpBalance['balance'] >= $markProfit) {
                    $reinvestFirst = new ActivateMultiMark();
                    $reinvestFirst->activateAuto($afterUpBalance['id_user'], 1, $afterUpBalance['id'], false, 1);
                }
                ?>
                <?echo 'Вывод $markReinvest'?>
                <pre><?print_r($markReinvest)?></pre>
                <?
                //TODO: Автореинвест
                if ($afterUpBalance['balance'] >= $markProfit && $afterUpBalance['status'] < 2 && !$markReinvest) {
                    echo "Зашел в автореинвест на третий<br>";
                    $autoReinvest = new ActivateMultiMark();
                    $autoReinvest->activateAuto($afterUpBalance['id_user'], $afterUpBalance['mark'], $afterUpBalance['id']);
                }

                $db_row_cur = strtolower($m_curr);

                if ($idParentCore) {
                    mysqli_query($this->connectCore, "UPDATE `partner_balance` SET `$db_row_cur` = $db_row_cur + $level_par_cor[$i] WHERE `id_user` = '$idParentCore'");
                }

                mysqli_query($this->connectCore, "UPDATE `partner_balance` SET `$db_row_cur` = $db_row_cur + $level_my[$i] WHERE `id_user` = 1");
                $today = date('Y-m-d');
                mysqli_query($this->connectCore, "UPDATE `profit` SET `$db_row_cur` = `$db_row_cur` + $level_my[$i] WHERE `date` = '$today'");


                $i++;
            }

            //Расчет админу паренту
            $par_direct = number_format($tar['to_par'] * 0.9,2);
            $admin_direct = number_format($tar['to_admin'] * 0.9,2);


            //Админу
            if($admin_direct != 0) {
                $balance_admin = mysqli_fetch_assoc(mysqli_query($this->connect, "select * from users where id = 1"));
                $res_balance_admin = $balance_admin['balance'] + $admin_direct;
                mysqli_query($this->connect, "UPDATE `users` SET `balance` = $res_balance_admin WHERE id = 1");
	            if ($dom == 'testtest') {
		            mysqli_query($this->connect, "INSERT INTO `payments_order` VALUES('', '$us_id', 1, '$l_o[$i]', '$admin_direct', '$num_order', 0, NOW(), 0, $tarif)");
	            }
            }

            //Паренту
            if($par_direct != 0) {
                $balance_par = mysqli_fetch_assoc(mysqli_query($this->connect, "select * from users where id = '$row_users[refer]'"));
                $res_balance_par = $balance_par['balance'] + $par_direct;
                mysqli_query($this->connect, "UPDATE `users` SET `balance` = $res_balance_par WHERE id = '$row_users[refer]'");
	            if ($dom == 'testtest') {
		            mysqli_query($this->connect, "INSERT INTO `payments_order` VALUES('', '$us_id', '$row_users[refer]', '$l_o[$i]', '$par_direct', '$num_order', 0, NOW(), 0, $tarif)");
	            }
            }

            mysqli_query($this->connect, "INSERT INTO `buy_order` VALUES('', $us_id, $num_order, $type_follow, NOW())");
            mysqli_query($this->connect, "UPDATE `users`  SET `type_follow` = $type_follow WHERE `id` = '$us_id'");

            if (!empty($id_order) && !$markReinvest)
                mysqli_query($this->connect, "UPDATE `orders_mark_inside`  SET `balance` = 0 WHERE `id` = '$id_order'");

            $paror = $paror_res;

            if ($showMsg) {
            	//TODO: Пока хз почему происходит ебала с банасами, временно подчищаем те у кого статус два и баланс не нулевой
            	$this->ClearBalance();

	            MessageSend(2, 'Ваш аккаунт активирован.<br>'.share_btns(), '/profile');
            }
        }
    }
    public function ClearBalance() {
        $query = DB::query("SELECT id FROM `orders_mark_inside` WHERE status = 2 AND balance != 0");
        while ($res = DB::fetch_array($query)) {
        	$id = $res['id'];
	        DB::query("UPDATE `orders_mark_inside`  SET `balance` = 0 WHERE `id` = '$id'");
		}
    }

    public function activateAuto($userId, $tarifId, $reinvest = false, $firstReinvest = false, $fromTarif = false) {
        global $site_info, $curecho, $crypto, $m_curr, $dom;
        $db_row_cur = strtolower($m_curr);
        $today = date('Y-m-d');
        $fromTarif = Main::getMarks()[$fromTarif];

        /**
         * 00 - админу
         * 000 - Паренту
         * 0000 - Перевод с тарифа на баланс
         */

        /**
         * Комисссия конструктору
         */
        $half = 100 - $this->comission;
        $partnerHalf = 100 - $this->partnersPrice;

        if ($tarifId < 1 OR $tarifId > 9) {
            MessageSend(1, 'Такой тариф не существует', '/profile');
        } else {
            $tarif = Main::getMarksInside()[$tarifId];

            if (!$tarif['status']) {
                MessageSend(1, 'Такой тариф не существует', '/profile');
            }
        }

        $user = DB::query("SELECT * FROM `users` WHERE `id` = $userId")->fetch_assoc();

        /**
         * Получаем ко-во активаций для определения overflow
         */
        $userMarksCount = DB::query("SELECT count(*) FROM `orders_mark_inside` WHERE mark = $tarifId")->fetch_row()[0];

        $overflow = $site_info['overflow'];

        if (!$userMarksCount && $overflow == 0) {
            $overflow = 1;
        }

        switch($overflow) {
            case 1:
                $overflowObj = new Multimark($user['refer'], $tarifId, true);
                $overflowRes = $overflowObj->overflowMulti();
                break;
            case 2:
                $overflowObj = new Multimark(1, $tarifId, true);
                $overflowRes = $overflowObj->overflowMulti();
                break;
            default:
                $overflowObj = new Multimark($userId, $tarifId, true);
                $overflowRes = $overflowObj->overflowMulti();
        }

        $priceLevels = [$tarif['level1'],$tarif['level2'], $tarif['level3'], $tarif['level4'], $tarif['level5'], $tarif['level6'], $tarif['level7'], $tarif['level8']];

        $levels = $tarif['level'];

        $massive[] = DB::query("SELECT * FROM `orders_mark_inside` WHERE id = $overflowRes")->fetch_assoc();

        //Переводим на баланс
        if ($reinvest && !$firstReinvest) {
//            $priceFirst = 0;
//            if ($fromTarif['reinvest_first'])
//                $priceFirst = mark_enter(1);
            $priceEnter = mark_enter($tarifId);
            $tarifBalance = DB::query("SELECT `balance` FROM `orders_mark_inside` WHERE id = $reinvest")->fetch_assoc()['balance'];

            $toUserSum = $tarifBalance - $priceEnter;

//            echo '<pre>';
//            print_r($tarif);
//            echo '</pre>';
//
//            die($toUserSum." ".$tarifBalance." ".$priceEnter." ".$priceFirst);


            if ($crypto) {
                $toUserSum = in_crypto($toUserSum);
            }
            DB::query("INSERT INTO `payments_order` VALUES('', '9999999', '$userId', '0000', '$toUserSum', '0', 0, NOW(), 0, $tarifId)");
            DB::query("UPDATE `orders_mark_inside` SET `status` = 2 WHERE `id` = $reinvest");
            DB::query("UPDATE `users`  SET `balance` = `balance` + $toUserSum WHERE `id` = $userId");
        }

        for ($i = 0; $i < $levels; $i++) {
            $mark = $massive[$i];

            /**
             * Комиссия конструктору
             */
            $mark['full_price'] = $priceLevels[$i];
            $mark['price'] = round($priceLevels[$i]/100, 2, PHP_ROUND_HALF_DOWN) * $half;
            $mark['comission'] = round($priceLevels[$i]/100, 2, PHP_ROUND_HALF_DOWN) * $this->comission;
            /**
             * Партнерские
             */
            if ($this->getWalletParCore()) {
                $mark['partner_price'] = round($mark['comission'] / 100, 2, PHP_ROUND_HALF_DOWN) * $partnerHalf;
                $mark['comission'] = round($mark['comission'] / 100, 2, PHP_ROUND_HALF_DOWN) * $this->partnersPrice;
            }

            $massive[$i] = $mark;

            $refer = $massive[$i]['par_or'];

            /**
             * Если итерация последняя не записываем
             */
            if ($i + 1 < $levels)
                $massive[] = DB::query("SELECT * FROM `orders_mark_inside` WHERE id = $refer")->fetch_assoc();
        }

        $markProfit = mark_profit($tarifId, 1);
        $markEnter = mark_enter($tarifId);

//        if ($tarif['reinvest_first'])
//            $markEnter += mark_enter(1);

        $balanceForWithdrow = $user['balance'];
        if ($reinvest) {
            $balanceForWithdrow = DB::query("SELECT * FROM `orders_mark_inside` WHERE id = $reinvest")->fetch_assoc()['balance'];
        }

        if ($balanceForWithdrow < $markEnter) {
            MessageSend(1, "Недостаточно средств на балансе. Необходимо ".$markEnter.$curecho.". Ваш баланс: ".$user['balance']." ".$curecho.".", "/profile");
        }

        //Активация маркетинга
        $mark = $massive[0];
        if ($reinvest)
            DB::query("INSERT INTO `orders_mark_inside` VALUES('', $userId, $mark[id_user], $tarifId, 1, 0, 0, 0, $mark[id], NOW(), 1)");
        else
            DB::query("INSERT INTO `orders_mark_inside` VALUES('', $userId, $mark[id_user], $tarifId, 1, 0, 0, 0, $mark[id], NOW(), 0)");

        foreach ($massive as $i => $mark) {
            //Списываем с баланса
            if ($reinvest) {
                DB::query("UPDATE `orders_mark_inside` SET `balance` = `balance` - $mark[full_price] WHERE `id` = $reinvest");
            } else {
                DB::query("UPDATE `users` SET `balance` = `balance` - $mark[full_price] WHERE `id` = $userId");
            }

            //Прибавляем на баланс
            DB::query("UPDATE `orders_mark_inside` SET `balance` = `balance` + $mark[price], `pay_people` = `pay_people` + 1 WHERE `id` = $mark[id]");
            DB::query("INSERT INTO `payments_order` VALUES('', '$userId', $mark[id_user], $mark[id], '$mark[price]', '0', 0, NOW(), 0, $tarifId)");

            DB::query("UPDATE `profit` SET `$db_row_cur` = `$db_row_cur` + $mark[comission] WHERE `date` = '$today'", $this->connectCore);


            $mark['status'] = DB::query("select * from orders_mark_inside where id = $mark[id]")->fetch_assoc()['status'];
            if ($tarif['reinvest_first'] && !$firstReinvest && (($mark['balance'] + $mark['price']) >= $markProfit) && $mark['status'] < 2) {
                $reinvestObj = new ActivateMultiMark();
                $reinvestObj->activateAuto($mark['id_user'], 1, $mark['id'], true);
            }

            $sumBalance =  round($mark['balance'] + $mark['price'], 2);
            $markProfit =  round($markProfit, 2);

            //Автореинвест
            if ($tarif['reinvest'] && ($sumBalance >= $markProfit) && !$firstReinvest && $mark['status'] < 2) {

                $reinvestObj = new ActivateMultiMark();
                $reinvestObj->activateAuto($mark['id_user'], $tarif['reinvest'], $mark['id'], false, $tarifId);
            }

	        if (!$tarif['reinvest'] && ($sumBalance >= $markProfit)) {
		        $tarifBalance = DB::query("SELECT `balance` FROM `orders_mark_inside` WHERE id = $mark[id]")->fetch_assoc()['balance'];

		        DB::query("INSERT INTO `payments_order` VALUES('', '9999999', $mark[id_user], '0000', '$tarifBalance', '0', 0, NOW(), 0, $tarifId)");
		        DB::query("UPDATE `orders_mark_inside` SET `status` = 2 WHERE `id` = $mark[id]");
		        DB::query("UPDATE `users`  SET `balance` = `balance` + $tarifBalance WHERE `id` = $mark[id_user]");
	        }
        }

        if ($tarif['to_admin']) {
            $toAdminFee = round($tarif['to_admin']/100, 2, PHP_ROUND_HALF_DOWN) * $this->comission;
            $toAdminSum = round($tarif['to_admin']/100, 2, PHP_ROUND_HALF_DOWN) * $half;
            DB::query("UPDATE `users` SET `balance` = `balance` + $toAdminSum WHERE `id` = 1");
            DB::query("INSERT INTO `payments_order` VALUES('', '$userId', 1, '00', '$toAdminSum', '0', 0, NOW(), 0, $tarifId)");
            DB::query("UPDATE `profit` SET `$db_row_cur` = `$db_row_cur` + $toAdminFee WHERE `date` = '$today'", $this->connectCore);
        }
        if ($tarif['to_par']) {
            $toParFee = round($tarif['to_par']/100, 2, PHP_ROUND_HALF_DOWN) * $this->comission;
            $toParSum = round($tarif['to_par']/100, 2, PHP_ROUND_HALF_DOWN) * $half;
            DB::query("UPDATE `users` SET `balance` = `balance` + $toParSum WHERE `id` = $user[refer]");
            DB::query("INSERT INTO `payments_order` VALUES('', '$userId', '$user[refer]', '000', '$toParSum', '0', 0, NOW(), 0, $tarifId)");
            DB::query("UPDATE `profit` SET `$db_row_cur` = `$db_row_cur` + $toParFee WHERE `date` = '$today'", $this->connectCore);
        }

        $fee = $tarif['to_admin'] + $tarif['to_par'];

        if (!$firstReinvest && !$reinvest) {
            mysqli_query($this->connect, "UPDATE `users` SET `type_follow` = 1 WHERE `id` = $userId");

            DB::query("UPDATE `users` SET `balance` = `balance` - $fee WHERE `id` = $userId");
            //TODO: Подчищаем закрытые тарифы
            $this->ClearBalance();
            MessageSend(2, 'Ваш аккаунт активирован.<br>'.share_btns(), '/profile');
        }
    }

    public static function updateBalance($us, $sum) {
        DB::query("UPDATE `users` SET `balance` = `balance` + $sum WHERE `id` = $us");
    }
}
