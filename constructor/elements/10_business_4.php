<?include 'layout/header.php';?>
	<div class="pixfort_business_10" id="section_business_4">

		<div class="bundle_section pix_builder_bg">
			<div class="container">
				<div class="sixteen columns ">
					<h3 class="what_st editContent">What You Are Going To Have</h3>
					<p class="great_st editContent">great prices for great tutorials</p>
					<div class="one-third column alpha">
						<div class="onethird_st">
							<div class="icons_style"> <img src="/constructor/elements/images/10_business/icon-1.png" class="icon_st" alt=""></div>
							<p class="T1 editContent">High Quality Tutorials</p>
							<p class="T2 editContent">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p>
						</div>
					</div>
					<div class="one-third column ">
						<div class="onethird_st">
							<div class="icons_style"> <img src="/constructor/elements/images/10_business/icon-2.png" class="icon_st" alt=""></div>

							<p class="T1 editContent">Best Bundle Ever</p>
							<p class="T2 editContent">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p>
						</div>
					</div>
					<div class="one-third column omega">
						<div class="onethird_st">
							<div class="icons_style"> <img src="/constructor/elements/images/10_business/icon-3.png" class="icon_st" alt=""></div>

							<p class="T1 editContent">Latest New Offers</p>
							<p class="T2 editContent">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

