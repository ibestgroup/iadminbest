<?include 'layout/header.php';?>
	<div class="pixfort_business_10" id="section_business_5">
		<div class="why_section pix_builder_bg">
			<div class="container">
				<div class="nine columns alpha">
					<h3 class="title_st editContent">Why To Choose Us?</h3>
					<p class="txt_st editContent">we are the leading company in web tutorials with over 500 handpicked videos will make your dreams come to real with awseome projects.</p>
					<div class="video_style">
						<iframe src="https://player.vimeo.com/video/102732914?title=0&amp;byline=0&amp;portrait=0&amp;color=5b9a68&amp;wmode=opaque"  >
						</iframe>
					</div>
				</div>
				<div class="seven columns omega">
					<div class="text_input">
						<div class="headtext_style"><span class="editContent"><span class="pix_text">Get Full Course For FREE!</span></span></div>
						<p class="subtext_style editContent">Just Subscribe to get full access to our new tutorial + all attachments files.</p>
						<div class="contact_st">
							<form id="contact_form" pix-confirm="hidden_pix_10">
								<div id="result"></div>
								<input type="text" name="name" id="name" placeholder="Enter Your Full Name" class="pix_text">
								<input type="text" name="number" id="number" placeholder="Enter Your Phone Number" class="pix_text">
								<input type="email" name="email" id="email" placeholder="Enter Your Email" class="pix_text">
								<button class="subscribe_btn pix_text" id="subscribe_btn_10">
									<span class="editContent">SUBSCRIBE TO DOWNLOAD</span>
								</button>
							</form>
		  				</div>
						<h6 class="note_st editContent">*Note: Bundle ends in two weeks from now.</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="hidden_pix_10" class="confirm_page confirm_page_10 pix_builder_bg">
		<div class="pixfort_business_10">

			<div class="confirm_header"><span class="editContent"><span class="pix_text">Thank You Very Much!</span></span></div>
			<div class="confirm_text">
				<span class="editContent"><span class="pix_text">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</span></span>
			</div>
			<div class="center_text padding_bottom_60">
                <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
                    <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
                </ul>
            </div>
		</div>
	</div>
<?include 'layout/footer.php';?>

