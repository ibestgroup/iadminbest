<?include 'layout/header.php';?>
	<div class="pixfort_subscribe_12" id="section_subscribe_2_1">
		<div class="page_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="context_style pix_builder_bg">
						<div class="title_style">
							<span class="editContent"><span class="pix_text">We Are Coming Soon</span></span>
						</div>
						<div class="subtitle_style">
							<span class="editContent"><span class="pix_text">Just Subscribe to get full access to our new tutorial + all attachments files.</span></span>
						</div>
						<div class="email_subscribe  subscribe_btn" >
							<form id="contact_form" pix-confirm="hidden_pix_12">
								<div id="result"></div>
								<input type="email" name="email" id="email" placeholder="Enter Your Email" class="pix_text">
								<button class="subscribe_btn pix_text" id="subscribe_btn_12">
									<span class="editContent">SUBSCRIBE</span>
								</button>
							</form>
							<div class="clearfix"></div>
							<div class="note_st"><span class="editContent"><span class="pix_text">*Note: Bundle ends in two weeks from now.</span></span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="hidden_pix_12" class="confirm_page confirm_page_12 pix_builder_bg">
		<div class="pixfort_subscribe_12">
			<div class="confirm_logo"><span class="conf_img"><img src="/constructor/elements/images/12_subscribe/confirm1.png"></span></div>
			<div class="confirm_header"><span class="editContent"><span class="pix_text">THANK YOU FOR SUBSCRIPTION</span></span></div>
			<div class="confirm_text">
				<span class="editContent"><span class="pix_text">Our service is astonishingly thin and light.</span></span>
			</div>

			<div class="center_text padding_bottom_60">
                <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
                    <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
                </ul>
            </div>
		</div>
	</div>
<?include 'layout/footer.php';?>

