<?include 'layout/header.php';?>
	<div class="pixfort_gym_13 " id="section_gym_2">
		<div class="page_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
		            <div class="text_page">
		                    <h1 class="title"><span class="editContent"><span class="pix_text">THE SHAPE OF YOUR LIFE. PERIOD.</span></span></h1>
		                    <h3 class="subtitle"><span class="editContent"><span class="pix_text">High intensity workouts. Training plans. Personalized to you.</span></span></h3>
		            </div>
		            <div class="one_link">
		                <span class="start_btn ">
		                        <a class="slow_fade pix_text" href="#"><span class="editContent">Start your workout now</span></a>
		                </span>
		            </div>
		            <p class="note_st"><span class="editContent">*Note: Bundle ends in two weeks from now.</span></p>
		            <div class="arrow_st"><a href="#" id="a_press"><img src="/constructor/elements/images/13_gym/arrow.png"  alt=""></a></div>
		        </div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

