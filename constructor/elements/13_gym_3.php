<?include 'layout/header.php';?>
	<div class="pixfort_gym_13" id="section_gym_3">
		<div class="logos_sect pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
		           <div class="text_logos"><span class="editContent"><span class="pix_text">SOME OF THE WORLD'S MOST RESPECTED COMPANIES TRUST FLATPACK</span></span></div>
		           <div class="padd_updown">
		               <div class="one-third column  alpha"> <img src="/constructor/elements/images/13_gym/logo1.png" class="logo_width" alt=""> </div>
		               <div class="one-third column       "> <img src="/constructor/elements/images/13_gym/logo2.png" class="logo_width" alt=""> </div>
		               <div class="one-third column  omega"> <img src="/constructor/elements/images/13_gym/logo3.png" class="logo_width" alt=""> </div>
		           </div>
		        </div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

