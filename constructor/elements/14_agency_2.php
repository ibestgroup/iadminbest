<?include 'layout/header.php';?>
    <div class="pixfort_agency_14"  id="section_agency_2">
        <div class="agency_st pix_builder_bg">
        	<div class="container">
        		<div class="sixteen columns">
                   <div class="top_padding">
                        <div class="one-third column alpha">
                            <div class="part_st">
                                <div class="img_st pix_builder_bg2"><img src="/constructor/elements/images/14_agency/icon-1.png"  alt=""></div>
                				<div class="text_agency_style">
                					<span class="editContent"><p class="agency_st">High Quality Tutorials</p></span>
                					<span class="editContent"><p class="agency_text">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p></span>
                				</div>
                            </div>
                        </div>
                        <div class="one-third column ">
                            <div class="part_st">
            					<div class="img_st pix_builder_bg2"><img src="/constructor/elements/images/14_agency/icon-2.png"   alt=""></div>
                				<div class="text_agency_style">
                					<span class="editContent"><p class="agency_st">Best Bundle Ever</p></span>
                					<span class="editContent"><p class="agency_text">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p></span>
                				</div>
                            </div>
                        </div>
                        <div class="one-third column  omega">
                            <div class="part_st">
            					<div class="img_st pix_builder_bg2"><img src="/constructor/elements/images/14_agency/icon-3.png"   alt=""></div>
                				<div class="text_agency_style">
                					<span class="editContent"><p class="agency_st">Latest New Offers</p></span>
                					<span class="editContent"><p class="agency_text">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p></span>
                				</div>
                            </div>
                        </div>

                    </div>
            	</div>
        	</div>
        </div>
    </div>
<?include 'layout/footer.php';?>

