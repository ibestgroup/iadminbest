<?include 'layout/header.php';?>
		<div class="pixfort_agency_14" id="section_agency_4">
			<div class="envato_unbounce pix_builder_bg">
				<div class="container">
					<div class="sixteen columns">
						<div class="L1_style"><span class="editContent"><span class="pix_text">What our awesome clients say</span></span></div>
						<div class="L2_style"><span class="editContent"><span class="pix_text">great words from great people</span></span></div>
						<div class="eight columns  alpha ">
							<div class="box_style pix_builder_bg">
								<div class="b_style">
									<img src="/constructor/elements/images/testimonials/envato-logo.png" class="logo_style" alt="">
								</div>
								<div class="txt_style">
									<div class="inner_txt_style">
										<span class="editContent"><p class="txt">Im a web designer, you guys are very inspiring I wish to see more work from maybe more freebies.</p></span>
										<span class="pix_testi_bottom"><img src="/constructor/elements/images/14_agency/stars.original.png" class="star_st" alt="">
											<span class="env_st"><span class="editContent"><span class="pix_text">via Envato.com</span></span></span></span>
										</div>
									</div>
								</div>
							</div>
							<div class="eight columns omega">
								<div class="box_style pix_builder_bg">
									<div class="b_style">
										<img src="/constructor/elements/images/testimonials/unboune-logo.png" class="logo_style" alt="">
									</div>
									<div class="txt_style">
										<div class="inner_txt_style">
											<span class="editContent"><p class="txt">Great service with fast and relible support The design work and detail put into themes are great.</p></span>
											<span class="pix_testi_bottom"><img src="/constructor/elements/images/14_agency/stars.original.png" class="star_st" alt="">
											<span class="env_st"><span class="editContent"><span class="pix_text">via Unbounce.com</span></span></span></span>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

