<?include 'layout/header.php';?>
	<div class="pixfort_party_15" id="section_party_1">
		<div class="page_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<img src="/constructor/elements/images/15_party/Intro.png" class="img_width"  alt="">
					<div class="text_page">
						<span class="editContent"><h1 class="title">OCT.31TH @ CENTRAL PARK</h1></span>
						<p class="subtitle editContent">CELEBRATE THIS EVENT WITH FLATPACK BUNDLE <br><br>
							AND DON'T MISS THE SHOW!</p>
						</div>
						<div class="center_text padding_bottom_60">
                <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
                    <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
                </ul>
            </div>
							</div>
				</div>
			</div>
		</div>
<?include 'layout/footer.php';?>
