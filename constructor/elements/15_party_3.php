<?include 'layout/header.php';?>
    <div class="pixfort_party_15" id="section_party_3">
        <div class="subscribe pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="text_input pix_builder_bg">
                        <div class="headtext_style">
                            <span class="editContent">
                                <span class="pix_text">
                                    GET YOUR TICKET NOW!
                                </span>
                            </span>
                        </div>
                        <div class="segment pix_builder_bg"></div>
                        <span class="editContent">
                            <p class="subtext_style">
                                Just Subscribe to get full access to our new tutorial + all attachments files.
                            </p>
                        </span>
                        <div class="contact_st">
                            <form id="contact_form" pix-confirm="hidden_pix_15">
                                <div id="result"></div>
                                <input type="text" name="name" id="name" placeholder="Enter Your Full Name" class="pix_text">
                                <input type="email" name="email" id="email" placeholder="Enter Your Email" class="pix_text">
                                <button class="subscribe_btn pix_text" id="submit_btn_15">
                                    <span class="editContent">SUBSCRIBE TO DOWNLOAD</span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <div id="hidden_pix_15" class="confirm_page confirm_page_15 pix_builder_bg">
        <div class="pixfort_party_15">
            <div class="confirm_header">
                <span class="editContent">
                    <span class="pix_text">
                        Thank You Very Much!
                    </span>
                </span>
            </div>
            <div class="confirm_text">
                <span class="editContent">
                    <span class="pix_text">
                        Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.
                    </span>
                </span>
            </div>
            <div class="center_text padding_bottom_60">
                <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
                    <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
                </ul>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>

