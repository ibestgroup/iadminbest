<?include 'layout/header.php';?>
	<div class="pixfort_medical_3 pix_builder_bg" id="section_pixfort_medical_3">
		<div class="container">
			<div class="three columns alpha">
				<div class="text_image_center">
					<span><img src="/constructor/elements/images/16_medical/icon-1.png" bottom-center></span>
				</div>
			</div>
			<div class="five columns">
				<div class="content_div">
					<h4 class="editContent">Second Headline</h4>
					<p class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor.</p>

					<a class="medical_text_button slow_fade editContent pix_text" href="#">LEARN MORE</a>
				</div>
			</div>
			<div class="three columns">
				<div class="text_image_center">
					<span><img src="/constructor/elements/images/16_medical/icon-2.png"></span>
				</div>
			</div>
			<div class="five columns omega">
				<div class="content_div">
					<h4 class="editContent">Fourth Headline</h4>
					<p class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor.</p>

					<a class="medical_text_button slow_fade editContent pix_text" href="#">LEARN MORE</a>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

