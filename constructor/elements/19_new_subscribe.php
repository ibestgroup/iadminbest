<?include 'layout/header.php';?>
<div class="pixfort_subscribe_19">
	<div class="page_style pix_builder_bg">
		<div class="container">
			<div class="sixteen columns ">
				<div class="coming_soon pix_builder_bg">
					<div class="bande"></div>
					<div class="marge">
						<div class="logo_div">
							<img src="/constructor/elements/images/main/logo.png" class="img_st" alt="">
						</div>
						<div class="social_div">
							<a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/yt.png" alt=""></a>
                            <a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/twitter.png" alt=""></a>
                            <a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/facebook.png" alt=""></a>
							<div class="htext_style">
	                            <span class="editContent">
	                                <span class="pix_text">Stay connected</span>
	                            </span>
	                        </div>
						</div>
						<div class="clearfix"></div>
						<div class="main_content">
							<h1 class="title editContent">We Are Coming Soon</h1>
							<p class="subtitle editContent">Just Subscribe to get full access to our new tutorial + all attachments files.</p>

							<div class="contact_st">
								<form id="contact_form" pix-confirm="hidden_pix_19">
									<div id="result"></div>
									<input type="email" name="email" id="email" placeholder="Enter Your Email" class="pix_text" required>
									<button class="subscribe_btn espace pix_text" id="subscribe_btn_19">
										<span class="editContent">SUBSCRIBE NOW</span>
									</button>
								</form>
							</div>
							<p class="note_st editContent">*Note: Bundle ends in two weeks from now.</p>
						</div>
					</div>
					<div class="foot_st pix_builder_bg">
						<span class="rights_st editContent"> All rights reserved Copyright &copy; 2015 FLATPACK by
							<span class="pixfort_st">PixFort</span>
						</span>
						<div class="soc_icons pix_builder_bg">
						<ul class="bottom-icons">
                            <li><a class="pi pixicon-facebook2 white" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-twitter2 white" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-instagram white" href="#fakelink"></a></li>
                        </ul>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="hidden_pix_19" class="confirm_page confirm_page_19 pix_builder_bg">
<div class="pixfort_subscribe_19">
	<div class="confirm_logo"><span class="conf_img"><img src="/constructor/elements/images/12_subscribe/confirm1.png"></span></div>
	<div class="confirm_header"><span class="editContent"><span class="pix_text">THANK YOU FOR SUBSCRIPTION</span></span></div>
	<div class="confirm_text">
		<span class="editContent"><span class="pix_text">Our service is astonishingly thin and light.</span></span>
	</div>
	<div class="center_text padding_bottom_60">
        <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
            <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
            <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
            <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
        </ul>
    </div>
</div>
</div>
<?include 'layout/footer.php';?>

