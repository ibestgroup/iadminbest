<?include 'layout/header.php';?>
    <div class="pixfort_normal_1" id="section_normal_1">
        <div class="header_style pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="one-third column alpha">
                        <img src="/constructor/elements/images/1_normal/logo.png" alt="">
                    </div>
                    <div class="two-thirds column omega">
                            <a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/yt.png" alt=""></a>
                            <a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/twitter.png" alt=""></a>
                            <a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/facebook.png" alt=""></a>
      			       <div class="htext_style">
                            <span class="editContent">
                                <span class="pix_text">Stay connected</span>
                            </span>
                        </div>
                    </div>
  		        </div>
            </div><!-- container -->
        </div>
    </div>
<?include 'layout/footer.php';?>
