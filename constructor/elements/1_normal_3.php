<?include 'layout/header.php';?>
    <div class="pixfort_normal_1" id="section_normal_3">	
        <div class="adv_st pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="one-third column onethird_style alpha">
                        <span class="circle slow_fade pix_builder_bg2">
                            <img src="/constructor/elements/images/1_normal/1.png" alt="">
                        </span>
                        <div class="comment_style">
                            <span class="editContent">
                                <span class="c1_style">
                                    <span class="pix_text">+1500</span>
                                </span>
                            </span>
                            <span class="editContent">
                                <span class="c2_style">
                                    <span class="pix_text">High quality video tutorials.</span>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="one-third column onethird_style alpha">
                        <span class="circle slow_fade pix_builder_bg2">
                            <img src="/constructor/elements/images/1_normal/2.png" alt="">
                        </span>
                        <div class="comment_style">
                            <span class="editContent">
                                <span class="c1_style">
                                    <span class="pix_text">+40</span>
                                </span>
                            </span>
                            <span class="editContent">
                                <span class="c2_style">
                                    <span class="pix_text">Days Free trial demo subscribtion.</span>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="one-third column onethird_style alpha">
                        <span class="circle slow_fade pix_builder_bg2">
                            <img src="/constructor/elements/images/1_normal/3.png" alt="">
                        </span>
                        <div class="comment_style">
                            <span class="editContent">
                                <span class="c1_style">
                                    <span class="pix_text">+75</span>
                                </span>
                            </span>
                            <span class="editContent">
                                <span class="c2_style">
                                    <span class="pix_text">New books from store.</span>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
