<?include 'layout/header.php';?>
    <div class="pixfort_normal_1 creative_features" id="section_normal_3">  
        <div class="adv_st pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="one-third column onethird_style  alpha">
                        <span class="slow_fade creative_circle pix_builder_bg2">
                            <span class="pi pixicon-heart3 creative_c_icon" id="ui-id-23"></span>
                        </span>
                        <div class="comment_style">
                            <span class="editContent"><span class="c1_style2"><span class="pix_text">New Features</span></span></span><br>
                            <span class="editContent"><span class="c2_style2"><span class="pix_text">Free trial demo.</span></span></span>
                        </div>
                    </div>
                    <div class="one-third column onethird_style alpha">
                        <span class="slow_fade 2 creative_circle pix_builder_bg2">
                            <span class="pi pixicon-music creative_c_icon" id="ui-id-23"></span>
                        </span>
                        <div class="comment_style">
                            <span class="editContent"><span class="c1_style2"><span class="pix_text">High Quality Pages<br></span></span></span>
                            <span class="editContent"><span class="c2_style2"><span class="pix_text">Free trial demo.</span></span></span>
                        </div>
                    </div>
                    <div class="one-third column onethird_style alpha">
                        <span class="slow_fade 2 creative_circle pix_builder_bg2">
                            <span class="pi pixicon-params creative_c_icon" id="ui-id-23"></span>
                        </span>
                        <div class="comment_style">
                            <span class="editContent"><span class="c1_style2"><span class="pix_text">Super Useful</span></span></span><br>
                            <span class="editContent"><span class="c2_style2"><span class="pix_text">Save your time.</span></span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
