<?include 'layout/header.php';?>
        <div class="big_padding pix_builder_bg dark" id="section_normal_4_1">
            <div class="container">
                <div class="sixteen columns">
                    <div class="eight columns alpha margin_bottom_30">
                            <span class="bold_text big_text blue_text editContent">GET FREE SUPPORT</span>
                            <p class="big_title bold_text editContent">Get The Most Amazing Builder!</p>
                            <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            <a href="#" class="pix_button btn_normal pix_button_flat blue_bg bold_text">
                                <i class="pi pixicon-download"></i> 
                                <span>GET SOFTWARE</span>
                            </a> 

                        </div>
                    <div class="eight columns omega pix_container">
                        <img src="/iadminbest/constructor/elements/images/main/software-img-3.png" class="img_style" alt="">
                    </div>
                </div>
	       </div>
        </div>
<?include 'layout/footer.php';?>
