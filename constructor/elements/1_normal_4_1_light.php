<?include 'layout/header.php';?>
        <div class="big_padding pix_builder_bg dark cta_bg" id="section_normal_4_1">
            <div class="container">
                <div class="sixteen columns">
                    <div class="twelve columns alpha margin_bottom_30">
                        <div class="title_70 light_blue margin_bottom">
                            <span class="pi pixicon-download2"></span>
                        </div>
                            <p class="big_title editContent"><strong>Download Software From Themeforest Today!</strong></p>
                            <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            <a href="#" class="active_bg_open pix_button btn_normal btn_big pix_button_flat bold_text blue_bg " name="fff">
                                <i class="pi pixicon-stack-2"></i> 
                                <span>DOWNLOAD FLATPACK</span>
                            </a> 

                        </div>
                </div>
	       </div>
        </div>
<?include 'layout/footer.php';?>
