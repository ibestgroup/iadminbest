<?include 'layout/header.php';?>
    <div class="pixfort_normal_1"  id="section_normal_4_2">
        <div class="m6_style pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="eight columns  alpha">
                        <img src="/constructor/elements/images/1_normal/domains-that-never-sleep.png" class="img_style" alt="">
                    </div>
                    <div class="eight columns  omega">
                        <div class="gtext_style">
                            <p class="t1_style editContent">Bring it to life again with us</p>
                            <p class="t2_style editContent">Our service is astonishingly thin and light.</p>
                            <p class="t3_style editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
