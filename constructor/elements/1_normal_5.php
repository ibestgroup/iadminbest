<?include 'layout/header.php';?>
    <div class="pixfort_normal_1"  id="section_normal_5">
        <div class="logos_style pix_builder_bg">
            <div class="container">
                <div class="sixteen columns pad_bot">
                    <div class="titres">
                        <span class="L1_style">
                            <span class="editContent pix_text">Great people trusted our services</span>
                        </span><br>
                        <span class="L2_style">
                            <span class="editContent pix_text">great words from great people</span>
                        </span>
                    </div>
                    <div class="four columns logos_st alpha">
                        <img src="/constructor/elements/images/1_normal/logo_1.png"  alt="">
                    </div>
                    <div class="four columns logos_st">
                        <img src="/constructor/elements/images/1_normal/logo_2.png" alt="">
                    </div>
                    <div class="four columns logos_st">
                        <img src="/constructor/elements/images/1_normal/logo_3.png"  alt="">
                    </div>
                    <div class="four columns logos_st omega">
                        <img src="/constructor/elements/images/1_normal/logo_4.png"  alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
