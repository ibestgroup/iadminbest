<?include 'layout/header.php';?>
    <div class="pixfort_normal_1" id="section_normal_6">
        <div class="envato_unbounce pix_builder_bg">
            <div class="container">
                <h3 class="L1_style">
                    <span class="editContent pix_text">What our awesome clients say</span>
                </h3>
                <h5 class="L2_style">
                    <span class="editContent pix_text">great words from great people</span>
                </h5>
                <div class="eight columns stbox alpha">
                    <div class="box_1 pix_builder_bg">
                        <div class="b_style">
                            <img src="/constructor/elements/images/testimonials/envato-logo.png" class="logo_style" alt="">
                       </div>
                       <div class="txt_style">
                            <p class="txt editContent">Im a web designer, you guys are very inspiring I wish to see more work from maybe more freebies.</p>
                            <span class="stars_span">
                                <img src="/constructor/elements/images/1_normal/stars.original.png" alt="">
                                <span class="env_st editContent ">
                                    <span class="pix_text">via Envato.com</span>
                                </span>
                            </span>
                       </div>
                   </div>
                </div>
                <div class="eight columns stbox omega">
                    <div class="box_2 pix_builder_bg">
                        <div class="b_style">
                            <img src="/constructor/elements/images/testimonials/unboune-logo.png" class="logo_style" alt="">
                       </div>
                       <div class="txt_style">
                            <p class="txt editContent">Great service with fast and relible support The design work and detail put into themes are great.</p>
                            <span class="stars_span">
                                <img src="/constructor/elements/images/1_normal/stars.original.png" alt="">
                                <span class="env_st editContent ">
                                    <span class="pix_text">via pixfort.com</span>
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
