<?include 'layout/header.php';?>
    <div class="pixfort_normal_1" id="section_normal_8">
        <div  class="foot_st">
            <div class="container ">
                <div class="seven columns alpha ">
                    <span class="rights_st"> All rights reserved Copyright &copy; 2014 FLATPACK by
                        <span class="pixfort_st">PixFort</span>
                    </span>
                </div>
                <div class="nine columns omega">
                    <div class="socbuttons">
                        <div class="soc_icons">
                            <ul class="bottom-icons">
                                <li><a class="pi pixicon-facebook2 normal_gray" href="#fakelink"></a></li>
                                <li><a class="pi pixicon-twitter2 normal_gray" href="#fakelink"></a></li>
                                <li><a class="pi pixicon-instagram normal_gray" href="#fakelink"></a></li>
                            </ul>
                        </div>
                        <div class="likes_st">Your likes &amp; share makes us happy!</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
