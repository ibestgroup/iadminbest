<?include 'layout/header.php';?>
		<div class="pixfort_gym_13 construction " id="section_gym_5">
			<div class="join_us_section pix_builder_bg">
				<div class="container">
					<div class="sixteen columns">
						<div class="ten columns alpha area_1">
							<span class="const_title"><span class="editContent"><span class="pix_text">Lorem ipsum dolor sit amet consectetur!</span></span></span>
							<p><span class="editContent">Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur Lorem ipsum dolor sit amet consectetur.</span>
							</p>
						</div>

						<div class="six columns omega">
							<div class="pix_form_area">
								<div class="substyle pix_builder_bg">
									<div class="title-style"><span class="editContent"><span class="pix_text">Get A FREE Quote!</span></span></div>
									<br>
									<div class="text-style">
										<span class="editContent"><span class="pix_text">Lorem ipsum dolor sit amet consectetur.</span></span>
									</div><br>
									<div class="clearfix"></div>
									<form id="contact_form" pix-confirm="hidden_pix_13">
										<div id="result"></div>
										<input type="text" name="name" id="name" placeholder="Your Full Name" class="pix_text">
										<input type="email" name="email" id="email" placeholder="Your Email" class="pix_text">
										<input type="text" name="number" id="number" placeholder="Your Phone Number" class="pix_text">
										<span class="send_btn">
											<button id="submit_btn_13" class="slow_fade pix_text">
												<span class="editContent">Send Information</span>
											</button>
										</span>
									</form>
                      <div class="clearfix"></div>
                  </div>
                  <div class="note_contact pix_builder_bg"><span class="editContent"><span class="pix_text">*Note: Bundle ends in two weeks from now.</span></span></div>
              </div>
          </div>
      </div>
  </div>
</div>
</div>
<div id="hidden_pix_13" class="confirm_page confirm_page_13 construction pix_builder_bg">
	<div class="pixfort_gym_13 ">
		<div class="confirm_header">
			<span class="editContent">
				<span class="pix_text">
					Thank You Very Much!
				</span>
			</span>
		</div>
		<div class="confirm_text">
			<span class="editContent">
				<span class="pix_text">
					Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.
				</span>
			</span>
		</div>
		<div class="center_text padding_bottom_60">
			<ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
				<li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
				<li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
				<li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
			</ul>
		</div>
	</div>
</div>
<?include 'layout/footer.php';?>

