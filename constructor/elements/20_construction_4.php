<?include 'layout/header.php';?>
    <div class="normal_padding pixfort_gym_13 construction pix_builder_bg" id="section_gym_4">
    <div class="gym_life">
    	<div class="container">
    		<div class="sixteen columns gym_life">
               <div class="">
                    <div class="one-third column alpha">
                        <div class="part_st">
                            <div class="img_gym_st2"><img src="/constructor/elements/images/20_construction/r1.jpg" class="logo_style" alt=""></div>
            				<div class="text_gym_style">
            					<p class="gym_st"><span class="editContent">Welcome to Gym</span></p>
            					<p class="gym_text"><span class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</span></p>
            				</div>

                            <span class="link_3_btn editContent"><a class="slow_fade pix_text" href="#">Join Now</a></span>
                        </div>
                    </div>
                    <div class="one-third column ">
                        <div class="part_st">
        					<div class="img_gym_st2"><img src="/constructor/elements/images/20_construction/r2.jpg" class="logo_style"  alt=""></div>
            				<div class="text_gym_style">
            					<p class="gym_st"><span class="editContent">Fitness Activities</span></p>
            					<p class="gym_text"><span class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</span></p>
            				</div>
                            <span class="link_3_btn editContent"><a class="slow_fade pix_text" href="#">Join Now</a></span>
                        </div>
                    </div>
                    <div class="one-third column  omega">
                        <div class="part_st">
        					<div class="img_gym_st2"><img src="/constructor/elements/images/20_construction/r3.jpg" class="logo_style"  alt=""></div>
            				<div class="text_gym_style">
            					<p class="gym_st"><span class="editContent">High quality Staff</span></p>
            					<p class="gym_text"><span class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</span></p>
            				</div>
                            <span class="link_3_btn editContent">
                                    <a class="slow_fade pix_text" href="#">Join Now</a>
                            </span>
                        </div>
                    </div>
                </div>
        	</div>
    	</div>
    </div>
    </div>
<?include 'layout/footer.php';?>

