<?include 'layout/header.php';?>
		<div class="pixfort_pix_1 construction" id="section_testimonials_1">
			<div class="tesi_style pix_builder_bg">
				<div class="container">
					<div class="sixteen columns">
						<img src="/constructor/elements/images/main/light-normal.png" alt="" class="q-icon">
						<ul class="testimonials ticker_fade">
							<!-- First Testimonial -->
							<li>
								<p class="quote-text editContent">
									<!-- Change to your text here -->
									Great service with fast and reliable support. The design work and
									detail put into these themes has been recognized by my customers.
								</p>
								<div class="quote-dots">. . .</div>
								<div class="quote-title editContent pix_text">
									<!-- Change title Here -->
									Envato
								</div>
								<div class="quote-link editContent pix_text">
									<!-- Change Link Here -->
									via envato.com
								</div>

							</li>
							<!-- End of First Testimonial -->
							<!-- Second Testimonial -->
							<li>
								<p class="quote-text editContent">
									<!-- Change to your text here -->
									Im a web designer, you guys are very inspiring. I wish to see more work from you, maybe more freebies.
								</p>
								<div class="quote-dots">. . .</div>
								<div class="quote-title editContent pix_text">
									<!-- Change title Here -->
									Unbounce
								</div>
								<div class="quote-link editContent pix_text">
									<!-- Change Link Here -->
									via unbounce.com
								</div>

							</li>
							<!-- End of Second Testimonial -->
							<!-- Third Testimonial -->
							<li>
								<p class="quote-text editContent">
									<!-- Change to your text here -->
									I’m a happy man. This is fantastic code, and it’s only just getting on its feet! love FlatBox alot :)
								</p>
								<div class="quote-dots">. . .</div>
								<div class="quote-title editContent pix_text">
									<!-- Change title Here -->
									PixFort
								</div>
								<div class="quote-link editContent pix_text">
									<!-- Change Link Here -->
									via pixfort.com
								</div>
							</li>
							<!-- End of Third Testimonial -->
						</ul>
					</div>
				</div><!-- container -->
			</div>
		</div>
<?include 'layout/footer.php';?>

