<?php include 'layout/header.php';?>


		<div class="light_gray_bg small_padding pix_builder_bg" id="section_text_2">
            <div class="container">
                <div class="eight columns alpha mobile_center">
                    <img class="extra_small_padding padding_left_10" src="/constructor/elements/images/main/logo.png" alt="">
                </div>
                <div class="eight columns omega right_text mobile_center">
                    <a href="#" class="pix_button btn_normal small_wide_button small_bold bold_text orange_bg ">
                        <span>Buy Tickets</span>
                    </a> 
                </div>
            </div>
        </div>


        <div class="" id="section_header_1">
            <div class="header_style header_nav_1 pix_builder_bg">
                <div class="container">
                    <div class="sixteen columns firas2">
                        <nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
                            <div class="containerss">                            
                                <div class="navbar-header">
                                    <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                                        <span class="sr-only">Toggle navigation</span>
                                    </button>
                                    
                                </div>
                                <div class="padding_25 right_socials">
                                        <ul class="bottom-icons">
                                            <li><a class="pi pixicon-facebook2 light_gray" href="#fakelink"></a></li>
                                            <li><a class="pi pixicon-twitter2 light_gray" href="#fakelink"></a></li>
                                            <li><a class="pi pixicon-instagram light_gray" href="#fakelink"></a></li>
                                        </ul>
                                    </div>
                                <div id="navbar-collapse-02" class="collapse navbar-collapse">
                                    <ul class="nav navbar-nav navbar-left">
                                        <li class="active propClone"><a href="/login">Войти</a></li>
                                        <li class="propClone"><a href="/register">Регистрация</a></li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container -->
                        </nav>
                    </div>
                </div><!-- container -->
            </div>
        </div>
<?php include 'layout/footer.php';?>

