<?include 'layout/header.php';?>
        <div class="padding_top_60 padding_bottom_20 pix_builder_bg" id="section_intro_title">
            <div class="container ">       
                <div class="eight columns small_padding">
                    <div class="intro_div">
                        <img class="padding_bottom_20" src="/constructor/elements/images/main/logo.png" alt="">
                        <p class="editContent light_gray">
                            We are a group of Developers and Web Designers, We are specialized in WordPress theme Development, WordPress theme Customization, HTML Templates, PSD Templates, Marketing Landing Pages and Web elements design.
                        </p>
                        <ul class="bottom-icons padding_top_20">
                                <li><a class="pi pixicon-facebook2 light_gray" href="#fakelink"></a></li>
                                <li><a class="pi pixicon-twitter2 light_gray" href="#fakelink"></a></li>
                                <li><a class="pi pixicon-instagram light_gray" href="#fakelink"></a></li>
                            </ul>
                    </div>
                </div>
                <div class="eight columns small_padding">
                    <div class="intro_div">
                        <h3 class="gray editContent"><strong>Need to contact us?</strong></h3>
                        <p class="editContent light_gray">
                            You can send us a message to our office:<br>
                            121 King Street, Melbourne<br>
                            Victoria 3000 Australia<br>
                            +61 9 8451 2976<br>
                        </p>
                        <a href="#" class="active_bg_open pix_button btn_normal  bold_text orange_bg ">
                            <span>Contact Us</span>
                        </a> 
                    </div>
                </div>
                <hr>
                <div class="editContent light_gray small_text">
                    FLATPACK Copyright © 2016 PixFort. All Rights Reserved
                </div>
            </div>

        </div>

<?include 'layout/footer.php';?>

