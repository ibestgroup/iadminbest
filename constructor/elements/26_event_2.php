<?include 'layout/header.php';?>

		<div class="extra_padding pix_builder_bg dark event_bg" id="section_normal_4_1">
            <div class="container">
                <div class="sixteen columns">
                    <div class="twelve columns alpha margin_bottom_30">
                        
                        <p class="big_title editContent"><strong>GET YOUR TICKET TODAY</strong></p>
                        <p class="normal_text normal_gray editContent">
                            Eventbrite is the world's largest self-service ticketing platform. We build the technology to allow anyone to create, share, find and attend new things to do that fuel their passions and enrich their lives.
                        </p>
                        <a href="#" class="pix_button btn_normal bold_text orange_bg " name="fff">
                            <span>Buy Tickets Now</span>
                        </a> 

                    </div>
                </div>
	       </div>
        </div>

<?include 'layout/footer.php';?>

