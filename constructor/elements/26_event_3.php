<?include 'layout/header.php';?>

        <div class="section_pointer" pix-name="21_event"></div>

        <div class="light_gray_bg normal_padding pix_builder_bg" id="section_text_2">
            <div class="container">
                <div class="eight columns alpha">
                    <div class="content_div">
                        <h4 class="editContent margin_bottom_10"><strong>First Title Here</strong></h4>
                        <p class="editContent light_gray">Lorem ipsum dolor sit amet consectetur elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <a class="pix_button btn_normal small_wide_button dark slow_fade light_blue_bg editContent small_bold" href="#"><strong>Buy Tickets</strong></a>
                    </div>
                </div>
                <div class="eight columns omega">
                    <div class="content_div">
                        <h4 class="editContent margin_bottom_10"><strong>Second Title Here</strong></h4>
                        <p class="editContent light_gray">Lorem ipsum dolor sit amet consectetur elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        <a class="pix_button btn_normal small_wide_button dark slow_fade orange_bg editContent small_bold" href="#"><strong>Buy Tickets</strong></a>
                    </div>
                </div>
            </div>
        </div>

<?include 'layout/footer.php';?>

