<?include 'layout/header.php';?>
        <div class="pix_builder_bg padding_bottom_60" id="section_pixfort_medical_3">
            <div class="container normal_padding">
                <div class="three columns alpha">
                    <div class="big_circle ">
                        <i class="big_circle_content pi pixicon-chat"></i>
                    </div>
                </div>
                <div class="five columns">
                    <div class="content_div">
                        <h4 class="editContent"><strong>Start a New Conversation</strong></h4>
                        <p class="editContent small_text light_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
                <div class="three columns">
                    <div class="big_circle">
                        <i class="big_circle_content pi pixicon-compass"></i>
                    </div>
                </div>
                <div class="five columns omega">
                    <div class="content_div">
                        <h4 class="editContent"><strong>Start a New Conversation</strong></h4>
                        <p class="editContent small_text light_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>

            <div class="container normal_padding">
                <div class="three columns alpha">
                    <div class="big_circle">
                        <i class="big_circle_content pi pixicon-heart2"></i>
                    </div>
                </div>
                <div class="five columns">
                    <div class="content_div">
                        <h4 class="editContent"><strong>Start a New Conversation</strong></h4>
                        <p class="editContent small_text light_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
                <div class="three columns">
                    <div class="big_circle">
                        <i class="big_circle_content pi pixicon-map2"></i>
                    </div>
                </div>
                <div class="five columns omega">
                    <div class="content_div">
                        <h4 class="editContent"><strong>Start a New Conversation</strong></h4>
                        <p class="editContent small_text light_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    </div>
                </div>
            </div>
        </div>

<?include 'layout/footer.php';?>

