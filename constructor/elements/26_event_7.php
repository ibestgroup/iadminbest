<?include 'layout/header.php';?>
        <div class="light_gray_bg big_padding pix_builder_bg " id="section_text_2">
            <div class="container">       
                <div class="fourteen columns offset-by-one">
                    <div class="event_box row pix_builder_bg">
                        <div class="event_box_1 ">
                            <div class="event_box_img ">
                                <img src="/constructor/elements/images/main/placeholder150.png" alt="">
                            </div>
                        </div>
                        <div class="event_box_2">
                            <div class="padding_15 hor_padding">
                                <h4>
                                    <strong>Start a New Conversation</strong>
                                </h4>
                                <p class="editContent small_text light_gray">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
                                </p>
                            </div>
                        </div>
                        <div class="event_box_3">
                            <div class="hor_padding">
                                <h4>
                                    <strong>Between 12:00 - 13:15</strong>
                                </h4>
                                <p class="editContent small_text orange">
                                    Mark Smith, Developer at Themeforest
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="event_box row pix_builder_bg">
                        <div class="event_box_1 ">
                            <div class="event_box_img ">
                                <img src="/constructor/elements/images/main/placeholder150.png" alt="">
                            </div>
                        </div>
                        <div class="event_box_2">
                            <div class="padding_15 hor_padding">
                                <h4>
                                    <strong>Start a New Conversation</strong>
                                </h4>
                                <p class="editContent small_text light_gray">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
                                </p>
                            </div>
                        </div>
                        <div class="event_box_3">
                            <div class="hor_padding">
                                <h4>
                                    <strong>Between 12:00 - 13:15</strong>
                                </h4>
                                <p class="editContent small_text orange">
                                    Mark Smith, Developer at Themeforest
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="event_box row pix_builder_bg">
                        <div class="event_box_1 ">
                            <div class="event_box_img ">
                                <img src="/constructor/elements/images/main/placeholder150.png" alt="">
                            </div>
                        </div>
                        <div class="event_box_2">
                            <div class="padding_15 hor_padding">
                                <h4>
                                    <strong>Start a New Conversation</strong>
                                </h4>
                                <p class="editContent small_text light_gray">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.
                                </p>
                            </div>
                        </div>
                        <div class="event_box_3">
                            <div class="hor_padding">
                                <h4>
                                    <strong>Between 12:00 - 13:15</strong>
                                </h4>
                                <p class="editContent small_text orange">
                                    Mark Smith, Developer at Themeforest
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?include 'layout/footer.php';?>

