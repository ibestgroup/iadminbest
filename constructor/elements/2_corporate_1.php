<?include 'layout/header.php';?>
	<div class="pixfort_corporate_2" id="section_corporate_1">
		<div class="header_style">
			<div class="container">
				<div class="sixteen columns header_area">
					<img src="/constructor/elements/images/2_corporate/logo.png" alt="">
        			<a href="#" class="slow_fade social_button">
        				<img src="/constructor/elements/images/social_icons/yt.png" alt="">
        			</a>
        			<a href="#" class="slow_fade social_button">
        				<img src="/constructor/elements/images/social_icons/twitter.png" alt="">
        			</a>
		            <a href="#" class="slow_fade social_button">
		            	<img src="/constructor/elements/images/social_icons/facebook.png" alt="">
		            </a>
			 		<div class="htext_style editContent">
			 			<span class="pix_text">Stay connected</span>
			 		</div>
				</div>
			</div>
		</div>
		<div class="page_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns context_style">
					<div class="title_style">
						<span class="editContent">
							<span class="pix_text">CORPORATE LANDING PAGE</span>
						</span>
					</div>
					<div class="subtitle_style">
						<span class="editContent">
							<span class="pix_text">
								More than 10 unique unbounce templates in one bundle isn't that awesome with a lot of features and great design brought to you by PixFort.
							</span>
						</span>
					</div>
	                <div class="contact_style">
	                    <div class="contact_btn">
	                    	<a href="#" class="pix_button">
	                    		<span class="editContent">Contact us now</span>
	                    	</a>
	                    </div>
	                </div>
	                <div class="note_st">
	                	<span class="editContent">
	                		<span class="pix_text">*Note: Bundle ends in two weeks from now.</span>
	                	</span>
	                </div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>
