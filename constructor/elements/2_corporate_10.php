<?include 'layout/header.php';?>
    <div class="pixfort_corporate_2" id="section_corporate_10">
        <div  class="foot_st">
            <div class="container ">
                <div class="seven columns alpha ">
                    <span class="rights_st"> All rights reserved Copyright &copy; 2014 FLATPACK by
                        <span class="pixfort_st">PixFort</span>
                    </span>
                </div>
                <div class="nine columns omega ">
                    <div class="socbuttons">
                        <div class="soc_icons">
                            <a href="https://twitter.com/share" class="twitter-share-button" data-via="pixfort" data-count="none">
                                Tweet
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <!-- Place this tag where you want the +1 button to render. -->
                            <!-- <span class="confirm_gp">
                                    <span class="g-plusone " data-size="medium" data-annotation="none"></span>
                                </span>
                            <!== Place this tag after the last +1 button tag. -->
                            <iframe src=
                                  "https://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fthemeforest.net%2Fuser%2FPixFort&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35&amp;appId=445119778844521"
                                  class="c1">
                            </iframe>
                        </div>
                        <div class="likes_st">Your likes &amp; share makes us happy!</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>

