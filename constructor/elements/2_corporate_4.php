<?include 'layout/header.php';?>
	<div class="pixfort_corporate_2" id="section_corporate_4">
		<div class="amazing_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="gstyle">
						<div class="t1_style">
							<span class="editContent">
								<span class="pix_text">The most amazing thing is here</span>
							</span>
						</div>
						<div class="t2_style">
							<span class="editContent">
								<span class="pix_text">Our service is astonishingly thin and light.</span>
							</span>
						</div>
						<div class="t3_style">
							<span class="editContent">
								<span class="pix_text">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud.</span>
							</span>
						</div>
					</div>
					<span>
						<img src="/constructor/elements/images/2_corporate/promo-image.png" class="pub_st" alt="">
					</span>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>
