<?include 'layout/header.php';?>
	<div class="pixfort_corporate_2" id="section_corporate_5">
		<div class="adv_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="L1_style">
						<span class="editContent">
							<span class="pix_text">Great people trusted our services</span>
						</span>
					</div>
					<div class="L2_style">
						<span class="editContent">
							<span class="pix_text">great words from great people</span>
						</span>
					</div>
					<div class="four columns just_st alpha">
						<div class="h_style">
							<span class="editContent"><span class="pix_text">+950</span></span>
						</div>
						<div class="cc_style">
							<span class="editContent"><span class="pix_text">Themes and Template Sales</span></span>
						</div>
					</div>
					<div class="four columns just_st ">
					    <div class="h_style">
					    	<span class="editContent"><span class="pix_text">+150</span></span>
					    </div>
					    <div class="cc_style">
					    	<span class="editContent"><span class="pix_text">Followers love and trust us</span></span>
					    </div>
					</div>
					<div class="four columns just_st ">
					    <div class="h_style">
					    	<span class="editContent"><span class="pix_text">+85</span></span>
					    </div>
					    <div class="cc_style">
					    	<span class="editContent"><span class="pix_text">Items selling on themeforest</span></span>
					    </div>
					</div>
					<div class="four columns just_st omega">
					    <div class="h_style">
					    	<span class="editContent"><span class="pix_text">+6K</span></span>
					    </div>
					    <div class="cc_style">
					    	<span class="editContent"><span class="pix_text">Working hours this year wow</span></span>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>
