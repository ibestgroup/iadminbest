<?include 'layout/header.php';?>
<?
$marks = array_filter(Main::getMarks(), function ($ar) {
    return $ar['status'] == 1;
});
?>

    <div class="pixfort_corporate_2" id="section_corporate_6">
        <div class="amazing_style pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="gstyle">
                        <div class="t1_style">
                            <span class="editContent"><span class="pix_text">Выбери свой тариф</span></span>
                        </div>
                    </div>
                    <?
                    foreach ($marks as $mark) {
                    $price_level = [$mark['level1'], $mark['level2'], $mark['level3'], $mark['level4'], $mark['level5'], $mark['level6'], $mark['level7'], $mark['level8']];
                    ?>
                        <div class="one-third column">
                            <div class="th1_style pix_builder_bg">
                                <div class="a_bloc1_style pix_builder_bg">
                                    <p class="a1_style editContent"><?=$mark['name']?></p>
                                    <p class="a2_style editContent">Get All The Freebies</p>
                                </div>
                                <br>
                                <div class="quick-info">
                                    <p class="pix_edit">
                                        Реинвест в <b class="p-reinvest-tarif-name"><?=Main::getMarks()[$mark['reinvest']]['name']?></b>
                                        - <span class="p-reinvest-tarif-sum"><?= Main::money_mark_auto($mark['reinvest']); ?></span><?=Main::getIcon()?>
                                    </p>
                                    <hr>
                                    <?if ($mark['reinvest_first']) {?>
                                        <p class="pix_edit">Реинвест в <b><?=Main::getMarks()[1]['name']?></b>
                                            - <span class="p-reinvest-first-sum"><?= Main::money_mark_auto(1);?></span><?= Main::getIcon()?>
                                        </p>
                                        <hr>
                                    <?}?>
                                </div><!-- /.quick-info -->
                                <div class="flat_pricing">
                                    <div class="row tableWrapper pix_edit">
                                        <div class="col-lg-1 brb">Ур.</div>
                                        <div class="col-lg-3 brb">Цена</div>
                                        <div class="col-lg-4 brb">Чел</div>
                                        <div class="col-lg-3 brb">Доход</div>
                                    </div>
                                    <ul class="info p-mark-levels tableWrapper pix_edit">
                                        <?
                                        for ($i = 0; $i < $mark['level']; $i++) {?>
                                            <li class="row p-marks-level-line">
                                                <div class="col-lg-1 brb p-mark-level-n"><?= $i+1;?></div>
                                                <div class="col-lg-3 brb"><span class="p-mark-level-price"><?=$price_level[$i]?></span><?=Main::getIcon();?></div>
                                                <div class="col-lg-4 brb p-mark-level-user"><?= pow($mark['width'], $i+1);?></div>
                                                <div class="col-lg-3 brb"><span class="p-mark-level-profit"><?= pow($mark['width'], $i+1) * $price_level[$i] * 0.9?></span><?= Main::getIcon() ?></div>
                                            </li>
                                            <?
                                        } ?>
                                    </ul>
                                </div>

                                <div class="a_bloc3_style pix_builder_bg">
                                    <div class="subscribe_st">

                                        <span class="subscribe_a editContent"> <a href="/login" class="slow_fade pix_text">Выьрать</a> </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?}?>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
