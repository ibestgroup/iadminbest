<?include 'layout/header.php';?>
	<div class="pixfort_corporate_2" id="section_corporate_8">
		<div class="awesome_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<h1 class="h_awesome editContent"> What our awesome clients say </h1>
					<p class="txt_awesome editContent">
						Great service with fast and reliable support The design work and detail put into themes are great.
					</p>	
					<div class="via_st">
						<img src="/constructor/elements/images/2_corporate/stars.original.png" alt="">
						<div class="top_3 editContent pix_text">via Unbounce.com</div>
					</div>
				</div>
			</div><!-- container -->
		</div>
	</div>
<?include 'layout/footer.php';?>

