<?include 'layout/header.php';?>
	<div class="pixfort_corporate_2" id="section_corporate_9">
		<div class="contact_section pix_builder_bg">
			<div class="container">
				<div class="sixteen columns pix_inline_block">
					<div class="eight columns  alpha">
						<div class="headtext_style"><span class="editContent"><span class="pix_text">See our office</span></span></div>
						<p class="subtext_style editContent">Our service is totaly thin and light.</p>
						<div class="plan">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d83998.91163207508!2d2.3470599!3d48.85885894999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sParis%2C+France!5e0!3m2!1sen!2s!4v1408382253934" style="border:0"></iframe>
						</div>
					</div>
					<div class="eight columns omega">
						<div class="contact_zone">
							<div class="headtext_style"><span class="editContent"><span class="pix_text">Contact us now</span></span></div>
							<p class="subtext_style editContent">Our service is totaly thin and light.</p>
							<div class="contact_st">
								<form id="contact_form" pix-confirm="hidden_pix_2">
									<div id="result"></div>
									<input type="text" name="name" id="name" placeholder="Your Full Name" class="pix_text">
									<input type="text"  name="company" id="company" placeholder="Your Company" class="pix_text">
									<input type="email" name="email" id="email" placeholder="Enter Your Email" class="pix_text">
									<textarea  rows="5" name="message" class="text_area pix_text" placeholder="Write your Message Here"></textarea>
									<button type="submit" class="subscribe_btn submit_btn_landing pix_text" id="subscribe_btn_2">
										<span class="editContent">Send it right away</span>
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div><!-- container -->
		</div>
	</div>
	<div id="hidden_pix_2" class="confirm_page confirm_page_2">
		<div class="pixfort_corporate_2 big_padding">
			<div class="confirm_header"><span class="editContent"><span class="pix_text">WOW, You Are AWESOME</span></span></div>
			<div class="confirm_text">
				<span class="editContent"><span class="pix_text">Your form has been submitted and we will contact you as soon as possible.</span></span>
			</div>
			<div class="center_text padding_bottom_60">
                <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
                    <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
                </ul>
            </div>
		</div>
	</div>
<?include 'layout/footer.php';?>

