<?include 'layout/header.php';?>
    <div class="pixfort_app_3" id="section_app_1">
        <div class="header_style pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="logo_st">
                        <img src="/constructor/elements/images/3_app/logo.png" class="logo_st" alt="">
                    </div>
                    <div class="soc_st">
                        <span class="htext_style">
                            <span class="editContent">
                                <span class="pix_text">Stay connected</span>
                            </span>
                        </span>
                        <div class="social_span">
                            <ul class="bottom-icons">
                                <li><a class="pi pixicon-facebook2 normal_gray" href="#fakelink"></a></li>
                                <li><a class="pi pixicon-twitter2 normal_gray" href="#fakelink"></a></li>
                                <li><a class="pi pixicon-instagram normal_gray" href="#fakelink"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="sixteen columns">
                    <div class="seven columns alpha left_part">
                        <div class="hbloc_style">
                            <p class="title_style editContent">APP LANDING PAGE</p>
                            <p class="txt_title editContent">
                                More than 10 unique HTML templates in one bundle isn't that awesome with a lot of features and great design brought to you by PixFort.
                            </p>
                            <span class="editContent">
                                <span class="contact_btn">
                                    <a class="slow_fade pix_button" href="#">DOWNLOAD</a>
                                </span>
                            </span>
                            <div>
                               <span><a href="#"><img src="/constructor/elements/images/3_app/apple.png" class="log_st slow_fade" alt=""></a></span>
                               <span><a href="#"><img src="/constructor/elements/images/3_app/android.png" class="log_st slow_fade" alt=""></a></span>
                               <span><a href="#"><img src="/constructor/elements/images/3_app/windows.png" class="log_st slow_fade" alt=""></a></span>
                            </div>
                        </div>
                    </div>
                    <div class="nine columns omega right_part">
                        <div class="phones_style">
                            <img src="/constructor/elements/images/3_app/phones.png" class="phones" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>

