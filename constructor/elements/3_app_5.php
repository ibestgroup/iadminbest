<?include 'layout/header.php';?>
    <div class="pixfort_app_3" id="section_app_5">
        <div class="services_style">
	       <div class="container">
                <div class="sixteen columns">
                    <div class="gstyle">
                        <div class="t1_style">Our Services Prices and Plans</div>
                        <div class="t2_style">Our service is totaly thin and light.</div>
                        <div class="t3_style">Lorem ipsum dolor sit amet consectetur  enim ad minim veniam quis nostrud.</div>
                    </div>
                    <div class="one-third column  alpha">
                        <div class="th1_style">
                            <div class="a_bloc1_style">
                                <p class="a1_style">Free</p>
                                <p class="a2_style">Get All The Freebies</p>
                            </div>
                            <br>
                            <p class="a3_style">5 Projects</p>
                            <p class="a3_style">1 GB Storage</p>
                            <p class="a3_style">No Domain</p>
                            <p class="a3_style">1 User</p>
                            <p class="a3_style"><span class="fsc1">0$</span>/m </p>
                            <div class="a_bloc3_style">
                                <div class="subscribe_st">
                                    <span class="subscribe_a"> <a href="#" class="slow_fade">Subscribe</a> </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="one-third column ">
                        <div class="th2_style">
                            <div class="b_bloc1_style">
                                <p class="b1_style">Free</p>
                                <p class="b2_style">Get All The Freebies</p>
                            </div>
                            <br>
                            <p class="b3_style">25 Projects</p>
                            <p class="b3_style">50 GB Storage</p>
                            <p class="b3_style">with 1 Domain</p>
                            <p class="b3_style">10 Users</p>
                            <p class="b3_style"><span class="fsc2">9,99$</span>/m </p>
                            <div class="b_bloc3_style">
                                <div class="subscribe_st">
                                    <span class="subscribe_b"> <a href="#" class="slow_fade">Subscribe</a> </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="one-third column   omega">
                        <div class="th1_style">
                            <div class="a_bloc1_style">
                                <p class="a1_style">Free</p>
                                <p class="a2_style">Get All The Freebies</p>
                            </div>
                            <br>
                            <p class="a3_style">100 Projects</p>
                            <p class="a3_style">250 GB Storage</p>
                            <p class="a3_style">Unlimited Domains</p>
                            <p class="a3_style">50 Users</p>
                            <p class="a3_style"><span class="fsc1">70$</span>/m </p>
                            <div class="a_bloc3_style">
                                <div class="subscribe_st">
                                    <span class="subscribe_a"> <a href="#" class="slow_fade">Subscribe</a> </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- container -->
        </div>
    </div>
<?include 'layout/footer.php';?>
