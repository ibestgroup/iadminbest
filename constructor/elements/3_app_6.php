<?include 'layout/header.php';?>
	<div class="pixfort_app_3" id="section_app_6">
		<div class="companies_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
            		<div class="titres">
    		    		<div class="comp1_style">
    		    			<span class="editContent">
    		    				<span class="pix_text">Companies We Work With</span>
    		    			</span>
    		    		</div>
                		<div class="comp2_style">
                			<span class="editContent">
                				<span class="pix_text">great words from great people</span>
                			</span>
                		</div>
            		</div>
            		<div class="four columns logos_st alpha">
            			<img src="/constructor/elements/images/3_app/logo_1.png"  alt="">
            		</div>
            		<div class="four columns logos_st">
            			<img src="/constructor/elements/images/3_app/logo_2.png"  alt="">
            		</div>
            		<div class="four columns logos_st">
            			<img src="/constructor/elements/images/3_app/logo_3.png"  alt="">
            		</div>
            		<div class="four columns logos_st omega">
            			<img src="/constructor/elements/images/3_app/logo_4.png"  alt="">
            		</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

