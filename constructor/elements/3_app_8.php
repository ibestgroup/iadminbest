<?include 'layout/header.php';?>
	<div class="pixfort_app_3" id="section_app_8">
		<div class="download_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<h1 class="h_download editContent">Download App Now</h1>
					<p class="txt_download editContent">
						Lorem ipsum dolor sit amet consectetur enim ad minim veniam quis nostrud.
					</p>
            		<span class="contact_btn editContent">
            			<a class="slow_fade pix_text" href="#">DOWNLOAD</a>
            		</span>
            		<div>
						<a href="#"><span><img src="/constructor/elements/images/3_app/apple.png" class="log_st slow_fade" alt=""></span></a>
						<a href="#"><span><img src="/constructor/elements/images/3_app/android.png" class="log_st slow_fade" alt=""></span></a>
						<a href="#"><span><img src="/constructor/elements/images/3_app/windows.png" class="log_st slow_fade" alt=""></span></a>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

