<?include 'layout/header.php';?>
    <div class="pix_builder_bg" id="section_404">
        <div class="footer3">
            <div class="container ">
                <div class="ten columns alpha offset-by-three">
                    <div class="content_div center_text">
                        <p class="big_title bold_text editContent">Ooooooppss...</p>
                        
                        <div class="margin_bottom_30">
                            <p class="title_170 editContent">
                                <strong>
                                4<i class="pi pixicon-blocked dark_red title_140"></i>4
                                </strong>
                            </p>
                            <p class="normal_text light_gray center_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmoda tempor incididunt dolor sit amet consectetur.</p>
                        </div>
                        <a href="#" class="pix_button pix_button_line  green_1 btn_big">
                            <strong><span>Go Back Home</span><i class="pi pixicon-arrow-right p_right"></i></strong>
                        </a>    
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>

