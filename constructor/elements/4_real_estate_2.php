<?include 'layout/header.php';?>
	<div class="pixfort_real_estate_4" id="section_real_estate_2">
		<div class="page_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
            		<div class="text_page">
			        	<img src="/constructor/elements/images/4_real_estate/home-logo.png" class="home_st" alt="">
                    	<span class="editContent"><h1 class="start_style">START YOUR NEW LIFE</h1></span>
			        	<p class="txt_start">
				            <span class="editContent">More than 10 unique HTML templates in one bundle isn't that awesome with a lot of features and great design brought to you by PixFort.</span>
			        	</p>
                    	<span class="gethouse_btn editContent"><a class="slow_fade pix_text" href="#">GET HOUSE NOW</a></span>
                    	<p class="note_st"> <span class="editContent">*Note: Bundle ends in two weeks from now.</span></p>
             		</div>
        		</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

