<?include 'layout/header.php';?>
	<div class="pixfort_real_estate_4" id="section_real_estate_3">
		<div class="cal_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns pad_top">
             		<div class="one-third column  alpha">
						<div class="img_st">
							<img src="/constructor/elements/images/4_real_estate/1.png"  alt="">
						</div>
						<div class="ctext_style">
							<p class="calc_st editContent">High quality tutorials</p>
							<p class="calc_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
						</div>
             		</div>
             		<div class="one-third column ">
						<div class="img_st">
							<img src="/constructor/elements/images/4_real_estate/2.png"  alt="">
						</div>
						<div class="ctext_style">
							<p class="calc_st editContent">Good prices for all</p>
							<p class="calc_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
						</div>
             		</div>
					<div class="one-third column  omega">
						<div class="img_st">
							<img src="/constructor/elements/images/4_real_estate/3.png"  alt="">
						</div>
						<div class="ctext_style">
							<p class="calc_st editContent">Pay easy and online</p>
							<p class="calc_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
						</div>
             		</div>
        		</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

