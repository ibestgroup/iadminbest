<?include 'layout/header.php';?>
    <div class="pixfort_real_estate_4" id="section_real_estate_4_1">
        <div class="Homes pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <h1 class="title_homes editContent">Featured Homes</h1>
                    <p class="subtitle_homes editContent">
  		                Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore.
                    </p>
                    <div class="eight columns  alpha">
                        <div class="bloc_home">
                            <img src="/constructor/elements/images/4_real_estate/image-1.png" class="Home_photo"   alt="">
        				    <div class="Home_info">
                                <h3 class="tit_info editContent">Big Green House</h3>
                                <h4 class="txt_info editContent">Lorem ipsum dolor sit elite.</h4>
                                <div class="info1  editContent">
                                    <span class="gris">Rooms:&nbsp;</span><span class="green">3</span>  <span class="gris left40">Size:&nbsp;</span><span class="green">145 m&sup2;</span>
                                    <br>
                                    <span class="gris">Bathrooms:&nbsp;</span><span class="green">2</span>  <span class="gris left10">Garage:&nbsp;</span><span class="green">Yes</span>
                                </div>
                                <div class="info2 editContent">
                                    <span class="gris">Price:&nbsp;</span> <span class="green">$50,000</span>
                                    <span class="contact_btn"> <a class="slow_fade" href="#">Contact Now</a> </span>
                                </div>
        				    </div>
                        </div>
                    </div>
                    <div class="eight columns  omega">
                        <div class="bloc_home">
        				    <img src="/constructor/elements/images/4_real_estate/image-2.png" class="Home_photo"  alt="">
        				    <div class="Home_info">
                                <h3 class="tit_info editContent">Good BedRoom</h3>
                                <h4 class="txt_info editContent">Lorem ipsum dolor sit elite.</h4>
                                <div class="info1 editContent">
                                    <span class="gris">Rooms:&nbsp;</span><span class="green">1</span>  <span class="gris left40">Size:&nbsp;</span><span class="green">75 m&sup2;</span>
                                    <br>
                                    <span class="gris">Bathrooms:&nbsp;</span><span class="green">1</span>  <span class="gris left10">Garage:&nbsp;</span><span class="green">No</span>
                                </div>
                                <div class="info2 editContent">
                                    <span class="gris">Price:&nbsp;</span> <span class="green">$18,500</span>
                                    <span class="contact_btn"> <a class="slow_fade" href="#">Contact Now</a> </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>

