<?include 'layout/header.php';?>
    <div class="pixfort_real_estate_4" id="section_real_estate_4_2">
        <div class="Homes pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <h1 class="title_homes editContent">New Latest Offers</h1>
                    <p class="subtitle_homes editContent">
  		                Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore.
                    </p>
                    <div class="eight columns  alpha">
                        <div class="bloc_home">
        				    <img src="/constructor/elements/images/4_real_estate/image-3.png" class="Home_photo"    alt="">
        				    <div class="Home_info">
                                <h3 class="tit_info editContent">White kitchen</h3>
                                <h4 class="txt_info editContent">Lorem ipsum dolor sit elite.</h4>
                                <div class="info1 editContent">
                                    <span class="gris">Rooms:&nbsp;</span><span class="green">2</span>  <span class="gris left40">Size:&nbsp;</span><span class="green">235 m&sup2;</span>
                                    <br>
                                    <span class="gris">Bathrooms:&nbsp;</span><span class="green">3</span>  <span class="gris left10">Garage:&nbsp;</span><span class="green">Yes</span>
                                </div>
                                <div class="info2 editContent">
                                    <span class="gris">Price:&nbsp;</span> <span class="green">$4,000</span>
                                    <span class="contact_btn"> <a class="slow_fade" href="#">Contact Now</a> </span>
                                </div>
        				    </div>
                        </div>
                    </div>
                    <div class="eight columns  omega">
                        <div class="bloc_home">
        				    <img src="/constructor/elements/images/4_real_estate/image-7.png" class="Home_photo"  alt="">
        				    <div class="Home_info">
                                <h3 class="tit_info editContent">Pro Salon</h3>
                                <h4 class="txt_info editContent">Lorem ipsum dolor sit elite.</h4>
                                <div class="info1 editContent">
                                    <span class="gris">Rooms:&nbsp;</span><span class="green">4</span>  <span class="gris left40">Size:&nbsp;</span><span class="green">435 m&sup2;</span>
                                    <br>
                                    <span class="gris">Bathrooms:&nbsp;</span><span class="green">4</span>  <span class="gris left10">Garage:&nbsp;</span><span class="green">Yes</span>
                                </div>
                                <div class="info2 editContent">
                                    <span class="gris">Price:&nbsp;</span> <span class="green">$13,000</span>
                                    <span class="contact_btn"> <a class="slow_fade" href="#">Contact Now</a></span>
                                </div>
        				    </div>
                        </div>
                    </div>
                    <div class="eight columns  alpha">
                        <div class="bloc_home">
        				    <img src="/constructor/elements/images/4_real_estate/image-8.png" class="Home_photo"   alt="">
        				    <div class="Home_info">
                                <h3 class="tit_info editContent">Old Desk Space</h3>
                                <h4 class="txt_info editContent">Lorem ipsum dolor sit elite.</h4>
                                <div class="info1 editContent">
                                    <span class="gris">Rooms:&nbsp;</span><span class="green">2</span>  <span class="gris left40">Size:&nbsp;</span><span class="green">155 m&sup2;</span>
                                    <br>
                                    <span class="gris">Bathrooms:&nbsp;</span><span class="green">1</span>  <span class="gris left10">Garage:&nbsp;</span><span class="green">No</span>
                                </div>
                                <div class="info2 editContent">
                                    <span class="gris">Price:&nbsp;</span> <span class="green">$24,000</span>
                                    <span class="contact_btn"> <a class="slow_fade" href="#">Contact Now</a> </span>
                                </div>
        				    </div>
                        </div>
                    </div>
                    <div class="eight columns  omega">
                        <div class="bloc_home">
        				    <img src="/constructor/elements/images/4_real_estate/image-11.png" class="Home_photo"  alt="">
        				    <div class="Home_info">
                                <h3 class="tit_info editContent">Beautiful Room</h3>
                                <h4 class="txt_info editContent">Lorem ipsum dolor sit elite.</h4>
                                <div class="info1 editContent">
                                    <span class="gris">Rooms:&nbsp;</span><span class="green">1</span>  <span class="gris left40">Size:&nbsp;</span><span class="green">45 m&sup2;</span>
                                    <br>
                                    <span class="gris">Bathrooms:&nbsp;</span><span class="green">1</span>  <span class="gris left10">Garage:&nbsp;</span><span class="green">No</span>
                                </div>
                                <div class="info2 editContent">
                                    <span class="gris">Price:&nbsp;</span> <span class="green">$99,000</span>
                                    <span class="contact_btn"> <a class="slow_fade" href="#">Contact Now</a> </span>
                                </div>
        				    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>

