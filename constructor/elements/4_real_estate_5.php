<?include 'layout/header.php';?>
	<div class="pixfort_real_estate_4" id="section_real_estate_5">
		<div class="Designs_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="eight columns homeplan_st alpha">
						<img src="/constructor/elements/images/4_real_estate/proto.jpg"  class="img4_st" alt="">
					</div>
					<div class="eight columns omega">
						<div class="Designs_text_style">
							<p class="amaz_tit editContent">Bring it to life again with us</p>
							<p class="amaz_subtit editContent">Our service is totaly thin and light.</p>
							<p class="amaz_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
							<span class="contactus_btn editContent"><a class="slow_fade pix_text" href="#">Contact Us Now</a></span>
                		</div>
					</div>
    			</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

