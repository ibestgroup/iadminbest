<?include 'layout/header.php';?>
	<div class="pixfort_real_estate_4" id="section_real_estate_6">
		<div class="logos_style">
			<div class="container">
				<div class="sixteen columns ">
            		<div class="titres">
                		<span class="L1_style">Great people trusted our services</span><br>
                		<span class="L2_style">great words from great people</span>
            		</div>
            		<div class="logos_div">
                		<div class="four columns logos_st alpha">
                			<img src="/constructor/elements/images/4_Real_estate/logo_1.png" alt="">
                		</div>
                		<div class="four columns logos_st">
                			<img src="/constructor/elements/images/4_Real_estate/logo_2.png" alt="">
                		</div>
             			<div class="four columns logos_st">
             				<img src="/constructor/elements/images/4_Real_estate/logo_3.png" alt="">
             			</div>
                		<div class="four columns logos_st omega">
                			<img src="/constructor/elements/images/4_Real_estate/logo_4.png" alt="">
                		</div>
            		</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

