<?include 'layout/header.php';?>
    <div class="pixfort_real_estate_4" id="section_real_estate_7">
    <div class="envato_unbounce pix_builder_bg">
        <div class="container">
            <div class="sixteen columns">
                <span class="editContent">
                    <span class="pix_text">
                        <span class="L1_style">What our awesome clients say</span>
                    </span>
                </span><br>
                <span class="editContent">
                    <span class="pix_text">
                        <span class="L2_style">great words from great people</span>
                    </span>
                </span><br>
            <div class="eight columns  alpha ">
               <div class="box_style ">
                   <div class="b_style">
                        <img src="/constructor/elements/images/testimonials/envato-logo.png" class="logo_style" alt="">
                   </div>
                   <div class="txt_style">
                        <p class="txt editContent">Im a web designer, you guys are very inspiring I wish to see more work from maybe more freebies.</p>
                        <span class="stars_div">
                            <span class="zoom"> <img src="/constructor/elements/images/4_real_estate/stars.original.png" alt="">  </span>
                            <span class="env_st editContent">via Envato.com</span>
                        </span>
                   </div>
               </div>
            </div>

            <div class="eight columns omega">
                <div class="box_style">
                   <div class="b_style">
                        <img src="/constructor/elements/images/testimonials/unboune-logo.png" class="logo_style" alt="">
                   </div>

                   <div class="txt_style">
                        <p class="txt editContent">Great service with fast and relible support The design work and detail put into themes are great.</p>
                        <span class="stars_div">
                            <span class="zoom"> <img src="/constructor/elements/images/4_real_estate/stars.original.png" alt="">  </span>
                            <span class="env_st editContent">via Unbounce.com</span>
                        </span>
                   </div>
                </div>
            </div>
		</div>
	</div>
</div>
</div>
<?include 'layout/footer.php';?>

