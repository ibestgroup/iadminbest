<?include 'layout/header.php';?>
	<div class="pixfort_real_estate_4" id="section_real_estate_8">
		<div class="contact_section pix_builder_bg">
			<div class="container">
				<div class="sixteen columns ">
					<div class="eight columns alpha no-margin-right style11">
						<div class="plan_st">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d83998.91163207508!2d2.3470599!3d48.85885894999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sParis%2C+France!5e0!3m2!1sen!2s!4v1408382253934"></iframe>
						</div>
					</div>
					<div class="eight columns Contact_bloc omega no-margin-left">
						<div class="estate_text_input ">
							<div class="headtext_style"><span class="editContent"><span class="pix_text">Contact us now</span></span></div>
							<p class="subtext_style editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</p>
							<div class="contact_st ">
								<form id="contact_form" pix-confirm="hidden_pix_4">
									<div id="result"></div>
									<input type="text" name="name" id="name" placeholder="Your Full Name" class="pix_text">
									<input type="email" name="email" id="email" placeholder="Enter Your Email" class="pix_text">
									<input type="text"  name="number" id="number" placeholder="Your Phone Number" class="pix_text">
									<button class="submit_btn slow_fade pix_text" id="submit_btn_4">
										<span class="editContent">Send Information</span>
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="hidden_pix_4" class="confirm_page confirm_page_4 pix_builder_bg">
		<div class="pixfort_real_estate_4">
			<div class="confirm_header"><span class="editContent"><span class="pix_text">Thank You Very Much!</span></span></div>
			<div class="confirm_text">
				<span class="editContent"><span class="pix_text">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</span></span>
			</div>
			<div class="center_text padding_bottom_60">
				<ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
					<li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
					<li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
					<li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
				</ul>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

