<?include 'layout/header.php';?>
	<div class="pixfort_hotel_5" id="section_hotel_2">
		<div class="page_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="text_page">
						<h1 class="just_style editContent">JUST MAGICAL</h1>
						<h1 class="four_style editContent">FOUR SEASONS RESORT</h1>
						<div class="red_segment pix_builder_bg"></div>
						<p class="txt_start editContent">
							More than 10 unique HTML templates in one bundle isn't that awesome with a lot of features and great design brought to you by PixFort.
						</p>
						<span class="gethouse_btn editContent"><a class="pix_text" href="#">Get House Now</a></span>
						<p class="note_st editContent"> *Note: Bundle ends in two weeks from now.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

