<?include 'layout/header.php';?>
	<div class="pixfort_hotel_5" id="section_hotel_3">
		<div class="cal_style pix_builder_bg2">
			<div class="container">
				<div class="sixteen columns pad_top">
					<div class="four columns  alpha">
						<img src="/constructor/elements/images/5_hotel/1.png" class="img_st" alt="">
						<div class="ctext_style">
							<p class="calc_st editContent">High quality tutorials</p>
							<p class="calc_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
						</div>
					</div>
					<div class="four columns column ">
						<div class="img_st">
							<img src="/constructor/elements/images/5_hotel/2.png"  alt="">
						</div>
						<div class="ctext_style">
							<p class="calc_st editContent">Good prices for all</p>
							<p class="calc_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
						</div>
					</div>
					<div class="four columns column ">
						<div class="img_st">
							<img src="/constructor/elements/images/5_hotel/3.png"  alt="">
						</div>
						<div class="ctext_style">
							<p class="calc_st editContent">Pay easy and online</p>
							<p class="calc_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
						</div>
					</div>
					<div class="four columns column  omega">
						<div class="img_st">
							<img src="/constructor/elements/images/5_hotel/4.png"  alt="">
						</div>
						<div class="ctext_style">
							<p class="calc_st editContent">Be The Best</p>
							<p class="calc_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

