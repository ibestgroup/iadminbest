<?include 'layout/header.php';?>
        <div class="pixfort_hotel_5" id="section_hotel_4_1">
            <div class="Homes pix_builder_bg">
               <div class="container">
                  <div class="sixteen columns">
                    <h1 class="title_homes editContent">Featured Homes</h1>
                    <p class="subtitle_homes editContent">
                        Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore.
                    </p>
                    <div class="four columns  alpha">
                        <div class="bg_Homes">
                            <div class="img_homes_st"><img src="/constructor/elements/images/5_hotel/image-1.png" class=""  alt=""></div>
                            <div class="text_homes_style">
                               <p class="calc_homes_st editContent">Welcome to Hotel</p>
                               <p class="calc_homes_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
                           </div>
                           <span class="perday_btn editContent">  <a class="pix_text" href="#">$45 per day</a>  </span>
                       </div>
                    </div>

                    <div class="four columns column ">
                        <div class="bg_Homes">
                            <div class="img_homes_st"><img src="/constructor/elements/images/5_hotel/image-2.png" class=""  alt=""></div>
                            <div class="text_homes_style">
                               <p class="calc_homes_st editContent">Best Services</p>
                               <p class="calc_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
                           </div>
                           <span class="perday_btn editContent">  <a class="pix_text" href="#">$30 per day</a>  </span>
                       </div>
                    </div>
                    <div class="four columns column ">
                        <div class="bg_Homes">
                            <div class="img_homes_st"><img src="/constructor/elements/images/5_hotel/image-3.png" class=""  alt=""></div>
                            <div class="text_homes_style">
                                <p class="calc_homes_st editContent">Daily Trips</p>
                                <p class="calc_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
                            </div>
                            <span class="perday_btn editContent">  <a class="pix_text" href="#">$75 per day</a>  </span>
                        </div>
                    </div>
                    <div class="four columns column  omega">
                        <div class="bg_Homes">
                            <div class="img_homes_st"><img src="/constructor/elements/images/5_hotel/image-4.png" class=""  alt=""></div>
                            <div class="text_homes_style">
                                <p class="calc_homes_st editContent">Great Support</p>
                                <p class="calc_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
                            </div>
                            <span class="perday_btn editContent">  <a class="pix_text" href="#">$125 per day</a>  </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>

