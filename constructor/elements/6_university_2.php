<?include 'layout/header.php';?>
	<div class="pixfort_university_6" id="section_university_2">
		<div class="page_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="text_page">
						<h1 class="head_line editContent">DISCOVER THE UNIQUE</h1>
						<h1 class="head_line head_line_2 editContent">COSMOPOLITAN ENVIRONMENT OF</h1>
						<h1 class="head_line editContent">BOSTON <span class="head_line head_line_2">&</span> CAMBRIDGE.</h1>
					</div>
					<div class="tow_links">
						<span class="college_btn editContent">
							<a class="pix_text" href="#">College Events</a>
						</span>
						<span class="join_btn editContent">
							<a class="pix_text" href="#">Join us now</a>
						</span>
					</div>
					<p class="note_st editContent"> *Note: Bundle ends in two weeks from now.</p>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

