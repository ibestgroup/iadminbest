<?include 'layout/header.php';?>
	<div class="pixfort_university_6" id="section_university_4">
		<div class="uni_life pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="four columns  alpha">
						<div class="part_st">
							<div class="img_uni_st"><img src="/constructor/elements/images/6_university/1.png"  alt=""></div>
							<div class="text_uni_style">
								<p class="uni_st editContent">Welcome to University</p>
								<p class="uni_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
							</div>
							<span class="link_1_btn editContent"><a class="pix_text"  href="#">Apply Now</a></span>
						</div>
					</div>
					<div class="four columns column ">
						<div class="part_st">
							<div class="img_uni_st"><img src="/constructor/elements/images/6_university/2.png"  alt=""></div>
							<div class="text_uni_style">
								<p class="uni_st editContent">Academics Students</p>
								<p class="uni_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
							</div>
							<span class="link_2_btn editContent"> <a class="pix_text"  href="#">Apply Now</a> </span>
						</div>
					</div>
					<div class="four columns column ">
						<div class="part_st">
							<div class="img_uni_st"><img src="/constructor/elements/images/6_university/3.png"  alt=""></div>
							<div class="text_uni_style">
								<p class="uni_st editContent">Financial Aid</p>
								<p class="uni_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
							</div>

							<span class="link_3_btn editContent">
								<a class="pix_text"  href="#">Apply Now</a>
							</span>
						</div>
					</div>
					<div class="four columns columns  omega">
						<div class="part_st">
							<div class="img_uni_st"><img src="/constructor/elements/images/6_university/4.png"  alt=""></div>
							<div class="text_uni_style">
								<p class="uni_st editContent">Residential Life</p>
								<p class="uni_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elite.</p>
							</div>
							<span class="link_4_btn editContent">
								<a class="pix_text"  href="#">Apply Now</a>
							</span>
						</div>
					</div>
				</div>
			</div><!-- container -->
		</div>
	</div>
<?include 'layout/footer.php';?>

