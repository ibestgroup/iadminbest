<?include 'layout/header.php';?>
	<div class="pixfort_university_6" id="section_university_6">
		<div class="envato_unbounce">
			<div class="container">
				<div class="sixteen columns">
					<span class="L1_style">What our awesome clients say</span><br>
					<span class="L3_style">great words from great people</span><br>
					<div class="eight columns  alpha ">
						<div class="box_style">
							<div class="b_style">
								<img src="/constructor/elements/images/testimonials/envato-logo.png" class="logo_style" alt="">
							</div>
							<div class="txt_style">
								<div class="txt_style_inner">
									<p class="txt">Im a web designer, you guys are very inspiring I wish to see more work from maybe more freebies.</p>
									<span class="via_span"> <img src="/constructor/elements/images/6_university/stars.original.png" alt="">  
									<span class="env_st">via Envato.com</span></span>   
									</div>
							</div>
						</div>
					</div>
					<div class="eight columns omega">
						<div class="box_style">
							<div class="b_style">
								<img src="/constructor/elements/images/testimonials/unboune-logo.png" class="logo_style" alt="">
							</div>
							<div class="txt_style">
								<div class="txt_style_inner">
									<p class="txt">Great service with fast and relible support The design work and detail put into themes are great.</p>
									<span class="via_span"> <img src="/constructor/elements/images/6_university/stars.original.png" alt="">  
									<span class="env_st">via Unbounce.com</span></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

