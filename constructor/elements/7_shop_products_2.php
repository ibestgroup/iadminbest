<?include 'layout/header.php';?>
	<div class="pixfort_shop_7" id="section_shop_2">

		<div class="page_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="eight columns left_box alpha">
						<h1 class="discount editContent">GET DISCOUNT CODE</h1>
						<div class="segment pix_builder_bg"> </div>
						<p class="shop_text editContent">Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod tempor incididunt labore et dolore magna aliqua.</p>
						<div class="email_subscribe">
							<form id="contact_form" pix-confirm="hidden_pix_7">
								<div id="result"></div>
								<span> <input type="email" name="email" id="email"  placeholder="Enter Email For Discount Code" class="pix_text"> </span>
								<button class="subscribe_btn submit_btn_landing pix_button_pro pix_inline_block" id="submit_btn_7" >
									<span class="editContent">SUBSCRIBE</span>
								</button>
							</form>
						</div>

				<p class="note_st editContent"> *Note: Bundle ends in two weeks from now.</p>
			</div>

			<div class="eight columns  omega">
				<div class="sold">
					<div class="center_img"><img src="/constructor/elements/images/7_shop_products/bag.png"  alt=""> </div>
					<div class="txt_sold">
						<p class="s_per editContent">50%</p>
						<p class="text_s editContent">SLAE OFF</p>
					</div>
				</div>

			</div>
		</div>
	</div><!-- container -->
</div>

</div>



<!-- ========================================================================================================= -->
<!-- The Confirmation Page Popup -->
<div id="hidden_pix_7" class="confirm_page confirm_page_7 pix_builder_bg">
<div class="pixfort_shop_7">
	<div class="confirm_header"><span class="editContent"><span class="pix_text">Thank You Very Much!</span></span></div>
	<div class="sub_text">
		<span class="editContent"><span class="pix_text">Your Discount Code</span></span>
	</div>
	<div class="code_text ">
		<span class="box_div pix_builder_bg"><span class="editContent"><span class="pix_text">FLATPACK2014</span></span></span>
	</div>
	<div class="confirm_text">
		<span class="editContent"><span class="pix_text">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</span></span>
	</div>
	<div class="center_text padding_bottom_60">
        <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
            <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
            <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
            <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
        </ul>
    </div>
</div>
</div>
<!-- ========================================================================================================= -->
<?include 'layout/footer.php';?>

