<?include 'layout/header.php';?>
	<div class="pixfort_shop_7" id="section_shop_4">

		<div class="new_story pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="eight columns alpha">
						<div class="zone_left">
							<div class="icon-1_st"> <img src="/constructor/elements/images/7_shop_products/icon-1.png" class="icon_st" alt=""></div>
							<div class="text_st">
								<p class="title_st editContent">High Quality Tutorials</p>
								<p class="subject_st editContent">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p>
							</div>
						</div>

						<div class="clearfix"></div>

						<div class="zone_left">
							<div class="icon-2_st"> <img src="/constructor/elements/images/7_shop_products/icon-2.png" class="icon_st" alt=""></div>
							<div class="text_st">
								<p class="title_st editContent">New Latest Offers</p>
								<p class="subject_st editContent">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p>
							</div>
						</div>
						<div class="clearfix"></div>

						<div class="zone_left">
							<div class="icon-3_st"> <img src="/constructor/elements/images/7_shop_products/icon-3.png" class="icon_st" alt=""></div>
							<div class="text_st">
								<p class="title_st editContent">Best Bundle Ever</p>
								<p class="subject_st editContent">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p>
							</div>
						</div>
					</div>

					<div class="eight columns omega">
						<div class="pict_st">
							<img src="/constructor/elements/images/7_shop_products/ipad.png"  alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

