<?include 'layout/header.php';?>
	<div class="pixfort_ecourse_8" id="section_ecourse_4">
		<div class="bundle_section pix_builder_bg">
			<div class="container">
				<div class="sixteen columns bg_bundle">
					<h3 class="what_st editContent">What You Are Going To Have</h3>
					<p class="great_st editContent">great prices for great tutorials</p>
					<div class="one-third column alpha">
						<div class="onethird_st">
							<div class="icon-1_st"> <img src="/constructor/elements/images/8_ecourse/icon-1.png" class="icon_st" alt=""></div>
							<p class="T1 editContent">High Quality Tutorials</p>
							<p class="T2 editContent">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p>
						</div>
					</div>
					<div class="one-third column ">
						<div class="onethird_st">
							<div class="icon-2_st"> <img src="/constructor/elements/images/8_ecourse/icon-2.png" class="icon_st" alt=""></div>

							<p class="T1 editContent">Best Bundle Ever</p>
							<p class="T2 editContent">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p>
						</div>
					</div>
					<div class="one-third column omega">
						<div class="onethird_st">
							<div class="icon-3_st"> <img src="/constructor/elements/images/8_ecourse/icon-3.png" class="icon_st" alt=""></div>
							<p class="T1 editContent">Latest New Offers</p>
							<p class="T2 editContent">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="sixteen columns bg_Quality">
					<div class="text_zone">
						<h3 class="TT1 editContent">High Quality Tutorials</h3>
						<p class="TT2 editContent">Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt.</p>
						<div class="get_tut_btn editContent">  <a href="#" class="pix_builder_bg2">Get Tutorial</a>  </div>
					</div>
					<div class="picture_zone">
						<img src="/constructor/elements/images/8_ecourse/people.png"  alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

