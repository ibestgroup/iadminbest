<?include 'layout/header.php';?>
	<div class="pixfort_ecourse_8" id="section_ecourse_6">
		<div class="envato_unbounce pix_builder_bg">
			<div class="container">
				<div class="sixteen columns cadre_st">
					<h3 class="L1_style editContent">What our awesome clients say</h3>
					<h6 class="L3_style editContent">great words from great people</h6>
					<div class="eight columns  alpha ">
						<div class="box_1 pix_builder_bg">
							<div class="b_style">
								<img src="/constructor/elements/images/testimonials/envato-logo.png" class="logo_style" alt="">
							</div>
							<div class="txt_style">
								<span class="editContent"><span class="pix_text">Im a web designer, you guys are very inspiring I wish to see more work from maybe more freebies.</span></span>
								<br><br>
								<img src="/constructor/elements/images/8_ecourse/stars.original.png" class="stars_st" alt="">
								<span class="env_st"><span class="editContent"><span class="pix_text">via Envato.com</span></span></span>
							</div>
						</div>
					</div>
					<div class="eight columns  omega">
						<div class="box_2 pix_builder_bg">
							<div class="b_style">
								<img src="/constructor/elements/images/testimonials/unboune-logo.png" class="logo_style" alt="">
							</div>
							<div class="txt_style">
								<span class="editContent"><span class="pix_text">Great service with fast and relible support The design work and detail put into themes are very great.</span></span>
								<br><br>
								<img src="/constructor/elements/images/8_ecourse/stars.original.png" class="stars_st" alt="">
								<span class="env_st"><span class="editContent"><span class="pix_text">via Unbounce.com</span></span></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

