<?include 'layout/header.php';?>
	<div class="pixfort_ebook_9" id="section_ebook_1">
		<div class="page_style pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="eight columns  alpha">
						<div class="logo"> <img src="/constructor/elements/images/9_ebook/logo.png"  alt=""> </div>
						<div class="text_input">
							<div class="headtext_style"><span class="editContent"><span class="pix_text">Get The eBook For FREE!</span></span></div>
							<div class="segment pix_builder_bg"></div>
							<p class="subtext_style editContent">Just Subscribe to get full access to our new tutorial + all attachments files.</p>
							<div class="contact_st">
								<form id="contact_form" pix-confirm="hidden_pix_9">
									<div id="result"></div>
									<input type="text" name="name" id="name" placeholder="Enter Your Full Name" class="pix_text">
									<input type="email" name="email" id="email" placeholder="Enter Your Email" class="pix_text">
									<button class="subscribe_btn pix_button_pro" id="submit_btn_9">
										<span class="editContent">SUBSCRIBE TO DOWNLOAD</span>
									</button>
								</form>
							</div>
							<div class="htext_style"><span class="editContent"><span class="pix_text">Stay connected</span></span></div>

							<div class="soc_logos">
								<div class="center_text padding_bottom_60">
					                <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
					                    <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
					                    <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
					                    <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
					                </ul>
					            </div>
							</div>
						</div>
					</div>
					<div class="eight columns  omega">
						<img src="/constructor/elements/images/9_ebook/book.png" class="book_pict" alt="">
					</div>
					<img src="/constructor/elements/images/9_ebook/trees.png" class="trees" alt="">
				</div>
			</div>
		</div>
	</div>
	<div id="hidden_pix_9" class="confirm_page confirm_page_9 pix_builder_bg">
		<div class="pixfort_ebook_9">
			<div class="confirm_header"><span class="editContent"><span class="pix_text">Thank You Very Much!</span></span></div>
			<div class="sub_text">
				<span class="editContent"><span class="pix_text">Download Tutorial: 4.5 MB</span></span>
			</div>
			<div class="code_text">
				<a href="#" class="download_button pix_builder_bg"><span class="editContent"><span class="pix_text">Download Now</span></span></a>
			</div>
			<div class="confirm_text">
				<span class="editContent"><span class="pix_text">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</span></span>
			</div>
			<div class="center_text padding_bottom_60">
                <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
                    <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
                </ul>
            </div>
		</div>
	</div>
	<div class="pixfort_ebook_9">
		<div class="chapters pix_button_pro">
			<div class="container">
				<div class="sixteen columns">
					<div class="one-third column alpha">
						<h3 class="title_chapter editContent">1. First FlatPack Chapter</h3>
						<h6 class="text_chapter editContent">Just Subscribe to get full access to our new tutorial + all attachments files.</h6>
					</div>
					<div class="one-third column">
						<h3 class="title_chapter editContent">2. Second FlatPack Chapter</h3>
						<h6 class="text_chapter editContent">Just Subscribe to get full access to our new tutorial + all attachments files.</h6>
					</div>
					<div class="one-third column omega">
						<h3 class="title_chapter editContent">3. Third FlatPack Chapter</h3>
						<h6 class="text_chapter editContent">Just Subscribe to get full access to our new tutorial + all attachments files.</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

