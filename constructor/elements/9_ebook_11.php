<?include 'layout/header.php';?>
		<div class="pixfort_ebook_9" id="section_ebook_11">
			<div class="page_style">
				<div class="container">
					<div class="sixteen columns">
						<div class="eight columns  alpha">
							<div class="logo"> <img src="/constructor/elements/images/9_ebook/logo.png"  alt=""> </div>
							<div class="text_input">
								<div class="headtext_style">Get The eBook For FREE!</div>
								<div class="segment"> </div>
								<p class="subtext_style">Just Subscribe to get full access to our new tutorial + all attachments files.</p>
								<div class="contact_st">
									<fieldset id="contact_form">
										<div id="result"></div>

										<input type="text" name="name" id="name" placeholder="Enter Your Full Name">

										<input type="email" name="email" id="email" placeholder="Enter Your Email">
										<button class="subscribe_btn" id="submit_btn_9">SUBSCRIBE TO DOWNLOAD</button>
									</fieldset>
								</div>
								<div class="htext_style">Stay connected</div>

								<div class="soc_logos">
									<a href="https://twitter.com/share" class=
									"twitter-share-button" data-via="pixfort" data-count="none">
									Tweet
								</a>
								&nbsp;&nbsp;&nbsp;
								<!-- Place this tag where you want the +1 button to render. -->
					  				<!-- <span class="confirm_gp">
										  <div class="g-plusone " data-size="medium" data-annotation="none"></div>
									  </span>

									  <!== Place this tag after the last +1 button tag. -->
									  <iframe src=
									  "https://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fthemeforest.net%2Fuser%2FPixFort&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35&amp;appId=445119778844521"
									  class="c1">
									</iframe>
								</div>
							</div>
						</div>
						<div class="eight columns  omega">
							<img src="/constructor/elements/images/9_ebook/book.png" class="book_pict" alt="">
						</div>
						<img src="/constructor/elements/images/9_ebook/trees.png" class="trees" alt="">
					</div>
				</div>
			</div>
		</div>
		<div id="hidden_pix_9" class="confirm_page confirm_page_9">
			<div class="pixfort_ebook_9">
				<div class="confirm_header">Thank You Very Much!</div>
				<div class="sub_text">
					Download Tutorial: 4.5 MB
				</div>
				<div class="code_text">
					<a href="#" class="download_button">Download Now</a>
				</div>
				<div class="confirm_text">
					Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.
				</div>
				<div class="confirm_social">
					<div class="confirm_social_box">
						<a href="https://twitter.com/share" class="twitter-share-button" data-via="pixfort" data-count="none">Tweet</a>
						<!-- Place this tag where you want the +1 button to render. -->
						<span class="confirm_gp">
							<div class="g-plusone " data-size="medium" data-annotation="none"></div>
						</span>
						<!-- Place this tag after the last +1 button tag. -->
						<div class="fb-like" data-href="http://themeforest.net/item/flatpack-landing-pages-pack-with-page-builder/10591107" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
					</div>
				</div>
			</div>
		</div>
<?include 'layout/footer.php';?>

