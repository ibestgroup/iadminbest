<?include 'layout/header.php';?>
	<div class="pixfort_ebook_9" id="section_ebook_3">
		<div class="amazing_ebook pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<div class="eight columns alpha">
						<h3 class="TT1 editContent">Your amazing eBook is here</h3>
						<h4 class="TT2 editContent">Our service is astonishingly thin and light.</h4>
						<h5 class="TT3 editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h5>
						<div class="down_box pix_text">
							<span class=" editContent">
								<span class="download_st">Total Downloads</span>
								<span class="times_st">+3520 Times</span>
							</span>
						</div>
					</div>
					<div class="eight columns omega">
						<div class="videoWrapper">
							<iframe src="https://player.vimeo.com/video/102732914?title=0&amp;byline=0&amp;portrait=0&amp;color=ff2a68&amp;wmode=opaque" width="470" height="264" ></iframe>
							<div class="frameCover" data-type="video" data-selector=".frameCover"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

