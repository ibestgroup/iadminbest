<?include 'layout/header.php';?>
	<div class="pixfort_ebook_9" id="section_ebook_4">
		<div class="writers_section pix_builder_bg">
			<div class="container">
				<div class="sixteen columns">
					<h2 class="title_st editContent">Who Wrote The Book</h2>
					<h4 class="subtitle_st editContent">great prices for great tutorials</h4>
					<div class="eight columns alpha">
						<div class="b_style">
							<img src="/constructor/elements/images/9_ebook/author-1.png" class="logo_style" alt="">
						</div>
						<div class="author_text">
							<h1 class="name_st editContent">Nadeen Dorman</h1>
							<h6 class="job_st editContent">CEO & Founder at FLATPACK</h6>
							<p class="details_st editContent">I have found this ebook to in deliver detailed insights into a great topic this eBook is full of and the great instructional methods the and expertise displayed in this ebook has allowed me to grow my own great self business.</p>
							<div class="icons_st">
								<span><a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/yt.png"></a></span>
								<span><a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/twitter.png"></a></span>
								<span> <a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/facebook.png"></a></span>
							</div>
						</div>
					</div>
					<div class="eight columns omega">
						<div class="b_style">
							<img src="/constructor/elements/images/9_ebook/author-2.png" class="logo_style" alt="">
						</div>
						<div class="author_text">
							<h1 class="name_st editContent">Mark Hugman</h1>
							<h6 class="job_st editContent">Designer at FLATPACK</h6>
							<p class="details_st editContent">I have found this ebook to in deliver detailed insights into a great topic this eBook is full of and the great instructional methods the and expertise displayed in this ebook has allowed me to grow my own great self business.</p>

							<div class="icons_st">
								<span><a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/yt.png"></a></span>
								<span><a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/twitter.png"></a></span>
								<span> <a href="#" class="slow_fade social_button"><img src="/constructor/elements/images/social_icons/facebook.png"></a></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>

