<?include 'layout/header.php';?>
        <div class="big_padding pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                   <a href="#" class="pix_button small_button btn_normal"><i class="pi pixicon-check"></i> Button</a>
                   <a href="#" class="pix_button small_button btn_normal pix_button_flat2"><i class="pi pixicon-check"></i> Flat Button</a>
                   <a href="#" class="pix_button small_button pix_button_line"><i class="pi pixicon-check"></i> Line Button</a>

                   <a href="#" class="pix_button small_button btn_normal">Button</a>
                   <a href="#" class="pix_button small_button btn_normal pix_button_flat2">Flat Button</a>
                   <a href="#" class="pix_button small_button pix_button_line">Line Button</a>

                   <br>
                    <a href="#" class="pix_button btn_normal"><i class="pi pixicon-check"></i> Button</a>
                   <a href="#" class="pix_button btn_normal pix_button_flat2"><i class="pi pixicon-check"></i> Flat Button</a>
                   <a href="#" class="pix_button pix_button_line"><i class="pi pixicon-check"></i> Line Button</a>
                   <br>
                   <a href="#" class="pix_button btn_normal big_text bold_text btn_big"><i class="pi pixicon-params"></i> Button</a>
                   <a href="#" class="pix_button btn_normal pix_button_flat2 big_text btn_big bold_text"><i class="pi pixicon-check"></i> Flat Button</a>
                   <a href="#" class="pix_button pix_button_line big_text bold_text btn_big"><i class="pi pixicon-check"></i> Line Button</a>
                   <br>
                   <a href="#" class="pix_button btn_normal normal_text bold_text"><i class="pi pixicon-check"></i> Button</a>
                   <a href="#" class="pix_button btn_normal pix_button_flat2 normal_text bold_text"><i class="pi pixicon-check"></i> Flat Button</a>
                   <a href="#" class="pix_button pix_button_line normal_text bold_text"><i class="pi pixicon-check"></i> Line Button</a>
                   <br>
                   <a href="#" class="pix_button btn_normal normal_text bold_text">Button <i class="pi pixicon-paperplane p_right"></i></a>
                   <a href="#" class="pix_button btn_normal pix_button_flat2 normal_text bold_text"> Flat Button <i class="pi pixicon-params p_right"></i></a>
                   <a href="#" class="pix_button pix_button_line normal_text bold_text">Line Button <i class="pi pixicon-check p_right"></i></a>
                   <br>
                   <a href="#" class="pix_button btn_normal blue_bg bold_text"><i class="pi pixicon-check"></i> Button</a>
                   <a href="#" class="pix_button btn_normal pix_button_flat blue_bg bold_text"><i class="pi pixicon-check"></i> Flat Button</a>
                   <a href="#" class="pix_button pix_button_line blue_border_button bold_text"><i class="pi pixicon-check"></i> Line Button</a>
                </div>
	       </div>
        </div>
    <div class="section_pointer" pix-name="main"></div>
<?include 'layout/footer.php';?>
