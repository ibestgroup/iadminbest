<?include 'layout/header.php';?>
	<div class="pix_call_1 dark pix_builder_bg" id="section_call_1_dark">
		<div class="container ">
			<div class="two-thirds column pix_area1">
				<div class="call_text">
					<h2 class="editContent">Call to action text goes here</h2>
				</div>
			</div>
			<div class="one-third column pix_area2">
				<a href="#" class="main_button pix_text"><span class="editContent">CALL US NOW</span></a>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>
