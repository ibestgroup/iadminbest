<?include 'layout/header.php';?>
    <div class="pix_builder_bg" id="section_call_2">
        <div class="footer3">
            <div class="container ">
                <div class="ten columns alpha offset-by-three">
                    <div class="content_div center_text">
                        <div class="margin_bottom_30 green_1">
                            <i class="pi pixicon-command title_56"></i> 
                        </div>
                        <div class="margin_bottom_30">
                            <p class="big_title bold_text editContent">CTA Section Element</p>
                            <p class="normal_text light_gray center_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmoda tempor incididunt dolor sit amet consectetur.</p>
                        </div>
                        <a href="#" class="pix_button pix_button_line bold_text green_1 btn_big">
                            <span>CALL TO ACTION BUTTON</span>
                        </a>    
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
