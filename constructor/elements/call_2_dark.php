<?include 'layout/header.php';?>
    <div class="dark pix_builder_bg" id="section_call_2_dark">
        <div class="footer3">
            <div class="container ">
                <div class="ten columns alpha offset-by-three">
                    <div class="content_div center_text">
                        <div class="margin_bottom_30 orange">
                            <i class="pi pixicon-video2 title_56"></i> 
                        </div>
                        <div class="margin_bottom_30">
                            <p class="big_title bold_text editContent">Dark CTA Section</p>
                            <p class="normal_text light_gray center_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmoda tempor incididunt dolor sit amet consectetur.</p>
                        </div>
                        <a href="#" class="pix_button pix_button_line bold_text orange btn_big">
                            <span>CALL TO ACTION BUTTON</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
