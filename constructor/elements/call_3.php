<?include 'layout/header.php';?>
	<div class="pix_call_1 pix_builder_bg" id="section_call_3">
		<div class="container ">
			<div class="two-thirds column pix_area1">
				<div class="call_text">
					<h2 class="editContent"><strong>Awesome light CTA Section</strong></h2>
					<p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmoda tempor.</p>
				</div>
			</div>
			<div class="one-third column pix_area2">
				<a href="#" class="pix_button pix_button_line orange btn_big">
					<i class="pi pixicon-mail"></i> 
                    <span><strong>CALL TO ACTION BUTTON</strong></span>
                </a>    
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>
