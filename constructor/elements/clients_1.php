<?include 'layout/header.php';?>
    <div class="" id="section_clients_1">  
        <div class="big_padding pix_builder_bg">
            <div class="container">
                <div class="sixteen columns center_text">
                    <p class="normal_title editContent">
                        <strong>Take a Quick Look</strong>
                    </p>
                    <span class="pix_text normal_gray">
                        <span class="normal_text editContent">
                            The Best Companies Which Trust Our Digital Goods
                        </span>
                    </span>
                </div>
                <div class="pix_inline_block center_text">
                    <div class="sixteen columns ">
                        <div class="one-third column alpha">
                            <img src="/constructor/elements/images/main/logos/theguardian.png" class="margin_vertical" alt="">
                        </div>
                        <div class="one-third column">
                            <img src="/constructor/elements/images/main/logos/taskrabbit.png" class="margin_vertical" alt="">
                        </div>
                        <div class="one-third column omega">
                            <img src="/constructor/elements/images/main/logos/popsugar.png" class="margin_vertical" alt="">
                        </div>
                       
                    </div>
                    <div class="sixteen columns">
                        <div class="one-third column alpha">
                            <img src="/constructor/elements/images/main/logos/bigcommerce.png" class="margin_vertical" alt="">
                        </div>
                        <div class="one-third column">
                            <img src="/constructor/elements/images/main/logos/foursquare.png" class="margin_vertical" alt="">
                        </div>
                        <div class="one-third column omega">
                            <img src="/constructor/elements/images/main/logos/squarespace.png" class="margin_vertical" alt="">
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
