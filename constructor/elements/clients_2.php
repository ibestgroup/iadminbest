<?include 'layout/header.php';?>
    <div class="" id="section_clients_2">  
        <div class="big_padding pix_builder_bg">
            <div class="container">
                <div class="pix_inline_block ">
                    <div class="sixteen columns ">
                        <div class="four columns alpha">
                            <p class="big_text editContent">How Trust PixFort</p>
                            <span class="pix_text normal_gray">
                                <p class="normal_text editContent">
                                    The Best Companies Which Trust Our Digital Goods
                                </p>
                            </span>
                        </div>
                        <div class="four columns center_text">
                            <img src="/constructor/elements/images/main/logos/bigcommerce.png" class="margin_vertical" alt="">
                        </div>
                        <div class="four columns center_text">
                            <img src="/constructor/elements/images/main/logos/foursquare.png" class="margin_vertical" alt="">
                        </div>
                        <div class="four columns omega center_text">
                            <img src="/constructor/elements/images/main/logos/squarespace.png" class="margin_vertical" alt="">
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
