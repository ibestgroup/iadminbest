<?php
include 'layout/header.php';
?>
<div class="pixfort_real_estate_4" id="section_real_estate_2">
    <div class="page_style pix_builder_bg">
        <div class="container">
            <div class="sixteen columns">
                <div class="text_page">
                    <img src="/constructor/elements/images/4_real_estate/home-logo.png" class="home_st" alt="">
                    <span class="editContent"><h1 class="start_style">СТАРТ ВАШЕГО САЙТА</h1></span>
                    <p class="txt_start">
                        <span class="editContent">Счетчик до старта сайта</span>
                    </p>
                    <div class="countdown js-countdown-1 text-center pix_edit" data-start="<?=$date_start?>">
                        <div class="time-circle pix_edit">
                            <div class="progress pix_edit"></div>
                            <span class="time days"></span>
                            <span class="time-name pix_edit">дней</span>
                        </div>
                        <span class="dots">:</span>
                        <div class="time-circle pix_edit">
                            <div class="progress pix_edit"></div>
                            <span class="time hours"></span>
                            <span class="time-name pix_edit">часов</span>
                        </div>
                        <span class="dots">:</span>
                        <div class="time-circle pix_edit">
                            <div class="progress pix_edit"></div>
                            <span class="time minutes"></span>
                            <span class="time-name pix_edit">минут</span>
                        </div>
                        <span class="dots">:</span>
                        <div class="time-circle pix_edit">
                            <div class="progress pix_edit"></div>
                            <span class="time seconds"></span>
                            <span class="time-name pix_edit">секунд</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?include 'layout/footer.php';?>

