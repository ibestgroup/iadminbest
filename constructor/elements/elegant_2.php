<?include 'layout/header.php';?>
    <div class="pixfort_agency_14 elegant_2" id="section_agency_1">
        <div class="page_style pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="text_page">
                        <h1 class="title">
                            <span class="editContent">NEW ELEGANT DESIGN</span>
                        </h1>
                        <div class="green_segment pix_builder_bg"></div>
                        <p class="subtitle editContent">
                                Lorem ipsum dolor amet consectur adipiscing elit sed do eiusmod tempor incididunt<br><br>
                                just announced three product updates on our new Releases blog channel
                        </p>
                    </div>
                    <div class="first_link">
                        <span class="get_1_btn">
                            <a class="slow_fade pix_text" href="#">
                                <span class="editContent">GET A FREE QUOTE</span>
                            </a>
                        </span>
                    </div>
                </div>
            </div><!-- container -->
        </div>
    </div>
    <div class="section_pointer" pix-name="18_elegant"></div>
<?include 'layout/footer.php';?>
