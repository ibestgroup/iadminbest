<?include 'layout/header.php';?>


        <div class="light_gray_bg pix_builder_bg" id="section_call_2">
            <div class="footer3">
                <div class="container ">
                    <div class="ten columns alpha offset-by-three">
                        <div class="content_div center_text">
                            <div class="margin_bottom_30 dark_red">
                                <i class="pi pixicon-clipboard title_56"></i> 
                            </div>
                            <div class="margin_bottom_30">
                                <p class="big_title bold_text editContent">Eventbrite Popup</p>
                                <p class="normal_text light_gray center_text editContent">This is an example section for popups layouts, you can use it as it is, or you can check our documentation to learn how to add popups to any button or link in your landing pages, it's easy as cup cakes!</p>
                                <p class="normal_text light_gray center_text editContent">
                                    You can also open this popup from any link in the page by adding the popup's id <strong>#popup_2</strong> in the link field.
                                </p>
                            </div>
                            <a href="#popup_2" class="pix_button pix_button_line bold_text dark_red btn_big">
                                <span>POPUP BUTTON</span>
                            </a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="popup_2" class="well pix_popup pop_style_1 pop_hidden light_gray_bg pix_builder_bg">
            <div style="width:700px;text-align:left;" >

                <!-- THE IFRAME CODE HERE -->
                <iframe src="//eventbrite.com/tickets-external?eid=21069420203&ref=etckt" frameborder="0" height="260" width="100%" vspace="0" hspace="0" marginheight="5" marginwidth="5" scrolling="auto" allowtransparency="true"></iframe>

            </div>
        </div>

<?include 'layout/footer.php';?>
