<?include 'layout/header.php';?>
    <div class="pixfort_normal_1" id="section_features_1">  
        <div class="features_1 pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="f_1_icon_box">
                                <span class="pi pixicon-heart3 creative_c_icon" id="ui-id-23"></span>
                            </div>
                            <div class="">
                                <span class="c1_style2"><span class="editContent"><span class="pix_text">Lovely Support</span></span></span><br>
                                <span class="c2_style2 editContent"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="f_1_icon_box">
                                <span class="pi pixicon-lab creative_c_icon" id="ui-id-23"></span>
                            </div>
                            <div class="">
                                <span class="c1_style2"><span class="editContent"><span class="pix_text">Great Experience</span></span></span><br>
                                <span class="c2_style2 editContent"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="f_1_icon_box">
                                <span class="pi pixicon-cup creative_c_icon" id="ui-id-23"></span>
                            </div>
                            <div class="">
                                <span class="c1_style2"><span class="editContent"><span class="pix_text">Coffee Cups</span></span></span><br>
                                <span class="c2_style2 editContent"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="f_1_icon_box">
                                <span class="pi pixicon-diamond creative_c_icon" id="ui-id-23"></span>
                            </div>
                            <div class="">
                                <span class="c1_style2"><span class="editContent"><span class="pix_text">Diamond Luxury</span></span></span><br>
                                <span class="c2_style2 editContent"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
