<?include 'layout/header.php';?>
    <div class="pixfort_normal_1 dark" id="section_features_1_dark">  
        <div class="features_1  pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="four columns  onethird_style alpha">
                        <div class="f1_box">
                            <div class="f_1_icon_box">
                                <span class="pi pixicon-settings creative_c_icon" id="ui-id-23"></span>
                            </div>
                            <div class="">
                                <span class="c1_style2"><span class="editContent"><span class="pix_text">PLACES</span></span></span><br>
                                <span class="c2_style2"><span class="editContent"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="f_1_icon_box">
                                <span class="pi pixicon-location2 creative_c_icon" id="ui-id-23"></span>
                            </div>
                            <div class="">
                                <span class="c1_style2"><span class="editContent"><span class="pix_text">PLACES</span></span></span><br>
                                <span class="c2_style2 editContent"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="f_1_icon_box">
                                <span class="pi pixicon-bubble creative_c_icon" id="ui-id-23"></span>
                            </div>
                            <div class="">
                                <span class="c1_style2"><span class="editContent"><span class="pix_text">PLACES</span></span></span><br>
                                <span class="c2_style2 editContent"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="f_1_icon_box">
                                <span class="pi pixicon-params creative_c_icon" id="ui-id-23"></span>
                            </div>
                            <div class="">
                                <span class="editContent"><span class="c1_style2"><span class="pix_text">PLACES</span></span></span><br>
                                <span class="c2_style2 editContent"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
