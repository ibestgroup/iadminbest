<?include 'layout/header.php';?>
    <div class="" id="section_features_2">  
        <div class="light_gray_bg big_padding pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon green_1">
                                <span class="pi pixicon-chat"></span>
                            </div>
                            <div class="center_text">
                                <span class="small_title bold_text"><span class="editContent"><span class="pix_text">Live Chat</span></span></span><br>
                                <span class="editContent light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon green_1">
                                <span class="pi pixicon-scissors"></span>
                            </div>
                            <div class="center_text">
                                <span class="small_title bold_text"><span class="editContent"><span class="pix_text">Easy Editing</span></span></span><br>
                                <span class="editContent light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon green_1">
                                <span class="pi pixicon-telescope"></span>
                            </div>
                            <div class="center_text">
                                <span class="small_title bold_text"><span class="editContent"><span class="pix_text">High Quality</span></span></span><br>
                                <span class="editContent light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon green_1">
                                <span class="pi pixicon-camera2"></span>
                            </div>
                            <div class="center_text">
                                <span class="small_title bold_text"><span class="editContent"><span class="pix_text">Instant Images</span></span></span><br>
                                <span class="editContent light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
