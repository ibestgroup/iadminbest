<?include 'layout/header.php';?>
    <div class="dark" id="section_features_2_dark">  
        <div class="big_padding pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon light_blue">
                                <span class="pi pixicon-video2"></span>
                            </div>
                            <div class="center_text">
                                <span class="small_title bold_text"><span class="editContent"><span class="pix_text">Video Support</span></span></span><br>
                                <span class="editContent light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon light_blue">
                                <span class="pi pixicon-map2"></span>
                            </div>
                            <div class="center_text">
                                <span class="small_title bold_text"><span class="editContent"><span class="pix_text">Map Location</span></span></span><br>
                                <span class="editContent light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon light_blue">
                                <span class="pi pixicon-layers2"></span>
                            </div>
                            <div class="center_text">
                                <span class="small_title bold_text"><span class="editContent"><span class="pix_text">Multiple Demos</span></span></span><br>
                                <span class="editContent light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon light_blue">
                                <span class="pi pixicon-tools"></span>
                            </div>
                            <div class="center_text">
                                <span class="small_title bold_text"><span class="editContent"><span class="pix_text">Premium Tools</span></span></span><br>
                                <span class="editContent light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies and interoperable internal.</span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
