<?include 'layout/header.php';?>
    <div class="" id="section_features_3">  
        <div class="big_padding pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="one-third column onethird_style alpha">
                        <div class="f1_box">
                            <div class="margin_bottom">
                                <img src="/constructor/elements/images/main/r1.png" alt="">
                            </div>
                            <div class="margin_bottom">
                                <span class="small_title bold_text"><span class="editContent"><span class="pix_text">Fast Food</span></span></span><br>
                                <span class="editContent light_gray normal_text"><span class="pix_text">Lorem ipsum dolor sit Seamlessly empower fully researched growth strategies interoperable internal.</span></span>
                            </div>
                            <a href="#" class="pix_button bold_text small_button pix_button_line dark_red">
                                <i class="pi pixicon-clipboard"></i>
                                <span>Order Meal</span>
                            </a>
                        </div>
                    </div>
                    <div class="one-third column onethird_style alpha">
                        <div class="f1_box">
                            <div class="margin_bottom">
                                <img src="/constructor/elements/images/main/r2.png" alt="">
                            </div>
                            <div class="margin_bottom">
                                <span class="small_title bold_text"><span class="editContent"><span class="pix_text">Drinks & Snacks</span></span></span><br>
                                <span class="editContent light_gray normal_text"><span class="pix_text">Lorem ipsum dolor sit Seamlessly empower fully researched growth strategies interoperable internal.</span></span>
                            </div>
                            <a href="#" class="pix_button bold_text small_button pix_button_line dark_red">
                                <i class="pi pixicon-tag"></i>
                                <span>Order Meal</span>
                            </a>
                        </div>
                    </div>
                    <div class="one-third column onethird_style alpha">
                        <div class="f1_box">
                            <div class="margin_bottom">
                                <img src="/constructor/elements/images/main/r3.png" alt="">
                            </div>
                            <div class="margin_bottom">
                                <span class="small_title bold_text"><span class="editContent"><span class="pix_text">Main Menu Food</span></span></span><br>
                                <span class="editContent light_gray normal_text"><span class="pix_text">Lorem ipsum dolor sit Seamlessly empower fully researched growth strategies interoperable internal.</span></span>
                            </div>
                            <a href="#" class="pix_button bold_text small_button pix_button_line dark_red">
                                <i class="pi pixicon-flag"></i>
                                <span>Order Meal</span>
                            </a>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
