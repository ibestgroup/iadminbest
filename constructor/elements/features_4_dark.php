<?include 'layout/header.php';?>
    <div class="dark" id="section_features_4_dark">
        <div class="big_padding pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="one-third column onethird_style alpha">
                        <div class="f1_box pix_container">
                            <div class="margin_bottom">
                                <img src="/constructor/elements/images/main/car1.png" class="feature_image" alt="">
                            </div>
                            <div class="margin_bottom">
                                <span class="small_title bold_text">
                                    <span class="editContent">
                                        <span class="pix_text">
                                            Mercedes S Class
                                        </span>
                                    </span>
                                </span>
                                <br>
                                <span class="editContent light_gray normal_text">
                                    <span class="pix_text">
                                        Lorem ipsum dolor sit Seamlessly empower fully researched growth.
                                        <br>
                                        <strong>Car Speed:</strong> 24Km/h<br>
                                        <strong>Seats Number:</strong> 7 Seats<br>
                                        <strong>Totla Price:</strong> $35,000<br>
                                    </span>
                                </span>
                            </div>
                            <a href="#" class="pix_button bold_text small_button btn_normal pix_button_flat dark_yellow_bg">
                                <i class="pi pixicon-ribbon"></i>
                                <span>Order Now</span>
                            </a>
                        </div>
                    </div>
                    <div class="one-third column onethird_style alpha">
                        <div class="f1_box pix_container">
                            <div class="margin_bottom">
                                <img src="/constructor/elements/images/main/car2.png" class="feature_image" alt="">
                            </div>
                            <div class="margin_bottom">
                                <span class="small_title bold_text">
                                    <span class="editContent">
                                        <span class="pix_text">
                                            Mercedes Benz CLA 
                                        </span>
                                    </span>
                                </span>
                                <br>
                                <span class="editContent light_gray normal_text">
                                    <span class="pix_text">
                                        Lorem ipsum dolor sit Seamlessly empower fully researched growth.
                                        <br>
                                        <strong>Car Speed:</strong> 24Km/h<br>
                                        <strong>Seats Number:</strong> 7 Seats<br>
                                        <strong>Totla Price:</strong> $35,000<br>
                                    </span>
                                </span>
                            </div>
                            <a href="#" class="pix_button bold_text small_button btn_normal pix_button_flat dark_yellow_bg">
                                <i class="pi pixicon-ribbon"></i>
                                <span>Order Now</span>
                            </a>
                        </div>
                    </div>
                    <div class="one-third column onethird_style alpha">
                        <div class="f1_box pix_container">
                            <div class="margin_bottom">
                                <img src="/constructor/elements/images/main/car3.png" class="feature_image" alt="">
                            </div>
                            <div class="margin_bottom">
                                <span class="small_title bold_text">
                                    <span class="editContent">
                                        <span class="pix_text">
                                            4x4 New Mercedes Benz
                                        </span>
                                    </span>
                                </span>
                                <br>
                                <span class="editContent light_gray normal_text">
                                    <span class="pix_text">
                                        Lorem ipsum dolor sit Seamlessly empower fully researched growth.
                                        <br>
                                        <strong>Car Speed:</strong> 24Km/h<br>
                                        <strong>Seats Number:</strong> 7 Seats<br>
                                        <strong>Totla Price:</strong> $35,000<br>
                                    </span>
                                </span>
                            </div>
                            <a href="#" class="pix_button bold_text small_button btn_normal pix_button_flat dark_yellow_bg">
                                <i class="pi pixicon-ribbon"></i>
                                <span>Order Now</span>
                            </a>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
