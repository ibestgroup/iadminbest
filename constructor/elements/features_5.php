<?include 'layout/header.php';?>
    <div class="" id="section_features_5">
        <div class="light_gray_bg big_padding pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon light_blue">
                                <span class="pi pixicon-profile-male"></span>
                            </div>
                            <div class="center_text">
                                <span class="normal_title bold_text"><span class="editContent"><span class="pix_text">+12K Clients</span></span></span><br>
                                <span class="editContent normal_text light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon orange">
                                <span class="pi pixicon-piechart"></span>
                            </div>
                            <div class="center_text">
                                <span class="normal_title bold_text"><span class="editContent"><span class="pix_text">+60K Sales</span></span></span><br>
                                <span class="editContent normal_text light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon green_1">
                                <span class="pi pixicon-wallet"></span>
                            </div>
                            <div class="center_text">
                                <span class="normal_title bold_text"><span class="editContent"><span class="pix_text">+3M Revenu</span></span></span><br>
                                <span class="editContent normal_text light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon dark_yellow">
                                <span class="pi pixicon-lightbulb"></span>
                            </div>
                            <div class="center_text">
                                <span class="normal_title bold_text"><span class="editContent"><span class="pix_text">+45 Projects</span></span></span><br>
                                <span class="editContent normal_text light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies.</span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
