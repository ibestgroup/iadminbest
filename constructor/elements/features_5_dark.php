<?include 'layout/header.php';?>
    <div class="" id="section_features_5_dark">
        <div class="dark big_padding pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon light_blue">
                                <span class="pi pixicon-content-right"></span>
                            </div>
                            <div class="center_text">
                                <span class="normal_title bold_text"><span class="editContent"><span class="pix_text">+8K Sites</span></span></span><br>
                                <span class="editContent normal_text light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon orange">
                                <span class="pi pixicon-microphone"></span>
                            </div>
                            <div class="center_text">
                                <span class="normal_title bold_text"><span class="editContent"><span class="pix_text">+15 Speakers</span></span></span><br>
                                <span class="editContent normal_text light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon green_1">
                                <span class="pi pixicon-share"></span>
                            </div>
                            <div class="center_text">
                                <span class="normal_title bold_text"><span class="editContent"><span class="pix_text">+1.4M likes</span></span></span><br>
                                <span class="editContent normal_text light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies.</span></span>
                            </div>
                        </div>
                    </div>
                    <div class="four columns onethird_style alpha">
                        <div class="f1_box">
                            <div class="big_icon dark_yellow">
                                <span class="pi pixicon-umbrella"></span>
                            </div>
                            <div class="center_text">
                                <span class="normal_title bold_text"><span class="editContent"><span class="pix_text">+75 Projects</span></span></span><br>
                                <span class="editContent normal_text light_gray"><span class="pix_text">Seamlessly empower fully researched growth strategies.</span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
