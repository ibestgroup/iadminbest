<?include 'layout/header.php';?>
    <div class="" id="section_features_6">
        <div class="big_padding pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="eight columns onethird_style alpha">
                        <div class="f1_box pix_container">
                            <div class="margin_bottom ">
                                <img src="/constructor/elements/images/main/1.jpg" class="feature_image pix_radius" alt="">
                            </div>
                            <div class="margin_bottom">
                                <span class="small_title bold_text">
                                    <span class="editContent">
                                        <span class="pix_text">
                                            First Title Goes Here
                                        </span>
                                    </span>
                                </span>
                                <br>
                                <span class="editContent light_gray normal_text">
                                    <span class="pix_text">
                                        Lorem ipsum dolor sit Seamlessly empower fully researched growth grand element.
                                    </span>
                                </span>
                            </div>
                            <a href="#" class="pix_button bold_text small_button  pix_button_line green_1">
                                <i class="pi pixicon-eye"></i>
                                <span>More Details</span>
                            </a>
                        </div>
                    </div>
                    <div class="eight columns onethird_style alpha">
                        <div class="f1_box pix_container">
                            <div class="margin_bottom">
                                <img src="/constructor/elements/images/main/2.jpg" class="feature_image" alt="">
                            </div>
                            <div class="margin_bottom">
                                <span class="small_title bold_text">
                                    <span class="editContent">
                                        <span class="pix_text">
                                            Second Title Goes Here
                                        </span>
                                    </span>
                                </span>
                                <br>
                                <span class="editContent light_gray normal_text">
                                    <span class="pix_text">
                                         Lorem ipsum dolor sit Seamlessly empower fully researched growth grand element.
                                    </span>
                                </span>
                            </div>
                            <a href="#" class="pix_button bold_text small_button  pix_button_line green_1">
                                <i class="pi pixicon-tag"></i>
                                <span>More Details</span>
                            </a>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
