<?include 'layout/header.php';?>
    <div class="pixfort_footer_1" id="section_footer_1">
        <div  class="new_footer_1 pix_footers pix_builder_bg">
            <div class="container ">
                <div class="sixteen columns bg_foot">
                    <div class="seven columns alpha desk_left">
                        <div class="footer_1_text">
                            <span class="editContent">
                                <span class="pix_text">All rights reserved Copyright © 2015 </span>
                                <strong><span class="pix_text">PixFort</span></strong>
                            </span>
                        </div>
                    </div>
                    <div class="nine columns omega desk_right">
                        <ul class="bottom-icons">
                            <li><a class="pi pixicon-facebook2" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-twitter2" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-instagram" href="#fakelink"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
