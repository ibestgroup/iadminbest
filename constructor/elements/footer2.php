<?include 'layout/header.php';?>
    <div class="pixfort_footer_1" id="section_footer_2">
        <div  class="new_footer_1 footer2 pix_footers pix_builder_bg">
            <div class="container ">
                    <div class="five columns alpha">
                        <div class="footer_1_text area_1">
                            <img src="/constructor/elements/images/main/footer-logo-light.png" class="footer2_logo" alt="">
                            <span class="editContent">
                                <span class="pix_text">
                                    <span class="tagline">© Copyright 2015</span>
                                </span>
                            </span>
                        </div>
                    </div>
                    <div class="eight columns footer2_center">
                        <ul class="footer2_menu">
                            <li>
                                <a href="#">item 1</a>
                                <a href="#">item 1</a>
                                <a href="#">item 1</a>
                            </li>
                        </ul>
                    </div>
                    <div class="three columns omega desk_right">
                        <ul class="bottom-icons">
                            <li><a class="pi pixicon-facebook2" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-twitter2" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-instagram" href="#fakelink"></a></li>
                        </ul>
                    </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
