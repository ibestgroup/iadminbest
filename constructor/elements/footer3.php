<?include 'layout/header.php';?>
    <div class="pixfort_text_4 pix_builder_bg" id="section_text_4">
        <div class="footer3">
        <div class="container ">
            <div class="five columns alpha">
            	<div class="content_div area_1">
                    <img src="/constructor/elements/images/main/footer-logo-3.png" class="pix_footer_logo" alt="">
                	<p class="small_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmoda tempor incididunt.</p>
                    <ul class="bottom-icons">
                        <li><a class="pi pixicon-facebook2" href="#fakelink"></a></li>
                        <li><a class="pi pixicon-twitter2" href="#fakelink"></a></li>
                        <li><a class="pi pixicon-instagram" href="#fakelink"></a></li>
                    </ul>
                </div>
            </div>
            <div class="three columns">
				<div class="content_div area_2">
					<span class="pix_text"><span class="editContent footer3_title">Navigation</span></span>
                	<ul class="footer3_menu">
                        <li><a href="#" class="pix_text"><span class="editContent">Home</span></a></li>
                        <li><a href="#" class="pix_text"><span class="editContent">Overview</span></a></li>
                        <li><a href="#" class="pix_text"><span class="editContent">About</span></a></li>
                        <li><a href="#" class="pix_text"><span class="editContent">Buy now</span></a></li>
                        <li><a href="#" class="pix_text"><span class="editContent">support</span></a></li>
                    </ul>
                </div>
            </div>
            <div class="four columns">
				<div class="content_div area_3">
					<span class="pix_text"><span class="editContent big_number">347 567 78 90</span></span>
                    <span class="pix_text"><span class="editContent small_bold light_color">AVAILABLE FROM 12PM - 18PM</span></span>
                    <h4 class="editContent med_title">New York, NY</h4>
                	<p class="editContent small_bold">560 Judah St & 15th Ave, Apt 5 San Francisco, CA, 230903</p>
                </div>
            </div>
            <div class="four columns omega">
            	<div class="content_div">
            		<span class="pix_text"><span class="editContent footer3_title">Info</span></span>
                	<p class="editContent ">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt Lorem ipsum dolor sit amet consectetur adipiscing elit sed.</p>
                </div>
            </div>
        </div>
    </div>
    </div>
<?include 'layout/footer.php';?>
