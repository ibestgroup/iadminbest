<?include 'layout/header.php';?>
    <div class="pix_builder_bg dark" id="section_footer_4_dark">
        <div class="footer3">
            <div class="container ">
                <div class="six columns alpha offset-by-five">
                    <div class="content_div center_text">
                        <img src="/constructor/elements/images/main/footer-logo-4-dark.png" class="pix_footer_logo" alt="">
                        <p class="small_text light_gray center_text editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmoda tempor incididunt.</p>
                        <ul class="bottom-icons center_text big_title ">
                            <li><a class="pi pixicon-facebook6 normal_gray" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-twitter6 normal_gray" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-googleplus7 normal_gray" href="#fakelink"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
