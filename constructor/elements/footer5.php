<?include 'layout/header.php';?>
    <div class="pix_builder_bg" id="section_footer_5">
        <div class="footer3">
            <div class="container ">
                <div class="sixteen columns ">
                    <div class="one-third column alpha mobile_center margin_vertical ">
                        <div class="pix_div_fit">
                            <img src="/constructor/elements/images/main/footer-logo-4.png" class="pix_footer_logo pix_img_fit" alt="">
                        </div>
                    </div>
                    <div class="one-third column margin_vertical">
                        <p class="small_text light_gray left_text mobile_center editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmoda tempor incididunt consectetur adipiscing elit.</p>
                    </div>
                    <div class="one-third column omega right_text margin_vertical mobile_center">
                        <ul class="bottom-icons center_text big_title">
                            <li><a class="pi pixicon-facebook6 normal_gray" href="#fakelink"></a> </li>
                            <li><a class="pi pixicon-twitter6 normal_gray" href="#fakelink"></a> </li>
                            <li><a class="pi pixicon-googleplus7 normal_gray" href="#fakelink"></a> </li>
                        </ul>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
