<?include '../layout/header.php';?>
    <div class="big_padding pix_builder_bg dark form_1_bg" id="section_normal_4_1">
        <div class="container">
            <div class="sixteen columns">
                <div class="ten columns alpha padding_top_60">
                    <p class="big_title editContent "><strong>Cutting Edge HTML Form Builder</strong></p>
                    <p class="normal_text normal_gray editContent">
                        Eventbrite is the world's largest self-service ticketing platform. We build the technology to allow anyone to create, share, find and attend new things to do that fuel their passions and enrich their lives.
                    </p>
                </div>

                <div class="six columns omega">
                    <div class="">
                        <div class="substyle pix_builder_bg">
                            <form id="form_1" class="pix_form p-form js-form" data-type="register">
                                <div id="result"></div>
                                <input type="text" name="login" id="login" placeholder="Логин" class="pix_text">
                                <input type="email" name="email" id="email" placeholder="Ваш Email" class="pix_text">
                                <input type="password" name="password" id="password" placeholder="Ваш пароль" class="pix_text">
                                <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Повторите пароль" class="pix_text">
                                <input type="checkbox" name="terms" required="" class="pix_edit" title="You should accept the terms of use."><span class="label_span editContent pix_edit">
                                    Accept <a href="#" class="pix_edit"></a>Terms of Use. </span><br>
                                <span class="send_btn">
                                    <button type="submit" class="submit_btn orange_bg pix_text">
                                        <span class="editContent">Зарегистрироваться</span>
                                    </button>
                                </span>
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
       </div>
    </div>
<?include '../layout/footer.php';?>
