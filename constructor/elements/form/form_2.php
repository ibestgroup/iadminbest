<?include '../layout/header.php';?>
    <div class="extra_padding pix_builder_bg dark form_2_bg">
        <div class="container">
            <div class="sixteen columns">
                <div class="six columns alpha">
                    <div class="">
                        <div class="substyle pix_builder_bg">
                            <form id="form_1" class="pix_form p-form js-form" data-type="register">
                                <div id="result"></div>
                                <input type="text" name="login" id="login" placeholder="Логин" class="pix_text">
                                <input type="email" name="email" id="email" placeholder="Ваш Email" class="pix_text">
                                <input type="password" name="password" id="password" placeholder="Ваш пароль" class="pix_text">
                                <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Повторите пароль" class="pix_text">
                                <input type="checkbox" name="terms" required="" class="pix_edit" title="You should accept the terms of use."><span class="label_span editContent pix_edit">
                                    Accept <a href="#" class="pix_edit"></a>Terms of Use. </span><br>
                                <span class="send_btn">
                                    <button type="submit" class="submit_btn green_1_bg pix_text">
                                        <span class="editContent">Зарегистрироваться</span>
                                    </button>
                                </span>
                                <span class="pix_text"><span class="pix_note editContent">*Some awesome dummy text goes here.</span></span>
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="ten columns omega padding_top_30">
                    <p class="big_title editContent "><strong>Productive & extremely Fast Form Builder!</strong></p>
                    <p class="normal_text normal_gray editContent">
                        Eventbrite is the world's largest self-service ticketing platform. We build the technology to allow anyone to create, share, find and attend new things to do that fuel their passions and enrich their lives.
                    </p>
                </div>
            </div>
       </div>
    </div>
<?include '../layout/footer.php';?>
