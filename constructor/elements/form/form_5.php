<?include '../layout/header.php';?>
        <div class="pixfort_university_6" id="section_university_5">
            <div class="join_us_section pix_builder_bg">
               <div class="container">
                <div class="sixteen columns">
                    <div class="ten columns  alpha">
                        <div class="zone_left">
                            <div class="psingle_item">
                             <div class="icon_st"> <img src="/constructor/elements/images/6_university/icon_1.png"  alt=""></div>
                             <div class="text_st">
                                <h3 class="title_st editContent">High Quality Tutorials</h3>
                                <h6 class="subject_st editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</h6>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="psingle_item">
                         <div class="icon_st"> <img src="/constructor/elements/images/6_university/icon_2.png"  alt=""></div>
                         <div class="text_st">
                            <p class="title_st editContent">New Latest Offers</p>
                            <p class="subject_st editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="psingle_item">
                     <div class="icon_st"> <img src="/constructor/elements/images/6_university/icon_3.png"  alt=""></div>
                     <div class="text_st">
                        <p class="title_st editContent">The Best Bundle Ever</p>
                        <p class="subject_st editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="six columns omega">
            <div class="substyle pix_builder_bg">
                <div class="title-style"><span class="editContent"><span class="pix_text">Зарегистрироваться</span></span></div>
                <br>
                <div class="text-style">
                  <span class="editContent"><span class="pix_text">Lorem ipsum dolor sit amet consectetur.</span></span>
              </div><br>
              <div class="clearfix"></div>
                <form id="contact_form" class="contact_form p-form js-form" data-type="register">
                    <div id="result"></div>
                    <input type="text" name="login" id="login" placeholder="Логин" class="pix_text">
                    <input type="email" name="email" id="email" placeholder="Ваш Email" class="pix_text">
                    <input type="password" name="password" id="password" placeholder="Ваш пароль" class="pix_text">
                    <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Повторите пароль" class="pix_text"><input type="checkbox" name="terms" required="" class="pix_edit" title="You should accept the terms of use."><span class="label_span editContent pix_edit">
                                    Accept <a href="#" class="pix_edit"></a>Terms of Use. </span><br>
                    <span class="send_btn">
                        <button type="submit" class="submit_btn orange_bg pix_text">
                            <span class="editContent">Зарегистрироваться</span>
                        </button>
                    </span>
                    <span class="pix_text"><span class="pix_note editContent">*Some awesome dummy text goes here.</span></span>
                </form>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</div><!-- container -->
</div>
</div>
<?include '../layout/footer.php';?>

