<?include '../layout/header.php';?>
	<div class="pixfort_business_10" id="section_business_5">
		<div class="why_section pix_builder_bg">
			<div class="container">
				<div class="seven center omega">
					<div class="text_input">
						<div class="headtext_style"><span class="editContent"><span class="pix_text">Авторизоваться</span></span></div>
						<div class="contact_st">
                            <form id="contact_form" class="pix_form p-form js-form" data-type="auth">
                                <div id="result"></div>
                                <input type="text" name="login" id="login" placeholder="Логин" class="pix_text">
                                <input type="password" name="password" id="password" placeholder="Ваш пароль" class="pix_text">
                                <span class="send_btn">
                                    <button type="submit" class="submit_btn subscribe_btn pix_text">
                                        <span class="editContent">Авторизоваться</span>
                                    </button>
                                </span>
                            </form>
		  				</div>
						<h6 class="note_st editContent">*Note: Bundle ends in two weeks from now.</h6>
					</div>
				</div>
			</div>
		</div>
	</div>
<?include '../layout/footer.php';?>

