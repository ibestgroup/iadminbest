<?include '../layout/header.php';?>
<div class="pixfort_gym_13 pix_builder_bg" id="section_gym_5">
    <div class="join_us_section">
        <div class="container">
            <div class="sixteen columns">
                <div class="ten columns  alpha">
                    <div class="zone_left">
                        <div class="icon_st"> <img src="/constructor/elements/images/13_gym/heart.png"  alt=""></div>
                        <div class="text_st">
                            <p class="title_st"><span class="editContent">High Quality Tutorials</span></p>
                            <p class="subject_st"><span class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</span></p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="icon_st"> <img src="/constructor/elements/images/13_gym/water.png"  alt=""></div>
                        <div class="text_st">
                            <p class="title_st"><span class="editContent">New Latest Offers</span></p>
                            <p class="subject_st"><span class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</span></p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="icon_st"> <img src="/constructor/elements/images/13_gym/timer.png"  alt=""></div>
                        <div class="text_st">
                            <p class="title_st"><span class="editContent">The Best Bundle Ever</span></p>
                            <p class="subject_st"><span class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</span></p>
                        </div>
                    </div>
                </div>

                <div class="six columns omega">
                    <div class="pix_form_area">
                        <div class="substyle pix_builder_bg">
                            <div class="title-style"><span class="editContent"><span class="pix_text">Зарегистрироваться</span></span></div>
                            <br>
                            <div class="text-style">
                                <span class="editContent"><span class="pix_text">Зарегистрируйтесь на сайте и получите доступ к личному кабинету.</span></span>
                            </div><br>

                            <div class="clearfix"></div>

                            <form id="contact_form" class="pix_form p-form js-form" data-type="register">
                                <div id="result"></div>
                                <input type="text" name="login" id="login" placeholder="Логин" class="pix_text">
                                <input type="email" name="email" id="email" placeholder="Ваш Email" class="pix_text">
                                <input type="password" name="password" id="password" placeholder="Ваш пароль" class="pix_text">
                                <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Повторите пароль" class="pix_text">
                                <input type="checkbox" name="terms" required="" class="pix_edit" title="You should accept the terms of use."><span class="label_span editContent pix_edit">
                                    Accept <a href="#" class="pix_edit"></a>Terms of Use. </span><br>
                                <span class="send_btn">
                                    <button type="submit" class="submit_btn orange_bg pix_text">
                                        <span class="editContent">Зарегистрироваться</span>
                                    </button>
                                </span>
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?include '../layout/footer.php';?>

