<?include '../layout/header.php';?>
    <div class="pixfort_party_15" id="section_party_3">
        <div class="subscribe pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="text_input pix_builder_bg">
                        <div class="headtext_style">
                            <span class="editContent">
                                <span class="pix_text">
                                    Авторизоваться
                                </span>
                            </span>
                        </div>
                        <div class="segment pix_builder_bg"></div>
                        <span class="editContent">
                            <p class="subtext_style">
                                Авторизуйтесь на сайте используя ваш логин и пароль
                            </p>
                        </span>
                        <div class="contact_st">
                            <form id="contact_form" class="pix_form p-form js-form" data-type="auth">
                                <div id="result"></div>
                                <input type="text" name="login" id="login" placeholder="Логин" class="pix_text" required>
                                <input type="password" name="password" id="password" placeholder="Ваш пароль" class="pix_text" required>
                                <span class="send_btn">
                                    <button type="submit" class="submit_btn subscribe_btn pix_text">
                                        <span class="editContent">Авторизоваться</span>
                                    </button>
                                </span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    

<?include '../layout/footer.php';?>

