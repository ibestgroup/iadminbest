<?include 'layout/header.php';?>
		<div class="extra_padding pix_builder_bg dark form_2_bg" id="section_normal_4_1">
            <div class="container">
                <div class="sixteen columns">
                	<div class="six columns alpha">
						<div class="">
							<div class="substyle pix_builder_bg">
								<form id="contact_form" class="pix_form" pix-confirm="hidden_pix_4">
                                    <div id="result"></div>
                                    <input type="text" name="name" id="name" placeholder="Your Full Name" class="pix_text"> 
                                    <input type="email" name="email" id="email" placeholder="Your Email" class="pix_text">
                                    <textarea rows="5" name="message" class="text_area pix_text" placeholder="Write your Message Here"></textarea>
                                    <span class="send_btn">
                                        <button type="submit" class="submit_btn green_1_bg pix_text" id="submit_btn_6">
                                            <span class="editContent">Send Information</span>
                                        </button>
                                    </span>
                                    <span class="pix_text"><span class="pix_note editContent">*Some awesome dummy text goes here.</span></span>
                                </form>
                  				<div class="clearfix"></div>
              				</div>
						</div>
      				</div>
                    <div class="ten columns omega padding_top_30">
						<p class="big_title editContent "><strong>Productive & extremely Fast Form Builder!</strong></p>
                        <p class="normal_text normal_gray editContent">
                            Eventbrite is the world's largest self-service ticketing platform. We build the technology to allow anyone to create, share, find and attend new things to do that fuel their passions and enrich their lives.
                        </p>
					</div>
                </div>
	       </div>
        </div>
		<div id="hidden_pix_4" class="confirm_page confirm_page_4 green_1_bg pix_builder_bg ">
	        <div class="pixfort_real_estate_4">
	            <div class="confirm_header"><span class="editContent"><span class="pix_text">Thank You Very Much!</span></span></div>
	            <div class="confirm_text">
	                <span class="editContent"><span class="pix_text">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</span></span>
	            </div>
	            <div class="center_text padding_bottom_60">
	                <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
	                    <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
	                    <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
	                    <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
	                </ul>
	            </div>
	        </div>
	    </div>
<?include 'layout/footer.php';?>
