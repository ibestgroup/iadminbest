<?include 'layout/header.php';?>
    <div class="pixfort_university_6" id="section_university_5">
    <div class="join_us_section pix_builder_bg">
    	<div class="container">
            <div class="sixteen columns">
                <div class="ten columns  alpha">
                    <div class="zone_left">
                    <div class="psingle_item">
                       <div class="icon_st"> <img src="/constructor/elements/images/6_university/icon_1.png"  alt=""></div>
                       <div class="text_st">
                            <h3 class="title_st editContent">High Quality Tutorials</h3>
                            <h6 class="subject_st editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</h6>
                       </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="psingle_item">
                       <div class="icon_st"> <img src="/constructor/elements/images/6_university/icon_2.png"  alt=""></div>
                       <div class="text_st">
                            <p class="title_st editContent">New Latest Offers</p>
                            <p class="subject_st editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</p>
                       </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="psingle_item">
                       <div class="icon_st"> <img src="/constructor/elements/images/6_university/icon_3.png"  alt=""></div>
                       <div class="text_st">
                            <p class="title_st editContent">The Best Bundle Ever</p>
                            <p class="subject_st editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</p>
                       </div>
                    </div>
                    </div>
                </div>
                <div class="six columns omega">
                <div class="substyle pix_builder_bg">
                    <div class="title-style"><span class="editContent"><span class="pix_text">Join Us Now</span></span></div>
                    <br>
                    <div class="text-style">
                      <span class="editContent"><span class="pix_text">Lorem ipsum dolor sit amet consectetur.</span></span>
                    </div><br>
                    <div class="clearfix"></div>
                    <form action="http://www.ivoipmarket.com/svmailmaster/index.php/lists/js5201vkmc83c/subscribe" method="post" accept-charset="utf-8" target="_blank" id="contact_form">

                        <div class="form-group">
                        <label>Email <span class="required">*</span></label>
                        <input type="text" class="form-control" name="EMAIL" placeholder="" value="" required />
                        </div>

                        <div class="form-group">
                        <label>First name</label>
                        <input type="text" class="form-control" name="FNAME" placeholder="" value=""/>
                        </div>

                        <div class="form-group">
                        <label>Last name</label>
                        <input type="text" class="form-control" name="LNAME" placeholder="" value=""/>
                        </div>
                            
                            <div class="clearfix"><!-- --></div>
                            <div class="actions">
                                <span class="send_btn">
                            <button type="submit" class="btn btn-primary btn-submit submit_btn">Subscribe</button>
                        </span>
                            </div>
                            <div class="clearfix"><!-- --></div>
                                    
                        </form>
                    
                    <div class="clearfix"></div>
                </div>
                </div>
    		</div>
    	</div><!-- container -->
    </div>
    </div>
    <div id="hidden_pix_6" class="confirm_page confirm_page_6 pix_builder_bg">
      <div class="pixfort_university_6">
      
      <div class="confirm_header"><span class="editContent"><span class="pix_text">Thank You Very Much!</span></span></div>
      <div class="confirm_text">
        <span class="editContent"><span class="pix_text">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</span></span>
      </div>
      <div class="confirm_social">
        <div class="confirm_social_box pix_builder_bg">
              <a href="https://twitter.com/share" class="twitter-share-button" data-via="pixfort" data-count="none">Tweet</a>
                <span class="confirm_gp">
                  <span class="g-plusone " data-size="medium" data-annotation="none"></span>
                </span>
                <div class="fb-like" data-href="http://themeforest.net/item/flatpack-landing-pages-pack-with-page-builder/10591107" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
            </div>
        </div>
    </div>
    </div>
    <div class="section_pointer" pix-name="6_university"></div>
<?include 'layout/footer.php';?>
