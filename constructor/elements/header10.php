<?include 'layout/header.php';?>

<!-- Part 1: #Header -->
<div class="pixfort_normal_1" id="section_header_10">
  <div class="header_style pix_builder_bg">
    <div class="container">

        <div class="sixteen columns">
            <img src="/constructor/elements/images/1_normal/logo.png" alt="">

                    <nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg navbar-fixed-top">
                        
                    <div class="container">
                    
                        <div class="navbar-header">
                            <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                                <span class="sr-only">Toggle navigation</span>
                            </button>
                            
                        </div>
                                    
                        <div id="navbar-collapse-02" class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active propClone"><a href="#">Home</a></li>
                                <li class="propClone"><a href="#">Work</a></li>
                                <li class="propClone"><a href="#">Blog</a></li>
                                <li class="propClone"><a href="#">Contact</a></li>
                            </ul>
                        </div><!-- /.navbar-collapse -->
                        
                    </div><!-- /.container -->
                        
                </nav>
        </div>
    </div><!-- container -->
  </div>
</div>
<?include 'layout/footer.php';?>
