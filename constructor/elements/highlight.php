<?include 'layout/header.php';?>
        <div class="big_padding pix_builder_bg highlight-section test9" id="highlight">
            <div class="highlight-left pix_builder_bg"></div>
            <div class="container ">
                <div class="sixteen columns ">
                    <div class="eight columns alpha">
                        <div class="highlight_inner">
                            
                            <p class="big_title editContent">Get The Most Amazing Builder!</p>
                            <span class="bold_text normal_text blue_text editContent">GET FREE SUPPORT</span>
                            <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            <a href="#" class="pix_button pix_button_line blue_border_button bold_text"><i class="pi pixicon-check"></i> Line Button</a>
                            </div>
                    </div>                 
                    <div class="eight columns omega ">
                        
                    </div>
                </div>
           </div>
        </div>


        <div class="big_padding pix_builder_bg  highlight-section test9">
            <div class="highlight-right pix_builder_bg"></div>
            <div class="container">
                <div class="sixteen columns">
                    <div class="eight columns alpha">
                        <br>
                    </div>                 
                    <div class="eight columns omega ">
                         <div class="highlight_inner">
                        <span class="bold_text big_text blue_text editContent">GET FREE SUPPORT</span>
                            <p class="big_title bold_text editContent">Get The Most Amazing Builder!</p>
                            <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            <a href="#" class="pix_button btn_normal pix_button_flat blue_bg bold_text"><i class="pi pixicon-paperplane"></i> Subscribe Now!</a> 
                            <a href="#" class="pix_button pix_button_line blue_border_button bold_text"><i class="pi pixicon-check"></i> Line Button</a>
                            </div>
                    </div>
                </div>
           </div>
        </div>

    <div class="section_pointer" pix-name="main"></div>
<?include 'layout/footer.php';?>
