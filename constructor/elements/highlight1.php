<?include 'layout/header.php';?>
    <div class="pix_builder_bg hl1" id="section_highlight_1">
        <div class="big_padding highlight-section">
            <div class="highlight-left pix_builder_bg"></div>
            <div class="container ">
                <div class="sixteen columns ">
                    <div class="eight columns alpha">
                        <div class="highlight_inner">
                            <p class="big_title editContent">Restaurant High Quality Services</p>
                            <p class="bold_text margin_bottom normal_text orange editContent">BOOK YOUR SEAT ONLINE</p>
                            <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat.</p>
                            <a href="#" class="pix_button pix_button_line brown bold_text editContent"><i class="pi pixicon-ribbon"></i> Bookmark Recipes</a>
                            </div>
                    </div>                 
                    <div class="eight columns omega ">
                        
                    </div>
                </div>
           </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
