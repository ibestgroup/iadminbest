<?include 'layout/header.php';?>
    <div class="hl3 pix_builder_bg" id="section_highlight_3">
        <div class="big_padding highlight-section">
            <div class="highlight-right pix_builder_bg"></div>
            <div class="container">
                <div class="sixteen columns">
                    <div class="eight columns alpha">
                        <br>
                    </div>                 
                    <div class="eight columns omega ">
                        <div class="highlight_inner">
                            <div class="left_text">
                                <div class="big_icon left_text green_1">
                                    <span class="pi pixicon-globe2"></span>
                                </div>
                                <h4 class="big_title margin_bottom"><span class="editContent"><strong>Get The Best   Ticket Today!</strong></span></h4>
                                <p class="normal_text editContent">You can add unlimited fields directly from HTML</p>
                                <form id="contact_form" class="pix_form" pix-confirm="hidden_pix_4">
                                    <div id="result"></div>
                                    <input type="text" name="name" id="name" placeholder="Your Full Name" class="pix_text"> 
                                    <input type="email" name="email" id="email" placeholder="Your Email" class="pix_text">
                                    <input type="text" name="number" id="number" placeholder="Your Phone Number" class="pix_text">
                                    <span class="send_btn">
                                        <button type="submit" class="submit_btn green_1_bg pix_text" id="submit_btn_6">
                                            <span class="editContent">Send Information</span>
                                        </button>
                                    </span>
                                    <span class="pix_text"><span class="pix_note editContent">*Some awesome dummy text goes here.</span></span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        </div>
    </div>
    <div id="hidden_pix_4" class="confirm_page confirm_page_4 green_1_bg pix_builder_bg ">
        <div class="pixfort_real_estate_4">
            <div class="confirm_header"><span class="editContent"><span class="pix_text">Thank You Very Much!</span></span></div>
            <div class="confirm_text">
                <span class="editContent"><span class="pix_text">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</span></span>
            </div>
            <div class="center_text padding_bottom_60">
                <ul class="bottom-icons confirm_icons center_text big_title pix_inline_block">
                    <li><a class="pi pixicon-facebook6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-twitter6 white" href="#fakelink"></a></li>
                    <li><a class="pi pixicon-googleplus7 white" href="#fakelink"></a></li>
                </ul>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
