
// jQuery Initialization
jQuery(document).ready(function($){
"use strict"; 

      /* Testimonials ticker function call */ 
         $('.ticker_fade').list_ticker({
          speed:4000,
          effect:'fade'
        });

    /* Apply full screen section
 ********************************************/

    $(function() {
        if ($('.js-countdown-1').length) {
            var Countdown = function(options) {
                $.extend(this, {
                    endDate: new Date(2019, 4, 26, 15, 2, 20, 0)
                }, options);
                console.log(options);
                this.cache();
                this.bind();

                return this;
            };
            $.extend(Countdown.prototype, {
                cache: function() {
                    this.date = new Date();
                    this.secCounter = parseInt((this.endDate - this.date) / 1000);
                },
                bind: function() {
                    this.timer();
                },
                timer: function() {
                    var timeInSec = this.secCounter,
                        $secondsCircle = $('.seconds').closest('.time-circle').find('.progress'),
                        $minutesCircle = $('.minutes').closest('.time-circle').find('.progress'),
                        $hoursCircle = $('.hours').closest('.time-circle').find('.progress'),
                        $daysCircle = $('.days').closest('.time-circle').find('.progress'),
                        $seconds = $('.seconds'),
                        $minutes = $('.minutes'),
                        $days = $('.days'),
                        $hours = $('.hours');

                    function pad(number) {
                        return (number < 10 ? '0' : '') + number
                    }

                    function setScale(type, degree) {
                        type.css({
                            '-webkit-transform': 'rotate(' + -degree * 6 + 'deg)',
                            '-moz-transform': 'rotate(' + -degree * 6 + 'deg)',
                            '-ms-transform': 'rotate(' + -degree * 6 + 'deg)',
                            '-o-transform': 'rotate(' + -degree * 6 + 'deg)',
                            'transform': 'rotate(' + -degree * 6 + 'deg)'
                        });
                    };

                    var setInterv = setInterval(function() {

                        (timeInSec > 0 ? timeInSec-- : timeInSec = 0);

                        var getSeconds = timeInSec % 60,
                            getMinutes = Math.floor(timeInSec / 60 % 60),
                            getHours = Math.floor(timeInSec / 3600 % 24),
                            getDays = Math.floor(timeInSec / 86400 % 7),
                            getWeeks = Math.floor(timeInSec / 604800);

                        $seconds.text(pad(getSeconds));
                        $minutes.text(pad(getMinutes));
                        $hours.text(pad(getHours));
                        $days.text(pad(getDays));

                        setScale($secondsCircle, getSeconds);
                        setScale($minutesCircle, getMinutes);
                        setScale($hoursCircle, getHours);
                        setScale($daysCircle, getDays);

                        if (timeInSec <= 0) {
                            clearInterval(setInterv);
                            console.log('End of counting');
                        }

                    }, 1000);
                }
            });
            window.Countdown = Countdown;

            var app = new Countdown({
                //properties
                endDate: new Date($('.js-countdown-1').data('start'))
            });
        }
    });

    $('.js-form').submit(function (e) {
       e.preventDefault();

       var form = $(this),
           formData = new FormData(form.get(0)),
           type = form.data('type');

       formData.append("ajax", "1");
       formData.append("type", form.data('type'));

       let url = '/login';
       if (type == 'register') {
           url = '/register';
       }

       $.ajax({
           url: url,
           type: 'post',
           dataType: 'json',
           contentType: false,
           processData: false,
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
           },
           data: formData,
           success: function (data) {
               if (data.type == 'success') {
                   if (form.data('type') == 'auth')
                       window.location.href = data.url;
                       // window.location.href = '/profile';
                   if (form.data('type') == 'register')
                       window.location.href = data.url;
                       // window.location.href = '/';
               }

               for (var i in data.error) {
                   form.find('#result').append('<p class="text-danger">'+data.error[i]+'</p>');
               }
               form.find('#result').show();
           }
       });
    });
});