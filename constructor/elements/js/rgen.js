/*
++++++++++++++++++++++++++++++++++++++++++++++++++++++
AUTHOR : R.Genesis.Art
PROJECT : R.Gen Landing Page (v0.17)
++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/
var rgen = {};
rgen.elcheck = function(el) {
    'use strict';
    if ($(el).length > 0) {
        return true;
    } else {
        return false;
    };
};
(function(){
    'use strict';

    jQuery(document).ready(function($) {

        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });

        /* NAVIGATION
        ********************************************/


        /* All navigation utilities
        ********************************************/
        if (rgen.elcheck(".nav-wrp")) {

            var $nav = $(".nav-wrp");

            $nav.attr('data-glass') === 'y' ? $nav.addClass('bg-glass') : null;
            $nav.attr('data-above') === 'y' ? $nav.addClass('show-above') : null;

            if ($nav.attr('data-sticky') == 'y') {
                $nav.addClass('navbar-fixed-top').removeClass('show-above');
                $(window).scroll(function () {
                    if ($(window).scrollTop() > $("nav").height()) {
                        $nav.addClass("nav-sticky");
                        $nav.attr('data-glass') === 'y' ? $nav.removeClass('bg-glass') : null;

                    } else {
                        $nav.removeClass("nav-sticky");
                        $nav.attr('data-glass') === 'y' ? $nav.addClass('bg-glass') : null;
                    }
                });
            };

            if ($nav.attr('data-hide') == 'y') {
                $nav.addClass('nav-hide');
                $(window).scroll(function () {
                    if ($(window).scrollTop() > $("nav").height()) {
                        $nav.addClass("nav-show");
                    } else {
                        $nav.removeClass("nav-show");
                    }
                });
            };
        }

        /* Apply ID on each sections
        ********************************************/
        if (rgen.elcheck(".main-container section")) {
            $(".main-container section").each(function(index, el) {
                $(this).attr('id', rgen.uid());
            });
        }

        /* Apply full screen section
        ********************************************/
        if (rgen.elcheck("[data-fullwh='y']")) {
            $("[data-fullwh='y']").each(function(index, el) {
                rgen.fullwh(this);
                var fullwhSection = this;
                $(window).resize(function(){
                    rgen.fullwh(fullwhSection);
                });
            });
        }
        if (rgen.elcheck("[data-fullh='y']")) {
            $("[data-fullh='y']").each(function(index, el) {
                rgen.fullh(this);
                var fullhSection = this;
                $(window).resize(function(){
                    rgen.fullh(fullhSection);
                });
            });
        }

        /* Apply background image
        ********************************************/
        if (rgen.elcheck("[data-bg]")) {
            $("[data-bg]").each(function(index, el) {
                $(this).css({backgroundImage: "url("+$(this).attr("data-bg")+")"});
            });
        }


        /* Tab widgets
        ********************************************/
        if (rgen.elcheck(".tab-widgets")) {
            $(".tab-widgets").each(function(index, el) {
                var obj = $(this).find('[data-tb]');
                rgen.tabs(obj);
            });
        }

        if (rgen.elcheck(".tabs-auto")) {
            $(".tabs-auto").each(function(index, el) {
                var tabObj = {
                    count: index,
                    tb: this
                }
                rgen.tabs(tabObj);
            });
        }


        /* Carousel widgets
        ********************************************/
        if (rgen.elcheck(".carousel-widgets")) {
            var carousel = 0;
            $('.carousel-widgets').each(function(){

                // SET ID ON ALL OBJECTS
                carousel++;
                var owlObj = 'owl'+carousel;
                $(this).css({opacity:0});
                $(this).attr("id", owlObj);
                $(this).addClass(owlObj);
                rgen.slider("#"+owlObj);
            });
        }

        /* Set blur background image
        ********************************************/
        if (rgen.elcheck("[data-blurimg]")) {
            $("[data-blurimg]").each(function(index, el) {
                var blurObj = {
                    container: $(this),
                    img: $(this).attr('data-blurimg')
                }
                rgen.blur(blurObj);
            });
        }

        /* Other section 1 script
        ********************************************/
        if (rgen.elcheck('.other-section-1')) {
            $('.other-section-1').each(function(){
                var $el = $(this);
                var img = $el.find('.r img');
                $el.find('ol > li').click(function(e){

                    e.preventDefault();

                    var src = $(this).attr('data-img');
                    $(this).parent().find('.active').removeClass('active');
                    $(this).addClass('active');

                    img.css({opacity: 0, marginTop: -20});
                    img.attr('src', src);
                    img.stop().animate({
                            opacity: 1,
                            marginTop: 0},
                        500, function() {
                        });
                }).eq(0).click();
            });
        }

        /* Simple pop up gallery
        ********************************************/
        if (rgen.elcheck(".popgallery-widgets")) {
            var magnific = 0;
            $('.popgallery-widgets').each(function(){

                magnific++;
                var obj = 'popgallery'+magnific;
                $(this).attr("id", obj);
                $(this).addClass(obj);

                $('#'+obj).magnificPopup({
                    delegate: '.pop-img',
                    type: 'image',
                    tLoading: 'Loading image #%curr%...',
                    mainClass: 'mfp-img-mobile',
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                    },
                    image: {
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                        titleSrc: function(item) {
                            return item.el.attr('title');
                        }
                    }
                });
            });
        }

        /* Background slider
        ********************************************/
        if (rgen.elcheck("[data-bgslider]")) {
            $("[data-bgslider]").each(function(index, el) {
                var s1 = $(this).attr('data-bgslider');
                var s2 = s1.split('|');
                var bgslides = [];
                $.each(s2, function(index, val) {
                    bgslides.push({ src: val });
                });
                var bgslideSetting = {
                    obj: this,
                    delay: 6000,
                    slides: bgslides,
                    animation: 'kenburns'
                }
                rgen.bgSlider(bgslideSetting);
            });
        };

        /* Countdown
        ********************************************/
        if (rgen.elcheck(".countdown-widgets")) {
            var countdown = 0;
            $(".countdown-widgets").each(function(index, el) {
                var obj = 'countdown'+countdown;
                $(this).children('div').attr("id", obj);
                rgen.countdown("#"+obj);
                countdown++;
            });
        }


        /* Notify form
        ********************************************/
        if (rgen.elcheck("#subscribe")) {
            var $subscribeForm = $('#subscribe');
            var subscribe_validate_data = {
                form: "#subscribe",
                rules: { email: { required: true, email: true } },
                msg: {
                    email: {
                        required: "Please enter email before submit.",
                        email: "Please, enter a valid email"
                    }
                },
                msgpos: 'append',
                successMsg: "<div class='msg-success'>Congrats! You are in list. We will inform you as soon as we finish.</div>",
                errorMsg: "<div class='msg-error>Oops! Looks like something went wrong. Please try again later.</div>"
            }
            rgen.formVaidate(subscribe_validate_data);
            $('#subscribe').off('click').on('click', '#submit', function(e) {
                e.preventDefault();
                var formData = {
                    email: $subscribeForm.find('input').val()
                }
                rgen.contactForm($subscribeForm, formData, subscribe_validate_data);
                return false;
            });
        }

        /* Form widgets
        ********************************************/
        if (rgen.elcheck(".form-widgets")) {
            $(".form-widgets").each(function(index, el) {
                rgen.formWidget(this);
            });
        };


        /* RESPONSIVE
        ********************************************/
        $.mediaquery("bind", "mq-key", "(min-width: 992px)", {
            enter: function() {
                rgen.eqh(".eqh", ".eqh > div", "");
                rgen.eqh(".feature-section-3", ".info", "");
                rgen.eqh(".feature-section-5 .eqh", ".eqh > div", "destroy");
                if (!rgen.elcheck(".testimonial-section .carousel-widgets")) {
                    rgen.eqh(".testimonial-section", ".feedback-box1", "");
                }
            },
            leave: function() {
                rgen.eqh(".eqh", ".eqh > div", "destroy");
                rgen.eqh(".feature-section-3", ".info", "destroy");
            }
        });

        $.mediaquery("bind", "mq-key", "(min-width: 200px) and (max-width: 991px)", {
            enter: function() {
                $('.nav-transparent').removeClass('nav-transparent');

                if (rgen.elcheck(".content-section-8")) {
                    $('.content-section-8 .bg-section').appendTo('.content-section-8 .l');
                }
                $(".nav-wrp").removeClass('show-above').removeClass('bg-glass');
            },
            leave: function() {
                if (rgen.elcheck(".content-section-8")) {
                    $('.content-section-8 .bg-section').appendTo('.content-section-8');
                }

                $('.nav-wrp').attr('data-glass') === 'y' ? $('.nav-wrp').addClass('bg-glass') : null;
                $('.nav-wrp').attr('data-above') === 'y' ? $('.nav-wrp').addClass('show-above') : null;
            }
        });

        /* end ======================*/
    });
})();