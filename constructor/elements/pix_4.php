<?include 'layout/header.php';?>
    <div class="pixfort_normal_1 creative_video_header" id="section_pix_4">
        <div class="has-video creative_header">
            <div class="mask"></div>
                <div class="pix_video_div section_video">
                    <video autoplay="" poster="/constructor/elements/images/video/video_bg.jpg" class="bgvid" loop="">
                        <source src="/constructor/elements/images/video/video_bg.webm" type="video/webm">
                        <source src="/constructor/elements/images/video/video_bg.mp4" type="video/mp4">
                        <source src="/constructor/elements/images/video/video_bg.ogv" type="video/ogv">
                    </video>
                </div>
                <div class="section_wrapper clearfix">
                    <div class="container">
                        <div class="sixteen columns context_style">
                            <div class="title_style">
                                <span class="editContent">
                                    <span class="pix_text">NEW LANDING FOR CREATIVES</span>
                                </span>
                            </div>
                            <div class="subtitle_style">
                                <span class="creative_h_text">
                                    <span class="editContent"><span class="pix_text">More than 18 unique HTML templates in one bundle isn't that awesome with a lot of features and great design brought to you by PixFort.</span></span>
                                </span>
                            </div>
                        <div class="email_subscribe pix_builder_bg">
                            <form id="contact_form" pix-confirm="hidden_pix_1">
                                <input type="email" name="email" id="email" placeholder="Enter Your Email" class="pix_text">
                                <button class="subscribe_btn pix_text" id="subscribe_btn_1">SUBSCRIBE NOW</button>
                                <div id="result"></div>
                            </form>
                        </div>
                        <div class="note_st">
                            <span class="creative_note"><span class="editContent"><span class="pix_text">*Note: Bundle ends in two weeks from now.</span></span></span>
                        </div>
                    </div>
                </div><!-- container -->
            </div>
        </div>
    </div>
    <div id="hidden_pix_1" class="confirm_page confirm_page_1 pix_builder_bg">
        <div class="pixfort_normal_1">
            <div class="confirm_logo">
                <span class="conf_img">
                    <img src="/constructor/elements/images/video/confirm1.png" alt="">
                </span>
            </div>
            <div class="confirm_header">
                <span class="editContent">
                    <span class="pix_text">THANK YOU FOR SUBSCRIPTION</span>
                </span>
            </div>
            <div class="confirm_text">
                <span class="editContent">
                    <span class="pix_text">Our service is astonishingly thin and light.</span>
                </span>
            </div>
            <div class="confirm_social">
                <div class="confirm_social_box pix_builder_bg">
                    <a href="https://twitter.com/share" class="twitter-share-button" data-via="pixfort" data-count="none">Tweet</a>
                        <span class="confirm_gp">
                            <span class="g-plusone " data-size="medium" data-annotation="none"></span>
                        </span>
                    <div class="fb-like" data-href="http://themeforest.net/item/flatpack-landing-pages-pack-with-page-builder/10591107" data-layout="button" data-action="like" data-show-faces="false" data-share="false"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="section_pointer" pix-name="video"></div>
<?include 'layout/footer.php';?>
