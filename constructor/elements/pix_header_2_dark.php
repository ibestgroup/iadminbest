<?include 'layout/header.php';?>
    <div class="pixfort_normal_1" id="section_header_2_dark">
        <div class=" header_nav_1 dark pix_builder_bg">
            <div class="container">
                <div class="sixteen columns firas2">
                    <nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
                        <div class="containerss">
                            <div class="navbar-header">
                                <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle pix_text" type="button">
                                    <span class="sr-only ">Toggle navigation</span>
                                </button>
                                <img src="/constructor/elements/images/main/logo-dark.png" class="pix_nav_logo" alt="">
                            </div>
                            <div id="navbar-collapse-02" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="active propClone"><a href="#">Home</a></li>
                                    <li class="propClone"><a href="#">Work</a></li>
                                    <li class="propClone"><a href="#">Blog</a></li>
                                    <li class="propClone"><span class="text_span editContent">(+103) 55-323-34</span></li>
                                    <li class="propClone icon-item"><a class="pi pixicon-facebook2" href="#fakelink"></a></li>
                                    <li class="propClone icon-item"><a class="pi pixicon-twitter2" href="#fakelink"></a></li>
                                    <li class="propClone icon-item"><a class="pi pixicon-instagram" href="#fakelink"></a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container -->
                    </nav>
                </div>
            </div><!-- container -->
        </div>
    </div>
<?include 'layout/footer.php';?>
