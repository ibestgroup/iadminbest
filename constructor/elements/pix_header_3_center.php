<?include 'layout/header.php';?>
    <div class="pixfort_normal_1 pix_builder_bg" id="section_header_3_center">
        <div class="header_style header_nav_1">
            <div class="container">
                <div class="sixteen columns firas2">
                    <nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
                        <div class="containerss">
                            <div class="navbar-header">
                                <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                </button>
                            </div>
                            <div id="navbar-collapse-02" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-center">
                                    <li class="active propClone"><a href="#">Home</a></li>
                                    <li class="propClone"><a href="#">Work</a></li>
                                    <li class="propClone"><a href="#">Contact</a></li>
                                    <li class="propClone"><a class="" href="#"><span class="pix_header_button pix_builder_bg">Buy Now</span></a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container -->
                    </nav>
                </div>
            </div><!-- container -->
        </div>
    </div>
<?include 'layout/footer.php';?>
