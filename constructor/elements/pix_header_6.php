<?include 'layout/header.php';?>

    <div class="" id="section_header_6">
        <div class="header_style header_nav_1 pix_builder_bg">
            <div class="container">
                <div class="one-third column">
                    <nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
                        <div class="containerss">                            
                            <div class="navbar-header">
                                <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                </button>
                            </div>
                            
                            
                            <div id="navbar-collapse-02" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-left">
                                    <li class="active propClone"><a href="#">Home</a></li>
                                    <li class="propClone"><a href="#">Work</a></li>
                                    <li class="propClone"><a href="#">Blog</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                            
                        </div><!-- /.container -->
                    </nav>
                </div>
                <div class="one-third column center_text">
                    <div class="small_padding">
                        <img src="/constructor/elements/images/main/logo.png" alt="">
                    </div>
                </div>
                <div class="one-third column">
                    <div class="padding_25 right_socials">
                        <ul class="bottom-icons">
                            <li><a class="pi pixicon-facebook2 light_gray" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-twitter2 light_gray" href="#fakelink"></a></li>
                            <li><a class="pi pixicon-instagram light_gray" href="#fakelink"></a></li>
                        </ul>
                    </div>
                </div>

            </div><!-- container -->
        </div>
    </div>
<?include 'layout/footer.php';?>
