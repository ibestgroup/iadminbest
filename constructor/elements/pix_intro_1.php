<?include 'layout/header.php';?>
    <div class="header_nav_1 dark blue_moving_bg pix_builder_bg" id="section_intro_1">
        <div class="header_style   ">
            <div class="container">
                <div class="sixteen columns firas2">
                    <nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
                        <div class="containerss">
                            <div class="navbar-header">
                                <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                </button>
                                <img src="/constructor/elements/images/main/logo-dark.png" class="pix_nav_logo" alt="">
                            </div>
                            <div id="navbar-collapse-02" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="active propClone"><a href="#">Home</a></li>
                                    <li class="propClone"><a href="#">Work</a></li>
                                    <li class="propClone"><a href="#">Contact</a></li>
                                    <li class="propClone icon-item"><a class="pi pixicon-facebook2" href="#fakelink"></a></li>
                                    <li class="propClone icon-item"><a class="pi pixicon-twitter2" href="#fakelink"></a></li>
                                    <li class="propClone icon-item"><a class="pi pixicon-instagram" href="#fakelink"></a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container -->
                    </nav>
                </div>
            </div><!-- container -->
        </div>
        <div class="container">
            <div class="sixteen columns">
                <div class="ten offset-by-three columns">
                    <div class="center_text big_padding">
                        <p class="big_title bold_text editContent">Flexible Software System For Developers </p>
                        <p class="big_text normal_gray editContent">
                            From logo design to website development, hand-picked designers and developers are ready to complete.
                        </p>
                        <a href="#" class="pix_button btn_normal pix_button_flat blue_bg bold_text btn_big margin_right_10">
                            <i class="pi pixicon-box"></i> 
                            <span>VIEW LIVE DEMO</span>
                        </a> 
                        <a href="#" class="pix_button pix_button_line white_border_button bold_text btn_big">
                            <i class="pi pixicon-bag"></i> 
                            <span>EXPLORE FEATURES</span>
                            </a>
                    </div>
                </div>
                <img src="/constructor/elements/images/main/intro-image.png" class="big_image" alt="">
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
