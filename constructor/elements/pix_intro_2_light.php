<?include 'layout/header.php';?>
    <div class="pix_builder_bg" id="section_intro_2_light">
        <div class="" id="section_normal_1">
            <div class="header_nav_1">
                <div class="container">
                    <div class="sixteen columns firas2">
                        <nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
                            <div class="containerss">
                                <div class="navbar-header">
                                    <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                                        <span class="sr-only">Toggle navigation</span>
                                    </button>
                                    <img src="/iadminbest//constructor/elements/images/main/logo.png" class="pix_nav_logo" alt="">
                                </div>
                                <div id="navbar-collapse-02" class="collapse navbar-collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active propClone"><a href="#">Home</a></li>
                                        <li class="propClone"><a href="#">Work</a></li>
                                        <li class="propClone"><a href="#">Contact</a></li>
                                        <li class="propClone"><span class="text_span">(+103) 55-323-34</span></li>
                                        <li class="propClone"><a class="" href="#"><span class="pix_header_button pix_builder_bg">View Deals</span></a></li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container -->
                        </nav>
                    </div>
                </div><!-- container -->
            </div>
        </div>
        <div class="big_padding">
            <div class="container">
                <div class="sixteen columns">
                    <div class="eight columns alpha">
                        <img src="/iadminbest//constructor/elements/images/main/intro-car.jpg" class="img_style feature_image padding_top_60" alt="">
                    </div>
                    <div class="eight columns omega margin_bottom_30">
                            <p class="big_title editContent">Order Gorgeous New Car Today Via Themeforest!</p>
                            <p class="bold_text big_text dark_yellow margin_bottom editContent">STARTING FROM $339/M</p>
                            <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sedoton eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud.</p>
                            <a href="#" class="pix_button btn_normal pix_button_flat dark_yellow_bg bold_text">
                                <i class="pi pixicon-grid"></i> 
                                <span>VIEW HOT DEALS</span>
                            </a> 

                        </div>
                </div>
	       </div>
        </div>
    <div class="section_pointer" pix-name="main"></div>
    </div>
<?include 'layout/footer.php';?>
