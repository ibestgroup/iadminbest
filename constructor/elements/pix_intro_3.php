<?include 'layout/header.php';?>
    <div class="header_nav_1 dark inter_3_bg pix_builder_bg" id="section_intro_3">
        <div class="header_style">
            <div class="container">
                <div class="sixteen columns firas2">
                    <nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
                        <div class="containerss">
                            <div class="navbar-header">
                                <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                                    <span class="sr-only">Toggle navigation</span>
                                </button>
                                <img src="/constructor/elements/images/main/logo-dark.png" class="pix_nav_logo" alt="">
                            </div>
                            <div id="navbar-collapse-02" class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="active propClone"><a href="#">Home</a></li>
                                    <li class="propClone"><a href="#">Work</a></li>
                                    <li class="propClone"><a href="#">Contact</a></li>
                                    <li class="propClone icon-item"><a class="pi pixicon-facebook2" href="#fakelink"></a></li>
                                    <li class="propClone icon-item"><a class="pi pixicon-twitter2" href="#fakelink"></a></li>
                                    <li class="propClone icon-item"><a class="pi pixicon-instagram" href="#fakelink"></a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container -->
                    </nav>
                </div>
            </div><!-- container -->
        </div>
        <div class="container">
            <div class="sixteen columns margin_bottom_50 padding_top_60">
                <div class="twelve offset-by-two columns">
                    <div class="center_text big_padding">
                        <p class="big_title bold_text editContent">Restaurant Landing Page</p>
                        <p class="big_text normal_gray editContent">
                            From logo design to website development, hand-picked designers and developers are ready to complete.
                        </p>
                        <a href="#" class="pix_button pix_button_line white_border_button bold_text big_text btn_big">
                            <i class="pi pixicon-paper"></i> 
                            <span>Make a Reservation</span>
                            </a>
                    </div>
                </div>
            </div>
            <div class="center_text">
                <a href="#" class="intro_arrow">
                    <i class="pi pixicon-arrow-down"></i> 
                </a>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
