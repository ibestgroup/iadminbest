<?include 'layout/header.php';?>
        <div class="big_padding pix_builder_bg dark cta_bg" id="section_normal_4_1">
            <div class="container">
                <div class="sixteen columns">
                    <div class="twelve columns alpha margin_bottom_30">
                        <div class="title_70 light_blue margin_bottom">
                            <span class="pi pixicon-download2"></span>
                        </div>
                            <p class="big_title editContent"><strong>Download Software From Themeforest Today!</strong></p>
                            <p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                            <a href="#popup_1" class="active_bg_open pix_button btn_normal btn_big pix_button_flat bold_text blue_bg " name="fff">
                                <i class="pi pixicon-stack-2"></i> 
                                <span>DOWNLOAD FLATPACK</span>
                            </a> 

                        </div>
                </div>
	       </div>
        </div>
    <div class="section_pointer" pix-name="main"></div>

     <div id="popup_1" class="well pix_popup pop_style_1 pix_builder_bg pop_hidden max_400" pix-overlay="rgba(0,0,0,0.5)">
        <div class="center_text">
            <div class="big_icon light_blue">
                <span class="pi pixicon-paperplane"></span>
            </div>
            <span class="editContent"><h4 class="margin_bottom pix_text"><strong>Subscription Form</strong></h4></span>
            <p class="editContent">You can add unlimited fields directly from HTML</p>
            <form id="contact_form" class="pix_form">
                <div id="result"></div>
                <input type="text" name="name" id="name" placeholder="Your Full Name" class="pix_text"> 
                <input type="email" name="email" id="email" placeholder="Your Email" class="pix_text">
                <input type="text" name="number" id="number" placeholder="Your Phone Number" class="pix_text">
                <span class="send_btn">
                    <button type="submit" class="submit_btn pix_text" id="submit_btn_6">
                        <span class="editContent">Send Information</span>
                    </button>
                </span>
                <div class="pix_text pix_note"><span class="editContent">*Some dummy text goes here.</span></div>
            </form>
        </div>
    </div>
<?include 'layout/footer.php';?>
