<?include 'layout/header.php';?>


        <div class="light_gray_bg pix_builder_bg" id="section_call_2">
            <div class="footer3">
                <div class="container ">
                    <div class="ten columns alpha offset-by-three">
                        <div class="content_div center_text">
                            <div class="margin_bottom_30 green_1">
                                <i class="pi pixicon-envelope title_56"></i> 
                            </div>
                            <div class="margin_bottom_30">
                                <p class="big_title bold_text editContent">Subscription Form</p>
                                <p class="normal_text light_gray center_text editContent">This is an example section for popups layouts, you can use it as it is, or you can check our documentation to learn how to add popups to any button or link in your landing pages, it's easy as cup cakes!</p>
                                <p class="normal_text light_gray center_text editContent">
                                    You can also open this popup from any link in the page by adding the popup's id <strong>#popup_10</strong> in the link field.
                                </p>
                            </div>
                            <a href="#popup_10" class="pix_button pix_button_flat bold_text blue_bg white btn_big">
                                <span>POPUP BUTTON</span>
                            </a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="popup_10" class="well pop_hidden pix_builder_bg" pix-class="subscribe_popup">
            <div class="h_padding margin_vertical">
                <div class="center_text margin_bottom_30 margin_vertical">
                    <span class="editContent"><h4 class="margin_bottom pix_text"><strong>Subscription Form</strong></h4></span>
                    <p class="light_gray editContent">Seamlessly empower fully researched growth strategies and interoperable internal.</p>
                </div>
                <form id="contact_form" class="pix_form ">
                    <div class="sixteen columns center_text">
                        <div id="result"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="sixteen columns center_text">
                        <div class="one-third column alpha">
                            <input type="text" name="name" id="name" placeholder="Your Full Name" class="pix_text"> 
                        </div>
                        <div class="one-third column">
                            <input type="email" name="email" id="email" placeholder="Your Email" class="pix_text">
                        </div>
                        <div class="one-third column omega">
                            <span class="send_btn">
                                <button type="submit" class="submit_btn green_1_bg pix_text" id="submit_btn_6">
                                    <span class="editContent">Send Information</span>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
<?include 'layout/footer.php';?>
