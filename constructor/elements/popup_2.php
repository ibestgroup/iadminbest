<?include 'layout/header.php';?>


        <div class="light_gray_bg pix_builder_bg" id="section_call_2">
            <div class="footer3">
                <div class="container ">
                    <div class="ten columns alpha offset-by-three">
                        <div class="content_div center_text">
                            <div class="margin_bottom_30 dark_red">
                                <i class="pi pixicon-clipboard title_56"></i> 
                            </div>
                            <div class="margin_bottom_30">
                                <p class="big_title bold_text editContent">Reservation Popup</p>
                                <p class="normal_text light_gray center_text editContent">This is an example section for popups layouts, you can use it as it is, or you can check our documentation to learn how to add popups to any button or link in your landing pages, it's easy as cup cakes!</p>
                                <p class="normal_text light_gray center_text editContent">
                                    You can also open this popup from any link in the page by adding the popup's id <strong>#popup_2</strong> in the link field.
                                </p>
                            </div>
                            <a href="#popup_2" class="pix_button pix_button_line bold_text dark_red btn_big">
                                <span>POPUP BUTTON</span>
                            </a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="popup_2" class="well pix_popup pop_style_1 pop_hidden light_gray_bg pix_builder_bg">
            <div class="center_text ">
                <div class="big_icon dark_red">
                    <span class="pi pixicon-clipboard2"></span>
                </div>
                <span class="editContent"><h4 class="margin_bottom pix_text"><strong>Reservation Form</strong></h4></span>
                <p class="editContent">You can add unlimited fields directly from HTML</p>
                <form id="contact_form" class="pix_form">
                    <div id="result"></div>
                    <input type="text" name="name" id="name" placeholder="Your Full Name" class="pix_text"> 
                    <input type="email" name="email" id="email" placeholder="Your Email" class="pix_text">
                    <input type="text" name="number" id="number" placeholder="Your Phone Number" class="pix_text">
                    <select name="numberp" class="pix_text">
                        <option value="1">One Person</option>
                        <option value="2">Two Person</option>
                        <option value="3">Three Person</option>
                        <option value="+4">More</option>
                    </select>
                    
                    <span class="send_btn">
                        <button type="submit" class="orange_bg submit_btn pix_text" id="submit_btn_6">
                            <span class="editContent">Send Information</span>
                        </button>
                    </span>
                    <div class="pix_text pix_note"><span class="editContent">*Some dummy text goes here.</span></div>
                </form>
            </div>
        </div>

<?include 'layout/footer.php';?>
