<?include 'layout/header.php';?>


        <div class="light_gray_bg pix_builder_bg" id="section_call_2">
            <div class="footer3">
                <div class="container ">
                    <div class="ten columns alpha offset-by-three">
                        <div class="content_div center_text">
                            <div class="margin_bottom_30 blue_text">
                                <i class="pi pixicon-envelope title_56"></i> 
                            </div>
                            <div class="margin_bottom_30">
                                <p class="big_title bold_text editContent">Subscription Popup</p>
                                <p class="normal_text light_gray center_text editContent">This is an example section for popups layouts, you can use it as it is, or you can check our documentation to learn how to add popups to any button or link in your landing pages, it's easy as cup cakes!</p>
                                <p class="normal_text light_gray center_text editContent">
                                    You can also open this popup from any link in the page by adding the popup's id <strong>#popup_4</strong> in the link field.
                                </p>
                            </div>
                            <a href="#popup_4" class="pix_button pix_button_line bold_text blue_text btn_big">
                                <span>POPUP BUTTON</span>
                            </a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="popup_4" class="well pix_popup pop_hidden pix_builder_bg" pix-class="subscribe_popup">
            <div class="center_text">
                <span class="editContent"><h4 class="margin_bottom pix_text"><strong>Subscription Form</strong></h4></span>
                <p class="editContent">You can add unlimited fields directly from HTML</p>
                <form id="contact_form" class="pix_form">
                    <div id="result"></div>
                    <input type="email" name="email" id="email" placeholder="Enter Your Email" class="pix_text">
                    <span class="send_btn">
                        <button type="submit" class="submit_btn pix_text" id="submit_btn_6">
                            <span class="editContent">Subscibe To Newsletter</span>
                        </button>
                    </span>
                </form>
            </div>
        </div>
<?include 'layout/footer.php';?>
