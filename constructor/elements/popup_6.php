<?include 'layout/header.php';?>

        <div class="light_gray_bg pix_builder_bg" id="section_call_2">
            <div class="footer3">
                <div class="container ">
                    <div class="ten columns alpha offset-by-three">
                        <div class="content_div center_text">
                            <div class="margin_bottom_30 blue_text">
                                <i class="pi pixicon-envelope title_56"></i> 
                            </div>
                            <div class="margin_bottom_30">
                                <p class="big_title editContent"><strong>Subscription Form</strong></p>
                                <p class="normal_text light_gray center_text editContent">This is an example section for popups layouts, you can use it as it is, or you can check our documentation to learn how to add popups to any button or link in your landing pages, it's easy as cup cakes!</p>
                                <p class="normal_text light_gray center_text editContent">
                                    You can also open this popup from any link in the page by adding the popup's id <strong>#popup_6</strong> in the link field.
                                </p>
                            </div>
                            <a href="#popup_6" class="pix_button pix_button_flat white bold_text orange_bg btn_big">
                                <span>POPUP BUTTON</span>
                            </a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="popup_6" class="well pix_popup pop_hidden blue_bg dark pix_builder_bg">
            <div class="center_text">
                <span class="editContent"><span class="margin_bottom normal_title pix_text"><strong>Subscription Form</strong></span></span>
                <p class="editContent">You can add unlimited fields directly from HTML Builder</p>
                <form id="contact_form" class="pix_form">
                    <div id="result"></div>
                    <input type="text" name="name" id="name" placeholder="Your Full Name" class="pix_text"> 
                    <input type="email" name="email" id="email" placeholder="Enter Your Email" class="pix_text">
                    <span class="send_btn">
                        <button type="submit" class="submit_btn pix_text orange_bg" id="submit_btn_6">
                            <span class="editContent">Subscibe To Newsletter</span>
                        </button>
                    </span>
                </form>
            </div>
        </div>
<?include 'layout/footer.php';?>
