<?include 'layout/header.php';?>


        <div class="light_gray_bg pix_builder_bg" id="section_call_2">
            <div class="footer3">
                <div class="container ">
                    <div class="ten columns alpha offset-by-three">
                        <div class="content_div center_text">
                            <div class="margin_bottom_30 orange">
                                <i class="pi pixicon-flag2 title_56"></i> 
                            </div>
                            <div class="margin_bottom_30">
                                <p class="big_title bold_text editContent">Contact Form Popup</p>
                                <p class="normal_text light_gray center_text editContent">This is an example section for popups layouts, you can use it as it is, or you can check our documentation to learn how to add popups to any button or link in your landing pages, it's easy as cup cakes!</p>
                                <p class="normal_text light_gray center_text editContent">
                                    You can also open this popup from any link in the page by adding the popup's id <strong>#popup_7</strong> in the link field.
                                </p>
                            </div>
                            <a href="#popup_7" class="pix_button pix_button_flat bold_text green_1_bg white btn_big">
                                <span>POPUP BUTTON</span>
                            </a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="popup_7" class="well pix_popup pop_hidden pix_builder_bg"> 
            <div class="center_text">
                <div class="big_icon orange">
                    <span class="pi pixicon-flag2"></span>
                </div>
                <span class="editContent"><h4 class="margin_bottom pix_text"><strong>Additional Informations</strong></h4></span>
                <p class="editContent">You can add unlimited fields directly from HTML via FLATPACK form builder</p>
                <form id="contact_form" class="pix_form">
                    <div id="result"></div>        
                    <div class="ten ">
                        <div class="five columns">
                            <div class="small_padding_right">
                                <input type="email" name="email" id="email" placeholder="Your Email" class="pix_text">
                            </div>
                        </div>
                        <div class="five columns">
                                <input type="text" name="name" id="name" placeholder="Your Full Name" class="pix_text"> 
                        </div>
                    </div>    

                    <textarea name="message" rows="3" id="message" placeholder="Your Message" class="pix_text"></textarea>
                    <span class="send_btn">
                        <button type="submit" class="submit_btn green_1_bg pix_text" id="submit_btn_6">
                            <span class="editContent">Send Information</span>
                        </button>
                    </span>
                    <div class="pix_text pix_note"><span class="editContent">*Some dummy text goes here.</span></div>
                </form>
            </div>
        </div>
<?include 'layout/footer.php';?>
