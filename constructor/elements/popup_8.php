<?include 'layout/header.php';?>
        <div class="light_gray_bg pix_builder_bg" id="section_call_2">
            <div class="footer3">
                <div class="container ">
                    <div class="ten columns alpha offset-by-three">
                        <div class="content_div center_text">
                            <div class="margin_bottom_30 dark_yellow">
                                <i class="pi pixicon-video2 title_56"></i> 
                            </div>
                            <div class="margin_bottom_30">
                                <p class="big_title bold_text editContent">Video Popup</p>
                                <p class="normal_text light_gray center_text editContent">This is an example section for popups layouts, you can use it as it is, or you can check our documentation to learn how to add popups to any button or link in your landing pages, it's easy as cup cakes!</p>
                                <p class="normal_text light_gray center_text editContent">
                                    You can also open this popup from any link in the page by adding the popup's id <strong>#popup_8</strong> in the link field.
                                </p>
                            </div>
                            <a href="#popup_8" class="pix_button pix_button_flat bold_text dark_yellow_bg white btn_big">
                                <span>POPUP BUTTON</span>
                            </a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="popup_8" class="well pix_popup pop_hidden pix_builder_bg" > 
            <div class="center_text">
                <div class="big_icon dark_yellow">
                    <span class="pi pixicon-video2"></span>
                </div>
                <span class="editContent"><h4 class="normal_title margin_bottom pix_text"><strong>Video Popup</strong></h4></span>
                <p class="editContent">You can edit the popup directly from PixBuilder</p>
                <div class="pix_video">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/Da4pTvROe78?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <div class="frameCover" data-type="video" data-selector=".frameCover"></div>
                </div>
                
            </div>
        </div>
<?include 'layout/footer.php';?>
