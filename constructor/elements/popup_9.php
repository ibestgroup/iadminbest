<?include 'layout/header.php';?>


        <div class="light_gray_bg pix_builder_bg" id="section_call_2">
            <div class="footer3">
                <div class="container ">
                    <div class="ten columns alpha offset-by-three">
                        <div class="content_div center_text">
                            <div class="margin_bottom_30 green_1">
                                <i class="pi pixicon-layers2 title_56"></i> 
                            </div>
                            <div class="margin_bottom_30">
                                <p class="big_title bold_text editContent">Buttons Popup</p>
                                <p class="normal_text light_gray center_text editContent">This is an example section for popups layouts, you can use it as it is, or you can check our documentation to learn how to add popups to any button or link in your landing pages, it's easy as cup cakes!</p>
                                <p class="normal_text light_gray center_text editContent">
                                    You can also open this popup from any link in the page by adding the popup's id <strong>#popup_9</strong> in the link field.
                                </p>
                            </div>
                            <a href="#popup_9" class="pix_button pix_button_flat bold_text orange_bg white btn_big">
                                <span>POPUP BUTTON</span>
                            </a>    
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="popup_9" class="well pix_popup pix_builder_bg pop_hidden" > 
            <div class="center_text">
                <div class="pix_inline_block">
                    <div class="six columns">
                        <span class="editContent"><h4 class="normal_title margin_bottom pix_text"><strong>Popup With Buttons</strong></h4></span>
                        <p class="editContent">Seamlessly empower fully researched growth strategies and interoperable internal.</p>
                        <a href="#" class="pix_button btn_normal green_1_bg pix_button_flat bold_text"><i class="pi pixicon-layout"></i> 
                            <span class="editContent">First Button</span>
                        </a>
                        <a href="#" class="pix_button btn_normal orange_bg pix_button_flat bold_text"><i class="pi pixicon-image"></i> 
                            <span class="editContent">Second Button</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
<?include 'layout/footer.php';?>
