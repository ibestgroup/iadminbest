<?php include 'layout/header.php'; ?>
	<?
$marks = array_filter(Main::getMarks(), function ($ar) {
    return $ar['status'] == 1;
});
?>
<!-- Price section -->
<section class="price-section price-section-3">
	<div class="container">
		<div class="row">
            <?
            if (Main::siteInfo()['mark_type'] != 3) {
                foreach ($marks as $mark) {
                    $price_level = [$mark['level1'], $mark['level2'], $mark['level3'], $mark['level4'], $mark['level5'], $mark['level6'], $mark['level7'], $mark['level8']];
                    ?>
                    <div class="col-md-4">
                        <div class="price-table-2">
                            <div class="price-box">
                                <h2 class="hd"><?= $mark['name'] ?></h2>
                                <div class="price"><?= Main::money_mark_auto($mark['id'], 'profit') ?><?= Main::getIcon() ?></div>
                                <a href="/login" class="btn btn-default btn-sm">Выбрать</a>
                                <hr>
                                <div class="quick-info">
                                    <p>
                                        Реинвест в <b
                                                class="p-reinvest-tarif-name"><?= Main::getMarks()[$mark['reinvest']]['name'] ?></b>
                                        -
                                        <span class="p-reinvest-tarif-sum"><?= Main::money_mark_auto($mark['reinvest']); ?></span><?= Main::getIcon() ?>
                                    </p>
                                    <hr>
                                    <?
                                    if ($mark['reinvest_first']) { ?>
                                        <p>Реинвест в <b><?= Main::getMarks()[1]['name'] ?></b>
                                            -
                                            <span class="p-reinvest-first-sum"><?= Main::money_mark_auto(1); ?></span><?= Main::getIcon() ?>
                                        </p>
                                        <hr>
                                    <?
                                    } ?>
                                </div><!-- /.quick-info -->
                                <div class="row tableWrapper pix_edit">
                                    <div class="col-lg-1 brb">Ур.</div>
                                    <div class="col-lg-3 brb">Цена</div>
                                    <div class="col-lg-4 brb">Чел</div>
                                    <div class="col-lg-3 brb">Доход</div>
                                </div>
                                <ul class="info p-mark-levels tableWrapper pix_edit">
                                    <?
                                    for ($i = 0; $i < $mark['level']; $i++) { ?>
                                        <li class="row p-marks-level-line">
                                            <div class="col-lg-1 brb p-mark-level-n"><?= $i + 1; ?></div>
                                            <div class="col-lg-3 brb"><span
                                                        class="p-mark-level-price"><?= $price_level[$i] ?></span><?= Main::getIcon(); ?>
                                            </div>
                                            <div class="col-lg-4 brb p-mark-level-user"><?= pow($mark['width'], $i + 1); ?></div>
                                            <div class="col-lg-3 brb"><span
                                                        class="p-mark-level-profit"><?= pow($mark['width'], $i + 1) * $price_level[$i] * 0.9 ?></span><?= Main::getIcon() ?>
                                            </div>
                                        </li>
                                        <?
                                    } ?>
                                </ul>
                            </div><!-- /.price-box -->
                        </div><!-- /.price-table -->
                    </div><!-- /.column -->
                <?
                }
            } else {
                foreach ($marks as $mark) {
                    $moneyMark = Main::moneyMark($mark);
                    $price_level = [$mark['level1'], $mark['level2'], $mark['level3'], $mark['level4'], $mark['level5'], $mark['level6'], $mark['level7'], $mark['level8']];
                    ?>
                    <div class="col-md-4">
                        <div class="price-table-2">
                            <div class="price-box">
                                <h2 class="hd"><?= $mark['name'] ?></h2>
                                <div class="price"><?=$moneyMark['res_profit']?><?= Main::getIcon() ?></div>
                                <a href="/login" class="btn btn-default btn-sm">Выбрать</a>
                                <hr>
                                <div class="quick-info">
                                    <p class="pix_edit">
                                        Вход в тариф <b><?=$mark['name']?></b>
                                        - <span class="p-reinvest-first-sum">
                                        <?if ($mark['type'] == 1) {
                                            if (Main::siteInfo()['value'] == 2) {$mark['level1'] = in_crypto($mark['level1']);}
                                            echo $mark['level1'];
                                        } else {
                                            echo $moneyMark['res_price'];
                                        }?>
                                        </span><?= Main::getIcon()?>
                                    </p>
                                </div>
                                <div class="row tableWrapper pix_edit">
                                    <div class="col-lg-1 brb">Ур.</div>
                                    <div class="col-lg-3 brb">Цена</div>
                                    <div class="col-lg-4 brb">Чел</div>
                                    <div class="col-lg-3 brb">Доход</div>
                                </div>
                                <ul class="info p-mark-levels tableWrapper pix_edit">
                                    <?
                                    for ($i = 0; $i < $mark['level']; $i++) {?>
                                        <li class="row p-marks-level-line">
                                            <div class="col-lg-1 brb p-mark-level-n"><?= $i+1;?></div>
                                            <div class="col-lg-3 brb">
                                                <span class="p-mark-level-price"><?if (Main::siteInfo()['value'] == 2) {
                                                        $price_level[$i] = in_crypto($price_level[$i]);}
                                                    echo $price_level[$i];?>
                                                </span>
                                                <?=Main::getIcon();?>
                                            </div>
                                            <div class="col-lg-4 brb p-mark-level-user"><?= pow($mark['width'], $i+1);?></div>
                                            <div class="col-lg-3 brb">
                                                <?if ($mark['overflow']) {?>
                                                    <span class="p-mark-level-profit">
                                                        <?=pow($mark['width'], $i+1)*($price_level[$i]*0.9);?>
                                                    </span>
                                                    <?=Main::getIcon();?>
                                                <?} else {?>
                                                    <span style='font-size: 50px; line-height: 17px;'>&#8734;</span>
                                                <?}?>
                                            </div>
                                        </li>
                                        <?
                                    } ?>
                                </ul>
                            </div><!-- /.price-box -->
                        </div><!-- /.price-table -->
                    </div><!-- /.column -->
                    <?
                }
            }?>
		</div>
	</div>
</section>


<?php include 'layout/footer.php'; ?>