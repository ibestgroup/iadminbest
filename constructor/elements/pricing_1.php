<?include 'layout/header.php';?>
<?
$marks = array_filter(Main::getMarks(), function ($ar) {
    return $ar['status'] == 1;
});

?>

        <div class="center_text big_padding light_gray_bg pix_builder_bg" id="section_intro_title">
            <div class="container ">
                <?
                foreach ($marks as $mark) {
                $price_level = [$mark['level1'], $mark['level2'], $mark['level3'], $mark['level4'], $mark['level5'], $mark['level6'], $mark['level7'], $mark['level8']];
                ?>
                    <div class="six columns">
                        <div class="flat_pricing pix_builder_bg">
                            <p class="big_title blue_text editContent">
                                <strong><?=Main::money_mark_auto($mark['id'], 'profit')?><?=Main::getIcon()?></strong>
                            </p>
                            <p class="light_gray editContent">
                                <strong><?=$mark['name']?></strong>
                            </p>
                            <div class="quick-info">
                                <p>
                                    Реинвест в <b class="p-reinvest-tarif-name"><?=Main::getMarks()[$mark['reinvest']]['name']?></b>
                                    - <span class="p-reinvest-tarif-sum"><?= Main::money_mark_auto($mark['reinvest']); ?></span><?=Main::getIcon()?>
                                </p>
                                <hr>
                                <?if ($mark['reinvest_first']) {?>
                                    <p>Реинвест в <b><?=Main::getMarks()[1]['name']?></b>
                                        - <span class="p-reinvest-first-sum"><?= Main::money_mark_auto(1);?></span><?= Main::getIcon()?>
                                    </p>
                                    <hr>
                                <?}?>
                            </div><!-- /.quick-info -->
                            <div class="row tableWrapper pix_edit">
                                <div class="col-lg-1 brb">Ур.</div>
                                <div class="col-lg-3 brb">Цена</div>
                                <div class="col-lg-4 brb">Чел</div>
                                <div class="col-lg-3 brb">Доход</div>
                            </div>
                            <ul class="flat_pricing_list light_gray p-mark-levels tableWrapper pix_edit">
                                <?
                                for ($i = 0; $i < $mark['level']; $i++) {?>
                                    <li class="row p-marks-level-line">
                                        <div class="col-lg-1 brb p-mark-level-n"><?= $i+1;?></div>
                                        <div class="col-lg-3 brb"><span class="p-mark-level-price"><?=$price_level[$i]?></span><?=Main::getIcon();?></div>
                                        <div class="col-lg-4 brb p-mark-level-user"><?= pow($mark['width'], $i+1);?></div>
                                        <div class="col-lg-3 brb"><span class="p-mark-level-profit"><?= pow($mark['width'], $i+1) * $price_level[$i] * 0.9?></span><?= Main::getIcon() ?></div>
                                    </li>
                                    <?
                                } ?>
                            </ul>
                            <a href="/login" class="active_bg_open pix_button btn_normal btn_big bold_text blue_bg ">
                                <span>Выбрать тариф</span>
                            </a> 
                        </div>
                    </div>
                <?}?>
            </div>
        </div>
<?include 'layout/footer.php';?>
