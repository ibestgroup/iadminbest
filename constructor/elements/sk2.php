
    <meta name="author" content="">
    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <meta http-equiv="x-ua-compatible" content="IE=9">
    <!-- Font Awesome -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
    <!--headerIncludes-->
    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="/iadminbest/constructor/elements/stylesheets/menu.css">
    <link rel="stylesheet" href="/iadminbest/constructor/elements/stylesheets/flat-ui-slider.css">
    <link rel="stylesheet" href="/iadminbest/constructor/elements/stylesheets/base.css">
    <link rel="stylesheet" href="/iadminbest/constructor/elements/stylesheets/landings.css">
    <link rel="stylesheet" href="/iadminbest/constructor/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/iadminbest/constructor/elements/stylesheets/skeleton.css">
    <link rel="stylesheet" href="/iadminbest/constructor/elements/stylesheets/main.css">
    <link rel="stylesheet" href="/iadminbest/constructor/elements/stylesheets/landings_layouts.css">
    <link rel="stylesheet" href="/iadminbest/constructor/elements/stylesheets/box.css">
    <link rel="stylesheet" href="/iadminbest/constructor/elements/stylesheets/pixicon.css">
    <link rel="stylesheet" href="/iadminbest/constructor/elements/stylesheets/rgen.min.css">
    <link href="/iadminbest/constructor/elements/assets/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" href="/iadminbest/constructor/css/cryptocoins.css" />
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="/iadminbest/constructor/elements/images/favicon.ico">
<!--    <link rel="apple-touch-icon" href="/constructor/elements/images/apple-touch-icon.png">-->
<!--    <link rel="apple-touch-icon" sizes="72x72" href="/constructor/elements/images/apple-touch-icon-72x72.png">-->
<!--    <link rel="apple-touch-icon" sizes="114x114" href="/constructor/elements/images/apple-touch-icon-114x114.png">-->
