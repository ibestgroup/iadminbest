<?include 'layout/header.php';?>
    <div class="pixfort_pix_slider pix_builder_bg" id="section_slider">
        <div class="container">
            <div class="sixteen columns">
                <div id="myCarousel" class="carousel slide" data-interval="false">    					
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="/constructor/elements/images/main/image-1.jpg" alt="" />
                            <div class="carousel-caption editContent">
                                <h3>Thumbnail label</h3>
                                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/constructor/elements/images/main/image-2.jpg" alt="" />
                            <div class="carousel-caption editContent">
                                <h3>Thumbnail label</h3>
                                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec.</p>
                            </div>
                        </div>
                        <div class="item">
                            <img src="/constructor/elements/images/main/image-3.jpg" alt="" />
                            <div class="carousel-caption editContent">
                                <h3>Thumbnail label</h3>
                                <p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec.</p>
                            </div>
                        </div>
                    </div>
                    <!-- Controls -->
                    <a class="left carousel-control fui-arrow-left" href="#myCarousel" data-slide="prev"></a>
                    <a class="right carousel-control fui-arrow-right" href="#myCarousel" data-slide="next"></a>
                </div>
            </div>
        </div><!-- /.container -->
    </div>
<?include 'layout/footer.php';?>
