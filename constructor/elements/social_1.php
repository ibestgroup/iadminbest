<?include 'layout/header.php';?>
    <div class="light_gray_bg pix_builder_bg" id="section_social_1">
        <div class="footer3">
            <div class="container ">
                <div class="sixteen columns ">
                    <div class="four columns alpha margin_vertical mobile_center">
                        <div class="center_text big_title">
                            <a class="pi pixicon-facebook2 facebook_bg social_1" href="#fakelink"></a>
                        </div>
                    </div>
                    <div class="four columns margin_vertical mobile_center">
                        <div class="center_text big_title">
                            <a class="pi pixicon-twitter2 twitter_bg social_1" href="#fakelink"></a>
                        </div>
                    </div>
                    <div class="four columns margin_vertical mobile_center">
                        <div class="center_text big_title">
                            <a class="pi pixicon-googleplus2 google_plus_bg social_1" href="#fakelink"></a>
                        </div>
                    </div>
                    <div class="four columns omega margin_vertical mobile_center">
                        <div class="center_text big_title">
                            <a class="pi pixicon-dribbble2 dribbble_bg social_1" href="#fakelink"></a>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
