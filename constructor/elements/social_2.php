<?include 'layout/header.php';?>
    <div class="light_gray_bg big_padding pix_builder_bg" id="section_social_2">
        <div class="container">
            <div class="sixteen columns ">
                <div class="center_text title_70">
                    <a class="pi pixicon-facebook2 normal_gray margin_h" href="#fakelink"></a>
                    <a class="pi pixicon-twitter2 normal_gray margin_h" href="#fakelink"></a>
                    <a class="pi pixicon-dribbble2 normal_gray margin_h" href="#fakelink"></a>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
