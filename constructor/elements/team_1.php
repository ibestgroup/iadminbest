<?include 'layout/header.php';?>

        <div class="center_text big_padding pix_builder_bg" id="section_intro_title">
            <div class="container">       
                    <div class="eight columns">
                        <div class="simple_team">
                            <div class="simple_team_img ">
                                <img src="/constructor/elements/images/main/placeholder150.png" alt="">
                            </div>
                            <div>
                                <h4 class="editContent gray">
                                    <strong>John Doe</strong>
                                </h4>
                                <p class="editContent small_text light_gray">
                                    Lorem ipsum dolor sit amet consectetur elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <ul class="bottom-icons">
                                    <li><a class="pi pixicon-facebook2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-twitter2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-instagram light_gray" href="#fakelink"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="eight columns">
                        <div class="simple_team">
                            <div class="simple_team_img ">
                                <img src="/constructor/elements/images/main/placeholder150.png" alt="">
                            </div>
                            <div>
                                <h4 class="editContent gray">
                                    <strong>Marc Smith</strong>
                                </h4>
                                <p class="editContent small_text light_gray">
                                    Lorem ipsum dolor sit amet consectetur elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <ul class="bottom-icons">
                                    <li><a class="pi pixicon-facebook2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-twitter2 light_gray" href="#fakelink"></a></li>
                                    <li><a class="pi pixicon-instagram light_gray" href="#fakelink"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
<?include 'layout/footer.php';?>
