<?include 'layout/header.php';?>
	<div class="light_gray_bg big_padding pix_builder_bg" id="section_testimonials_1">
		<div class="container ">
			<div class="one-third column">
				<div class="content_div">
					<div class="margin_bottom">
						<img class="small_circle border_light" src="/constructor/elements/images/main/t1.png">
					</div>
                	<p class="editContent light_gray normal_text">Lorem ipsum dolor sit amet consectet adipiscing elit sed do eiusmoda tempo incididunt ut labore et dolore magnar aliqua Ut enim ad  voluptate velit esse.</p>                	
                	<h6 class="editContent bold_text dark_gray">Mark Smith</h6>
				</div>
			</div>
			<div class="one-third column">
				<div class="content_div">
					<div class="margin_bottom">
						<img class="small_circle border_light" src="/constructor/elements/images/main/t2.png">
					</div>
                	<p class="editContent light_gray normal_text">Lorem ipsum dolor sit amet consectet adipiscing elit sed do eiusmoda tempo incididunt ut labore et dolore magnar aliqua Ut enim ad  voluptate velit esse.</p>                	
                	<h6 class="editContent bold_text dark_gray">John Due</h6>
				</div>
            </div>
            <div class="one-third column omega">
            	<div class="content_div">
            		<div class="margin_bottom">
						<img class="small_circle border_light" src="/constructor/elements/images/main/t3.png">
					</div>
                	<p class="editContent light_gray normal_text">Lorem ipsum dolor sit amet consectet adipiscing elit sed do eiusmoda tempo incididunt ut labore et dolore magnar aliqua Ut enim ad  voluptate velit esse.</p>                	
                	<h6 class="editContent bold_text dark_gray">Sara Clark</h6>
                </div>
            </div>
		</div>
	</div>
<?include 'layout/footer.php';?>
