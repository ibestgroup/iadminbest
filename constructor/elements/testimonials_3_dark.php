<?include 'layout/header.php';?>
	<div class="dark center_text big_padding pix_builder_bg" id="section_testimonials_3_dark">
		<div class="container ">
			<div class="eight columns offset-by-four">
				<div class="content_div">
					<div class="margin_bottom">
						<img class="small_circle border_light" src="/constructor/elements/images/main/t2.png">
					</div>
                	<p class="editContent light_gray normal_text">Lorem ipsum dolor sit amet consectet adipiscing elit sed do eiusmoda tempo incididunt ut labore et dolore magnar aliqua Ut enim ad  voluptate velit esse.</p>                	
                	<h6 class="editContent bold_text dark_gray">Mark Smith</h6>
				</div>
			</div>
		</div>
	</div>
<?include 'layout/footer.php';?>
