<?include 'layout/header.php';?>
	<div class="pixfort_text_3 pix_builder_bg" id="section_text_3">
		<div class="container ">
			<div class="one-third column">
				<div class="content_div">
					<h4 class="editContent">First Headline Goes Here</h4>
                	<p class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmoda tempor incididunt ut labore et dolore magna aliqua Ut enim ad  voluptate velit esse cillum dolore.</p>
                	<a class="text_3_button slow_fade editContent pix_text" href="#">LEARN MORE</a>
				</div>
			</div>
			<div class="one-third column">
				<div class="content_div">
					<h4 class="editContent">Second Headline Goes Here</h4>
                	<p class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad  voluptate velit esse cillum dolore.</p>
                	<a class="text_3_button slow_fade editContent pix_text" href="#">LEARN MORE</a>
				</div>
            </div>
            <div class="one-third column omega">
            	<div class="content_div">
            		<h4 class="editContent">Third Headline Goes Here</h4>
                	<p class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad  voluptate velit esse cillum dolore.</p>
                	<a class="text_3_button slow_fade editContent pix_text" href="#">LEARN MORE</a>
                </div>
            </div>
		</div>
	</div>
<?include 'layout/footer.php';?>
