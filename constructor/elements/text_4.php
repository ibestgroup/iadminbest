<?include 'layout/header.php';?>
    <div class="pixfort_text_4 pix_builder_bg" id="section_text_4">
        <div class="container ">
            <div class="four columns alpha">
            	<div class="content_div">
            		<h4>First Headline</h4>
                	<p>Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmoda tempor incididunt.</p>
                	<a class="text_3_button slow_fade editContent pix_text" href="#">LEARN MORE</a>
                </div>
            </div>
            <div class="four columns">
				<div class="content_div">
					<h4 class="editContent">Second Headline</h4>
                	<p class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</p>
                	<a class="text_3_button slow_fade editContent pix_text" href="#">LEARN MORE</a>
                </div>
            </div>
            <div class="four columns">
				<div class="content_div">
					<h4 class="editContent">Third Headline</h4>
                	<p class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</p>
                	<a class="text_3_button slow_fade editContent pix_text" href="#">LEARN MORE</a>
                </div>
            </div>
            <div class="four columns omega">
            	<div class="content_div">
            		<h4 class="editContent">Fourth Headline</h4>
                	<p class="editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt.</p>
                	<a class="text_3_button slow_fade editContent pix_text" href="#">LEARN MORE</a>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
