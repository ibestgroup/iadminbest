<?include 'layout/header.php';?>
	<div class="light_gray_bg big_padding pix_builder_bg" id="section_title_2">
		<div class="container ">       
	            <div class="twelve columns offset-by-two">
	            	<div class="center_text">
	            		<div class="title_56 margin_bottom_10 light_blue">
				            <span class="pi pixicon-telescope"></span>
				        </div>
	            		<h2 class="big_title dark_gray editContent">Middle Heading Style</h2>
	                	<p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad  voluptate velit esse cillum dolore eu fugiat.</p>
	                </div>
	            </div>
	    </div>
	</div>
<?include 'layout/footer.php';?>
