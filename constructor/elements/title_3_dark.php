<?include 'layout/header.php';?>
	<div class="dark big_padding pix_builder_bg" id="section_title_3_dark">
		<div class="container ">       
	            <div class="sixteen columns">
	            	<div class="right_text">
	            		<div class="title_56 margin_bottom_10 light_blue">
				            <span class="pi pixicon-chat"></span>
				        </div>
	            		<h2 class="big_title dark_gray editContent">Right Heading Style</h2>
	                	<p class="normal_text light_gray editContent">Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad  voluptate velit esse cillum dolore eu fugiat.</p>
	                </div>
	            </div>
	    </div>
	</div>
<?include 'layout/footer.php';?>
