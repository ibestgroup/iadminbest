<?include 'layout/header.php';?>
    <div class="dark" id="section_title_4_dark">
        <div class="big_padding pix_builder_bg">
            <div class="container">
                <div class="sixteen columns">
                    <div class="eight columns alpha">
                        <p class="big_title bold_text editContent">Make Awesome Heading Section</p>
                    </div>
                    <div class="eight columns alpha">
                        <p class="big_text normal_gray editContent">
                            From logo design to website development, hand-picked designers and developers are ready to complete your new project.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?include 'layout/footer.php';?>
