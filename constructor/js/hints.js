function jsHints() {
    var enjoyhint_instance = new EnjoyHint({
        onSkip: function () {
            enjoyhint_instance.saveCurrentTutor({skip: true, lastStep: enjoyhint_instance.getCurrentStep()});
            $('#menu').removeClass('open');
        }
    });

    var enjoyhint_script_steps = [
        {
            "next #menu": "Здравствуйте! Позвольте я помогу вам создать свой сайт! <br>" +
            "<text style='color: #00a6eb'>Меню.</text>" +
            " Здесь вы можете настраивать страницы и выбирать блоки для размещения.",
            "nextButton" : {className: "myNext", text: "Дальше"},
            showSkip: false
        },
        {
            "next .i-tutor-pages": "Эта часть предназначена для добавления и настройки страниц.",
	        "nextButton" : {className: "myNext", text: "Дальше"},
            showSkip: false
        },
        {
            "next .i-tutor-pages-1" : "В этом разделе перечислены ваши созданные страницы. А справа от страницы расположены кнопки для копирования и переименования страниц.",
	        "nextButton" : {className: "myNext", text: "Дальше"},
            showSkip: false
        },
	    {
		    "next .i-tutor-pages-2" : "Что бы добавить новую страницу, воспользуйтесь этой кнопкой.",
		    "nextButton" : {className: "myNext", text: "Дальше"},
		    showSkip: false
	    },
        {
	        "next .i-tutor-block" : "Для добавления блока на страницу, выберите необходимый раздел.",
	        "nextButton" : {className: "myNext", text: "Дальше"},
	        showSkip: false
        },
        {
            "next #mini_button" : "At this step we fix radius<br>" +
            "<text style='color: #00ebe7'>radius</text> - sets the size of the circle radius<br>" +
            "{<br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' next #selector '</text> : <text style='color: #2bff3c'>' Some description ',</text> <br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' shape '</text> :  <text style='color: #2bff3c'>' circle '</text> <br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' radius '</text> : 80<br>" +
            "}<br>",
            shape : 'circle',
            radius: 80
        },
        {
            "next #animationSpeed" : "Sometimes you need to scroll the page either very slowly (as we've just done) or very fast.<br>" +
            "<text style='color: #00ebe7'>scrollAnimationSpeed</text> - sets the speed for the scroll page<br>" +
            "{<br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' next #selector '</text> : <text style='color: #2bff3c'>' Some description ',</text> <br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' scrollAnimationSpeed '</text> : 2500<br>" +
            "}<br>",
            scrollAnimationSpeed : 2500
        },
        {
            "key #suc_input" : "You can attach handlers to keyboard events.<br>" +
            "<text style='color: #00ebe7'>keyCode</text> - key code for any 'key' event.<br>" +
            "{<br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' key #selector '</text> : <text style='color: #2bff3c'>' Some description ',</text> <br>" +
            "<text style='color: #00a6eb'>&nbsp &nbsp ' keyCode '</text> : 13<br>" +
            "}<br>" +
            "Enter some text and press 'Enter'",
            keyCode : 13
        }

    ];

    if (typeof tutor.lastStep !== undefined) {
        enjoyhint_script_steps = enjoyhint_script_steps.slice(tutor.lastStep);
    }

    //set script config
    enjoyhint_instance.set(enjoyhint_script_steps);

    //run Enjoyhint script
    enjoyhint_instance.run();
}

EnjoyHint.prototype.saveCurrentTutor = function (data) {
    for (var key in data) {
        tutor[key] = data[key];
    }
    console.log(tutor);
    localStorage.setItem('tutor', JSON.stringify(tutor));
};
