$(function() {
    var rgen = {};
    rgen.elcheck = function(el) {
        'use strict';
        if ($(el).length > 0) {
            return true;
        } else {
            return false;
        };
    };
    rgen.videoBg = function (obj, imglist) {
        'use strict';
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };

        if( isMobile.any() ){
            $(obj).css("display","none");
            /*$(obj).vegas({
                slides: [
                    { src: "images/bg-1.jpg" },
                    { src: "images/bg-2.jpg" },
                    { src: "images/bg-3.jpg" },
                    { src: "images/bg-4.jpg" }
                ]
                slides: imglist
            });*/
        }
        else{
            $(obj).css("display","block");
            console.log($(obj).data('video'));
            var videoData = {videoURL:$(obj).data('video'),containment:'#videobg1',showControls:false, autoPlay:true, loop:true, vol:50, mute:true, startAt:0, realfullscreen:false, opacity:1, addRaster:true, quality:'default', optimizeDisplay:true};
            $(obj).YTPlayer(videoData);
        }
    };
    /* Apply background image
    ********************************************/
    if (rgen.elcheck("[data-bg]")) {
        $("[data-bg]").each(function(index, el) {
            $(this).css({backgroundImage: "url("+$(this).attr("data-bg")+")"});
        });
    }

    if (rgen.elcheck(".videobg")) {
        $(".videobg").each(function(index, el) {
            rgen.videoBg(el);
        });
    };
});