<?php

use Illuminate\Database\Seeder;
use App\Models\PaymentSystems;

class PaymentSystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = json_decode(file_get_contents(__DIR__."/data/PaymentSystems.json"));

        foreach ($data as $insert) {
            PaymentSystems::insert([
                'name' => $insert->name,
                'code' => $insert->code,
                'min' => $insert->min,
                'commission' => $insert->commission,
                'example' => $insert->example,
            ]);
        }
    }
}
