import CashOut from './components/CashOut';
import TicketAdd from './forms/TicketAdd';
import ProfileEdit from './forms/ProfileEdit';
import TicketAnswer from './forms/TicketAnswer';
import NewsAdd from './forms/NewsAdd';
import FaqAdd from './forms/FaqAdd';
import Promo from './forms/Promo';
import AdEdit from './forms/AdEdit';
import {NewsDelete, FaqDelete, PromoDelete} from './components/handlers';
import {AdminChartPayments, AdminChartActivation, AdminChartVisit, AdminChartRegister} from './components/admin/AdminChart';
import {AdminMain} from './components/admin/AdminMain';
import AdminUsers from './components/admin/AdminUsers';
import AdminMarks from './components/admin/AdminMarks';
import AdminMarksCustom from './components/admin/AdminMarksCustom';
import AdminWidgets from './components/admin/AdminWidgets';
import UserEdit from './components/admin/UserEdit';
import TreeHorizontal from './components/tree/treeHorizontal';
import SiteInfoEdit from "./components/admin/SiteInfoEdit";
import TreeHorizontalDrag from "./components/tree/treeHorizontalDrag";
import CashoutOrder from "./forms/CashoutOrder";
import Noty from "noty";
import SbAdmin from "./components/admin/SbAdmin";
import PreloaderProfile from "./components/PreloaderProfile";
import CashOutCrypto from "./components/CashOutCrypto";
import Friends from "./components/Friends";
import ParkingDomain from "./components/admin/ParkingDomain";
import EditMenu from "./forms/EditMenu";
import EditSite from "./forms/EditSite";
import CreateSite from "./forms/CreateSite";
import Clipboard from "clipboard";
import CopyRef from "./CopyRef";
import Roulette from "./components/Roulette";
import ProfileBuilder from "./forms/ProfileBuilder";
import Store from "./forms/Store";
import SidebarAdmin from "./forms/SidebarAdmin";
import BuxoneSync from "./forms/BuxoneSync";
import Birds from './forms/Birds';
import GetBonus from './forms/GetBonus';
import BuyBirds from "./forms/BuyBirds";
import CollectEgg from "./forms/CollectEgg";
import CashoutForBuy from "./components/CashoutForBuy";
import BirdsOrderEdit from "./components/admin/BirdsOrderEdit";
import EqualHeight from "./Helpers/EqualHeight";
import ToggleSidebars from "./vendor/ToggleSidebars";
import TelegramConfirm from "./components/TelegramConfirm";
import Notice from "./Helpers/Notice";
import NoticeTest from "./Helpers/NoticeTest";
import 'jquery.cookie';
import iAuthProtection from "./components/iAuthProtection";
import BotSettings from "./components/admin/BotSettings";


export default class Application {
    constructor(root) {

        this.root = root;

        this.__initCommon();
    }


    __initCommon() {

        if ($('.js-cashOut').length) {
            new CashOut();
        }

        if ($('.js-cashOutCrypto').length) {
            new CashOutCrypto($('.js-cashOutCrypto'));
        }

        if ($('.js-cashoutOrder').length) {
            new CashoutOrder();
        }


        if ($('.js-ticketAdd').length) {
            new TicketAdd();
        }

        if ($('.js-answerAdd').length) {
            new TicketAnswer();
        }

        if ($('.js-newsAdd').length) {
            new NewsAdd();
        }

        if ($('.js-faqAdd').length) {
            new FaqAdd();
        }

        if ($('.js-adEdit').length) {
            new AdEdit();
        }

        if ($('.js-promo').length) {
            new Promo();
        }

        if ($('.js-profile-edit').length) {
            new ProfileEdit();
        }

        if ($('.js-delete-news').length) {
            new NewsDelete();
        }

        if ($('.js-delete-faq').length) {
            new FaqDelete();
        }

        if ($('.js-delete-promo').length) {
            new PromoDelete();
        }

        if ($('.link-clipboard').length) {
            new CopyRef();
        }

        if ($('.js-friends').length) {
            new Friends();
        }

        if ($('.js-telegram-confirm').length || $('input[name=iauth_code]').length || $('.iauth-protected').length) {
            // new TelegramConfirm();
        }



        if ($('.js-iauth-protection').length) {
            new iAuthProtection();
        }

        /**
         * Referal tree
         */

        if ($('.js-tree-tarif').length) {
            new TreeHorizontalDrag('.js-tree-tarif');
        }

        /**
         * Admin Charts
         */

        if ($('#chart-payments').length) {
            new AdminChartPayments();
        }

        if ($('#chart-activation').length) {
            new AdminChartActivation();
        }

        if ($('#chart-visit').length) {
            new AdminChartVisit();
        }

        if ($('#chart-register').length) {
            new AdminChartRegister();
        }
        /**
         * Admin load-page
         */

        if ($('.load-page').length) {
            new AdminMain();
        }

        if ($('#load-users').length) {
            new AdminUsers();
        }

        if ($('.js-markEdit').length) {
            new AdminMarks();
        }

        if ($('.js-markEditCustom').length) {
            new AdminMarksCustom();
        }

        if ($('.js-siteInfo').length) {
            new SiteInfoEdit();
        }

        if ($('.js-userEdit').length) {
            new UserEdit();
        }

        if ($('.js-parking').length) {
            new ParkingDomain();
        }

        if ($('.js-store').length) {
            new Store();
        }

        if ($('.js-sidebar-admin').length) {
            new SidebarAdmin();
        }


        /**
         * widgets
         */
        if ($('.js-widgets').length) {
            new AdminWidgets();
            new PreloaderProfile();
        }


        if ($('.js-editMenu').length) {
            new EditMenu();
        }

        if ($('.js-createSite').length) {
            new CreateSite();
        }

        if ($('.js-editSite').length) {
            new EditSite();
        }

        if ($('.js-roulette').length) {
            new Roulette();
        }

        if ($('#editor1').length) {
        }


        /**
         * Profile Builder
         */
        if ($('.js-profile-builder').length || $('.referal-refresh').length || $('.wrapper-admin').length) {
            new ProfileBuilder();
        }


        // if ($('.cc').length) {
        //     new CryptoIcons();
        // }



        if ($('.js-buxone').length) {
            new BuxoneSync();
        }

        /**
         * for ferma
         */

        if ($('.js-birds').length) {
            new Birds();
        }

        if ($('.js-getBonus').length) {
            new GetBonus();
        }

        if ($('.js-buyBirds').length) {
            new BuyBirds();
        }

        if ($('.js-myDeposits').length) {
            new CollectEgg();
        }

        if ($('.for-equal-height').length) {
            new EqualHeight();
        }

        if ($('.js-birds-order').length) {
            new BirdsOrderEdit();
        }


        if ($('.toggle-sidebar-left').length || $('.toggle-sidebar-right').length) {
            new ToggleSidebars();
        }

        if ($('.demo-enable').length) {
            $('body').on('click', '.demo-enable', function () {
                let cook = $(this).attr('data-template');

                $.cookie('demo', cook, {
                    expires: 1,
                    path: '/',
                });
                window.open('/profile', '_blank');
            });
        }

        if ($('.demo-disable').length) {
            $('body').on('click', '.demo-disable', function () {
                $.removeCookie('demo');
                window.location.href = '/profile';
            });
        }

        if ($('.notify-test').length) {
            $('body').on('click', '.notify-test', function () {
                let type = $(this).attr('data-type'),
                    theme = $('.theme-notify').val(),
                    position = $('.position-notify').val();

                new NoticeTest(type, "Тестовое сообщение", position, theme);
                // new Notice(type, "Тестовое сообщение", position, theme);
            });
        }

        if ($('.doc-content').length) {
                $('li[data-content]').on('click', function () {
                    $('li[data-content]').removeClass('active');
                    $(this).addClass('active');
                    let el = $($(this).attr('data-content'));
                    $('.doc-content').hide();
                    el.show();
                });
        }

        if ($('#tree1').length) {
            new BotSettings();
        }
    }
}
