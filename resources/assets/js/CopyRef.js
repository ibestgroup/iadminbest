import Notice from './Helpers/Notice';
import Clipboard from "clipboard";

export default class CopyRef {
    constructor() {
        $('.link-clipboard').on('click', function () {
            new Notice('success', 'Ссылка скопирована в буфер обмена');
        });
        new Clipboard('.link-clipboard');
    }
}