export default class EqualHeight {

    /**
     * Notification Wrapper
     * @param type
     * @param text
     */
    constructor () {
        let tallestcolumn = 0;
        $('.for-equal-height').each(function ()
            {
                let currentHeight = $(this).height() + 25;
                if(currentHeight > tallestcolumn)
                {
                    tallestcolumn = currentHeight;
                }
            }
        );
        $('.for-equal-height').height(tallestcolumn);
    }
}