
export default class ModalEditMenu {

    /**
     * Notification Wrapper
     * @param type
     * @param text
     */
    constructor (text) {
        this.modal = `
                <div class="modal fade in" id="modal-default">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title">Настройка</h4>
                            </div>
                            <div class="modal-body">
                            <div class="col-lg-12">
                                <div class="col-lg-2">Иконка</div>
                                <div class="col-lg-5">Текст</div>
                                <div class="col-lg-5">Ссылка</div>
                            </div>
                            <div class="col-lg-12 mb-10">
                                <div class="col-lg-2">
                                    <select class="selectpicker from-control icon-menu">
                                        <option data-content="<i class='fa fa-tachometer' aria-hidden='true'></i>" value="fa fa-tachometer"></option>
                                        <option data-content="<i class='fa fa-users' aria-hidden='true'></i>" value="fa fa-users"></option>
                                        <option data-content="<i class='fa fa-sliders' aria-hidden='true'></i>" value="fa fa-sliders"></option>
                                        <option data-content="<i class='fa fa-life-ring' aria-hidden='true'></i>" value="fa fa-life-ring"></option>
                                        <option data-content="<i class='fa fa-star' aria-hidden='true'></i>" value="fa fa-star"></option>
                                        <option data-content="<i class='fa fa-bell' aria-hidden='true'></i>" value="fa fa-bell"></option>
                                        <option data-content="<i class='fa fa-bed' aria-hidden='true'></i>" value="fa fa-bed"></option>
                                        <option data-content="<i class='fa fa-bullseye' aria-hidden='true'></i>" value="fa fa-bullseye"></option>
                                        <option data-content="<i class='fa fa-calendar' aria-hidden='true'></i>" value="fa fa-calendar"></option>
                                        <option data-content="<i class='fa fa-cloud' aria-hidden='true'></i>" value="fa fa-cloud"></option>
                                        <option data-content="<i class='fa fa-comments' aria-hidden='true'></i>" value="fa fa-comments"></option>
                                        <option data-content="<i class='fa fa-cog' aria-hidden='true'></i>" value="fa fa-cog"></option>
                                        <option data-content="<i class='fa fa-gift' aria-hidden='true'></i>" value="fa fa-gift"></option>
                                        <option data-content="<i class='fa fa-envelope' aria-hidden='true'></i>" value="fa fa-envelope"></option>
                                        <option data-content="<i class='fa fa-desktop' aria-hidden='true'></i>" value="fa fa-desktop"></option>
                                        <option data-content="<i class='fa fa-glass' aria-hidden='true'></i>" value="fa fa-glass"></option>
                                        <option data-content="<i class='fa fa-leaf' aria-hidden='true'></i>" value="fa fa-leaf"></option>
                                        <option data-content="<i class='fa fa-male' aria-hidden='true'></i>" value="fa fa-male"></option>
                                        <option data-content="<i class='fa fa-plane' aria-hidden='true'></i>" value="fa fa-plane"></option>
                                        <option data-content="<i class='fa fa-sun-o' aria-hidden='true'></i>" value="fa fa-sun-o"></option>
                                        <option data-content="<i class='fa fa-sign-in' aria-hidden='true'></i>" value="fa fa-sign-in"></option>
                                        <option data-content="<i class='fa fa-rocket' aria-hidden='true'></i>" value="fa fa-rocket"></option>
                                        <option data-content="<i class='fa fa-trophy' aria-hidden='true'></i>" value="fa fa-trophy"></option>
                                        <option data-content="<i class='fa fa-venus-mars' aria-hidden='true'></i>" value="fa fa-venus-mars"></option>
                                        <option data-content="<i class='fa fa-file' aria-hidden='true'></i>" value="fa fa-file"></option>
                                        <option data-content="<i class='fa fa-area-chart' aria-hidden='true'></i>" value="fa fa-area-chart"></option>
                                        <option data-content="<i class='fa fa-bar-chart' aria-hidden='true'></i>" value="fa fa-bar-chart"></option>
                                        <option data-content="<i class='fa fa-line-chart' aria-hidden='true'></i>" value="fa fa-line-chart"></option>
                                        <option data-content="<i class='fa fa-rub' aria-hidden='true'></i>" value="fa fa-rub"></option>
                                        <option data-content="<i class='fa fa-money' aria-hidden='true'></i>" value="fa fa-money"></option>
                                        <option data-content="<i class='fa fa-eur' aria-hidden='true'></i>" value="fa fa-eur"></option>
                                        <option data-content="<i class='fa fa-btc' aria-hidden='true'></i>" value="fa fa-btc"></option>
                                        <option data-content="<i class='fa fa-usd' aria-hidden='true'></i>" value="fa fa-usd"></option>
                                        <option data-content="<i class='fa fa-skype' aria-hidden='true'></i>" value="fa fa-skype"></option>
                                        <option data-content="<i class='fa fa-odnoklassniki' aria-hidden='true'></i>" value="fa fa-odnoklassniki"></option>
                                        <option data-content="<i class='fa fa-vk' aria-hidden='true'></i>" value="fa fa-vk"></option>
                                        <option data-content="<i class='fa fa-microchip' aria-hidden='true'></i>" value="fa fa-microchip"></option>
                                        <option data-content="<i class='fa fa-book' aria-hidden='true'></i>" value="fa fa-book"></option>
                                        <option data-content="<i class='fa fa-gear' aria-hidden='true'></i>" value="fa fa-gear"></option>
                                        <option data-content="<i class='fa fa-th' aria-hidden='true'></i>" value="fa fa-th"></option>
                                        <option data-content="<i class='fa fa-gears' aria-hidden='true'></i>" value="fa fa-gears"></option>
                                    </select>
                                </div>
                                <div class="col-lg-5">
                                    <input type="text" class="text-menu" value="${text}">
                                </div>
                                <div class="col-lg-5 modal-link-edit">
                                    <div class="tab-link tab-link-active" data-tab="tab-link-1">Ссылки сайта</div>
                                    <div class="tab-link" data-tab="tab-link-2">Своя ссылка</div>
                                    <div class="tabs-link tab-link-1">
                                        <div class="form-group">
                                            <select class="form-control selectlink link-menu link-result" name="">
                                                <option value="/profile">Аккаунт</option>
                                                <option value="/profile/edit">Редактировать профиль</option>
                                                <option value="/profile/faq">FAQ</option>
                                                <option value="/profile/news">Новости</option>
                                                <option value="/profile/ticketadd">Создать вопрос</option>
                                                <option value="/profile/tickets">Вопросы</option>
                                                <option value="/profile/tickets?meticket=1">Мои вопросы</option>
                                                <option value="/profile/referral-list">Список рефералы</option>
                                                <option value="/profile/referral-list/active">Активные рефералы</option>
                                                <option value="/profile/reflink">Реферальные ссылки</option>
                                                <option value="/profile/historypay">История операций</option>
                                                <option value="/profile/admin">Админ панель</option>
                                                <option value="/profile/cashout">Вывод средств</option>
                                                <option value="/profile/promo">Промо материалы</option>
                                                <option value="/profile/orders">Раздел покупок</option>
                                                <option value="/profile/orders/my-orders">Мои покупки(объекты)</option>
                                                <option value="/profile/orders/stock">Склад</option>
                                                <option value="/profile/bonus">Бонус за клик</option>
                                                <option value="/profile/affiliate">Партнерская программа</option>
                                                <option value="/profile/hash-power">Псевдоочки</option>
                                                <option value="/profile/cashout-for-buy">Обмен на баланс для покупок</option>
                                                <option value="/profile/referalTree">Реферальное дерево</option>
                                                <option value="/logout">Выход</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="tabs-link tab-link-2" style="display: none;">
                                        <div class="form-group">
                                            <input type="text" class="link-menu-my form-control link-result" placeholder="Введите ссылку">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                                <button type="button" class="btn btn-primary save-changes">Применить</button>
                            </div>
                        </div>
                    </div>
                </div>
            `;
    }
}