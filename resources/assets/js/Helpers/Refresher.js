import axios from 'axios';
import _ from 'lodash';
import Notice from "./Notice";
import 'bootstrap-select';
import FileInput from "../vendor/File";
import ProfileBuilder from "../forms/ProfileBuilder"
// import {
//     AdminChartActivation,
//     AdminChartPayments,
//     AdminChartRegister,
//     AdminChartVisit
// } from "../components/admin/AdminChart";
import AdminMarksCustom from '../components/admin/AdminMarksCustom';
import "ckeditor4";
import TreeHorizontalDrag from "../components/tree/treeHorizontalDrag";
import iAuthProtection from "../components/iAuthProtection";
import BotSettings from "../components/admin/BotSettings";

export default class Refresh {
    constructor(path = location.pathname, container = '.content', source = '.content', data = {}, param = {}) {

        this.path = path;
        this.container = container;
        this.source = source;

        this.data = data;
        this.param = param;
        this.overlayTemplate = '<div class="overlay" style="position: absolute; text-align: center;"><img src="https://1.bp.blogspot.com/-RRbd-J49gic/XTK6tgT-y3I/AAAAAAAAO_E/akowvdssME0gKj1ukMtft4G8uxuACWqPACLcBGAs/s1600/loader%2B%25281%2529.gif"></div>';

        if (this.param.type == 'small') {
            this.overlayTemplate = '<div class="overlay" style="position: absolute; text-align: center;"><img src="https://1.bp.blogspot.com/-RRbd-J49gic/XTK6tgT-y3I/AAAAAAAAO_E/akowvdssME0gKj1ukMtft4G8uxuACWqPACLcBGAs/s1600/loader%2B%25281%2529.gif"></div>';
        } else if (this.param.type == 'absolute') {
        } else {
            this.overlayTemplate = '<div class="overlay" style="position:absolute; text-align: center;"><img src="https://1.bp.blogspot.com/-RRbd-J49gic/XTK6tgT-y3I/AAAAAAAAO_E/akowvdssME0gKj1ukMtft4G8uxuACWqPACLcBGAs/s1600/loader%2B%25281%2529.gif"></div>';
        }





        this.get();

    }

    get() {
        this.start();
        axios.get(this.path, {params: this.data}).then((response) => {
            $(this.container).html($(response.data).find(this.source).html());
            this.end();
        }).catch((error) => {
            this.end();

            if (error.message == "Request failed with status code 401") {
                new Notice("error", "Авторизуйтесь пожалуйста.");
            } else {
                new Notice("error", error.message);
            }
        })
    }

    start() {
        if (this.param.type == 'small') {
            $(this.container).html($(this.overlayTemplate));
        } else if (this.param.type == 'absolute') {
        } else {
            $(this.container).append($(this.overlayTemplate));
        }
    }

    end() {

        if ($.cookie('lang') === 'en') {
            this.loadLogo = 'Upload logo',
                this.loadAva = 'Upload avatar',
                this.loadImg = 'Upload image';
        } else {
            this.loadLogo = 'Загрузить логотипs',
                this.loadAva = 'Загрузить аватар',
                this.loadImg = 'Загрузить изображение';
        }

        $('.selectpicker').selectpicker('refresh');

        $('input[name=iauth_code]').pincodeInput({inputs:6, placeholders: "0 0 0 0 0 0"});

        // $('.iauth-pinlogin').pinlogin({fields:6, hideinput:false, reset: false, placeholder: '0', complete: function(pin) {
        //         $('input[name=iauth_code]').val(pin);
        //     }
        // });

        $('.iauth-protected').closest('form').find('input').on('focus', function () {
            let userInfo;
            $.ajax({
                type: `GET`,
                url: `/get-user-info`,
                dataType: 'json',
                async: false,
                success: function (data) {
                    userInfo = data;
                }
            });
            $(this).closest('form').find('.iauth-protected').show(100);
        });

        $('body').find('.example-disabled').find('input, select, textarea').attr('disabled', 'disabled').removeClass('selectpicker');

        new FileInput($('form').find("[name=avatar]")).imageInput(this.loadAva);
        $('[name="image"], [name="no_image"], [name="logo"], [name="image_current"], [name="no_image"], [name="bird_image"], [name="img"]').each(function(){
            new FileInput($(this))
                .imageInput(this.loadImg);
        });

        let pb = new ProfileBuilder('no');

        pb.textAlignValue();


        $('.js-edit-elements').on('click', function() {
            $(this).toggleClass('active-edit');
        });
        pb.editElement();

        $('body').find('*[data-tog]').on('click', function () {
            $($(this).attr('data-tog')).toggle(100);
        });

        if ($('#editor').length) {
            // CKEDITOR.replace('editor', {
            //     language: 'ru',
            //     uiColor: '#eeeeee',
            // });

            CKEDITOR.config.font_names = CKEDITOR.config.font_names + '; Arial Black/Arial Black, Helvetica, sans-serif';
            CKEDITOR.replace( 'editor', {
                language: 'ru',
                uiColor: '#eeeeee',
                toolbarGroups: [
                    { name: 'clipboard',   groups: [ 'undo', 'clipboard' ] },
                    { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
                    { name: 'links' },
                    { name: 'insert' },
                    { name: 'forms' },
                    { name: 'tools' },
                    { name: 'document',       groups: [ 'mode', 'document', 'doctools' ] },
                    { name: 'others' },
                    '/',
                    { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                    { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
                    { name: 'styles' },
                    { name: 'colors' },
                    { name: 'about' },
                    { name: 'basicstyles' },
                    { name: 'mode' },
                ],
                on: {
                    pluginsLoaded: function() {
                        var editor = this,
                            config = editor.config;

                        editor.ui.addRichCombo( 'my-combo', {
                            label: 'Маркетинги',
                            title: 'Маркетинги',
                            toolbar: 'styles,4',

                            panel: {
                                css: [ CKEDITOR.skin.getPath( 'editor' ) ].concat( config.contentsCss ),
                                multiSelect: false,
                            },

                            init: function() {
                                let marks = [];
                                $.ajax({
                                    url: `/get-site-info`,
                                    async: false,
                                    dataType: 'json',
                                    success: function (data) {
                                        marks = data.marks;
                                    }
                                });

                                Object.values(marks).forEach((mark) => {
                                this.startGroup( mark['name'] );
                                    this.add( '<a href="javascript:activate(' + mark['id'] + ');" class="btn btn-activate-tarif" style="">' + mark['name'] + '</a>', 'Кнопка активации' );
                                    this.add( '{mark_enter(' + mark['id'] + ')}', 'Цена тарифа' );
                                    this.add( '{mark_profit(' + mark['id'] + ')}', 'Доход тарифа' );
                                    this.add('{mark_next_user(' + mark['id'] + ')}', 'Следующий на очереди');
                                });

                            },

                            onClick: function( value ) {
                                editor.focus();
                                editor.fire( 'saveSnapshot' );

                                editor.insertHtml( value );

                                editor.fire( 'saveSnapshot' );
                            }
                        } );

                        editor.ui.addRichCombo( 'my-combo-2', {
                            label: 'Значения',
                            title: 'Значения',
                            toolbar: 'styles,99',

                            panel: {
                                css: [ CKEDITOR.skin.getPath( 'editor' ) ].concat( config.contentsCss ),
                                multiSelect: false,
                            },

                            init: function() {

                                this.startGroup( 'Информация о пользователе' );
                                this.add('{login}', 'Логин пользователя');
                                this.add('{avatar(высота:ширина)}', 'Аватар пользователя');
                                this.add('{avatar_link}', 'Прямая ссылка на аватар пользователя');
                                this.add('{profit}', 'Заработано пользователем');
                                this.add('{profit_to_inviter}', 'Заработано пользователем(партнерка)');
                                this.add('{link_visit}', 'Количество переходов по ссылке');
                                this.add('{count_ref}', 'Количество рефералов');
                                this.add('{balance}', 'Баланс пользователя');
                                this.add('{count_refs_level(уровень)}', 'Количество рефералов на уровне');
                                this.add('{count_refs_active_level(уровень)}', 'Количество рефералов на уровне');
                                this.add('{skype}', 'Skype');
                                this.add('{telegram}', 'Telegram');
                                this.add('{vk}', 'VK');
                                this.add('{facebook}', 'Facebook');
                                this.add('{twitter}', 'Twitter');
                                this.add('{instagram}', 'Instagram');
                                this.add('{lastname}', 'Фамилия');
                                this.add('{name}', 'Имя');
                                this.add('{fathername}', 'Отчество');
                                this.add('{referral_link}', 'Реферальная ссылка');
                                this.add('{percent_sum_user}', '10% от активаций пользователя');
                                this.add('{text_status}', 'Текст статуса');
                                this.add('{activate_mark_count}', 'Количество активированных тарифов');

                                this.startGroup( 'Информация о кураторе' );
                                this.add('{login_parent}', 'Логин куратора');
                                this.add('{avatar_parent(высота:ширина)}', 'Аватар куратора');
                                this.add('{avatar_parent_link}', 'Прямая ссылка на аватар куратора');
                                this.add('{profit_parent}', 'Заработано пользователем');
                                this.add('{profit_to_inviter_parent}', 'Заработано пользователем(партнерка)');
                                this.add('{skype_parent}', 'Skype');
                                this.add('{telegram_parent}', 'Telegram');
                                this.add('{vk_parent}', 'VK');
                                this.add('{facebook_parent}', 'Facebook');
                                this.add('{twitter_parent}', 'Twitter');
                                this.add('{instagram_parent}', 'Instagram');
                                this.add('{lastname_parent}', 'Фамилия');
                                this.add('{name_parent}', 'Имя');
                                this.add('{fathername_parent}', 'Отчество');
                                this.add('{text_status_parent}', 'Текст статуса куратора');
                                this.add('{activate_mark_count_parent}', 'Количество активированных тарифов');

                                this.startGroup( 'Информация о сайте' );
                                this.add('{last_user_login}', 'Логин последнего зарегистрированного пользователь');
                                this.add('{user_count}', 'Количество пользователей');
                                this.add('{user_activate_count}', 'Количество активированных');
                                this.add('{all_cash}', 'Оборот проекта');


                            },

                            onClick: function( value ) {
                                editor.focus();
                                editor.fire( 'saveSnapshot' );

                                editor.insertHtml( value );

                                editor.fire( 'saveSnapshot' );
                            }
                        } );
                    }
                }
            } );

            // $('body').find('#cke_83').append('<span>wqe</span>');

        }

        if ($('.js-tree-tarif').length) {
            new TreeHorizontalDrag('.js-tree-tarif');
        }


        // if ($('#chart-payments').length) {
        //     new AdminChartPayments();
        // }
        //
        // if ($('#chart-activation').length) {
        //     new AdminChartActivation();
        // }
        //
        // if ($('#chart-visit').length) {
        //     new AdminChartVisit();
        // }

        if (this.data.marks == 'yes') {
            new AdminMarksCustom();
        }

        // new iAuthProtection();
        $('.iauth-confirm').closest('form').find('input').on('focus', function () {
            $(this).closest('form').find('.iauth-confirm').show(100);
        });


        let botSettings = new BotSettings(true);
        botSettings.treeInit();

        $(this.container).remove('.overlay');
    }
}