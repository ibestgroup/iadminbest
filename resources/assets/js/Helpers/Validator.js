import validate from 'validate.js';
import _ from 'lodash';


export default class Validator {
    constructor(form, constraints) {
        this.form = form;
        this.constraints = constraints;
    }

    validate() {
        let errors = validate(this.form, this.constraints);
        this.showErrors(errors || {});

        return !errors;
    }

    showErrors(errors) {
        _.each(this.form.querySelectorAll("input[name], select[name], textarea[name]"), (input) => {
            this.showErrorsForInput(input, errors && errors[input.name]);
        })
    }
    showErrorsForInput(input, errors) {
        // console.log(errors);
        let formGroup = this.closestParent(input.parentNode, "form-group"),
            params = {};

        if (this.constraints.hasOwnProperty(input.getAttribute('name'))) {
            params = this.constraints[input.getAttribute('name')];
        }

        if (formGroup)
            this.resetFormGroup(formGroup);

        if (errors) {
            formGroup.classList.add("has-error");
            _.each(errors, (error) => {
                this.addError(formGroup, error);
            });
        } else if (params.presence) {
            formGroup.classList.add("has-success");
        }
    }

    resetFormGroup(formGroup) {
        formGroup.classList.remove("has-error");
        formGroup.classList.remove("has-success");
        _.each(formGroup.querySelectorAll(".help-block"), function(el) {
            el.remove();
        });
    }

    closestParent(child, className) {
        if (!child || child === document) {
            return null;
        }
        if (child.classList.contains(className)) {
            return child;
        } else {
            return this.closestParent(child.parentNode, className);
        }
    }

    addError(messages, error) {
        var block = document.createElement("span");
        block.classList.add("help-block");
        block.innerText = error;
        messages.appendChild(block);
    }
}
