/**
 * Cash out component
 */
import Validator from '../Helpers/Validator';
import axios from "axios";
import _ from "lodash";
import Notice from "../Helpers/Notice";

export default class CashOut {
    constructor() {

        this.form = document.querySelectorAll('.js-cashOut')[0];
        this.sumInp = $(this.form).find('[name=sum]');
        this.addressInp = $(this.form).find('[name=address]');
        this.selector = $(this.form).find('[name="system"]');
        this.system = this.selector.find(':selected').data('param');

        $(document).ready(function () {
            $('body').on('focus', '.js-cashOut *', function () {
                $(this).closest('.js-cashOut').find('.iauth-protected').show(100);
            });
        });

        this.__initComponent();
    }

    __initComponent() {
        // this.draw();
        this.selector.change((el) => {
            this.system = this.selector.find(':selected').data('param');
            // this.draw();
            this.validator();
        });

        this.sumInp.keyup((el) => {
            this.validator();
            // this.draw();
        });

        $(this.form).submit((event) => {
            console.log(123);
            event.preventDefault();

            if (this.validator()) {

                axios.post(this.form.getAttribute('action'), new FormData(this.form))
                    .then((response) => {
                        $(this.form).find('.overlay').remove();
                        $(this.form).find('.alert').remove();

                        let typeNotice = "success";
                        if (response.data.type) {
                            typeNotice = response.data.type;
                        }

                        // new Notice(typeNotice, response.data.message);
                        $(this.form).find('.box-body').prepend(`<p class="alert alert-${typeNotice}">${response.data.message}</p>`)

                    }).catch((error) => {
                        let errors = error.response.data.errors;

                        $(this.form).find('.alert').remove();

                        _.each(errors, (error) => {
                            $(this.form).find('.box-body').prepend(`<p class="alert alert-danger">${error}</p>`)
                        });

                        $(this.form).find('.overlay').remove();
                    });

                $(this.form).find('.box').append(
                    `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
                );
            }
        })
    }

    get sum() {
        return Number(this.sumInp.val());
    }

    get commission() {
        return Number((((this.sum * this.system.commission) / 100) + this.system.custom_comission).toFixed(2));
    }

    customComission() {
        if (this.system.custom_comission > 0) {
            return ' + ' + this.system.custom_comission;
        } else {
            return '';
        }
    }

    get min() {
        return Number(this.system.min);
    }

    get example() {
        return this.system.example;
    }

    calculate() {
        return this.sum + this.commission;
    }


    // this.customComission = (this.system.custom_comission > 0)?' + ' + this.system.custom_comission:'';

    draw() {
        this.addressInp.attr('placeholder', this.example);
        this.result.find('span').text(this.system.commission + '%' + this.customComission());
        this.result.find('.js-cashOut-commission-show').text(this.commission);
        this.result.find('.js-cashOut-result-show').text(this.calculate());
        this.result.find('.js-cashOut-min-show').text(this.min);
    }

    validator() {
        return new Validator(this.form, {
            sum: {
                presence: {message: "^Укажите сумму для вывода"},
                numericality: {
                    // onlyInteger: true,
                    greaterThanOrEqualTo: this.min,
                    message: "^Сумма снятия должна быть больше " + this.min
                }
            },
            address: {
                presence: {message: "^Укажите кошелек для вывода"}
            }
        }).validate();
    }
}