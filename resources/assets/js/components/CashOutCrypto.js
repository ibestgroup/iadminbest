/**
 * Cash out component
 */
import Validator from '../Helpers/Validator';
import axios from "axios";
import _ from "lodash";

export default class CashOutCrypto {
    constructor(root) {
        this.root = root;
        this.form = root.find('form').get(0);
        this.sumInp = root.find('[name=sum]');
        this.addressInp = root.find('[name=address]');
        this.result = root.find('.js-cashOut-result');
        this.selector = root.find('[name="system"]');
        this.system = this.selector.find(':selected').data('param');

        this.__initComponent();
    }

    __initComponent() {
        // this.draw();
        // this.selector.change((el) => {
        //     this.system = this.selector.find(':selected').data('param');
        //     this.draw();
        //     this.validator();
        // });

        // this.sumInp.keyup((el) => {
        //     this.validator();
        //     this.draw();
        // });

        $(this.form).submit((event) => {
            event.preventDefault();

            if (this.validator()) {
                axios.post(this.form.getAttribute('action'), new FormData(this.form))
                    .then((response) => {
                        this.root.find('.overlay').remove();
                        $(this.form).find('.alert').remove();
                        $(this.form).find('.box-body').prepend(`<p class="alert alert-success">${response.data.message}</p>`)

                    }).catch((error) => {
                        let errors = error.response.data.errors;

                        $(this.form).find('.alert').remove();

                        _.each(errors, (error) => {
                            $(this.form).find('.box-body').prepend(`<p class="alert alert-danger">${error}</p>`)
                        });

                        this.root.find('.overlay').remove();
                    });

                this.root.find('.box').append(
                    `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
                );
            }
        })
    }

    get sum() {
        return this.sumInp.val();
    }

    // get commission() {
    //     return ((this.sum * this.system.commission) / 100).toFixed(2);
    // }

    // get min() {
    //     return this.system.min;
    // }

    // calculate() {
    //     return this.sum - this.commission;
    // }

    // draw() {
        // this.addressInp.attr('placeholder', this.example);
        // this.result.find('span').text(this.system.commission);
        // this.result.find('.js-cashOut-commission-show').text(this.commission);
        // this.result.find('.js-cashOut-result-show').text(this.calculate());
        // this.result.find('.js-cashOut-min-show').text(this.min);
    // }

    validator() {
        return new Validator(this.form, {
            sum: {
                presence: {message: "^Укажите сумму для вывода"},
                numericality: {
                    greaterThanOrEqualTo: '1',
                    message: "^Сумма снятия должна быть больше 1"
                }
            },
            address: {
                presence: {message: "^Укажите кошелек для вывода"}
            }
        }).validate();
    }
}