import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';
import Validator from "../Helpers/Validator";


export default class CashoutForBuy {

    constructor () {
        this.form = document.querySelector('.js-cashoutForBuy');

        this.submit();
    }

    validator() {
        return new Validator(this.form, {
            name: {
                presence: {message: "^Вопрос не может быть пустым"},
                length: {
                    minimum: 3,
                    tooShort: "^Минимальная длина %{count} символов"
                }
            }
        }).validate();
    }

    submit() {
        $(document).on('submit', '.js-cashoutForBuy', (e) => {

            e.preventDefault();

            console.log(e);

            // if (this.validator()) {

                axios.post(e.target.action, new FormData(e.target))
                    .then((response) => {

                        $('.box').find('.overlay').remove();

                        new Notice("success", response.data.message);
                        new Refresh(location.pathname, '.box-body', '.box-body');

                    }).catch((error) => {
                    let errors = error.response.data.errors;
                    console.log(error.response.data);
                    _.each(errors, (error) => {

                        new Notice("error", error);
                        $(this.form).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)
                    });

                    $('.box').find('.overlay').remove();
                });

                $(e.target).closest('.box').append(
                    `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
                );
            // }
        });
    }

}