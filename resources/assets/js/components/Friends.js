import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';
import AdEdit from "../forms/AdEdit";
import datepicker from "bootstrap-datepicker";
import Validator from "../Helpers/Validator";
import MasonryLayout from 'masonry-layout';


export default class Friends {

    constructor () {
        this.form = document.querySelector('.js-friends');
        this.submit();
    }



    submit() {
        $(document).on('submit', '.js-friends', (e) => {

            e.preventDefault();
            this.form = document.querySelector('.js-friends[data-id="'+e.target.attributes['data-id'].nodeValue+'"]');
            console.log(this.form);
            axios.post(this.form.getAttribute('action'), new FormData(this.form))
                .then((response) => {

                    $('.box').find('.overlay').remove();

                    new Notice("success", response.data.success);
                    new Refresh(location.href, '.refresh', '.refresh');

                }).catch((error) => {
                let errors = error.response.data.errors;
                console.log(error.response.data);

                _.each(errors, (error) => {
                    $(this.form).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)
                });

                $('.box').find('.overlay').remove();
            });

        });
    }

}