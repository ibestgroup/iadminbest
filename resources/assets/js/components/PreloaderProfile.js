import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';
import AdEdit from "../forms/AdEdit";
import datepicker from "bootstrap-datepicker";
import Validator from "../Helpers/Validator";
import MasonryLayout from 'masonry-layout';
// import imagesLoaded from 'imagesloaded';


export default class PreloaderProfile {

    constructor () {

        var imagesLoaded = require('imagesloaded');
        var jQuery = require('jquery');

// provide jQuery argument
        imagesLoaded.makeJQueryPlugin( $ );

        $(function(){

            var $container = $('.mason-gallery');
            var width = $(window).width();
            var height = $(window).height();

            if ((width > 1024  )) {

                $container.imagesLoaded( function(){

                    $('#widgets').show();
                    $('#loading').hide();

                    new MasonryLayout( $container, {
                        // columnWidth: 200,
                        itemSelector: '.col-xs-6',

                    });
                    // $container.masonry({
                    //     itemSelector : '.col-xs-6',
                    //     // columnWidth: 120,
                    // });
                });

            }
            else {
                //load the css styles for screen res below 1024
            }

        });
    }

}