import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';
import AdEdit from "../forms/AdEdit";
import datepicker from "bootstrap-datepicker";
import Validator from "../Helpers/Validator";
// import pincodeInput from "bootstrap-pincode-input";
import "bootstrap-pincode-input";
import "jquery-pinlogin";


export default class TelegramConfirm {

    constructor () {

        $(document).ready(function () {

            $('input[name=iauth_code]').pincodeInput({inputs:6, placeholders: "0 0 0 0 0 0"});

            // $('.iauth-pinlogin').pinlogin({fields:6, hideinput:false, reset: false, placeholder: '0', complete: function(pin) {
            //         $('input[name=iauth_code]').val(pin);
            //     }
            // });

            $('body').on('click', '.iauth-checkbox', function() {
                let el = $(this).find('.iauth-checkbox-icon');
                el.toggleClass('checked');
                if (el.hasClass('checked')) {
                    $(this).find('input').val(1);
                } else {
                    $(this).find('input').val(0);
                }
            });

            $('.iauth-protected').closest('form').find('input').on('focus', function () {
                let userInfo;
                $.ajax({
                    type: `GET`,
                    url: `/get-user-info`,
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        userInfo = data;
                    }
                });
                $(this).closest('form').find('.iauth-protected').show(100);
            });


            $('body').find('input[name=telegram]').attr({'pattern': '@[A-Za-z0-9]{3,}', 'title': 'Должно начинаться с символа @'});

            $('body').on('click', '.teleg-close', function() {
                $(this).closest('.telegram-confirm-message').hide(100);
            });

            let userInfo, btn;


            $('body').on('focus', '.js-telegram-confirm input', function (e) {


                $.ajax({
                    type: `GET`,
                    url: `/get-user-info`,
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        userInfo = data;
                    }
                });


                let socType = '', socTypeFull = '', status = '', loginRes = '', textMess = '';
                if ($(this).attr('name') == 'vk_iauth') {
                    socType = 'vk'; socTypeFull = 'vk';
                    status = userInfo.vk_status;
                    loginRes = userInfo.vk_id;
                    textMess = 'ВКонтакте';
                } else if ($(this).attr('name') == 'telegram_iauth') {
                    socType = 'teleg'; socTypeFull = 'telegram';
                    status = userInfo.teleg_status;
                    loginRes = userInfo.teleg_login;
                    textMess = 'телеграм';
                }

                if (status) {
                    btn = `
                        У вас уже есть привязанный аккаунт <span class="iauth-color" style="font-weight: 500;">${loginRes}</span>
                        <div class="js-${socType}-unlink iauth-btn iauth-get-code mt-10" data-toggle="modal" data-target="#${socType}-unlink">
                            <span>ОТКЛЮЧИТЬ</span>
                        </div>
                    `;
                } else {
                    btn = `
                        Подтвердите ${textMess}, чтобы защитить аккаунт
                        <div class="js-${socType}-getcode iauth-btn iauth-get-code mt-10" data-toggle="modal" data-target="#${socType}-confirm">
                            <span>ПОДТВЕРДИТЬ</span>
                        </div>
                    `;
                }

                if (e.type == 'focusin') {
                    let posTop = $(this).parents('.js-telegram-confirm').height() + 'px';
                    let element = $(this).parents('.js-telegram-confirm').find('.telegram-confirm-message');
                    element.css('top', posTop);
                    if (userInfo.teleg_status) {
                        element.show(100);
                    }
                    $(this).parents('.js-telegram-confirm')
                        .find(`.${socTypeFull}-for-content`)
                        .html(btn + `<div class="clearfix"></div>`);
                }

            });


            $('body').on('click', '.js-teleg-getcode', function () {

                $.ajax({
                    type: `GET`,
                    url: `/get-user-info`,
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        userInfo = data;
                    }
                });

                let teleg = $(this).parents('.iauth-protected').find('input[name="telegram_iauth"]').val();

                let errMess = 0;
                if (teleg.length == 0) {
                    new Notice('error', 'Строка не может быть пустой');
                    errMess = 1;
                }

                if (teleg[0] != '@') {
                    new Notice('error', 'Должно начинаться с символа @');
                    errMess = 1;
                }

                if (teleg.length < 3) {
                    new Notice('error', 'Минимальная длина 3 символа');
                    errMess = 1;
                }

                if (errMess) return;

                $.ajax({
                    type: `POST`,
                    url: `/telegram-add`,
                    data: {
                        teleg_login: teleg
                    },
                    dataType: 'json',
                    success: function (data) {
                        // console.log(data);
                    }
                });


                if ($('#teleg-confirm').length) {
                    $('#teleg-confirm').remove();
                }

                $('body').append(`
                    <div class="modal fade" id="teleg-confirm">
                            <div class="modal-dialog">
                                <div class="modal-content iauth-protected">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                        <h3 class="ta-center"><span class="iauth-color">iAuth</span> защита</h3>

                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <div class="col-lg-12">
                                            <h3>Ваш телеграм <span class="teleg-login" style="font-weight: bold"></span></h3>
                                            <p>
                                                Вы можете поставить защиту на свой аккаунт с помощью интегрированного телеграм бота.
                                                Даже если злоумышленники получат доступ к вашему аккаунту, они не смогут вывести ваши средства
                                                если они не получили доступ к вашему Telegram аккаунту. Каждый раз, когда вы захотите вывести
                                                средства со своего аккаунта, вам будет приходить шестизначный код с подтверждением.
                                            </p>
                                            <p>
                                                Для того чтобы подтвердить ваш телеграм и получать сообщения откройте бота в своем телеграм.
                                            </p>
                                            <div class="iauth-btn mt-10" onClick="window.open('https://t.me/iAuth_bot', '_blank');">
                                                <span class="">ОТКРЫТЬ БОТА iAuth</span>
                                            </div>
                                            </div>
                                            <div class="col-lg-12 mt-10">
                                                <form action="/telegram-confirm" method="post" id="telegram-code" class="js-store">
                                                    <input type="hidden" name="teleg" value="${teleg}">
                                                    <input class="form-control" type="hidden" name="iauth_code" placeholder="Введите код">
                                                    <div id="pinlogin-confirm" class="iauth-pinlogin"></div>
                                                    <button class="iauth-btn mt-10">
                                                        <span class="">ПОДКЛЮЧИТЬ</span>
                                                    </button>
                                                </form>
                                            </div>
                                            <div class="clearfix"></div>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="iauth-btn button-close" data-dismiss="modal"><span>ЗАКРЫТЬ</span></button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                        </div>
                `);



                $('input[name=iauth_code]').pincodeInput({inputs:6, placeholders: "0 0 0 0 0 0"});
                // $('.iauth-pinlogin').pinlogin({fields:6, hideinput:false, reset: false, placeholder: '0', complete: function(pin) {
                //         $('input[name=iauth_code]').val(pin);
                //     }
                // });
                $('body').find('.teleg-login').text(teleg);

            });

            $('body').on('click', '.js-vk-getcode', function () {

                $.ajax({
                    type: `GET`,
                    url: `/get-user-info`,
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        userInfo = data;
                    }
                });

                let vk = $(this).parents('.iauth-protected').find('input[name="vk_iauth"]').val();

                let errMess = 0;
                if (vk.length == 0) {
                    new Notice('error', 'Строка не может быть пустой');
                    errMess = 1;
                }

                if (vk.length < 3) {
                    new Notice('error', 'Минимальная длина 3 символа');
                    errMess = 1;
                }

                if (errMess) return;

                $.ajax({
                    type: `POST`,
                    url: `/teleg-add`,
                    data: {
                        vk_login: vk
                    },
                    dataType: 'json',
                    success: function (data) {
                        // console.log(data);
                    }
                });


                if ($('#vk-confirm').length) {
                    $('#vk-confirm').remove();
                }

                $('body').append(`
                    <div class="modal fade" id="vk-confirm">
                            <div class="modal-dialog">
                                <div class="modal-content iauth-protected">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                        <h3 class="ta-center"><span class="iauth-color">iAuth</span> защита</h3>

                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            <div class="col-lg-12">
                                            <h3>Ваш VK <span class="vk-login" style="font-weight: bold"></span></h3>
                                            <p>
                                                Вы можете поставить защиту на свой аккаунт с помощью интегрированного бота ВКонтакте.
                                                Даже если злоумышленники получат доступ к вашему аккаунту, они не смогут вывести ваши средства
                                                если они не получили доступ к вашему аккаунту Вконтакте. Каждый раз, когда вы захотите вывести
                                                средства со своего аккаунта, вам будет приходить шестизначный код с подтверждением.
                                            </p>
                                            <p>
                                                Для того чтобы подтвердить ваш VK и получать сообщения откройте группу iAuth и справа нажмите
                                                "Написать сообщение" и .
                                            </p>
                                            <div class="iauth-btn mt-10" onClick="window.open('https://vk.com/iauthprotection', '_blank');">
                                                <span class="">ОТКРЫТЬ ГРУППУ iAuth</span>
                                            </div>
                                            </div>
                                            <div class="col-lg-12 mt-10">
                                                <form action="/telegram-confirm" method="post" id="telegram-code" class="js-store">
                                                    <input type="hidden" name="vk" value="${vk}">
                                                    <input class="form-control" type="hidden" name="iauth_code" placeholder="Введите код">
                                                    <div id="pinlogin-confirm" class="iauth-pinlogin"></div>
                                                    <button class="iauth-btn mt-10">
                                                        <span class="">ПОДКЛЮЧИТЬ</span>
                                                    </button>
                                                </form>
                                            </div>
                                            <div class="clearfix"></div>
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="iauth-btn button-close" data-dismiss="modal"><span>ЗАКРЫТЬ</span></button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                        </div>
                `);

                $('input[name=iauth_code]').pincodeInput({inputs:6, placeholders: "0 0 0 0 0 0"});
                // $('.iauth-pinlogin').pinlogin({fields:6, hideinput:false, reset: false, placeholder: '0', complete: function(pin) {
                //         $('input[name=iauth_code]').val(pin);
                //     }
                // });
                $('body').find('.vk-login').text(vk);

            });

            $('body').on('click', '.js-teleg-unlink, .js-teleg-notify', function () {

                $.ajax({
                    type: `GET`,
                    url: `/get-user-info`,
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        userInfo = data;
                    }
                });

                let teleg = $(this).parents('.js-telegram-confirm').find('input').val();
                // let userInfo;

                $.ajax({
                    type: `POST`,
                    url: `/telegram-sendcode`,
                    dataType: 'json',
                    data: {
                        site_id: userInfo.site_id,
                        user_id: userInfo.user_id,
                    },
                    success: function (data) {

                    }
                });

                // console.log(userInfo);


                if ($('#teleg-confirm').length) {
                    $('#teleg-confirm').remove();
                }
                if ($('#teleg-unlink').length) {
                    $('#teleg-unlink').remove();
                }

                $('body').append(`
                        <div class="modal fade" id="teleg-unlink">
                            <div class="modal-dialog">
                                <div class="modal-content iauth-protected">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                        <h3 class="ta-center"><span class="iauth-color">iAuth</span> защита</h3>

                                    </div>
                                    <div class="modal-body">
                                        <div class="col-lg-12">
                                            <h3>Ваш телеграм <span class="teleg-login" style="font-weight: bold">${userInfo.teleg_login}</span></h3>
                                            <p>
                                                Вам пришел код на ваш телеграм <b>${userInfo.teleg_login}</b>. Для того чтобы отвязать телеграм, введите код.
                                            </p>
                                            <p>
                                                Если код не пришел автоматически, откройте бота и введите команду <b>/start</b>.
                                            </p>
                                            <!--<div class="ta-center mt-10 mb-10">-->
                                                <!--<a href="https://t.me/iAuth_bot" class="iauth-btn" target="_blank" style="display: inline;">Открыть бота iAuth</a>-->
                                            <!--</div>-->
                                            <div class="iauth-btn iauth-get-code mt-10" onClick="window.open('https://t.me/iAuth_bot', '_blank');">
                                                <span class="">ОТКРЫТЬ БОТА iAuth</span>
                                            </div>
                                            </div>
                                            <div class="col-lg-12 mt-10">
                                                <form action="/telegram-unlink" method="post" id="telegram-code" class="js-store">
                                                    <input type="hidden" name="teleg" value="${userInfo.teleg_login}">
                                                    <input class="form-control" type="hidden" name="iauth_code" placeholder="Введите код">
                                                    <!--<div id="pinlogin-unlink" class="iauth-pinlogin"></div>-->
                                                    <button class="iauth-btn mt-10">
                                                        <span class="">ОТКЛЮЧИТЬ</span>
                                                    </button>
                                                </form>
                                            </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="iauth-btn button-close" data-dismiss="modal"><span>ЗАКРЫТЬ</span></button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                        </div>
                        
                        <div class="modal fade" id="teleg-notify">
                            <div class="modal-dialog">
                                <div class="modal-content iauth-protected">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>
                                        <h3 class="ta-center"><span class="iauth-color">iAuth</span> защита</h3>

                                    </div>
                                    <div class="modal-body">
                                        <div class="col-lg-12">
                                            <h3>Ваш телеграм <span class="teleg-login" style="font-weight: bold">${userInfo.teleg_login}</span></h3>
                                            <p>
                                                Вам пришел код на ваш телеграм <b>${userInfo.teleg_login}</b>. Для того чтобы изменить настройки оповещений.
                                            </p>
                                            <p>
                                                Если код не пришел автоматически, откройте бота и введите команду <b>/start</b>.
                                            </p>
                                            <!--<div class="ta-center mt-10 mb-10">-->
                                                <!--<a href="https://t.me/iAuth_bot" class="iauth-btn" target="_blank" style="display: inline;">Открыть бота iAuth</a>-->
                                            <!--</div>-->
                                            <div class="iauth-btn iauth-get-code mt-10" onClick="window.open('https://t.me/iAuth_bot', '_blank');">
                                                <span class="">ОТКРЫТЬ БОТА iAuth</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 mt-10">
                                            <form action="/telegram-notice" method="post" id="telegram-code" class="js-store">
                                                <input type="hidden" name="teleg" value="${userInfo.teleg_login}">
                                                <input class="form-control" type="hidden" name="iauth_code" placeholder="Введите код">
                                                <!--<div id="pinlogin-unlink" class="iauth-pinlogin"></div>-->
                                                <div style="line-height: 50px; margin-top: 10px; font-size: 18px;">
                                                    <div class="iauth-checkbox" style="float: left;">
                                                        <input type="hidden" name="system_notice" value="${(userInfo.system_notice)?'1':''}">
                                                        <div class="${(userInfo.system_notice)?'checked':''} iauth-checkbox-icon"></div>
                                                    </div>
                                                    <span style="padding-left: 10px;">Системные оповещения</span>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div style="line-height: 50px; margin-top: 10px; font-size: 18px;">
                                                    <div class="iauth-checkbox" style="float: left;">
                                                        <input type="hidden" name="notice" value="${(userInfo.notice)?'1':''}">
                                                        <div class="${(userInfo.notice)?'checked':''} iauth-checkbox-icon"></div>
                                                    </div>
                                                    <span style="padding-left: 10px;">Новости</span>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <button class="iauth-btn mt-10">
                                                    <span class="">СОХРАНИТЬ</span>
                                                </button>
                                            </form>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="iauth-btn button-close" data-dismiss="modal"><span>ЗАКРЫТЬ</span></button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                        </div>
                `);
                $('input[name=iauth_code]').pincodeInput({inputs:6, placeholders: "0 0 0 0 0 0"});
                // $('.iauth-pinlogin').pinlogin({fields:6, hideinput:false, reset: false, placeholder: '0', complete: function(pin) {
                //         $('input[name=iauth_code]').val(pin);
                //     }
                // });
            });

            $('body').on('click', '.iauth-get-code:not([data-toggle=modal])', function () {
                let userInfo;

                $.ajax({
                    type: `GET`,
                    url: `/get-user-info`,
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        userInfo = data;
                    }
                });


                $.ajax({
                    type: `POST`,
                    url: `/telegram-sendcode`,
                    dataType: 'json',
                    data: {
                        site_id: userInfo.site_id,
                        user_id: userInfo.user_id,
                    },
                });
                $(this).text('КОД ОТПРАВЛЕН').append('<img src="https://i.ibb.co/tDN2Hfd/checked-gif.gif" style="width: 50px; height: 50px;">');

            });

        });
    }

}