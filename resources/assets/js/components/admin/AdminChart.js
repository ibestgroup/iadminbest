import Notice from '../../Helpers/Notice';
import axios from "axios";
import Refresh from '../../Helpers/Refresher';
// import Chart from '../../../../../node_modules/chart.js/src/chart';
import Chart from 'chart.js'
import moment from 'moment';
import daterangepicker from 'daterangepicker';

export class AdminChartPayments {

    constructor () {

        let after = moment(new Date()).add(-7, 'days').format('YYYY-MM-DD'),
            before = moment(new Date()).format('YYYY-MM-DD');

        let ctx = $('#chart-payments');

        let config = this.chartBuild(after, before);

        window.chart = new Chart(ctx, config);

        $('#drp-payments').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment()
        });
        $('#drp-payments').on('apply.daterangepicker', (ev, picker) => {


            window.chart.destroy();

            let after = picker.startDate.format('YYYY-MM-DD'),
                before = picker.endDate.format('YYYY-MM-DD');

            config = this.chartBuild(after, before);
            window.chart = new Chart(ctx, config);
        });
    }

    chartBuild (after, before) {
        let ctx = $('#chart-payments'),
            labels = [],
            amounts = [],
            count = [];


        $.ajax({
            url: `/profile/chart/payments?after=${after}&before=${before}`,
            async: false,
            dataType: 'json',
            success: function (data) {
                labels = data.labels;

                for (let i = 0; i < labels.length; i++) {
                    let dataLength = Object.values(data[i]).length - 1;

                    amounts[i] = 0;
                    if (labels[i] == data[i]['label']) {
                        if (dataLength) {
                            for (let j = 0; j < dataLength; j++) {
                                amounts[i] += Number(data[i][j].amount);
                            }
                            count[i] = dataLength;
                        } else {
                            amounts[i] = 0;
                            count[i] = 0;
                        }
                    }

                }

                if (labels.length === 0) {
                    // new Notice('info', 'Не найдено ни одной записи');
                }
            }
        });

        let amountData = {
            label: "Сумма",
            yAxisID: 'amount',
            data: amounts,
            borderColor: [
                'rgba(0,166,90,1)'
            ],
            backgroundColor: [
                'rgba(0,166,90,0.35)'
            ],
            borderWidth: 1,
            pointHoverRadius: 1,
            pointRadius: 0,
            pointHoverBackgroundColor: "rgba(0,166,90,1)",
            pointHoverBorderColor: "rgba(0,166,90,1)",
            pointHoverBorderWidth: 0,
        },
        countData = {
            label: "Количество",
            yAxisID: 'count',
            data: count,
            borderColor: [
                'rgba(85,82,153,1)'
            ],
            backgroundColor: [
                'rgba(85,82,153,0.5)'
            ],
            borderWidth: 1,
            pointHoverRadius: 0,
            pointRadius: 0,
            pointHoverBackgroundColor: "rgba(85,82,153,1)",
            pointHoverBorderColor: "rgba(85,82,153,1)",
            pointHoverBorderWidth: 0,
        },
        yAmount = {
            id: 'amount',
            type: 'linear',
            position: 'left',
            gridLines: {
                // display: false,
                color: "#ededed"
            },
            ticks: {
                fontColor: "#9f9f9f",
                beginAtZero: true,
            }
        },
        yCount = {
            id: 'count',
            type: 'linear',
            position: 'right',
            gridLines: {
                display: false,
                color: "#ededed"
            },
            ticks: {
                fontColor: "#9f9f9f",
                beginAtZero: true,
            }
        };


        let configure = {
            type: 'line',
            data: {
                labels: labels,
                datasets: [amountData,countData],
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: true,
                        ticks: {
                            fontColor: "#9f9f9f",
                        },
                    }],
                    yAxes: [yAmount, yCount]
                },
                tooltips: {
                    mode: 'x-axis'
                }
            }
        };
        return configure;

    }

}

export class AdminChartActivation {

    constructor () {
        let after = moment(new Date()).add(-7, 'days').format('YYYY-MM-DD'),
            before = moment(new Date()).format('YYYY-MM-DD');

        let ctx = $('#chart-activation');

        let config = this.chartBuild(after, before);
        window.chart1 = new Chart(ctx, config);

        $('#drp-activation').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment()
        });
        $('#drp-activation').on('apply.daterangepicker', (ev, picker) => {

            if (window.chart1 != undefined) {
                window.chart1.destroy();
            }

            let after = picker.startDate.format('YYYY-MM-DD'),
                before = picker.endDate.format('YYYY-MM-DD');

            config = this.chartBuild(after, before);
            window.chart1 = new Chart(ctx, config);
        });
    }

    chartBuild (after, before) {
        let ctx = $('#chart-activation'),
            labels = [],
            amounts = [],
            count = [],
            counts = [],
            stepSize;

        $.ajax({
            url: `/profile/chart/activation?after=${after}&before=${before}`,
            async: false,
            dataType: 'json',
            success: function (data) {

                labels = data.labels;
                counts = data.counts;

                for (let i = 0; i < labels.length; i++) {
                    let dataLength = Object.values(data[i]).length - 1;

                    amounts[i] = 0;
                    count[i] = 0;
                    if (labels[i] == data[i]['label']){
                        if (dataLength) {
                            for (let j = 0; j < dataLength; j++) {
                                amounts[i] += Number(Number(data[i][j].amount));
                            }
                        }
                    }
                    amounts[i] = Math.round(amounts[i]);
                }

                if (labels.length === 0) {
                    // new Notice('info', 'Не найдено ни одной записи');
                }
            }
        });

        let amountData = {
                label: "Сумма",
                yAxisID: 'amount',
                data: amounts,
                borderColor: [
                    '#ff851b'
                ],
                backgroundColor: [
                    '#ff851b70'
                ],
                borderWidth: 1,
                pointHoverRadius: 0,
                pointRadius: 0,
                pointHoverBackgroundColor: "#ff851b",
                pointHoverBorderColor: "#ff851b",
                pointHoverBorderWidth: 0,
            },
            countData = {
                label: "Количество",
                yAxisID: 'count',
                data: counts,
                borderColor: [
                    '#00a7d0'
                ],
                backgroundColor: [
                    '#00a7d070'
                ],
                borderWidth: 1,
                pointHoverRadius: 0,
                pointRadius: 0,
                pointHoverBackgroundColor: "#00a7d0",
                pointHoverBorderColor: "#00a7d0",
                pointHoverBorderWidth: 0,
            },
            yAmount = {
                id: 'amount',
                type: 'linear',
                position: 'left',
                gridLines: {
                    // display: false,
                    color: "#ededed"
                },
                ticks: {
                    fontColor: "#9f9f9f",
                    beginAtZero: true,
                }
            },
            yCount = {
                id: 'count',
                type: 'linear',
                position: 'right',
                gridLines: {
                    display: false,
                    color: "#ededed"
                },
                ticks: {
                    fontColor: "#9f9f9f",
                    beginAtZero: true,
                }
            };


        let configure = {
            type: 'line',
            data: {
                labels: labels,
                datasets: [amountData,countData],
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            color: "#ededed"
                        },
                        ticks: {
                            fontColor: "#9f9f9f",
                        }
                    }],
                    yAxes: [yAmount, yCount]
                },
                tooltips: {
                    mode: 'x-axis'
                }
            }
        };

        return configure;

    }

}

export class AdminChartVisit {

    constructor () {

        let after = moment(new Date()).add(-7, 'days').format('YYYY-MM-DD'),
            before = moment(new Date()).format('YYYY-MM-DD');

        let ctx = $('#chart-visit');

        let config = this.chartBuild(after, before);
        window.chart2 = new Chart(ctx, config);

        $('#drp-visit').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment()
        });
        $('#drp-visit').on('apply.daterangepicker', (ev, picker) => {

            if (window.chart2 != undefined) {
                window.chart2.destroy();
            }

            let after = picker.startDate.format('YYYY-MM-DD'),
                before = picker.endDate.format('YYYY-MM-DD');
            config = this.chartBuild(after, before);
            window.chart2 = new Chart(ctx, config);
        });
    }

    chartBuild (after, before) {
        let ctx = $('#chart-visit'),
            labels = [],
            visit = [],
            visitProfile = [];

        $.ajax({
            url: `/profile/chart/visit?after=${after}&before=${before}`,
            async: false,
            dataType: 'json',
            success: function (data) {
                for (let i = 0; i < data.length; i++) {
                    visit[i] = data[i].visit;
                    visitProfile[i] = data[i].visit_prof;
                    labels[i] = moment(data[i].day).format('YYYY-MM-DD');
                }

                if (labels.length === 0) {
                    // new Notice('info', 'Не найдено ни одной записи');
                }
            }
        });

        let visitData = {
                label: "Посетителей",
                data: visit,
                borderColor: [
                    'rgba(255,255,255,1)'
                ],
                backgroundColor: [
                    'rgba(255,255,255,0.5)'
                ],
                borderWidth: 3,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
            },
            visitProfileData = {
                label: "Пользователей",
                data: visitProfile,
                borderColor: [
                    'rgba(255,255,255,1)'
                ],
                backgroundColor: [
                    'rgba(255,255,255,0.5)'
                ],
                borderWidth: 3,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
            },
            yVisit = {
                type: 'linear',
                position: 'left',
                gridLines: {
                    color: "#FFFFFF"
                },
                ticks: {
                    fontColor: "#9f9f9f",
                }
            };


        let configure = {
            type: 'line',
            data: {
                labels: labels,
                datasets: [visitData,visitProfileData],
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            color: "#FFFFFF"
                        },
                        ticks: {
                            fontColor: "#9f9f9f",
                        }
                    }],
                    yAxes: [yVisit]
                }
            }
        };

        return configure;

    }

}

export class AdminChartRegister {

    constructor () {

        let after = moment(new Date()).add(-7, 'days').format('YYYY-MM-DD'),
            before = moment(new Date()).format('YYYY-MM-DD');
        let ctx = $('#chart-register');

        let config = this.chartBuild(after, before);
        window.chart3 = new Chart(ctx, config);

        $('#drp-register').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment()
        });
        $('#drp-register').on('apply.daterangepicker', (ev, picker) => {

            if (window.chart3 != undefined) {
                window.chart3.destroy();
            }

            let after = picker.startDate.format('YYYY-MM-DD'),
                before = picker.endDate.format('YYYY-MM-DD');
            config = this.chartBuild(after, before);
            window.chart3 = new Chart(ctx, config);
        });
    }

    chartBuild (after, before) {
        let ctx = $('#chart-register'),
            labels = [],
            count = [],
            counts = [];

        $.ajax({
            url: `/profile/chart/register?after=${after}&before=${before}`,
            async: false,
            dataType: 'json',
            success: function (data) {

                // console.log(data);
                labels = data.labels;
                counts = data.counts;

                // for (let i = 0; i < labels.length; i++) {
                //     let dataLength = Object.values(data[i]).length - 1;
                //
                //
                // }

                if (labels.length === 0) {
                    // new Notice('info', 'Не найдено ни одной записи');
                }





                // for (let i = 0; i < data.length; i++) {
                //     labels[i] = moment(data[i].regdate).format('YYYY-MM-DD');
                // }
                //
                // labels = $.unique(labels);
                //
                // for (let i = 0; i < labels.length; i++) {
                //     for (let a = 0; a < data.length; a++) {
                //         if (labels[i] == moment(data[a].regdate).format('YYYY-MM-DD')){
                //             if(isNaN(count[i])) count[i] = 0;
                //             count[i]++;
                //         }
                //     }
                //}

                if (labels.length === 0) {
                    // new Notice('info', 'Не найдено ни одной записи');
                }
            }
        });

        let countData = {
                label: "Регистраций",
                data: counts,
                borderColor: [
                    '#31bbbb'
                ],
                backgroundColor: [
                    '#31bbbb70'
                ],
                borderWidth: 1,
                pointHoverRadius: 0,
                pointRadius: 0,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 0,
            },
            yCount = {
                type: 'linear',
                position: 'left',
                gridLines: {
                    color: "#ededed"
                },
                ticks: {
                    fontColor: "#9f9f9f",
                    stepSize: 5,
                    min: 0,
                    suggestedMax: 25,
                }
            };


        let configure = {
            type: 'line',
            data: {
                labels: labels,
                datasets: [countData],
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            color: "#ededed"
                        },
                        ticks: {
                            fontColor: "#9f9f9f",
                        }
                    }],
                    yAxes: [yCount]
                },
                tooltips: {
                    mode: 'x-axis'
                }
            }
        };

        return configure;

    }

}