import Notice from '../../Helpers/Notice';
import axios from "axios";
import Refresh from '../../Helpers/Refresher';
// import Chart from '../../../../../node_modules/chart.js/src/chart';
import Chart from 'chart.js';
import moment from 'moment';
import daterangepicker from 'daterangepicker';

export class AdminChartPayments {

    constructor () {

        let chart,
            after = moment(new Date()).add(-7, 'days').format('YYYY-MM-DD'),
            before = moment(new Date()).format('YYYY-MM-DD');

        this.chartBuild(after, before);

        $('#drp-payments').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment()
        });
        $('#drp-payments').on('apply.daterangepicker', (ev, picker) => {
            let after = picker.startDate.format('YYYY-MM-DD'),
                before = picker.endDate.format('YYYY-MM-DD'),
                update = true;

            this.chartBuild(after, before);
            chart.update();
        });
    }

    chartBuild (after, before) {
        let ctx = $('#chart-payments'),
            labels = [],
            amounts = [],
            count = [];

        $.ajax({
            url: `/profile/chart/payments?after=${after}&before=${before}`,
            async: false,
            dataType: 'json',
            success: function (data) {
                for (let i = 0; i < data.length; i++) {
                    data[i].date = moment(data[i].date).format('YYYY-MM-DD');
                    labels[i] = data[i].date;
                }

                labels = $.unique(labels);

                for (let i = 0; i < labels.length; i++) {
                    for (let a = 0; a < data.length; a++) {

                        if (labels[i] == data[a].date){
                            if (amounts[i] == null || amounts[i] == undefined || amounts[i] == '' ) {
                                amounts[i] = 0;
                            }
                            amounts[i] += data[a].amount;
                            if(isNaN(count[i])) count[i] = 0;
                            count[i]++;
                        }
                    }
                }

                if (labels.length === 0) {
                    // new Notice('info', 'Не найдено ни одной записи');
                }
            }
        });

        let amountData = {
            label: "Сумма",
            yAxisID: 'amount',
            data: amounts,
            borderColor: [
                'rgba(255,255,255,1)'
            ],
            backgroundColor: [
                'rgba(255,255,255,0.5)'
            ],
            borderWidth: 3,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
        },
        countData = {
            label: "Количество",
            yAxisID: 'count',
            data: count,
            borderColor: [
                'rgba(255,255,255,1)'
            ],
            backgroundColor: [
                'rgba(255,255,255,0.5)'
            ],
            borderWidth: 3,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
        },
        yAmount = {
            id: 'amount',
            type: 'linear',
            position: 'left',
            gridLines: {
                // display: false,
                color: "#FFFFFF"
            },
            ticks: {
                fontColor: "white",
            }
        },
        yCount = {
            id: 'count',
            type: 'linear',
            position: 'right',
            gridLines: {
                display: false,
                color: "#FFFFFF"
            },
            ticks: {
                fontColor: "white",
            }
        };


        let chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [amountData,countData],
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            color: "#FFFFFF"
                        },
                        ticks: {
                            fontColor: "white",
                        }
                    }],
                    yAxes: [yAmount, yCount]
                }
            }
        });

    }

}

export class AdminChartActivation {

    constructor () {

        let chart,
            after = moment(new Date()).add(-7, 'days').format('YYYY-MM-DD'),
            before = moment(new Date()).format('YYYY-MM-DD');

        this.chartBuild(after, before);

        $('#drp-activation').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment()
        });
        $('#drp-activation').on('apply.daterangepicker', (ev, picker) => {
            let after = picker.startDate.format('YYYY-MM-DD'),
                before = picker.endDate.format('YYYY-MM-DD');
            this.chartBuild(after, before);
            chart.update();
        });
    }

    chartBuild (after, before) {
        let ctx = $('#chart-activation'),
            labels = [],
            amounts = [],
            count = [],
            stepSize;

        $.ajax({
            url: `/profile/chart/activation?after=${after}&before=${before}`,
            async: false,
            dataType: 'json',
            success: function (data) {
                for (let i = 0; i < data.amount.length; i++) {
                    data.amount[i].date = moment(data.amount[i].date).format('YYYY-MM-DD');
                    labels[i] = data.amount[i].date;
                }


                labels = $.unique(labels);
                let stepSize = data.step;
                for (let i = 0; i < labels.length; i++) {
                    for (let a = 0; a < data.amount.length; a++) {

                        if (labels[i] == data.amount[a].date){
                            if (amounts[i] == null || amounts[i] == undefined || amounts[i] == '' ) {
                                amounts[i] = 0;
                            }
                            amounts[i] += data.amount[a].amount;

                        }
                    }
                    for (let a = 0; a < data.count.length; a++) {
                        if (labels[i] == moment(data.count[a].date).format('YYYY-MM-DD')){
                            if(isNaN(count[i])) count[i] = 0;
                            count[i]++;
                        }
                    }

                }

                if (labels.length === 0) {
                    // new Notice('info', 'Не найдено ни одной записи');
                }
            }
        });

        let amountData = {
                label: "Сумма",
                yAxisID: 'amount',
                data: amounts,
                borderColor: [
                    'rgba(255,255,255,1)'
                ],
                backgroundColor: [
                    'rgba(255,255,255,0.5)'
                ],
                borderWidth: 3,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
            },
            countData = {
                label: "Количество",
                yAxisID: 'count',
                data: count,
                borderColor: [
                    'rgba(255,255,255,1)'
                ],
                backgroundColor: [
                    'rgba(255,255,255,0.5)'
                ],
                borderWidth: 3,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
            },
            yAmount = {
                id: 'amount',
                type: 'linear',
                position: 'left',
                gridLines: {
                    // display: false,
                    color: "#FFFFFF"
                },
                ticks: {
                    fontColor: "white",
                    stepSize: stepSize,
                    min: 0,
                }
            },
            yCount = {
                id: 'count',
                type: 'linear',
                position: 'right',
                gridLines: {
                    display: false,
                    color: "#FFFFFF"
                },
                ticks: {
                    fontColor: "white",
                    // stepSize: stepSize,
                    min: 0,
                }
            };


        let chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [amountData, countData],
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            color: "#FFFFFF"
                        },
                        ticks: {
                            fontColor: "white",
                        }
                    }],
                    yAxes: [yAmount, yCount]
                }
            }
        });

    }

}

export class AdminChartVisit {

    constructor () {

        let chart,
            after = moment(new Date()).add(-7, 'days').format('YYYY-MM-DD'),
            before = moment(new Date()).format('YYYY-MM-DD');

        this.chartBuild(after, before);

        $('#drp-visit').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment()
        });
        $('#drp-visit').on('apply.daterangepicker', (ev, picker) => {
            let after = picker.startDate.format('YYYY-MM-DD'),
                before = picker.endDate.format('YYYY-MM-DD');
            this.chartBuild(after, before);
            chart.update();
        });
    }

    chartBuild (after, before) {
        let ctx = $('#chart-visit'),
            labels = [],
            visit = [],
            visitProfile = [];

        $.ajax({
            url: `/profile/chart/visit?after=${after}&before=${before}`,
            async: false,
            dataType: 'json',
            success: function (data) {
                for (let i = 0; i < data.length; i++) {
                    visit[i] = data[i].visit;
                    visitProfile[i] = data[i].visit_prof;
                    labels[i] = moment(data[i].day).format('YYYY-MM-DD');
                }

                if (labels.length === 0) {
                    // new Notice('info', 'Не найдено ни одной записи');
                }
            }
        });

        let visitData = {
                label: "Посетителей",
                data: visit,
                borderColor: [
                    'rgba(255,255,255,1)'
                ],
                backgroundColor: [
                    'rgba(255,255,255,0.5)'
                ],
                borderWidth: 3,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
            },
            visitProfileData = {
                label: "Пользователей",
                data: visitProfile,
                borderColor: [
                    'rgba(255,255,255,1)'
                ],
                backgroundColor: [
                    'rgba(255,255,255,0.5)'
                ],
                borderWidth: 3,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
            },
            yVisit = {
                type: 'linear',
                position: 'left',
                gridLines: {
                    color: "#FFFFFF"
                },
                ticks: {
                    fontColor: "white",
                }
            };


        let chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [visitData,visitProfileData],
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            color: "#FFFFFF"
                        },
                        ticks: {
                            fontColor: "white",
                        }
                    }],
                    yAxes: [yVisit]
                }
            }
        });

    }

}

export class AdminChartRegister {

    constructor () {

        let chart,
            after = moment(new Date()).add(-7, 'days').format('YYYY-MM-DD'),
            before = moment(new Date()).format('YYYY-MM-DD');

        this.chartBuild(after, before);

        $('#drp-register').daterangepicker({
            startDate: moment().subtract(7, 'days'),
            endDate: moment()
        });
        $('#drp-register').on('apply.daterangepicker', (ev, picker) => {
            let after = picker.startDate.format('YYYY-MM-DD'),
                before = picker.endDate.format('YYYY-MM-DD');
            this.chartBuild(after, before);
            chart.update();
        });
    }

    chartBuild (after, before) {
        let ctx = $('#chart-register'),
            labels = [],
            count = [];

        $.ajax({
            url: `/profile/chart/register?after=${after}&before=${before}`,
            async: false,
            dataType: 'json',
            success: function (data) {
                for (let i = 0; i < data.length; i++) {
                    labels[i] = moment(data[i].regdate).format('YYYY-MM-DD');
                }

                labels = $.unique(labels);

                for (let i = 0; i < labels.length; i++) {
                    for (let a = 0; a < data.length; a++) {
                        if (labels[i] == moment(data[a].regdate).format('YYYY-MM-DD')){
                            if(isNaN(count[i])) count[i] = 0;
                            count[i]++;
                        }
                    }
                }

                if (labels.length === 0) {
                    // new Notice('info', 'Не найдено ни одной записи');
                }
            }
        });

        let countData = {
                label: "Регистраций",
                data: count,
                borderColor: [
                    'rgba(255,255,255,1)'
                ],
                backgroundColor: [
                    'rgba(255,255,255,0.5)'
                ],
                borderWidth: 3,
                pointHoverRadius: 5,
                pointHoverBackgroundColor: "rgba(75,192,192,1)",
                pointHoverBorderColor: "rgba(220,220,220,1)",
                pointHoverBorderWidth: 2,
            },
            yCount = {
                type: 'linear',
                position: 'left',
                gridLines: {
                    color: "#FFFFFF"
                },
                ticks: {
                    fontColor: "white",
                    stepSize: 5,
                    min: 0,
                    suggestedMax: 25,
                }
            };


        let chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [countData],
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: {
                            color: "#FFFFFF"
                        },
                        ticks: {
                            fontColor: "white",
                        }
                    }],
                    yAxes: [yCount]
                }
            }
        });

    }

}