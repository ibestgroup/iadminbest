import Notice from '../../Helpers/Notice';
import axios from "axios";
import Refresh from '../../Helpers/Refresher';
import AdEdit from "../../forms/AdEdit";

export class AdminMain {

    constructor () {
        $('.load-page').on('click', function () {

            new Refresh($(this).data('action'), '.main-admin', '.content');
        });
    }

}