import Notice from '../../Helpers/Notice';
import axios from "axios";
import Refresh from '../../Helpers/Refresher';
import AdEdit from "../../forms/AdEdit";
import Validator from '../../Helpers/Validator';


export default class AdminMarks {

    constructor () {
        this.form = document.querySelector('.js-markEdit');
        this.tarif = document.getElementsByClassName('js-markEdit');
        this.calculate();



        $('[data-toggle="tooltip"]').tooltip();

        this.submit();

    }

    submit() {
        $(document).on('submit', '.js-markEdit', (e) => {

            let i = e.target.dataset.id - 1;
            e.preventDefault();

            console.log(this.tarif[i]);
            axios.post(this.tarif[i].getAttribute('action'), new FormData(this.tarif[i]))
                .then((response) => {
                    $('.box').find('.overlay').remove();
                    console.log(response);

                    let typeNotice = "success";
                    if (response.data.type) {
                        typeNotice = response.data.type;
                    }

                    new Notice(typeNotice, response.data.message);


                }).catch((error) => {
                let errors = error.response.data.errors;
                console.log(error.response);
                _.each(errors, (error) => {
                    $(this.tarif[i]).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)
                });

                $('.box').find('.overlay').remove();
            });

            $(e.target).closest('.box').append(
                `<div class="overlay">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>`
            );

        });
    }

    calculate() {
        let marksBuider = {
            data: [],
            onInit: function() {
                this.data = [];
                let obj = this;
                $('.js-tarif-block').each(function (el) {
                    let params = {};
                    $(this).find('input, select').each(function () {
                        if ($(this).attr('type') == 'checkbox')
                            params[$(this).attr('name')] = $(this).prop('checked')
                        else
                            params[$(this).attr('name')] = $(this).val();
                    });
                    params.id = $(this).data('id');
                    obj.data.push(params);
                });
                this.hideMessage();

                this.markSum();
            },
            markSum: function () {
                let $this = this;
                this.data.forEach(function (el) {
                    el.enter = 0;
                    el.profit = 0;
                    el.out = 0;

                    for (let i = 1; i <= el.level; i++) {
                        let levelPrice = parseFloat(el['level'+i]);

                        el.enter += parseFloat(el['level'+i]);
                        el.profit += levelPrice * Math.pow(el.width, i);
                    }
                    if (el.to_admin) {
                        el.enter += parseFloat(el.to_admin);
                    }
                    if (el.to_par) {
                        el.enter += parseFloat(el.to_par);
                    }

                    el.profit = (el.profit * 0.9).toFixed(2);
                    el.out = el.profit;
                });

                this.data.forEach(function (el) {
                    if (el.reinvest != 0) {
                        el.out -= $this.data[el.reinvest-1].enter;
                    }

                    if (el.reinvest_first) {
                        el.out -= $this.data[0].enter;
                    }

                    el.out = parseFloat(el.out).toFixed(2);
                    $this.render(el);
                });
            },
            render: function (el) {
                let $this = this.data;
                let tarif = $('[data-id='+el.id+']');
                tarif.find('.js-all-sum').text(el.profit);

                if (el.reinvest_first) {
                    tarif.find('.js-reinvest-first').text($this[0].enter).closest('tr').show();
                } else {
                    tarif.find('.js-reinvest-first').closest('tr').hide();
                }


                if (el.reinvest != 0) {
                    tarif.find('.js-reinvest').text($this[el.reinvest-1].enter).closest('tr').show();
                    tarif.find('.js-tarif-name').text($this[el.reinvest-1].name);
                } else {
                    tarif.find('.js-reinvest').closest('tr').hide();
                }

                if (el.out < 0) {
                    this.showMessage(el);
                }

                tarif.find('.js-out-sum').text(el.out);
                tarif.find('.js-enter').text(el.enter);
            },
            showMessage: function (el) {
                let messageBlock = '<div class="col-lg-12 alert alert-danger alert-dismissible js-tarif-message" role="alert">' +
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                    '<strong>Ошибка!</strong> Итоговый доход тарифа не может быть отицательным.</div>';
                // console.log( messageBlock);
                $('[data-id='+el.id+']').prepend(messageBlock);
            },
            hideMessage: function () {
                $('.js-tarif-message').remove();
            }
        };
        $('.js-tarif-block').on('change', 'select, input', function () {
            marksBuider.onInit();
        });
    }



}