import Notice from '../../Helpers/Notice';
import axios from "axios";
import Refresh from '../../Helpers/Refresher';
import AdEdit from "../../forms/AdEdit";
import Validator from '../../Helpers/Validator';
import "jquery-ui/ui/core.js";
import 'jquery.cookie';

export default class AdminMarksCustom {

    constructor () {
        $(document).ready(function () {
            $('.my-tooltip').tooltip({
                position: {
                    my: "center top",
                    at: "center top",
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                }
            });

            $('body').find('*[data-tog]').on('click', function () {
                if ($(this).attr('data-flag') == 1) {
                    animateRotate(0, 45, $(this).find('i'));
                    $(this).attr('data-flag', 0);
                } else {
                    animateRotate(45, 0, $(this).find('i'));
                    $(this).attr('data-flag', 1);
                }

                let idTog = $(this).attr('data-tog');
                $(idTog).toggle(100);

            });
        });

        if ($.cookie('lang') === 'en') {
            this.levelsTable = 'Level',
            this.numberPeoples = 'Number or people',
            this.profitTable = 'Profit';
            this.reinvestTable = 'Reinvest';
            this.reinvestFirstTable = 'Reinvest first tariff';
            this.totalTable = 'Total';
        } else {
            this.levelsTable = 'Уровень',
            this.numberPeoples = 'Количество человек',
            this.profitTable = 'Доход';
            this.reinvestTable = 'Реинвест';
            this.reinvestFirstTable = 'Реинвест в первый тариф';
            this.totalTable = 'Итог';
        }


        this.tarif = document.getElementsByClassName('js-markEditCustom');

        $('.mark-type').on('click', function () {
            let onlyLinear = $(this).parents('.js-markEditCustom').find('.only-linear');
            if ($(this).children('input').prop('checked') === true) {
                onlyLinear.hide();
            } else {
                onlyLinear.show();
            }
        });

        $('body').on('input change', 'select[name=overflow]', function () {
            let overflow = $(this).val();

            $(this).parents('.save-mark').find('.disable-overflow').each(function (e) {
                if (overflow != 0) {
                    $(this).removeAttr('disabled');
                } else {
                    if ($(this).is('input')) {
                        $(this).val(0);
                    } else {
                        $(this).val($(this).find('option:first-child').val());
                    }
                    $(this).attr('disabled', 'disabled');
                }
            });
        });



        $('select[name=level]').on('input', function () {
            let levels = $(this).val();
            $(this).parents('.save-mark').find('.levels-info').find('.for-result').each(function (e) {
                if (e >= levels) {
                    $(this).hide();
                } else {
                    $(this).show();
                }
            });
        });

        $('.reinvest').on('input', function () {
            if ($(this).val() > 0) {
                $(this).next().show();
            } else {
                $(this).next().hide();
            }
        });

        $('select, input').on('input change', () => {
            this.resultProfit();
        });

        function animateRotate(d, start, element){
            $({deg: 0}).animate({deg: d}, {
                step: function(now, fx){
                    fx.start = start;
                    element.css({
                        transform: "rotate(" + now + "deg)"
                    });
                },
                duration: 100,
            });
        }

        this.resultProfit();

        this.resultPrice();

        $('[data-toggle="tooltip"]').tooltip();

        this.submit();

    }

    submit() {
        $(document).on('submit', '.js-markEditCustom', (e) => {

            let i = e.target.dataset.id - 1;
            e.preventDefault();

            axios.post(e.target.getAttribute('action'), new FormData(e.target))
                .then((response) => {
                    $('.box').find('.overlay').remove();

                    let typeNotice = "success";
                    if (response.data.type) {
                        typeNotice = response.data.type;
                    }

                    new Notice(typeNotice, response.data.message);


                }).catch((error) => {
                // console.log(error.response);
                let errors = error.response.data.errors;
                _.each(errors, (error) => {
                    $(this.tarif[i]).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)
                });

                $('.box').find('.overlay').remove();
            });

            $(e.target).closest('.box').append(
                `<div class="overlay" style="text-align: center; vertical-align: middle; position: absolute;">
                    <img src="https://1.bp.blogspot.com/-RRbd-J49gic/XTK6tgT-y3I/AAAAAAAAO_E/akowvdssME0gKj1ukMtft4G8uxuACWqPACLcBGAs/s1600/loader%2B%25281%2529.gif">
                </div>`
            );

        });
    }

    resultPrice() {

        $('.for-result input').on('input', function () {
            let forResult = $(this).parents('.for-result');

            let toPar = Number(forResult.find('.to-par').val());
            let toAdmin = Number(forResult.find('.to-admin').val());
            let priceLevel = Number(forResult.find('.price-level').val());
            let result = toPar + toAdmin + priceLevel;

            forResult.find('.result').val(result);

        });
    }

    getMarksInfo() {
        let marks = {};

        $('.save-mark').each(function (e, el) {
            let markNum = $(this).data('id'),
                multiplier = ALTER_FEE == 1 ? 100 : 90;

            marks[markNum] = {};

            marks[markNum]['id'] = markNum;
            marks[markNum]['level'] = $('#level-'+markNum).val();
            marks[markNum]['width'] = $('#width-'+markNum).val();
            marks[markNum]['type'] = $('#type-'+markNum).val();
            marks[markNum]['overflow'] = $('#overflow-'+markNum).val();
            marks[markNum]['enter'] = 0;
            marks[markNum]['to_inviters'] = 0;

            for(let i = 1; i <= marks[markNum]['level']; i++) {
                marks[markNum]['people_'+i] = Math.pow(marks[markNum]['width'], i);
                marks[markNum]['level_'+i] = Number($('#level-'+ e +'-'+i).val());
                marks[markNum]['level_res_'+i] = Number((marks[markNum]['level_'+i]/100*multiplier).toFixed(2));
                marks[markNum]['to_par_'+i] = Number($('#to_par-'+ e +'-'+i).val());
                marks[markNum]['to_admin_'+i] = Number($('#to_admin-'+ e +'-'+i).val());
                marks[markNum]['level_enter_'+i] = marks[markNum]['level_'+i] + marks[markNum]['to_par_'+i] + marks[markNum]['to_admin_'+i];
                marks[markNum]['reinvest_'+i] = $('#reinvest-'+ e +'-'+i).val();
                marks[markNum]['reinvest_type_'+i] = $('#reinvest_type-'+ e +'-'+i).val();
                marks[markNum]['reinvest_first_'+i] = $('#reinvest_first-'+ e +'-'+i).val();
                marks[markNum]['reinvest_first_type_'+i] = $('#reinvest_first_type-'+ e +'-'+i).val();
                marks[markNum]['priority_'+i] = $('#priority-'+ e +'-'+i).val();

                if (i <= 1) {
                    for(let l = 1; l <= 10; l++) {
                        marks[markNum]['to_inviters'] = marks[markNum]['to_inviters'] + Number($('#to_inviters-'+ e +'-'+l).val());
                    }
                } else {
                    marks[markNum]['to_inviters'] = 0;
                }

                if (marks[markNum]['type'] == 2 || i <= 1) {
                    marks[markNum]['enter'] = marks[markNum]['enter'] + marks[markNum]['level_enter_'+i] + marks[markNum]['to_inviters'];
                }

                console.log(marks[markNum]['reinvest_first_type_'+i]);

                // if ($('#type-'+marks[markNum]['reinvest_first_type_'+i]).val() == 2 && marks[markNum]['reinvest_first_'+i]) {

                    // marks[markNum]['level_'+i] = Number($('#level-'+ e +'-'+i).val());
                    // marks[markNum]['to_par_'+i] = Number($('#to_par-'+ e +'-'+i).val());
                    // marks[markNum]['to_admin_'+i] = Number($('#to_admin-'+ e +'-'+i).val());
                    //
                    // marks[markNum]['level_enter_'+i] = marks[markNum]['level_'+i] + marks[markNum]['to_par_'+i] + marks[markNum]['to_admin_'+i];
                    // marks[markNum]['enter_first'] = marks['reinvest_first_type_'+i]['enter'];
                //     marks[markNum]['enter_first'] = marks[1]['enter'];
                //
                // }


                if ($('#type-'+marks[markNum]['reinvest_first_type_'+i]).val() == 2 && marks[markNum]['reinvest_first_'+i]) {
                    let resMark = marks[markNum]['reinvest_first_type_'+i] - 1;
                    let resClass = resMark + '-1';
                    marks[markNum]['enter_first'] = Number($('#level-' + resClass).val()) + Number($('#to_admin-' + resClass).val()) + Number($('#to_par-' + resClass).val());
                } else {
                    marks[markNum]['enter-first'] = 0;
                }

                if (marks[markNum]['reinvest_'+i] > 0) {
                    let resMark = marks[markNum]['reinvest_'+i] - 1;
                    let resClass = resMark + '-1';
                    marks[markNum]['reinvest_amount_'+i] = Number($('#level-' + resClass).val()) + Number($('#to_admin-' + resClass).val()) + Number($('#to_par-' + resClass).val());
                } else {
                    marks[markNum]['reinvest_amount_'+i] = 0;
                }
            }
        });

        return marks;
    }

    getMarkInfo()
    {

    }

    getMarksInfoCrypto() {
        let marks = {};

        $('.save-mark').each(function (e, el) {
            let markNum = $(this).data('id'),
                multiplier = ALTER_FEE == 1 ? 100 : 90;
            marks[markNum] = {};

            marks[markNum]['id'] = markNum;
            marks[markNum]['level'] = $('#level-'+markNum).val();
            marks[markNum]['width'] = $('#width-'+markNum).val();
            marks[markNum]['overflow'] = $('#overflow-'+markNum).val();
            marks[markNum]['type'] = $('#type-'+markNum).val();
            marks[markNum]['enter'] = 0;
            marks[markNum]['to_inviters'] = 0;
            for(let i = 1; i <= marks[markNum]['level']; i++) {
                marks[markNum]['people_'+i] = Math.pow(marks[markNum]['width'], i);
                marks[markNum]['level_'+i] = Number($('#level-'+ e +'-'+i).val());
                marks[markNum]['level_res_'+i] = Number(marks[markNum]['level_'+i]/100*multiplier);
                marks[markNum]['to_par_'+i] = Number($('#to_par-'+ e +'-'+i).val());
                marks[markNum]['to_admin_'+i] = Number($('#to_admin-'+ e +'-'+i).val());
                marks[markNum]['level_enter_'+i] = Number(marks[markNum]['level_'+i]) + Number(marks[markNum]['to_par_'+i]) + Number(marks[markNum]['to_admin_'+i]);
                marks[markNum]['reinvest_'+i] = $('#reinvest-'+ e +'-'+i).val();
                marks[markNum]['reinvest_type_'+i] = $('#reinvest_type-'+ e +'-'+i).val();
                marks[markNum]['reinvest_first_'+i] = $('#reinvest_first-'+ e +'-'+i).val();
                marks[markNum]['reinvest_first_type_'+i] = $('#reinvest_first_type-'+ e +'-'+i).val();
                marks[markNum]['priority_'+i] = $('#priority-'+ e +'-'+i).val();

                if (i <= 1) {
                    for(let l = 1; l <= 10; l++) {
                        marks[markNum]['to_inviters'] = marks[markNum]['to_inviters'] + Number($('#to_inviters-'+ e +'-'+l).val());
                    }
                } else {
                    marks[markNum]['to_inviters'] = 0;
                }

                if (marks[markNum]['type'] == 2 || i <= 1) {
                    marks[markNum]['enter'] = Number(marks[markNum]['enter']) + Number(marks[markNum]['level_enter_'+i]) + marks[markNum]['to_inviters'];
                }


                if (marks['reinvest_first_type_'+i]['type'] == 2 && marks[markNum]['reinvest_first_'+i]) {
                    marks[markNum]['enter_first'] = marks['reinvest_first_type_'+i]['enter'];
                } else if (marks[markNum]['reinvest_first_'+i]) {
                    marks[markNum]['enter_first'] = marks['reinvest_first_type_'+i]['enter'];
                }



                if (marks[markNum]['reinvest_'+i] > 0) {
                    let resMark = marks[markNum]['reinvest_'+i] - 1;
                    let resClass = resMark + '-1';
                    marks[markNum]['reinvest_amount_'+i] = Number($('#level-' + resClass).val()) + Number($('#to_admin-' + resClass).val()) + Number($('#to_par-' + resClass).val());
                } else {
                    marks[markNum]['reinvest_amount_'+i] = 0;
                }
            }



        });

        // console.log(marks);


        return marks;
    }

    resultProfit() {


        $.getJSON('/get-site-info', (data) => {

            let q = data.currency;
            let marks;
            if (q > 3) {
                marks = this.getMarksInfoCrypto();
            } else {
                marks = this.getMarksInfo();
            }

            // console.log(this.levelsTable);
            // console.log(marks);



            Object.values(marks).forEach((mark) => {

                // console.log(mark);
                let reinvestFields = ``;

                for (let l = 1; l <= mark['level']; l++) {

                    if (l == 1) {
                        if (mark['overflow'] == 0) {
                            reinvestFields = ``;
                        } else {
                            reinvestFields = `
                                <th>${this.reinvestTable}</th>
                                <th>${this.reinvestFirstTable}</th>`;
                        }
                        $('.result-profit-'+mark['id']).html(`
                            <tr>
                                <th>${this.levelsTable}</th>
                                <th>${this.numberPeoples}</th>
                                <th>${this.profitTable}</th>
                                ${reinvestFields}
                                <!--<th>Приоритет</th>-->
                                <th>${this.totalTable}</th>
                            </tr>
                        `);
                    }


                    let reinvestAmount = 0, reinvestString = 'нет', profitString = '', reinvestFirstAmount = 0, reinvestFirstString = 'нет', priority = 'Вывод';

                    if (data.currency > 3) {

                        profitString = mark['level_res_'+l] * mark['people_'+l];
                        profitString = profitString.toFixed(8) + '(' + mark['level_res_'+l].toFixed(8) +'*'+ mark['people_'+l]+')';

                        if (mark['reinvest_'+l] > 0 && mark['reinvest_type_' + l] > 0) {
                            reinvestAmount = marks[mark['reinvest_' + l]]['enter'] * mark['reinvest_type_' + l];
                            reinvestString = reinvestAmount.toFixed(8) + '(' + mark['reinvest_type_' + l] + '*' + marks[mark['reinvest_' + l]]['enter'].toFixed(8) + ')';
                        }

                        if (mark['reinvest_first_'+l] > 0) {
                            reinvestFirstAmount = marks[mark['reinvest_first_type_' + l]]['enter'] * mark['reinvest_first_' + l];
                            reinvestFirstString = reinvestFirstAmount.toFixed(8) + '(' + mark['reinvest_first_' + l] + '*' + marks['reinvest_first_type_'+i]['enter'] + ')';
                        }

                        $('.mark-enter-'+i).html(mark['enter'].toFixed(8));

                    } else {

                        profitString = mark['level_res_'+l] * mark['people_'+l];
                        profitString = profitString.toFixed(2) + '(' + mark['level_res_'+l].toFixed(2) +'*'+ mark['people_'+l]+')';
                        // console.log(profitString, mark['enter'], '.mark-enter-'+mark['id']);

                        if (mark['reinvest_'+l] > 0 && mark['reinvest_type_' + l] > 0) {
                            reinvestAmount = marks[mark['reinvest_' + l]]['enter'] * mark['reinvest_type_' + l];
                            reinvestString = reinvestAmount.toFixed(2) + '(' + mark['reinvest_type_' + l] + '*' + marks[mark['reinvest_' + l]]['enter'].toFixed(2) + ')';
                        }

                        if (mark['reinvest_first_'+l] > 0) {
                            console.log(marks);
                            console.log(marks['reinvest_first_type_'+l]);
                            console.log('reinvest_first_type_'+l);
                            reinvestFirstAmount = marks[mark['reinvest_first_type_' + l]]['enter'] * mark['reinvest_first_' + l];
                            reinvestFirstString = reinvestFirstAmount.toFixed(2) + '(' + mark['reinvest_first_' + l] + '*' + marks[mark['reinvest_first_type_' + l]]['enter'] + ')';
                        }

                        $('.mark-enter-'+mark['id']).html(mark['enter'].toFixed(2));

                    }


                    let nextLevel = l+1, priceNextLevel = 0;

                    if (nextLevel <= mark['level']) {
                        priceNextLevel = mark['level_enter_'+nextLevel];
                    }

                    if (mark['priority_' + l] == 321) {
                        priority = 'Реинвест';
                    }

                    let resultAmount = mark['level_res_'+l] * mark['people_'+l] - reinvestAmount - reinvestFirstAmount;

                    if (mark['type'] == 1) {
                        resultAmount = resultAmount - priceNextLevel;
                    }

                    let peopleCount;

                    if (mark['overflow'] == 0) {
                        peopleCount = '<i class="fa fa-infinity"></i>';
                        reinvestFields = ``;
                        resultAmount = '<i class="fa fa-infinity"></i>';
                        profitString = '<i class="fa fa-infinity"></i>';
                    } else {
                        peopleCount = mark['people_' + l];
                        reinvestFields = `
                                <td>${reinvestString}</td>
                                <td>${reinvestFirstString}</td>`;

                        if (data.currency > 3)
                            resultAmount = resultAmount.toFixed(8);
                        else
                            resultAmount = resultAmount.toFixed(2);

                    }

                    if (data.currency > 3) {
                        $('.result-profit-' + mark['id']).append(`
                            <tr class="ta-center">
                                <td>${l}</td>
                                <td>${peopleCount}</td>
                                <td>${profitString}</td>
                                ${reinvestFields}
                                <td>${resultAmount}</td>
                            </tr>
                        `);
                    } else {
                        $('.result-profit-' + mark['id']).append(`
                            <tr class="ta-center">
                                <td>${l}</td>
                                <td>${peopleCount}</td>
                                <td>${profitString}</td>
                                ${reinvestFields}
                                <td>${resultAmount}</td>
                            </tr>
                        `);
                    }



                    let n = l+1;
                    if (resultAmount < 0) {

                        $('.result-profit-'+mark['id']+' tr:nth-child('+n+')').css('border', '3px solid rgb(228, 62, 13)');
                        $('.result-profit-'+mark['id']+' tr:nth-child('+n+') td:first-child').prepend(`<a class="my-tooltip pull-left" title = "Итоговый доход, не может быть отрицательным." ><i class="fas fa-exclamation-triangle" style="color:rgb(228, 62, 13);"></i></a>`);


                        $('.my-tooltip').tooltip({
                            position: {
                                my: "center bottom-20",
                                at: "center top",
                                using: function( position, feedback ) {
                                    $( this ).css( position );
                                    $( "<div>" )
                                        .addClass( "arrow" )
                                        .addClass( feedback.vertical )
                                        .addClass( feedback.horizontal )
                                        .appendTo( this );
                                }
                            }
                        });
                        // $('.result-profit-'+i).append(`
                        //     Ошибка: Итоговый доход отрицательный
                        // `);
                    }
                }

            });


            // for (let i = 1; i <= 9; i++) {
            //     for (let l = 1; l <= marks[i]['level']; l++) {
            //         if (l == 1) {
            //             $('.result-profit-'+i).html(`
            //                 <tr>
            //                     <th>${this.levelsTable}</th>
            //                     <th>${this.numberPeoples}</th>
            //                     <th>${this.profitTable}</th>
            //                     <th>${this.reinvestTable}</th>
            //                     <th>${this.reinvestFirstTable}</th>
            //                     <!--<th>Приоритет</th>-->
            //                     <th>${this.totalTable}</th>
            //                 </tr>
            //             `);
            //         }
            //
            //
            //         let reinvestAmount = 0, reinvestString = 'нет', profitString = '', reinvestFirstAmount = 0, reinvestFirstString = 'нет', priority = 'Вывод';
            //
            //         if (data.currency > 3) {
            //
            //             profitString = marks[i]['level_res_'+l] * marks[i]['people_'+l];
            //             profitString = profitString.toFixed(8) + '(' + marks[i]['level_res_'+l].toFixed(8) +'*'+ marks[i]['people_'+l]+')';
            //
            //             if (marks[i]['reinvest_'+l] > 0 && marks[i]['reinvest_type_' + l] > 0) {
            //                 reinvestAmount = marks[marks[i]['reinvest_' + l]]['enter'] * marks[i]['reinvest_type_' + l];
            //                 reinvestString = reinvestAmount.toFixed(8) + '(' + marks[i]['reinvest_type_' + l] + '*' + marks[marks[i]['reinvest_' + l]]['enter'].toFixed(8) + ')';
            //             }
            //
            //             if (marks[i]['reinvest_first_'+l] > 0) {
            //                 reinvestFirstAmount = marks[1]['enter'] * marks[i]['reinvest_first_' + l];
            //                 reinvestFirstString = reinvestFirstAmount.toFixed(8) + '(' + marks[i]['reinvest_first_' + l] + '*' + marks[1]['enter'] + ')';
            //             }
            //
            //             $('.mark-enter-'+i).html(marks[i]['enter'].toFixed(8));
            //
            //         } else {
            //
            //             profitString = marks[i]['level_res_'+l] * marks[i]['people_'+l];
            //             profitString = profitString + '(' + marks[i]['level_res_'+l] +'*'+ marks[i]['people_'+l]+')';
            //
            //             if (marks[i]['reinvest_'+l] > 0 && marks[i]['reinvest_type_' + l] > 0) {
            //                 reinvestAmount = marks[marks[i]['reinvest_' + l]]['enter'] * marks[i]['reinvest_type_' + l];
            //                 reinvestString = reinvestAmount + '(' + marks[i]['reinvest_type_' + l] + '*' + marks[marks[i]['reinvest_' + l]]['enter'] + ')';
            //             }
            //
            //             if (marks[i]['reinvest_first_'+l] > 0) {
            //                 reinvestFirstAmount = marks[1]['enter'] * marks[i]['reinvest_first_' + l];
            //                 reinvestFirstString = reinvestFirstAmount + '(' + marks[i]['reinvest_first_' + l] + '*' + marks[1]['enter'] + ')';
            //             }
            //
            //             $('.mark-enter-'+i).html(marks[i]['enter']);
            //
            //         }
            //
            //
            //         let nextLevel = l+1, priceNextLevel = 0;
            //
            //         if (nextLevel <= marks[i]['level']) {
            //             priceNextLevel = marks[i]['level_enter_'+nextLevel];
            //         }
            //
            //         if (marks[i]['priority_' + l] == 321) {
            //             priority = 'Реинвест';
            //         }
            //
            //         let resultAmount = marks[i]['level_res_'+l] * marks[i]['people_'+l] - reinvestAmount - reinvestFirstAmount;
            //
            //         if (marks[i]['type'] == 1) {
            //             resultAmount = resultAmount - priceNextLevel;
            //         }
            //
            //         console.log(resultAmount);
            //         if (data.currency > 3) {
            //             $('.result-profit-' + i).append(`
            //                 <tr class="ta-center">
            //                     <td>${l}</td>
            //                     <td>${marks[i]['people_' + l]}</td>
            //                     <td>${profitString}</td>
            //                     <td>${reinvestString}</td>
            //                     <td>${reinvestFirstString}</td>
            //                     <!--<td>${priority}</td>-->
            //                     <td>${resultAmount.toFixed(8)}</td>
            //                 </tr>
            //             `);
            //         } else {
            //             $('.result-profit-' + i).append(`
            //                 <tr class="ta-center">
            //                     <td>${l}</td>
            //                     <td>${marks[i]['people_' + l]}</td>
            //                     <td>${profitString}</td>
            //                     <td>${reinvestString}</td>
            //                     <td>${reinvestFirstString}</td>
            //                     <!--<td>${priority}</td>-->
            //                     <td>${resultAmount.toFixed(2)}</td>
            //                 </tr>
            //             `);
            //         }
            //
            //
            //
            //         let n = l+1;
            //         if (resultAmount < 0) {
            //
            //             $('.result-profit-'+i+' tr:nth-child('+n+')').css('border', '3px solid rgb(228, 62, 13)');
            //             $('.result-profit-'+i+' tr:nth-child('+n+') td:first-child').prepend(`<a class="my-tooltip pull-left" title = "Итоговый доход, не может быть отрицательным." ><i class="fas fa-exclamation-triangle" style="color:rgb(228, 62, 13);"></i></a>`);
            //
            //
            //             $('.my-tooltip').tooltip({
            //                 position: {
            //                     my: "center bottom-20",
            //                     at: "center top",
            //                     using: function( position, feedback ) {
            //                         $( this ).css( position );
            //                         $( "<div>" )
            //                             .addClass( "arrow" )
            //                             .addClass( feedback.vertical )
            //                             .addClass( feedback.horizontal )
            //                             .appendTo( this );
            //                     }
            //                 }
            //             });
            //             // $('.result-profit-'+i).append(`
            //             //     Ошибка: Итоговый доход отрицательный
            //             // `);
            //         }
            //     }
            // }

        });

    }
}