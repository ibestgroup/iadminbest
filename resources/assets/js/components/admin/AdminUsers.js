import Notice from '../../Helpers/Notice';
import axios from "axios";
import Refresh from '../../Helpers/Refresher';

export default class AdminUsers {

    constructor () {
        $('#load-users').on('input', function () {

            $.ajax({
                url: `/profile/admin/users-load?user=${$(this).val()}`,
                async: false,
                dataType: 'json',
                before: $('.load-users').html('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>'),
                success: function (data) {

                    let userList = '', maxLimit = '', typefollow = '';

                    for (let i = 0; i < data.length; i++) {
                        if (i < 20) {
                            typefollow = `<span class="label label-danger">Не активирован</span>`;
                            if (data[i].type_follow)
                                typefollow = `<span class="label label-success">Активирован</span>`;

                            userList += `
                                <tr onclick='window.open("/profile/admin/users/${data[i].id}","_self")' style="cursor:pointer">
                                    <td>${data[i].id}</td>
                                    <td>${data[i].login}</td>
                                    <td>${data[i].regdate}</td>
                                    <td>${typefollow}</td>
                                </tr>`;
                            maxLimit = `Всего ${data.length} записей`;
                        } else {
                            maxLimit = `Показано 20 результатов из ${data.length}`;
                        }
                    }



                    let result = `${maxLimit}
                        <table class="table table-hover">
                            <tbody>
                                <tr>
                                    <th>ID</th>
                                    <th>Логин</th>
                                    <th>Дата регистрации</th>
                                    <th>Статус</th>
                                </tr>
                                ${userList}
                            </tbody>
                        </table>`;

                    if (data.length === 0) {
                        result = `<tr><td>Не найдено ни одного пользователя по вашему запросу</td></tr>`;
                    }

                    $('.load-users').html(result);


                    // if (data.length === 0) {
                    //     new Notice('info', 'Не найдено ни одного пользователя');
                    // }
                }
            });

        });
    }

}