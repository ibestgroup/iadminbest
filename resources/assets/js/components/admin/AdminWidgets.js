import Notice from '../../Helpers/Notice';
import axios from "axios";
import Refresh from '../../Helpers/Refresher';
import React from "react";
import ReactDOM from "react-dom";
import Widgets from './widgets/Widgets.js';

// import AdEdit from "../../forms/AdEdit";

export default class AdminWidgets {
    constructor () {
        ReactDOM.render(
          <Widgets />,
          document.getElementById('widgets')
        );
    }
}

