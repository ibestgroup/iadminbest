import axios from "axios";
import _ from "lodash";
import Notice from "../../Helpers/Notice";
import Refresh from "../../Helpers/Refresher";
require("jquery-confirm");

export default class BirdsOrderEdit {
    constructor() {
        this.form = $('.js-birds-order');



        this.submit();
    }



    submit() {
        $(document).on('submit', '.js-birds-order', (event) => {
            event.preventDefault();
            $.confirm({
                title: 'Подтвердите действие',
                content: 'Вы уверены что хотите выполнить это действие, восстановить данные будет невозможно',
                buttons: {
                    Да: () => {
                        let current = $(event.target);
                        axios.post(event.target.action, new FormData(event.target))
                            .then((response) => {
                                current.closest('.box').find('.overlay').remove();
                                current.find('.alert').remove();
                                current.find('.box-body').prepend(`<p class="alert alert-success">${response.data.message}</p>`);

                                let typeNotice = "success";
                                if (response.data.type) {
                                    typeNotice = response.data.type;
                                }

                                new Notice(typeNotice, response.data.message);
                                new Refresh(location.pathname, '.box-body', '.box-body');
                            }).catch((error) => {
                            let errors = error.response.data.errors;
                            current.find('.alert').remove();

                            _.each(errors, (error) => {
                                current.find('.box-body').prepend(`<p class="alert alert-danger">${error}</p>`)
                            });

                            current.closest('.box').find('.overlay').remove();
                            });

                        current.closest('.box').append(
                            `<div class="overlay">
                                    <i class="fa fa-refresh fa-spin"></i>
                                </div>`
                        );
                    },
                    Отменить: function () {
                    },
                    post: true
                }
            });
        })
    }
}
