import Notice from '../../Helpers/Notice';
import axios from "axios";
import Refresh from '../../Helpers/Refresher';
import Validator from "../../Helpers/Validator";
import FileInput from "../../vendor/File";
import 'jquery.cookie';
import 'select2';
import "ckeditor4";

export default class BotSettings
{

    constructor (refresh = false)
    {
        if (!refresh) {
            this.treeInit();
            this.treeSaveMove();
            this.treeEditElement();
            this.treeDestroyElement();
            this.treeCreateElement();
            this.editFormWidget();
            this.lineBreak();
        }
    }

    treeInit() {
        $.getJSON(
            '/profile/admin/bot-settings/get-json',
            data => {
                $('#tree1').tree({
                    data: data,
                    dragAndDrop: true,
                    autoOpen: true,
                    onCreateLi: (node, $li) => {
                        let removeBtn = '';
                        if (!node.children.length) {
                            removeBtn = `<a data-id="${node.id}" href="#node-${node.id}" data-node-id="${node.id}" class="bot-menu-btn remove btn btn-danger"><i class="fa fa-times"></i></a>`;
                        }
                        let checked = node.line_break ? 'checked="checked"' : '';
                        let checkbox = node.sort != 0
                                            ? `<span style="margin: 0 10px;">
                                                    <a class="my-tooltip" title="Кнопка с новой строки" style="color: #555;">
                                                        <i class="fas fa-level-down-alt"></i>
                                                        <input data-id="${node.id}" class="line-break" type="checkbox" ${checked}>
                                                    </a>
                                                </span>`
                                            : ``;

                        $li.find('.jqtree-element').append(`
                                ${checkbox}
                                <a data-id="${node.id}" href="#node-${node.id}" data-node-id="${node.id}" class="bot-menu-btn edit btn btn-info" data-toggle="modal"  data-target="#bot-settings-edit"><i class="fa fa-edit"></i></a>
                                <a data-id="${node.id}" href="#node-${node.id}" data-node-id="${node.id}" class="bot-menu-btn add btn btn-success" data-toggle="modal"  data-target="#bot-settings-add"><i class="fa fa-plus"></i></a>
                                ${removeBtn}
                            `);
                        $('.my-tooltip').tooltip({
                            position: {
                                my: "center top",
                                at: "center top",
                                using: function( position, feedback ) {
                                    $( this ).css( position );
                                    $( "<div>" )
                                        .addClass( "arrow" )
                                        .addClass( feedback.vertical )
                                        .addClass( feedback.horizontal )
                                        .appendTo( this );
                                }
                            }
                        });
                    },
                    onCanMoveTo: function(moved_node, target_node, position) {
                        return !(target_node.id == 1 && position != 'inside');
                    }
                });
            }
        );
    }

    treeSaveMove() {
        $('body').on(
            'tree.move',
            '#tree1',
            event => {
                event.preventDefault();
                event.move_info.do_move();

                let obj = event.move_info.moved_node.parent.children,
                    resArray = [];
                for (let i = 0; i < obj.length; i++) {
                    resArray[i] = obj[i].id;
                }

                $.ajax({
                    url: '/profile/admin/bot-settings/action/save-move',
                    type: 'post',
                    data: {
                        tree: {
                            moved_node: event.move_info.moved_node.id,
                            parent_node: event.move_info.moved_node.parent.id,
                            sort_node: resArray,
                            target_node: event.move_info.target_node.id,
                            position: event.move_info.position
                        }
                    },
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    dataType: 'json',
                    success: e => {
                        e.data.forEach(item => {
                            let node = $('#tree1').tree('getNodeById', item.id);
                            $('#tree1').tree(
                                'updateNode',
                                node,
                                {
                                    sort: item.sort,
                                    line_break: item.line_break,
                                }
                            );
                        });
                    }
                });
            }
        );
    }

    treeEditElement()
    {
        $('body').on('click', '.bot-menu-btn.edit', e => {
            e.preventDefault();

            $.ajax({
                url: '/profile/admin/bot-settings/get-json/' + $(e.currentTarget).attr('data-id'),
                type: 'get',
                success: data => {
                    data = JSON.parse(data);

                    $('#bot-settings-edit .content-menu').html(`<textarea id="editor" class="form-control" name="text"></textarea>`);

                    data.text = data.text != null ? data.text.replaceAll('\n', '<br />') : '';

                    $('#bot-settings-edit').find('textarea[name=text]').html(data.text != null ? data.text : '');
                    $('#bot-settings-edit input[name=id]').val(data.id);

                    if (data.type === 'part') {
                        $('.menu-name').hide();
                        $('.widget-name').hide();
                        $('#bot-settings-edit input[name=id]').val(data.part);
                    } else {
                        $('.menu-name').show();
                        $('.widget-name').show();
                        $('#bot-settings-edit input[name=id]').val(data.id);
                        $('#bot-settings-edit input[name=name]').show().val(data.name);
                        $('#bot-settings-edit .edit-widget').show().html(data.form_widget != null ? data.form_widget : '');
                    }

                    if (data.widget) {
                        $('#bot-settings-edit select[name=widget]').val(data.widget);
                    } else {
                        $('#bot-settings-edit select[name=widget]').val('');
                    }

                    if (data.widget_settings != null && data.widget_settings != "null") {
                        let widget_settings = JSON.parse(data.widget_settings);

                        for (const [key, value] of Object.entries(widget_settings)) {
                            let item = $(`#bot-settings-edit *[name='widget_settings[${key}]']`);
                            if (item.is(':checkbox')) {
                                item.prop('checked', true);
                            } else {
                                item.val(value);
                            }
                        }
                    }

                    this.addEditor();

                }
            });

        });
    }

    treeDestroyElement()
    {
        $('body').on('click', '.bot-menu-btn.remove', function() {
            $.ajax({
                url: '/profile/admin/bot-settings/action/remove',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    id: $(this).attr('data-id')
                },
                success: data => {
                    if (data.type === 'success') {
                        let nodeId = parseInt(data.id),
                            node = $('#tree1').tree('getNodeById', nodeId);

                        $('#tree1').tree('removeNode', node);
                    }
                }
            });
        });
    }

    treeCreateElement()
    {
        $('body').on('click', '.bot-menu-btn.add', function() {
            $.ajax({
                url: '/profile/admin/bot-settings/action/create',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    id: $(this).attr('data-id')
                },
                success: data => {
                    if (data.type === 'success') {
                        let nodeId = parseInt($(this).attr('data-id')),
                            parent_node = $('#tree1').tree('getNodeById', nodeId);

                        $('#tree1').tree(
                            'appendNode',
                            {
                                name: data.name,
                                id: data.id,
                            },
                            parent_node
                        );

                        $('#tree1').tree('openNode', parent_node);
                    }
                }
            });
        });
    }

    editFormWidget() {
        $('body').on('change', '.edit-form-widget', function () {
            $.ajax({
                url: '/profile/admin/bot-settings/get-form-widget',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    widget: $(this).val()
                },
                success: data => {
                    $('.edit-widget').html(data.widget);
                }
            });
        });
    }

    addEditor()
    {
        if ($('#editor').length) {
            CKEDITOR.config.font_names = CKEDITOR.config.font_names + '; Arial Black/Arial Black, Helvetica, sans-serif';
            CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
            CKEDITOR.replace( 'editor', {
                language: 'ru',
                uiColor: '#eeeeee',
                toolbarGroups: [
                    { name: 'values' },
                ],
                on: {
                    pluginsLoaded: function() {
                        var editor = this,
                            config = editor.config;

                        let marks = [];
                        $.ajax({
                            url: `/get-site-info`,
                            async: false,
                            dataType: 'json',
                            success: function (data) {
                                marks = data.marks;
                            }
                        });

                        Object.values(marks).forEach((mark) => {
                            editor.ui.addRichCombo( 'my-combo-' + mark['id'], {
                                label: mark['name'],
                                title: 'Маркетинги',
                                toolbar: 'values,' + mark['id'],

                                panel: {
                                    css: [ CKEDITOR.skin.getPath( 'editor' ) ].concat( config.contentsCss ),
                                    multiSelect: false,
                                },

                                init: function() {

                                    let markInfoValues = {
                                        price_sum: 'Общая цена уровня',
                                        price: 'Цена уровня',
                                        to_par: 'Пригласителю',
                                        to_admin: 'Админу',
                                        reinvest: 'Реинвест',
                                        reinvest_type: 'Тип реинвеста',
                                        reinvest_first: 'Реинвест в первый тариф',
                                        profit: 'Доход',
                                        people: 'Количество людей',
                                    };

                                    this.startGroup( mark['name'] );
                                    this.add( '<a href="javascript:activate(' + mark['id'] + ');" class="btn btn-activate-tarif" style="">' + mark['name'] + '</a>', 'Кнопка активации' );
                                    this.add( '{mark_enter(' + mark['id'] + ')}', 'Цена тарифа' );
                                    this.add( '{mark_profit(' + mark['id'] + ')}', 'Доход тарифа' );
                                    this.add('{mark_next_user(' + mark['id'] + ')}', 'Следующий на очереди');
                                    for (const [key, value] of Object.entries(markInfoValues)) {
                                        this.startGroup( `--- ${value}` );
                                        for (let i = 1; i <= mark['level']; i++) {
                                            this.add(`{mark_level_${key}(${mark['id']}_${i})}`, `Уровень ${i}`);
                                        }
                                    }
                                    this.startGroup( `--- Пригласителю (активация)` );
                                    for (let i = 1; i <= 10; i++) {
                                        this.add(`{mark_level_to_inviter(${mark['id']}_${i})}`, `Уровень ${i}`);
                                    }

                                },

                                onClick: function( value ) {
                                    editor.focus();
                                    editor.fire( 'saveSnapshot' );

                                    editor.insertHtml( value );

                                    editor.fire( 'saveSnapshot' );
                                }
                            } );

                        });

                        editor.ui.addRichCombo( 'my-combo-2', {
                            label: 'Значения',
                            title: 'Значения',
                            toolbar: 'values,-1',

                            panel: {
                                css: [ CKEDITOR.skin.getPath( 'editor' ) ].concat( config.contentsCss ),
                                multiSelect: false,
                            },

                            init: function() {

                                this.startGroup( 'Информация о пользователе' );
                                this.add('{login}', 'Логин пользователя');
                                this.add('{avatar(высота:ширина)}', 'Аватар пользователя');
                                this.add('{avatar_link}', 'Прямая ссылка на аватар пользователя');
                                this.add('{profit}', 'Заработано пользователем');
                                this.add('{profit_to_inviter}', 'Заработано пользователем(партнерка)');
                                this.add('{link_visit}', 'Количество переходов по ссылке');
                                this.add('{count_ref}', 'Количество рефералов');
                                this.add('{balance}', 'Баланс пользователя');
                                this.add('{count_refs_level(уровень)}', 'Количество рефералов на уровне');
                                this.add('{count_refs_active_level(уровень)}', 'Количество рефералов на уровне');
                                this.add('{skype}', 'Skype');
                                this.add('{telegram}', 'Telegram');
                                this.add('{vk}', 'VK');
                                this.add('{facebook}', 'Facebook');
                                this.add('{twitter}', 'Twitter');
                                this.add('{instagram}', 'Instagram');
                                this.add('{lastname}', 'Фамилия');
                                this.add('{name}', 'Имя');
                                this.add('{fathername}', 'Отчество');
                                this.add('{telegram_referral_link}', 'Реферальная ссылка');
                                this.add('{telegram_bind_link}', 'Привязка телеграм');
                                this.add('{telegram_id}', 'ID в телеграм');
                                this.add('{telegram_login}', 'Логин в телеграм');
                                this.add('{percent_sum_user}', '10% от активаций пользователя');
                                this.add('{text_status}', 'Текст статуса');
                                this.add('{text_status}', 'Текст статуса');

                                this.startGroup( 'Информация о кураторе' );
                                this.add('{login_parent}', 'Логин куратора');
                                this.add('{avatar_parent(высота:ширина)}', 'Аватар куратора');
                                this.add('{avatar_parent_link}', 'Прямая ссылка на аватар куратора');
                                this.add('{profit_parent}', 'Заработано пользователем');
                                this.add('{profit_to_inviter_parent}', 'Заработано пользователем(партнерка)');
                                this.add('{skype_parent}', 'Skype');
                                this.add('{telegram_parent}', 'Telegram');
                                this.add('{vk_parent}', 'VK');
                                this.add('{facebook_parent}', 'Facebook');
                                this.add('{twitter_parent}', 'Twitter');
                                this.add('{instagram_parent}', 'Instagram');
                                this.add('{lastname_parent}', 'Фамилия');
                                this.add('{name_parent}', 'Имя');
                                this.add('{fathername_parent}', 'Отчество');
                                this.add('{text_status_parent}', 'Текст статуса куратора');

                                this.startGroup( 'Информация о сайте' );
                                this.add('{last_user_login}', 'Логин последнего зарегистрированного пользователь');
                                this.add('{user_count}', 'Количество пользователей');
                                this.add('{user_activate_count}', 'Количество активированных');
                                this.add('{all_cash}', 'Оборот проекта');


                            },

                            onClick: function( value ) {
                                editor.focus();
                                editor.fire( 'saveSnapshot' );

                                editor.insertHtml( value );

                                editor.fire( 'saveSnapshot' );
                            }
                        } );

                        editor.ui.addRichCombo( 'emojies', {
                            label: 'Эмодзи',
                            title: 'Эмодзи',
                            toolbar: 'values,-2',
                            className: 'cke_emoji-items',

                            panel: {
                                css: [ CKEDITOR.skin.getPath( 'editor' ) ].concat( config.contentsCss ).concat('.cke_panel_listItem {width: 25px; height: 25px; float:left} .cke_panel_listItem a {padding: 0; margin: 0; text-align: center;} .cke_ltr, .cke_panel_container, .cke_panel_frame, .cke_combopanel {width: 180px!important;}'),
                                multiSelect: false,
                            },

                            init: function () {
                                let emojies = '😀😃😄😁😆😅😂🤣😊😇🙂🙃😉😌😍😘😗😙😚😋😜😝😛🤑🤗🤓😎🤡🤠😏😒😞😔😟😕🙁😣😖😫😩😤😠😡😶😐😑😯😦😧😮😲😵😳😱😨😰😢😥🤤😭😓😪😴🙄🤔🤥😬🤐🤢🤧😷🤒🤕😈👿👹👺💩👻💀☠️👽👾🤖🎃😺😸😹😻😼😽🙀😿😾👐🙌👏🙏🤝👍👎👊✊🤛🤜🤞✌️🤘👌👈👉👆👇☝️✋🤚🖐🖖👋🤙💪🖕✍️🤳💅🖖💄💋👄👅👂👃👣👁👀🗣👤👥👶👩👱‍♀️👵👴👮👷🕵️👩‍⚕️👨‍⚕️🤴👸💁💁‍♂️🙅🙅‍♂️🤦‍♀️🙋‍♂️🙋🤷‍♀️🤷‍♂️👰🤵🤶🎅👨‍✈️🏃🏃‍♀️🕺👚👕👖👔👗👙👘👠👡👢👞👟👒🎩🎓👑⛑🎒👝👛👜💼👓🕶🌂☂️🐶🐱🐭🐹🐰🦊🐻🐼🐨🐯🦁🐮🐷🐽🐸🐵🙈🙉🙊🐒🐺🐗🐴🦄🐝🐛🦋🦑🦂🦀🐙🦐🐊🐾🎄🌲🌴🌳🌱🌿☘️🍀🌎🌍🌏🌕🌖🌗🌘🌑🌒🌓🌔🌚🌞🌙💫⭐️🌟✨⚡️🔥💥☄️☀️🌤⛅️🌥🌦🌈☁️🌧⛈🌩🌨☃️⛄️❄️🌬💨🌪🌫🌊💧💦☔️🍏🍎🍐🍊🍋🍌🍉🍇🍓🍈🍒🍑🍍🥝🥑🍅🍆🥒🥕🌽🌶🥔🍠🌰🥜🍯🥐🍞🥖🧀🥚🍳🥓🥞🍤🍗🍖🍕🌭🍔🍟🥙🌮🌯🥗🥘🍝🍜🍲🍥🍣🍱🏅🎖🥇🥈🥉🏆🎰🎲🎯🚗🚕🚙🚌🚎🏎🚓🚑🚒🚐🚚🚛🚲✈️🛫🛬🚀⌚️📱🖥💻📟⏰🕰⏱💎💰💷💶💴💵💸⚒🛠⚔️💊✉️📩🗒🗓📆📅📓📔📒📕📗📘📙❤️💛💚💙💜🖤💔✅❗️❕❓❔‼️⁉️🔞📵🚭🚱🚷🚯🚳⛔️🆘❌';
                                let split = emojies.split(/([\uD800-\uDBFF][\uDC00-\uDFFF])/);
                                let arr = [];
                                for (var i=0; i<split.length; i++) {
                                    let char = split[i]
                                    if (char !== "") {
                                        arr.push(char);
                                    }
                                }
                                arr.forEach((item) => {
                                    this.add( item, `<span class="emoji-item">${item}</span>` );
                                });

                            },

                            onClick: function( value ) {
                                editor.focus();
                                editor.fire( 'saveSnapshot' );

                                editor.insertHtml( value );

                                editor.fire( 'saveSnapshot' );
                            }
                        } );

                    }
                }
            } );

        }

        $('body').on('click', '.cke_combo_button', function() {
            setTimeout(function() {
                $('.cke_panel_frame').find('li').css({"width": "20%", "float": "left"});
            }, 1000);
        });
    }

    lineBreak()
    {
        $('body').on('change', '.line-break', function() {
            $.ajax({
                url: '/profile/admin/bot-settings/action/line-break',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    lineBreak: $(this).prop('checked'),
                    id: $(this).attr('data-id')
                }
            });
        });
    }
}