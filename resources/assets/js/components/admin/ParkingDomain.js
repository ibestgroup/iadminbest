import Notice from '../../Helpers/Notice';
import axios from "axios";
import Refresh from '../../Helpers/Refresher';
import AdEdit from "../../forms/AdEdit";
import datepicker from "bootstrap-datepicker";
import Validator from "../../Helpers/Validator";
import FileInput from "../../vendor/File";


export default class ParkingDomain {

    constructor () {
        this.submit();
    }


    submit() {
        $(document).on('submit', (e) => {

            e.preventDefault();

            axios.post(e.target.action, new FormData(e.target))
                .then((response) => {

                    $('.box').find('.overlay').remove();

                    let typeNotice = "success";
                    if (response.data.type) {
                        typeNotice = response.data.type;
                    }

                    new Notice(typeNotice, response.data.message);
                    new Refresh(location.pathname, '.js-parking-refresh', '.js-parking-refresh');

                }).catch((error) => {

                    let errors = error.response.data.errors;
                    console.log(error.response.data);

                    _.each(errors, (error) => {
                        $(e.target).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)

                    });

                    $('.box').find('.overlay').remove();
                });

            $(e.target).closest('.box').append(
                `<div class="overlay">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>`
            );
        });
    }

}