import Notice from '../../Helpers/Notice';
import axios from "axios";
import Refresh from '../../Helpers/Refresher';
import AdEdit from "../../forms/AdEdit";
import datepicker from "bootstrap-datepicker";
import Validator from "../../Helpers/Validator";
import FileInput from "../../vendor/File";
import 'jquery.cookie';


export default class SiteInfoEdit {

    constructor () {
        this.form = document.querySelector('.js-siteInfo');

        if ($.cookie('lang') === 'en') {
            this.loadLogo = 'Upload logo',
            this.loadAva = 'Upload avatar',
            this.loadImg = 'Upload image';
        } else {
            this.loadLogo = 'Загрузить логотип',
            this.loadAva = 'Загрузить аватар',
            this.loadImg = 'Загрузить изображение';
        }

        this.file = new FileInput($(this.form).find("[name=logo]"))
            .imageInput(this.loadLogo);

        this.file = new FileInput($(this.form).find("[name=no_image]"))
            .imageInput(this.loadAva);

        $('[name=image], [name=img], [name=image_current]').each(function(){
            new FileInput($(this))
                .imageInput(this.loadImg);
        });

        this.submit();
    }

    validator() {
        return new Validator(this.form, {
            name: {
                presence: {message: "^Вопрос не может быть пустым"},
                length: {
                    minimum: 3,
                    tooShort: "^Минимальная длина %{count} символов"
                }
            }
        }).validate();
    }

    submit() {
        $(document).on('submit', '.js-siteInfo', (e) => {

            e.preventDefault();

            if (this.validator()) {
                axios.post(this.form.getAttribute('action'), new FormData(this.form))
                    .then((response) => {

                        $('.box').find('.overlay').remove();

                        new Refresh(location.pathname, '.box', '.box');

                        new Notice("success", response.data.message);

                    }).catch((error) => {
                    let errors = error.response.data.errors;
                    console.log(error.response.data);

                    _.each(errors, (error) => {
                        $(this.form).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)
                    });

                    $('.box').find('.overlay').remove();
                });

                // $(e.target).closest('.box').append(
                //     `<div class="overlay">
                //         <i class="fa fa-refresh fa-spin"></i>
                //     </div>`
                // );
            }
        });
    }

}