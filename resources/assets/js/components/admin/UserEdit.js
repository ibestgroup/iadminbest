import Notice from '../../Helpers/Notice';
import axios from "axios";
import Refresh from '../../Helpers/Refresher';
import AdEdit from "../../forms/AdEdit";
import datepicker from "bootstrap-datepicker";
import Validator from "../../Helpers/Validator";


export default class UserEdit {

    constructor () {
        this.form = document.querySelector('.js-userEdit');
        this.submit();
    }

    submit() {
        $(document).on('submit', this.form, (e) => {

            e.preventDefault();

            axios.post(this.form.getAttribute('action'), new FormData(this.form))
                .then((response) => {

                    $('.box').find('.overlay').remove();

                    new Notice("success", response.data.message);

                }).catch((error) => {
                let errors = error.response.data.errors;
                console.log(error.response.data);

                _.each(errors, (error) => {
                    $(this.form).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)
                });

                $('.box').find('.overlay').remove();
            });

            $(e.target).closest('.box').append(
                `<div class="overlay">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>`
            );
        });
    }

}