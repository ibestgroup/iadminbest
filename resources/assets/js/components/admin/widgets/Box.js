import axios from "axios";
import React from "react";
import ReactDOM from "react-dom";

/**
 * Достаточно общий компонент - выводит карточку-обертку для виджета,
 * можно задать произвольный контент
 */
export default class Box extends React.Component{
  render(){
    let toAll = '';
    if (!!this.props.link) {
        toAll = <div className="box-footer text-center">
                    <a href={this.props.link} className="uppercase">Посмотреть всех</a>
                </div>;
    }
      // <div className="col-md-6">
    return(
      <div className="col-xs-6">
        <div className={'box box-'+this.props.color}>
          <div className="box-header with-border">
            <h3 className="box-title">{this.props.title}</h3>
            <div className="box-tools pull-right">
              <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus"></i>
              </button>
              <button type="button" className="btn btn-box-tool" data-widget="remove"><i className="fa fa-times"></i>
              </button>
            </div>
          </div>
          <div className="box-body no-padding">
            <ul className="users-list clearfix">
              {this.props.content}
            </ul>
          </div>
          {toAll}
        </div>
      </div>
    );
  }
}

