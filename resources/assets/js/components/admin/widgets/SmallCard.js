import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";

/**
 * Компонент маленькой цветной карточки
 */
export default class SmallCard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            // title: this.props.title,
            value: 0,
            link: null,
            hidden: true,
            color: 'bg-aqua', // bg-red, bg-yellow, bg-green
            current: ''
        }
        this.getWidgetData(this.props.id);
    }

    handleImageErrored() {
      this.setState({ picture: "laravel/storage/app/avatars/0.jpg" });
    }

    /**
     * Получение данных для виджета
     * @param  {int} id - идентификатор виджета в таблице profiles
     * @return {void}
     */
    getWidgetData(id){
      return axios.get(`/profile/${this.props.uid}/widgets/${id}`)
      .then(response => {
        const wdata = response.data;

        this.setState({
            // title: wdata.title,
            value: wdata.value,
            link: wdata.link,
            link_text: wdata.link_text,
            picture: wdata.picture,
            hidden: false,
            color: !!wdata.color ? wdata.color : 'bg-aqua',
            current: !!wdata.current ? wdata.current : '',
        })
      })
      .catch((error) => {
        console.log("error",error)
        this.setState({ hidden: true });
      });
    }

    render(){
        if (this.state.hidden === true) {
            return false;
        }
            // <div className="col-lg-6 col-xs-6">
        return(
            <div className="col-xs-6">
                <div className={"small-box " + this.state.color}>
                    <div className="inner">
                        <h3>
                          {(!!this.state.picture
                            ?<img src={this.state.picture} onError={this.handleImageErrored.bind(this)} />
                            : ''
                          )}
                          {this.state.value} <i className={this.state.current} aria-hidden="true"></i>
                        </h3>
                        <p> {this.props.title} </p>
                    </div>
                    <div className="icon">
                        <i className="ion ion-bag"></i>
                    </div>
                    {(!!this.state.link
                        ? <a href={this.state.link} className="small-box-footer"> {this.state.link_text} <i className="fa fa-arrow-circle-right"></i></a>
                        : <div className="small-box-footer">&nbsp;</div>
                    )}
                </div>
            </div>
        );
    }
}


