import axios from "axios";
import React from "react";
import ReactDOM from "react-dom";
import Box from "./Box.js";

/**
 * Компонент c отображением таблицы
 */
export default class TableCard extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hidden: true,
            title: this.props.title,
            // link: 'javascript:void(0)',
            color: 'success', // bg-red, bg-yellow, bg-green
            current: '',
        }
        this.getWidgetData(this.props.id);
    }

    // handleImageErrored() {
    //   this.setState({ avatar: "http://placekitten.com/90/90" });
    // }

   /**
     * Получение данных для виджета
     * @param  {int} id - идентификатор виджета в таблице profiles
     * @return {void}
     */
    getWidgetData(id){
      return axios.get(`/profile/${this.props.uid}/widgets/${id}`)
      .then(response => {
        const wdata = response.data;
        this.setState({
            rows: wdata.rows,
            headers: wdata.headers,
            link: wdata.link,
            // title: wdata.title,
            hidden: false,
            color: !!wdata.color ? wdata.color : 'success',
            current: !!wdata.current ? wdata.current : '',
        });
        if (!!wdata.color) {
          this.setState({
              color: wdata.color
          });
        }
        if (!!wdata.title && this.props.title === undefined) {
          this.setState({
              title: wdata.title
          });
        }
      })
      .catch((error) => {
        console.log("error",error)
        this.setState({ hidden: true });
      });
    }


    renderRows(rows, headers){
      if (!!rows === false || !!headers === false) {
        return false
      }

      let renderedRows = [];
      // формируем основную таблицу - td
      for (var i = 0; i < rows.length; i++) {
        const rowKeys = Object.keys(rows[i]);



        let tds = [];
        for (var n = 0; n <= rowKeys.length - 1; n++) {
          const curCol = rows[i][rowKeys[n]];


          // если нам приходит массив из link,title - клеим ссылки
          if (curCol !== null && curCol.title !== undefined) {

            let classes = '';
            if (!!curCol.classes) {
                classes = curCol.classes;
            }
            tds.push(
                <td key={n}><a className={classes} href={curCol.link}>{curCol.title}</a></td>
              );
          }
          else{ // иначе - просто пишем данные

            var current = (rowKeys[n] == 'amount')?<i className={this.state.current} aria-hidden="true"></i>:'';
            tds.push(
                <td key={n}>{curCol}{current}</td>
              );
          }
        }
            // console.log(tds;

        renderedRows.push(
          <tr key={i}>{tds}</tr>
        );
      }


      // формируем голову - th
      let renderedHeads = [];
      for (var k = 0; k <= headers.length - 1; k++) {
        renderedHeads.push(<th key={k}>{headers[k]}</th>);
      }

          // console.log(data);

      const wholeTable = <div className="table-responsive">
                <table className="table no-margin">
                  <thead>
                  <tr>
                    {renderedHeads}
                  </tr>
                  </thead>
                  <tbody>
                    {renderedRows}
                  </tbody>
                </table>
              </div> ;

      return wholeTable;
    }


  render(){
    const table = this.renderRows(this.state.rows, this.state.headers);

    return(
      <Box
        color={this.state.color}
        title={this.state.title+' '+(!!this.state.total ? this.state.total : '')}
        // total={this.state.total}
        link={this.state.link}
        content={table}
      />
    )
  }
}


