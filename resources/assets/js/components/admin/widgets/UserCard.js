import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";


/**
 * Компонент с аватаркой
 */
export default class UserCard extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hidden: true,
            // color: 'bg-aqua' // bg-red, bg-yellow, bg-green
            current: '',
        }
        this.getWidgetData(this.props.id);
    }

    handleImageErrored() {
      // this.setState({ avatar: "//placekitten.com/90/90" });
      this.setState({ avatar: "/laravel/storage/app/avatars/0.jpg" });
    }
   /**
     * Получение данных для виджета
     * @param  {int} id - идентификатор виджета в таблице profiles
     * @return {void}
     */
    getWidgetData(id){
      return axios.get(`/profile/${this.props.uid}/widgets/${id}`)
      .then(response => {
        const wdata = response.data;
        this.setState({
            avatar: wdata.avatar,
            name: wdata.name,
            hidden: false,
            marketings: wdata.marketings,
            friends: wdata.friends,
            payouts: wdata.payouts,
            // color: !!wdata.color ? wdata.color : 'bg-aqua'
            current: !!wdata.current ? wdata.current : '',
            friendButton: (this.props.uid != id)?`кнопка друга`:'',
            idUser: this.props.uid,
        })
      })
      .catch((error) => {
        console.log("error",error)
        this.setState({ hidden: true });
      });
    }


  render(){
    return(
      <div className="col-xs-6">
          <div className="box box-widget widget-user">
            <div className="widget-user-header bg-aqua-active">
              <h3 className="widget-user-username">{this.state.name}</h3>
              <h5 className="widget-user-desc"></h5>
            </div>
            <div className="widget-user-image">
              <img className="img-circle" src={this.state.avatar} onError={this.handleImageErrored.bind(this)} width="90px" height="90px"/>


            </div>
            <div className="box-footer">
              <div className="row">
                <div className="col-sm-4 border-right">
                  <div className="description-block">
                    <h5 className="description-header">{this.state.friends}</h5>
                    <span className="description-text">Друзей</span>
                  </div>
                </div>
                <div className="col-sm-4 border-right">
                  <div className="description-block">
                    <h5 className="description-header">{this.state.marketings}</h5>
                    <span className="description-text">активировано тарифов</span>
                  </div>
                </div>
                <div className="col-sm-4">
                  <div className="description-block">
                    <h5 className="description-header">{this.state.payouts} <i className={this.state.current} aria-hidden="true"></i></h5>
                    <span className="description-text">заработано</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    )
  }
}
