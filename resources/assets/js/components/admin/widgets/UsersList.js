import axios from "axios";
import React from "react";
import ReactDOM from "react-dom";
import Box from "./Box.js";

/**
 * Компонент отображением списка пользователей
 */
export default class UsersList extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            hidden: true,
            link: 'javascript:void(0)',
            color: 'success' // bg-red, bg-yellow, bg-green
        }
        this.getWidgetData(this.props.id);
    }

    handleImageErrored() {
      this.setState({ avatar: "/laravel/storage/app/avatars/0.jpg" });
    }
   /**
     * Получение данных для виджета
     * @param  {int} id - идентификатор виджета в таблице profiles
     * @return {void}
     */
    getWidgetData(id){
      return axios.get(`/profile/${this.props.uid}/widgets/${id}`)
      .then(response => {
        const wdata = response.data;
        this.setState({
            users: wdata.users,
            link: wdata.link,
            total: wdata.total,
            hidden: false,
            color: !!wdata.color ? wdata.color : 'success'
        })
        if (!!wdata.color) {
          this.setState({
              color: wdata.color
          })
        }
      })
      .catch((error) => {
        console.log("error",error)
        this.setState({ hidden: true });
      });
    }


    renderUsers(users){
      if (!!users === false ) {
        return false
      }

      let renderedUsers = [];
      for (var i = users.length - 1; i >= 0; i--) {
        renderedUsers.push(
          <UserListCard
            avatar={users[i].avatar}
            login={users[i].login}
            lastActive={users[i].last_active}
            id={users[i].id}
            key={users[i].id}
          />
        );
      }
      return renderedUsers;
    }


  render(){
    const users = this.renderUsers(this.state.users);

    return(
      <Box
        color={this.state.color}
        title={this.props.title+' '+this.state.total}
        // total={this.state.total}
        link={this.state.link}
        content={users}
      />
    )
  }
}

export class UserListCard extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      avatar: this.props.avatar
    };
  }

  handleImageErrored() {
    const randKitten = Math.floor(Math.random() * (16 - 1) + 1);
    this.setState({ avatar: "laravel/storage/app/avatars/0.jpg" });
  }

  render(){
    return(
      <li>
        <img src={this.props.avatar} onError={this.handleImageErrored.bind(this)} width="90px" height="90px"/>
        <a className="users-list-name" href={"/profile/"+this.props.id}>{this.props.login}</a>
        <span className="users-list-date">{this.props.lastActive}</span>
      </li>
    );
  }
}

