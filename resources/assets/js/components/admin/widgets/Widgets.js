import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";

import Masonry from 'react-masonry-component';

import SmallCard from "./SmallCard.js";
import UserCard from "./UserCard.js";
import UsersList from "./UsersList.js";
import TableCard from "./TableCard.js";
// import Box from "./Box.js";

const masonryOptions = {
    transitionDuration: 0
};


/**
 * Компонент, выводящий все виджеты пользователя
 */
export default class Widgets extends React.Component {


  currentTime() {
    return new Date().toLocaleTimeString();
  };

  constructor(props){
    super(props);

    this.state = {
        widgets: null,
        uid: document.getElementById('profile-id').value, // user id профиля
        time: this.currentTime(),
        page: $('.mason-gallery').html(),
        iter: 0,
    }

    this.getWidgetsList();
  }

    sleep(milliseconds) {
        let start = new Date().getTime();
        for (let i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
                break;
            }
        }
    }

  componentDidMount() {

          this.interval = setInterval(() => {
              this.setState({time: this.currentTime()});
              
              // if (this.state.page == $('.mason-gallery').html()) {
              //       clearInterval(this.interval);
              //       $('.overlay').remove();
              // } else {
              //     this.state.page = $('.mason-gallery').html()
              // }
          }, 1500);
  }

  componentWillUnMount(){
      clearInterval(this.interval);
  }

  /**
   * Получение списка виджетов и пользовательских настроек для них
   * @return {void}
   */
  getWidgetsList(){
      return axios.get(`/profile/${this.state.uid}/widgets`)
          .then(response => {
              this.setState({widgets: response.data})
          })
          .catch((error) => {
              console.log("error",error)
          });
  }


  /**
   * Построение списка виджетов на основе полученных данных
   * @return {object} - коллекция виджетов
   */
  fillWidgetsList(){
    let widgets = [];
    const cTypes = {
        'UserCard': UserCard,
        'SmallCard': SmallCard,
        'TableCard': TableCard,
        'UsersList': UsersList,
    };
    if (this.state.widgets !== null) {
        for (var i = this.state.widgets.length - 1; i >= 0; i--) {
            const widget = this.state.widgets[i];
            const Type = cTypes[widget.ctype];

            // TODO: не обрабатывается дублирование значения сортировки
            // если в таблице есть два элемента с sort = 2, отобразит один
            if (!!Type) {
                widgets[widget.sort] =
                    <Type
                        onUpdate={() => {this.handleChildUpdate()}}
                        key={widget.id}
                        id={widget.id}
                        title={widget.arr_values.name_block}
                        color={widget.color}
                        uid={this.state.uid}
                    />;
            }
        }
    }
    return widgets;
  }

  render() {


        const widgets = this.fillWidgetsList();
        // Короче такое дело, из-за масонри я набыдлокодил ежесекундное обновление
        // Поэтому, дабы оправдать сей ебучий костыль - TODO: сделать часики :D
        //     {this.state.time}

        return (
            <div>
            <Masonry
                  className={'mason-gallery'}
                  elementType={'div'}
                  disableImagesLoaded={false}
                  updateOnEachImageLoad={false}
              >
                { widgets }
            </Masonry>
          </div>
        );

  }
}


