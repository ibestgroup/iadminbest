import axios from "axios";
import Notice from "../Helpers/Notice";
import Refresh from '../Helpers/Refresher';

export class NewsDelete {
    constructor() {
        $(document).on('click', '.js-delete-news', (e) => {
            let item = $(e.target),
                id = item.closest('li').data('id');

            this.delete(item, id);
            new Refresh();
        });
    }

    delete(item, id) {
        axios.post(item.data('action'), {id: id})
            .then((response) => {

                new Notice("warning", response.data.message);
                new Refresh();
            }).catch((error) => {

            new Notice("error", error.data.message);
        });
    }
}

export class FaqDelete {
    constructor() {
        $(document).on('click', '.js-delete-faq', (e) => {
            let item = $(e.target),
                id = item.closest('.box').data('id');

            this.delete(item, id);
        });
    }

    delete(item, id) {
        axios.post(item.data('action'), {id: id, headers: {
                '_token': $('meta[name="csrf-token"]').attr('content')
            }})
            .then((response) => {
                console.log(12333);
                new Notice("warning", response.data.message);

                new Refresh(location.pathname, '.refresh', '.refresh');
            }).catch((error) => {

            new Notice("error", error.data.message);
        });
    }
}

export class PromoDelete {
    constructor() {
        $(document).on('click', '.js-delete-promo', (e) => {
            let item = $(e.target),
                id = item.closest('.box').data('id');

            this.delete(item, id);
        });
    }

    delete(item, id) {
        axios.post(item.data('action'), {id: id})
            .then((response) => {

                new Notice("warning", response.data.message);

                new Refresh();
            }).catch((error) => {

            new Notice("error", error.data.message);
        });
    }
}