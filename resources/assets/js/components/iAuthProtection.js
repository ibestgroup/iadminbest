import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';
import AdEdit from "../forms/AdEdit";
import datepicker from "bootstrap-datepicker";
import Validator from "../Helpers/Validator";
// import pincodeInput from "bootstrap-pincode-input";
import "bootstrap-pincode-input";
import "jquery-pinlogin";


export default class iAuthProtection {

    constructor (start = true) {
        if (start) {
            $(document).ready(function () {

                $('input[name=iauth_code]').pincodeInput({inputs: 6, placeholders: "0 0 0 0 0 0"});

                $('body').on('click', '.iauth-close', function () {
                    $(this).closest('.iauth-confirm').hide(100);
                });

                // show form confirm or getcode
                $('body').on('click', '.iauth-toggle', function() {

                    $('.iauth-for-content').hide(200);

                    let messenger = $(this).closest('.iauth-toggle').data('mess'),
                        iAuth = new iAuthProtection(false);

                    iAuth.toggleForm(messenger);
                });

                // show button confirm
                $('body').on('focus', '.iauth-field-confirm input', function () {
                    let messenger = $(this).closest('.iauth-field-confirm').data('mess'),
                        iAuth = new iAuthProtection(false),
                        values,
                        btn;

                    values = iAuth.valueMess(messenger);

                    if (values['status'] != 1) {
                        btn = `
                            <div class="iauth-show-btn" style="display: none;">
                                Подтвердите ${values['string_mess']}, чтобы защитить аккаунт
                                <div class="js-${messenger}-getcode iauth-btn iauth-confirm-btn iauth-get-code mt-10" data-toggle="modal" data-target="#${messenger}-confirm" data-mess="${messenger}">
                                    <span>ПОДТВЕРДИТЬ</span>
                                </div>
                            </div>
                        `;

                        $(`.${messenger}-for-content`).html(btn);
                        $('.iauth-show-btn').show(200);
                    }
                });

                //show modal confirm
                $('body').on('click', '.iauth-confirm-btn', function () {
                    let messenger = $(this).data('mess'),
                        iAuth = new iAuthProtection(false),
                        loginInput = $(this).closest('.iauth-field-confirm').find(`input[name="iauth_${messenger}"]`).val();

                    if (iAuth.checkErrorLogin(loginInput, messenger)) {
                        $(`#${messenger}-confirm`).remove();
                        return false;
                    }

                    let flagPair;

                    $.ajax({
                        type: `POST`,
                        url: `/i-auth-check-pair`,
                        async: false,
                        data: {
                            messenger: messenger,
                            login: loginInput
                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data.message) {
                                flagPair = true;
                                new Notice('error', data.message);
                                $('body').find('#teleg-confirm, #vk-confirm').remove();
                            }
                        }
                    });


                    $.ajax({
                        type: `POST`,
                        url: `/i-auth-add`,
                        data: {
                            messenger: messenger,
                            login: loginInput
                        },
                        dataType: 'json',
                    });

                    if (!flagPair) {
                        iAuth.showModal('confirm', messenger, loginInput);
                    }

                });

                $('body').on('click', '.iauth-checkbox', function () {
                    let el = $(this).find('.iauth-checkbox-icon');
                    el.toggleClass('checked');
                    if (el.hasClass('checked')) {
                        $(this).find('input').val(1);
                    } else {
                        $(this).find('input').val(0);
                    }
                });

                // show iauth module when focus focus on input
                $('.iauth-confirm').closest('form').find('input').on('focus', function () {
                    $(this).closest('form').find('.iauth-confirm').show(100);
                });


                $('body').on('click', '.setting-mess', function () {
                    let messenger = $(this).data('mess'),
                        type = $(this).data('type-modal'),
                        iAuth = new iAuthProtection(false),
                        values;

                    iAuth.showModal(type, messenger);
                    values = iAuth.valueMess(messenger);

                    $.ajax({
                        type: `POST`,
                        url: `/i-auth-sendcode`,
                        dataType: 'json',
                        data: {
                            site_id: values['site_id'],
                            user_id: values['user_id'],
                            messenger: messenger,
                        },
                        success: function (data) {

                        }
                    });

                });

                $('body').on('click', '.iauth-get-code:not([data-toggle=modal])', function () {
                    let messenger = $(this).data('mess'),
                        iAuth = new iAuthProtection(false),
                        values;

                    values = iAuth.valueMess(messenger);
                    $.ajax({
                        type: `POST`,
                        url: `/i-auth-sendcode`,
                        dataType: 'json',
                        data: {
                            site_id: values['site_id'],
                            user_id: values['user_id'],
                            messenger: messenger,
                        },
                        success: function (data) {

                        }
                    });
                    $(this).text('КОД ОТПРАВЛЕН').append('<img src="https://i.ibb.co/tDN2Hfd/checked-gif.gif" style="width: 50px; height: 50px;">');

                });

            });
        }
    }

    toggleForm(messenger) {
        let field, values;

        values = this.valueMess(messenger);

        // $('body').find('.iauth-for-content').hide(200);
        // $('body').find('.iauth-toggle-field').hide(200);
        if (values['status'] == 1) {
            field = `
                <div class="col-lg-12 iauth-${messenger}-field iauth-field-get-code iauth-toggle-field" data-mess="${messenger}" style="display: none;">
                    <div class="form-group">
                        <div class="iauth-btn iauth-get-code mt-10" data-mess="${messenger}">
                            <span class="iauth-get-code" data-mess="${messenger}">ПОЛУЧИТЬ КОД</span>
                        </div>
                        <input type="hidden" name="messenger" class="" value="${messenger}">
                        <input type="hidden" name="iauth_code" class="" placeholder="Введите код">
                        <div class="iauth-pinlogin"></div>
                        
                        <div class="js-${messenger}-unlink setting-mess iauth-get-code mt-10 col-lg-6" data-type-modal="unlink" data-mess="${messenger}" data-toggle="modal" data-target="#${messenger}-unlink">
                            <span style="cursor: pointer; color: #49d8b2;" class="pull-left">Отключить ${values['string_mess']}</span>
                        </div>
                        <div class="js-${messenger}-notify setting-mess iauth-get-code mt-10 col-lg-6" data-type-modal="notify" data-mess="${messenger}" data-toggle="modal" data-target="#${messenger}-notify">
                            <span style="cursor: pointer; color: #49d8b2;" class="pull-right">Настроить оповещения ${values['string_mess']}</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                    `;
        } else {
            field = `
                        <div class="col-lg-12 iauth-${messenger}-field iauth-field-confirm iauth-toggle-field" data-mess="${messenger}" style="display: none;">
                            <div class="form-group" style="background: transparent">
                                <input style="background: transparent;" type="text" class="form-control iauth-confirm-input"
                                       name="iauth_${messenger}" placeholder="Введите ${values['account']}">
                            </div>
                            <div class="${messenger}-for-content" style="border-bottom: 1px solid #efefef; padding: 2px;">
    
                            </div>
                        </div>`
        }

        $('.iauth-for-content').html(field);
        $('input[name=iauth_code]').pincodeInput({inputs:6, placeholders: "0 0 0 0 0 0"});
        $('.iauth-toggle-field').show(200);
        $('.iauth-for-content').show(200);
    }

    valueMess(messenger) {
        let result = [], userInfo;

        $.ajax({
            type: `GET`,
            url: `/get-user-info`,
            dataType: 'json',
            async: false,
            success: function (data) {
                userInfo = data;
            }
        });

        if (messenger === 'teleg') {
            result['status'] = userInfo.teleg_status;
            result['login'] = userInfo.teleg_login;
            result['link'] = userInfo.teleg_login;
            result['notice'] = userInfo.notice;
            result['system_notice'] = userInfo.system_notice;
            result['string_mess'] = 'Telegram';
            result['messenger'] = 'teleg';
            result['account'] = 'Telegram';
            result['message_modal_confirm'] = `
                <p>
                    Вы можете поставить защиту на свой аккаунт с помощью интегрированного телеграм бота.
                    Даже если злоумышленники получат доступ к вашему аккаунту, они не смогут вывести
                    ваши средства
                    если они не получили доступ к вашему Telegram аккаунту. Каждый раз, когда вы
                    захотите вывести
                    средства со своего аккаунта, вам будет приходить шестизначный код с подтверждением.
                </p>
                <p>
                    Для того чтобы подтвердить ваш телеграм и получать сообщения откройте бота в своем
                    телеграм.
                </p>
            `;
            result['link_bot'] = `
                <div class="iauth-btn mt-10"
                     onClick="window.open('https://t.me/iAuth_bot', '_blank');">
                    <span class="">ОТКРЫТЬ БОТА iAuth</span>
                </div>
            `;

        } else if (messenger === 'vk') {
            result['status'] = userInfo.vk_status;
            result['login'] = userInfo.vk_id;
            result['link'] = userInfo.vk_link;
            result['notice'] = userInfo.vk_notice;
            result['system_notice'] = userInfo.vk_system_notice;
            result['string_mess'] = 'Вконтакте';
            result['messenger'] = 'vk';
            result['account'] = 'Ссылка на страницу ВКонтакте';
            result['message_modal_confirm'] = `
                <p>
                    Вы можете поставить защиту на свой аккаунт с помощью интегрированного бота ВКонтакте.
                    Даже если злоумышленники получат доступ к вашему аккаунту, они не смогут вывести ваши средства
                    если они не получили доступ к вашему аккаунту Вконтакте. Каждый раз, когда вы захотите вывести
                    средства со своего аккаунта, вам будет приходить шестизначный код с подтверждением.
                </p>
                <p>
                    Для того чтобы подтвердить ваш VK и получать сообщения откройте группу iAuth и справа нажмите
                    "Написать сообщение" и .
                </p>
            `;
            result['link_bot'] = `
                <div class="iauth-btn mt-10" onClick="window.open('https://vk.com/iauthprotection', '_blank');">
                    <span class="">ОТКРЫТЬ ГРУППУ iAuth</span>
                </div>`;
        }

        result['site_id'] = userInfo.site_id;
        result['user_id'] = userInfo.user_id;

        return result;
    }

    checkErrorLogin(login, messenger) {
        let errMess = 0;
        if (login.length == 0) {
            new Notice('error', 'Строка не может быть пустой');
            errMess = 1;
        }

        if (login[0] != '@' && messenger === 'teleg') {
            new Notice('error', 'Должно начинаться с символа @');
            errMess = 1;
        }

        // let res = login.match('^https\:\/\/vk\.com\/');
        if (login.match('^https\:\/\/vk\.com\/.+') == null && messenger === 'vk') {
            new Notice('error', 'Неправильная ссылка. Должна начинаться на https://vk.com/');
            errMess = 1;
        }

        if (login.length < 3) {
            new Notice('error', 'Минимальная длина 3 символа');
            errMess = 1;
        }

        if (errMess) return true;
    }

    showModal(type, messenger, loginInput = '') {
        let values = this.valueMess(messenger), modal, startModal, endModal;

        let checkModal = $(`#${messenger}-confirm, #${messenger}-unlink, #${messenger}-notify`);
        if (checkModal.length) {
            checkModal.remove();
        }

        startModal = `
            <div class="modal fade" id="${messenger}-${type}">
                <div class="modal-dialog">
                    <div class="modal-content iauth-protected">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span></button>
                            <h3 class="ta-center"><span class="iauth-color">iAuth</span> защита</h3>
    
                        </div>
                        <div class="modal-body">
                            <div class="col-lg-12">`;
        endModal = `
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="iauth-btn button-close" data-dismiss="modal"><span>ЗАКРЫТЬ</span></button>
                    </div>
                </div>
            </div>
        </div>
        `;

        if (type === 'confirm') {
            modal = `
                <h3>Ваш ${values['string_mess']} <span class="iauth-login" style="font-weight: bold">${loginInput}</span></h3>
                ${values['message_modal_confirm']}
                ${values['link_bot']}
                <form action="/i-auth-confirm" method="post" id="${messenger}-code" class="js-store">
                    <input type="hidden" name="messenger" value="${messenger}">
                        <input class="form-control" type="hidden" name="iauth_code"
                               placeholder="Введите код">
                            <div id="pinlogin-confirm" class="iauth-pinlogin"></div>
                            <button class="iauth-btn mt-10">
                                <span class="">ПОДКЛЮЧИТЬ</span>
                            </button>
            `;
        } else if (type === 'unlink') {

            modal = `
                <h3>Ваш ${values['string_mess']} <span class="${messenger}-login" style="font-weight: bold">${values.link}</span></h3>
                <p>
                    Вам пришел код на ваш ${values['string_mess']} <b>${values.link}</b>. Для того чтобы отвязать ${values['string_mess']}, введите код.
                </p>
                <p><b>Ваш аккаунт будет отвязан от всех аккаунтов одновременно.</b></p>
                <p>
                    Если код не пришел автоматически, откройте бота и введите команду <b>/start</b>.
                </p>
                ${values['link_bot']}
                </div>
                <div class="col-lg-12 mt-10">
                    <form action="/i-auth-unlink" method="post" id="${messenger}-code" class="js-store">
                        <input type="hidden" name="messenger" value="${messenger}">
                        <input class="form-control" type="hidden" name="iauth_code" placeholder="Введите код">
                        <button class="iauth-btn mt-10">
                            <span class="">ОТКЛЮЧИТЬ</span>
                        </button>
                `;

        } else if (type === 'notify') {
            modal = `
                    <h3>Ваш ${values['string_mess']} <span class="${messenger}-login" style="font-weight: bold">${values['login']}</span></h3>
                    <p>
                        Вам пришел код на ваш ${values['string_mess']} <b>${values['login']}</b>. Для того чтобы изменить настройки оповещений.
                    </p>
                    <p>
                        Если код не пришел автоматически, откройте бота и введите команду <b>/start</b>.
                    </p>
                    ${values['link_bot']}
                </div>
                <div class="col-lg-12 mt-10">
                    <form action="/i-auth-notice" method="post" id="${messenger}-code" class="js-store">
                        <input type="hidden" name="messenger" value="${messenger}">
                        <input class="form-control" type="hidden" name="iauth_code" placeholder="Введите код">
                        
                        <div style="line-height: 50px; margin-top: 10px; font-size: 18px;">
                            <div class="iauth-checkbox" style="float: left;">
                                <input type="hidden" name="system_notice" value="${(values['system_notice']) ? '1' : ''}">
                                <div class="${(values['system_notice']) ? 'checked' : ''} iauth-checkbox-icon"></div>
                            </div>
                            <span style="padding-left: 10px;">Системные оповещения</span>
                            <div class="clearfix"></div>
                        </div>
                        <div style="line-height: 50px; margin-top: 10px; font-size: 18px;">
                            <div class="iauth-checkbox" style="float: left;">
                                <input type="hidden" name="notice" value="${(values['notice']) ? '1' : ''}">
                                <div class="${(values['notice']) ? 'checked' : ''} iauth-checkbox-icon"></div>
                            </div>
                            <span style="padding-left: 10px;">Новости</span>
                            <div class="clearfix"></div>
                        </div>
                        <button class="iauth-btn mt-10">
                            <span class="">СОХРАНИТЬ</span>
                        </button>
            `;
        }

        modal = startModal + modal + endModal;

        $('body').append(modal);
        $('input[name=iauth_code]').pincodeInput({inputs: 6, placeholders: "0 0 0 0 0 0"});
    }
}