import axios from 'axios';
import * as d3 from "d3";

export default class Tree {
    constructor(root) {
        this.root = $(root);

        this.initialize();
    }

    initialize () {
        this.cacheNodes();
        this.bindEvents();
    }

    get(container) {
        this.container = container.find('.js-tree-container').get(0);
        return axios.get(container.data('path'));
    }

    cacheNodes () {}

    bindEvents() {}
}