import FileInput from '../vendor/File';
import axios from "axios";
import _ from "lodash";
import Notice from "../Helpers/Notice";
import Refresh from "../Helpers/Refresher";

export default class Birds {
    constructor() {

        $('[name=bird_image]').each(function(){
            new FileInput($(this))
                .imageInput('Загрузить изображение');
        });

        $('body').on('change', '[name=type]', function() {
            let type = $(this).children('option:selected').data('end'),
                eggName = $(this).children('option:selected').data('egg'),
                eggId = $(this).data('id-egg'),
                endText = $(`.end-text[data-id-egg=${eggId}]`),
                profitEgg = $(`.profit-egg[data-id-egg=${eggId}]`);
            profitEgg.text(eggName);
            if (type == 1) {
                endText.text('Срок жизни');
            } else {
                endText.text('Каждые сколько секунд');
            }
        });

        this.range();
        this.nowActivate();
        this.end();
        // this.submit();
    }

    // submit() {
    //     $(document).on('submit', '.js-birds', (event) => {
    //         event.preventDefault();
    //
    //         let current = $(event.target);
    //
    //         axios.post(event.target.action, new FormData(event.target))
    //             .then((response) => {
    //                 current.closest('.box').find('.overlay').remove();
    //                 current.find('.alert').remove();
    //                 current.find('.box-body').prepend(`<p class="alert alert-success">${response.data.message}</p>`);
    //
    //                 let typeNotice = "success";
    //                 if (response.data.type) {
    //                     typeNotice = response.data.type;
    //                 }
    //
    //                 new Notice(typeNotice, response.data.message);
    //                 new Refresh(location.pathname, '.box-body', '.box-body');
    //                 setTimeout(function(){
    //                     $('[name=bird_image]').each(function(){
    //                     new FileInput($(this))
    //                         .imageInput('Загрузить изображение');
    //                 });
    //                 }, 1000);
    //             }).catch((error) => {
    //             let errors = error.response.data.errors;
    //             current.find('.alert').remove();
    //
    //             _.each(errors, (error) => {
    //                 current.find('.box-body').prepend(`<p class="alert alert-danger">${error}</p>`)
    //             });
    //
    //             current.closest('.box').find('.overlay').remove();
    //             });
    //
    //         current.closest('.box').append(
    //             `<div class="overlay">
    //                     <i class="fa fa-refresh fa-spin"></i>
    //                 </div>`
    //         );
    //     })
    // }

    range() {
        $(document).ready(function() {
            $('body').on('input', 'select[name=range]', function () {
                let parentForm = $(this).parents('form');
                if ($(this).val() == 0) {

                    parentForm.find('.range-price').html('Цена');
                    parentForm.find('input[name=price]').attr('placeholder','Цена');
                    parentForm.find('.wrap-range-max-price').hide();
                    parentForm.find('.type-profit').html('сумма');
                } else {
                    parentForm.find('.range-price').html('Нижняя граница цены');
                    parentForm.find('input[name=price]').attr('placeholder','Цена (от)');
                    parentForm.find('.range-max-price').html('Верхняя граница цены');
                    parentForm.find('.wrap-range-max-price').show();
                    parentForm.find('.type-profit').html('процент');
                }
            })
        });
    }

    nowActivate() {
        $(document).ready(function() {
            $('body').on('input', '.select-now-activate', function () {
                let parentForm = $(this).parents('form');
                if ($(this).val() == 0) {
                    parentForm.find('.now-activate').hide();
                } else {
                    parentForm.find('.now-activate').show();
                }
            })
        });
    }

    end() {
        $(document).ready(function() {
            $('body').on('input', 'select[name=end]', function () {
                let parentForm = $(this).parents('form');
                if ($(this).val() == 0) {
                    parentForm.find('.only-end').hide();
                    parentForm.find('.end-text').html('Каждые сколько секунд');
                } else {
                    parentForm.find('.only-end').show();
                    parentForm.find('.end-text').html('Срок жизни (секунд)');
                }
            })
        });
    }
}
