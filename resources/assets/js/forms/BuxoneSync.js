import FileInput from '../vendor/File';
import axios from "axios";
import _ from "lodash";
import Notice from "../Helpers/Notice";
import Refresh from "../Helpers/Refresher";

export default class BuxoneSync {
    constructor() {
        this.form = $('.js-buxone');

        this.submit();
    }

    submit() {
        $(document).on('submit', '.js-buxone', (event) => {
            event.preventDefault();

            let current = $(event.target);

            axios.post(event.target.action, new FormData(event.target))
                .then((response) => {
                    current.closest('.box').find('.overlay').remove();
                    current.find('.alert').remove();
                    current.find('.box-body').prepend(`<p class="alert alert-success">${response.data.message}</p>`);

                    let typeNotice = "success";
                    if (response.data.type) {
                        typeNotice = response.data.type;
                    }

                    new Notice(typeNotice, response.data.message);

                    if (typeNotice == 'success') {
                        new Refresh(location.pathname, '.row', '.row');
                    }

                }).catch((error) => {
                let errors = error.response.data.errors;
                current.find('.alert').remove();

                _.each(errors, (error) => {
                    current.find('.box-body').prepend(`<p class="alert alert-danger">${error}</p>`)
                });

                current.closest('.box').find('.overlay').remove();
            });

            current.closest('.box').append(
                `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
            );
        })
    }
}
