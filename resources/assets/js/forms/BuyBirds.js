import FileInput from '../vendor/File';
import axios from "axios";
import _ from "lodash";
import Notice from "../Helpers/Notice";
import Refresh from "../Helpers/Refresher";

export default class BuyBirds {
    constructor() {
        this.form = $('.js-buyBirds');

        this.submit();
    }

    submit() {
        $(document).on('submit', '.js-buyBirds', (event) => {
            event.preventDefault();
            console.log(event);

            let current = $(event.target);

            axios.post(event.target.action, new FormData(event.target))
                .then((response) => {
                    current.closest('.box').find('.overlay').remove();
                    current.find('.alert').remove();
                    current.find('.box-body').prepend(`<p class="alert alert-success">${response.data.message}</p>`)

                    let typeNotice = "success";
                    if (response.data.type) {
                        typeNotice = response.data.type;
                    }

                    new Notice(typeNotice, response.data.message);

                    new Refresh(location.href, '.js-myStore', '.js-myStore');
                    // new Refresh(location.href, '.balance-for-buy', '.balance-for-buy', {'type': 'small'});
                    setTimeout(function(){
                        $('[name=bird_image]').each(function(){
                        new FileInput($(this))
                            .imageInput('Загрузить изображение');
                    });
                    }, 1000);
                }).catch((error) => {
                let errors = error.response.data.errors;
                current.find('.alert').remove();

                _.each(errors, (error) => {
                    current.find('.box-body').prepend(`<p class="alert alert-danger">${error}</p>`)
                });

                current.closest('.box').find('.overlay').remove();
                });

            current.closest('.box').append(
                `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
            );
        })
    }
}
