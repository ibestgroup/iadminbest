import Validator from '../Helpers/Validator';
import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';

export default class CashoutOrder {
    constructor() {
        this.form = document.querySelector('.js-cashoutOrder');
        this.cashout = document.getElementsByClassName('js-cashoutOrder');
        this.submit();
    }

    validator(form) {
        return new Validator(form, {
            amount: {
                presence: {message: "^Введите сумму"}
            }
        }).validate();

    }

    submit() {
        $(document).on('submit', '.js-cashoutOrder', (e) => {

            let i = e.target.dataset.idOrder,
                res = document.querySelector('#order-'+i);

            e.preventDefault();

            if (this.validator(res)) {
                axios.post(res.getAttribute('action'), new FormData(res))
                    .then((response) => {

                        $('.box').find('.overlay').remove();

                        if (response.data.success)
                            new Notice("success", response.data.message);
                        else
                            new Notice("error", response.data.error);

                        if (response.data.amount)
                            $('#balance-'+i).html(`<b>Баланс тарифа ${response.data.amount}</b>`);

                    }).catch((error) => {
                    let errors = error.response.data.error;


                    _.each(errors, (error) => {
                        // $(this.form).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`);
                    });

                    $('.box').find('.overlay').remove();
                });

                $(e.target).closest('.box').append(
                    `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
                );
            }
        });
    }
}