import FileInput from '../vendor/File';
import axios from "axios";
import _ from "lodash";
import Notice from "../Helpers/Notice";
import Refresh from "../Helpers/Refresher";

export default class CollectEgg {
    constructor() {
        this.form = $('.js-collectEgg');

        // $('.timer-remind').each(function () {
        //     let time = Number($(this).html());
        //     let interval = setInterval(() => {
        //         time = $(this).html()-1;
        //         if (time < 0) {
        //             time = 'Срок жизни истек'
        //             clearInterval(interval);
        //         }
        //         $(this).text(time);
        //     }, 1000);
        // });
        //
        // let intervalSecond = setInterval(() => {
        // }, 1000);

        $('body').on('click', '.js-myDeposits a, .js-myStore a', function (e) {
            $(this).parents('.main-box').attr('data-link', $(this).attr('href'));
        });

        let interval = setInterval(() => {
            // $('.timer-remind').each(function (el, e) {
            //     let url = $(e).parents('.js-myDeposits').attr('data-link');
            //     new Refresh(url, '#'+$(this).attr('id'), '#'+$(this).attr('id'), {}, {'type': 'absolute'});
            // });
            // $('.egg-number').each(function (el, e) {
            //     new Refresh(url, '#'+$(this).attr('id'), '#'+$(this).attr('id'), {}, {'type': 'absolute'});
            //
            // });
            $('.js-collect-refresh').each(function (el, e) {
                let url = $(e).parents('.main-box').attr('data-link');
                console.log(url);
                new Refresh(url, '.js-collect-refresh', '.js-collect-refresh', {}, {'type': 'absolute'});
            });
            // $('.progress-deposit').animate({width: '100%'}, 2000);
        }, 5000);


        this.submit();
    }

    submit() {
        $(document).on('submit', '.js-collectEgg', function(event) {
            event.preventDefault();

            let current = $(event.target);

            axios.post(event.target.action, new FormData(event.target))
                .then((response) => {
                    current.closest('.box').find('.overlay').remove();
                    current.find('.alert').remove();


                    let typeNotice = "success";
                    if (response.data.type) {
                        typeNotice = response.data.type;
                    }
                    new Notice(typeNotice, response.data.message);


                    let url = $(this).parents('.js-myDeposits').attr('data-link');
                    new Refresh(url, '.js-collect-refresh', '.js-collect-refresh');


                    setTimeout(function(){
                        $('[name=bird_image]').each(function(){
                        new FileInput($(this))
                            .imageInput('Загрузить изображение');
                    });
                    }, 1000);
                }).catch((error) => {
                    let errors = error.response.data.errors;
                    current.find('.alert').remove();

                    _.each(errors, (error) => {
                        current.find('.box-body').prepend(`<p class="alert alert-danger">${error}</p>`)
                    });

                    current.closest('.box').find('.overlay').remove();
                });
        });
    }
}
