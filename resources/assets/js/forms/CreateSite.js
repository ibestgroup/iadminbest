import FileInput from '../vendor/File';
import axios from "axios";
import _ from "lodash";
import Notice from "../Helpers/Notice";
import Refresh from "../Helpers/Refresher";

export default class CreateSite {
    constructor() {
        this.form = $('.js-createSite');

        this.submit();
    }

    submit() {
        $(document).on('submit', '.js-createSite', (event) => {
            event.preventDefault();

            let current = $(event.target);

            axios.post(event.target.action, new FormData(event.target))
                .then((response) => {
                    current.closest('.box').find('.overlay').remove();
                    current.find('.alert').remove();
                    current.find('.box-body').prepend(`<p class="alert alert-success">${response.data.message}</p>`)

                    if (response.data.type == "error") {
                        $('.box').prepend(`
                            <div class="col-lg-12">
                                <div class="alert alert-error">
                                    <h4><i class="icon fa fa-info"></i> Ошибка</h4>
                                    ${response.data.message}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        `);
                    } else {
                        $('.box').prepend(`
                            <div class="col-lg-12">
                                <div class="alert alert-success">
                                    <h4><i class="icon fa fa-info"></i> ${response.data.head}</h4>
                                    ${response.data.message}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        `);
                    }

                    new Refresh(location.pathname, '.box-body', '.box-body');

                }).catch((error) => {
                let errors = error.response.data.errors;
                current.find('.alert').remove();

                _.each(errors, (error) => {
                    current.find('.box-body').prepend(`<p class="alert alert-danger">${error}</p>`)
                });

                current.closest('.box').find('.overlay').remove();
            });

            current.closest('.box').append(
                `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
            );
        })
    }
}
