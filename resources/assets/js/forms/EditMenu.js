import FileInput from '../vendor/File';
import axios from "axios";
import _ from "lodash";
import Notice from "../Helpers/Notice";
import Refresh from "../Helpers/Refresher";
import "jquery-ui/ui/widgets/sortable.js";
import "bootstrap-select";
import ModalEditMenu from '../Helpers/ModalEditMenu';

export default class EditMenu {
    constructor() {

        this.editTopMenu();
        this.editSideBar();
        $('body').on('click', '.save-changes', function () {

            $('.edit-now').children('i').first().remove();
            $('.edit-now').prepend($('.selectpicker option:selected').data('content'));
            $('.edit-now').children('span').text($('.text-menu').val());
            $('.edit-now').attr('href', $('.'+$('.tab-link-active').data('tab') + ' .link-result').val());
            $('body').removeClass('modal-open');
            $('.fade').remove();

        });
        this.tabs();
        this.submit();
    }

    submit() {
        $(document).on('submit', '.js-editMenu', (event) => {
            event.preventDefault();
            console.log(event);


            let current = $(event.target);

            axios.post(event.target.action, new FormData(event.target))
                .then((response) => {
                    current.closest('.box').find('.overlay').remove();
                    current.find('.alert').remove();

                    let responses = response.data;
                    console.log(responses);
                    _.each(responses, (response) => {
                        console.log(response.type);
                        new Notice(response.type, response.message);
                        event.target.action.closest('.box-footer').prepend(`<p class="alert alert-danger">${response}</p>`)
                    });
                }).catch((error) => {
            });
            console.log(event.target[1].defaultValue);
            let typeMenu = '.sidebar';
            if (event.target[1].defaultValue == 'top_menu') {
                typeMenu = '.navbar-custom-menu';
            } else {
                typeMenu = '.sidebar';
            }

            setTimeout(function(){
                new Refresh(location.href, typeMenu, typeMenu);
            }, 1000);
            // console.log('sdds',event.target[1].defaultValue);
            current.closest('.box').append(
                `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
            );
        })
    }

    editSideBar() {
        /**
         * По клику добавляем кнопки для редактирования
         */

        $('body').on('click', '.menu-setting', function () {
            if (!$(this).hasClass('open')) {

                $(".all-slides").sortable({
                    cursor: 'move'
                });
                $('.sidebar-menu li').append(`
                    <a class="edit-menu-btn drag-n-drop"><i class="fa fa-bars"></i></a>
                    <a class="edit-menu-btn modal-open" data-toggle="modal" data-target="#modal-default"><i class="fa fa-edit"></i></a>
                    <a class="edit-menu-btn delete-btn" data-toggle="modal" data-target="#modal-default"><i class="fa fa-times"></i></a>
                `);
                $('.sidebar-menu').append(`
                    <button class="btn btn-primary add-menu mb-10">Добавить</button>
                `);
                $('.leftMenu').show();
                $(this).addClass('open');
            }
        });

        /**
         * Кнопка удаления
         */

        $('body').on('click', '.delete-btn', function () {
            $(this).parent('li').remove();
        });

        /**
         * Кнопка добавления
         */

        $('body').on('click', '.add-menu', function () {
            $(this).before(`
                <li>
                    <a href="ass">
                        <i class="fa fa-dashboard"></i>
                        <span>Пункт меню</span>
                    </a>
                    <a class="edit-menu-btn modal-open drag-n-drop"><i class="fa fa-bars"></i></a>
                    <a class="edit-menu-btn modal-open" data-toggle="modal" data-target="#modal-default"><i class="fa fa-edit"></i></a>
                    <a class="edit-menu-btn delete-btn"><i class="fa fa-times"></i></a>
                </li>
            `);
        });

        /**
         * Открываем модальное окно для редактирования
         */

        $('body').on('click', '.modal-open', function () {

            /* Удаляем предыдущее модальное окно */
            $('.fade').remove();

            /* Удаляем на предыдущих кнопках отметку редактирования */
            $('.sidebar-menu li a').removeClass('edit-now');

            let li = $(this).parent('li').children('a').first(),
                text = li.children('span').html(),
                icon = li.children('i').attr('class'),
                link = li.attr('href');

            /* Отмечаем что кнопка в данный момент редактируется */
            li.addClass('edit-now');

            /* Создаем модальное окно */
            let modalWindow = new ModalEditMenu(text);
            $('body').append(modalWindow.modal);

            $('.selectpicker').selectpicker();
            $('.bootstrap-select').width('55px');

            /* Выбираем selected для select */
            $('.selectpicker option').each(function () {
                if ($(this).val() == icon) {
                    $(this).attr('selected', 'selected');
                    $('.filter-option-inner-inner').html(`<i class="${icon}" aria-hidden='true'></i>`);
                }
            });

            $('.selectlink option').each(function () {
                if ($(this).val() == link) {
                    $(this).attr('selected', 'selected');
                }
            });

            $('.link-menu-my').val($('.edit-now').attr('href'));

        });

        /**
         * Сохраняем изменения меню
         */

        $("body").on('click', ".save-menu", function() {
            let htmlMenu = $('.sidebar-menu').get(0).outerHTML,
                jqHtmlMenu = $(htmlMenu);
            jqHtmlMenu.find('.modal-open, .add-menu, .js-editMenu, .delete-btn, .drag-n-drop').remove();
            htmlMenu = jqHtmlMenu.get(0).outerHTML.replace(/\r?\n/g, "");
            $('.html-menu').val(htmlMenu);
        });
    }

    editTopMenu() {
        /**
         * По клику добавляем кнопки для редактирования
         */

        $('body').on('click', '.top-menu-setting', function () {

            if (!$(this).hasClass('open')) {
                $(".all-slides-top").sortable({
                    cursor: 'move'
                });
                $('.navbar-nav li').append(`
                    <a class="edit-menu-btn modal-open drag-n-drop"><i class="fa fa-bars"></i></a>
                    <a class="edit-menu-btn modal-open" data-toggle="modal" data-target="#modal-default"><i class="fa fa-edit"></i></a>
                    <a class="edit-menu-btn delete-btn" data-toggle="modal" data-target="#modal-default"><i class="fa fa-times"></i></a>
                `);
                $('.navbar-nav').append(`
                    <button class="btn btn-primary add-top-menu mt-10">Добавить</button>
                `);
                $('.topMenu').show();
                $(this).addClass('open');
            }
        });

        /**
         * Кнопка удаления
         */

        $('body').on('click', '.delete-btn', function () {
            $(this).parent('li').remove();
        });

        /**
         * Кнопка добавления
         */

        $('body').on('click', '.add-top-menu', function () {
            $(this).before(`
                <li>
                    <a href="ass">
                        <i class="fa fa-dashboard"></i>
                        <span>Пункт меню</span>
                    </a>
                    <a class="edit-menu-btn modal-open drag-n-drop"><i class="fa fa-bars"></i></a>
                    <a class="edit-menu-btn modal-open" data-toggle="modal" data-target="#modal-default"><i class="fa fa-edit"></i></a>
                    <a class="edit-menu-btn delete-btn"><i class="fa fa-times"></i></a>
                </li>
            `);
        });

        /**
         * Открываем модальное окно для редактирования
         */

        $('body').on('click', '.modal-open', function () {

            /* Удаляем предыдущее модальное окно */
            $('.fade').remove();

            /* Удаляем на предыдущих кнопках отметку редактирования */
            $('.navbar-nav li a').removeClass('edit-now');

            let li = $(this).parent('li').children('a').first(),
                text = li.children('span').html(),
                icon = li.children('i').attr('class'),
                link = li.attr('href');

            /* Отмечаем что кнопка в данный момент редактируется */
            li.addClass('edit-now');

            /* Создаем модальное окно */
            let modalWindow = new ModalEditMenu(text);
            $('body').append(modalWindow.modal);

            $('.selectpicker').selectpicker();
            $('.bootstrap-select').width('55px');

            /* Выбираем selected для select */
            $('.selectpicker option').each(function () {
                if ($(this).val() == icon) {
                    $(this).attr('selected', 'selected');
                    $('.filter-option-inner-inner').html(`<i class="${icon}" aria-hidden='true'></i>`);
                }
            });

            $('.selectlink option').each(function () {
                if ($(this).val() == link) {
                    $(this).attr('selected', 'selected');
                }
            });

            $('.link-menu-my').val($('.edit-now').attr('href'));
        });

        /**
         * Сохраняем изменения меню
         */

        $("body").on('click', ".save-top-menu", function() {
            let htmlMenu = $('.navbar-nav').get(0).outerHTML,
                jqHtmlMenu = $(htmlMenu);
            jqHtmlMenu.find('.modal-open, .add-menu, .js-editMenu, .delete-btn, .drag-n-drop, .add-top-menu').remove();
            htmlMenu = jqHtmlMenu.get(0).outerHTML.replace(/\r?\n/g, "");
            $('.html-menu').val(htmlMenu);
        });
    }

    tabs() {
        $('body').on('click', '.tab-link', function () {
            $('.tabs-link').hide(100);
            $('.' + $(this).data('tab')).show(100);
            $('.tab-link').removeClass('tab-link-active');
            $(this).addClass('tab-link-active');
        });
    }
}
