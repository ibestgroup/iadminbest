import FileInput from '../vendor/File';
import axios from "axios";
import _ from "lodash";
import Notice from "../Helpers/Notice";
import Refresh from "../Helpers/Refresher";
import "jquery-ui/ui/widgets/sortable.js";
import "bootstrap-select";
import ModalEditMenu from '../Helpers/ModalEditMenu';

export default class EditSite {
    constructor() {
        if ($('.boxed-site').is(':checked')) {
            $('.site-width').hide();
        }


        this.tabs();

        this.changes();

        this.submit();
    }

    submit() {
        $(document).on('submit', '.js-editSite', (event) => {
            event.preventDefault();
            console.log(event);


            let current = $(event.target);

            axios.post(event.target.action, new FormData(event.target))
                .then((response) => {
                    current.closest('.box').find('.overlay').remove();
                    current.find('.alert').remove();

                    let responses = response.data;

                    let typeNotice = "success";
                    if (response.data.type) {
                        typeNotice = response.data.type;
                    }
                    new Notice(typeNotice, response.data.message);

                    _.each(responses, (response) => {
                        event.target.action.closest('.box-footer').prepend(`<p class="alert alert-danger">${response}</p>`)
                    });
                }).catch((error) => {
            });

            current.closest('.box').append(
                `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
            );
        })
    }

    changes() {

        $('.boxed-site').on('change', function () {
            $('.width-site').prop('disabled', function(i, v) { return !v; });
        });

        $('.boxed-header').on('change', function () {
            $('.width-header').prop('disabled', function(i, v) { return !v; });
        });

        $('input, select').on('change keyup keypress select click', function () {


            let leftSidebar = {
                'bg_color': $('input[name="left_sidebar[style][bg_color]"]').val(),
                'bg_img': $('input[name="left_sidebar[style][bg_img]"]').val(),
                'width': $('input[name="left_sidebar[style][width]"]').val(),
                'status': !!$('input[name="left_sidebar[status]"]').is(':checked'),
            }, rightSidebar = {
                'bg_color': $('input[name="right_sidebar[style][bg_color]"]').val(),
                'bg_img': $('input[name="right_sidebar[style][bg_img]"]').val(),
                'width': $('input[name="right_sidebar[style][width]"]').val(),
                'status': !!$('input[name="right_sidebar[status]"]').is(':checked'),
            }, topSidebar = {
                'bg_color': $('input[name="top_sidebar[style][bg_color]"]').val(),
                'bg_img': $('input[name="top_sidebar[style][bg_img]"]').val(),
                'height': $('input[name="top_sidebar[style][height]"]').val(),
                'status': !!$('input[name="top_sidebar[status]"]').is(':checked'),
            }, header = {
                'status': !!$('input[name="header[status]"]').is(':checked'),
                'img': $('input[name="header[img]"]').val(),
                'width_full': !!$('input[name="header[width_full]"]').is(':checked'),
                'width': $('input[name="header[width]"]').val(),
                'height': $('input[name="header[height]"]').val(),
                'border': $('input[name="header[border][width]"]').val() + 'px ' +
                        $('select[name="header[border][style]"]').val() + ' ' +
                        $('input[name="header[border][color]"]').val(),
                'border_radius': $('input[name="header[border_radius][tl]"]').val() + 'px ' +
                        $('input[name="header[border_radius][tr]"]').val() + 'px ' +
                        $('input[name="header[border_radius][br]"]').val() + 'px ' +
                        $('input[name="header[border_radius][bl]"]').val() + 'px',
            }, mainStyle = {
                'width_full': !!$('input[name="main_style[width_full]"]').is(':checked'),
                'width': $('input[name="main_style[width]"]').val(),
                'bg_color_body': $('input[name="main_style[bg_color_body]"]').val(),
                'bg_img_body': $('input[name="main_style[bg_img_body]"]').val(),
                'bg_color_content': $('input[name="main_style[bg_color_content]"]').val(),
                'bg_img_content': $('input[name="main_style[bg_img_content]"]').val(),
                'margin_top': $('input[name="main_style[margin_top]"]').val(),
                'border': $('input[name="main_style[border][width]"]').val() + 'px ' +
                    $('select[name="main_style[border][style]"]').val() + ' ' +
                    $('input[name="main_style[border][color]"]').val(),
                'border_radius': $('input[name="main_style[border_radius][tl]"]').val() + 'px ' +
                    $('input[name="main_style[border_radius][tr]"]').val() + 'px ' +
                    $('input[name="main_style[border_radius][br]"]').val() + 'px ' +
                    $('input[name="main_style[border_radius][bl]"]').val() + 'px',
            }

            // leftSidebar
            $('.main-sidebar').css({
                'display': (leftSidebar.status)?'block':'none',
                'background-color': leftSidebar.bg_color,
                'background-image': `url(${leftSidebar.bg_img})`,
                'width': leftSidebar.width + 'px',
                'padding-top': (topSidebar.status)?topSidebar.height + 'px':0,
            });

            // rightSidebar
            $('.right-sidebar').css({
                'display': (rightSidebar.status)?'block':'none',
                'background-color': rightSidebar.bg_color,
                'background-image': `url(${rightSidebar.bg_img})`,
                'width': rightSidebar.width + 'px',
                'padding-top': (topSidebar.status)?topSidebar.height + 'px':0,
            });

            //topSidebar
            $('.navbar').css({
                'display': (topSidebar.status)?'block':'none',
                'background-color': topSidebar.bg_color,
                'background-image': `url(${topSidebar.bg_img})`,
                'height': topSidebar.height + 'px',
                'min-height': topSidebar.height + 'px',
            });

            //header
            $('.my-header').css({
                'display': (header.status)?'block':'none',
                'width': (header.width_full)?'100%':header.width + 'px',
                'height': (header.height == 0)?'auto':header.height + 'px',
            });
            $('.header-img').css({
                'height': (header.height == 0)?'auto':header.height + 'px',
                'border': header.border,
                'border-radius': header.border_radius,
            });

            // console.log(header.border_radius);

            $('.header-img').attr('src', header.img);

            //mainStyle
            let width;
            if ($('.boxed-site').is(':checked')) {
                width = '100%';
            } else {
                width = $('.width-site').val() + 'px';
            }

            $('body').css({
                'background-color': mainStyle.bg_color_body,
                'background-image': `url(${mainStyle.bg_img_body})`,
            });

            $('.content').css({
                'background-color': mainStyle.bg_color_content,
                'background-image': `url(${mainStyle.bg_img_content})`,
                'margin-left': (leftSidebar.status)?leftSidebar.width + 'px': 0,
                'margin-right': (rightSidebar.status)?rightSidebar.width + 'px': 0,
            });


            $('.wrapper').css({
                'border': mainStyle.border,
                'border-radius': mainStyle.border_radius,
                'width': width,
                'margin': 'auto',
                'margin-top': mainStyle.margin_top + 'px',
            });

        });



    }

    tabs() {
        $('body').on('click', '.tab', function () {
            $('.tabs').hide(100);
            $('.' + $(this).data('tab')).show(100);
            $('.tab').removeClass('tab-active');
            $(this).addClass('tab-active');
        });
    }
}
