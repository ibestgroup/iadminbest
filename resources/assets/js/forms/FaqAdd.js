import Validator from '../Helpers/Validator';
import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';

export default class FaqAdd {
    constructor() {
        this.form = document.querySelector('.js-faqAdd');

        this.submit();
    }

    validator() {
        // return new Validator(this.form, {
        //     name: {
        //         presence: {message: "^Вопрос не может быть пустым"},
        //         length: {
        //             minimum: 15,
        //             tooShort: "^Минимальная длина %{count} символов"
        //         }
        //     },
        //     text: {
        //         presence: {message: "^Ответ не может быть пустым"},
        //         length: {
        //             minimum: 15,
        //             tooShort: "^Минимальная длина %{count} символов"
        //         }
        //     }
        // }).validate();

        return true;

    }

    submit() {
        $(document).on('submit', 'form', (e) => {

            e.preventDefault();

            // if (this.validator()) {
                axios.post(e.target.action, new FormData(e.target))
                    .then((response) => {

                        $('.box').find('.overlay').remove();


                        let typeNotice = "success";
                        if (response.data.type) {
                            typeNotice = response.data.type;
                        }

                        new Notice(typeNotice, response.data.message);


                        new Refresh(location.pathname, '.refresh', '.refresh');
                        
                        $('.collapsed-box').boxWidget({
                            animationSpeed: 500,
                            collapseTrigger: '#my-collapse-button-trigger',
                            removeTrigger: '#my-remove-button-trigger',
                            collapseIcon: 'fa-minus',
                            expandIcon: 'fa-plus',
                            removeIcon: 'fa-times'
                        });

                    }).catch((error) => {
                    let errors = error.response.data.errors;

                    _.each(errors, (error) => {
                        $(this.form).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)
                    });

                    $('.box').find('.overlay').remove();
                });

                $(e.target).closest('.box').append(
                    `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
                );
            // }
        });
    }
}