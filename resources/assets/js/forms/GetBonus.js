import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';
import AdEdit from "../forms/AdEdit";
import datepicker from "bootstrap-datepicker";
import Validator from "../Helpers/Validator";
import FileInput from "../vendor/File";


export default class GetBonus {

    constructor () {
        this.submit();
    }

    submit() {
        $(document).on('submit', '.js-getBonus', (e) => {

            e.preventDefault();


            axios.post(e.target.action, new FormData(e.target))
                .then((response) => {

                    $('.box').find('.overlay').remove();

                    new Notice("success", response.data.message);
                    new Refresh(location.pathname, '.js-getBonus', '.js-getBonus');

                }).catch((error) => {
                let errors = error.response.data.errors;
                _.each(errors, (error) => {

                    new Notice("error", error);
                    $(e.target).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)
                });

                $('.box').find('.overlay').remove();
            });

            $(e.target).closest('.box').append(
                `<div class="overlay">
                    <i class="fa fa-refresh fa-spin"></i>
                </div>`
            );
        });
    }

}