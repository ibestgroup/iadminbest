import Validator from '../Helpers/Validator';
import axios from "axios";

export default class NewsAdd {
    constructor() {
        this.form = document.querySelector('.js-newsAdd');
        $('.selectpicker').selectpicker();
        this.submit();
    }

    validator() {
        return new Validator(this.form, {
            name: {
                presence: {message: "^Укажите Название"},
                length: {
                    minimum: 15,
                    tooShort: "^Минимальная длина %{count} символов"
                }

            },
            text: {
                presence: {message: "^Заполните содержимое новости"},
                length: {
                    minimum: 15,
                    tooShort: "^Минимальная длина %{count} символов"
                }
            }
        }).validate();

    }

    submit() {
        $(this.form).submit((e) => {

            e.preventDefault();

            if (this.validator()) {
                axios.post(this.form.getAttribute('action'), new FormData(this.form))
                    .then((response) => {

                        $('.box').find('.overlay').remove();
                        $('.box').find('.box-body')
                            .prepend(`
                                <p class="alert alert-success">
                                    ${response.data.message} <a href="${response.data.link}">Перейти к новости</a>
                                </p>`);

                    }).catch((error) => {
                        let errors = error.response.data.errors;

                        _.each(errors, (error) => {
                            $('.box').find('.box-body').prepend(`<p class="alert alert-danger">${error}</p>`)
                        });

                        $('.box').find('.overlay').remove();
                    });

                $(this.form).closest('.box').append(
                    `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
                );
            }
        });
    }
}