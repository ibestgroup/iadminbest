import axios from "axios";
import _ from "lodash";
import Notice from "../Helpers/Notice";
import Refresh from "../Helpers/Refresher";
import "jquery-ui/ui/widgets/sortable.js";
import "jquery-ui/ui/core.js";
import "bootstrap-select";
import PHPUnserialize from "php-unserialize";
import FileInput from "../vendor/File";
import 'jquery.cookie';
// import 'bootstrap3-wysihtml5-npm';
// import Intro from "";

export default class ProfileBuilder {

    constructor(method) {
        if ($.cookie('lang') === 'en') {
            this.loadLogo = 'Upload logo',
                this.loadAva = 'Upload avatar',
                this.loadImg = 'Upload image';
        } else {
            this.loadLogo = 'Загрузить логотипs',
                this.loadAva = 'Загрузить аватар',
                this.loadImg = 'Загрузить изображение';
        }
        if (method != 'no') {
            $(document).ready(() => {

                $(document).ready(function () {

                    $('body').on('click', '.btn-app', function () {
                        new Refresh(location.pathname, '.grid-list-refresh', '.grid-list-refresh');
                    });

                    $('body').on('change', 'select[name=widget]', function () {
                        new Refresh(location.pathname, '.refresh-select-box', '.refresh-select-box', {widget: $(this).val()})
                    });

                    $('.enable-panel').on('click', function () {
                        $('.panel-redactor').slideToggle();
                        $('.panel-redactor').toggleClass('show-panel');

                        if ($('.panel-redactor').hasClass('show-panel')) {
                            $.cookie('panel-redactor', true);
                        } else {
                            $.cookie('panel-redactor', false);
                        }
                    });

                    $('body').on('click', 'input[name=random_referrals]', function () {
                        if ($(this).is(':checked')) {
                            $('input[name=first_page]').attr({'checked': 'checked', 'disabled': 'disabled'});
                        } else {
                            $('input[name=first_page]').removeAttr('disabled checked');
                        }

                    });

                    // $('body').on('click', '.button-close', function () {
                    //     new Refresh(location.pathname, '.grid', '.grid')
                    // });
                });

                this.form = $('.js-profile-builder');

                $('body').on('click', '.js-outline-grid', function () {
                    $('body').find('.grid').toggleClass('outline-grid');
                    $(this).toggleClass('active-edit');
                });

                // $('body').on('click', '.apply-change', function () {
                //     new Refresh(location.pathname, '.refresh-edit-box', '.refresh-edit-box');
                // });

                $('body').on('click', '.js-activate-grid', () => {
                    this.gridActivate();
                    $('.js-activate-grid').toggleClass('active-edit');
                    $('.btn-sort-grid').animate({height: '40px'}, {duration: 100});

                    if ($('.js-activate-grid').hasClass('active-edit')) {
                        $('.grid, .for-grid').sortable('enable');
                        $('.btn-sort-grid').animate({height: '40px'}, {duration: 100});
                    } else {
                        $('.grid, .for-grid').sortable('disable');
                        $('.btn-sort-grid').animate({height: 0}, {duration: 100});
                    }

                });

                $('body').on('click', '.js-activate-sort-list', () => {
                    this.sortList();
                });

                $('body').on('click', '.js-builder-btn', function () {
                    $('#edit-form-box').attr('data-block-id', $(this).data('id'));
                    $('input[name=block_id]').val($(this).data('id'));
                    $('#edit-form-box').attr('data-widget', $(this).data('widget'));


                    new Refresh(location.pathname, '#edit-form-box', '#edit-form-box', {id: $(this).data('id')});
                    $('.selectpicker').selectpicker('refresh');

                });


                $('.js-edit-elements').on('click', function() {
                    $(this).toggleClass('active-edit');
                });

                this.textEdit();
                this.submit();
                this.refreshElement();
                this.addElement();
                this.editElement();


                $(document).on('click', '.pagination a',function(event){

                    event.preventDefault();

                    $('li').removeClass('active');

                    $(this).parent('li').addClass('active');

                    let myurl = $(this).attr('href');
                    let box;

                    if ($(this).parents('.custom-paginate').length) {

                        let refreshBox = $(this).parents('.custom-paginate').data('refresh');

                        let obj = new ProfileBuilder('no');
                        let page = obj.getAllUrlParams($(this).attr('href')).page;

                        $.cookie('page-all-users', page);

                        if (refreshBox) {
                            box = refreshBox;
                        } else {
                            box = '.box-body';
                        }

                    } else {
                        let parent = $(this).parents('.main-box').data('id');
                        box = '#box-' + parent + ' .box-body';
                    }

                    new Refresh(myurl, box, box);

                });

                $('body').on('click', '.refresh-edit-box a', function() {
                    $(this).parents('div[data-widget=tarifs], div[data-widget=mydeposits]').attr('data-link', $(this).attr('href'));
                });

                $(document).on('click', '.referal-refresh a', function(event){

                    event.preventDefault();

                    $('li').removeClass('active');

                    $(this).parent('li').addClass('active');
                    let boxId = $(this).parents('[data-id]').data('id');

                    let myurl = $(this).attr('href');

                    new Refresh(myurl, '#box-' + boxId, '#box-' + boxId);

                });

                new FileInput($('form').find("[name=avatar]")).imageInput(this.loadAva);

            });

        }


    }

    submit() {
        $(document).on('submit', '.js-profile-builder', (event) => {
            event.preventDefault();

            let lengthContent = $(".cke_wysiwyg_frame").contents().find("body").html();
            if (lengthContent) {
                lengthContent = lengthContent.length;
            }
            if (lengthContent > 25000) {
                new Notice("error", `Максимальная длина контента 25000 символов. У вас ${lengthContent}`);
                return false;
            }


            $('.modal-backdrop').remove();
            let current = $(event.target);

            axios.post(event.target.action, new FormData(event.target))
                .then((response) => {
                    current.closest('.box').find('.overlay').remove();

                    let typeNotice = "success";
                    if (response.data.type) {
                        typeNotice = response.data.type;
                    }

                    new Notice(typeNotice, response.data.message);
                    new Refresh(location.pathname, '.profile-page', '.profile-page');
                    new Refresh(location.pathname, '.refresh-box-edit', '.refresh-box-edit', {id: response.data.block});


                    $('.grid').sortable('refresh');
                    $('.for-grid').sortable('refresh');

                }).catch((error) => {

                    let errors = error.response.data.errors;
                    current.find('.alert').remove();

                    _.each(errors, (error) => {
                        current.find('.box-body').prepend(`<p class="alert alert-danger">${error}</p>`)
                    });

                    current.closest('.box').find('.overlay').remove();

                });

            current.closest('.box').append(
                `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
            );
        });
    }

    updateSort(id, num, parent ) {
        axios.post('/profile/update-sort', {
            'id': id,
            'num': num,
            'parent': parent,
        });
    }

    gridActivate() {
        $(document).ready(function() {

            $(".for-grid").sortable({
                option: 'enable',
                opacity: 0.5,
                delay: 0,
                distance: 10,
                cursor: 'move',
                placeholder: 'sort-placeholder',
                scroll: true,
                scrollSensitivity: 20,
                scrollSpeed: 100,
                start: function (e, ui) {
                    ui.placeholder.height(ui.item.height());
                    ui.placeholder.width(ui.item.width());
                },
                beforeStop: function () {

                    $('.for-grid').each(function () {

                        $(this).children().each((e, el) => {
                            let pb = new ProfileBuilder('no');
                            let parent = $(this).data('id');
                            if (!parent) {
                                parent = 0;
                            }


                            pb.updateSort($(el).data('id'), e, parent);
                        });
                    });

                    new Notice("success", "Перемещение завершено", 'topCenter', 'sunset', 1000);

                    $('.for-grid').sortable('refresh');
                },
            });

            $(".grid").sortable({
                option: 'enable',
                connectWith: ".grid",
                opacity: 0.5,
                delay: 0,
                distance: 10,
                cursor: 'move',
                placeholder: 'sort-placeholder',
                scroll: true,
                scrollSensitivity: 20,
                scrollSpeed: 40,
                start: function (e, ui) {
                    $('.grid').css('border', '1px dashed #aaa');
                    let width = ui.item.width() / ui.item.parents('.grid').width() * 100;
                    ui.placeholder.height(ui.item.height());
                    ui.placeholder.width(width + '%');
                    ui.placeholder.css({'float': 'left'});
                },
                change: function (e, ui) {
                    let width = ui.item.width() / ui.item.parents('.grid').width() * 100;
                    ui.placeholder.height(ui.item.height());
                    ui.placeholder.width(width + '%');
                    ui.placeholder.css({'float': 'left'});
                },
                beforeStop: function () {
                    $('.grid').css('border', '');
                    $('.grid').each(function () {

                        $(this).children('div:not(.edit-grid, .sort-placeholder)').each((e, el) => {

                            let pb = new ProfileBuilder('no');
                            let parent = $(this).data('id');
                            if (!parent) {
                                parent = 0;
                            }
                            pb.updateSort($(el).data('id'), e, parent);
                        });
                    });

                    new Notice("success", "Перемещение завершено", 'topCenter', 'sunset', 1000);

                    $('.grid').sortable('refresh');
                },
            });


            $('.edit-grid, .js-sort-grid').sortable('disabled');

        });
    }

    sortList() {
        $(document).ready(function() {

            $('.for-btns').each(function () {
               $(this).html(`
                    <span class="sort-btn"><i class="fa fa-bars"></i></span>
                    <span class="delete-btn"><i class="fa fa-times"></i></span>
                `);
            });


            $('.for-btn-add').html(`<p class="btn btn-primary add-list"><i class="fa fa-plus"></i>Добавить элемент</p>`);

            $(".sort-list").sortable({
                placeholder: 'sort-list-placeholder',
                start: function(e, ui){
                    ui.placeholder.height(ui.item.height());
                },
                beforeStop: function () {
                    $('.sort').each(function (e, el) {

                        $(this).find('input, select').each(function () {
                            let valueName = $(this).attr('name');
                            valueName = valueName.replace(/\[.*\]\[/, '[' + e +'][');
                            $(this).attr('name', valueName);
                        });

                    });
                }
            });

            $('.selectpicker').selectpicker();

        });
    }

    addElement() {

        $(document).ready(() => {

            $('body').on('click', '.add-list', function() {

                let example = $(this).parents('.for-btn-add').data('example');
                let exampleHtml = $(example).get(0).outerHTML;

                let countEl = $(example + '-count > .sort').length + 1;

                exampleHtml = exampleHtml.replace(/element\-number/g, countEl);
                exampleHtml = exampleHtml.replace(/element\-hide/g, '');
                exampleHtml = exampleHtml.replace(/element\-list/g, '');
                exampleHtml = exampleHtml.replace(/example\-disabled/g, '');
                exampleHtml = exampleHtml.replace(/<select class="form-control"/g, '<select class="form-control selectpicker"');
                exampleHtml = exampleHtml.replace(/disabled="disabled"/g, '');
                console.log(example, exampleHtml);

                $(example + '-count').append(exampleHtml);
                $('.selectpicker').selectpicker();


            });

            $('body').on('click', '.delete-btn', function () {
                $(this).parents('.sort').remove();
            });

        });
    }

    editElement() {

            $("body").on({
                mouseenter: function (e) {
                    if ($('.js-edit-elements').hasClass('active-edit')) {
                        $(this).children('.edit-grid').animate({height: '40px'}, {duration: 100});
                    }
                },
                mouseleave: function (e) {
                    if ($('.js-edit-elements').hasClass('active-edit')) {
                        $(this).children('.edit-grid').animate({height: 0}, {duration: 100});
                    }
                },
            }, '.for-grid > .out-grid');

            $("body").on({
                mouseenter: function (e) {
                    if ($('.js-edit-elements').hasClass('active-edit')) {
                        $(this).children('.edit-main-box').animate({height: '40px'}, {duration: 100});
                    }

                },
                mouseleave: function (e) {
                    if ($('.js-edit-elements').hasClass('active-edit')) {
                        $(this).children('.edit-main-box').animate({height: 0}, {duration: 100});
                    }

                },
            }, '.grid > div');
    }

    refreshElement() {
        $('#edit-form-box').on('focusout', '.for-input-post input[type=color]', (event) => {
            this.refreshElementFunction(event);
        });
        $('#edit-form-box').on('change select', '.for-input-post input[type!=color], .for-input-post select, input[type!=color]', (event) => {
            this.refreshElementFunction(event);
        });
        $('#edit-form-box').on('click', '.text-align-edit, .font-weight, .font-style', (event) => {
            this.refreshElementFunction(event);
        });
        
        $('body').on('input select change', '#edit-login-box .edit-box', (event) => {
            this.refreshElementFunction(event);
        });

    }

    refreshElementFunction(event) {
        event.preventDefault();

        let link = location.pathname,
            refresh = '.refresh-edit-box';

        let current = $($(event.target).get(0)).parents('form').get(0);

        if ($($(event.target).get(0)).parents('#edit-form-box, #edit-login-box').data('link') !== '') {
            link = $($(event.target).get(0)).parents('.main-main').find('#edit-form-box, #edit-login-box').data('link');
        }

        let dataLink = $('body').find('#edit-form-box, #edit-login-box').attr('data-link');

        if (dataLink != '') {
            link = dataLink;
        }

        if ($(current).attr('data-refresh') != null) {
            refresh = $(current).attr('data-refresh');
        }

        axios.post($(current).attr('action'), new FormData(current)).then(() => {
            new Refresh(link, refresh, refresh, {id: $('input[name=block_id]').val()});
        });

    }

    textAlignValue() {
        $(document).ready(function() {
            $('body').find('.text-align-edit').each(function (e, el) {
               let alignValue = $(el).parents('.text-form-edit').find('select.text-align-select').val();
                $(el).parents('.text-form-edit').find('.text-align-edit[data-align='+alignValue+']').addClass('active');
            });

            $('body').find('.js-edit-bold').each(function (e, el) {
               if ($(el).is(':checked')) {
                   $(el).parents('.text-form-edit').find('.font-weight').addClass('active');
               } else {
                   $(el).parents('.text-form-edit').find('.font-weight').removeClass('active');
               }
            });

            $('body').find('.js-edit-italic').each(function (e, el) {
               if ($(el).is(':checked')) {
                   $(el).parents('.text-form-edit').find('.font-style').addClass('active');
               } else {
                   $(el).parents('.text-form-edit').find('.font-style').removeClass('active');
               }
            });
        });
    }

    textEdit() {
        $('body').on('click', '.text-align-edit', function () {
           $(this).parents('.text-form-edit').find('select option').removeAttr('selected');
           $(this).parents('.text-form-edit').find('.text-align-edit').removeClass('active');
           $(this).parents('.text-form-edit').find('select option[value=' + $(this).data('align') + ']').attr('selected', 'selected');
           $(this).addClass('active');
        });

        $('body').on('click', '.font-weight', function () {
           $(this).toggleClass('active');
           if ($(this).hasClass('active')) {
               $(this).parents('.text-form-edit').find('input.js-edit-bold').attr('checked', true);
           } else {
               $(this).parents('.text-form-edit').find('input.js-edit-bold').attr('checked', false);
           }
        });

        $('body').on('click', '.font-style', function () {
            $(this).toggleClass('active');
            if ($(this).hasClass('active')) {
                $(this).parents('.text-form-edit').find('input.js-edit-italic').attr('checked', true);
            } else {
                $(this).parents('.text-form-edit').find('input.js-edit-italic').attr('checked', false);
            }
        });
    }

    getAllUrlParams(url) {

        // извлекаем строку из URL или объекта window
        var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

        // объект для хранения параметров
        var obj = {};

        // если есть строка запроса
        if (queryString) {

            // данные после знака # будут опущены
            queryString = queryString.split('#')[0];

            // разделяем параметры
            var arr = queryString.split('&');

            for (var i=0; i<arr.length; i++) {
                // разделяем параметр на ключ => значение
                var a = arr[i].split('=');

                // обработка данных вида: list[]=thing1&list[]=thing2
                var paramNum = undefined;
                var paramName = a[0].replace(/\[\d*\]/, function(v) {
                    paramNum = v.slice(1,-1);
                    return '';
                });

                // передача значения параметра ('true' если значение не задано)
                var paramValue = typeof(a[1])==='undefined' ? true : a[1];

                // преобразование регистра
                paramName = paramName.toLowerCase();
                paramValue = paramValue.toLowerCase();

                // если ключ параметра уже задан
                if (obj[paramName]) {
                    // преобразуем текущее значение в массив
                    if (typeof obj[paramName] === 'string') {
                        obj[paramName] = [obj[paramName]];
                    }
                    // если не задан индекс...
                    if (typeof paramNum === 'undefined') {
                        // помещаем значение в конец массива
                        obj[paramName].push(paramValue);
                    }
                    // если индекс задан...
                    else {
                        // размещаем элемент по заданному индексу
                        obj[paramName][paramNum] = paramValue;
                    }
                }
                // если параметр не задан, делаем это вручную
                else {
                    obj[paramName] = paramValue;
                }
            }
        }

        return obj;
    }
}
