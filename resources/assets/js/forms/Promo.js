import Validator from '../Helpers/Validator';
import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';

export default class Promo {
    constructor() {
        this.form = document.querySelector('.js-promo');

        this.submit();
    }

    validator() {
        return new Validator(this.form, {
            name: {
                presence: {message: "^Вопрос не может быть пустым"},
                length: {
                    minimum: 3,
                    tooShort: "^Минимальная длина %{count} символов"
                }
            },
            image: {
                presence: {message: "^Ответ не может быть пустым"},
                length: {
                    minimum: 3,
                    tooShort: "^Минимальная длина %{count} символов"
                }
            },
            width: {
                presence: {message: "^Ответ не может быть пустым"}
            },
            height: {
                presence: {message: "^Ответ не может быть пустым"}
            }
        }).validate();

    }

    submit() {
        $(document).on('submit', this.form, (e) => {
            e.preventDefault();

            if (this.validator()) {
                axios.post(this.form.getAttribute('action'), new FormData(this.form))
                    .then((response) => {
                        $('.box').find('.overlay').remove();

                        new Notice("success", response.data.message);

                        if (location.pathname == '/profile/promo/add')
                            $('.form-control').val('');

                        console.log(location.pathname);

                        new Refresh(location.pathname, '.refresh', '.refresh');
                    }).catch((error) => {
                    let errors = error.response.data.errors;

                    _.each(errors, (error) => {
                        $(this.form).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)
                    });

                    $('.box').find('.overlay').remove();
                });

                $(e.target).closest('.box').append(
                    `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
                );
            }
        });
    }
}