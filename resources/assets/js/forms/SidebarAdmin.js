import Validator from '../Helpers/Validator';
import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';
import Store from '../forms/Store';
import AdminMarksCustom from "../components/admin/AdminMarksCustom";

export default class SidebarAdmin {

    constructor() {
        // $('body').on('click', '.js-sidebar-admin a', function (e) {
        //     e.preventDefault();
        //     let href = $(this).attr('href');
        //     if (href !== '#') {
        //         $(this).addClass('active');
        //         $('.content-wrapper').attr('data-link', href);
        //
        //         new Refresh(href, '.content-wrapper', '.content-wrapper');
        //     }
        //
        // });
    }

    // validator() {
    //     return new Validator(this.form, {
    //         name: {
    //             presence: {message: "^Вопрос не может быть пустым"},
    //             length: {
    //                 minimum: 15,
    //                 tooShort: "^Минимальная длина %{count} символов"
    //             }
    //         },
    //         text: {
    //             presence: {message: "^Ответ не может быть пустым"},
    //             length: {
    //                 minimum: 15,
    //                 tooShort: "^Минимальная длина %{count} символов"
    //             }
    //         }
    //     }).validate();
    //
    // }
    //
    // submit() {
    //     $(document).on('submit', this.form, (e) => {
    //
    //         e.preventDefault();
    //
    //         if (this.validator()) {
    //             axios.post(this.form.getAttribute('action'), new FormData(this.form))
    //                 .then((response) => {
    //
    //                     $('.box').find('.overlay').remove();
    //
    //                     new Notice("success", response.data.message);
    //
    //
    //                 }).catch((error) => {
    //                 let errors = error.response.data.errors;
    //
    //                 _.each(errors, (error) => {
    //                     $(this.form).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)
    //                 });
    //
    //                 $('.box').find('.overlay').remove();
    //             });
    //
    //             $(e.target).closest('.box').append(
    //                 `<div class="overlay">
    //                     <i class="fa fa-refresh fa-spin"></i>
    //                 </div>`
    //             );
    //         }
    //     });
    // }
}