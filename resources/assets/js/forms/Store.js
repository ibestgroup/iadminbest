import axios from "axios";
import _ from "lodash";
import Notice from "../Helpers/Notice";
import Refresh from "../Helpers/Refresher";
import "jquery-ui/ui/widgets/sortable.js";
import "jquery-ui/ui/core.js";
import "bootstrap-select";
import PHPUnserialize from "php-unserialize";
import FileInput from "../vendor/File";
import 'jquery.cookie';
export default class Store {

    constructor() {

        // this.file = new FileInput($('body').find("[name=image]"))
        //     .imageInput('Загрузить картинку');

        $('[name="image"], [name="no_image"], [name="logo"], [name="image_current"], [name="no_image"], [name="bird_image"]').each(function(){
            new FileInput($(this))
                .imageInput('Загрузить изображение');
        });


        $(document).ready(function () {
            $('.my-tooltip').tooltip({
                position: {
                    my: "center top",
                    at: "center top",
                    using: function( position, feedback ) {
                        $( this ).css( position );
                        $( "<div>" )
                            .addClass( "arrow" )
                            .addClass( feedback.vertical )
                            .addClass( feedback.horizontal )
                            .appendTo( this );
                    }
                }
            });

            // $('body').find('h3[data-tog]').on('click', function () {
            //     $($(this).attr('data-tog')).toggle(100);
            // });
        });

        $('.selectpicker').selectpicker();

        $('select.select-access').on('select change', function () {

            if ($(this).val() == 4 || $(this).val() == 1) {
                $(this).parents('form').find('.condition-access').hide(300);
            } else {
                $(this).parents('form').find('.condition-access').show(300);
            }
        });

        $('body').on('change', '.js-auto-activate #mark', function () {


            axios.get($(this).parents('form').attr('action') + '?mark=' + $(this).val())
                .then((response) => {

                    // console.log(response.data.message, response);
                    $('#mark-enter').text(response.data.markEnter);

                    $('.count-users').text(response.data.count);
                    let countResult = response.data.count;
                    console.log($('#activate-count').val());
                    if (countResult > $('#activate-count').val()) {
                        countResult = $('#activate-count').val();
                    }
                    $('.count-users').text(response.data.count);
                    $('.count-users-result').text(countResult);
                    $('#for-result').show(200);
                    $('#start-activate').show(200);
                }).catch((error) => {

                });
        });
        // $(document).ready(() => {
            this.submit();
        // });

    }

    submit() {
        $(document).on('submit', '.js-store', (event) => {
            event.preventDefault();
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            let current = $(event.target),
                reload = current.data('load');

            axios.post(event.target.action, new FormData(event.target))
                .then((response) => {
                    current.closest('.box').find('.overlay').remove();


                    if (response.data.action) {
                        let action = response.data.action;
                        if (action == 'sandbox-disable') {
                            $('.sandbox-attention').remove();
                        } else if (action == 'sandbox-enable') {
                            $('body').prepend(`<div class="sandbox-attention">Сайт в режиме песочницы. Действия ограничены.</div>`);
                        }
                    }

                    let typeNotice = "success";
                    if (response.data.type) {
                        typeNotice = response.data.type;
                    }

                    let link = location.pathname;
                    if ($('.content-wrapper').data('link') != '') {
                        link = $('.content-wrapper').data('link');
                    }

                    new Notice(typeNotice, response.data.message);
                    new Refresh(link, reload, reload);



                }).catch((error) => {

                    let errors = error.response.data.errors;
                    current.find('.alert').remove();

                    _.each(errors, (error) => {
                        current.find('.box-body').prepend(`<p class="alert alert-danger">${error}</p>`)
                    });

                    current.closest('.box').find('.overlay').remove();
                });
            current.closest('.box').append(
                `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
            );
        });
    }
}
