import Validator from '../Helpers/Validator';
import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';

export default class TicketAnswer {
    constructor() {
        this.form = document.querySelector('.js-answerAdd');

        this.submit();
    }
    validator() {
        return new Validator(this.form, {
            text: {
                presence: {message: "^Сообщение не может быть пустым"},
                length: {
                    minimum: 5,
                    tooShort: "^Минимальная длина %{count} символов"
                }
            }
        }).validate();

    }

    submit() {
        $(document).on('submit', this.form, (e) => {

            e.preventDefault();

            if (this.validator()) {
                axios.post(this.form.getAttribute('action'), new FormData(this.form))
                    .then((response) => {

                        $('.box').find('.overlay').remove();

                        new Notice("success", response.data.message);
                        console.log(131234);
                        new Refresh();
                    }).catch((error) => {
                    let errors = error.response.data.errors;

                    _.each(errors, (error) => {
                        $(this.form).closest('.box-footer').prepend(`<p class="alert alert-danger">${error}</p>`)
                    });
  
                    $('.box').find('.overlay').remove();
                });

                $(e.target).closest('.box').append(
                    `<div class="overlay">
                        <i class="fa fa-refresh fa-spin"></i>
                    </div>`
                );
            }
        });
    }
}