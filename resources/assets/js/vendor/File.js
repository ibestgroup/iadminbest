require("bootstrap-fileinput");

export default class FileInput {
    constructor(file) {
        this.file = file;
    }

    imageInput(label, err_container = '#kv-avatar-errors-1') {
        return this.file.fileinput({
            showBrowse: true,
            showCancel: false,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: label,
            showUpload: false,
            removeLabel: '',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Отменить изменения',
            elErrorContainer: err_container,
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: `<img src="${this.file.data('default')}">`,
            layoutTemplates: {main: '{preview} {remove} {browse}'},
            allowedFileExtensions: ["jpg", "png", "gif"]
        });
    }
}