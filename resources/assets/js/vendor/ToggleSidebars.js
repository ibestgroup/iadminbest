import Validator from '../Helpers/Validator';
import Notice from '../Helpers/Notice';
import axios from "axios";
import Refresh from '../Helpers/Refresher';

export default class AdEdit {
    constructor() {
        $('body').on('click', '.toggle-sidebar-left', function () {
            let width = $('#left-sidebar').width();
            console.log(width);
            if ($(this).hasClass('active')) {
                $('#left-sidebar').animate({'left': '-'+width+'px'});
                $(this).removeClass('active');
            } else {
                $('#left-sidebar').animate({'left': '0px'});
                $(this).addClass('active');
            }
        });

        $('body').on('click', '.toggle-sidebar-right', function () {
            let width = $('#right-sidebar').width();
            console.log(width);
            if ($(this).hasClass('active')) {
                $('#right-sidebar').animate({'right': '-'+width+'px'});
                $(this).removeClass('active');
            } else {
                $('#right-sidebar').animate({'right': '0px'});
                $(this).addClass('active');
            }
        });
    }
}