<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'registration' => 'Sign up',
    'login' => 'Sign in',
    'menu' => 'Menu',
    'home_header' => 'Create your own MLM project<br> It is easy. <br>It is free.',
    'home' => 'Home',
    'about' => 'What is iAdmin.best',


    'login_name' => 'Username',
    'telegram_login_name' => 'Telegram',
    'password' => 'Password',
    'repeat_password' => 'Repeat password',
    'accept' => 'I accept',
    'terms' => 'terms',
    'reset_password' => 'I forgot password',
    'remember_me' => 'Remember me',

    'logout' => 'Log out',

    'instrument' => 'i<span class="theme-color">A</span>dmin.<span class="theme-color">best</span> - is the MLM-projects builder',
    'desc' => 'iAdmin - IS NOT just a common landing page builder, but it IS a powerful engine made by our team specially for MLM-project leaders who wants to open their own finance projects. With our service you can create MLM projects of these several types',
    'list_h_1' => 'MLM matrix',
    'list_desc_1' => 'Customize your site structure as you see fit. Any width of the matrix, any number of levels.',
    'list_h_2' => 'Paid content',
    'list_desc_2' => 'You can add the info product * to your project, and give participants access to it after level acquisition.',
    'list_h_3' => 'Reinvestment',
    'list_desc_3' => 'If necessary, you can fully automate the system for participants using reinvestments.',
    'list_add_info' => '* - To add any information product, you must have copyright, or permission from the author to distribute.',
//    'list_h_1' => 'MLM Matrix project',
//    'list_desc_1' => 'Matrix finance project with multi-level structure',
//    'list_h_2' => 'Investing project',
//    'list_desc_2' => 'Interest-bearing deposit project with flexible really useful settings',
//    'list_h_3' => 'Farm project',
//    'list_desc_3' => 'Game-typed finance project with money-widthdraw option',


    'ready' => 'Your finance project is almost ready',
    'ready_desc' => 'Please, come up with your default site address (then it\'s available to be switched to the second level domain)',

//    'features' => '<h2 class="mb-4" id="about">iAdmin.best - Features</h2>
//<h3>With this service you can simple create your own game-typed finance project with money widthdraw function. This will take just a few minutes</h3>
//<h3>For example you can create your farm-project:</h3>
//<h3>Just choose your project\'s categories that will generate revenue. <b>There may be several types of revenue at the same time in one project!</b></h3>
//<img src="/iadminbest/public/img/landing/image1.png" alt="" style="border: 3px solid #96dde8; border-radius: 10px; min-width: 100%;">
//<h3>Add any quantity of revenue types to each category</h3>
//<img src="/iadminbest/public/img/landing/image2.png" alt="" style="border: 3px solid #96dde8; border-radius: 10px; min-width: 100%;">',

    'features' => '<h2 class="mb-4" id="about">Site features</h2>
                <h3>On our site you can customize your MLM matrix.</h3>
                <h3>Choose the number of levels, the width of the matrix and any of the many settings. <b>There may be several different rates on one site!</b></h3>
                <img src="/iadminbest/public/img/landing/image1_en.png" alt="" style="border: 3px solid #ff8d00; border-radius: 10px; max-width: 100%;">
                ',
//                <h3>Например создать свою ферму:</h3>
//                <h3>К каждой категории добавить любое количество источников</h3>
//                <img src="/iadminbest/public/img/landing/image2.png" alt="" style="border: 3px solid #96dde8; border-radius: 10px; max-width: 100%;">

    'price' => 'How much is for using?',
    'price_desc' => 'Create any project(including the hosting) for you is ABSOLUTELY FREE. The iAdmin.best service only charges a commission of 10% from the transactions of your visitors, but not from you.',

    'footer' => '<div class="col-md">
<div class="ftco-footer-widget mb-4">
<h2 class="ftco-heading-2">IADMIN.BEST</h2>
<p>Investing projects builder.</p>
</div>
</div>
<div class="col-md">
<div class="ftco-footer-widget mb-4 ml-5">
<h2 class="ftco-heading-2">Pages</h2>
<ul class="list-unstyled">
<li><a href="/" class="py-2 d-block" style="color: #fff;">Home</a></li>
<li><a href="{{route(\'login\')}}" class="py-2 d-block" style="color: #fff;">Sign in</a></li>
<li><a href="{{route(\'register\')}}" class="py-2 d-block" style="color: #fff;">Sign up</a></li>
</ul>
</div>
</div>
<div class="col-md">
<div class="ftco-footer-widget mb-4">
<h2 class="ftco-heading-2">E-mail</h2>
<ul class="list-unstyled">
<li><a href="#" class="py-2 d-block" style="color: #fff;">support@iadmin.best</a></li>
<li><a href="https://vk.com/id527512598" target="_blank" class="py-2 d-block" style="color: #fff;">Connect the developer</a></li>
</ul>
</div>
</div>
<div class="col-md">
</div>',

    'my_sites' => 'My sites',
    'affiliate_programm' => 'Affiliate programm',
    'ref_link' => 'Your personal refferal link',
    'your_balance' => 'Your balance',
    'affiliate_desc' => 'Get up to 50% of our commission from every replenishment made by users registered through your refferal link',
    'affiliate_th' => '<tr>
<th style="width: 10px">#</th>
<th>Rang</th>
<th>Earned</th>
<th style="width: 40px">Your earnings</th>
</tr>',
    'rang_1' => 'Inactive',
    'rang_2' => 'Beginner',
    'rang_3' => 'Junior refferer',
    'rang_4' => 'Refferer',
    'rang_5' => 'Junior master',
    'rang_6' => 'Master',
    'rang_7' => 'Junior guru',
    'rang_8' => 'Guru',
    'rang_9' => 'Ace',
    'your_rang' => 'Your rang now',

    'refferals_th' => '<tr>
<th style="width: 10px">#</th>
<th>Nickname</th>
<th>Amount of profects</th>
<th>Total Number of Top-ups</th>
</tr>',
    'total_ref' => 'Total refferals',
    'no_ref' => 'You don\'t have refferals yet',

    'sites_th' => '<tr>
<th style="width: 10px">#</th>
<th>Project name</th>
<th>Project link</th>
<th>Date of creation</th>
</tr>',

    'no_sites' => 'You haven\'t create any project yet',
    'create_site' => 'Create a project',
    'select_site' => '<option value="1">MLM/Matrix Project</option>
<option value="2">Investment Project</option>
<option value="3">Game-typed Project</option>',



    'documentation' => 'Documentation',
    'on_panel' => 'On/Off panel',

    'edit_panel' => 'Website edit panel',
    'admin_panel' => 'Admin panel',
    'add_area' => 'Add area',
    'add_element' => 'Add item',
    'on_grid' => 'On/Off grid',
    'sort' => 'Reorder',
    'setting_elements' => 'Setting items',
    'edit_site' => 'Site editor',


    'site_area' => 'Site areas',
    'site_header' => 'Site header',
    'site_top_sidebar' => 'Upper menu',
    'site_left_menu' => 'Left <br>menu <br>',
    'site_content' => 'Content <br>area <br>',
    'site_right_menu' => 'Right <br>menu <br>',
    'left_menu' => 'Left menu',
    'right_menu' => 'Right menu',
    'top_sidebar' => 'Upper menu',
    'bg_color' => 'Background color',
    'width' => 'Width',
    'height' => 'Height',
    'bg_img' => 'Background image',
    'full_width' => 'Full width',
    'setting_width' => 'Width setting',
    'image' => 'Image',
    'border' => 'Border',
    'color' => 'Color',
    'type' => 'Type',
    'border_radius' => 'Border radius',
    'top_left' => 'Top left',
    'top_right' => 'Top right',
    'bottom_left' => 'Bottom left',
    'bottom_right' => 'Bottom right',
    'main_setting' => 'Main settings',
    'main_bg' => 'Main background',
    'content_bg' => 'Content area background',
    'site_margin_top' => 'Site margin top',
    'margin' => 'Margin',

    'back_site' => 'Back to website',
    'admin_main' => 'Main page',
    'admin_pages' => 'Pages',
    'admin_templates' => 'Site templates',
    'admin_pages_auth' => 'Authorization pages',
    'admin_setting' => 'Site settings',
    'admin_marks' => 'Marketing types',
    'admin_finance' => 'Finances',
    'admin_content' => 'Site content',
    'admin_faq' => 'FAQ',
    'admin_promo' => 'Promo resourses',
    'admin_news' => 'News',
    'admin_edit_landing' => 'Edit main page',
    'admin_user_list' => 'Userlist',
    'admin_parking' => 'Domain parking',
    'admin_edit_marks' => 'Edit marketing type',
    'admin_docs' => 'Documentation',
    'admin_questions' => 'I have a question',
    'admin_telegram_bot' => 'Telegram bot',

    'admin_economy' => 'Site economy',
    'admin_sum_payment' => 'Total payment amount',
    'admin_sum_cashout' => 'Total withdraw',
    'admin_sum_balance_site' => 'Site balance',
    'admin_sum_balance_users' => 'Users balance',
    'admin_sum_reserved' => 'Reserved in tariffs',
    'admin_sum_all' => 'Project money flow',

    'admin_statistic' => 'Site statistic',
    'admin_users' => 'Users',
    'admin_users_active' => 'Activated users',
    'admin_activate' => 'Activation number',

    'admin_site_name' => 'Site name',
    'admin_site_logo' => 'Site logo',
    'admin_load_logo' => 'Upload logo',
    'admin_custom_avatar' => 'Default avatar',
    'admin_load_avatar' => 'Upload avatar',
    'admin_start_reg' => 'Registration start date',
    'admin_start_activation' => 'Activation start date',
    'admin_cashout_limit' => 'Withdraw limit',
    'admin_more' => 'From',
    'admin_less' => 'Up to',
    'admin_cashout_before_start' => 'Withdraw before start',

    'admin_enable' => 'Enable',
    'admin_disable' => 'Disable',
    'admin_enable_but' => 'Enable, but unavailable to activate via button',
    'admin_under_admin' => 'Out of admin',
    'admin_save' => 'Save',

    'admin_tarif' => 'Tariff',
    'admin_status' => 'Status',
    'admin_mark_name' => 'Tariff name',
    'admin_overflow' => 'Overflow',
    'admin_mark_type' => 'Tariff type',
    'admin_mark_type_line' => 'Linear(sequential levels purchase)',
    'admin_mark_type_multi' => 'Multi(one-time purchase of all levels)',

    'admin_width' => 'Width',
    'admin_levels' => 'Number of levels',
    'required_mark' => 'Mark required',

    'admin_prices' => 'Prices for levels and reinvests',
    'admin_lev' => 'lev.',
    'admin_price' => 'Price',
    'admin_to_parent' => 'R',
    'admin_to_parent_desc' => 'To referrer. Enter how much will get the person who inveted new user',
    'admin_to_admin' => 'A',
    'admin_to_admin_desc' => 'To the administrator. Enter how much will get the administrator.',
    'admin_reinvest' => 'Reinvest (to/number)',
    'admin_reinvest_first' => 'To the first',
    'admin_reinvest_first_desc' => 'Enter the number of reinvests to the first tariff.',
    'admin_priority' => 'Priority',
    'admin_priority_desc' => 'Choose the order of funds distribution.',
    'admin_priority_1' => 'Withdraw -> Level up -> Reinvest -> Reinvest to the first',
    'admin_priority_2' => 'Withdraw -> Reinvest -> Reinvest to the first -> Level up',
    'admin_priority_3' => 'Withdraw -> Reinvest to the first -> Реинвест -> Level up',
    'admin_priority_4' => 'Reinvest to the first -> Reinvest -> Level up -> Withdraw',
    'admin_priority_5' => 'Reinvest -> Reinvest to the first -> Level up -> Withdraw',

    'admin_mark_enter' => 'Enter',
    'admin_mark_profit' => 'Income',
    'admin_mark_level' => 'Level',
    'admin_mark_people' => 'Number of people',
    'admin_mark_reinvest' => 'Reinvest',
    'admin_mark_reinvest_first' => 'Reinvest to the first tariff',
    'admin_total' => 'Result',


    'admin_enable_w' => 'Enable',
    'admin_disable_w' => 'Disable',

    'admin_your_pages' => 'Your created pages',
    'admin_name' => 'Name',
    'admin_settings' => 'Setting',
    'admin_delete_page' => 'Delete page',
    'admin_delete_page_modal' => 'If you delete this page, all widgets, areas and blocks it contains also will be deleted. Are you sure you want to delete it all?',
    'admin_accept' => 'Save',
    'admin_add_page' => 'Add page',
    'admin_empty_link' => 'If "link" field is empty, the link will be created automaticly',
    'admin_page_name' => 'Name',
    'admin_page_link' => 'Link',
    'admin_add' => 'Add',
    'admin_close' => 'Close',
    'close' => 'Close',

    'payment_system' => 'Payment system',
    'enter_amount' => 'Enter amount',
    'amount' => 'Sum',
    'wallet_for_withdraw' => 'Wallet for withdraw',
    'wallet' => 'Wallet',

    'admin_you_templates' => 'Your created templates',
    'admin_add_template' => 'Add template',
    'admin_name_template' => 'Name',

    'admin_auth_setting' => 'Configure page "Sign in" and "Sign up"',
    'admin_auth_sign_in' => 'Authorization',
    'admin_auth_sign_up' => 'Registration',
    'admin_auth_password' => 'Password recover',
    'admin_auth_page' => 'Page',
    'admin_auth_page_sign_in' => 'authorization',
    'admin_auth_page_sign_up' => 'registration',
    'admin_auth_page_password' => 'password recover',

    'admin_design_header' => 'Design header',
    'admin_design_main_block' => 'Design block',
    'admin_design_inputs' => 'Design inputs',

    'admin_edit_text' => 'Text',
    'text' => 'Text',

    'opacity' => 'Opacity',
    'text_color' => 'Text color',
    'icon_color' => 'Icon color',

    'setting_button' => 'Setting buttons',
    'decor' => 'Design',
    'btn_static' => 'Static',
    'btn_hover' => 'Hover',
    'btn_active' => 'Active',

    'align' => 'Align',
    'align_left' => 'To left',
    'align_center' => 'Center',
    'align_right' => 'To right',

    'border_solid' => 'Solid',
    'border_double' => 'Double',
    'border_dashed' => 'Dashed',
    'border_dotted' => 'Dotted',
    'border_inset' => 'Convex',
    'border_unset' => 'Groove',

    'pagination' => 'Pagination',
    'send_link' => 'Send recovery link',


    'modal_add_area' => 'Add area',
    'modal_name_area' => 'Area name',
    'modal_place_area' => 'Where the area will be located',

    'in_main' => 'In main area',
    'in_left_menu' => 'In left menu',
    'in_top_menu' => 'In top menu',
    'in_right_menu' => 'In right menu',

    'modal_add_widget' => 'Add widget fund replenishments',
    'modal_name_widget' => 'Block name',
    'modal_head_widget' => 'Block headline',
    'modal_place_widget' => 'In what area',
    'modal_create_area_on_page' => 'Create an area on this page',

    'modal_widget' => 'Withdraw balance exchange to balance for purchases',

    'modal_edit_block' => 'Edit block',
    'modal_editor_block' => 'Editable block',
    'modal_setting_block' => 'Block settings',
    'modal_setting_widget' => 'Widget settings',

    'widg_activate' => 'Tariff activation',
    'widg_tarifs' => 'My tariffs',
    'widg_store' => 'Tariffs for investment',
    'widg_mydeposits' => 'My investments',
    'widg_bonusclick' => 'Bonus per click',
    'widg_exchange' => 'Withdraw balance exchange to balance for purchases',
    'widg_profilebox' => 'Avatar and information',
    'widg_transaction' => 'Transactions block',
    'widg_balance' => 'Top up form',
    'widg_referrals' => 'Referrals list',
    'widg_news' => 'News',
    'widg_faq' => 'FAQ',
    'widg_promo' => 'Promo materials',
    'widg_profileedit' => 'Edit Profile',
    'widg_cashout' => 'Withdraw form',
    'widg_content' => 'With text',
    'widg_horizontalmenu' => 'Horizontal Menu',
    'widg_verticalmenu' => 'Vertical Menu',

    'information_user' => 'User information',
    'information_main' => 'Main information',
    'information_referer' => 'Referrer information',
    'information_site' => 'Site information',
    'information_tariff' => 'Tariff information',
    'value_images' => 'Images',
    'social_network' => 'Social networks',

    'without_icon' => 'Without image',

    'value_login' => 'Nickname',
    'value_balance' => 'Balance',
    'value_count_ref' => 'Number of referrals',
    'value_email' => 'Email(post',
    'value_link_visit' => 'Link visits',
    'value_profit' => 'Earned',
    'value_percent_sum_user' => '10% from participant\'s earnings',
    'value_last_active' => 'Latest activity',
    'value_vk' => 'VK',
    'value_twitter' => 'Twitter',
    'value_facebook' => 'Facebook',
    'value_instagram' => 'Instagramm',
    'value_skype' => 'Skype',
    'value_telegram' => 'Telegram',
    'value_text_status' => 'Text status',
    'value_login_parent' => 'Referrer\'s Nickname',
    'value_link_visit_parent' => 'Link visits',
    'value_count_ref_parent' => 'Number of referrals',
    'value_profit_parent' => 'Earned',
    'value_users_count' => 'Registered users',
    'value_activate_count' => 'Number of activations',
    'value_transaction_sum' => 'Project\'s finance turnover',
    'value_percent_sum' => '10% from finance turnover',
    'value_balance_cashout' => 'Wuthdraw balance',
    'value_affiliate_profit' => 'Earned from referral',
    'value_affiliate_profit_all' => 'Earned from affiliate program',

    'value_avatar' => 'Avatar',
    'value_name' => 'Name',
    'value_lastname' => 'Surname',
    'value_fathername' => 'Middle name',
    'value_password' => 'Password',
    'value_telephone' => 'Mobile number',

    'value_mark_name' => 'Tariff\'s name',
    'value_mark_profit' => 'Earned with tariff',
    'value_mark_profit_all' => 'Total tariff\'s income',
    'value_mark_balance' => 'Reserved',
    'value_mark_more' => 'Details button',
    'value_avatar_parent' => 'Referrer\'s avatar',
    'value_site_logo' => 'Website\'s logo',

    'block_for_menu' => 'Block for menu',
    'block_for_custom' => 'Regular block',

    'top' => 'From the top',
    'bottom' => 'From the bottom',
    'left' => 'From the left',
    'right' => 'From the right',

    'create' => 'Create',

    'main_page' => 'Main page',
    'logout_page' => 'Log out',
    'profile_page' => 'Profile main page',
    'connect_ibux_page' => 'iBux.best connect page',

    'id_transaction' => 'Transaction ID',
//    'amount' => 'Amount',
    'from_user' => 'From',
    'to_user' => 'To',
//    'type' => 'Type',
    'date' => 'Date',
    'deposit_name' => 'Deposit name',
    'deposit_number' => 'Deposit amount',
    'deposit_resource' => 'Earnable resources',
    'deposit_seconds' => 'Seconds left',
    'deposit_balance' => 'Deposis balance',
    'deposit_stock' => 'Reserved',
    'deposit_btn' => '"Withdraw funds" button',

    'invest_name' => 'Name',
    'invest_price' => 'Price/price range',
    'invest_second' => 'Seconds',
    'invest_second_to_minute' => 'Minutes',
    'invest_second_to_hour' => 'Hours',
    'invest_second_to_day' => 'Days',
    'invest_store_profit_second' => 'In one second',
    'invest_store_profit_minute' => 'In one minute',
    'invest_store_profit_hour' => 'In one hour',
    'invest_store_profit_day' => 'In one day',
    'invest_store_profit_all' => 'Total(if life span is limited)',
    'invest_number' => 'How much of this position does user have',
    'invest_store_btn' => '"Deposit funds" button',

    'time' => 'Time',
    'profit' => 'Profit',

    'padding_button' => 'Button\'s inner margin',
    'design_button' => 'Button design',
    'mains' => 'Main',
    'configure' => 'Set up',

    'ask' => 'Question',
    'answer' => 'Answer',
    'modal_main_setting_widget' => 'Widget main settings',
    'number_records' => 'Number of page records',
    'only_first_page' => 'Display only the first page',
    'what_display' => 'What to display',
    'top_button' => 'Buttons on top',
    'button_more' => '"Details" buttons',
    'elements' => 'Elements',

    'elements_with_tariff' => 'Elements with tariffs',
    'dividing_lines' => 'Dividing lines',
    'font' => 'Font',
    'table_color' => 'Table colors',
    'table_head_color' => 'Table head color',

    'static_color' => 'Default color',
    'hover_color' => 'Hover color',

    'head_news' => 'New\'s headline',
    'date_color' => 'Date color',

    'setting_time_line' => 'Timeline setting(line on the left)',
    'area_date' => 'Date area',
    'color_line' => 'Line color',
    'in_area_date' => 'in date area',
    'in_area_icon' => 'in icon area',
    'text_news' => 'New\'s color',
    'footer_news' => 'Bottom part',
    'design_btn' => 'Button design',

    'place_avatar' => 'Place for avatar and nickname',
    'elements_color' => 'Element colors',

    'free_place_naming' => 'Naming free place',
    'level_up_view' => 'Level up view',

    'text_left' => 'Text on the left',
    'text_right' => 'Text on the right',
    'btn' => 'Button',

    'design_banner' => 'Banner design',
    'design_head_banner' => 'Banner headline design',
    'design_element_banner' => 'Banner element common design',

    'padding' => 'Margins',

    'design_input_with_code' => 'Code field design',

    'show_random_ref' => 'Display random referrals',
    'active_ref' => 'Activated referrals',
    'non_active_ref' => 'Inactivated referrals',

    'column_with_value' => 'Colomns with value',

    'users_color' => 'Users color',

    'activity' => 'Activated',
    'unactivity' => 'Inactivated',

    'setting_list_tariff' => 'Tariff list settings',
    'setting_ref_tree' => 'Referral tree settings',
    'ref_tree' => 'Referral tree',
    'busy_place' => 'Occupied place',
    'busy_places' => 'Occupied places',
    'free_place' => 'Free place',
    'free_places' => 'Free places',

    'element_transaction' => 'Transaction elements',
    'color_transaction' => 'Transaction colors',

    'out_transaction' => 'Outgoing transactions',
    'in_transaction' => 'Incoming transactions',

    'transaction_payments_order' => 'Internal transfers',
    'transaction_cashout' => 'System payouts',
    'transaction_payment' => 'Replenishments',
    'head' => 'Headline',
    'bg' => 'Background',

    'margins' => 'Spacing from next blocks',
    'margin_left' => 'Margin left',
    'margin_right' => 'Margin right',


    'admin_edit_promo' => 'Edit promo material',
    'admin_create_promo' => 'Create promo',
    'link_image' => 'Image direct link',
    'width_banner' => 'Banner width',
    'height_banner' => 'Banner height',
    'list_promo' => 'Promo materials list',

    'faq' => 'FAQ',
    'create_ask' => 'Add question',

    'edit' => 'Edit',
    'delete' => 'Delete',

    'create_news' => 'Create news',
    'bg_color_icon' => 'Icon background color',
    'icon' => 'Icon',
    'publish' => 'Publish',

    'if_working' => 'If your website is working now, please confirm that the domain is successfully delegated',
    'delegate' => 'Delegated',
    'parking' => 'Park the domain',
    'enter_domain' => 'Enter domain name',

    'find_user' => 'Find a user',
    'login_or_id' => 'Nickname or ID',
    'start_enter' => 'Start to input here',

    'user_surname' => 'Surname',
    'user_name' => 'Name',
    'user_fathername' => 'Middle name',
    'user_date_register' => 'Registration date',
    'user_last_active' => 'Latest activity',
    'user_count_ref' => 'Number of referrals',
    'user_who_invite' => 'Invited by',
    'user_balance' => 'Balance',

    'user_privilege' => 'Privilege',
    'user_buys' => 'Purchases',
    'user_not_buys' => 'User doesn\'t have purchases',

    'for_cashout' => 'For withdraw',
    'for_buy' => 'For purchases',
    'to_list_users' => 'To userlist',
    'back' => 'Back',
    'user_not_fount' => 'This user does not exist',

    'all_payments' => 'All replenishments',
    'user' => 'User',

    'privilege_banned' => 'Banned user',
    'privilege_custom' => 'Normal user',
    'privilege_before_start' => 'May activate before start',

    'activations' => 'Activations',
    'tariff' => 'Tariff',

    'no_activation' => 'This user does not have activations',
    'no_payments' => 'This user does not have replenishments',
    'no_cashout' => 'This user does not have payouts',

    'payments' => 'Replenishments',
    'cashout' => 'Payouts',

    'font_size' => 'Font size',
    'font_setting' => 'Font settings',

    'save' => 'Save',


    'land_edit_mode' => 'Edit mode',
    'land_elements' => 'Elements',
    'land_content' => 'Content',
    'land_detalization' => 'Detalization',
    'land_no_changes' => 'No changes to save',
    'land_templates' => 'Templates',
    'land_preview' => 'Preview',
    'land_page_clear' => 'Clear the page',
    'land_clear_cache' => 'Clear browser cache',
    'land_create_your_page_by' => 'Create your page by drag/drop elements to this area',
    'land_all_blocks' => 'All blocks',
    'land_header' => 'Header',
    'land_introduction' => 'Introduction',
    'land_counters' => 'Counters',
    'land_additions' => 'Additions',
    'land_pop_ups' => 'Pop-ups',
    'land_text_blocks' => 'Text blocks',
    'land_reviews' => 'Reviews',
    'land_btnbl' => 'Blocks with buttons',
    'land_clients' => 'Clients',
    'land_social_networks' => 'Social networks',
    'land_team' => 'Team',
    'land_tariffs' => 'Tariffs',
    'land_sliders' => 'Sliders',
    'land_forms' => 'Forms',
    'land_footer' => 'Footer',
    'land_pages' => 'Pages',
    'land_add_new' => 'Add new',
    'land_page_sections' => 'Page sections',
    'land_edit_css' => 'Edit CSS',
    'land_editing' => 'Editing',
    'land_styles' => 'Styles',
    'land_bg_img' => 'Background image',
    'land_bg_color' => 'Background color',
    'land_top_field' => 'Top field',
    'land_values' => 'Values',
    'land_bottom_field' => 'Bottom field',
    'land_shadow' => 'Shadow',
    'land_border_width' => 'Border width',
    'land_border_radius' => 'Border radius',
    'land_border_color' => 'Border color',
    'land_img_size' => 'Image size',
    'land_img_fix_type' => 'Image fixation mode',
    'land_bg_img_repeat' => 'Background image repeat',
    'land_save_changes' => 'Save changes',
    'land_copy' => 'Copy',
    'land_reset' => 'Reset',
    'land_delete' => 'Delete',
    'land_img' => 'Image',
    'land_input_img_address' => 'Enter image URL link',
    'land_enter_img_url' => 'Input image URL here',
    'land_or' => 'OR',
    'land_upld_img' => 'Upload image',
    'land_choose_img' => 'Choose image',
    'land_animation' => 'Animation',
    'land_choose_anmt_type' => 'Choose animation type',
    'land_anmt_delay' => 'Animation delay',
    'land_anmt_time' => 'Animation duration',
    'land_def_1sec' => 'Default (1 second)',
    'land_slow_mo2sec' => 'Slow-mo (2 seconds)',
    'land_color' => 'Color',
    'land_font_size' => 'Font size',
    'land_bg_clr' => 'Background color',
    'land_icns' => 'Icons',
    'land_transparency' => 'Transparency',
    'land_font_family' => 'Font family',
    'land_chngs_svd' => 'Changes have been saved',
    'land_seo_stngs_fr' => 'SEO settings of',
    'land_pg_hdl' => 'Page headline',
    'land_pg_dscrpt' => 'Page description',
    'land_keywords' => 'Keywords',
    'land_add_to_hdr' => 'Add to header',
    'land_extracod_head' => 'Additional code you want to add to the <head> block',
    'land_save_seo_set' => 'Save SEO settings',
    'land_cancel' => 'Cancel',
    'land_chs_ext_tmp' => 'Please, choose existing template',
    'land_attntn' => 'Attention please:',
    'land_dnt_shw_ths_agn' => 'Don\'t show this again!',
    'land_shw_prvw' => 'Show preview',
    'land_nmbr_act_usrs' => 'Number of active users',
    'land_ttl_fnn_trn' => 'Total financial turnover',
    'land_trnsct_nmbr' => 'Number of transactions',
    'land_dz_ppsd' => 'Project lifetime (days)',
    'land_all_usrs' => 'Number of all users',
    'land_ttl_pouts' => 'Total withdraw amount',
    'land_bgclr' => 'Background color',
    'land_fnt_clr' => 'Font color',
    'land_transparent' => 'Transparent',
    'land_dfltt' => 'Back to default',
    'land_blct' => 'Dublicate',
    'land_code' => 'Code',
    'land_upload' => 'Upload',
    'video' => 'Video',
    'input' => 'Input',
    'checkbox' => 'Checkbox',
    'form' => 'Form',
    'styles' => 'Styles',
    'change' => 'Change',
    'land_ntc_txt1' => 'You can watch only one page; links to another pages will not work. When you editing your page, the preview refresh will not work. You have to use button "Preview" again instead.',
    'load_elements_constructor' => 'Load constructor elements',

    'choose_page' => 'Choose a page',
    'page_profile' => 'Profile',
    'page_logout' => 'Log out',
    'or' => 'or',
    'choose_block' => 'Choose a block (on site page)',
    'choose_image' => 'Choose image',
    'choose' => 'Choose',

    'js_builder' => 'builder_en.js',

    'without_reinvest' => 'Without reinvest',
    'number_first_reinvest' => 'Enter the number of reinvests in the first tariff plan.',
    'add' => 'Add',
    'add_promo' => 'Add promo',

    'crypto_payment' => '<strong>Attention!</strong> Commission for replenishment is 0.5% <br><strong>Attention!</strong> Do not make more than one transfer to one wallet, after making a transfer, you will be issued a new wallet',
    'crypto_address' => 'For top up balance use the address below',


    'error_start' => 'Activations are not available. Wait for the project to start.',

    'success_activated' => 'Tariff activated',

    'admin_cashout_limit_day' => 'Per day',
    'admin_cashout_limit_day_all' => 'Per day whole site',

    'error_cashout_limit_day' => 'Withdraw limit :qwe',

    'access_refer' => 'You need for access :condition referrals. You have :count',
    'access_link_visit' => 'You need for access :condition link follows. You have :count',
    'access_activate' => 'You need activate any tariff plan for access.',
    'access_activate_number' => 'You need activate :condition tariff plan for access.',

    'auth_page_login' => 'Sign in page',
    'auth_page_register' => 'Sign up page',

    'auto_activate' => 'Auto activation',
    'activates' => 'Activations',

    'item_changed' => 'Item has been modified',
    'design_changed' => 'Design has been modified',

    'mark_saved' => 'Tariff plan saved',
    'details' => 'Details',

    'to_inviters' => 'Direct income to inviters',


    'register_login_required' => "Login field, required",
    'register_login_regex' => "Login must consist of latin letters and numbers",
    'register_login_max' => "Password length max 20 letters",
    'register_login_unique' => "This login is already in use",
    'register_email_required' => "Email field, required",
    'register_email_max' => "Email length max 255 letters",
    'register_email_unique' => "This email is already in use",
    'register_password_required' => "Password field, required",
    'register_password_confirmed' => "Password mismatch",
    'register_password_min' => "Password length min 6 letters",
    'register_terms_required' => 'You have not accepted the terms',
    'register_terms_accepted' => 'You have not accepted the terms',



    'new_password' => 'New password',
    'confirm_new_password' => 'Confirm new password',

    'yes' => 'Yes',
    'no' => 'no',

    // public WKEIVGZ7J8onprz21qUwlSE1CmZCSdlp
    // private SHXh4q8uVv1V8I9zgXEQ3rSuMCxLjMo8
    // TWINVEST
    // HAKER
    // BARSA

];
