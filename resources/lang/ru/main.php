<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'registration' => 'Регистрация',
    'login' => 'Войти',
    'menu' => 'Меню',
    'home_header' => 'Создай свой проект.<br> Это просто. <br>Это бесплатно.',
    'home' => 'Главная',
    'about' => 'Что это?',

    'login_name' => 'Логин',
    'telegram_login_name' => 'Телеграм',
    'password' => 'Пароль',
    'repeat_password' => 'Повторите пароль',
    'accept' => 'Я принимаю',
    'terms' => 'пользовательское соглашение',
    'reset_password' => 'Я забыл свой пароль',
    'remember_me' => 'Запомнить меня',

    'logout' => 'Выйти',

    'instrument' => 'i<span class="theme-color">A</span>dmin.<span class="theme-color">best</span> - это инструмент',
    'desc' => 'iAdmin - это не просто инструмент для создания красивой странички. Это мощный инструмент для создания полноценного проекта. На нашем сервисе Вы можете создавать МЛМ проекты разного типа:',
    'list_h_1' => 'МЛМ матрицы',
    'list_desc_1' => 'Настройте Вашу структуру на сайте так, как считаете нужным. Любая ширина матрицы, любое количество уровней.',
    'list_h_2' => 'Платный контент',
    'list_desc_2' => 'Вы можете добавить инфопродукт* к своему проекту, и дать участникам доступ к нему после приобретения уровня.',
    'list_h_3' => 'Реинвесты',
    'list_desc_3' => 'При необходимости, вы можете полностью автоматизировать систему для участников, с помощью реинвестов.',
    'list_add_info' => '* - Для добавления какого-либо инфопродукта, у вас должны быть авторские права, или разрешение от автора на распространение.',


    'ready' => 'Ваш сайт уже почти готов',
    'ready_desc' => 'Вам осталось только зарегистрироваться и выбрать ссылку для Вашего проекта',


    'features' => '<h2 class="mb-4" id="about">Возможности сайта</h2>
                <h3>На нашем сайте вы можете настроить вашу МЛМ матрицу.</h3>
                <h3>Выберите количество уровней, ширину матрицы и любые из множества настроек. <b>На одном сайте может быть несколько разных тарифов!</b></h3>
                <img src="/iadminbest/public/img/landing/image1.png" alt="" style="border: 3px solid #ff8d00; border-radius: 10px; max-width: 100%;">
                ',
//                <h3>Например создать свою ферму:</h3>
//                <h3>К каждой категории добавить любое количество источников</h3>
//                <img src="/iadminbest/public/img/landing/image2.png" alt="" style="border: 3px solid #96dde8; border-radius: 10px; max-width: 100%;">

    'price' => 'Сколько стоят услуги?',
    'price_desc' => 'Вы можете начать абсолютно бесплатно. Никаких пробных версий и прочего, это действительно бесплатно. Однако с каждой транзакции Вашего сайта мы взымаем свою комиссию в размере 10%.',

    'footer' => '<div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">IADMIN.BEST</h2>
                        <p>Конструктор инвестиционных проектов.</p>
                    </div>
                </div>
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4 ml-5">
                        <h2 class="ftco-heading-2">Страницы</h2>
                        <ul class="list-unstyled">
                            <li><a href="/" class="py-2 d-block" style="color: #fff;">Главная</a></li>
                            <li><a href="{{route(\'login\')}}" class="py-2 d-block" style="color: #fff;">Войти</a></li>
                            <li><a href="{{route(\'register\')}}" class="py-2 d-block" style="color: #fff;">Регистрация</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">E-mail</h2>
                        <ul class="list-unstyled">
                            <li><a href="#" class="py-2 d-block" style="color: #fff;">support@iadmin.best</a></li>
                            <li><a href="https://vk.com/id527512598" target="_blank" class="py-2 d-block" style="color: #fff;">Вконтакте администратора</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md">
                </div>',

    'my_sites' => 'Мои сайты',
    'affiliate_programm' => 'Партнерская программа',
    'ref_link' => 'Ваша ссылка',
    'your_balance' => 'Ваш баланс',
    'affiliate_desc' => 'Вы получаете свой процент, от 10% пополнения сайта. Ваш процент зависит от ранга в программе лояльности. Всего 9 рангов.',
    'affiliate_th' => '<tr>
                            <th style="width: 10px">#</th>
                            <th>Название</th>
                            <th>Заработано до</th>
                            <th style="width: 40px">Процент отчислений</th>
                        </tr>',
    'rang_1' => 'Неактивный',
    'rang_2' => 'Новичок',
    'rang_3' => 'Младший рефовод',
    'rang_4' => 'Рефовод',
    'rang_5' => 'Младший мастер',
    'rang_6' => 'Мастер',
    'rang_7' => 'Младший гуру',
    'rang_8' => 'Гуру',
    'rang_9' => 'Ас',
    'your_rang' => 'Ваш ранг',

    'refferals_th' => '<tr>
                            <th style="width: 10px">#</th>
                            <th>Логин</th>
                            <th>Количество сайтов</th>
                            <th>Пополнений</th>
                        </tr>',
    'total_ref' => 'У Вас всего рефералов',
    'no_ref' => 'У вас нет ни одного реферала',

    'sites_th' => '<tr>
                            <th style="width: 10px">#</th>
                            <th>Название</th>
                            <th>Ссылка</th>
                            <th>Дата создания</th>
                        </tr>',

    'no_sites' => 'Вы еще не создали ни одного сайта',
    'create_site' => 'Создать сайт',
    'select_site' => '<option value="1">МЛМ/Матричный проект</option>
                        <option value="2">Сборник/Инвестиции/Хайп</option>
                        <option value="3">Игра с выводом денег/Ферма (NEW!)</option>',

    'documentation' => 'Документация',
    'on_panel' => 'Вкл/Выкл панель',

    'edit_panel' => 'Панель для редактирования сайта',
    'admin_panel' => 'Админ панель',
    'add_area' => 'Добавить область',
    'add_element' => 'Добавить элемент',
    'on_grid' => 'Вкл/Выкл сетку',
    'sort' => 'Изменить порядок',
    'setting_elements' => 'Настройка элементов',
    'edit_site' => 'Редактор сайта',

    'site_area' => 'Области сайта',
    'site_header' => 'Шапка сайта',
    'site_top_sidebar' => 'Верхнее меню',
    'site_left_menu' => 'Левое <br>меню <br>',
    'site_content' => 'Область <br>контента <br>',
    'site_right_menu' => 'Правое <br>меню <br>',
    'left_menu' => 'Левое меню',
    'right_menu' => 'Правое меню',
    'top_sidebar' => 'Верхнее меню',
    'bg_color' => 'Цвет фона',
    'width' => 'Ширина',
    'height' => 'Высота',
    'bg_img' => 'Картинка фон',
    'full_width' => 'На всю ширину',
    'setting_width' => 'Настройки ширины',
    'image' => 'Картинка',
    'border' => 'Рамка',
    'color' => 'Цвет',
    'type' => 'Тип',
    'border_radius' => 'Закругление углов',
    'top_left' => 'Верх слева',
    'top_right' => 'Верх справа',
    'bottom_left' => 'Низ слева',
    'bottom_right' => 'Низ справа',
    'main_setting' => 'Основные настройки',
    'main_bg' => 'Общий фон',
    'content_bg' => 'Фон области контента',
    'site_margin_top' => 'Отступ сайта сверху',
    'margin' => 'Отступ',

    'back_site' => 'Вернуться на сайт',
    'admin_main' => 'Главная',
    'admin_pages' => 'Страницы сайта',
    'admin_templates' => 'Шаблоны сайта',
    'admin_pages_auth' => 'Страницы авторизации',
    'admin_setting' => 'Настройки сайта',
    'admin_marks' => 'Маркетинги',
    'admin_finance' => 'Финансы сайта',
    'admin_content' => 'Контент сайта',
    'admin_faq' => 'FAQ',
    'admin_promo' => 'Промоматериалы',
    'admin_news' => 'Новости',
    'admin_edit_landing' => 'Редактировать главную',
    'admin_user_list' => 'Список пользователей',
    'admin_parking' => 'Парковка домена',
    'admin_edit_marks' => 'Редактирование маркетингов',
    'admin_docs' => 'Документация',
    'admin_questions' => 'У меня есть вопрос',
    'admin_telegram_bot' => 'Телеграм бот',

    'admin_economy' => 'Экономика сайта',
    'admin_sum_payment' => 'Всего пополнений',
    'admin_sum_cashout' => 'Всего выведено с проекта',
    'admin_sum_balance_site' => 'Баланс сайта',
    'admin_sum_balance_users' => 'Баланс участников',
    'admin_sum_reserved' => 'В резерве тарифов',
    'admin_sum_all' => 'Оборот проекта',

    'admin_statistic' => 'Статистика сайта',
    'admin_users' => 'Участников',
    'admin_users_active' => 'Активировано участников',
    'admin_activate' => 'Количество активаций',

    'admin_site_name' => 'Название сайта',
    'admin_site_logo' => 'Логотип сайта',
    'admin_load_logo' => 'Загрузить логотип',
    'admin_custom_avatar' => 'Стандартный аватар для пользователей',
    'admin_load_avatar' => 'Загрузить аватар',
    'admin_start_reg' => 'Дата старта регистраций',
    'admin_start_activation' => 'Дата старта активаций',
    'admin_cashout_limit' => 'Ограничения на вывод',
    'admin_more' => 'от',
    'admin_less' => 'до',
    'admin_cashout_before_start' => 'Вывод до старта',

    'admin_enable' => 'Включен',
    'admin_disable' => 'Выключен',

    'admin_enable_but' => 'Включен, но нельзя активировать по кнопке',
    'admin_under_admin' => 'Под админа',
    'admin_save' => 'Сохранить',
    'save' => 'Сохранить',

    'admin_tarif' => 'Тариф',
    'admin_status' => 'Статус',
    'admin_mark_name' => 'Название тарифа',
    'admin_overflow' => 'Переливы',
    'admin_mark_type' => 'Тип тарифа',
        'admin_mark_type_line' => 'Линейный(поочередная покупка уровней)',
        'admin_mark_type_multi' => 'Мульти(разовая покупка всех уровней)',

    'admin_width' => 'Ширина',
    'admin_levels' => 'Кол-во уровней',
    'required_mark' => 'Маркетинг обязателен',

    'admin_prices' => 'Цены на уровень и реинвесты',
    'admin_lev' => 'Ур.',
    'admin_price' => 'Цена',
    'admin_to_parent' => 'Приг',
    'admin_to_parent_desc' => 'Пригласителю. Укажите сколько получит лично пригласивший участник.',
    'admin_to_admin' => 'Адм',
    'admin_to_admin_desc' => 'Администратору. Укажите сколько получит администратор.',
    'admin_reinvest' => 'Реинвест (куда/количество)',
    'admin_reinvest_first' => 'В первый',
    'admin_reinvest_first_desc' => 'Укажите количество реинвестов в первый тариф.',
    'admin_priority' => 'Приоритет',
    'admin_priority_desc' => 'Выберите в каком порядке будут распределяться средства.',
        'admin_priority_1' => 'Вывод -> Повыш. ур. -> Реинвест -> Реинвест в первый',
        'admin_priority_2' => 'Вывод -> Реинвест -> Реинвест в первый -> Повыш. ур.',
        'admin_priority_3' => 'Вывод -> Реинвест в первый -> Реинвест -> Повыш. ур.',
        'admin_priority_4' => 'Реинвест в первый -> Реинвест -> Повыш. ур. -> Вывод',
        'admin_priority_5' => 'Реинвест -> Реинвест в первый -> Повыш. ур. -> Вывод',
        'admin_priority_6' => 'Повыш. ур. -> Вывод -> Реинвест -> Реинвест в первый',

    'admin_mark_enter' => 'Вход',
    'admin_mark_profit' => 'Доход',
    'admin_mark_level' => 'Уровень',
    'admin_mark_people' => 'Количество человек',
    'admin_mark_reinvest' => 'Реинвест',
    'admin_mark_reinvest_first' => 'Ренивест в первый тариф',
    'admin_total' => 'Итог',


    'admin_enable_w' => 'Включена',
    'admin_disable_w' => 'Выключена',

    'admin_your_pages' => 'Ваши созданные страницы',
    'admin_name' => 'Название',
    'admin_settings' => 'Настройки',
    'admin_delete_page' => 'Удалить страницу',
    'admin_delete_page_modal' => 'Если вы удалите эту страницу, то все виджеты, области и блоки которые были на этой будут удалены. Вы уверены что хотите продолжить?',
    'admin_accept' => 'Применить',
    'admin_add_page' => 'Добавить страницу',
    'admin_empty_link' => 'Если поле "Ссылка" оставить пустым то ссылка будет добавлена автоматически.',
    'admin_page_name' => 'Название страницы',
    'admin_page_link' => 'Ссылка',
    'admin_add' => 'Добавить',
    'close' => 'Закрыть',

    'payment_system' => 'Платежная система',
    'enter_amount' => 'Введите сумму',
    'amount' => 'Сумма',
    'wallet_for_withdraw' => 'Кошелек для вывода',
    'wallet' => 'Кошелек',

    'admin_you_templates' => 'Ваши созданные шаблоны',
    'admin_add_template' => 'Добавить шаблон',
    'admin_name_template' => 'Название шаблона',

    'admin_auth_setting' => 'Настроить страницы авторизации и регистрации',
    'admin_auth_sign_in' => 'Авторизация',
    'admin_auth_sign_up' => 'Регистрация',
    'admin_auth_password' => 'Восстановление пароля',
    'admin_auth_page' => 'Страница',
    'admin_auth_page_sign_in' => 'авторизации',
    'admin_auth_page_sign_up' => 'регистрации',
    'admin_auth_page_password' => 'восстановления пароля',

    'admin_design_header' => 'Оформление шапки',
    'admin_design_main_block' => 'Основной блок',
    'admin_design_inputs' => 'Оформление полей',

    'admin_edit_text' => 'Текст',
    'text' => 'Текст',

    'opacity' => 'Прозрачность',
    'text_color' => 'Цвет текста',
    'icon_color' => 'Цвет иконок',

    'setting_button' => 'Настройки кнопок',
    'decor' => 'Оформление',
    'btn_static' => 'В состоянии покоя',
    'btn_hover' => 'При наведении',
    'btn_active' => 'Активная',

    'align' => 'Выравнивание',
    'align_left' => 'По левому краю',
    'align_center' => 'По центру',
    'align_right' => 'По правому краю',

    'border_solid' => 'Сплошная',
    'border_double' => 'Двойная',
    'border_dashed' => 'Пунктир',
    'border_dotted' => 'Точечная',
    'border_inset' => 'Выпуклая',
    'border_unset' => 'Выемка',

    'pagination' => 'Пагинация(нумерация страниц)',
    'send_link' => 'Отправить ссылку для восстановления',


    'modal_add_area' => 'Добавить область',
    'modal_name_area' => 'Название области',
    'modal_place_area' => 'Где будет область',

    'in_main' => 'В основной области',
    'in_left_menu' => 'В левом меню',
    'in_top_menu' => 'В верхнем меню',
    'in_right_menu' => 'В правом меню',

    'modal_add_widget' => 'Добавить виджет',
    'modal_name_widget' => 'Название блока',
    'modal_head_widget' => 'Заголовок блока',
    'modal_place_widget' => 'В какой области',
    'modal_create_area_on_page' => 'Создайте область на этой странице',
    'modal_widget' => 'Виджет',

    'modal_edit_block' => 'Редактировать блок',
    'modal_editor_block' => 'Редактируемый блок',
    'modal_setting_block' => 'Настройки блока',
    'modal_setting_widget' => 'Настройки виджета',

    'widg_activate' => 'Активация тарифов',
    'widg_tarifs' => 'Мои тарифы',
    'widg_store' => 'Тарифы для инвестиций',
    'widg_mydeposits' => 'Мои вклады',
    'widg_bonusclick' => 'Бонус за клик',
    'widg_exchange' => 'Обмен баланса "Для вывода" на баланс "Для покупок',
    'widg_profilebox' => 'Аватар с информацией',
    'widg_transaction' => 'Блок с операциями',
    'widg_balance' => 'Форма пополнения',
    'widg_referrals' => 'Список рефералов',
    'widg_news' => 'Новости',
    'widg_faq' => 'Частозадаваемые вопросы',
    'widg_promo' => 'Промоматериалы',
    'widg_profileedit' => 'Редактирование профиля',
    'widg_cashout' => 'Форма вывода',
    'widg_content' => 'С текстом',
    'widg_horizontalmenu' => 'Горизонтальное меню',
    'widg_verticalmenu' => 'Вертикальное меню',

    'information_user' => 'Информация о пользователе',
    'information_main' => 'Основная информация',
    'information_referer' => 'Информация о кураторе',
    'information_site' => 'Информация о сайте',
    'information_tariff' => 'Информация о тарифе',
    'value_images' => 'Изображения',
    'social_network' => 'Социальные сети',

    'without_icon' => 'Без картинки',

    'value_login' => 'Логин',
    'value_balance' => 'Баланс</option',
    'value_count_ref' => 'Количество рефералов',
    'value_email' => 'Email(почта',
    'value_link_visit' => 'Переходы по ссылке',
    'value_profit' => 'Заработано',
    'value_percent_sum_user' => '10% от заработка участника',
    'value_last_active' => 'Последняя активность',
    'value_vk' => 'Вконтакте',
    'value_twitter' => 'Twitter',
    'value_facebook' => 'Facebook',
    'value_instagram' => 'Instagramm',
    'value_skype' => 'Skype',
    'value_telegram' => 'Telegram',
    'value_text_status' => 'Текст статуса',
    'value_login_parent' => 'Логин куратора',
    'value_link_visit_parent' => 'Переходы по ссылке',
    'value_count_ref_parent' => 'Количество рефералов',
    'value_profit_parent' => 'Заработано',
    'value_users_count' => 'Зарегистрировано человек',
    'value_activate_count' => 'Количество активаций',
    'value_transaction_sum' => 'Оборот проекта',
    'value_percent_sum' => '10% от оборота',
    'value_balance_cashout' => 'Баланс для вывода',
    'value_affiliate_profit' => 'Заработано с реферала',
    'value_affiliate_profit_all' => 'Заработано по партнерской программе',

    'value_avatar' => 'Аватар',
    'value_name' => 'Имя',
    'value_lastname' => 'Фамилия',
    'value_fathername' => 'Отчество',
    'value_password' => 'Пароль',
    'value_telephone' => 'Номер телефона',

    'value_mark_name' => 'Название тарифа',
    'value_mark_profit' => 'Заработано на тарифе',
    'value_mark_profit_all' => 'Общий доход тарифа',
    'value_mark_balance' => 'Резерв',
    'value_mark_more' => 'Кнопка подробнее',
    'value_avatar_parent' => 'Аватар куратора',
    'value_site_logo' => 'Логотип сайта',

    'block_for_menu' => 'Блок для меню',
    'block_for_custom' => 'Обычный блок',

    'top' => 'Сверху',
    'bottom' => 'Снизу',
    'left' => 'Слева',
    'right' => 'Справа',

    'create' => 'Создать',

    'main_page' => 'Главная сайте',
    'logout_page' => 'Выход из аккаунта',
    'profile_page' => 'Главная профиля',
    'connect_ibux_page' => 'Страница привязки iBux.best',

    'id_transaction' => 'ID операции',
//    'amount' => 'Сумма',
    'from_user' => 'От кого',
    'to_user' => 'Кому',
//    'type' => 'Тип',
    'date' => 'Дата',
    'deposit_name' => 'Название вклада',
    'deposit_number' => 'Размер вклада',
    'deposit_resource' => 'Что зарабатываем',
    'deposit_seconds' => 'Осталось секунд',
    'deposit_balance' => 'Баланс вклада',
    'deposit_stock' => 'В резерве',
    'deposit_btn' => 'Кнопка "Забрать средства"',

    'invest_name' => 'Название',
    'invest_price' => 'Цена/Диапазон цены',
    'invest_second' => 'Секунды',
    'invest_second_to_minute' => 'Минуты',
    'invest_second_to_hour' => 'Часы',
    'invest_second_to_day' => 'Дни',
    'invest_store_profit_second' => 'В секунду',
    'invest_store_profit_minute' => 'В минуту',
    'invest_store_profit_hour' => 'В час',
    'invest_store_profit_day' => 'В день',
    'invest_store_profit_all' => 'Всего(если срок жизни ограничен)',
    'invest_number' => 'Сколько этой позиции есть у пользователя',
    'invest_store_btn' => 'Кнопка "Сделать вклад"',

    'time' => 'Время',
    'profit' => 'Доход',

    'padding_button' => 'Внутренние отступы кнопок',
    'design_button' => 'Оформление кнопок',
    'mains' => 'Основные',
    'configure' => 'Настроить',

    'ask' => 'Вопрос',
    'answer' => 'Ответ',
    'modal_main_setting_widget' => 'Основные настройки виджета',
    'number_records' => 'Количество записей на странице',
    'only_first_page' => 'Отображать только первую страницу',
    'what_display' => 'Что отображать',
    'top_button' => 'Кнопки сверху',
    'button_more' => 'Кнопки "Подробнее"',
    'elements' => 'Элементы',

    'elements_with_tariff' => 'Элемены с тарифами',
    'dividing_lines' => 'Разделительные линии',
    'font' => 'Шрифт',
    'table_color' => 'Цвета таблицы',
    'table_head_color' => 'Цвет заголовка таблицы',

    'static_color' => 'Цвет в состоянии покоя',
    'hover_color' => 'Цвет при наведении',

    'head_news' => 'Заголовок новости',
    'date_color' => 'Цвет даты',

    'setting_time_line' => 'Настройка таймлайна(линия слева)',
    'area_date' => 'Область с датой',
    'color_line' => 'Цвет линии',
    'in_area_date' => 'в области даты',
    'in_area_icon' => 'в области иконки',
    'text_news' => 'Цвет новости',
    'footer_news' => 'Нижняя часть',
    'design_btn' => 'Оформление кнопки',

    'place_avatar' => 'Место аватара и логина',
    'elements_color' => 'Цвета элементов',

    'free_place_naming' => 'Название свободного места',
    'level_up_view' => 'Просмотр верха структуры',

    'text_left' => 'Текст слева',
    'text_right' => 'Текст справа',
    'btn' => 'Кнопка',

    'design_banner' => 'Оформление баннера',
    'design_head_banner' => 'Оформление заголовка баннера',
    'design_element_banner' => 'Общее оформление елемента баннера',

    'padding' => 'Отступы',

    'design_input_with_code' => 'Оформление поля с кодом',

    'show_random_ref' => 'Показать случайных рефералов',
    'active_ref' => 'Активированные рефералы',
    'non_active_ref' => 'Неактивированные рефералы',

    'column_with_value' => 'Столбцы со значениями',

    'users_color' => 'Цвета пользователей',

    'activity' => 'Активированные',
    'unactivity' => 'Не активированные',

    'setting_list_tariff' => 'Настройки списка тарифов',
    'setting_ref_tree' => 'Настройки реферального дерева',
    'ref_tree' => 'Реферальное дерево',
    'busy_place' => 'Занятое место',
    'busy_places' => 'Занятые места',
    'free_place' => 'Свободное место',
    'free_places' => 'Свободные места',

    'element_transaction' => 'Элементы операций',
    'color_transaction' => 'Цвета транзакций',

    'out_transaction' => 'Исходящие транзакции',
    'in_transaction' => 'Входящие транзакции',

    'transaction_payments_order' => 'Внутренние переводы',
    'transaction_cashout' => 'Выводы из системы',
    'transaction_payment' => 'Пополнения',
    'head' => 'Заголовок',
    'bg' => 'фон',

    'margins' => 'Отступы от соседних блоков',
    'margin_left' => 'Отступ слева',
    'margin_right' => 'Отступ справа',

    'admin_edit_promo' => 'Редактировать промоматериал',
    'admin_create_promo' => 'Создать промоматериал',
    'link_image' => 'Прямая ссылка на картинку',
    'width_banner' => 'Ширина баннера',
    'height_banner' => 'Высота баннера',
    'list_promo' => 'Список промоматериалов',

    'faq' => 'Частозадаваемые вопросы',
    'create_ask' => 'Добавить вопрос',

    'edit' => 'Редактировать',
    'delete' => 'Удалить',

    'create_news' => 'Создать новость',
    'bg_color_icon' => 'Цвет фона иконки',
    'icon' => 'Иконка',
    'publish' => 'Опубликовать',

    'if_working' => 'Если Ваш сайт работает, подтвердите то что домен делигирован успешно',
    'delegate' => 'Делегирован',
    'parking' => 'Припарковать домен',
    'enter_domain' => 'Введите домен',

    'find_user' => 'Найти пользователя',
    'login_or_id' => 'Логин или ID',
    'start_enter' => 'Начните вводить данные',

    'user_surname' => 'Фамилия',
    'user_name' => 'Имя',
    'user_fathername' => 'Отчество',
    'user_date_register' => 'Дата регистрации',
    'user_last_active' => 'Последняя активность',
    'user_count_ref' => 'Количество рефералов',
    'user_who_invite' => 'Кто пригласил',
    'user_balance' => 'Баланс',

    'user_privilege' => 'Привилегия',
    'user_buys' => 'Покупки',
    'user_not_buys' => 'У пользователя нет покупок',

    'for_cashout' => 'Для вывода',
    'for_buy' => 'Для покупок',
    'to_list_users' => 'К списку пользователей',
    'back' => 'Назад',
    'user_not_fount' => 'Такого пользователя не существует',

    'all_payments' => 'Все пополнения',
    'user' => 'Пользователь',

    'privilege_banned' => 'Забаненый пользователь',
    'privilege_custom' => 'Обычный пользователь',
    'privilege_before_start' => 'Может активировать до старта',

    'activations' => 'Активации',
    'tariff' => 'Тариф',

    'no_activation' => 'У этого пользователя нет активаций',
    'no_payments' => 'У этого пользователя нет пополнений',
    'no_cashout' => 'У этого пользователя нет выводов',

    'payments' => 'Пополнения',
    'cashout' => 'Выводы',

    'font_size' => 'Размер шрифта',
    'font_setting' => 'Настройки шрифта',



    'land_edit_mode' => 'РЕЖИМ РЕДАКТИРОВАНИЯ',
    'land_elements' => 'Элементы',
    'land_content' => 'Контент',
    'land_detalization' => 'Детально',
    'land_no_changes' => 'Нет изменений для сохранения',
    'land_templates' => 'Шаблоны',
    'land_preview' => 'Предпросмотр',
    'land_page_clear' => 'Очистить страницу',
    'land_clear_cache' => 'Очистить кэш браузера',
    'land_create_your_page_by' => 'Создайте свою страницу перетягивая элементы в эту область',
    'land_all_blocks' => 'Все блоки',
    'land_header' => 'Шапка',
    'land_introduction' => 'Введение',
    'land_counters' => 'Счетчики',
    'land_additions' => 'Дополнения',
    'land_pop_ups' => 'Попапы',
    'land_text_blocks' => 'Текстовые блоки',
    'land_reviews' => 'Отзывы',
    'land_btnbl' => 'Блоки с кнопками',
    'land_clients' => 'Клиенты',
    'land_social_networks' => 'Социальные сети',
    'land_team' => 'Команда',
    'land_tariffs' => 'Тарифы',
    'land_sliders' => 'Слайдеры',
    'land_forms' => 'Формы',
    'land_footer' => 'Подвал',
    'land_pages' => 'Страницы',
    'land_add_new' => 'Добавить',
    'land_page_sections' => 'Разделы страницы',
    'land_edit_css' => 'Изменить CSS',
    'land_editing' => 'Изменение',
    'land_styles' => 'Стили',
    'land_bg_img' => 'Фоновое изображение',
    'land_bg_color' => 'Цвет фона',
    'land_top_field' => 'Верхнее поле',
    'land_values' => 'Значения',
    'land_bottom_field' => 'Нижнее поле',
    'land_shadow' => 'Тень',
    'land_border_width' => 'Ширина рамки',
    'land_border_radius' => 'Скругление рамки',
    'land_border_color' => 'Цвет рамки',
    'land_img_size' => 'Размер изображения',
    'land_img_fix_type' => 'Фиксация изображения',
    'land_bg_img_repeat' => 'Повтор фонового изображения',
    'land_save_changes' => 'Сохранить изменения',
    'land_copy' => 'Скопировать',
    'land_reset' => 'Сбросить',
    'land_delete' => 'Удалить',
    'land_img' => 'Изображение',
    'land_input_img_address' => 'Введите путь к картинке',
    'land_enter_img_url' => 'Введите URL адрес картинки',
    'land_or' => 'ИЛИ',
    'land_upld_img' => 'Загрузить изображение',
    'land_choose_img' => 'Выбрать изображение',
    'land_animation' => 'Анимация',
    'land_choose_anmt_type' => 'Выберите тип анимации',
    'land_anmt_delay' => 'Задержка анимации',
    'land_anmt_time' => 'Время выполнения анимации',
    'land_def_1sec' => 'По умолчанию (1 секунда)',
    'land_slow_mo2sec' => 'slow-mo (2 секунды)',
    'land_color' => 'Цвет',
    'land_font_size' => 'Размер шрифта',
    'land_bg_clr' => 'Цвет фона',
    'land_icns' => 'Иконки',
    'land_transparency' => 'Прозрачность',
    'land_font_family' => 'Семейство шрифтов',
    'land_chngs_svd' => 'Изменения сохранены',
    'land_seo_stngs_fr' => 'Настройки SEO для',
    'land_pg_hdl' => 'Заголовок страницы',
    'land_pg_dscrpt' => 'Описание страницы',
    'land_keywords' => 'Ключевые слова',
    'land_add_to_hdr' => 'Добавить в header',
    'land_extracod_head' => 'Дополнительный код, который вы хотите добавить в блок <head>',
    'land_save_seo_set' => 'Сохранить настройки SEO',
    'land_cancel' => 'Отмена',
    'land_chs_ext_tmp' => 'Выберите существующий шаблон',
//    'land_preview' => 'Предварительный просмотр',
    'land_attntn' => 'Пожалуйста прочтите:',
    'land_dnt_shw_ths_agn' => 'Не показывать это окно снова.',
    'land_shw_prvw' => 'Показать превью',
    'land_nmbr_act_usrs' => 'Количество активных пользователей',
    'land_ttl_fnn_trn' => 'Общий оборот средств',
    'land_trnsct_nmbr' => 'Количество переводов',
    'land_dz_ppsd' => 'Прошло дней от старта проекта',
    'land_all_usrs' => 'Общее количество пользователей',
    'land_ttl_pouts' => 'Выведено всего с проекта',
    'land_bgclr' => 'Цвет фона',
    'land_fnt_clr' => 'Цвет шрифта',
    'land_transparent' => 'Прозрачный',
    'land_dfltt' => 'Восстановить умолчания',
    'land_blct' => 'дублировать',
    'land_code' => 'код',
    'land_upload' => 'Загрузить',
    'video' => 'Видео',
    'input' => 'Поле',
    'checkbox' => 'Чекбокс',
    'form' => 'Форма',
    'styles' => 'Стили',
    'change' => 'Изменение',
    'land_ntc_txt1' => 'вы можете просматривать только одну страницу; ссылки на другие страницы не будут работать. Когда вы вносите изменения на свою страницу, перезагрузка предварительного просмотра не будет работать, вместо этого вам придется снова использовать кнопку «Предпросмотр».',
    'load_elements_constructor' => 'Загрузка элементов конструктора',


    'choose_page' => 'Выберите страницу',
    'page_profile' => 'Профиль',
    'page_logout' => 'Выход',
    'or' => 'или',
    'choose_block' => 'Выберите блок (на странице сайта)',
    'choose_image' => 'Выбрать изображение',
    'choose' => 'Выбрать',

    'js_builder' => 'builder.js',

    'without_reinvest' => 'Без реинвеста',
    'number_first_reinvest' => 'Укажите количество реинвестов в первый тариф.',
    'add' => 'Добавить',
    'add_promo' => 'Добавить промоматериал',

    'crypto_payment' => '<strong>Внимание!</strong> Комиссия за пополнение составляет 0.5% <br><strong>Внимание!</strong> Не совершайте более одного перевода на один кошелек, после соверешения перевода, вам будет выдан новый кошелек',
    'crypto_address' => 'Для пополнения воспользуйтесь указанным ниже адресом',

    'access' => 'Доступ',
    'no_access' => 'У вас нет доступа к этой странице',


    'error_start' => 'Активации не доступны. Дождитесь старта проекта.',

    'success_activated' => 'Тариф активирован',

    'admin_cashout_limit_day' => 'В сутки',
    'admin_cashout_limit_day_all' => 'Сутки на всем сайте',

    'error_cashout_limit_day' => 'Лимит на вывод :qwe',

    'main.access_refer' => 'Для доступа необходимо :condition рефералов. У вас :count',
    'main.access_link_visit' => 'Для доступа необходимо :condition переходов по ссылке. У вас :count',
    'main.access_activate' => 'Для доступа нужно активировать любой тариф',
    'main.access_activate_number' => 'Для доступа нужно активировать тариф: :condition',

    'auth_page_login' => 'Страница авторизации',
    'auth_page_register' => 'Страница регистрации',

    'auto_activate' => 'Автоактивации',
    'activates' => 'Активации',

    'item_changed' => 'Элемент изменен',
    'design_changed' => 'Офорфмление сайта изменено',

    'mark_saved' => 'Маркетинг сохранен',
    'details' => 'Подробнее',

    'to_inviters' => 'Пригласителям(кураторам) при активации тарифа.',


    'register_login_required' => "Поле логин, обязательно для заполнения.",
    'register_login_regex' => "Логин должен состоять из латинских букв и цифр.",
    'register_login_max' => "Придумайте логин до 20 символов.",
    'register_login_unique' => "Этот логин используется уже другим пользователем.",
    'register_email_required' => "Поле Email, обязательно для заполнения.",
    'register_email_max' => "Слишком длинный email.",
    'register_email_unique' => "Этот email уже используется. <a href='/password/reset'>Восстановить пароль</a>",
    'register_password_required' => "Пароль обязателен для заполнения",
    'register_password_confirmed' => "Пароли не совпадают",
    'register_password_min' => "Минимальная длина пароля 6 символов",
    'register_terms_required' => 'Вы не приняли пользовательское соглашение',
    'register_terms_accepted' => 'Вы не приняли пользовательское соглашение',

    'new_password' => 'Новый пароль',
    'confirm_new_password' => 'Повторите новый пароль',

    'yes' => 'Да',
    'no' => 'Нет',
];
