<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароли должны совпадать и иметь не менее 6 символов',
    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'На ваш email была отправлена ссылка для восстановления!',
    'token' => 'Токен для данного пользователя неверен, либо пользователь не существует',
    'user' => "Мы не нашли пользователя с указанным email.",

];
