@extends('layout.core.auth')
@section('class-page', 'login-page')

@section('content')
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="login-box-body p-0">
            <h2>{{__('main.login')}}</h2>
            @if (count($errors) > 0)
                @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{{$error}}</p>
                @endforeach
            @endif
            <div class="col-lg-12">
                <form action="{{route('login')}}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group has-feedback">
                        <input type="text" name="login" class="form-control" value="{{old('login')}}" placeholder="{{__('main.login_name')}}">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="{{__('main.password')}}">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox">
                                <input type="checkbox" name="remember" id="remember">
                                <label for="remember">{{__('main.remember_me')}}</label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-flat btn-block btn-flat">{{__('main.login')}}</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <div class="ta-center mt-30">
                    <a href="{{route('password.request')}}" class="btn btn-warning btn-flat">{{__('main.reset_password')}}</a><br>
                    <a href="{{route('register')}}" class="text-center btn btn-primary btn-flat mt-10">{{__('main.registration')}}</a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
@endsection
