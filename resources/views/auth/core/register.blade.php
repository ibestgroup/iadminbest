@extends('layout.core.auth')
@section('page-class', 'register-page')

@section('content')
    <div class="register-box">

        <div class="register-box-body p-0">
            <h2>{{__('main.registration')}}</h2>
            @if (count($errors) > 0)
                @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{!! $error !!}</p>
                @endforeach
            @endif
            <div class="col-lg-12" style="padding-bottom: 20px; padding-top: 20px;">
                <form action="{{route('register')}}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="refer_login" value="{{\Illuminate\Support\Facades\Cookie::get('refer_login')}}">
                    <input type="hidden" name="refer" value="{{\Illuminate\Support\Facades\Cookie::get('refer_id')}}">
                    <div class="form-group has-feedback">
                        <input type="text" name="login" class="form-control" placeholder="Ваш логин">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Пароль">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password_confirmation" class="form-control" placeholder="Повторите пароль">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="checkbox icheck">
                                <input id="user-terms" type="checkbox" name="terms">
                                <label for="user-terms">Я принимаю <a href="#">пользовательское соглашение</a></label>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="form-group has-feedback">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Регистрация</button>
                    </div>
                </form>

                <div class="ta-center">
                    <a href="{{route('login')}}" class="text-center btn btn-info btn-flat">У меня уже есть аккаунт</a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /.form-box -->
    </div>
    <!-- /.register-box -->
@endsection