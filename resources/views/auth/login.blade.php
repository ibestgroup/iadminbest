@extends('layout.core.auth')
@section('class-page', 'login-page')

@section('content')
    @inject('helper', "\App\Libraries\Helpers")
    @php
        $template = \App\Libraries\Helpers::siteTemplate();
        $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);
        $styleId = \App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['id'];
    @endphp
    <style>
        .wrap-login-box {
            margin: auto;
            margin-top: {{$mainStyle['main_style']['margin_top']}}px;
            border: {{$mainStyle['main_style']['border']['width']}}px {{$mainStyle['main_style']['border']['style']}} {{$mainStyle['main_style']['border']['color']}};
            border-radius: {{$mainStyle['main_style']['border_radius']['tl']}}px {{$mainStyle['main_style']['border_radius']['tr']}}px {{$mainStyle['main_style']['border_radius']['br']}}px {{$mainStyle['main_style']['border_radius']['bl']}}px;
            overflow: hidden;
            background-color: {{$mainStyle['main_style']['bg_color_content']}};
        }
    </style>
<div class="login-box">
    @php
        $bgColorBtn = (isset($mainStyle['login_style']['bg_color_btn']))?$mainStyle['login_style']['bg_color_btn']:'';
        $colorBtn = (isset($mainStyle['login_style']['color_btn']))?$mainStyle['login_style']['color_btn']:'';
        $bgColorBtnH = (isset($mainStyle['login_style']['bg_color_btn_h']))?$mainStyle['login_style']['bg_color_btn_h']:'';
        $colorBtnH = (isset($mainStyle['login_style']['color_btn_h']))?$mainStyle['login_style']['color_btn_h']:'';

        $borderRadiusBtn = $helper->getProperty('border_radius', (isset($mainStyle['login_style']['border_radius_btn']))?$mainStyle['login_style']['border_radius_btn']:'');
        $borderRadiusBtnH = $helper->getProperty('border_radius', (isset($mainStyle['login_style']['border_radius_btn_h']))?$mainStyle['login_style']['border_radius_btn_h']:'');
        $borderBtn = $helper->getProperty('border', (isset($mainStyle['login_style']['border_btn']))?$mainStyle['login_style']['border_btn']:'');
        $borderBtnH = $helper->getProperty('border', (isset($mainStyle['login_style']['border_btn_h']))?$mainStyle['login_style']['border_btn_h']:'');
    @endphp
    <style>

        .login-box {
            border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['login_style']['border_radius']))?$mainStyle['login_style']['border_radius']:'')}};
            border: {{$helper->getProperty('border', (isset($mainStyle['login_style']['border']))?$mainStyle['login_style']['border']:'')}};
            font: {!! $helper->getProperty('font', (isset($mainStyle['login_style']['font']))?$mainStyle['login_style']['font']:'') !!};
        }

        .login-box-body {
            background: {{(isset($mainStyle['login_style']['bg_color_body']))?$mainStyle['login_style']['bg_color_body']:''}};
        }

        .login-box h2{
            background: {{(isset($mainStyle['login_style']['bg_color']))?$mainStyle['login_style']['bg_color']:''}};
            height: {{(isset($mainStyle['login_style']['height']))?$mainStyle['login_style']['height']:''}}px;
            line-height: {{(isset($mainStyle['login_style']['height']))?$mainStyle['login_style']['height']:''}}px;
            font-family: {!! $mainStyle['login_style']['font']['family'] ?? '' !!};
        }

        .login-box input {
            background: {{(isset($mainStyle['login_style']['bg_color_input']))?$mainStyle['login_style']['bg_color_input']:''}};
            color: {{(isset($mainStyle['login_style']['color_input']))?$mainStyle['login_style']['color_input']:''}};
            border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['login_style']['border_radius_input']))?$mainStyle['login_style']['border_radius_input']:'')}};
            border: {{$helper->getProperty('border', (isset($mainStyle['login_style']['border_input']))?$mainStyle['login_style']['border_input']:'')}};
        }
        .login-box .form-control-feedback {
            color: {{(isset($mainStyle['login_style']['color_input_icon']))?$mainStyle['login_style']['color_input_icon']:''}};
        }
        .login-box .btn {
            background-color: {{$bgColorBtn}};
            color: {{$colorBtn}};
            border-radius: {{$borderRadiusBtn}};
            border: {{$borderBtn}};
        }
        .login-box .btn:hover {
            background-color: {{$bgColorBtnH}};
            color: {{$colorBtnH}};
            border-radius: {{$borderRadiusBtnH}};
            border: {{$borderBtnH}};
        }
    </style>
    <!-- /.login-logo -->
    <div class="login-box-body p-0">
        <h2>{{(isset($mainStyle['login_style']['text']))?$mainStyle['login_style']['text']:__('main.login')}}</h2>
        @if (count($errors) > 0)
            @foreach($errors->all() as $error)
                <p class="alert alert-danger">{{$error}}</p>
            @endforeach
        @endif
        <div class="col-lg-12">
            <form action="{{route('login')}}" method="post">
                {{ csrf_field() }}
                <div class="form-group has-feedback">
                    <input type="text" name="login" class="form-control" value="{{old('login')}}" placeholder="{{__('main.login_name')}}">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="{{__('main.password')}}">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox">
                            <input type="checkbox" name="remember" id="remember">
                            <label for="remember">{{__('main.remember_me')}}</label>
                        </div>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-flat btn-block btn-flat">{{__('main.login')}}</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
            <div class="ta-center mt-30">
                <a href="{{route('password.request')}}" class="btn btn-warning btn-flat">{{__('main.reset_password')}}</a><br>
                <a href="{{route('register')}}" class="text-center btn btn-primary btn-flat mt-10">{{__('main.registration')}}</a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@endsection
