@extends('layout.core.auth')
@section('class-page', 'login-page')

@section('content')
    @inject('helper', "\App\Libraries\Helpers")
    @php
        $template = \App\Libraries\Helpers::siteTemplate();
        $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);
        $styleId = \App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['id'];
    @endphp
    <style>
        .wrap-login-box {
            margin: auto;
            margin-top: {{$mainStyle['main_style']['margin_top']}}px;
            border: {{$mainStyle['main_style']['border']['width']}}px {{$mainStyle['main_style']['border']['style']}} {{$mainStyle['main_style']['border']['color']}};
            border-radius: {{$mainStyle['main_style']['border_radius']['tl']}}px {{$mainStyle['main_style']['border_radius']['tr']}}px {{$mainStyle['main_style']['border_radius']['br']}}px {{$mainStyle['main_style']['border_radius']['bl']}}px;
            overflow: hidden;
            background-color: {{$mainStyle['main_style']['bg_color_content']}};
        }
    </style>
    <div class="restore-box login-box">


        @php
            $bgColorBtn = (isset($mainStyle['restore_style']['bg_color_btn']))?$mainStyle['restore_style']['bg_color_btn']:'';
            $colorBtn = (isset($mainStyle['restore_style']['color_btn']))?$mainStyle['restore_style']['color_btn']:'';
            $bgColorBtnH = (isset($mainStyle['restore_style']['bg_color_btn_h']))?$mainStyle['restore_style']['bg_color_btn_h']:'';
            $colorBtnH = (isset($mainStyle['restore_style']['color_btn_h']))?$mainStyle['restore_style']['color_btn_h']:'';

            $borderRadiusBtn = $helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius_btn']))?$mainStyle['restore_style']['border_radius_btn']:'');
            $borderRadiusBtnH = $helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius_btn_h']))?$mainStyle['restore_style']['border_radius_btn_h']:'');
            $borderBtn = $helper->getProperty('border', (isset($mainStyle['restore_style']['border_btn']))?$mainStyle['restore_style']['border_btn']:'');
            $borderBtnH = $helper->getProperty('border', (isset($mainStyle['restore_style']['border_btn_h']))?$mainStyle['restore_style']['border_btn_h']:'');
        @endphp
        <style>

            .restore-box {
                border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius']))?$mainStyle['restore_style']['border_radius']:'')}};
                border: {{$helper->getProperty('border', (isset($mainStyle['restore_style']['border']))?$mainStyle['restore_style']['border']:'')}};
            }

            .restore-box-body {
                background: {{(isset($mainStyle['restore_style']['bg_color_body']))?$mainStyle['restore_style']['bg_color_body']:''}};
            }

            .restore-box h2{
                background: {{(isset($mainStyle['restore_style']['bg_color']))?$mainStyle['restore_style']['bg_color']:''}};
                height: {{(isset($mainStyle['restore_style']['height']))?$mainStyle['restore_style']['height']:''}}px;
                line-height: {{(isset($mainStyle['restore_style']['height']))?$mainStyle['restore_style']['height']:''}}px;
            }

            .restore-box input {
                background: {{(isset($mainStyle['restore_style']['bg_color_input']))?$mainStyle['restore_style']['bg_color_input']:''}};
                color: {{(isset($mainStyle['restore_style']['color_input']))?$mainStyle['restore_style']['color_input']:''}};
                border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius_input']))?$mainStyle['restore_style']['border_radius_input']:'')}};
                border: {{$helper->getProperty('border', (isset($mainStyle['restore_style']['border_input']))?$mainStyle['restore_style']['border_input']:'')}};
            }
            .restore-box .form-control-feedback {
                color: {{(isset($mainStyle['restore_style']['color_input_icon']))?$mainStyle['restore_style']['color_input_icon']:''}};
            }
            .restore-box .btn {
                background-color: {{$bgColorBtn}};
                color: {{$colorBtn}};
                border-radius: {{$borderRadiusBtn}};
                border: {{$borderBtn}};
            }
            .restore-box .btn:hover {
                background-color: {{$bgColorBtnH}};
                color: {{$colorBtnH}};
                border-radius: {{$borderRadiusBtnH}};
                border: {{$borderBtnH}};
            }
        </style>
        <!-- /.login-logo -->
        <div class="restore-box-body p-0">
            <h2>
                Восстановление пароля
            </h2>
            <p class="restore-box-msg">Напишите email который вы указывали при регистрации</p>
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="col-lg-12">
                <form action="{{route('password.email')}}" aria-label="{{ __('Reset Password') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email..." required>
                        <label for="email" class="glyphicon glyphicon-envelope form-control-feedback"></label>
                        @if ($errors->has('email'))
                            <span class="help-block">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                    <div class="form-group has-feedback">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                            {{ __('Отправить ссылку для восстановления') }}
                        </button>
                    </div>
                </form>
                <div class="ta-center mt-30">
                    <a href="{{route('login')}}" class="btn btn-flat btn-success">{{ __('main.login') }}</a><br>
                    <a href="{{route('register')}}" class="text-center btn btn-flat btn-primary mt-10">{{ __('main.registration') }}</a>
                </div>
            </div>
            <div class="clearfix"></div>

        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
@endsection
