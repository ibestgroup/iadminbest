@extends('layout.core.auth')

@section('content')
    @inject('helper', "\App\Libraries\Helpers")
    @php
        $template = \App\Libraries\Helpers::siteTemplate();
        $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);
        $styleId = \App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['id'];
    @endphp
    <style>
        .wrap-login-box {
            margin: auto;
            margin-top: {{$mainStyle['main_style']['margin_top']}}px;
            border: {{$mainStyle['main_style']['border']['width']}}px {{$mainStyle['main_style']['border']['style']}} {{$mainStyle['main_style']['border']['color']}};
            border-radius: {{$mainStyle['main_style']['border_radius']['tl']}}px {{$mainStyle['main_style']['border_radius']['tr']}}px {{$mainStyle['main_style']['border_radius']['br']}}px {{$mainStyle['main_style']['border_radius']['bl']}}px;
            overflow: hidden;
            background-color: {{$mainStyle['main_style']['bg_color_content']}};
        }
    </style>
{{--<div class="container">--}}
    {{--<div class="row justify-content-center">--}}
        {{--<div class="col-md-8">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">{{ __('Reset Password') }}</div>--}}

                {{--<div class="card-body">--}}
                    {{--<form method="POST" action="{{ route('password.request') }}" aria-label="{{ __('Reset Password') }}">--}}
                        {{--@csrf--}}

                        {{--<input type="hidden" name="token" value="{{ $token }}">--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>--}}

                                {{--@if ($errors->has('email'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>--}}

                                {{--@if ($errors->has('password'))--}}
                                    {{--<span class="invalid-feedback" role="alert">--}}
                                        {{--<strong>{{ $errors->first('password') }}</strong>--}}
                                    {{--</span>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row">--}}
                            {{--<label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

                            {{--<div class="col-md-6">--}}
                                {{--<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group row mb-0">--}}
                            {{--<div class="col-md-6 offset-md-4">--}}
                                {{--<button type="submit" class="btn btn-primary">--}}
                                    {{--{{ __('Reset Password') }}--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

<div class="restore-box">

    @php
        $bgColorBtn = (isset($mainStyle['restore_style']['bg_color_btn']))?$mainStyle['restore_style']['bg_color_btn']:'';
        $colorBtn = (isset($mainStyle['restore_style']['color_btn']))?$mainStyle['restore_style']['color_btn']:'';
        $bgColorBtnH = (isset($mainStyle['restore_style']['bg_color_btn_h']))?$mainStyle['restore_style']['bg_color_btn_h']:'';
        $colorBtnH = (isset($mainStyle['restore_style']['color_btn_h']))?$mainStyle['restore_style']['color_btn_h']:'';

        $borderRadiusBtn = $helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius_btn']))?$mainStyle['restore_style']['border_radius_btn']:'');
        $borderRadiusBtnH = $helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius_btn_h']))?$mainStyle['restore_style']['border_radius_btn_h']:'');
        $borderBtn = $helper->getProperty('border', (isset($mainStyle['restore_style']['border_btn']))?$mainStyle['restore_style']['border_btn']:'');
        $borderBtnH = $helper->getProperty('border', (isset($mainStyle['restore_style']['border_btn_h']))?$mainStyle['restore_style']['border_btn_h']:'');
    @endphp
    <style>

        .restore-box {
            border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius']))?$mainStyle['restore_style']['border_radius']:'')}};
            border: {{$helper->getProperty('border', (isset($mainStyle['restore_style']['border']))?$mainStyle['restore_style']['border']:'')}};
            font: {!! $helper->getProperty('font', (isset($mainStyle['restore_style']['font']))?$mainStyle['restore_style']['font']:'') !!};
        }

        .restore-box-body {
            background: {{(isset($mainStyle['restore_style']['bg_color_body']))?$mainStyle['restore_style']['bg_color_body']:''}};
        }

        .restore-box h2{
            background: {{(isset($mainStyle['restore_style']['bg_color']))?$mainStyle['restore_style']['bg_color']:''}};
            height: {{(isset($mainStyle['restore_style']['height']))?$mainStyle['restore_style']['height']:''}}px;
            line-height: {{(isset($mainStyle['restore_style']['height']))?$mainStyle['restore_style']['height']:''}}px;
            font-family: {!! $mainStyle['restore_style']['font']['family'] ?? '' !!};
        }

        .restore-box input {
            background: {{(isset($mainStyle['restore_style']['bg_color_input']))?$mainStyle['restore_style']['bg_color_input']:''}};
            color: {{(isset($mainStyle['restore_style']['color_input']))?$mainStyle['restore_style']['color_input']:''}};
            border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius_input']))?$mainStyle['restore_style']['border_radius_input']:'')}};
            border: {{$helper->getProperty('border', (isset($mainStyle['restore_style']['border_input']))?$mainStyle['restore_style']['border_input']:'')}};
        }
        .restore-box .form-control-feedback {
            color: {{(isset($mainStyle['restore_style']['color_input_icon']))?$mainStyle['restore_style']['color_input_icon']:''}};
        }
        .restore-box .btn {
            background-color: {{$bgColorBtn}};
            color: {{$colorBtn}};
            border-radius: {{$borderRadiusBtn}};
            border: {{$borderBtn}};
        }
        .restore-box .btn:hover {
            background-color: {{$bgColorBtnH}};
            color: {{$colorBtnH}};
            border-radius: {{$borderRadiusBtnH}};
            border: {{$borderBtnH}};
        }
    </style>
    <div class="restore-logo">
        <b>Восстановление пароля</b>
    </div>
    <!-- /.login-logo -->
    <div class="restore-box-body">
        <p class="restore-box-msg">Введите логин и измените пароль</p>
        @if (count($errors) > 0)
            @foreach($errors->all() as $error)
                <p class="alert alert-danger">{{$error}}</p>
            @endforeach
        @endif
        <form action="{{ route('password.request') }}" method="post" aria-label="{{ __('Reset Password') }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="form-group has-feedback">
                <label for="login">Ваш логин</label>
                <input id="login" type="text" name="login" class="form-control" value="" placeholder="Ваш логин" required>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="password">Введите новый пароль</label>
                <input type="password" id="password" name="password" class="form-control" placeholder="Введите новый пароль" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <label for="password-confirm">Повторите пароль</label>
                <input type="password" id="password-confirm" name="password_confirmation" class="form-control" placeholder="Повторите пароль" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary">Восстановить пароль</button>
                </div>
            </div>
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
@endsection
