@extends('layout.min')
@section('page-class', 'register-page')

@section('content')
    <div class="register-box">

        <div class="register-box-body">
            @if ($success)
                Ваш почтовый ящик подтвержден, сейчас вы будете переадресованы на страницу авторизации
                <script>
                    setTimeout(function(){
                        location.href = '{{route("profile")}}';
                    }, 3000);
                </script>
            @else
            @endif
        </div>
        <!-- /.form-box -->
    </div>
    <!-- /.register-box -->
@endsection