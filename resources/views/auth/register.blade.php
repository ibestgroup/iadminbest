@extends('layout.core.auth')
@section('page-class', 'register-page')

@section('content')
    @inject('helper', "\App\Libraries\Helpers")
    @php
        $template = \App\Libraries\Helpers::siteTemplate();
        $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);
        $styleId = \App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['id'];
    @endphp
    <style>
        .wrap-login-box {
            margin: auto;
            margin-top: {{$mainStyle['main_style']['margin_top']}}px;
            border: {{$mainStyle['main_style']['border']['width']}}px {{$mainStyle['main_style']['border']['style']}} {{$mainStyle['main_style']['border']['color']}};
            border-radius: {{$mainStyle['main_style']['border_radius']['tl']}}px {{$mainStyle['main_style']['border_radius']['tr']}}px {{$mainStyle['main_style']['border_radius']['br']}}px {{$mainStyle['main_style']['border_radius']['bl']}}px;
            overflow: hidden;
            background-color: {{$mainStyle['main_style']['bg_color_content']}};
        }
    </style>
    <div class="register-box">
        @php
            $bgColorBtn = (isset($mainStyle['register_style']['bg_color_btn']))?$mainStyle['register_style']['bg_color_btn']:'';
            $colorBtn = (isset($mainStyle['register_style']['color_btn']))?$mainStyle['register_style']['color_btn']:'';
            $bgColorBtnH = (isset($mainStyle['register_style']['bg_color_btn_h']))?$mainStyle['register_style']['bg_color_btn_h']:'';
            $colorBtnH = (isset($mainStyle['register_style']['color_btn_h']))?$mainStyle['register_style']['color_btn_h']:'';

            $borderRadiusBtn = $helper->getProperty('border_radius', (isset($mainStyle['register_style']['border_radius_btn']))?$mainStyle['register_style']['border_radius_btn']:'');
            $borderRadiusBtnH = $helper->getProperty('border_radius', (isset($mainStyle['register_style']['border_radius_btn_h']))?$mainStyle['register_style']['border_radius_btn_h']:'');
            $borderBtn = $helper->getProperty('border', (isset($mainStyle['register_style']['border_btn']))?$mainStyle['register_style']['border_btn']:'');
            $borderBtnH = $helper->getProperty('border', (isset($mainStyle['register_style']['border_btn_h']))?$mainStyle['register_style']['border_btn_h']:'');
        @endphp
        <style>

            .register-box {
                border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['register_style']['border_radius']))?$mainStyle['register_style']['border_radius']:'')}};
                border: {{$helper->getProperty('border', (isset($mainStyle['register_style']['border']))?$mainStyle['register_style']['border']:'')}};
                font: {!! $helper->getProperty('font', (isset($mainStyle['register_style']['font']))?$mainStyle['register_style']['font']:'') !!};
            }

            .register-box-body {
                background: {{(isset($mainStyle['register_style']['bg_color_body']))?$mainStyle['register_style']['bg_color_body']:''}};
            }

            .register-box h2{
                background: {{(isset($mainStyle['register_style']['bg_color']))?$mainStyle['register_style']['bg_color']:''}};
                height: {{(isset($mainStyle['register_style']['height']))?$mainStyle['register_style']['height']:''}}px;
                line-height: {{(isset($mainStyle['register_style']['height']))?$mainStyle['register_style']['height']:''}}px;
                font-family: {!! $mainStyle['register_style']['font']['family'] ?? '' !!};
            }

            .register-box input {
                background: {{(isset($mainStyle['register_style']['bg_color_input']))?$mainStyle['register_style']['bg_color_input']:''}};
                color: {{(isset($mainStyle['register_style']['color_input']))?$mainStyle['register_style']['color_input']:''}};
                border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['register_style']['border_radius_input']))?$mainStyle['register_style']['border_radius_input']:'')}};
                border: {{$helper->getProperty('border', (isset($mainStyle['register_style']['border_input']))?$mainStyle['register_style']['border_input']:'')}};
            }
            .register-box .form-control-feedback {
                color: {{(isset($mainStyle['register_style']['color_input_icon']))?$mainStyle['register_style']['color_input_icon']:''}};
            }
            .register-box .btn {
                background-color: {{$bgColorBtn}};
                color: {{$colorBtn}};
                border-radius: {{$borderRadiusBtn}};
                border: {{$borderBtn}};
            }
            .register-box .btn:hover {
                background-color: {{$bgColorBtnH}};
                color: {{$colorBtnH}};
                border-radius: {{$borderRadiusBtnH}};
                border: {{$borderBtnH}};
            }
        </style>
        <div class="register-box-body p-0">
            <h2>{{(isset($mainStyle['register_style']['text']))?$mainStyle['register_style']['text']:__('main.registration')}}</h2>
            @if (count($errors) > 0)
                @foreach($errors->all() as $error)
                    <p class="alert alert-danger">{!! $error !!}</p>
                @endforeach
            @endif
            <div class="col-lg-12" style="padding-bottom: 20px; padding-top: 20px;">
                <form action="{{route('register')}}" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="refer_login" value="{{\Illuminate\Support\Facades\Cookie::get('refer_login')}}">
                    <input type="hidden" name="refer" value="{{\Illuminate\Support\Facades\Cookie::get('refer_id')}}">
                    <div class="form-group has-feedback">
                        <input type="text" name="login" class="form-control" placeholder="{{__('main.login_name')}}">
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="email" name="email" class="form-control" placeholder="Email">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="{{__('main.password')}}">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password_confirmation" class="form-control" placeholder="{{__('main.repeat_password')}}">
                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    </div>
                    {{--<div class="row">--}}
                        {{--<div class="col-xs-12">--}}
                            {{--<div class="checkbox icheck">--}}
                                {{--<input id="user-terms" type="checkbox" name="terms">--}}
                                {{--<label for="user-terms">{{__('main.accept')}} <a href="#">terms</a></label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<!-- /.col -->--}}
                    {{--</div>--}}
                    <div class="form-group has-feedback">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{__('main.registration')}}</button>
                    </div>
                </form>

                <div class="ta-center">
                    <a href="{{route('login')}}" class="text-center btn btn-info btn-flat">{{__('main.login')}}</a>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /.form-box -->
    </div>
    <!-- /.register-box -->
@endsection