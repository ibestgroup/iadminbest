<aside class="main-sidebar core-sidebar js-sidebar-admin">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="image ta-center">
                <img src="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)->get()[0]->logo}}" class="img-circle" alt="User Image" style="max-height: 100px; max-width: 100px;">
            </div>
            <div class="info ta-center" style="padding: 5px; white-space: normal; text-align: center;">
                <h3>{{\App\Models\SiteInfo::first()['name']}}</h3>
            </div>
        </div>

        {{--{!! \App\Models\Menu::find(1)['styles']  !!}--}}
        <ul class="sidebar-menu all-slides ui-sortable tree" data-widget="tree">
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('profile')}}"><i class="fa fa-arrow-left"></i> {{__('main.back_site')}}</a>
            </li>
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('admin')}}"><i class="fa fa-dashboard"></i> {{__('main.admin_main')}}</a>
            </li>
            <li class="treeview" style="height: auto;">
                <a href="#">
                    <i class="fas fa-file-alt"></i><span> {{__('main.admin_pages')}}</span>
                    <span class="pull-right-container">
                        <span class="fa fa-angle-left pull-right"></span>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('pages')}}"><i class="fa fa-circle-o"></i> {{__('main.admin_pages')}}</a></li>
                    <li><a href="{{route('templates')}}"><i class="fa fa-circle-o"></i> {{__('main.admin_templates')}}</a></li>
                    <li><a href="{{route('templatesLibrary')}}"><i class="fa fa-circle-o"></i> Моя библиотека</a></li>
                    <li><a href="{{route('authPages')}}"><i class="fa fa-circle-o"></i> {{__('main.admin_pages_auth')}}</a></li>
                    <li><a href="{{route('templatesMarket')}}"><i class="fa fa-circle-o"></i> Магазин шаблонов</a></li>
                </ul>
            </li>

            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('setting')}}"><i class="fa fa-cogs"></i> {{__('main.admin_setting')}}</a>
            </li>
            @if (\App\Libraries\Helpers::siteType() == 1)
                <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                    <a href="{{route('markEdit')}}"><i class="fas fa-funnel-dollar"></i> {{__('main.admin_marks')}}</a>
                </li>
                <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                    <a href="{{route('activatesAdmin')}}"><i class="fas fa-cubes"></i> {{__('main.activates')}}</a>
                </li>
                @if (\App\Models\SiteInfo::first()->start == 0)
                    <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                        <a href="{{route('autoActivate')}}"><i class="fas fa-coins"></i> {{__('main.auto_activate')}}</a>
                    </li>
                @endif
            @else
                <li class="treeview" style="height: auto;">
                    <a href="#">
                        <i class="fa fa-usd"></i><span> Настройка инвестиций</span>
                        <span class="pull-right-container">
                        <span class="fa fa-angle-left pull-right"></span>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('createTypesOrder')}}"><i class="fa fa-circle-o"></i> Зарабатываемые ресурсы</a></li>
                        <li><a href="{{route('birds')}}"><i class="fa fa-circle-o"></i> Тарифы для вклада</a></li>
                    </ul>
                </li>
            @endif

            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="{{route('paymentsAdmin')}}"><i class="fas fa-hand-holding-usd"></i> {{__('main.admin_finance')}}</a>--}}
            {{--</li>--}}


            <li class="treeview" style="height: auto;">
                <a href="#">
                    <i class="fas fa-hand-holding-usd"></i> {{__('main.admin_finance')}}</span>
                    <span class="pull-right-container">
                        <span class="fa fa-angle-left pull-right"></span>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('paymentsAdmin')}}"><i class="fa fa-circle-o"></i> Пополнения</a></li>
                    <li><a href="{{route('cashoutsAdmin')}}"><i class="fa fa-circle-o"></i> Выводы</a></li>
                </ul>
            </li>

            <li class="treeview" style="height: auto;">
                <a href="#">
                    <i class="fas fa-file-alt"></i><span> {{__('main.admin_content')}}</span>
                    <span class="pull-right-container">
                        <span class="fa fa-angle-left pull-right"></span>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('addFaq')}}"><i class="fa fa-circle-o"></i> FAQ</a></li>
                    <li><a href="{{route('addPromo')}}"><i class="fa fa-circle-o"></i> {{__('main.admin_promo')}}</a></li>
                    <li><a href="{{route('addNews')}}"><i class="fa fa-circle-o"></i> {{__('main.admin_news')}}</a></li>
                </ul>
            </li>
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('landingEdit')}}"><i class="fas fa-tools"></i> {{__('main.admin_edit_landing')}}</a>
            </li>
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('createNotify')}}"><i class="fas fa-bell"></i> Стиль уведомлений</a>
            </li>
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('allUsers')}}"><i class="fa fa-users"></i> {{__('main.admin_user_list')}}</a>
            </li>
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('parking')}}"><i class="fas fa-link"></i> {{__('main.admin_parking')}}</a>
            </li>
            @if (\App\Models\SiteInfo::where('id', 1)->first()['cashout_confirm'] == 1)
                <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                    <a href="{{route('cashoutRequest')}}"><i class="fa fa-money"></i> Заявки на вывод</a>
                </li>
            @endif
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('sandBox')}}"><i class="fas fa-umbrella-beach"></i> Песочница</a>
            </li>
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('botSettings')}}"><i class="fas fa-robot"></i> {{ __('main.admin_telegram_bot') }}</a>
            </li>
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('documentation')}}"><i class="fas fa-file-alt"></i> {{__('main.admin_docs')}}</a>
            </li>
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="https://vk.com/igroupbest"><i class="fab fa-vk"></i> {{__('main.admin_questions')}}</a>
            </li>
        </ul>

    </section>
    <!-- /.sidebar -->
</aside>