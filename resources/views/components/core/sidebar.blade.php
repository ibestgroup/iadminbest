<aside class="main-sidebar core-sidebar js-sidebar-admin" style="width: 260px; background: #222933!important;">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        {{--{!! \App\Models\Menu::find(1)['styles']  !!}--}}
        <ul class="sidebar-menu all-slides ui-sortable tree" data-widget="tree">
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('profile')}}"><i class="fa fa-arrow-left"></i> {!! __('main.my_sites') !!}</a>
            </li>
            <li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">
                <a href="{{route('createAffiliate')}}"><i class="fa fa-dashboard"></i> {!! __('main.affiliate_programm') !!}</a>
            </li>
            {{--<li class="treeview" style="height: auto;">--}}
                {{--<a href="#">--}}
                    {{--<i class="fas fa-file-alt"></i><span> Страницы сайта</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{route('pages')}}"><i class="fa fa-circle-o"></i> Страницы сайта</a></li>--}}
                    {{--<li><a href="{{route('templates')}}"><i class="fa fa-circle-o"></i> Шаблоны сайта</a></li>--}}
                    {{--<li><a href="{{route('authPages')}}"><i class="fa fa-circle-o"></i> Страницы авторизации</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}

            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="{{route('setting')}}"><span class="fa fa-cogs"></span> Настройки сайта</a>--}}
            {{--</li>--}}
            {{--@if (\App\Libraries\Helpers::siteType() == 1)--}}
                {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                    {{--<a href="{{route('markEdit')}}"><i class="fas fa-funnel-dollar"></i> Маркетинги</a>--}}
                {{--</li>--}}
            {{--@else--}}
                {{--<li class="treeview" style="height: auto;">--}}
                    {{--<a href="#">--}}
                        {{--<i class="fa fa-usd"></i><span> Настройка инвестиций</span>--}}
                        {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                    {{--</a>--}}
                    {{--<ul class="treeview-menu">--}}
                        {{--<li><a href="{{route('createTypesOrder')}}"><i class="fa fa-circle-o"></i> Зарабатываемые ресурсы</a></li>--}}
                        {{--<li><a href="{{route('birds')}}"><i class="fa fa-circle-o"></i> Тарифы для вклада</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            {{--@endif--}}

            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="{{route('paymentsAdmin')}}"><i class="fas fa-hand-holding-usd"></i> Финансы сайта</a>--}}
            {{--</li>--}}

            {{--<li class="treeview" style="height: auto;">--}}
                {{--<a href="#">--}}
                    {{--<i class="fas fa-file-alt"></i><span> Контент сайта</span>--}}
                    {{--<span class="pull-right-container">--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--</span>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu">--}}
                    {{--<li><a href="{{route('addFaq')}}"><i class="fa fa-circle-o"></i> FAQ</a></li>--}}
                    {{--<li><a href="{{route('addPromo')}}"><i class="fa fa-circle-o"></i> Промоматериалы</a></li>--}}
                    {{--<li><a href="{{route('addNews')}}"><i class="fa fa-circle-o"></i> Новости</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="{{route('landingEdit')}}"><span class="fas fa-tools"></span> Редактировать главную</a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="{{route('allUsers')}}"><span class="fa fa-users"></span> Список пользователей</a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="{{route('parking')}}"><span class="fas fa-link"></span> Парковка домена</a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="{{route('markEdit')}}"><span class="fa fa-dashboard"></span> Редактирование маркетингов</a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
            {{--<a href="/profile/cashout"><i class="fa fa-money" aria-hidden="true"></i> <span>Вывод</span></a>--}}
            {{--</li>--}}
        </ul>
        <form action="{{route('storeMenu')}}" class="js-editMenu leftMenu" method="post" style="display: none;">
            @csrf
            <input type="hidden" name="type" value="sidebar">
            <input type="hidden" class="html-menu" name="html_menu">
            <button class="btn btn-primary save-menu">Сохранить</button>
        </form>

    </section>
    <!-- /.sidebar -->
</aside>