<style>
    @if ($mainStyle['right_sidebar']['status'] == 1)
        .right-sidebar {
            display: block;
            width: {{$mainStyle['right_sidebar']['style']['width']}}px;
            background-color: {{$mainStyle['right_sidebar']['style']['bg_color']}};
            {{--background-image: url({{$mainStyle['right_sidebar']['style']['bg_img']}});--}}
            {{($mainStyle['right_sidebar']['style']['bg_img'])?'background-image: url('.$mainStyle['right_sidebar']['style']['bg_img'].');':''}}

}

        .content {
            margin-right: {{$mainStyle['right_sidebar']['style']['width']}}px;
        }

        .toggle-sidebar-right {
            display: none;
        }

        @media (max-width: 700px) {

            .right-sidebar {
                width: {{$mainStyle['left_sidebar']['style']['width']}}px;
                /*overflow: hidden;*/
                right: -{{$mainStyle['left_sidebar']['style']['width']}}px;
            }

            .content {
                margin-right: 0px;
            }

            .toggle-sidebar-right {
                display: block;
            }
        }
    @else
        .right-sidebar {
            display: none;
            width: 0px;
        }
        .content {
            margin-right: 0px;
        }
    @endif

</style>
<aside class="right-sidebar" id="right-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        @if ($mainStyle['right_sidebar']['status'] == 1)
            <button class="btn toggle-sidebar-right" style="position: absolute; width: 35px; height: 35px; left: -30px; background: {{$mainStyle['right_sidebar']['style']['bg_color']}};"> <i class="fa fa-bars"></i> </button>
        @endif
        {{\App\Http\Controllers\ProfileController::pageConstruct('all', 'right', $demo)}}

        {{--<ul class="sidebar-menu all-slides ui-sortable tree" data-widget="tree">--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile" class=""><i class="fa fa-tachometer" aria-hidden="true"></i><span>Профиль</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle">--}}
                {{--<a href="/profile/edit" class=""><i class="fa fa-cog" aria-hidden="true"></i><span>Редактировать профиль</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/referral-list" class=""><i class="fa fa-users" aria-hidden="true"></i><span>Рефералы</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/referalTree" class=""><i class="fa fa-leaf" aria-hidden="true"></i><span>Реферальное дерево</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/news" class=""><i class="fa fa-book" aria-hidden="true"></i><span>Новости</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/promo" class=""><i class="fa fa-th" aria-hidden="true"></i><span>Промо материалы</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/faq" class="edit-now"><i class="fa fa-cloud" aria-hidden="true"></i><span>F.A.Q</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/admin" class=""><i class="fa fa-gears" aria-hidden="true"></i><span>Админ панель</span></a>--}}
            {{--</li>--}}
        {{--</ul>--}}

        {{--@if (Auth::id() == 1)--}}
            {{--<form action="{{route('storeMenu')}}" class="js-editMenu leftMenu" method="post" style="display: none;">--}}
                {{--@csrf--}}
                {{--<input type="hidden" name="type" value="sidebar">--}}
                {{--<input type="hidden" class="html-menu" name="html_menu">--}}
                {{--<button class="btn btn-primary save-menu">Сохранить</button>--}}
            {{--</form>--}}
        {{--@endif--}}


            {!! \App\Libraries\Helpers::adsense() !!}
{{--            @if (\App\Libraries\Helpers::adsense())--}}
                {{--<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>--}}
                {{--<!-- sidebar -->--}}
                {{--<ins class="adsbygoogle"--}}
                     {{--style="display:block"--}}
                     {{--data-ad-client="ca-pub-7640044855853499"--}}
                     {{--data-ad-slot="2238326273"--}}
                     {{--data-ad-format="auto"--}}
                     {{--data-full-width-responsive="true"></ins>--}}
                {{--<script>--}}
                    {{--(adsbygoogle = window.adsbygoogle || []).push({});--}}
                {{--</script>--}}

                {{--<a href="https://prtglp.ru/affiliate/11073561">--}}
                    {{--<img src="https://glopart.ru/uploads/images/96628/16c6516af1794876bb637caf4b5c4238.jpg" width="100%" alt="">--}}
                {{--</a>--}}
            {{--@endif--}}
    </section>
    <!-- /.sidebar -->
</aside>