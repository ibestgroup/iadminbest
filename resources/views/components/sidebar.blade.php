<style>
        @if ($mainStyle['left_sidebar']['status'] == 1)
            #left-sidebar {
                display: block;
                width: {{$mainStyle['left_sidebar']['style']['width']}}px;
                background-color: {{$mainStyle['left_sidebar']['style']['bg_color']}};
                {{--background-image: url({{$mainStyle['left_sidebar']['style']['bg_img']}});--}}
                {{($mainStyle['left_sidebar']['style']['bg_img'])?'background-image: url('.$mainStyle['left_sidebar']['style']['bg_img'].')':''}};
            }

            .content {
                margin-left: {{$mainStyle['left_sidebar']['style']['width']}}px;
            }

            .toggle-sidebar-left {
                display: none;
            }

            @media (max-width: 700px) {

                #left-sidebar {
                    width: {{$mainStyle['left_sidebar']['style']['width']}}px;
                    /*overflow: hidden;*/
                    left: -{{$mainStyle['left_sidebar']['style']['width']}}px;
                }

                .content {
                    margin-left: 0px;
                }

                .toggle-sidebar-left {
                    display: block;
                }
            }

        @else
            #left-sidebar {
                display: none;
                width: 0px;
            }
            .content {
                margin-left: 0px;
            }
        @endif
</style>
<aside class="main-sidebar" id="left-sidebar">
    <section class="sidebar">
        @if ($mainStyle['left_sidebar']['status'] == 1)
            <button class="btn toggle-sidebar-left" style="position: absolute; width: 35px; height: 35px; right: -30px; background: {{$mainStyle['left_sidebar']['style']['bg_color']}};"> <i class="fa fa-bars"></i> </button>
        @endif
        {{--<div class="user-panel">--}}
            {{--<div class="pull-left image">--}}
                {{--<img src="/laravel/storage/app/{{Auth::user()->avatar}}" class="img-circle" alt="User Image">--}}
            {{--</div>--}}
            {{--<div class="pull-left info">--}}
                {{--<p>{{Auth::user()->login}} <i class="fa fa-circle text-success"></i> Online</p>--}}
                {{--<p class="balance-info">Баланс: {{Auth::user()->balance}} {!! \App\Libraries\Helpers::getCurrent(\App\Models\SiteInfo::find(1)['value'], 'icon') !!}</p>--}}
                {{--<p class="balance-info">На тарифах: {{\App\Models\OrdersMarkInside::where('id_user', Auth::id())->where('status', '<', 2)->sum('balance') - \App\Models\OrdersMarkInside::where('id_user', Auth::id())->where('status', '<', 2)->sum('cashout')}} {!! \App\Libraries\Helpers::getCurrent(\App\Models\SiteInfo::find(1)['value'], 'icon') !!}</p>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{\App\Http\Controllers\ProfileController::pageConstruct('all', 'left', $demo)}}


        {{--<ul class="sidebar-menu all-slides ui-sortable tree" data-widget="tree">--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile" class=""><i class="fa fa-tachometer" aria-hidden="true"></i><span>Профиль</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle">--}}
                {{--<a href="/profile/edit" class=""><i class="fa fa-cog" aria-hidden="true"></i><span>Редактировать профиль</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/referral-list" class=""><i class="fa fa-users" aria-hidden="true"></i><span>Рефералы</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/referalTree" class=""><i class="fa fa-leaf" aria-hidden="true"></i><span>Реферальное дерево</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/news" class=""><i class="fa fa-book" aria-hidden="true"></i><span>Новости</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/promo" class=""><i class="fa fa-th" aria-hidden="true"></i><span>Промо материалы</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/faq" class="edit-now"><i class="fa fa-cloud" aria-hidden="true"></i><span>F.A.Q</span></a>--}}
            {{--</li>--}}
            {{--<li class="ui-sortable-handle" style="position: relative; left: 0px; top: 0px;">--}}
                {{--<a href="/profile/admin" class=""><i class="fa fa-gears" aria-hidden="true"></i><span>Админ панель</span></a>--}}
            {{--</li>--}}
        {{--</ul>--}}

        {{--@if (Auth::id() == 1)--}}
            {{--<form action="{{route('storeMenu')}}" class="js-editMenu leftMenu" method="post" style="display: none;">--}}
                {{--@csrf--}}
                {{--<input type="hidden" name="type" value="sidebar">--}}
                {{--<input type="hidden" class="html-menu" name="html_menu">--}}
                {{--<button class="btn btn-primary save-menu">Сохранить</button>--}}
            {{--</form>--}}
        {{--@endif--}}
            {!! \App\Libraries\Helpers::adsense() !!}
            {{--<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>--}}
            {{--<!-- sidebar -->--}}
            {{--<ins class="adsbygoogle"--}}
                 {{--style="display:block"--}}
                 {{--data-ad-client="ca-pub-7640044855853499"--}}
                 {{--data-ad-slot="2238326273"--}}
                 {{--data-ad-format="auto"--}}
                 {{--data-full-width-responsive="true"></ins>--}}
            {{--<script>--}}
                {{--(adsbygoogle = window.adsbygoogle || []).push({});--}}
            {{--</script>--}}
                {{--<div class="clearfix"></div>--}}
                {{--<a href="https://prtglp.ru/affiliate/11073561">--}}
                    {{--<img src="https://glopart.ru/uploads/images/96628/16c6516af1794876bb637caf4b5c4238.jpg" width="100%" alt="">--}}
                {{--</a>--}}
    </section>
    <!-- /.sidebar -->
</aside>