<style>
    @if ($mainStyle['top_sidebar']['status'] == 1)
        #top-sidebar {
            display: block;
            height: {{$mainStyle['top_sidebar']['style']['height']}}px;
            background-color: {{$mainStyle['top_sidebar']['style']['bg_color']}};
{{--            background-image: url({{$mainStyle['top_sidebar']['style']['bg_img']}});--}}
            {{($mainStyle['top_sidebar']['style']['bg_img'])?'background-image: url('.$mainStyle['top_sidebar']['style']['bg_img'].');':''}}
            min-height: 0;
        }

        .main-sidebar, .right-sidebar {
            padding-top: {{$mainStyle['top_sidebar']['style']['height'].'px'}};
        }

        #top-sidebar .grid {
            height: {{$mainStyle['top_sidebar']['style']['height']}}px;
            min-height: {{$mainStyle['top_sidebar']['style']['height']}}px;
        }
    @else
        #top-sidebar {
            display: none;
            height: 0;
            min-height: 0;
        }
        .main-sidebar, .right-sidebar {
            padding-top: 0;
        }
    @endif

    @media (max-width: 700px) {
        .navbar-custom-menu li, .navbar-custom-menu li a {
            display: block!important;
            width: 100%!important;
        }

        #top-sidebar .top-sidebar-min {
            display: block!important;
        }
    }

    @media (min-width: 701px) {
        #top-sidebar .top-sidebar-min {
            display: none!important;
        }
    }

</style>
<nav class="navbar navbar-static-top" id="top-sidebar">
    <div class="navbar-custom-menu top-sidebar-min">
        <ul class="nav navbar-nav all-slides-top in">
            <li class="ui-sortable-handle" id="toggle-ts" style="text-align: center;"><a><i class="fa fa-bars"></i> {{ __('main.menu') }}</a></li>
        </ul>
    </div>
    <div class="clearfix"></div>
    {{\App\Http\Controllers\ProfileController::pageConstruct('all', 'top', $demo)}}
</nav>

<script>
    $('document').ready(function () {
        $('#toggle-ts').on('click', function () {
            console.log(123);
            $('#top-sidebar > .grid').toggle();
        });
    });
</script>