@inject('notice', "\App\Models\Notice")
@inject('helper', "\App\Libraries\Helpers")
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{\App\Models\SiteInfo::find(1)['name']}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/iadminbest/public/css/app.css?v=312">
    <link rel="stylesheet" href="/iadminbest/constructor/css/cryptocoins.css">

    <link rel="icon" type="image/png" href="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['logo']}}" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://kit.fontawesome.com/289e5f1e22.js" crossorigin="anonymous"></script>

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400;600&display=swap" rel="stylesheet">
    <script>
        let ALTER_FEE = '{{\App\Models\SiteInfo::find(1)->alter_fee}}';
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini root">

@if (\App\Libraries\Helpers::checkSandBox())
    <div class="sandbox-attention">
        Сайт в режиме песочницы. Действия ограничены.
    </div>
@endif
<div class="clearfix"></div>
<div class="wrapper wrapper-admin">
    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <ul class="nav navbar-nav" style="float: right;">
                <li class="user user-menu">
                    <a href="/logout">
                        <i class="fa fa-sign-out"></i> {{__('main.logout')}}
                    </a>
                </li>
            </ul>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->

    @include("components.admin.sidebar")

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('title')
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            @if (\App\Helpers\MarksHelper::marksError())
                <p class="alert alert-error">
                    {!! \App\Helpers\MarksHelper::marksError('message')!!}
                </p>
            @endif
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->



<input type="hidden" id="notify-theme-now" value="{{\App\Models\NotifyTheme::query()->first()->theme}}">
<input type="hidden" id="notify-position-now" value="{{\App\Models\NotifyTheme::query()->first()->position}}">
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.js"></script>

<script src="{{ url('/iadminbest/public/js/app.js') }}?v=251353"></script>
{{--<script>--}}
    {{--function activate(i) {--}}
        {{--$.ajax({--}}
            {{--url: `/profile/activate?tarif=${i}`,--}}
            {{--async: false,--}}
            {{--dataType: 'json',--}}
            {{--before: $('.load-users').html('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>'),--}}
            {{--success: function (data) {--}}
                {{--console.log(data);--}}
                {{--var type = 'error';--}}

                {{--if (data.success) type = 'success';--}}

                {{--new Noty({--}}
                    {{--text: data.message,--}}
                    {{--type: type,--}}
                    {{--theme: 'sunset',--}}
                    {{--progressBar: true,--}}
                    {{--timeout: 3000--}}
                {{--}).show();--}}
            {{--}--}}
        {{--});--}}
        {{--return false;--}}
    {{--}--}}
{{--</script>--}}
</body>
</html>

