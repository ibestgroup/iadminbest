<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <title>{{\App\Models\SiteInfo::find(1)['name']}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/iadminbest/public/css/app.css?v=5">
    <link rel="stylesheet" href="/iadminbest/public/css/jquery.enjoyhint.css">

    <link rel="icon" type="image/png" href="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['logo']}}" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{--<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>--}}


    {{--<script src="/iadminbest/public/js/enjoyhint.js" crossorigin="anonymous"></script>--}}
    {{--<script src="/iadminbest/public/js/jquery.enjoyhint.js" crossorigin="anonymous"></script>--}}

    <script src="https://kit.fontawesome.com/289e5f1e22.js" crossorigin="anonymous"></script>
    <script src="https://cdn.ckeditor.com/4.13.0/full/ckeditor.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/enjoyhint/3.1.0/enjoyhint.css" integrity="sha256-NEUBaBwhznG8DXBeJpMx+QRfz/z2CtldZsA5zVqpeg4=" crossorigin="anonymous" />

    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/enjoyhint/3.1.0/enjoyhint.js" integrity="sha256-0YhJ9ZZSnPtc1akqOqAEQj+JixqxDJI43TqE79JC5yI=" crossorigin="anonymous"></script>--}}
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google Font -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400;600&display=swap" rel="stylesheet">


    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(56723590, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/56723590" style="position:absolute; left:-9999px;" alt="" /></div></noscript>


    <!-- /Yandex.Metrika counter -->

    <!-- VK Pixel -->
    <script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?168",t.onload=function(){VK.Retargeting.Init("VK-RTRG-659142-a27Rk"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-659142-a27Rk" style="position:fixed; left:-999px;" alt=""/></noscript>
    <script>
        let ALTER_FEE = '{{\App\Models\SiteInfo::find(1)->alter_fee}}';
    </script>
</head>
<body class="hold-transition skin-blue sidebar-mini root">
@if (!empty($_COOKIE['demo']))
<div style="background: #3e3e3e; color: #fefefe; padding: 30px 0; text-align: center;">
    Демонстрация шаблона. <span class="demo-disable btn btn-primary">Отключить демонстрацию</span>
</div>
@endif
@if (\App\Libraries\Helpers::checkSandBox())
    <div class="sandbox-attention">
        Сайт в режиме песочницы. Действия ограничены.
    </div>
@endif

@php
    if ($demo) {
        $classTemplate = \App\Models\CoreTemplates::query();
        $classSiteElements = \App\Models\CoreSiteElements::query();
        $template = $demo;
    } else {
        $classTemplate = \App\Models\Templates::query();
        $classSiteElements = \App\Models\SiteElements::query();
        $template = \App\Libraries\Helpers::siteTemplate();
    }

    $mainStyle = unserialize($classSiteElements->where('name', 'main-style')->where('template', $template)->first()['option_value']);
@endphp

@if ($mainStyle['header']['status'])
    <style>
        .my-header {
            width: {{($mainStyle['header']['width_full'])?'100%':$mainStyle['header']['width'].'px'}};
            height: {{($mainStyle['header']['height'] == 0)?'auto':$mainStyle['header']['height'].'px'}};
        }
        .header-img{
            height: {{($mainStyle['header']['height'] == 0)?'auto':$mainStyle['header']['height'].'px'}};
            width: 100%;
            border-radius: {{\App\Libraries\Helpers::getProperty('border_radius', $mainStyle['header']['border_radius'])}};
            border: {{\App\Libraries\Helpers::getProperty('border', $mainStyle['header']['border'])}};
        }
    </style>
    <div class="my-header">
        <img src="{{$mainStyle['header']['img']}}" alt="" width="100%" class="header-img">
    </div>
@endif
<div class="clearfix"></div>
<div class="wrapper">
    <div class="in-wrapper">
        <header class="main-header">
            <style>
                body {
                    background-color: {{$mainStyle['main_style']['bg_color_body']}};
                    {{($mainStyle['main_style']['bg_img_body'])?'background-image: url('.$mainStyle['main_style']['bg_img_body'].')':''}};
                }

                .wrapper {
                    width: {{($mainStyle['main_style']['width_full'])?'100%':$mainStyle['main_style']['width'].'px'}};
                    margin: auto;
                    margin-top: {{$mainStyle['main_style']['margin_top']}}px;
                    border: {{$mainStyle['main_style']['border']['width']}}px {{$mainStyle['main_style']['border']['style']}} {{$mainStyle['main_style']['border']['color']}};
                    border-radius: {{$mainStyle['main_style']['border_radius']['tl']}}px {{$mainStyle['main_style']['border_radius']['tr']}}px {{$mainStyle['main_style']['border_radius']['br']}}px {{$mainStyle['main_style']['border_radius']['bl']}}px;
                    overflow: hidden;
                }

                @media (max-width: 700px) {
                    .wrapper,
                    #top-sidebar > .grid > div,
                    .navbar-custom-menu, .nav {
                        width: 100%;
                    }

                    #top-sidebar > .grid {
                        display: none;
                    }

                }

                .in-wrapper {
                    overflow: visible;
                }

                .content {
                    background-color: {{$mainStyle['main_style']['bg_color_content']}};
                    {{($mainStyle['main_style']['bg_img_content'])?'background-image: url('.$mainStyle['main_style']['bg_img_content'].')':''}};
                }


            </style>
            @include("components.topSidebar")

        </header>
        <!-- Left side column. contains the logo and sidebar -->

        @include("components.sidebar")

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Main content -->
            <section class="content" style="min-height: 100vh;">
                @yield('content')
            </section>
            <!-- /.content -->

        </div>
        <!-- /.content-wrapper -->
        @include("components.rightSidebar")


        @php
            $page = $page ?? '';
        @endphp

        @include('templates.sites.pagebuilder.modals', array('page' => $page))

    </div>
    <!-- ./wrapper -->
</div>

@if (Auth::id() == 1)
<div style="position: fixed; left: 10px; bottom: 70px; z-index: 10000;">
    <a href="{{route('documentation')}}" class="btn btn-primary btn-enable-panel"><span class="fas fa-file-alt"></span> {{__('main.documentation')}}</a>
</div>
<div style="position: fixed; left: 10px; bottom: 10px; z-index: 10000;">
    <button class="btn btn-primary enable-panel btn-enable-panel">{{__('main.on_panel')}}</button>
</div>
@endif
<input type="hidden" id="notify-theme-now" value="{{\App\Models\NotifyTheme::query()->first()->theme}}">
<input type="hidden" id="notify-position-now" value="{{\App\Models\NotifyTheme::query()->first()->position}}">
<script>
</script>

<script src="{{ url('/iadminbest/public/js/app.js') }}?v=3"></script>

{{--<script src="/iadminbest/constructor/js/enjoyhint/enjoyhint.js"></script>--}}
{{--<script src="/iadminbest/constructor/js/enjoyhint/enjoyhint.min.js"></script>--}}
{{--<script src="/iadminbest/public/js/hints.js"></script>--}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.js"></script>
<script>
    function activate(i) {
        $.ajax({
            url: `/profile/activate?tarif=${i}`,
            async: false,
            dataType: 'json',
            before: $('.load-users').html('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>'),
            success: function (data) {
                console.log(data);
                let type = 'error',
                    positionNew = $('#notify-position-now').val(),
                    themeNew = $('#notify-theme-now').val();

                if (data.success) type = 'success';

                new Noty({
                    text: data.message,
                    type: type,
                    layout: positionNew,
                    theme: themeNew,
                    progressBar: true,
                    timeout: 3000
                }).show();
            }
        });
        return false;
    }

    //initialize instance
    // var enjoyhint_instance = new EnjoyHint({});
    //
    // //simple config.
    // //Only one step - highlighting(with description) "New" button
    // //hide EnjoyHint after a click on the button.
    // var enjoyhint_script_steps = [
    //
    //     {
    //         'click .enable-panel' : 'Нажми включить панель'
    //     },
    //     {
    //         'next .panel-redactor' : 'Нажми включить панель'
    //     },
    //
    // ];
    //
    // //set script config
    // enjoyhint_instance.set(enjoyhint_script_steps);
    //
    // //run Enjoyhint script
    // enjoyhint_instance.run();

</script>
</body>
</html>