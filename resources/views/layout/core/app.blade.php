@inject('notice', "\App\Models\Notice")
@inject('helper', "\App\Libraries\Helpers")
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>iAdmin.best - конструктор сайтов</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/iadminbest/public/css/app.css">
    <link rel="shortcut icon" type="image/png" href="/iadminbest/favicon.ico"/>

    <link rel="icon" type="image/png" href="/iadminbest/storage/app{{\App\Models\SiteInfo::find(1)['logo']}}" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="ah-verification" content="a89672ec3977" />

    <!-- Google Font -->
    <script src="https://kit.fontawesome.com/289e5f1e22.js" crossorigin="anonymous"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400;600&display=swap" rel="stylesheet">

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(56723590, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/56723590" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    <!-- VK Pixel -->
    <script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?168",t.onload=function(){VK.Retargeting.Init("VK-RTRG-659142-a27Rk"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-659142-a27Rk" style="position:fixed; left:-999px;" alt=""/></noscript>
    <script type="text/javascript">!function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?168",t.onload=function(){VK.Retargeting.Init("VK-RTRG-659152-gozkv"),VK.Retargeting.Hit()},document.head.appendChild(t)}();</script><noscript><img src="https://vk.com/rtrg?p=VK-RTRG-659152-gozkv" style="position:fixed; left:-999px;" alt=""/></noscript>
</head>
<body class="hold-transition skin-blue  sidebar-mini root">
<div class="clearfix"></div>
<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top" style="min-height: 70px;">
            <ul class="nav navbar-nav" style="float: right;">
                <li class="user user-menu">
                    <a href="/logout">
                        <i class="fa fa-sign-out"></i>{{__('main.logout')}}
                    </a>
                </li>
            </ul>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->

@include("components.core.sidebar")

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-left: 260px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @yield('title')
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

</div>
<!-- ./wrapper -->



<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.js"></script>

<script src="{{ url('/iadminbest/public/js/app.js') }}?v=1"></script>
{{--<script>--}}
    {{--function activate(i) {--}}
        {{--$.ajax({--}}
            {{--url: `/profile/activate?tarif=${i}`,--}}
            {{--async: false,--}}
            {{--dataType: 'json',--}}
            {{--before: $('.load-users').html('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>'),--}}
            {{--success: function (data) {--}}
                {{--console.log(data);--}}
                {{--var type = 'error';--}}

                {{--if (data.success) type = 'success';--}}

                {{--new Noty({--}}
                    {{--text: data.message,--}}
                    {{--type: type,--}}
                    {{--theme: 'sunset',--}}
                    {{--progressBar: true,--}}
                    {{--timeout: 3000--}}
                {{--}).show();--}}
            {{--}--}}
        {{--});--}}
        {{--return false;--}}
    {{--}--}}
{{--</script>--}}
</body>
</html>

