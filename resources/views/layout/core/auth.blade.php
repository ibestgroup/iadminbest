@inject('notice', "\App\Models\Notice")
@inject('helper', "\App\Libraries\Helpers")
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{\App\Models\SiteInfo::find(1)['name']}}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {{--<link rel="icon" href="/moneytree/public/img/qr-mt-3.png">--}}
    <link rel="stylesheet" href="/iadminbest/public/css/app.css">

    <link rel="icon" type="image/png" href="/iadminbest/storage/app{{\App\Models\SiteInfo::find(1)['logo']}}" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="https://kit.fontawesome.com/289e5f1e22.js" crossorigin="anonymous"></script>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;400;600&display=swap" rel="stylesheet">
    {{--<a href="/" class="logo">--}}
    {{--<span class="logo-span-h-small fwb">H</span> <span class="logo-span-m-small fwb">M</span>--}}
    {{--</a>--}}

    <script src="https://cdn.jsdelivr.net/npm/clipboard-js@0.3.6/clipboard.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Cabin&display=swap" rel="stylesheet">
</head>
@php
    $template = \App\Libraries\Helpers::siteTemplate();
    $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);
@endphp
<body class="hold-transition skin-blue sidebar-mini root">
@if (\App\Models\ProfilePageSetting::find(1)['header_img'])
    <div class="my-header" style="width: {{\App\Models\ProfilePageSetting::find(1)['header_width']}}px">
        <img src="{{\App\Models\ProfilePageSetting::find(1)['header_img']}}" alt="" width="100%" class="header-img">
    </div>
@endif
<div class="wrapper" style="height: auto; min-height: 100%;">

    <div class="in-wrapper">
    <style>
        body {
            background-color: {{$mainStyle['main_style']['bg_color_body']}};
            background-image: url({{$mainStyle['main_style']['bg_img_body']??'https://iadmin.best'}});
        }

        .wrapper {
            width: {{($mainStyle['main_style']['width_full'])?'100%':$mainStyle['main_style']['width'].'px'}};
            margin: auto;
            margin-top: {{$mainStyle['main_style']['margin_top']}}px;
            border: {{$mainStyle['main_style']['border']['width']}}px {{$mainStyle['main_style']['border']['style']}} {{$mainStyle['main_style']['border']['color']}};
            border-radius: {{$mainStyle['main_style']['border_radius']['tl']}}px {{$mainStyle['main_style']['border_radius']['tr']}}px {{$mainStyle['main_style']['border_radius']['br']}}px {{$mainStyle['main_style']['border_radius']['bl']}}px;
            overflow: hidden;
        }

        .in-wrapper {
            overflow: visible;
        }

        .content {
            background-color: {{$mainStyle['main_style']['bg_color_content']}};
            background-image: url({{$mainStyle['main_style']['bg_img_content']??'https://iadmin.best'}});
        }
    </style>
    <header class="main-header">
        {{--@include("components.topSidebar")--}}

    </header>
    <!-- Left side column. contains the logo and sidebar -->

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    </div>
</div>
<!-- ./wrapper -->

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script src="/iadminbest/public/js/app.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.js"></script>
    <script>
        function activate(i) {
            $.ajax({
                url: `/profile/activate?tarif=${i}`,
                async: false,
                dataType: 'json',
                before: $('.load-users').html('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>'),
                success: function (data) {
                    console.log(data);
                    var type = 'error';

                    if (data.success) type = 'success';

                    new Noty({
                        text: data.message,
                        type: type,
                        theme: 'sunset',
                        progressBar: true,
                        timeout: 3000
                    }).show();
                }
            });
            return false;
        }
    </script>

    {{--<script type="text/javascript" src="https://vk.com/js/api/openapi.js?162"></script>--}}

    {{--<!-- VK Widget -->--}}
    {{--<div id="vk_community_messages"></div>--}}
    {{--<script type="text/javascript">--}}
        {{--VK.Widgets.CommunityMessages("vk_community_messages", 127607773, {expanded: "1",tooltipButtonText: "Есть вопрос?"});--}}
    {{--</script>--}}
</body>
</html>
