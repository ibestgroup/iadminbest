<div style="background: #ff8d00; width: 600px; padding: 20px; border-radius: 5px; font-size: 16px">
    <h3 style="color: #fff; width: 130px; height: 130px; line-height: 130px; margin: auto; background: #222933; border-radius: 50%; text-align: center;">iAdmin.best</h3>
    <p style="color: #fff; padding: 5px; margin: 0px; font-size: 19px;">Здравствуйте, {{$user->login}}!</p>
    <p style="color: #fff; padding: 5px; margin: 0px;">Вы получили это письмо, так как зарегистрированы на сайте iAdmin.work(зеркало сайта iAdmin.best).</p>
    <p style="color: #fff; padding: 5px; margin: 0px;">На конструторе очень много новых изменений.</p>
    <p style="color: #fff; padding: 5px; margin: 0px;">Посмотреть об обновлениях вы можете в группе Вконтакте.</p>
    <a href="https://vk.com/igroupbest" style="padding: 10px; background: #b36400; color: #fff; border-radius: 5px; width: 200px; display: block; margin: auto; text-decoration: none; text-align: center;">Группа Вконтакте</a>
    <p style="color: #fff; padding: 5px; margin: 0px;">Если вы не создали сайт, из-за того что не могли разобраться, то на сайте теперь есть подробная документация.</p>
    <a href="https://iadmin.work" style="padding: 10px; background: #b36400; color: #fff; border-radius: 5px; width: 200px; display: block; margin: auto; text-decoration: none; text-align: center;">Перейти на сайт</a>
</div>