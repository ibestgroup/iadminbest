<div style="background: #ff8d00; width: 600px; padding: 20px; border-radius: 5px; text-align: center;">
    <h3 style="color: #fff; width: 120px; height: 120px; line-height: 120px; margin: auto; background: #222933; border-radius: 50%;">iAdmin.best</h3>
    <p style="color: #fff; padding: 5px; margin: 0px;">Приветствую тебя. Я разработал новый конструктор сайтов, и подумал что тебе это будет интересно.</p>
    <p style="color: #fff; padding: 5px; margin: 0px;">Все мы когда то хотели свой сайт, и думали: "Умел бы я программировать, сделал бы крутой проект!" Теперь это возможно с новым сервисов и без навыком программирования.</p>
    <p style="color: #fff; padding: 5px; margin: 0px;">На этом конструкторе я исправил все недочеты аналогичных сервисов, добавил кучу фишек, и объединил это в своем сервисе.</p>
    <h4 style="color: #fff; padding: 5px;">Коротко о возможностях моего конструктора:</h4>
    <p style="color: #fff; padding: 5px 10px; font-style: italic; font-weight: bold; margin: 0px">- Создание матричных проектов с любым маркетингом. Функционал значительно шире чем в аналогах.</p>
    <p style="color: #fff; padding: 5px 10px; font-style: italic; font-weight: bold; margin: 0px">- Создание хайпов, удвоителей, сборников.</p>
    <p style="color: #fff; padding: 5px 10px; font-style: italic; font-weight: bold; margin: 0px">- Создание игр "ферм" с богатым функционалом.</p>
    <p style="color: #fff; padding: 5px 10px; font-style: italic; font-weight: bold; margin: 0px">- Настройка личного кабинета продумана настолько, что можно создать несколько сайтов, абсолютно не похожими друг на друга.</p>
    <p style="color: #fff; padding: 10px 5px;">Это лишь верхушка айсберга. Ты можешь убедиться в этом.</p>

    <a href="https://iadmin.best" style="background: #b36400; color: #fff; border-radius: 5px; width: 200px; display: block; margin: auto; text-decoration: none;">Перейти на сайт</a>
</div>