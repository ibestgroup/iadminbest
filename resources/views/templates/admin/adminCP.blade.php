@extends("layout.admin.app")

@section('title', __('main.admin_panel'))

@section('content')
    @if (Auth::id() == 1)

        {{--{{dd(\App\Http\Controllers\AdminCPController::json())}}--}}

        <div class="row">

            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        {{--<h3 class="box-title">Графики</h3>--}}
                        <h4 style="color: #00a65a;"><i class="fa fa-money"></i> Пополнения</h4>
                    </div>
                    <div class="small-box" style="margin-bottom: 0;">
                        <div class="inner">
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <div class="chart">
                            <canvas id="chart-payments"></canvas>
                        </div>
                        <div class="form-group" style="margin-bottom: 0;">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" name="datarange" id="drp-payments">
                            </div>
                            <!-- /.input group -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        {{--<h3 class="box-title">Графики</h3>--}}
                        <h4 style="color: #ff7701;"><i class="fa fa-cubes"></i> Активации</h4>
                    </div>
                    <div class="small-box" style="margin-bottom: 0;">
                        <div class="inner">
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <div class="chart">
                            <canvas id="chart-activation"></canvas>
                        </div>
                        <div class="form-group" style="margin-bottom: 0;">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" name="datarange" id="drp-activation">
                            </div>
                            <!-- /.input group -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        {{--<h3 class="box-title">Графики</h3>--}}
                        <h4 style="color: #31bbbb;"><i class="fa fa-user-plus"></i> Регистрации</h4>
                    </div>
                    <div class="small-box" style="margin-bottom: 0;">
                        <div class="inner">
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <div class="chart">
                            <canvas id="chart-register"></canvas>
                        </div>
                        <div class="form-group" style="margin-bottom: 0;">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" name="datarange" id="drp-register">
                            </div>
                            <!-- /.input group -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
{{--                        <h3 class="box-title">{{__('main.admin_economy')}}</h3>--}}
                        <h3 class="box-title">{{__('main.admin_statistic')}}</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">

                            {{--<div class="col-lg-5">--}}
                                {{--<div class="image ta-center">--}}
                                    {{--                                    <img src="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)->get()[0]->logo}}" class="img-circle" alt="User Image" style="max-height: 100px; max-width: 100px;">--}}
                                    {{--<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e7/Instagram_logo_2016.svg/768px-Instagram_logo_2016.svg.png" class="img-circle" alt="User Image" style="max-height: 100px; max-width: 100px;">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-7">--}}
                                {{--<h3>Название сайта</h3>--}}
                            {{--</div>--}}
                            <div class="clearfix"></div>
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td>
                                            {{__('main.admin_site_name')}}
                                        </td>
                                        <td class="ta-center">
                                            <span class="badge bg-orange" style="font-size: 16px;">
                                                {{\App\Models\SiteInfo::find(1)->name}}
                                            </span>
                                        </td>
                                    </tr>
                                    @if (\App\Libraries\Helpers::siteType() == 1)
                                    <tr>
                                        <td>
                                            Дата старта
                                        </td>
                                        <td class="ta-center">
                                            <span class="badge bg-aqua" style="font-size: 16px;">
                                                {!! (!$siteInfo->start)?'Не стартовал':\App\Models\Start::find(1)->start_pay.'<i class="fa fa-clock"></i>' !!}
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Время работы
                                        </td>
                                        <td class="ta-center">
                                            <span class="badge bg-light-blue" style="font-size: 16px;">
                                                {{(!$siteInfo->start)?'Не стартовал':\Illuminate\Support\Carbon::parse(\App\Models\Start::find(1)->start_pay)->diffInDays(\Illuminate\Support\Carbon::now()).'дн.'}}
                                            </span>
                                        </td>
                                    </tr>
                                    @endif
                                    <tr>
                                        <td>
                                            {{__('main.admin_users')}}
                                        </td>
                                        <td class="ta-center">
                                            <span class="badge bg-green" style="font-size: 16px;">
                                                {{\App\Models\Users::count()}} <i class="fa fa-users"></i>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            {{__('main.admin_users_active')}}
                                        </td>
                                        <td class="ta-center">
                                            <span class="badge bg-purple" style="font-size: 16px;">
                                                {{\App\Models\Users::where('type_follow', '>', 0)->count()}} <i class="fa fa-user-check"></i>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        @if (\App\Libraries\Helpers::siteType() == 1)
                                            <td>
                                                {{__('main.admin_activate')}}
                                            </td>
                                            <td class="ta-center">
                                                <span class="badge bg-orange" style="font-size: 16px;">
                                                    {{\App\Models\OrdersMarkInside::whereRaw('id != par_or')->count()}} <i class="fa fa-gift"></i>
                                                </span>
                                            </td>
                                        @else
                                            <td>
                                                Количество покупок
                                            </td>
                                            <td class="ta-center">
                                                <span class="badge bg-orange" style="font-size: 16px;">
                                                    {{\App\Models\BirdsOrder::count()}} <i class="fa fa-gift"></i>
                                                </span>
                                            </td>
                                        @endif
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-6">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <td style="width: 50%;">{{__('main.admin_sum_payment')}}</td>
                                        <td class="ta-center" style="width: 50%;">
                                            <span class="badge bg-green" style="font-size: 16px;">
                                                @if (\App\Libraries\Helpers::siteCurrency() > 3)
                                                    {{\App\Models\PaymentsCrypto::sum('amount')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}
                                                @else
                                                    {{\App\Models\Payments::sum('amount')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}
                                                @endif
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{__('main.admin_sum_cashout')}}</td>
                                        <td class="ta-center">
                                            <span class="badge bg-yellow" style="font-size: 16px;">
                                                {{\App\Models\Cashout::where('payment_system', '!=', 1919)->where('payment_system', '!=', 333)->where('status', 2)->sum('amount')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{__('main.admin_sum_balance_site')}}</td>
                                        <td class="ta-center">
                                            <span class="badge bg-light-blue" style="font-size: 16px;">
                                                @php
                                                    if (\App\Libraries\Helpers::siteCurrency() > 3) {
                                                        $paymentsClass = \App\Models\PaymentsCrypto::class;
                                                    } else {
                                                        $paymentsClass = \App\Models\Payments::class;
                                                    }

                                                @endphp
                                                @if (\App\Libraries\Helpers::siteType() == 1)
                                                    {{$paymentsClass::sum('amount') - \App\Models\Cashout::where('payment_system', '!=', 1919)->where('payment_system', '!=', 333)->where('status', 2)->sum('amount')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}
                                                @else
                                                    {{$paymentsClass::sum('amount')/100*90 - \App\Models\Cashout::where('payment_system', '!=', 1919)->where('payment_system', '!=', 333)->where('status', 2)->sum('amount')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}
                                                @endif
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{__('main.admin_sum_balance_users')}}</td>
                                        <td class="ta-center">
                                            <span class="badge bg-green" style="font-size: 16px;">
                                                {{\App\Models\Users::sum('balance')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}
                                            </span>
                                        </td>
                                    </tr>
                                    @if (\App\Libraries\Helpers::siteType() == 1)
                                        <tr>
                                            <td>{{__('main.admin_sum_reserved')}}</td>
                                            <td class="ta-center">
                                                <span class="badge bg-purple" style="font-size: 16px;">
                                                    {{\App\Models\OrdersMarkInside::sum('balance') - \App\Models\OrdersMarkInside::sum('cashout')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>{{__('main.admin_sum_all')}}</td>
                                            <td class="ta-center">
                                                <span class="badge bg-orange" style="font-size: 16px;">
                                                    {{(\App\Models\PaymentsOrder::where('wallet', '!=', 'to_balance')->where('type', '!=', 'inside_transaction')->sum('amount'))/90*100}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}
                                                </span>
                                                <span style="display: none;">
                                                    @php
                                                        //$activate = new \App\Libraries\Activate();
                                                        //dd($activate->orderInfo(4,1));

                                                        $userBalances = \App\Models\Users::sum('balance');
                                                        $reserveOrders = \App\Models\OrdersMarkInside::sum('balance') - \App\Models\OrdersMarkInside::sum('cashout');
                                                        $turnover = (\App\Models\PaymentsOrder::where('wallet', '!=', 'to_balance')->where('type', '!=', 'inside_transaction')->sum('amount'))/90*100*0.1;
                                                        $sumDN = $userBalances + $reserveOrders + $turnover;
                                                        $sumDN = \App\Libraries\Helpers::myRound($sumDN);
                                                        echo $sumDN;
                                                    @endphp
                                                </span>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        {{--<div class="col-lg-4">--}}
                            {{--<div class="col-lg-12 ta-center economy">--}}
                                {{--<h4>{{__('main.admin_sum_payment')}}</h4>--}}
                                {{--@if (\App\Libraries\Helpers::siteCurrency() > 3)--}}
                                    {{--<h2>{{\App\Models\PaymentsCrypto::sum('amount')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</h2>--}}
                                {{--@else--}}
                                    {{--<h2>{{\App\Models\Payments::sum('amount')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</h2>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-4">--}}
                            {{--<div class="col-lg-12 ta-center economy">--}}
                                {{--<h4>{{__('main.admin_sum_cashout')}}</h4>--}}
                                {{--<h2>{{\App\Models\Cashout::where('payment_system', '!=', 1919)->where('payment_system', '!=', 333)->where('status', 2)->sum('amount')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</h2>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-4">--}}
                            {{--<div class="col-lg-12 ta-center economy">--}}
                                {{--<h4>{{__('main.admin_sum_balance_site')}}</h4>--}}
                                {{--@php--}}
                                    {{--if (\App\Libraries\Helpers::siteCurrency() > 3) {--}}
                                        {{--$paymentsClass = \App\Models\PaymentsCrypto::class;--}}
                                    {{--} else {--}}
                                        {{--$paymentsClass = \App\Models\Payments::class;--}}
                                    {{--}--}}

                                {{--@endphp--}}
                                {{--@if (\App\Libraries\Helpers::siteType() == 1)--}}
                                    {{--<h2>{{$paymentsClass::sum('amount') - \App\Models\Cashout::where('payment_system', '!=', 1919)->where('payment_system', '!=', 333)->where('status', 2)->sum('amount')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</h2>--}}
                                {{--@else--}}
                                    {{--<h2>{{$paymentsClass::sum('amount')/100*90 - \App\Models\Cashout::where('payment_system', '!=', 1919)->where('payment_system', '!=', 333)->where('status', 2)->sum('amount')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</h2>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-4 mt-10">--}}
                            {{--<div class="col-lg-12 ta-center economy">--}}
                                {{--<h4>{{__('main.admin_sum_balance_users')}}</h4>--}}
                                {{--<h2>{{\App\Models\Users::sum('balance')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</h2>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--@if (\App\Libraries\Helpers::siteType() == 1)--}}
                            {{--<div class="col-lg-4 mt-10">--}}
                                {{--<div class="col-lg-12 ta-center economy">--}}
                                    {{--<h4>{{__('main.admin_sum_reserved')}}</h4>--}}
                                    {{--<h2>{{\App\Models\OrdersMarkInside::sum('balance') - \App\Models\OrdersMarkInside::sum('cashout')}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</h2>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-4 mt-10">--}}
                                {{--<div class="col-lg-12 ta-center economy">--}}
                                    {{--<h4>{{__('main.admin_sum_all')}}</h4>--}}
                                    {{--<h2>{{(\App\Models\PaymentsOrder::where('wallet', '!=', 'to_balance')->where('type', '!=', 'inside_transaction')->sum('amount'))/90*100}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</h2>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    </div>
                </div>
            </div>
        </div>

        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="box box-primary">--}}
                    {{--<div class="box-header with-border">--}}
                        {{--<h3 class="box-title">Графики</h3>--}}
                    {{--</div>--}}

                    {{--<div class="col-lg-4">--}}
                        {{--<div class="small-box" style="color: #00a65a;">--}}
                            {{--<div class="inner">--}}
                                {{--<h4><i class="fa fa-money"></i> Пополнения</h4>--}}
                            {{--</div>--}}
                            {{--<div class="icon">--}}
                                {{--<i class="ion ion-bag"></i>--}}
                            {{--</div>--}}
                            {{--<div class="chart">--}}
                                {{--<canvas id="chart-payments"></canvas>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="input-group">--}}
                                    {{--<div class="input-group-addon">--}}
                                        {{--<i class="fa fa-calendar"></i>--}}
                                    {{--</div>--}}
                                    {{--<input type="text" class="form-control" name="datarange" id="drp-payments">--}}
                                {{--</div>--}}
                                {{--<!-- /.input group -->--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}

                    {{--<div class="col-lg-4">--}}
                        {{--<div class="small-box">--}}
                            {{--<div class="inner">--}}
                                {{--<h4 style="color: #ff7701;"><i class="fa fa-cubes"></i> Активации</h4>--}}
                            {{--</div>--}}
                            {{--<div class="icon">--}}
                                {{--<i class="ion ion-bag"></i>--}}
                            {{--</div>--}}
                            {{--<div class="chart">--}}
                                {{--<canvas id="chart-activation"></canvas>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="input-group">--}}
                                    {{--<div class="input-group-addon">--}}
                                        {{--<i class="fa fa-calendar"></i>--}}
                                    {{--</div>--}}
                                    {{--<input type="text" class="form-control" name="datarange" id="drp-activation">--}}
                                {{--</div>--}}
                                {{--<!-- /.input group -->--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}

                    {{--<div class="col-lg-4">--}}
                        {{--<div class="small-box">--}}
                            {{--<div class="inner">--}}
                                {{--<h4 style="color: #31bbbb;"><i class="fa fa-user-plus"></i> Регистрации</h4>--}}
                            {{--</div>--}}
                            {{--<div class="icon">--}}
                                {{--<i class="ion ion-bag"></i>--}}
                            {{--</div>--}}
                            {{--<div class="chart">--}}
                                {{--<canvas id="chart-register"></canvas>--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--<div class="input-group">--}}
                                    {{--<div class="input-group-addon">--}}
                                        {{--<i class="fa fa-calendar"></i>--}}
                                    {{--</div>--}}
                                    {{--<input type="text" class="form-control pull-right" name="datarange" id="drp-register">--}}
                                {{--</div>--}}
                                {{--<!-- /.input group -->--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Последние пользователи</h3>
                            </div>
                            <div class="box-body">
                                <table class="table table-bordered" style="width: 100%;">
                                    <tbody>
                                    <tr>
                                        <th class="ta-center">ID</th>
                                        <th class="ta-center">Логин</th>
                                        <th class="ta-center">Дата</th>
                                    </tr>
                                    @foreach($users as $user)
                                        <tr>
                                            <td class="ta-center">{{$user->id}}</td>
                                            <td class="ta-center">{{$user->login ?: $user->telegram_login}}</td>
                                            <td class="ta-center">{{$user->regdate}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Последние пополнения</h3>
                            </div>
                            <div class="box-body">
                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th class="ta-center">ID</th>
                                        <th class="ta-center">Пользователь</th>
                                        <th class="ta-center">Сумма</th>
                                    </tr>
                                    @foreach($payments as $payment)
                                        <tr>
                                            <td class="ta-center">{{$payment->id}}</td>
                                            <td class="ta-center">{{\App\Libraries\Helpers::convertUs('login', $payment->user_id)}}</td>
                                            <td class="ta-center">{{$payment->amount}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Последние активации</h3>
                            </div>
                            <div class="box-body">
                                <table class="table table-bordered" style="width: 100%!important; table-layout: fixed;">
                                    <tbody style="">
                                    <tr>
                                        <th class="ta-center" style="">Пользователь</th>
                                        <th class="ta-center" style="">Маркетинг</th>
                                    </tr>
                                    @foreach($activations as $activation)
                                        <tr>
                                            <td class="ta-center" style="">{{\App\Libraries\Helpers::convertUs('login', $activation->id_user)}}</td>
                                            <td class="ta-center one-string" style=""><span>{{\App\Helpers\MarksHelper::markName($activation->mark)}}</span></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="box box-primary">--}}
                    {{--<div class="box-header with-border">--}}
                        {{--<h3 class="box-title">{{__('main.admin_statistic')}}</h3>--}}
                    {{--</div>--}}
                    {{--<div class="box-body">--}}
                        {{--<div class="col-lg-4">--}}
                            {{--<div class="col-lg-12 ta-center economy">--}}
                                {{--<h4>{{__('main.admin_users')}}</h4>--}}
                                {{--<h2>{{\App\Models\Users::count()}} <i class="fa fa-users"></i></h2>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-4">--}}
                            {{--<div class="col-lg-12 ta-center economy">--}}
                                {{--<h4>{{__('main.admin_users_active')}}</h4>--}}
                                {{--<h2>{{\App\Models\Users::where('type_follow', '>', 0)->count()}} <i class="fa fa-user-"></i></h2>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-4">--}}
                            {{--<div class="col-lg-12 ta-center economy">--}}
                                {{--<h4>{{__('main.admin_activate')}}</h4>--}}
                                {{--<h2>{{\App\Models\OrdersMarkInside::where('id', '>', 9)->count()}} <i class="fa fa-gift"></i></h2>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-12">--}}
                {{--<div class="box box-primary">--}}
                    {{--<div class="box-header">--}}
                        {{--<h3 class="ta-center">Идеи для доработок</h3>--}}
                    {{--</div>--}}
                    {{--<div class="box-body">--}}
                        {{--Напишите свою идею. Ваша идея будет в общем списке и выставлена на общее голосование. Лучшие идеи будут реализованы.--}}
                        {{--<form action="" class="js-store" method="post">--}}
                            {{--<div class="form-group">--}}
                                {{--<label for="">Заголовок</label>--}}
                                {{--<input type="text" name="name" class="form-control" placeholder="Заголовок идеи">--}}
                                {{--<label for="">Суть</label>--}}
                                {{--<textarea name="" id="" class="form-control" placeholder="Суть идеи"></textarea>--}}
                                {{--<button class="btn btn-primary mt-10">Предложить идею</button>--}}
                            {{--</div>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="row js-store">--}}
            {{--<div class="col-lg-6 col-xs-6">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-aqua">--}}
                    {{--<div class="inner">--}}
                        {{--<div class="col-lg-6 no-padding">--}}
                            {{--<p>Пополнений</p>--}}
                            {{--<example-component></example-component>--}}
                            {{--<h3>{{$count['payments']}}</h3>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-6 no-padding textright">--}}
                            {{--<p>Сумма</p>--}}
                            {{--<h3>{{$count['payments_sum']}}</h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-bag"></i>--}}
                    {{--</div>--}}
                    {{--<div class="chart">--}}
                        {{--<canvas id="chart-payments"></canvas>--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="input-group">--}}
                            {{--<div class="input-group-addon">--}}
                                {{--<i class="fa fa-calendar"></i>--}}
                            {{--</div>--}}
                            {{--<input type="text" class="form-control pull-right" name="datarange" id="drp-payments">--}}
                        {{--</div>--}}
                        {{--<!-- /.input group -->--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 col-xs-6">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-green">--}}
                    {{--<div class="inner">--}}
                        {{--<div class="col-lg-6 no-padding">--}}
                            {{--<p>Активаций</p>--}}
                            {{--<h3>{{$count['activation']}}</h3>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-6 no-padding textright">--}}
                            {{--<p>Сумма</p>--}}
                            {{--<h3>{{$count['activation_sum']}}</h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-bag"></i>--}}
                    {{--</div>--}}
                    {{--<div class="chart">--}}
                        {{--<canvas id="chart-activation"></canvas>--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="input-group">--}}
                            {{--<div class="input-group-addon">--}}
                                {{--<i class="fa fa-calendar"></i>--}}
                            {{--</div>--}}
                            {{--<input type="text" class="form-control pull-right" name="datarange" id="drp-activation">--}}
                        {{--</div>--}}
                        {{--<!-- /.input group -->--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 col-xs-6">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-yellow">--}}
                    {{--<div class="inner">--}}
                        {{--<div class="col-lg-6 no-padding">--}}
                            {{--<p>Визиты</p>--}}
                            {{--<h3>{{$count['visit']}}</h3>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-6 no-padding textright">--}}
                            {{--<p>Просмотры кабинета</p>--}}
                            {{--<h3>{{$count['visit_prof']}}</h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-bag"></i>--}}
                    {{--</div>--}}
                    {{--<div class="chart">--}}
                        {{--<canvas id="chart-visit"></canvas>--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="input-group">--}}
                            {{--<div class="input-group-addon">--}}
                                {{--<i class="fa fa-calendar"></i>--}}
                            {{--</div>--}}
                            {{--<input type="text" class="form-control pull-right" name="datarange" id="drp-visit">--}}
                        {{--</div>--}}
                        {{--<!-- /.input group -->--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 col-xs-6">--}}
                {{--<!-- small box -->--}}
                {{--<div class="small-box bg-red">--}}
                    {{--<div class="inner">--}}
                        {{--<div class="col-lg-6 no-padding">--}}
                            {{--<p>Пользователей</p>--}}
                            {{--<h3>{{$count['users']}}</h3>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="ion ion-bag"></i>--}}
                    {{--</div>--}}
                    {{--<div class="chart">--}}
                        {{--<canvas id="chart-register"></canvas>--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<div class="input-group">--}}
                            {{--<div class="input-group-addon">--}}
                                {{--<i class="fa fa-calendar"></i>--}}
                            {{--</div>--}}
                            {{--<input type="text" class="form-control pull-right" name="datarange" id="drp-register">--}}
                        {{--</div>--}}
                        {{--<!-- /.input group -->--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="col-md-6">--}}
            {{--<div class="box">--}}
                {{--<div class="box-header">--}}
                    {{--<h3>Парковка домена</h3>--}}
                {{--</div>--}}
                {{--<div class="box-body js-parking-refresh">--}}
                    {{--@if($parking == null)--}}
                        {{--<p>Вы не припарковали домен. Ваш необходимо сделать это для запуска сайта.</p>--}}
                        {{--<form action="{{route('storeParking')}}" class="js-parking" method="post">--}}
                            {{--@csrf--}}
                            {{--<div class="input-group">--}}
                                {{--<span class="input-group-addon">http://</span>--}}
                                {{--<input type="text" name="domain" class="form-control" placeholder="Введите домен">--}}
                            {{--</div>--}}
                            {{--<br>--}}
                            {{--<button class="btn btn-primary">Припарковать домен</button>--}}
                        {{--</form>--}}


                    {{--@elseif ($parking['delegate'] == 0)--}}

                        {{--{{$parking['domain']}}--}}
                        {{--<br>--}}
                        {{--Если Ваш сайт работает, подтвердите то что домен делигирован успешно--}}

                        {{--<form action="{{route('delegateParking')}}" class="js-parking" method="post">--}}
                            {{--@csrf--}}
                            {{--<button class="btn btn-success">Делегирован</button>--}}
                        {{--</form>--}}

                        {{--<form action="{{route('deleteParking')}}" class="js-parking" method="post">--}}
                            {{--@csrf--}}
                            {{--<button class="btn btn-danger">Удалить</button>--}}
                        {{--</form>--}}

                    {{--@else--}}
                        {{--{{$parking['domain']}}--}}

                        {{--<form action="{{route('deleteParking')}}" class="js-parking" method="post">--}}
                            {{--@csrf--}}
                            {{--<button class="btn btn-danger">Удалить</button>--}}
                        {{--</form>--}}
                    {{--@endif--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}


    @endif
@endsection
