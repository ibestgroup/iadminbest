@extends("layout.admin.app")

@section('title', __('main.admin_panel'))

@section('content')
    @if (Auth::id() == 1)
        <style>
            .load-users td, table td {
                text-align: center;
            }
        </style>
        <div class="box">
            <div class="box-header">
                <h2>{{ __('main.find_user') }}</h2>
            </div>
            <div class="box-body">
                <div class="input-group">
                    <span class="input-group-addon">{{ __('main.login_or_id') }}</span>
                    <input type="text" class="form-control" placeholder="{{ __('main.start_enter') }}" id="load-users">
                </div>
                <div class="load-users">
                </div>
            </div>
        </div>
        <div class="box" id="box-all-users">
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th>ID</th>
                        <th>{{ __('main.login_name') }}</th>
                        <th>{{ __('main.telegram_login_name') }}</th>
                        {!! (\App\Libraries\Helpers::checkSandBox(true))?'<th>Баланс</th>':'' !!}
                        <th>Email</th>
                        <th>{{ __('main.user_surname') }}</th>
                        <th>{{ __('main.user_name') }}</th>
                        <th>{{ __('main.user_date_register') }}</th>
                        <th>{{ __('main.user_last_active') }}</th>
                        <th>{{ __('main.user_count_ref') }}</th>
                    </tr>
                    @foreach ($users as $user)
                        <tr onclick='window.open("/profile/admin/users/{{$user->id}}","_self")' style="cursor:pointer; {{($user->type_follow)?'color:#ff8d00;':''}}">
                            <td>{{ $user['id'] }}</td>
                            <td>{{ $user['login'] }}</td>
                            <td>{{ $user['telegram_login'] }}</td>
                            {!! (\App\Libraries\Helpers::checkSandBox(true))?'<td>'.$user['balance'].'</td>':'' !!}
                            <td>{{ $user['email'] }}</td>
                            <td>{{ $user['users_data']['lastname'] }}</td>
                            <td>{{ $user['users_data']['name'] }}</td>
                            <td>{{ $user['regdate'] }}</td>
                            <td>{{ $user['last_active'] }}</td>
                            <td>{{ $user['ref_quan'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="custom-paginate clearfix" data-refresh="#box-all-users">
                    {{$users->appends(request()->input())->links()}}
                </div>
            </div>
        </div>
    @endif
@endsection
