@extends("layout.admin.app")

@section('title', 'Админ панель')

@section('content')
@if (Auth::id() == 1)
@inject('helper', "\App\Libraries\Helpers")
@php
    $template = \App\Libraries\Helpers::siteTemplate();
    $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);
    $styleId = \App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['id_in_system'];
@endphp
<style>
    .wrap-login-box {
        margin: auto;
        margin-top: {{$mainStyle['main_style']['margin_top']}}px;
        border: {{$mainStyle['main_style']['border']['width']}}px {{$mainStyle['main_style']['border']['style']}} {{$mainStyle['main_style']['border']['color']}};
        border-radius: {{$mainStyle['main_style']['border_radius']['tl']}}px {{$mainStyle['main_style']['border_radius']['tr']}}px {{$mainStyle['main_style']['border_radius']['br']}}px {{$mainStyle['main_style']['border_radius']['bl']}}px;
        overflow: hidden;
        background-color: {{$mainStyle['main_style']['bg_color_content']}};
    }
</style>
<div class="box" id="edit-login-box" style="overflow: visible;" data-link="{{\Illuminate\Support\Facades\Request::url()}}">
    <div class="box-header">
        <h3 class="box-title">{{ __('main.admin_auth_setting') }}</h3>
    </div>
        <!-- /.box-header -->

    <div class="col-lg-12 mb-10">
        <h4>Заменить стандартные страницы авторизации на созданные в редакторе главной страницы</h4>
        <p>Здесь вы можете указать созданные вами страницы, чтобы при регистрации переадресация не шла на стандартные страницы конструктора.</p>
        <form action="{{route('authPagesStore')}}" method="post" class="js-store">
            <div class="col-lg-6 pl-0">
                <b>{!! __('main.auth_page_login') !!}</b>
                <input type="text" class="form-control" name="login_page" value="{{$authPages->login}}">
            </div>
            <div class="col-lg-6 pr-0">
                <b>{!! __('main.auth_page_register') !!}</b>
                <input type="text" class="form-control" name="register_page" value="{{$authPages->register}}">
            </div>
            <input type="submit" class="btn btn-primary mt-10" value="{{__('main.save')}}">
        </form>
    </div>
    <div class="nav-tabs-custom mt-10">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#auth-login" data-toggle="tab" aria-expanded="true">{{ __('main.admin_auth_sign_in') }}</a></li>
            <li><a href="#auth-register" data-toggle="tab" aria-expanded="true">{{ __('main.admin_auth_sign_up') }}</a></li>
            <li><a href="#auth-restore" data-toggle="tab" aria-expanded="true">{{ __('main.admin_auth_password') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="auth-login">
                <h3 class="mt-0">{{ __('main.admin_auth_page') }} {{ __('main.admin_auth_page_sign_in') }}</h3>
                <div class="box-body wrap-login-box refresh-edit-login" style="position: relative;">
                    <div class="login-box">
                        @php
                            $bgColorBtn = (isset($mainStyle['login_style']['bg_color_btn']))?$mainStyle['login_style']['bg_color_btn']:'';
                            $colorBtn = (isset($mainStyle['login_style']['color_btn']))?$mainStyle['login_style']['color_btn']:'';
                            $bgColorBtnH = (isset($mainStyle['login_style']['bg_color_btn_h']))?$mainStyle['login_style']['bg_color_btn_h']:'';
                            $colorBtnH = (isset($mainStyle['login_style']['color_btn_h']))?$mainStyle['login_style']['color_btn_h']:'';

                            $borderRadiusBtn = $helper->getProperty('border_radius', (isset($mainStyle['login_style']['border_radius_btn']))?$mainStyle['login_style']['border_radius_btn']:'');
                            $borderRadiusBtnH = $helper->getProperty('border_radius', (isset($mainStyle['login_style']['border_radius_btn_h']))?$mainStyle['login_style']['border_radius_btn_h']:'');
                            $borderBtn = $helper->getProperty('border', (isset($mainStyle['login_style']['border_btn']))?$mainStyle['login_style']['border_btn']:'');
                            $borderBtnH = $helper->getProperty('border', (isset($mainStyle['login_style']['border_btn_h']))?$mainStyle['login_style']['border_btn_h']:'');
                        @endphp
                        <style>

                            .login-box {
                                border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['login_style']['border_radius']))?$mainStyle['login_style']['border_radius']:'')}};
                                border: {{$helper->getProperty('border', (isset($mainStyle['login_style']['border']))?$mainStyle['login_style']['border']:'')}};
                                font: {!! $helper->getProperty('font', (isset($mainStyle['login_style']['font']))?$mainStyle['login_style']['font']:'') !!};
                            }

                            .login-box-body {
                                background: {{(isset($mainStyle['login_style']['bg_color_body']))?$mainStyle['login_style']['bg_color_body']:''}};
                            }

                            .login-box h2{
                                background: {{(isset($mainStyle['login_style']['bg_color']))?$mainStyle['login_style']['bg_color']:''}};
                                height: {{(isset($mainStyle['login_style']['height']))?$mainStyle['login_style']['height']:''}}px;
                                line-height: {{(isset($mainStyle['login_style']['height']))?$mainStyle['login_style']['height']:''}}px;
                                font-family: {!! $mainStyle['login_style']['font']['family'] ?? '' !!};
                            }

                            .login-box input {
                                background: {{(isset($mainStyle['login_style']['bg_color_input']))?$mainStyle['login_style']['bg_color_input']:''}};
                                color: {{(isset($mainStyle['login_style']['color_input']))?$mainStyle['login_style']['color_input']:''}};
                                border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['login_style']['border_radius_input']))?$mainStyle['login_style']['border_radius_input']:'')}};
                                border: {{$helper->getProperty('border', (isset($mainStyle['login_style']['border_input']))?$mainStyle['login_style']['border_input']:'')}};
                            }
                            .login-box .form-control-feedback {
                                color: {{(isset($mainStyle['login_style']['color_input_icon']))?$mainStyle['login_style']['color_input_icon']:''}};
                            }
                            .login-box .btn {
                                background-color: {{$bgColorBtn}};
                                color: {{$colorBtn}};
                                border-radius: {{$borderRadiusBtn}};
                                border: {{$borderBtn}};
                            }
                            .login-box .btn:hover {
                                background-color: {{$bgColorBtnH}};
                                color: {{$colorBtnH}};
                                border-radius: {{$borderRadiusBtnH}};
                                border: {{$borderBtnH}};
                            }
                        </style>
                        <!-- /.login-logo -->
                        <div class="login-box-body p-0">
                            <h2>{{(isset($mainStyle['login_style']['text']))?$mainStyle['login_style']['text']:__('main.admin_auth_sign_in')}}</h2>
                            <div class="col-lg-12">
                                <form action="{{route('login')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group has-feedback">
                                        <input type="text" name="login" class="form-control" value="{{old('login')}}" placeholder="{{ __('main.login_name') }}">
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <input type="password" name="password" class="form-control" placeholder="{{ __('main.password') }}">
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="checkbox">
                                                <input type="checkbox" name="remember" id="remember">
                                                <label for="remember">{{ __('main.remember_me') }}</label>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-xs-4">
                                            <button type="submit" class="btn btn-primary btn-flat btn-block btn-flat">{{ __('main.login') }}</button>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </form>
                                <div class="ta-center mt-30">
                                    <a href="{{route('password.request')}}" class="btn btn-warning btn-flat">{{ __('main.reset_password') }}</a><br>
                                    <a href="{{route('register')}}" class="text-center btn btn-primary btn-flat mt-10">{{ __('main.registration') }}</a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                            <!-- /.login-box-body -->
                    </div>
                        <!-- /.login-box -->
                </div>

                <div class="box edit-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ __('main.admin_settings') }}</h3>
                    </div>

                    <form action="{{route('updateElement')}}" method="post" data-refresh='.refresh-edit-login'>
                        @csrf
                        <input type="hidden" name="block_id" value="{{$styleId}}">
                        <input type="hidden" name="type" value="main_style">
                        <div class="box-body">
                            <div class="col-lg-6">
                                <h4>{{ __('main.admin_design_header') }}</h4>
                                <label>{{ __('main.admin_edit_text') }}</label>
                                <input type="text" class="form-control" name="login_style[text]" value="{{(isset($mainStyle['login_style']['text']))?$mainStyle['login_style']['text']:__('main.admin_auth_sign_in')}}">

                                <label>{{ __('main.font') }}</label>
                                {!! $helper->getForm('align', 'login', '', $mainStyle) !!}

                                <label>{{ __('main.height') }}</label>
                                {!! $helper->getForm('height', 'login', '', $mainStyle) !!}

                                <label>{{ __('main.bg_color') }}</label>
                                {!! $helper->getForm('color', 'login', '', $mainStyle, 'bg') !!}
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.bg_color') }}</label>
                                    {!! $helper->getForm('color', 'login', 'body', $mainStyle, 'bg') !!}
                                </div>
                                <div class="clearfix"></div>
                                <h4>{{ __('main.admin_design_inputs') }}</h4>
                                <h5>{{ __('main.color') }}</h5>
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.bg_color') }}</label>
                                    {!! $helper->getForm('color', 'login', 'input', $mainStyle, 'bg') !!}
                                </div>
                                <div class="col-lg-6 pr-0">
                                    <label>{{ __('main.text_color') }}</label>
                                    {!! $helper->getForm('color', 'login', 'input', $mainStyle) !!}
                                </div>
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.icon_color') }}</label>
                                    {!! $helper->getForm('color', 'login', 'input_icon', $mainStyle) !!}
                                </div>

                                <div class="clearfix"></div>
                                <h5>{{ __('main.border') }}</h5>
                                {!! $helper->getForm('border', 'login', 'input', $mainStyle) !!}

                                <h5>{{ __('main.border_radius') }}</h5>
                                {!! $helper->getForm('border_radius', 'login', 'input', $mainStyle) !!}
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-lg-6">
                                <h4>{{ __('main.admin_design_main_block') }}</h4>
                                <h5>{{ __('main.border') }}</h5>
                                {!! $helper->getForm('border', 'login', '', $mainStyle) !!}
                                <h5>{{ __('main.border_radius') }}</h5>
                                {!! $helper->getForm('border_radius', 'login', '', $mainStyle) !!}

                                <h4>{{ __('main.setting_button') }}</h4>
                                {!! $helper->getForm('btn', 'login', '', $mainStyle) !!}
                            </div>
                        </div>
                    </form>
                </div>

            </div>
            <div class="tab-pane" id="auth-register">
                <h3 class="mt-0">{{ __('main.admin_auth_page') }} {{ __('main.admin_auth_page_sign_up') }}</h3>
                <div class="box-body wrap-login-box refresh-edit-register" style="position:relative;">
                    <div class="register-box">

                        @php
                            $bgColorBtn = (isset($mainStyle['register_style']['bg_color_btn']))?$mainStyle['register_style']['bg_color_btn']:'';
                            $colorBtn = (isset($mainStyle['register_style']['color_btn']))?$mainStyle['register_style']['color_btn']:'';
                            $bgColorBtnH = (isset($mainStyle['register_style']['bg_color_btn_h']))?$mainStyle['register_style']['bg_color_btn_h']:'';
                            $colorBtnH = (isset($mainStyle['register_style']['color_btn_h']))?$mainStyle['register_style']['color_btn_h']:'';

                            $borderRadiusBtn = $helper->getProperty('border_radius', (isset($mainStyle['register_style']['border_radius_btn']))?$mainStyle['register_style']['border_radius_btn']:'');
                            $borderRadiusBtnH = $helper->getProperty('border_radius', (isset($mainStyle['register_style']['border_radius_btn_h']))?$mainStyle['register_style']['border_radius_btn_h']:'');
                            $borderBtn = $helper->getProperty('border', (isset($mainStyle['register_style']['border_btn']))?$mainStyle['register_style']['border_btn']:'');
                            $borderBtnH = $helper->getProperty('border', (isset($mainStyle['register_style']['border_btn_h']))?$mainStyle['register_style']['border_btn_h']:'');
                        @endphp
                        <style>

                            .register-box {
                                border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['register_style']['border_radius']))?$mainStyle['register_style']['border_radius']:'')}};
                                border: {{$helper->getProperty('border', (isset($mainStyle['register_style']['border']))?$mainStyle['register_style']['border']:'')}};
                                font: {!! $helper->getProperty('font', (isset($mainStyle['register_style']['font']))?$mainStyle['register_style']['font']:'') !!};
                            }

                            .register-box-body {
                                background: {{(isset($mainStyle['register_style']['bg_color_body']))?$mainStyle['register_style']['bg_color_body']:''}};
                            }

                            .register-box h2{
                                background: {{(isset($mainStyle['register_style']['bg_color']))?$mainStyle['register_style']['bg_color']:''}};
                                height: {{(isset($mainStyle['register_style']['height']))?$mainStyle['register_style']['height']:''}}px;
                                line-height: {{(isset($mainStyle['register_style']['height']))?$mainStyle['register_style']['height']:''}}px;
                                font-family: {!! $mainStyle['register_style']['font']['family'] ?? '' !!};
                            }

                            .register-box input {
                                background: {{(isset($mainStyle['register_style']['bg_color_input']))?$mainStyle['register_style']['bg_color_input']:''}};
                                color: {{(isset($mainStyle['register_style']['color_input']))?$mainStyle['register_style']['color_input']:''}};
                                border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['register_style']['border_radius_input']))?$mainStyle['register_style']['border_radius_input']:'')}};
                                border: {{$helper->getProperty('border', (isset($mainStyle['register_style']['border_input']))?$mainStyle['register_style']['border_input']:'')}};
                            }
                            .register-box .form-control-feedback {
                                color: {{(isset($mainStyle['register_style']['color_input_icon']))?$mainStyle['register_style']['color_input_icon']:''}};
                            }
                            .register-box .btn {
                                background-color: {{$bgColorBtn}};
                                color: {{$colorBtn}};
                                border-radius: {{$borderRadiusBtn}};
                                border: {{$borderBtn}};
                            }
                            .register-box .btn:hover {
                                background-color: {{$bgColorBtnH}};
                                color: {{$colorBtnH}};
                                border-radius: {{$borderRadiusBtnH}};
                                border: {{$borderBtnH}};
                            }
                        </style>
                        <div class="register-box-body p-0">
                            <h2>{{(isset($mainStyle['register_style']['text']))?$mainStyle['register_style']['text']:__('main.admin_auth_sign_up')}}</h2>
                            <div class="col-lg-12" style="padding-bottom: 20px; padding-top: 20px;">
                                <form>
                                    <div class="form-group has-feedback">
                                        <input type="text" name="login" class="form-control" placeholder="{{ __('main.login_name') }}">
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <input type="email" name="email" class="form-control" placeholder="Email">
                                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <input type="password" name="password" class="form-control" placeholder="{{ __('main.password') }}">
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <input type="password" name="password_confirmation" class="form-control" placeholder="{{ __('main.repeat_password') }}">
                                        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="checkbox icheck">
                                                <input id="user-terms" type="checkbox" name="terms">
                                                <label for="user-terms">{{ __('main.accept') }} <a href="#">{{ __('main.terms') }}</a></label>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <div class="form-group has-feedback">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat">{{ __('main.registration') }}</button>
                                    </div>
                                </form>

                                <div class="ta-center">
                                    <a href="{{route('login')}}" class="text-center btn btn-info btn-flat">{{ __('main.login') }}</a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- /.form-box -->
                    </div>
                </div>

                <div class="box edit-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ __('main.admin_settings') }}</h3>
                    </div>

                    <form action="{{route('updateElement')}}" method="post" data-refresh='.refresh-edit-register'>
                        @csrf
                        <input type="hidden" name="block_id" value="{{$styleId}}">
                        <input type="hidden" name="type" value="main_style">
                        <div class="box-body">
                            <div class="col-lg-6">
                                <h4>{{ __('main.admin_design_header') }}</h4>
                                <label>{{ __('main.admin_edit_text') }}</label>
                                <input type="text" class="form-control" name="register_style[text]" value="{{(isset($mainStyle['register_style']['text']))?$mainStyle['register_style']['text']:'Регистрация'}}">

                                <label>{{ __('main.font') }}</label>
                                {!! $helper->getForm('align', 'register', '', $mainStyle) !!}
                                <label>{{ __('main.height') }}</label>
                                {!! $helper->getForm('height', 'register', '', $mainStyle) !!}

                                <label>{{ __('main.bg_color') }}</label>
                                {!! $helper->getForm('color', 'register', '', $mainStyle, 'bg') !!}
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.bg_color') }}</label>
                                    {!! $helper->getForm('color', 'register', 'body', $mainStyle, 'bg') !!}
                                </div>
                                <div class="clearfix"></div>
                                <h4>{{ __('main.admin_design_inputs') }}</h4>
                                <h5>{{ __('main.color') }}</h5>
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.bg_color') }}</label>
                                    {!! $helper->getForm('color', 'register', 'input', $mainStyle, 'bg') !!}
                                </div>
                                <div class="col-lg-6 pr-0">
                                    <label>{{ __('main.text_color') }}</label>
                                    {!! $helper->getForm('color', 'register', 'input', $mainStyle) !!}
                                </div>
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.icon_color') }}</label>
                                    {!! $helper->getForm('color', 'register', 'input_icon', $mainStyle) !!}
                                </div>

                                <div class="clearfix"></div>
                                <h5>{{ __('main.border') }}</h5>
                                {!! $helper->getForm('border', 'register', 'input', $mainStyle) !!}

                                <h5>{{ __('main.border_radius') }}</h5>
                                {!! $helper->getForm('border_radius', 'register', 'input', $mainStyle) !!}
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-lg-6">
                                <h4>{{ __('main.admin_design_main_block') }}</h4>
                                <h5>{{ __('main.border') }}</h5>
                                {!! $helper->getForm('border', 'register', '', $mainStyle) !!}
                                <h5>{{ __('main.border_radius') }}</h5>
                                {!! $helper->getForm('border_radius', 'register', '', $mainStyle) !!}

                                <h4>{{ __('main.setting_button') }}</h4>
                                {!! $helper->getForm('btn', 'register', '', $mainStyle) !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="tab-pane" id="auth-restore">
                <h3 class="mt-0">{{ __('main.admin_auth_page') }} {{ __('main.admin_auth_page_sign_up') }}</h3>
                <div class="box-body wrap-login-box refresh-edit-restore" style="position:relative;">
                    <div class="restore-box login-box">
                        @php
                            $bgColorBtn = (isset($mainStyle['restore_style']['bg_color_btn']))?$mainStyle['restore_style']['bg_color_btn']:'';
                            $colorBtn = (isset($mainStyle['restore_style']['color_btn']))?$mainStyle['restore_style']['color_btn']:'';
                            $bgColorBtnH = (isset($mainStyle['restore_style']['bg_color_btn_h']))?$mainStyle['restore_style']['bg_color_btn_h']:'';
                            $colorBtnH = (isset($mainStyle['restore_style']['color_btn_h']))?$mainStyle['restore_style']['color_btn_h']:'';

                            $borderRadiusBtn = $helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius_btn']))?$mainStyle['restore_style']['border_radius_btn']:'');
                            $borderRadiusBtnH = $helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius_btn_h']))?$mainStyle['restore_style']['border_radius_btn_h']:'');
                            $borderBtn = $helper->getProperty('border', (isset($mainStyle['restore_style']['border_btn']))?$mainStyle['restore_style']['border_btn']:'');
                            $borderBtnH = $helper->getProperty('border', (isset($mainStyle['restore_style']['border_btn_h']))?$mainStyle['restore_style']['border_btn_h']:'');
                        @endphp
                        <style>

                            .restore-box {
                                border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius']))?$mainStyle['restore_style']['border_radius']:'')}};
                                border: {{$helper->getProperty('border', (isset($mainStyle['restore_style']['border']))?$mainStyle['restore_style']['border']:'')}};
                                font: {!! $helper->getProperty('font', (isset($mainStyle['restore_style']['font']))?$mainStyle['restore_style']['font']:'') !!};
                            }

                            .restore-box-body {
                                background: {{(isset($mainStyle['restore_style']['bg_color_body']))?$mainStyle['restore_style']['bg_color_body']:''}};
                            }

                            .restore-box h2{
                                background: {{(isset($mainStyle['restore_style']['bg_color']))?$mainStyle['restore_style']['bg_color']:''}};
                                height: {{(isset($mainStyle['restore_style']['height']))?$mainStyle['restore_style']['height']:''}}px;
                                line-height: {{(isset($mainStyle['restore_style']['height']))?$mainStyle['restore_style']['height']:''}}px;
                                font-family: {!! $mainStyle['restore_style']['font']['family'] ?? '' !!};
                            }

                            .restore-box input {
                                background: {{(isset($mainStyle['restore_style']['bg_color_input']))?$mainStyle['restore_style']['bg_color_input']:''}};
                                color: {{(isset($mainStyle['restore_style']['color_input']))?$mainStyle['restore_style']['color_input']:''}};
                                border-radius: {{$helper->getProperty('border_radius', (isset($mainStyle['restore_style']['border_radius_input']))?$mainStyle['restore_style']['border_radius_input']:'')}};
                                border: {{$helper->getProperty('border', (isset($mainStyle['restore_style']['border_input']))?$mainStyle['restore_style']['border_input']:'')}};
                            }
                            .restore-box .form-control-feedback {
                                color: {{(isset($mainStyle['restore_style']['color_input_icon']))?$mainStyle['restore_style']['color_input_icon']:''}};
                            }
                            .restore-box .btn {
                                background-color: {{$bgColorBtn}};
                                color: {{$colorBtn}};
                                border-radius: {{$borderRadiusBtn}};
                                border: {{$borderBtn}};
                            }
                            .restore-box .btn:hover {
                                background-color: {{$bgColorBtnH}};
                                color: {{$colorBtnH}};
                                border-radius: {{$borderRadiusBtnH}};
                                border: {{$borderBtnH}};
                            }
                        </style>
                        <div class="restore-box-body p-0">
                            <h2>{{(isset($mainStyle['restore_style']['text']))?$mainStyle['restore_style']['text']:__('main.admin_auth_password')}}</h2>
                            <div class="col-lg-12" style="padding-bottom: 20px; padding-top: 20px;">

                                <form>
                                    {{ csrf_field() }}
                                    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email..." required>
                                        <label for="email" class="glyphicon glyphicon-envelope form-control-feedback"></label>
                                        @if ($errors->has('email'))
                                            <span class="help-block">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback">
                                        <button type="submit" class="btn btn-primary btn-block btn-flat">
                                            {{ __('main.send_link') }}
                                        </button>
                                    </div>
                                </form>

                                <div class="ta-center">
                                    <a href="{{route('login')}}" class="text-center btn btn-info btn-flat">{{ __('main.login') }}</a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="box edit-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ __('main.admin_settings') }}</h3>
                    </div>

                    <form action="{{route('updateElement')}}" method="post" data-refresh='.refresh-edit-restore'>
                        @csrf
                        <input type="hidden" name="block_id" value="{{$styleId}}">
                        <input type="hidden" name="type" value="main_style">
                        <div class="box-body">
                            <div class="col-lg-6">
                                <h4>{{ __('main.admin_design_header') }}</h4>
                                <label>{{ __('main.admin_edit_text') }}</label>
                                <input type="text" class="form-control" name="restore_style[text]" value="{{(isset($mainStyle['restore_style']['text']))?$mainStyle['restore_style']['text']:'Регистрация'}}">

                                <label>{{ __('main.font') }}</label>
                                {!! $helper->getForm('align', 'restore', '', $mainStyle) !!}

                                <label>{{ __('main.height') }}</label>
                                {!! $helper->getForm('height', 'restore', '', $mainStyle) !!}

                                <label>{{ __('main.bg_color') }}</label>
                                {!! $helper->getForm('color', 'restore', '', $mainStyle, 'bg') !!}
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.bg_color') }}</label>
                                    {!! $helper->getForm('color', 'restore', 'body', $mainStyle, 'bg') !!}
                                </div>
                                <div class="clearfix"></div>
                                <h4>{{ __('main.admin_design_inputs') }}</h4>
                                <h5>{{ __('main.color') }}</h5>
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.bg_color') }}</label>
                                    {!! $helper->getForm('color', 'restore', 'input', $mainStyle, 'bg') !!}
                                </div>
                                <div class="col-lg-6 pr-0">
                                    <label>{{ __('main.text_color') }}</label>
                                    {!! $helper->getForm('color', 'restore', 'input', $mainStyle) !!}
                                </div>
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.icon_color') }}</label>
                                    {!! $helper->getForm('color', 'restore', 'input_icon', $mainStyle) !!}
                                </div>

                                <div class="clearfix"></div>
                                <h5>{{ __('main.border') }}</h5>
                                {!! $helper->getForm('border', 'restore', 'input', $mainStyle) !!}

                                <h5>{{ __('main.border_radius') }}</h5>
                                {!! $helper->getForm('border_radius', 'restore', 'input', $mainStyle) !!}
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-lg-6">
                                <h4>{{ __('main.admin_design_main_block') }}</h4>
                                <h5>{{ __('main.border') }}</h5>
                                {!! $helper->getForm('border', 'restore', '', $mainStyle) !!}
                                <h5>{{ __('main.border_radius') }}</h5>
                                {!! $helper->getForm('border_radius', 'restore', '', $mainStyle) !!}

                                <h4>{{ __('main.setting_button') }}</h4>
                                {!! $helper->getForm('btn', 'restore', '', $mainStyle) !!}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
        <!-- /.box-body -->
    <!-- /.box -->

@endif
@endsection
