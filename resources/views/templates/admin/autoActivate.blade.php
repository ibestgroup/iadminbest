@extends("layout.admin.app")

@section('title', __('main.admin_panel'))

@section('content')
    @if (Auth::id() == 1)
        <div class="box">
            <!-- /.box-header -->
            <div class="box-header">
                <h4>{{ __('main.auto_activate') }}</h4>
            </div>
            <div class="box-body">
                @if (\App\Models\SiteInfo::find(1)->start)
                    Функция доступна только до старта проекта
                @else
                    <form action="{{route('autoActivateStore')}}" class="js-store js-auto-activate" method="post">
                        Сколько пользователей активировать?
                        <select name="activate_count" id="activate-count" class="form-control">
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="40">40</option>
                            <option value="50">50</option>
                        </select>
                        Какое будет указано время активации у пользователей?
                        <select name="time_type" id="" class="form-control">
                            <option value="1">Реальное время активации</option>
                            <option value="2">Дата старта проекта</option>
                        </select>
                        Какой тариф вы хотите активировать?
                        <select name="mark" id="mark" class="form-control">
                            @foreach ($marks as $mark)
                                <option value="{{$mark->id}}" >{{\App\Helpers\MarksHelper::markName($mark->id)}}</option>
                            @endforeach
                        </select>
                        <div id="for-result" style="display: none;">Цена тарифа <span id="mark-enter"></span>{!! \App\Libraries\Helpers::siteCurrency('icon') !!} Эта сумма есть у <span class="count-users"></span> пользователей</div>
                        {{--<input type="submit" class="btn btn-primary mt-10" id="start-activate" style="display: none;" value="Активировать">--}}
                        <button class="btn btn-primary mt-10" id="start-activate" style="display: none;">Активировать <span class="count-users-result"></span> неактивированных пользователей</button>
                    </form>
                @endif
            </div>
        </div>
    @endif
@endsection
