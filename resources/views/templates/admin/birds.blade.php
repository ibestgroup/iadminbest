@extends("layout.admin.app")

@section('title', 'Админ панель')

@section('content')
    @if (Auth::id() == 1)

        <div class="row js-birds">
            <div class="col-md-12">
                <div class="box">

                    <div class="box-header">
                        <a href="{{route('admin')}}" class="btn btn-primary btn-flat mt-5">Админ панель</a>
                        <a href="{{route('createTypesOrder')}}" class="btn btn-primary btn-flat mt-5">Категории вкладов</a>
                        <h2>Добавьте источники дохода</h2>
                        <p>Выберите с помощью чего будут добывать Ваши пользователи. Это могут быть деревья и фрукты, птицы и яйца, машины и заказы. Все что Вам вздумается.</p>
                    </div>
                    <div class="box-body">

                        <div class="col-md-12 type-order-item">
                            <form action="{{route('storeBirds')}}" method="post" enctype="multipart/form-data" class="js-store">
                                @csrf
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Название</label>
                                        <input class="form-control" type="text" name="name" placeholder="Введите имя" autocomplete="off">
                                        <label>Что зарабатывает</label>
                                        <select class="form-control" name="type">
                                            @forelse($typesOrder as $order)
                                                <option value="{{$order['id']}}">{{$order['egg']}}</option>
                                            @empty
                                                <option disabled="disabled">Создайте категорию</option>
                                            @endforelse
                                        </select>
                                        <label>Тип цены
                                            <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                  title="Если цена фиксированная, то доход указывается точным числом.
                                                        Если диапазон, то доход указывается в процентах.">?</span>
                                        </label>
                                        <select class="form-control" name="range">
                                            <option value="0">Фиксированная цена</option>
                                            <option value="1">Диапазон (от и до)</option>
                                        </select>
                                        <div class="col-lg-12 p-0">
                                            <div class="col-lg-6 pl-0">
                                                <label class="range-price">Цена</label>
                                                <input class="form-control" type="number" name="price" placeholder="Цена" autocomplete="off">
                                            </div>
                                            <div class="col-lg-6 pr-0 wrap-range-max-price" style="display: none;">
                                                <label class="range-max-price">Верхняя граница цены</label>
                                                <input class="form-control" type="number" name="max_price" placeholder="Цена (до)" autocomplete="off">
                                            </div>
                                        </div>
                                        <label>Доход (<span class="type-profit">сумма</span>)</label>
                                        <span style="background: #ff1f1f;" class="form-prompt" data-toggle="tooltip" data-placement="top"
                                              title="Если доход в процентах, обратите внимание, что если вы установите 50%,
                                              то это не +50% а всего 50% от вклада. То есть если вы хотите чтобы пользователь
                                              получил прибыль в размере 50% нужно установить 150%.">!</span>
                                        <input class="form-control" type="number" name="profit" placeholder="Доход" autocomplete="off">


                                        <div class="col-lg-6 pl-0">
                                            <label>Срок жизни
                                                <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                      title="Бесконечный - постоянно приносит доход.
                                                      Определенный срок - по истечению срока, перестает приносить доход и в дальнейшем являтеся неавтивным.">?</span>
                                            </label>
                                            <select class="form-control" name="end">
                                                <option value="0">Бесконечный</option>
                                                <option value="1">Определенный срок</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6 pr-0">
                                            <div class="only-end" style="display: none;">
                                                <label>Сбор по окончанию срока
                                                    <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                          title="Если установлено да, то средства замораживаются и прибыль можно вывести только после установленного срока.
                                                          Если нет, то пользователь может выводить доход в любой момент">?</span>
                                                </label>
                                                <select class="form-control" name="only_end">
                                                    <option value="0">Нет</option>
                                                    <option value="1">Да</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-lg-6 pl-0">
                                            <label>Количество активных вкладов</label>
                                                    <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                          title="Если установлено да, то средства замораживаются и прибыль можно вывести только после установленного срока.
                                                          Если нет, то пользователь может выводить доход в любой момент">?</span>
                                            <select class="form-control select-now-activate" name="select_now_activate">
                                                <option value="0" selected="selected">Неограниченно</option>
                                                <option value="1">Указать количество</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6 pr-0 now-activate" style="display: none;">
                                            <label>Количество активных вкладов
                                                <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                      title="Максимально возможное количество одновременных вкладов">?</span>
                                            </label>
                                            <input type="number" name="now_activate" class="form-control" value="0">
                                        </div>
                                        <div class="clearfix"></div>

                                        <label class="end-text">Каждые сколько секунд</label>
                                        <div class="clearfix"></div>
                                        <div class="col-md-6 pl-0">
                                            <input class="form-control" type="number" name="second" placeholder="За промежуток времени" autocomplete="off">
                                        </div>
                                        <div class="col-md-6 pr-0">
                                            <select name="type_time" id="" class="form-control">
                                                <option value="1">Секунд</option>
                                                <option value="2">Минут</option>
                                                <option value="3">Часов</option>
                                                <option value="4">Дней</option>
                                            </select>
                                        </div>

                                        <label>Текст на кнопке
                                            <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                  title="Что будет написано на кнопке в магазине">?</span>
                                        </label>
                                        <input class="form-control" type="text" name="buy_btn" placeholder="Текст кнопки" value="Сделать вклад" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <p class="ta-center">Изображение</p>
                                    <div class="kv-avatar text-center">
                                        <div class="file-loading">
                                            <input id="bird-create" name="bird_image" type="file" data-default="/iadminbest/storage/app/resource/no-image.png">
                                        </div>
                                    </div>
                                </div>
                                <input class="btn btn-primary type-order-item-btn" type="submit">
                            </form>
                        </div>
                        <h2>Ваши пакеты на сайте</h2>
                        @foreach($birds as $bird)
                            <div class="col-md-12 type-order-item">
                                <form action="{{route('updateBirds')}}" method="post" enctype="multipart/form-data" class="js-store">
                                    @csrf
                                    <input type="hidden" name="bird_id" value="{{$bird['id']}}">
                                    <div class="form-group col-md-8">
                                        <label for="name-{{$bird['id']}}">Название</label>
                                        <input type="text" class="form-control" name="name" id="name-{{$bird['id']}}" placeholder="Имя" value="{{$bird['name']}}">

                                        <label for="name-{{$bird['id']}}">Что зарабатывает</label>
                                        <select class="form-control" name="type" id="" data-id-egg="{{$bird['id']}}">
                                            @forelse($typesOrder as $order)
                                                <option value="{{$order['id']}}" {{($bird['type'] == $order['id'])?'selected':''}} data-end="{{$order->end}}" data-egg="{{$order->egg}}"}}>{{$order['egg']}}</option>
                                            @empty
                                                <option disabled="disabled">Создайте категорию</option>
                                            @endforelse
                                        </select>

                                        <label>Тип цены
                                            <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                  title="Если цена фиксированная, то доход указывается точным числом.
                                                        Если диапазон, то доход указывается в процентах.">?</span>
                                        </label>
                                        <select class="form-control" name="range">
                                            <option value="0" {{($bird['range'] == 0)?'selected="selected"':''}}>Фиксированная цена</option>
                                            <option value="1" {{($bird['range'] == 1)?'selected="selected"':''}}>Диапазон (от и до)</option>
                                        </select>

                                        <div class="col-lg-12 p-0">
                                            <div class="col-lg-6 pl-0">
                                                <label class="range-price" for="price-{{$bird['id']}}">Цена</label>
                                                <input class="form-control" type="number" name="price" id="price-{{$bird['id']}}" value="{{(int)$bird['price']}}" placeholder="Цена" autocomplete="off">
                                            </div>
                                            <div class="col-lg-6 pr-0 wrap-range-max-price" style="{{($bird['range'] == 0)?'display: none;':''}}">
                                                <label class="range-max-price" for="max-price-{{$bird['id']}}">Верхняя граница цены</label>
                                                <input class="form-control" type="number" name="max_price" id="max-price-{{$bird['id']}}" value="{{(int)$bird['max_price']}}" placeholder="Цена (до)" autocomplete="off">
                                            </div>
                                        </div>

                                        <label for="profit-{{$bird['id']}}">
                                            Доход (<span class="type-profit">сумма</span>) (<span class="profit-egg" data-id-egg="{{$bird['id']}}">{{\App\Libraries\Helpers::getTypeOrder($bird['type'], 'egg')}}</span>)
                                        </label>
                                        <span style="background: #ff1f1f;" class="form-prompt" data-toggle="tooltip" data-placement="top"
                                              title="Если доход в процентах, обратите внимание, что если вы установите 50%,
                                              то это не +50% а всего 50% от вклада. То есть если вы хотите чтобы пользователь
                                              получил прибыль в размере 50% нужно установить 150%.">!</span>
                                        <input type="text" class="form-control" name="profit" id="profit-{{$bird['id']}}" placeholder="Имя" value="{{$bird['profit']}}">

                                        <div class="col-lg-6 pl-0">
                                            <label>Срок жизни
                                                <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                      title="Бесконечный - постоянно приносит доход.
                                                      Определенный срок - по истечению срока, перестает приносить доход и в дальнейшем являтеся неавтивным.">?</span>
                                            </label>
                                            <select class="form-control" name="end">
                                                <option value="0" {{($bird['end'] == 0)?'selected="selected"':''}}>Бесконечный</option>
                                                <option value="1" {{($bird['end'] == 1)?'selected="selected"':''}}>Определенный срок</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6 pr-0">
                                            <div class="only-end" style="{{($bird['end'] == 0)?'display: none;':''}}">
                                                <label>Сбор по окончанию срока
                                                    <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                          title="Если установлено да, то средства замораживаются и прибыль можно вывести только после установленного срока.
                                                          Если нет, то пользователь может выводить доход в любой момент">?</span>
                                                </label>
                                                <select class="form-control" name="only_end">
                                                    <option value="0" {{($bird['only_end'] == 0)?'selected="selected"':''}}>Нет</option>
                                                    <option value="1" {{($bird['only_end'] == 1)?'selected="selected"':''}}>Да</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                        <div class="col-lg-6 pl-0">
                                            <label>Количество активных вкладов</label>
                                                <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                      title="Сколько активных вкладов у пользователя может быть одновременно на сайте.">?</span>
                                            <select class="form-control select-now-activate" name="select_now_activate">
                                                <option value="0" {{($bird['now_activate'] == 0)?'selected="selected"':''}}>Неограниченно</option>
                                                <option value="1" {{($bird['now_activate'] > 1)?'selected="selected"':''}}>Указать количество</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-6 pr-0 now-activate" style="{{($bird['now_activate'] == 0)?'display:none;':''}}">
                                            <label>Количество активных вкладов
                                                <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                      title="Максимально возможное количество одновременных вкладов">?</span>
                                            </label>
                                            <input type="number" name="now_activate" class="form-control" value="{{$bird['now_activate']}}">
                                        </div>
                                        <div class="clearfix"></div>

                                        <label for="second-{{$bird['id']}}" data-id-egg="{{$bird['id']}}" class="end-text">
                                            @if ($bird->end == 1)
                                                Срок жизни (секунд)
                                            @else
                                                Каждые сколько секунд
                                            @endif
                                        </label>
                                        <input type="number" class="form-control" name="second" id="second-{{$bird['id']}}" placeholder="" value="{{$bird['second']}}">


                                        <label>Текст на кнопке
                                            <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                                  title="Что будет написано на кнопке в магазине">?</span>
                                        </label>
                                        <input class="form-control" type="text" name="buy_btn" placeholder="Текст кнопки" value="{{$bird['buy_btn']}}" autocomplete="off">

                                    </div>
                                    <div class="col-md-4">
                                        <p class="ta-center">Изображение</p>
                                        <div class="kv-avatar text-center">
                                            <div class="file-loading">
                                                <input id="avatar" name="bird_image" type="file" data-default="/iadminbest/storage/app/{{$bird['image']}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <input type="submit" class="btn btn-primary type-order-item-btn save" value="Сохранить">
                                </form>
                                <form action="{{route('deleteBirds')}}" method="post" enctype="multipart/form-data" class="js-store">
                                    <input type="hidden" name="bird_id" value="{{$bird['id']}}">
                                    <input type="submit" class="btn btn-danger type-order-item-btn save" value="Удалить">
                                </form>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
