@extends("layout.admin.app")

@section('title', 'Админ панель')

@section('content')
    @if (Auth::id() == 1)
        <div class="box">
            <!-- /.box-header -->
            <div class="box-header">
                <h3>Владения пользователей</h3>
                <a href="{{route('birdsList')}}" class="btn btn-primary">Все</a>
                @foreach($typesOrder as $order)
                    <a href="{{route('birdsList')}}?type={{$order['id']}}" class="btn btn-primary">{{$order['bird']}}</a>
                @endforeach
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th style="width: 135px">Владелец (#ID)</th>
                        <th>Категория</th>
                        <th>Объект</th>
                        <th>Кол-во</th>
                        <th>Баланс(на складе)</th>
                        <th>Действие</th>
                    </tr>
                @forelse($birds as $bird)
                        <tr>
                            <td>{{$bird->id}}</td>
                            <td>
                                <a href="{{route('userInfo', ['id' => $bird->user_id])}}">{{\App\Libraries\Helpers::convertUs('login', $bird->user_id)}}</a>
                                (<a href="{{route('userInfo', ['id' => $bird->user_id])}}">#{{$bird->user_id}}</a>)
                            </td>
                            <td>
                                <a href="{{route('birdsList')}}?type={{$bird->bird->type}}">
                                    {{$bird->bird->typesOrder->bird}}
                                </td>
                            <td>
                                <a href="{{route('birdsList')}}?order_id={{$bird->bird->id}}">
                                {{$bird->bird->name}}
                                </a>
                            </td>
                            <td>
                                {{$bird->number}}
                            </td>
                            <td>
                                <input type="number" form="order-edit-{{$bird->id}}" name="stock" value="{{$bird->stock}}">
                            </td>
                            <td>
                                <form action="{{route('birdDelete')}}" class="js-birds-order" id="order-del-{{$bird->id}}" method="post">
                                    @csrf
                                    <input type="hidden" name="bird_id" value="{{$bird->id}}">
                                </form>
                                <form action="{{route('birdEdit')}}" class="js-birds-order" id="order-edit-{{$bird->id}}" method="post">
                                    @csrf
                                    <input type="hidden" name="bird_id" value="{{$bird->id}}">
                                </form>
                                <input type="submit" value="Сохранить" form="order-edit-{{$bird->id}}" class="btn btn-primary">
                                <input type="submit" value="Удалить" form="order-del-{{$bird->id}}" class="btn btn-danger">
                            </td>
                        </tr>
                @empty
                    <tr>
                        <td></td>
                        <td>
                            В данной категории нет объектов
                        </td>
                    </tr>
                @endforelse
                </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection
