@extends("layout.admin.app")

@section('title', __('bot.bot_settings'))

@section('content')
    <script src="https://cdn.ckeditor.com/4.16.2/full/ckeditor.js"></script>

    <div class="box">
        <div class="box-header">
            <h2>{{ __('bot.bot_settings') }}</h2>
        </div>
        <div class="box-body">
            @if ($isConnected)
                @inject("helper", "\App\Libraries\Helpers")
                @if (empty(\App\Models\Users::find(1)->telegram_id))
                    <p>Для того чтобы начать пользоваться ботом, вам нужно привязать аккаунт администратора.
                        Для этого, просто отправьте вашему боту(<a href="https://t.me/{{$helper->getBotUsername()}}" target="_blank">&#64;{{$helper->getBotUsername()}}</a>) этот токен, с аккаунта, который будет администратором: <b>{{ \App\Models\Users::find(1)->telegram_bind_code }}</b><br>
                        После этого вы сможете дальше настраивать бот
                    </p>
                @else
                    <div class="additional-field"
                        >Шапка
                        <a data-id="header" class="bot-menu-btn edit btn btn-info" data-toggle="modal" data-target="#bot-settings-edit"><i class="fa fa-edit" aria-hidden="true"></i></a>
                    </div>
                    <div class="additional-field">
                        Подвал
                        <a data-id="footer" class="bot-menu-btn edit btn btn-info" data-toggle="modal" data-target="#bot-settings-edit"><i class="fa fa-edit" aria-hidden="true"></i></a>
                    </div>
                    <div class="additional-field">
                        Разделитель
                        <a data-id="separator" class="bot-menu-btn edit btn btn-info" data-toggle="modal" data-target="#bot-settings-edit"><i class="fa fa-edit" aria-hidden="true"></i></a>
                    </div>
                    <div id="tree1"></div>
                @endif
            @else
                {{--<form action="{{ route('addApiKey') }}" class="js-store">--}}
                    {{--<input type="text" name="key">--}}
                    {{--<input type="submit" value="save">--}}
                {{--</form>--}}
                <p>
                    Введите сюда API ключ вашего бота, который вы получили через BotFather. Если у вас нет API ключа, то вот инструкция, как получить ключ за 2 минуты.
                    <a class="btn btn-primary" href="{{ route('botSettingsManual') }}">Инструкция</a>
                </p>
                <form action="{{ route('addApiKey') }}" class="js-store">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputEmail">API ключ вашего бота</label>
                            <input type="text" class="form-control" id="inputEmail" placeholder="API ключ вашего бота" name="key">
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Привязать бота</button>
                    </div>
                </form>
            @endif
        </div>
    </div>

    <div class="modal fade in" id="bot-settings-edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Настройка</h4>
                </div>
                <div class="modal-body">
                    <form action="{{ route('botSettingsUpdate') }}" class="js-store" id="edit-bot-widget">
                        <input class="form-control col-sm-4" type="hidden" name="id">
                        <div class="row">
                            <div class="col-sm-6 col-md-6 menu-name">
                                <label>Название пункта меню</label>
                                <input class="form-control col-sm-4" type="text" name="name">
                            </div>

                            <div class="col-sm-6 col-md-6 widget-name">
                                <label>Виджет</label>
                                <select class="form-control edit-form-widget" name="widget" id="">
                                    <option value="">Без виджета</option>
                                    @foreach($widgetList as $item)
                                        <option value="{{ $item->name }}">{{ $item->name_ru }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-12 col-md-12">
                                <label>Содержимое меню</label>
                                <div class="content-menu">
                                    <textarea id="editor" class="form-control" name="text"></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 edit-widget mt-10">
                                здусь нужно выводить стандартный виджет если есть, но это видимо в js тоже придется делать
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>
                    <button type="submit" class="btn btn-primary" form="edit-bot-widget">Применить</button>
                </div>
            </div>
        </div>
    </div>
    <style>
        .bot-menu-btn {
            /*display: inline-block;*/
            width: 25px;
            height: 25px;
            line-height: 19px;
            text-align: center;
            margin: 3px;
            border-radius: 50%;
            padding: 3px;
            color: #fff;
        }

        .jqtree-element.jqtree_common,
        .additional-field {
            padding: 3px 15px;
            line-height: 35px;
            border-bottom: 1px solid #aaa;
            border-left: 1px solid #aaa;
            border-radius: 10px;
        }

        .jqtree-title.jqtree_common:focus-visible {
            outline-width: 0px!important;
        }

        ul.jqtree-tree li.jqtree-selected > .jqtree-element, ul.jqtree-tree li.jqtree-selected > .jqtree-element:hover {
            background: #ffd485!important;
        }
    </style>
@endsection
