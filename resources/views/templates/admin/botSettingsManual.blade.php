@extends("layout.admin.app")

@section('title', __('bot.bot_settings'))

@section('content')
    <script src="https://cdn.ckeditor.com/4.16.2/full/ckeditor.js"></script>

    <div class="box">
        <div class="box-header">
            <h2>{{ __('bot.bot_settings') }}</h2>
        </div>
        <div class="box-body">
            <a class="btn btn-primary" href="{{route('botSettings')}}">Назад</a>
            <div class="blog-single-content entry-content pull-up expanded">
                <p>1. Перейти в бота:&nbsp;<a href="https://t.me/BotFather" target="_blank" rel="noreferrer noopener">@BotFather</a></p>
                <p>2. Нажать кнопку:&nbsp;<strong>START</strong>&nbsp;(Если вы ранее уже создавали ботов — перейдите к 3 пункту)</p>
                <img width="75%" src="https://wiki.joinchat.us/wp-content/uploads/2019/08/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA-%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0-2019-08-18-%D0%B2-18.24.49.png">
                <p>3. Написать боту:&nbsp;<code>/newbot</code></p>
                <img width="75%" src="https://wiki.joinchat.us/wp-content/uploads/2019/08/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA-%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0-2019-08-18-%D0%B2-18.25.19.png">
                <p>4. Бот спросит вас как назвать нового бота. Придумайте и напишите .</p>
                <img width="75%" src="https://wiki.joinchat.us/wp-content/uploads/2019/08/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA-%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0-2019-08-18-%D0%B2-18.25.55.png">
                <p>5. Далее нужно ввести ник бота, что бы он заканчивался нa слово&nbsp;<code>bot</code></p>
                <img width="75%" src="https://wiki.joinchat.us/wp-content/uploads/2019/08/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA-%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0-2019-08-18-%D0%B2-18.27.06.png">
                <p>6. <strong>Бот создан!</strong> Скопируйте полученный API KEY и вставьте его в поле <b>API ключ</b>.</p>
                <a class="btn btn-primary" href="{{route('botSettings')}}">Я получил ключ!</a>
            </div>
        </div>
    </div>
@endsection
