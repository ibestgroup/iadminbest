@extends("layout.admin.app")

@section('title', 'Админ панель')

@section('content')
    @if (Auth::id() == 1)
        <div class="box">
            <!-- /.box-header -->

            <div class="col-lg-6">
                <h2>Баланс сайта: {{\App\Models\Payments::sum('amount') - \App\Models\Cashout::where('status', 2)->where('payment_system', '!=', 1919)->where('payment_system', '!=', 333)->sum('amount')}} <i class="fa fa-rub"></i></h2>
            </div>
            <div class="col-lg-6">
                <h2>Сумма всех заявок: {{\App\Models\Cashout::where('status', 0)->where('payment_system', '!=', 1919)->where('payment_system', '!=', 333)->sum('amount')}} <i class="fa fa-rub"></i></h2>
            </div>

            <div class="box-header">
                <h3 class="box-title">Заявки на вывод</h3>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th>ID</th>
                        <th>Сумма</th>
                        <th>Платежная система</th>
                        <th>Пользователь</th>
                        <th>Кошелек</th>
                        <th>Дата</th>
                        <th>Подтвердить</th>
                    </tr>
                    @foreach ($cashouts as $cashout)
                        <tr class="ta-center">
                            <td>{{ $cashout['id'] }}</td>
                            <td>{{ $cashout['amount'] }}</td>
                            <td>{{ $cashout['payment_system'] }}</td>
                            <td>{{ \App\Libraries\Helpers::convertUs('login', $cashout['id_user']) }}</td>
                            <td>{{ $cashout['to'] }}</td>
                            <td>{{ $cashout['date'] }}</td>
                            <td>
                                <form action="{{route('cashoutRequestConfirm')}}" method="post" class="js-store">
                                    @csrf
                                    <input type="hidden" name="id" value="{{$cashout->id}}">
                                    <button class="btn btn-primary btn-flat">Вывести</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer clearfix">
            </div>
        </div>
    @endif
@endsection
