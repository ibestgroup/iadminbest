@extends("layout.admin.app")

@section('title', __('main.admin_panel'))

@section('content')
    @if (Auth::id() == 1)
        <div class="box">
            <!-- /.box-header -->
            <div class="box-header">
                <h4>Все выводы</h4>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th>ID</th>
                        <th>{{ __('main.user') }}</th>
                        <th>{{ __('main.amount') }}</th>
                        <th>{{ __('main.date') }}</th>
                    </tr>

                    @foreach ($cashouts as $item)
                        <tr class="ta-center" onclick='window.open("/profile/admin/users/{{$item->id_user}}","_self")' style="cursor:pointer">
                            <td>{{ $item['id'] }}</td>
                            <td>{{ \App\Libraries\Helpers::convertUs('login', $item->id_user) }}</td>
                            <td>{{ $item->amount }}</td>
                            <td>{{ $item->date }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="custom-paginate clearfix">
                    {{$cashouts->appends(request()->input())->links()}}
                </div>
            </div>
        </div>
    @endif
@endsection
