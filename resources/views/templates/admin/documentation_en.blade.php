@extends("layout.admin.app")

@section('title', 'Админ панель')

@section('content')
    @php
        $path = '/iadminbest/storage/app/documentation/';
    @endphp
    <style>
        h3, h4 {
            border-bottom: 3px solid #ff8d00;
            padding: 5px 10px;
        }
        h4 {
            border-bottom: 2px solid #ff8d00;
        }
        h5 {
            font-size: 16px;
            border-bottom: 1px solid #ff8d00;
            padding: 5px 10px;
        }
        .documentation {
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Helvetica,Arial,sans-serif!important;
        }
        p {
            text-indent: 10px;
        }
    </style>
    @if (Auth::id() == 1)
        <div class="row documentation">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h2 class="ta-center">Documentation for working with  i<span class="theme-color">A</span>dmin.best MLM project creator</h2>
                    </div>
                <div class="box-body">
                    <h3>Lesson 1.1</h3>
                    <div style="width: 560px; margin: auto;">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/IA5wOga8ol4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <h3>Lesson 1.2</h3>
                    <div style="width: 560px; margin: auto;">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/Nau2YBu4jCI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <h3>Lesson 1.3</h3>
                    <div style="width: 560px; margin: auto;">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/m-elrdhPLSA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <h3>Lesson 1.4</h3>
                    <div style="width: 560px; margin: auto;">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/zmqV6hXMdno" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <h3>Lesson 1.5</h3>
                    <div style="width: 560px; margin: auto;">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/QpnbEXiwymI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <h3>Lesson 1.6</h3>
                    <div style="width: 560px; margin: auto;">
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/YkDlpYsHUZg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                    <h3>Start usage</h3>
                    <p>
                        The process of creating sites on the iAdmin.best builder is maximally simplified. However at the beginning it is important to know the main policy of the service, it's work principles and abilities.
                    </p>
                    <h4>Abilities</h4>
                    <p>First of all, let's understand the possibilities.</p>
                    <p>On this site builder you can create 3 types of finance project websites (we’ll talk about each separately):<br>
                        - <b>MLM Matrix</b> <br>
                        - <b>Hyip projects</b> (Investing)<br>
                        - <b>Game-typed finance projects with money withdrawal function </b> (farms/ items which generate profitable) <br>
                    </p>
                    <p>You are available these projects follow your own wishes. Also, you allowed to create different projects that are totally various.<br>
                        - <b>You are available to edit your account</b> - create new pages and fill it with content. <br>
                        - <b>Add widgets</b> - every site function is divided to widgets(Exmp. "News widget", "Latest refferals widget").
                        You can setup each widget with your wishes and add it to any page.<br>
                        - <b>It is possible for you to edit the main page</b> - that actually is a landing page.
                        (The standart landing page editor is installed and ready to use)<br>
                        - <b>It is possible to edit the authorization and registration pages</b> - through the same way you can customize the authorization pages on the site.<br>
                    </p>
                    <h4>Main FAQ</h4>
                    <p>That was a very short brief overview of the functionality, and in this documentation we will consider it in more details,
                        but now for sure you have some questions that should get answers:</p>
                        <blockquote style="border-left-color: #ff8d00;">
                            <p>Where will the funds be stored?</p>
                            <small>
                                For each site project, the system will allocate a separate account. Each of your sites has its own money balance.
                                This is done for several reasons:
                                1) According to hyip projectses structure the main balance of the site may be less than balances of participants.
                                2) For safety. For example, if an administrator was hacked on another site, attackers will not be able to withdraw funds.
                                Funds for your site are stored in a separate account.
                            </small>
                        </blockquote>
                        <blockquote style="border-left-color: #ff8d00;">
                            <p>How often can I withdraw money?</p>
                            <small>
                                Withdraw for administrators is available around the clock, 7 days a week.
                                It is also possible for HYIP administrators to establish a withdraw by orders, and you can decide whether to confirm the conclusion or not.
                            </small>
                        </blockquote>
                        <blockquote style="border-left-color: #ff8d00;">
                            <p>May the funds be delayed on tariffs?</p>
                            <small>
                                No. For more details, please check the documentation in the “MLM Matrix” section.
                                You can set the priorities in marketing types by yourself.
                                You can set a priority for withdrawal, and then the funds will be used for withdrawal first,
                                and only then the rest will be reserved for reinvest (only if it is enabled).
                                If there is no reinvest, then the funds will immediately be sent to the site balance.
                            </small>
                        </blockquote>

                    <h3>Design your web site</h3>
                    <p>There are a lot of subtle settings in the design of the site, that will be clearly explained in order below.</p>

                    <p>First of all, we have to click on the button <img src="{{$path}}1.png" alt=""> in the lower left corner and open our main panel.</p>
                    <img src="{{$path}}2.png" alt=""> <br>
                    <p>  Let's look at all the items in order.<br>
                        <span class="theme-color"><i class="fas fa-cogs"></i> <b>Admin panel</b></span> - clicking on this button will take you to the admin panel.<br>
                        <span class="theme-color"><i class="fa fa-border-none"></i> <b>Add area</b></span> - there are "Area" and "Elements", items are stored in areas.
                        For example: if you click on the "Add area" button and select "1/3" in the "Width" field, the width of the area will become "1/3" of the page.
                        Then you can create a second area, but already set the width to 2/3. You will have two areas that are nearby.
                        Then in the future you will be able to add elements to these areas.<br>
                        <span class="theme-color"><i class="fa fa-plus"></i> <b>Add item</b></span> - option you can add some widgets.There are a lot of widgets for any occasion. There will be added a lot of new widgets in accordance with the requirements of users.
                        For example: you want to add a news widget. Click "Add item", point the area this item should be located, select the "News" widget and click "Add item". Now you have a widget that you can customize.<br>
                        <span class="theme-color"><i class="fa fa-border-style"></i> <b>"On / Off" grid</b></span> - grid is a grid switch. The created areas are "invisible" and to detect them we can temporarily turn on the grid.<br>
                        <span class="theme-color"><i class="fas fa-hand-paper"></i> <b>Reorder</b></span> - by clicking on this button, you activate the drag and drop mode. You can either drag the area, with all the elements inside, or swap the elements inside the area.
                        In order to swap areas you need to pull the blue slider.
                        <img src="{{$path}}3.png" alt="">. <b>In order to swap widgets themselves, just pull the widget itself..</b>
                        Widgets can be moved from one area to another.
                        You can also drag a block to an area in the side menu.<br>
                        <span class="theme-color"><i class="fas fa-cubes"></i> <b>Setting items</b></span> - this button activates the editing mode for items.
                        In this case, when you hover over the elements, a similar menu will pop up at the top right. <img src="{{$path}}4.png" alt=""> By clicking on the green button, you will see a modal window with widget settings. Here you can change the color, text, and edit the settings.
                        <b>Attention! Menu items are also widgets that you can edit and move. The pop-up menu at the bottom right may edit the area.</b>
                        <br>
                        <span class="theme-color"><i class="fas fa-tools"></i> <b>Site editor</b></span> -  this section contains settings for the main site design. Here you can turn on and off such global areas as:
                        <b>Site header</b>, <b>Top menu</b>, <b>Left menu</b>, <b>Right menu</b>. The main area is always on by default and cannot be turned off. Turning an area on and off is very simple.
                        Just click on the appropriate switches in this block: <img src="{{$path}}5.png" alt=""> The rest of the settings accord the description.
                        <br>
                    </p>


                    <h3>How to change admin password</h3>
                    <p>To do this, we need the "Edit Profile" widget. It is already installed by default. To see the sample, you can click on the button in the upper menu on the right. <img src="{{$path}}6.png" alt="">. You will open the page with the widget we need. We activate the editing mode of the elements by clicking on the
                        <span class="theme-color"><i class="fas fa-cubes"></i> <b>Setting items</b></span>, button, after which we hover over the block and click on the green button of the pop-up menu.  <img src="{{$path}}8.png" alt=""> The widget editing menu opens. Next, go to the tab <b>"Widget setting"</b> -> <b>"Elements"</b> -> <b>"Configure" button</b> -> <b>"Add element" button</b>.
                        Enter the name "Password" and in the second field select <b>"Password"</b>. If desired, select the icon, and click save. Now in this widget a field has appeared in which you can create a new password.
                        <img src="{{$path}}7.png" alt="">
                    </p>

                    <h3>Adding pages.</h3>
                    <p>In the site builder you are available to add your own pages. Let's try. In the left menu in the admin panel, click
                        "<span class="fas fa-file-alt"></span>Site Pages" and select "Site pages". Here you can create, edit and delete site pages.
                        Now in the bottom block input Title" and "Link". The "Link" must be written using Latin letters only.
                        If you don't input any link, it will be created automatically. Click Add. Ok, now we have one more page. Let's add it to our menu.
                        To do this, go to the profile page, and activate the element editing mode by clicking on the <span class="theme-color"><i class="fas fa-cubes"></i> <b>Settings item</b></span>.
                        Then we hover over the menu, and click on the green button of the pop-up menu. Then we hover over the menu, and click on the green button of the pop-up menu.  Now, under the block, select <b>"Widget Settings"</b> ->
                        <b>"Elements"</b> -> <b>"Configure"</b> -> <b>"Add item"</b>. Input the name of the menu item, select our page and icon as desired, then click "Save". If pull the green sliders we can change the menu items locations. Great, we have created our new page. Now you can add any widgets to it.
                        <br>
                        Отлично, мы создали свою страничку. Теперь вы можете добавить на нее любой виджет.
                    </p>

                    <h3>Authorization Pages</h3>
                        In our site builder is available to edit <b>Authorization</b>, <b>Registration</b> and <b>Password recovery</b> pages.
                        To do this, in the admin panel in the menu on the left choose <b>Site Pages</b> -> <b>Authorization Pages</b>.
                        Then we get into the authorization page edit module. By switching the tabs you can choose the page you want to edit. The settings are located in the bottom, there you can choose the color, edit text and setup other page design options.
                    </p>

                    {{--<h3>Добавление и редактирование виджета</h3>--}}
                    {{--<p>В конструкторе есть возможность оформить страницы <b>Авторизации</b>, <b>Регистрации</b> и <b>Восстановления пароля</b>. Для этого в админ панели в меню слева откройте--}}
                        {{--нажмите <b>Страницы сайта</b> -> <b>Страницы авторизации</b>. Вы попадаете в модуль редактирования страниц авторизации. Переключаясь между вкладками, вы можете редактировать нужную вам страницу.--}}
                        {{--Внизу находятся настройки. В них вы можете менять цвет, текст и другие параметры оформления страницы.--}}
                    {{--</p>--}}






                    {{--<h5>МЛМ матрицы</h5>--}}
                    {{--<p>Вы можете создать матричный проект, в котором настроить разные маркетинги: <br>--}}
                        {{--- <b>Линейный маркетинг</b> - это маркетинг в котором уровни приобретаются по порядку.<br>--}}
                        {{--- <b>Мульти маркетинг</b> - это маркетинг в котором все уровни приобретаются сразу.<br>--}}
                        {{--Так же вы можете установить тип переливов на каждом маркетинге отдельно: <br>--}}
                        {{--- <b>Живая очередь</b> - когда независимо от того кто пригласил человека, структура будет строиться равномерно от верхушки структуры, то есть от администратора проекта. В данном случае это вы. <br>--}}
                        {{--- <b>Переливы под пригласителя</b> - в этом случае каждый участник строит свою структуру, и пользователи будут в структуре участника. <br>--}}
                    {{--</p>--}}

                    {{--</p>--}}
                    <h3>The documentation is being creating...</h3>
                    If you still have any questions, please ask the developer or leave it in support group.
                    <a href="https://vk.com/igroupbest" class="btn btn-primary">Группа Вконтакте</a> <a href="https://vk.com/id527512598" class="btn btn-primary">Admin VK</a>
                </div>
            </div>
        </div>

    @endif
@endsection
