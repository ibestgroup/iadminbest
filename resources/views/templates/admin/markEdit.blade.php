@extends("layout.admin.app")

@section('title', 'Редактировать маркетинги')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Редактировать маркетинги</h3>
                </div>

                <div class="box-body">
                @foreach ($marks as $key => $mark)
                    <form class="save-mark js-markEdit" data-table="mark" data-id="{{$mark['id']}}" action="{{route('markEdit')}}" method="post">
                        <input type="hidden" name="mark_id" value="{{$mark['id']}}">
                        <div class="tarif-settings js-tarif-block" data-id="{{$mark['id']}}">
                            <div class="col-lg-4 tarif-settings-wrap">
                                <div class="form-column">
                                    <label>Активность</label>
                                    <label class="switch" title="Включить тариф">
                                        <input type="checkbox" {{($mark['status'])?'checked':''}} name="status" class="switch-input">
                                        <span class="switch-label" data-on="On" data-off="Off"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </div>
                                <div class="form-group form-column">
                                    <label for="name">Название</label>
                                    <input type="text" name="name" class="form-control" id="name" value="{{$mark['name']}}" placeholder="Тариф №{{$mark['id']}}">
                                </div>
                                <div class="form-group form-column">
                                    <label for="width">Ширина</label>
                                    <select name="width" class="form-control" id="width">
                                        @for ($i = 2; $i <= 5; $i++)
                                        <option value="{{$i}}" {{($i == $mark['width'])?'selected':''}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group form-column">
                                    <label for="reinvest">Реинвест</label>
                                    <select name="reinvest" class="form-control" id="reinvest">
                                        @for ($i = 1; $i <= 9; $i++)
                                        <option value="{{$i}}" {{($i == $mark['reinvest'])?'selected':''}}>{{$markInfo[$i-1]['mark_name']}}</option>
                                        @endfor
                                        <option value="0" <?=(0 == $mark['reinvest'])?'selected':'';?>>{{ __('main.without_reinvest') }}</option>
                                    </select>
                                </div>
                                <div class="form-group form-column">
                                    <label for="level">Кол-во уровней</label>
                                    <select name="level" class="form-control" id="level">
                                        @for ($i = 1; $i <= 4; $i++)
                                        <option value="{{$i}}" {{($i == $mark['level'])?'selected':''}}>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="checkbox form-column switch-wrap">
                                    <label class="switch" title="Включить тариф">
                                        <input type="checkbox" {{($mark['reinvest_first'])?'checked':''}} name="reinvest_first" class="switch-input">
                                        <span class="switch-label" data-on="On" data-off="Off"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                    <span class="form-prompt" data-toggle="tooltip" data-placement="top" title="Включить реинвест в первый тариф">?</span>
                                </div>
                                <div class="checkbox form-column switch-wrap">
                                    <label class="switch" title="Включить тариф">
                                        <input type="checkbox" {{($mark['only_reinvest'])?'checked':''}} name="only_reinvest" class="switch-input">
                                        <span class="switch-label" data-on="On" data-off="Off"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                    <span class="form-prompt" data-toggle="tooltip" data-placement="top" title="Если эта опция включена, то пользователь не сможет сразу активировать этот тариф из личного кабинета. Ему придется идти по заданному Вами маркетингу с самого начала.">?</span>
                                </div>
                            </div>
                            <div class="col-lg-4 tarif-settings-wrap">
                                <div class="form-group form-column">
                                    <label for="to_par">Пригласившему</label>
                                    <input type="number" name="to_par" value="{{$mark['to_par']}}" class="form-control" id="to_par" placeholder="0">
                                </div>
                                <div class="form-group form-column">
                                    <label for="to_admin">Админу</label>
                                    <input type="number" name="to_admin" value="{{$mark['to_admin']}}" class="form-control" id="to_admin" placeholder="0">
                                </div>
                                @for ($i = 1; $i <= 4; $i++)
                                <div class="form-group form-column">
                                    <label for="level{{$i}}">Ур {{$i}}</label>
                                    <input type="number" name="level{{$i}}" value="{{$mark['level'.$i]}}" class="form-control" id="level{{$i}}" placeholder="0">
                                </div>
                                @endfor
                            </div>
                            <div class="col-lg-4">
                                <div class="tarif-result__title">Результаты</div>
                                <table class="table table-bordered table-hover tarif-result-table">
                                    <tr>
                                        <td colspan="2">
                                            <b>Доход</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <span class="js-all-sum">{{$markInfo[$key]['mark_profit']}}</span>
                                        </td>
                                    </tr>
                                    <tr style="<?=(!$mark['reinvest'])?'display:none':''?>">
                                        <td>
                                            Реинвест в <span class="js-tarif-name">{{$markInfo[$key]['reinvest_name']}}</span>
                                        </td>
                                        <td>
                                            <span class="js-reinvest">{{$markInfo[$key]['enter_reinvest']}}</span>
                                        </td>
                                    </tr>
                                    <tr style="<?=(!$mark['reinvest_first'])?'display:none':''?>">
                                        <td>
                                            Реинвест в 1 тариф
                                        </td>
                                        <td class="js-reinvest-first">
                                            <span class="js-reinvest-first">{{$markInfo[$key]['enter_first']}}</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            На вывод
                                        </td>
                                        <td>
                                            <span class="js-out-sum">{{$markInfo[$key]['mark_profit'] - $markInfo[$key]['enter_reinvest'] - $markInfo[$key]['enter_first']}}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Вход в тариф
                                        </td>
                                        <td>
                                            <span class="js-enter">{{$markInfo[$key]['mark_enter']}}</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                            <button class="btn btn-primary" data-click="{{$key+1}}">Сохранить</button>
                        </div>
                    </form>
                    <hr>
                    @endforeach

                </div>
                <script>
                    // console.log($('.js-adEdit').length);
                    // import AdEdit from '/resources/assets/js/forms/AdEdit.js';
                    // new AdEdit();
                </script>
            </div>
        </div>
    </div>
@endsection
