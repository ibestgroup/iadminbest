@extends("layout.admin.app")

@section('title', __('main.admin_edit_marks'))

@section('content')
    <div class="row">
        <style>
            .form-control[type=number] {
                padding: 2px 2px 2px 0;
                min-width: 60px;
            }
        </style>

        <div class="col-md-12">
            <div class="tarif-settings">
                <div class="box box-primary mark-element">
                    <div class="box-header with-border">
                        <h3 class="ta-center box-title">Создать новый тариф</h3>
                    </div>
                    <div class="box-body">
                        <form class="create-mark js-store" action="{{route('markCreate')}}" method="post">
                            @csrf
                            <div class="row">
                                <div class="form-group col-xs-4">
                                    <label for="">Название тарифа</label>
                                    <input type="text" placeholder="Название тарифа" name="name" class="form-control">
                                </div>
                                <div class="form-group col-xs-4">
                                    <label for="width">{!! __('main.admin_width') !!}</label>
                                    <select name="width" class="form-control">
                                        @for ($i = 2; $i <= 32; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="form-group col-xs-4">
                                    <label for="level">{!! __('main.admin_levels') !!}</label>
                                    <select name="level" class="form-control">
                                        @for ($i = 1; $i <= \App\Helpers\MarksHelper::globalCount(); $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <input type="submit" value="Создать" class="btn btn-primary">
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
                <style>
                    .box-shadow {
                        box-shadow: 0 5px 20px rgba(0, 0, 0, 0.1);
                    }
                    .box-shadow > h3, .mark-element .box-header > h2 {
                        padding: 10px;
                        transition: all .3s linear;
                        font-size: 16px;
                    }
                    .box-shadow > h3:hover, .mark-element .box-header > h2:hover {
                        color: #ff8d00;
                        cursor: pointer;
                    }

                    .box-shadow h3 > i,
                    h2.box-title > i
                    {
                        color: #ff8d00;
                    }
                    .box-shadow h3 {
                        border-left: 3px solid #ff8d18;
                        margin-bottom: 0;
                    }
                    .mark-togg {
                        border-left: 3px solid #ff8d18;
                    }

                    *[data-tog] {
                        transition: all .3s linear;
                    }
                </style>
                @foreach ($marks as $key => $mark)
                    <div class="box mark-element">
                        <div class="box-header with-border">
                            <h2 class="box-title" data-tog="#mark-tog-{{$mark->id}}" data-flag="0">
                                <i class="fa fa-plus"></i> #{{$mark->id}} {{$mark->name}}
                            </h2>
                            <div class="pull-right .col-lg-4">
                                <button form="delete-mark-{{$mark->id}}" class="btn btn-danger" style="display: inline-block; float: right;">{!! __('main.delete') !!}</button>
                                <button form="form-mark-{{$mark->id}}" class="btn btn-primary" style="display: inline-block; float: right; margin-right: 5px;">{!! __('main.admin_save') !!}</button>
                            </div>
                        </div>
                        <div class="box-body" id="mark-tog-{{$mark->id}}" style="display: none;">
                            <form class="save-mark js-markEditCustom" action="{{route('markEdit')}}" data-id="{{$mark['id']}}" method="post" id="form-mark-{{$mark->id}}">
                                {{--<div class="row">--}}
                                    {{--<h3 class="ta-center">{!! __('main.admin_tarif') !!}</h3>--}}
                                <input type="hidden" name="mark_number" value="{{$mark->id}}">
                                {{--<div class="nav-tabs-custom">--}}
                                    {{--<ul class="nav nav-tabs">--}}
                                        {{--<li class="active"><a href="#main-setting-tabs-{{$mark->id}}" data-toggle="tab" aria-expanded="true">Основные настройки</a></li>--}}
                                        {{--<li><a href="#price-levels-tabs-{{$mark->id}}" data-toggle="tab" aria-expanded="true">Цены на уровни</a></li>--}}
                                        {{--<li><a href="#result-tabs-{{$mark->id}}" data-toggle="tab" aria-expanded="true">Таблица с результатом</a></li>--}}
                                    {{--</ul>--}}
                                    {{--<div class="tab-content">--}}
                                <div class="box-shadow">
                                    <h3 data-tog="#main-setting-tabs-{{$mark->id}}" data-flag="0"><i class="fa fa-plus"></i> Основные настройки тарифа</h3>
                                    <div class="mark-togg" id="main-setting-tabs-{{$mark->id}}" style="display: none;">
                                        <input type="hidden" name="mark_id" value="{{$mark['id']}}">
                                        <div class="col-lg-2 ta-center">
                                            <label>{!! __('main.admin_status') !!}</label>
                                            <select name="status" class="form-control" id="status-{{$mark['id']}}">
                                                <option value="0" {{($mark['status'] == 0)?'selected':''}}>{!! __('main.admin_disable') !!}</option>
                                                <option value="1" {{($mark['status'] == 1)?'selected':''}}>{!! __('main.admin_enable_but') !!}</option>
                                                <option value="2" {{($mark['status'] == 2)?'selected':''}}>{!! __('main.admin_enable') !!}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-2 ta-center">
                                            <label for="name">{!! __('main.admin_mark_name') !!}</label>
                                            <input type="text" name="name" class="form-control" id="name"
                                                   value="{{$mark['name']}}" placeholder="{!! __('main.admin_tarif') !!} №{{$mark['id']}}">
                                        </div>
                                        <div class="col-lg-2 ta-center">
                                            <label>{!! __('main.admin_overflow') !!}</label>
                                            <select name="overflow" class="form-control" id="overflow-{{$mark['id']}}">
                                                <option value="0" {{($mark['overflow'] == 0)?'selected':''}}>{!! __('main.admin_disable') !!}</option>
                                                <option value="1" {{($mark['overflow'] == 1)?'selected':''}}>{!! __('main.admin_enable') !!}</option>
                                                <option value="2" {{($mark['overflow'] == 2)?'selected':''}}>{!! __('main.admin_under_admin') !!}</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-2 ta-center">
                                            <label>{!! __('main.admin_mark_type') !!}</label>
                                            <select name="type" class="form-control" id="type-{{$mark['id']}}">
                                                <option value="1" {{($mark['type'] == 1)?'selected':''}}>{!! __('main.admin_mark_type_line') !!}</option>
                                                <option value="2" {{($mark['type'] == 2)?'selected':''}}>{!! __('main.admin_mark_type_multi') !!}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-2 ta-center">
                                            <label for="width">{!! __('main.admin_width') !!}</label>
                                            <select name="width" class="form-control disable-overflow" id="width-{{$mark['id']}}" {{(!$mark->overflow)?'disabled="disabled"':''}}>
                                                @for ($i = 2; $i <= 32; $i++)
                                                    <option value="{{$i}}" {{($i == $mark['width'])?'selected':''}}>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-2 ta-center">
                                            <label for="level">{!! __('main.admin_levels') !!}</label>
                                            <select name="level" class="form-control" id="level-{{$mark['id']}}">
                                                @for ($i = 1; $i <= \App\Helpers\MarksHelper::globalCount(); $i++)
                                                    <option value="{{$i}}" {{($i == $mark['level'])?'selected':''}}>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-2 ta-center">
                                            <label for="required_mark">{!! __('main.required_mark') !!}</label>
                                            <select name="required_mark" id="required_mark" class="form-control">
                                                <option value="0">{!! __('main.no') !!}</option>
                                                <option value="1" {{ $mark['required_mark'] ? 'selected' : '' }}>{!! __('main.yes') !!}</option>
                                            </select>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="box-shadow">
                                    <h3 data-tog="#price-levels-tabs-{{$mark->id}}" data-flag="0"><i class="fa fa-plus"></i> Цены на уровни</h3>
                                    <div class="mark-togg" id="price-levels-tabs-{{$mark->id}}" style="display: none;">
                                        <div class="levels-info">
                                            {{--<h3 class="ta-center">{!! __('main.admin_prices') !!}</h3>--}}
                                            <div class="form-group ta-center va-middle">
                                                <div class="box-body table-responsive no-padding">
                                                    <table class="table table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <th>{!! __('main.admin_lev') !!}</th>
                                                            <th>{!! __('main.admin_price') !!}</th>
                                                            <th>{!! __('main.admin_to_parent') !!}<a class="my-tooltip" title="{!! __('main.admin_to_parent_desc') !!}"><span class="fa fa-question-circle"></span></a></th>
                                                            <th>{!! __('main.admin_to_admin') !!}<a class="my-tooltip" title="{!! __('main.admin_to_admin_desc') !!}"><span class="fa fa-question-circle"></span></a></th>
                                                            {{--<th>Итого</th>--}}
                                                            <th colspan="2">
                                                                {!! __('main.admin_reinvest') !!}
                                                            </th>
                                                            <th>{!! __('main.admin_reinvest_first') !!}<a class="my-tooltip" title="{!! __('main.number_first_reinvest') !!}"><span class="fa fa-question-circle"></span></a></th>
                                                            <th>{!! __('main.admin_priority') !!}<a class="my-tooltip" title='{!! __('main.admin_priority_desc') !!}'><span class="fa fa-question-circle"></span></a></th>
                                                        </tr>
                                                        @for ($i = 1; $i <= \App\Helpers\MarksHelper::globalCount(); $i++)
                                                            <tr class="for-result" {{($i > $mark['level'])?'style=display:none':''}}>
                                                                <td class="va-middle">{{ $i}}</td>
                                                                <td class="va-middle">
                                                                    <input type="number" name="level{{$i}}" {!! \App\Libraries\Helpers::stepForInput() !!} value="{{$mark['level'.$i]}}" class="form-control price-level" id="level-{{$key}}-{{$i}}" placeholder="0">
                                                                </td>
                                                                <td class="va-middle">
                                                                    <input type="number" name="to_par_{{$i}}" {!! \App\Libraries\Helpers::stepForInput() !!} value="{{$marksLevelInfo[$key]['to_par_'.$i]}}" class="form-control to-par" id="to_par-{{$key}}-{{$i}}" placeholder="0">
                                                                </td>
                                                                <td class="va-middle">
                                                                    <input type="number" name="to_admin_{{$i}}" {!! \App\Libraries\Helpers::stepForInput() !!} value="{{$marksLevelInfo[$key]['to_admin_'.$i]}}" class="form-control to-admin" id="to_admin-{{$key}}-{{$i}}" placeholder="0">
                                                                </td>
                                                                <td class="va-middle">
                                                                    <select name="reinvest_{{$i}}" id="reinvest-{{$key}}-{{$i}}" class="form-control reinvest disable-overflow" {{(!$mark->overflow)?'disabled="disabled"':''}}>
                                                                        <option value="0" {{(0 == $marksLevelInfo[$key]['reinvest_'.$i])?'selected':''}}>{{ __('main.without_reinvest') }}</option>
                                                                        @foreach ($marks as $markIn)
                                                                            <option value="{{$markIn->id}}" {{($markIn->id == $marksLevelInfo[$key]['reinvest_'.$i])?'selected':''}}>{{\App\Helpers\MarksHelper::markName($markIn->id)}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </td>
                                                                <td class="va-middle">
                                                                    <input type="number" name="reinvest_type_{{$i}}" value="{{$marksLevelInfo[$key]['reinvest_type_'.$i]}}" class="form-control disable-overflow" id="reinvest_type-{{$key}}-{{$i}}" placeholder="0" {{(!$mark->overflow)?'disabled="disabled"':''}}>
                                                                </td>
                                                                <td class="va-middle">
                                                                    <select name="reinvest_first_type_{{$i}}" id="reinvest_first_type-{{$key}}-{{$i}}" class="form-control reinvest disable-overflow" {{(!$mark->overflow)?'disabled="disabled"':''}}>
                                                                        <option value="0" {{(0 == $marksLevelInfo[$key]['reinvest_first_type_'.$i])?'selected':''}}>{{ __('main.without_reinvest') }}</option>
                                                                        @foreach ($marks as $markIn)
                                                                            <option value="{{$markIn->id}}" {{($markIn->id == $marksLevelInfo[$key]['reinvest_first_type_'.$i])?'selected':''}}>{{\App\Helpers\MarksHelper::markName($markIn->id)}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <input type="number" name="reinvest_first_{{$i}}" value="{{$marksLevelInfo[$key]['reinvest_first_'.$i]}}" class="form-control disable-overflow" id="reinvest_first-{{$key}}-{{$i}}" placeholder="0" {{(!$mark->overflow)?'disabled="disabled"':''}}>
                                                                </td>
                                                                <td class="va-middle">
                                                                    <select name="priority_{{$i}}" id="priority-{{$key}}-{{$i}}" class="form-control priority disable-overflow" {{(!$mark->overflow)?'disabled="disabled"':''}}>
                                                                        <option value="2134" {{($marksLevelInfo[$key]['priority_'.$i] == 2134)?'selected':''}}>{!! __('main.admin_priority_6') !!}</option>
                                                                        <option value="1234" {{($marksLevelInfo[$key]['priority_'.$i] == 1234)?'selected':''}}>{!! __('main.admin_priority_1') !!}</option>
                                                                        <option value="1342" {{($marksLevelInfo[$key]['priority_'.$i] == 3421)?'selected':''}}>{!! __('main.admin_priority_2') !!}</option>
                                                                        <option value="1432" {{($marksLevelInfo[$key]['priority_'.$i] == 3421)?'selected':''}}>{!! __('main.admin_priority_3') !!}</option>
                                                                        <option value="4321" {{($marksLevelInfo[$key]['priority_'.$i] == 4321)?'selected':''}}>{!! __('main.admin_priority_4') !!}</option>
                                                                        <option value="3421" {{($marksLevelInfo[$key]['priority_'.$i] == 3421)?'selected':''}}>{!! __('main.admin_priority_5') !!}</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                        @endfor
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="col-lg-12 col-md-12">--}}

                                        {{--<h3 class="ta-center">Цены на уровни</h3>--}}
                                        <div class="box-shadow" style="margin: 10px;">
                                            <h3 data-tog="#inviters-tabs-{{$mark->id}}" data-flag="0"><i class="fa fa-plus"></i> {{__('main.to_inviters')}}</h3>
                                            <div class="mark-togg" id="inviters-tabs-{{$mark->id}}" style="display: none;">
                                                <div class="form-group ta-center va-middle">
                                                    <div class="box-body table-responsive no-padding">
                                                        {{--<h3 class="ta-center">{{__('main.to_inviters')}}</h3>--}}
                                                        <table class="table table-striped">
                                                            <tbody>
                                                            <tr>
                                                                @for ($i = 1; $i <= \App\Helpers\MarksHelper::globalCount(); $i++)
                                                                    <th>{{$i}} {{__('main.admin_mark_level')}}</th>
                                                                @endfor
                                                            </tr>
                                                            <tr>
                                                                @for ($i = 1; $i <= \App\Helpers\MarksHelper::globalCount(); $i++)
                                                                    <td>
                                                                        <input type="number" class="form-control" id="to_inviters-{{$key}}-{{$i}}" {!! \App\Libraries\Helpers::stepForInput() !!} value="{{$marksLevelInfo[$key]['to_inviter_'.$i]}}" name="to_inviter_{{$i}}">
                                                                    </td>
                                                                @endfor
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="box-shadow">

                                    <h3 data-tog="#result-tabs-{{$mark->id}}" data-flag="0"><i class="fa fa-plus"></i> Таблица результата</h3>
                                    <div class="mark-togg" id="result-tabs-{{$mark->id}}" style="display: none;">
                                        <div class="col-lg-12 col-md-12">
                                            <h4 class="ta-center">{!! __('main.admin_mark_enter') !!}: <span class="mark-enter-{{$mark['id']}}">0</span>{!! \App\Libraries\Helpers::siteCurrency('icon') !!}</h4>
                                            <h4 class="ta-center">{!! __('main.admin_mark_profit') !!}</h4>
                                            {{--<div class="result-profit-{{$mark['id']}}">--}}
                                            {{--<div class="col-lg-1">Ур</div>--}}
                                            {{--<div class="col-lg-1">Чел</div>--}}
                                            {{--<div class="col-lg-2">Доход</div>--}}
                                            {{--<div class="col-lg-4">Реивест</div>--}}
                                            {{--<div class="col-lg-2">Итог</div>--}}
                                            {{--вывод--}}
                                            {{--повышение уровня--}}
                                            {{--реинвест в первый тариф--}}
                                            {{--реинвест--}}

                                            {{--@for ($i = 1; $i < $mark['level']; $i++)--}}

                                            {{--{{($i > $mark['level'])?'style=display:none':''}}--}}
                                            {{--@endfor--}}
                                            {{--</div>--}}
                                                <div class="box-body no-padding">
                                                    <table class="table table-hover">
                                                        <tbody class="result-profit-{{$mark['id']}}">
                                                        <tr>
                                                            <th>{!! __('main.admin_mark_level') !!}</th>
                                                            <th>{!! __('main.admin_mark_people') !!}</th>
                                                            <th>{!! __('main.admin_mark_profit') !!}</th>
                                                            <th>{!! __('main.admin_mark_reinvest') !!}</th>
                                                            <th>{!! __('main.admin_mark_reinvest_first') !!}</th>
                                                            <th>{!! __('main.admin_total') !!}</th>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="ta-center">
                                    {{--<button class="btn btn-primary mark-save-btn" style="display: inline-block;">{!! __('main.admin_save') !!}</button>--}}
                                    {{--<button form="delete-mark-{{$mark->id}}" class="btn btn-danger mark-save-btn" style="display: inline-block;">{!! __('main.delete') !!}</button>--}}
                                </div>
                            </form>
                        </div>

                        <form class="delete-mark js-store" action="{{route('markDelete')}}" method="post" style="display: inline;" id="delete-mark-{{$mark->id}}">
                            @csrf
                            <input type="hidden" placeholder="" name="mark_id" value="{{$mark->id}}">
                        </form>
                    </div>
                    <div class="clearfix"></div>
                @endforeach
            </div>
        </div>
    </div>
@endsection