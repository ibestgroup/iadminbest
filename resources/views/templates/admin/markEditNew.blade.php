@extends("layout.admin.app")

@section('title', __('main.admin_edit_marks'))

@section('content')
    <div class="row">
        <style>
            .form-control[type=number] {
                padding: 2px 2px 2px 0;
                min-width: 60px;
            }
        </style>
        {{--@php--}}
            {{--$orders = \App\Models\OrdersMarkInside::where('mark', 8)->where('level', '>', 2)->get();--}}

            {{--foreach ($orders as $order) {--}}
                {{--$diff = $order->balance - $order->cashout;--}}
                {{--if ($diff) {--}}
                    {{--echo $order->id.'---'.$order->balance.'---'.$order->cashout.'<br>';--}}
                {{--}--}}
            {{--}--}}

                    {{--echo '<br>';--}}
            {{--foreach ($orders as $order) {--}}
                {{--if ($order->balance == 648) {--}}
                    {{--echo $order->id.'---'.$order->balance.'---'.$order->cashout.'<br>';--}}
                {{--}--}}
            {{--}--}}
        {{--exit;--}}
        {{--@endphp--}}
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="tarif-settings">
                        @foreach ($marks as $key => $mark)
                                <div class="mark-element">
                                <form class="save-mark js-markEditCustom" action="{{route('markEdit')}}" data-id="{{$mark['id']}}" method="post">
                                    <div class="col-lg-2 col-md-2">
                                        <h3 class="ta-center">{!! __('main.admin_tarif') !!}</h3>
                                        <input type="hidden" name="mark_id" value="{{$mark['id']}}">
                                        <div class="col-lg-12 ta-center">
                                            {{--<label>Активность</label>--}}
                                            {{--<label class="switch" title="Включить тариф">--}}
                                                {{--<input type="checkbox"--}}
                                                       {{--{{($mark['status'])?'checked':''}} name="status"--}}
                                                       {{--class="switch-input">--}}
                                                {{--<span class="switch-label" data-on="Включен" data-off="Выключен"></span>--}}
                                                {{--<span class="switch-handle"></span>--}}
                                            {{--</label>--}}

                                            <label>{!! __('main.admin_status') !!}</label>
                                            <select name="status" class="form-control" id="status-{{$mark['id']}}">
                                                <option value="0" {{($mark['status'] == 0)?'selected':''}}>{!! __('main.admin_disable') !!}</option>
                                                <option value="1" {{($mark['status'] == 1)?'selected':''}}>{!! __('main.admin_enable_but') !!}</option>
                                                <option value="2" {{($mark['status'] == 2)?'selected':''}}>{!! __('main.admin_enable') !!}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-12 ta-center">
                                            <label for="name">{!! __('main.admin_mark_name') !!}</label>
                                            <input type="text" name="name" class="form-control" id="name"
                                                   value="{{$mark['name']}}" placeholder="{!! __('main.admin_tarif') !!} №{{$mark['id']}}">
                                        </div>
                                        <div class="col-lg-12 ta-center">
                                            <label>{!! __('main.admin_overflow') !!}</label>
                                            <select name="overflow" class="form-control" id="overflow-{{$mark['id']}}">
                                                    <option value="0" {{($mark['overflow'] == 0)?'selected':''}}>{!! __('main.admin_disable') !!}</option>
                                                    <option value="1" {{($mark['overflow'] == 1)?'selected':''}}>{!! __('main.admin_enable') !!}</option>
                                                    <option value="2" {{($mark['overflow'] == 2)?'selected':''}}>{!! __('main.admin_under_admin') !!}</option>
                                            </select>
                                        </div>
                                        <div class="col-lg-12 ta-center">
                                            <label>{!! __('main.admin_mark_type') !!}</label>
                                            <select name="type" class="form-control" id="type-{{$mark['id']}}">
                                                    <option value="1" {{($mark['type'] == 1)?'selected':''}}>{!! __('main.admin_mark_type_line') !!}</option>
                                                    <option value="2" {{($mark['type'] == 2)?'selected':''}}>{!! __('main.admin_mark_type_multi') !!}</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-12 ta-center">
                                            <label for="width">{!! __('main.admin_width') !!}</label>
                                            <select name="width" class="form-control" id="width-{{$mark['id']}}">
                                                @for ($i = 1; $i <= 32; $i++)
                                                <option value="{{$i}}" {{($i == $mark['width'])?'selected':''}}>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-12 ta-center">
                                            <label for="level">{!! __('main.admin_levels') !!}</label>
                                            <select name="level" class="form-control" id="level-{{$mark['id']}}">
                                                @for ($i = 1; $i <= 8; $i++)
                                                <option value="{{$i}}" {{($i == $mark['level'])?'selected':''}}>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-10 col-md-10 levels-info">
                                        <h3 class="ta-center">{!! __('main.admin_prices') !!}</h3>
                                        <div class="col-lg-12">
                                            <div class="form-group col-lg-12 ta-center va-middle">
                                                <div class="box-body table-responsive no-padding">
                                                    <table class="table table-hover">
                                                        <tbody>
                                                            <tr>
                                                                <th>{!! __('main.admin_lev') !!}</th>
                                                                <th>{!! __('main.admin_price') !!}</th>
                                                                <th>{!! __('main.admin_to_parent') !!}<a class="my-tooltip" title="{!! __('main.admin_to_parent_desc') !!}"><span class="fa fa-question-circle"></span></a></th>
                                                                <th>{!! __('main.admin_to_admin') !!}<a class="my-tooltip" title="{!! __('main.admin_to_admin_desc') !!}"><span class="fa fa-question-circle"></span></a></th>
                                                                {{--<th>Итого</th>--}}
                                                                <th colspan="2">
                                                                    {!! __('main.admin_reinvest') !!}
                                                                </th>
                                                                <th>{!! __('main.admin_reinvest_first') !!}<a class="my-tooltip" title="{!! __('main.number_first_reinvest') !!}"><span class="fa fa-question-circle"></span></a></th>
                                                                <th>{!! __('main.admin_priority') !!}<a class="my-tooltip" title='{!! __('main.admin_priority_desc') !!}'><span class="fa fa-question-circle"></span></a></th>
                                                            </tr>
                                                            @for ($i = 1; $i <= 8; $i++)
                                                            <tr class="for-result" {{($i > $mark['level'])?'style=display:none':''}}>
                                                                <td class="va-middle">{{ $i}}</td>
                                                                <td class="va-middle">
                                                                    <input type="number" name="level{{$i}}" {!! \App\Libraries\Helpers::stepForInput() !!} value="{{$mark['level'.$i]}}" class="form-control price-level" id="level-{{$key}}-{{$i}}" placeholder="0">
                                                                </td>
                                                                <td class="va-middle">
                                                                    <input type="number" name="to_par_{{$i}}" {!! \App\Libraries\Helpers::stepForInput() !!} value="{{$marksLevelInfo[$key]['to_par_'.$i]}}" class="form-control to-par" id="to_par-{{$key}}-{{$i}}" placeholder="0">
                                                                </td>
                                                                <td class="va-middle">
                                                                    <input type="number" name="to_admin_{{$i}}" {!! \App\Libraries\Helpers::stepForInput() !!} value="{{$marksLevelInfo[$key]['to_admin_'.$i]}}" class="form-control to-admin" id="to_admin-{{$key}}-{{$i}}" placeholder="0">
                                                                </td>
                                                                <td class="va-middle">
                                                                        <select name="reinvest_{{$i}}" id="reinvest-{{$key}}-{{$i}}" class="form-control reinvest">
                                                                            @for ($l = 1; $l <= 9; $l++)
                                                                                <option value="{{$l}}" {{($l == $marksLevelInfo[$key]['reinvest_'.$i])?'selected':''}}>{{\App\Helpers\MarksHelper::markName($l)}}</option>
                                                                            @endfor
                                                                            <option value="0" {{(0 == $marksLevelInfo[$key]['reinvest_'.$i])?'selected':''}}>{{ __('main.without_reinvest') }}</option>
                                                                        </select>
                                                                </td>
                                                                <td class="va-middle">
                                                                    <input type="number" name="reinvest_type_{{$i}}" value="{{$marksLevelInfo[$key]['reinvest_type_'.$i]}}" class="form-control to-admin" id="reinvest_type-{{$key}}-{{$i}}" placeholder="0">
                                                                </td>
                                                                <td class="va-middle">
                                                                    <input type="number" name="reinvest_first_{{$i}}" value="{{$marksLevelInfo[$key]['reinvest_first_'.$i]}}" class="form-control to-admin" id="reinvest_first-{{$key}}-{{$i}}" placeholder="0">
                                                                </td>
                                                                <td class="va-middle">
                                                                    <select name="priority_{{$i}}" id="priority-{{$key}}-{{$i}}" class="form-control priority">
                                                                            <option value="1234" {{($marksLevelInfo[$key]['priority_'.$i] == 1234)?'selected':''}}>{!! __('main.admin_priority_1') !!}</option>
                                                                            <option value="1342" {{($marksLevelInfo[$key]['priority_'.$i] == 3421)?'selected':''}}>{!! __('main.admin_priority_2') !!}</option>
                                                                            <option value="1432" {{($marksLevelInfo[$key]['priority_'.$i] == 3421)?'selected':''}}>{!! __('main.admin_priority_3') !!}</option>
                                                                            <option value="4321" {{($marksLevelInfo[$key]['priority_'.$i] == 4321)?'selected':''}}>{!! __('main.admin_priority_4') !!}</option>
                                                                            <option value="3421" {{($marksLevelInfo[$key]['priority_'.$i] == 3421)?'selected':''}}>{!! __('main.admin_priority_5') !!}</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            @endfor
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        {{--<div class="col-lg-12 col-md-12">--}}

                                        <div class="col-lg-12 col-md-12">
                                            <h3 class="ta-center">{{__('main.to_inviters')}}</h3>
                                                <div class="form-group col-lg-12 ta-center va-middle">
                                                    <div class="box-body table-responsive no-padding">
                                                    <div class="box-body no-padding">
                                                        <table class="table table-hover">
                                                        <tbody>
                                                            <tr>
                                                                @for ($i = 1; $i <= 8; $i++)
                                                                    <th>{{$i}} {{__('main.admin_mark_level')}}</th>
                                                                @endfor
                                                            </tr>
                                                            <tr>
                                                                @for ($i = 1; $i <= 8; $i++)
                                                                    <td>
                                                                        <input type="number" id="to_inviters-{{$key}}-{{$i}}" {!! \App\Libraries\Helpers::stepForInput() !!} value="{{$marksLevelInfo[$key]['to_inviter_'.$i]}}" name="to_inviter_{{$i}}">
                                                                    </td>
                                                                @endfor
                                                            </tr>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-md-12">
                                            <h3 class="ta-center">{!! __('main.admin_mark_enter') !!}: <span class="mark-enter-{{$mark['id']}}">0</span>{!! \App\Libraries\Helpers::siteCurrency('icon') !!}</h3>
                                            <h3 class="ta-center">{!! __('main.admin_mark_profit') !!}</h3>
                                            {{--<div class="result-profit-{{$mark['id']}}">--}}
                                                {{--<div class="col-lg-1">Ур</div>--}}
                                                {{--<div class="col-lg-1">Чел</div>--}}
                                                {{--<div class="col-lg-2">Доход</div>--}}
                                                {{--<div class="col-lg-4">Реивест</div>--}}
                                                {{--<div class="col-lg-2">Итог</div>--}}
                                                {{--вывод--}}
                                                {{--повышение уровня--}}
                                                {{--реинвест в первый тариф--}}
                                                {{--реинвест--}}

                                                {{--@for ($i = 1; $i < $mark['level']; $i++)--}}

                                                    {{--{{($i > $mark['level'])?'style=display:none':''}}--}}
                                                {{--@endfor--}}
                                            {{--</div>--}}
                                            <div class="col-lg-12">
                                                <div class="box-body no-padding">
                                                    <table class="table table-hover">
                                                        <tbody class="result-profit-{{$mark['id']}}">
                                                            <tr>
                                                                <th>{!! __('main.admin_mark_level') !!}</th>
                                                                <th>{!! __('main.admin_mark_people') !!}</th>
                                                                <th>{!! __('main.admin_mark_profit') !!}</th>
                                                                <th>{!! __('main.admin_mark_reinvest') !!}</th>
                                                                <th>{!! __('main.admin_mark_reinvest_first') !!}</th>
                                                                <th>{!! __('main.admin_total') !!}</th>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="ta-center">
                                        <button class="btn btn-primary mark-save-btn" style="display: inline-block;">{!! __('main.admin_save') !!}</button>
                                        <button form="delete-mark-{{$mark->id}}" class="btn btn-danger mark-save-btn" style="display: inline-block;">{!! __('main.delete') !!}</button>
                                    </div>
                                </form>

                                <form class="delete-mark js-store" action="{{route('markDelete')}}" method="post" style="display: inline;" id="delete-mark-{{$mark->id}}">
                                    @csrf
                                    <input type="hidden" placeholder="" name="mark_id" value="{{$mark->id}}">
                                </form>
                                </div>
                                <div class="clearfix"></div>
                        @endforeach

                        <form class="create-mark js-store" action="{{route('markCreate')}}" method="post">
                            @csrf
                            <input type="text" placeholder="Название тарифа" name="name">
                            <input type="submit" value="Создать" class="btn btn-primary">
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection