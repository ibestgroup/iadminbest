@extends("layout.admin.app")

@section('title', 'Стиль оповещений')

@section('content')



    <div class="row">
        <div class="col-md-12">
            <div class="box js-parking-refresh">
                <div class="box-body">
                    <form action="{{route('storeNotify')}}" class="js-store">
                        @csrf
                        <div class="form-group">
                            <label for="">Стиль</label>
                            <select name="theme" id="" class="form-control theme-notify">
                                <option value="mint" {{($notify->theme == 'mint')?'selected="selected"':''}}>mint</option>
                                <option value="sunset" {{($notify->theme == 'sunset')?'selected="selected"':''}}>sunset</option>
                                <option value="relax" {{($notify->theme == 'relax')?'selected="selected"':''}}>relax</option>
                                <option value="nest" {{($notify->theme == 'nest')?'selected="selected"':''}}>nest</option>
                                <option value="metroui" {{($notify->theme == 'metroui')?'selected="selected"':''}}>metroui</option>
                                <option value="semanticui" {{($notify->theme == 'semanticui')?'selected="selected"':''}}>semanticui</option>
                                <option value="light" {{($notify->theme == 'light')?'selected="selected"':''}}>light</option>
                                <option value="bootstrap-v3" {{($notify->theme == 'bootstrap-v3')?'selected="selected"':''}}>bootstrap-v3</option>
                                <option value="bootstrap-v4" {{($notify->theme == 'bootstrap-v4')?'selected="selected"':''}}>bootstrap-v4</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Позиция</label>
                            <select name="position" id="" class="form-control position-notify">
                                <option value="top" {{($notify->position == 'top')?'selected="selected"':''}}>Верх</option>
                                <option value="topLeft" {{($notify->position == 'topLeft')?'selected="selected"':''}}>Верх слева</option>
                                <option value="topCenter" {{($notify->position == 'topCenter')?'selected="selected"':''}}>Верх центр</option>
                                <option value="topRight" {{($notify->position == 'topRight')?'selected="selected"':''}}>Верх справа</option>
                                <option value="center" {{($notify->position == 'center')?'selected="selected"':''}}>Центр</option>
                                <option value="centerLeft" {{($notify->position == 'centerLeft')?'selected="selected"':''}}>Центр слева</option>
                                <option value="centerRight" {{($notify->position == 'centerRight')?'selected="selected"':''}}>Центр справа</option>
                                <option value="bottom" {{($notify->position == 'bottom')?'selected="selected"':''}}>Низ</option>
                                <option value="bottomLeft" {{($notify->position == 'bottomLeft')?'selected="selected"':''}}>Низ слева</option>
                                <option value="bottomCenter" {{($notify->position == 'bottomCenter')?'selected="selected"':''}}>Низ центр</option>
                                <option value="bottomRight" {{($notify->position == 'bottomRight')?'selected="selected"':''}}>Низ справа</option>
                            </select>
                        </div>
                        <span class="btn btn-success notify-test" data-type="success">Попробовать</span>
                        <span class="btn btn-warning notify-test" data-type="warning">Попробовать</span>
                        <span class="btn btn-danger notify-test" data-type="error">Попробовать</span>
                        <span class="btn btn-info notify-test" data-type="info">Попробовать</span>
                        <div class="clearfix"></div>
                        <button class="btn btn-primary mt-10">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
