@extends("layout.admin.app")

@section('title', 'Админ панель')

@section('content')
    @if (Auth::id() == 1)
    <div class="box" style="overflow: visible;">
        <div class="box-header">
            <h3 class="box-title">{{ __('main.admin_your_pages') }}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body no-padding">
            <table class="table table-hover">
                <tbody>
                <tr>
                    <th>#</th>
                    {{--<th>ID</th>--}}
                    <th>{{ __('main.admin_name') }}</th>
                    <th>{{ __('main.admin_page_link') }}</th>
                    <th>{{ __('main.admin_status') }}</th>
{{--                    <th>{{ __('main.access') }}</th>--}}
                    <th>{{ __('main.admin_settings') }}</th>
                </tr>
                @forelse($pages as $key => $page)
                    <tr>
                        <td class="ta-center va-middle">{{$key+1}}</td>
{{--                        <td class="ta-center va-middle">{{$page->id}}</td>--}}
                        <td class="va-middle">{{$page->name_ru}}</td>
                        <td class="va-middle">/{{($page->name == 'profile')?'':'profile/'}}{{$page->name}}</td>
                        <td class="ta-center va-middle">{!! ($page->status)?'<span class="label label-success">'.__('main.admin_enable_w') .'</span>':'<span class="label label-warning">'.__('main.admin_disable_w') .'</span>' !!}</td>
                        {{--<td class="ta-center va-middle">--}}
                            {{----}}
                        {{--</td>--}}
                        <td class="ta-center va-middle">
                            <div class="col-lg-6">
                                Доступ <br>
                                <form action="{{route('pagesStore')}}" class="js-store" method="post" style="display: inline-block;" data-load='.table'>
                                    @csrf
                                    <input type="hidden" name="id" value="{{$page->id}}">
                                    <input type="hidden" name="type_post" value="access">
                                    <select name="access" class="selectpicker select-access">
                                        <option value="1" {{($page->access == 1)?'selected':''}}>Всем</option>
                                        <option value="2" {{($page->access == 2)?'selected':''}}>Если рефералов больше</option>
                                        <option value="3" {{($page->access == 3)?'selected':''}}>Если переходов по ссылке больше</option>
                                        <option value="4" {{($page->access == 4)?'selected':''}}>Если активирован любой тариф</option>
                                        <option value="5" {{($page->access == 5)?'selected':''}}>Если активирован конкретный тариф</option>
                                    </select>
                                    <input type="number" name="condition" class="form-control condition-access" value="{{$page->condition}}" style="{{($page->access <= 1 or $page->access == 4)?'display:none;':''}}">
                                    <button class="btn btn-primary">{{ __('main.admin_accept') }}</button>
                                </form>
                            </div>
                            <div class="col-lg-6">
                                Статус <br>
                                <form action="{{route('pagesStore')}}" class="js-store" method="post" style="display: inline-block;" data-load='.table'>
                                    @csrf
                                    <input type="hidden" name="id" value="{{$page->id}}">
                                    <select name="edit" class="selectpicker">
                                        <option value="enable">{{ __('main.admin_enable_w') }}</option>
                                        <option value="disable">{{ __('main.admin_disable_w') }}</option>
                                    </select>
                                    <button class="btn btn-primary">{{ __('main.admin_accept') }}</button>
                                </form>
                            </div>

                            <p class="btn btn-danger" data-toggle="modal" data-target="#delete-page-{{$page->id}}">{{ __('main.admin_delete_page') }}</p>

                            <div class="modal fade" id="delete-page-{{$page->id}}">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title" style="text-align: center; color: #FF8D00;">{{ __('main.admin_delete_page') }}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                {{ __('main.admin_delete_page_modal') }}
                                                <form action="{{route('pagesStore')}}" method="post" class="js-store">
                                                    @csrf
                                                    <input type="hidden" name="edit" value="delete">
                                                    <input type="hidden" name="id" value="{{$page->id}}">
                                                    <button class="btn btn-danger">{{ __('main.admin_delete_page') }}</button>
                                                </form>
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">{{ __('main.admin_close') }}</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>У вас нет созданных страниц</td>
                    </tr>
                @endforelse

                </tbody>
            </table>
        </div>




        <!-- /.box-body -->
    </div>
    <!-- /.box -->

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">{{ __('main.admin_add_page') }}</h3>
        </div>

        <form action="{{route('pagesAdd')}}" class="js-store" method="post" data-load='.content'>
            @csrf
            <div class="box-body">
                <div class="col-lg-12">
                    <p>{{ __('main.admin_empty_link') }}</p>
                </div>
                <div class="col-lg-4">
                    <input type="text" class="form-control" name="name_ru" placeholder="{{ __('main.admin_page_name') }}">
                </div>
                <div class="col-lg-4">
                    <input type="text" class="form-control" name="name" placeholder="{{ __('main.admin_page_link') }}">
                </div>
                <div class="col-lg-4">
                    <select name="status" class="selectpicker">
                        <option value="1">{{ __('main.admin_enable_w') }}</option>
                        <option value="0">{{ __('main.admin_disable_w') }}</option>
                    </select>
                </div>
            </div>
            <div class="box-footer ta-center">
                <button class="btn btn-primary">{{ __('main.admin_add') }}</button>
            </div>
        </form>

    </div>
    @endif
@endsection
