@extends("layout.admin.app")

@section('title', __('main.parking'))

@section('content')



    <div class="row">
        <div class="col-md-6">
            <div class="box js-parking-refresh">
                <div class="box-body">

                    @if($parking == null)

                        <form action="{{route('storeParking')}}" class="js-parking" method="post">
                            @csrf
                            <div class="input-group">
                                <span class="input-group-addon">http://</span>
                                <input type="text" name="domain" class="form-control" placeholder="{{ __('main.enter_domain') }}">
                            </div>
                            <br>
                            <button class="btn btn-primary">{{ __('main.parking') }}</button>
                        </form>


                    @elseif ($parking['delegate'] == 0)

                        {{$parking['domain']}}
                        <br>
                        {{ __('main.if_working') }}

                        <form action="{{route('delegateParking')}}" class="js-parking" method="post">
                            @csrf
                            <button class="btn btn-success">{{ __('main.delegate') }}</button>
                        </form>

                        <form action="{{route('deleteParking')}}" class="js-parking" method="post">
                            @csrf
                            <button class="btn btn-danger">{{ __('main.delete') }}</button>
                        </form>

                    @else
                        {{$parking['domain']}}

                        <form action="{{route('deleteParking')}}" class="js-parking" method="post">
                            @csrf
                            <button class="btn btn-danger">{{ __('main.delete') }}</button>
                        </form>
                    @endif
                </div>
                <h2>Как припарковать свой домен?</h2>
                <p>Если у Вас есть свое купленное и свободное доменное имя, то Вы можете его припарковать к своему существующему сайту.</p>
                <br>
                <p>Для этого вам нужно добавть A-записи у своего домен провайдера на IP нашего сервиса. IP нашего сервиса: <b>185.43.220.116</b></p>
                <p>После проделанной процедуры добавьте домен в поле выше и ожидайте когда Ваш регистратор создаст
                    DNS зоны(от Вас ничего не требуется, регистраторы доменов это делают автоматически). После того как
                    Ваш сайт доступен нужно написать в группу ВК чтобы мы добавили домен в базу нашего сервера.
                    Отметим что сайт будет доступен так же и по старой ссылке.</p>
            </div>
        </div>
    </div>
@endsection
