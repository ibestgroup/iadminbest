@extends("layout.admin.app")

@section('title', __('main.admin_panel'))

@section('content')
    @if (Auth::id() == 1)
        <div class="box">
            <!-- /.box-header -->
            <div class="box-header">
                <h4>{{ __('main.all_payments') }}</h4>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th>ID</th>
                        <th>{{ __('main.user') }}</th>
                        <th>{{ __('main.amount') }}</th>
                        <th>{{ __('main.date') }}</th>
                    </tr>

                    @foreach ($payments as $payment)
                        <tr class="ta-center" onclick='window.open("/profile/admin/users/{{$payment->user_id}}","_self")' style="cursor:pointer">
                            <td>{{ $payment['id'] }}</td>
                            <td>{{ \App\Libraries\Helpers::convertUs('login', $payment['user_id']) }}</td>
                            <td>{{ $payment['amount'] }}</td>
                            <td>{{ $payment['date'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="custom-paginate clearfix">
                    {{$payments->appends(request()->input())->links()}}
                </div>
            </div>
        </div>
    @endif
@endsection
