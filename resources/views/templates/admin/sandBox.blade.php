@extends("layout.admin.app")

@section('title', __('main.admin_panel'))

@section('content')
    @if (Auth::id() == 1)


        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Песочница</h3>
                    </div>
                    <div class="box-body">
                        <p>
                            Добро пожаловать в режим песочницы. <br>
                            В мире разработчиков так обычно называют тестовый режим. В тестовом режиме вы можете протестировать абсолютно все. <br>
                            Вы можете в один клик создать тысячу пользователей, начислить им баланс и активировать тарифы от их лица.
                            Проверить движение денежных средств и как будет все работать. Так же вы можете авторизоваться от лица тестовых пользователей,
                            для этого вам нужно будет ввести пароль 123123. <br>
                            Когда закончите тестировать, просто отключите этот режим, и все балансы обнулятся,
                            тестовые пользователи и все транзакции будут удалены. Останется только оформление сайта и созданные вами тарифы. <br>
                            Обратите внимание, что в тестовом режиме нельзя регистрироваться вручную, нельзя пополнить баланс.
                            Так же, если у вас уже есть зарегистрированные пользователи или пополнения баланса, тестовый режим окажется для вас недоступным.
                        </p>
                        @if ($siteInfo->sandbox == 0)
                            <form action="{{route('sandBox')}}" method="post" class="js-store">
                                @csrf
                                <input type="hidden" name="type" value="enable">
                                <button class="btn btn-primary">Включить тестовый режим</button>
                            </form>
                        @else
                            <form action="{{route('sandBox')}}" method="post" class="js-store">
                                @csrf
                                <input type="hidden" name="type" value="disabled">
                                <button class="btn btn-primary">Выключить тестовый режим</button>
                            </form>

                            <div class="row">
                                <div class="col-lg-6">
                                    <h3>Создать пользователей</h3>
                                    <form action="{{route('sandBox')}}" method="post" class="js-store">
                                        <input type="hidden" name="type" value="user_create">
                                        <div class="form-group">
                                            <label for="width">Количество пользователей</label>
                                            <input type="number" name="count" value="" max="1000" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="width">ID куратора</label>
                                            <input type="number" name="refer" value="1" class="form-control">
                                        </div>
                                        <button class="btn-primary btn">Создать</button>
                                    </form>
                                </div>
                                <div class="col-lg-6">
                                    <h3>Начислить баланс пользователям</h3>
                                    <form action="{{route('sandBox')}}" method="post" class="js-store">
                                        <input type="hidden" name="type" value="balance">
                                        <div class="form-group">
                                            <label for="width">ID пользователя (0 - всем)</label>
                                            <input type="number" name="user_id" value="0" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label for="width">Что сделать</label>
                                            <select name="what_do" class="form-control">
                                                <option value="increment">Прибавить</option>
                                                <option value="set_up">Установить</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="width">Сумма</label>
                                            <input type="number" name="amount" value="0" class="form-control">
                                        </div>
                                        <button class="btn-primary btn">Создать</button>
                                    </form>
                                </div>
                                <div class="col-lg-6">
                                    <h3>Активировать пользователей</h3>
                                    <form action="{{route('autoActivateStore')}}" class="js-store js-auto-activate" method="post">
                                        <div class="form-group">
                                            <label for="">Сколько пользователей активировать?</label>
                                            <select name="activate_count" id="activate-count" class="form-control">
                                                <option value="1">1</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Какое будет указано время активации у пользователей?</label>
                                            <select name="time_type" id="" class="form-control">
                                                <option value="1">Реальное время активации</option>
                                                <option value="2">Дата старта проекта</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Какой тариф вы хотите активировать?</label>
                                            <select name="mark" id="mark" class="form-control">
                                                <option value="0" >Выбрать тариф</option>
                                                @foreach ($marks as $mark)
                                                    <option value="{{$mark->id}}" >{{\App\Helpers\MarksHelper::markName($mark->id)}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div id="for-result" style="display: none;">Цена тарифа <span id="mark-enter"></span>{!! \App\Libraries\Helpers::siteCurrency('icon') !!} Эта сумма есть у <span class="count-users"></span> пользователей</div>
                                        {{--<input type="submit" class="btn btn-primary mt-10" id="start-activate" style="display: none;" value="Активировать">--}}
                                        <button class="btn btn-primary mt-10" id="start-activate" style="display: none;">Активировать <span class="count-users-result"></span> неактивированных пользователей</button>
                                    </form>
                                </div>
                            </div>
                        @endif


                    </div>
                </div>
            </div>
        </div>

    @endif
@endsection
