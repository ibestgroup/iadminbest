@extends("layout.admin.app")

@section('title', __('main.admin_setting'))

@section('content')
    @inject('helper', "\App\Libraries\Helpers")
    <div class="row">
        @if ($helper->siteType() == 1)

            <div class="col-md-12">
                <div class="box">
                    <div class="box-body">
                        <form action="{{route('setting')}}" class="js-siteInfo" method="post">
                            <div class="form-group">
                                <label>{!! __('main.admin_site_name') !!}</label>
                                <input type="text" class="form-control" placeholder="{!! __('main.admin_site_name') !!}" name="name" value="{{$siteInfo['name']}}">
                            </div>
                            <div class="col-lg-6 ta-center">
                                <div class="form-group">
                                    <label for="">{!! __('main.admin_site_logo') !!}</label>
                                    <div class="kv-avatar text-center">
                                        <div class="file-loading">
                                            <input id="logo" name="logo" type="file" data-default="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)->first()->logo}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 ta-center">
                                <div class="form-group">
                                    <label for="">{!! __('main.admin_custom_avatar') !!}</label>
                                    <div class="kv-avatar text-center">
                                        <div class="file-loading">
                                            <input id="logo" name="no_image" type="file" data-default="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)->first()['no_image']}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{!! __('main.admin_start_reg') !!}</label>
                                <div class="input-group">

                                    @if ($siteInfo['start'])
                                        <b>{{date('Y-m-d H:i:s', strtotime($siteStart['start_reg']))}}</b>
                                        <span class="form-prompt red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Невозможно изменить, так как проект стартовал">?</span>
                                    @else
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="datetime-local" class="form-control pull-right" name="start_reg" value="{{$siteStart['start_reg']}}">
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{!! __('main.admin_start_activation') !!}</label>
                                <div class="input-group">
                                    @if ($siteInfo['start'])
                                        <b>{{date('Y-m-d H:i:s', strtotime($siteStart['start_pay']))}}</b>
                                        <span class="form-prompt red" data-toggle="tooltip" data-placement="top" title="" data-original-title="Невозможно изменить, так как проект стартовал">?</span>
                                    @else
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="datetime-local" class="form-control pull-right" name="start_pay" value="{{$siteStart['start_pay']}}">
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-6 pl-0">
                                    <label>{!! __('main.admin_cashout_limit') !!}</label>
                                    <div class="clearfix"></div>
                                    <div class="col-lg-6 pl-0">
                                        {!! __('main.admin_more') !!}
                                        <input type="number" class="form-control" name="min_cashout" step="{{(\App\Libraries\Helpers::siteCurrency() > 3)?'0.00000001':'0.01'}}" value="{{$siteInfo['min_cashout']}}">
                                    </div>
                                    <div class="col-lg-6 pr-0">
                                        {!! __('main.admin_less') !!}
                                        <input type="number" class="form-control" name="max_cashout" step="{{(\App\Libraries\Helpers::siteCurrency() > 3)?'0.00000001':'0.01'}}" value="{{$siteInfo['max_cashout']}}">
                                    </div>
                                    <div class="col-lg-6 pl-0">
                                        {!! __('main.admin_cashout_limit_day') !!}
                                        <input type="number" class="form-control" name="cashout_limit_day" step="{{(\App\Libraries\Helpers::siteCurrency() > 3)?'0.00000001':'0.01'}}" value="{{$siteInfo['cashout_limit_day']}}">
                                    </div>
                                    <div class="col-lg-6 pr-0">
                                        {!! __('main.admin_cashout_limit_day_all') !!}
                                        <input type="number" class="form-control" name="cashout_limit_day_all" step="{{(\App\Libraries\Helpers::siteCurrency() > 3)?'0.00000001':'0.01'}}" value="{{$siteInfo['cashout_limit_day_all']}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-lg-6 pl-0">
                                    <label>{!! __('main.admin_cashout_before_start') !!}</label>
                                    <div class="clearfix"></div>
                                    <div class="col-lg-12">
                                        {!! __('main.admin_status') !!}
                                        <select name="cashout_before_start" class="form-control"  id="">
                                            <option value="0" {{($siteInfo['cashout_before_start'] == 0)?'selected="selected"':''}}>{!! __('main.admin_disable') !!}</option>
                                            <option value="1" {{($siteInfo['cashout_before_start'] == 1)?'selected="selected"':''}}>{!! __('main.admin_enable') !!}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <button class="btn btn-primary">{!! __('main.admin_save') !!}</button>
                        </form>
                    </div>
                </div>
            </div>

        @else

            <div style="display:none">
                @php
                    $mytime = Carbon\Carbon::now();
                    echo $mytime->toDateTimeString();
                @endphp
            </div>
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header">
                        <a href="{{route('admin')}}" class="btn btn-primary btn-flat mt-5">Админ панель</a>
                    </div>
                    <div class="box-body">
                        <h2>Основные настройки</h2>
                        <form action="{{route('setting')}}" method="post" class="js-siteInfo">
                            <div class="col-md-4">
                                <div class="kv-avatar text-center">
                                    <label for="logo">Логотип</label>
                                    <div class="file-loading">
                                        <input id="logo" name="logo" type="file" data-default="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['logo']}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="form-group">
                                    <label for="name">Название сайта</label>
                                    <input type="text" name="name" class="form-control" value="{{$siteInfo['name']}}" id="name" placeholder="Название сайта">
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <label>Вывод</label>
                                    <select name="cashout_confirm" class="form-control" id="">
                                        <option value="0" {{($siteInfo['cashout_confirm'] == 0)?'selected="selected"':''}}>Автоматический</option>
                                        <option value="1" {{($siteInfo['cashout_confirm'] == 1)?'selected="selected"':''}}>По заявкам</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6 pl-0">
                                        <label>Ограничения на вывод</label>
                                        <div class="clearfix"></div>
                                        <div class="col-lg-6 pl-0">
                                            от
                                            <input type="number" class="form-control" name="min_cashout" value="{{$siteInfo['min_cashout']}}">
                                        </div>
                                        <div class="col-lg-6 pr-0">
                                            до
                                            <input type="number" class="form-control" name="max_cashout" value="{{$siteInfo['max_cashout']}}">
                                        </div>
                                        <div class="col-lg-6 pl-0">
                                            {!! __('main.admin_cashout_limit_day') !!}
                                            <input type="number" class="form-control" name="cashout_limit_day" step="{{(\App\Libraries\Helpers::siteCurrency() > 3)?'0.00000001':'0.01'}}" value="{{$siteInfo['cashout_limit_day']}}">
                                        </div>
                                        <div class="col-lg-6 pr-0">
                                            {!! __('main.admin_cashout_limit_day_all') !!}
                                            <input type="number" class="form-control" name="cashout_limit_day_all" step="{{(\App\Libraries\Helpers::siteCurrency() > 3)?'0.00000001':'0.01'}}" value="{{$siteInfo['cashout_limit_day_all']}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-lg-12 p-0">
                                    <div class="col-lg-6 pl-0">
                                        <label>Пополнения будут идти на</label>
                                        <div class="clearfix"></div>
                                        <select name="payment_to" class="form-control" id="">
                                            <option value="0" {{($siteInfo['payment_to'] == 0)?'selected="selected"':''}}>Баланс для покупок</option>
                                            <option value="1" {{($siteInfo['payment_to'] == 1)?'selected="selected"':''}}>Баланс для вывода</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-6 pr-0">
                                        <label>Покупки будут оплачиваться с</label>
                                        <div class="clearfix"></div>
                                        <select name="payment_pay" class="form-control" id="">
                                            <option value="0" {{($siteInfo['payment_pay'] == 0)?'selected="selected"':''}}>Баланса для покупок</option>
                                            <option value="1" {{($siteInfo['payment_pay'] == 1)?'selected="selected"':''}}>Баланса для вывода</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="clearfix"></div>

                            <input type="submit" name="main_setting" class="btn btn-primary mt-10" value="Сохранить">
                        </form>

                        @if($helper->siteType() == 3)
                        <h2>Настройки валюты</h2>
                        <form action="{{route('currency')}}" method="post" class="js-store">
                            @csrf
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="type-current">Будет ли виртуальная валюта?</label><br>
                                    <input type="radio" name="status" value="1" id="type-current" {{($currency['status'] === 1)?'checked':''}}> да<br>
                                    <input type="radio" name="status" value="0" id="type-current" {{($currency['status'] === 0)?'checked':''}}> нет<br>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label for="name-current">Название валюты</label>
                                    <input type="text" name="name" class="form-control" value="{{$currency['name']}}" id="name-current" placeholder="Например: серебро">
                                </div>
                                <div class="form-group">
                                    <label for="price-current">Цена валюты</label>
                                    <div class="input-group">
                                        <input type="text" name="price" class="form-control" value="{{$currency['price']}}" id="price-current" placeholder="Например: серебро">
                                        <span class="input-group-addon">= 1 {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 mb-10">
                                <div class="kv-avatar text-center">
                                    <label for="image_current">Изображение валюты</label>
                                    <div class="file-loading">
                                        <input id="image_current" name="img" type="file" data-default="/iadminbest/storage/app/{{($currency['img'] == 'no_image')?'currency.png':$currency['img']}}">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <input type="submit" name="current_setting" class="btn btn-primary mt-10" value="{{ __('main.save') }}">
                        </form>
                        @endif

                        {{--<h2>Бонусы за пополнение</h2>--}}

                        {{--<form action="{{route('storeBonus')}}" method="post" class="js-siteInfo">--}}

                            {{--<label for="bonus">Бонус за пополнение</label> <br>--}}
                            {{--Если сумма от--}}
                            {{--<input type="number" name="more" value="0" id="bonus" placeholder="" style="width: 50px;">--}}
                            {{--рублей до--}}
                            {{--<input type="number" name="less" value="0" id="bonus" placeholder="" style="width: 50px;">--}}
                            {{--то бонус--}}
                            {{--<input type="number" name="amount" value="0" id="bonus" placeholder="" style="width: 50px;">--}}
                            {{--<select name="type" id="">--}}
                                {{--<option value="1">виртуальных монет/рублей</option>--}}
                                {{--<option value="2">процентов</option>--}}
                            {{--</select>--}}
                            {{--<input type="submit" name="bonus_setting" class="btn btn-primary" value="Добавить">--}}
                        {{--</form>--}}
                        {{--<h3>Созданные бонусы</h3>--}}
                        {{--@foreach($bonuses as $bonus)--}}
                            {{--<form action="{{route('deleteBonus')}}" method="post" class="js-siteInfo">--}}
                                {{--Если сумма больше {{$bonus['more']}} и меньше {{$bonus['less']}} то прибавляем {{$bonus['amount']}} {{($bonus['type'] == 1)?'монет':'процентов'}}--}}
                                {{--<input type="hidden" name="bonus_id" value="{{$bonus['id']}}">--}}
                                {{--<input type="submit" name="bonus_del" value="Удалить">--}}
                            {{--</form>--}}
                        {{--@endforeach--}}

                        <h2>Бонусы по клику для пользователей</h2>

                        <form action="{{route('updateBonusSetting')}}" method="post" class="js-store">
                            Включить
                            <input type="radio" value="1" name="status" {{($bonusSetting['status'] == 1)?'checked':''}}> да
                            <input type="radio" value="0" name="status" {{($bonusSetting['status'] == 0)?'checked':''}}> нет
                            <br>
                            Бонус может выпасть от
                            <input type="number" name="more" value="{{$bonusSetting['more']}}" placeholder="" style="width: 50px;">
                            до
                            <input type="number" name="less" value="{{$bonusSetting['less']}}" placeholder="" style="width: 50px;">
                            каждые
                            <input type="number" name="second" value="{{$bonusSetting['second']}}"placeholder="" style="width: 75px;">
                            секунд <br>
                            Бонус за обмен "баланса для вывода" на "баланс для покупок"
                            <input type="number" name="cashout_for_buy" id="bonus-cashout" value="{{$bonusSetting['cashout_for_buy']}}">%<br>
                            <input type="submit" value="Сохранить" class="btn btn-primary">
                        </form>

                        <h2>Реферальная программа</h2>
                        <form action="{{route('affiliateStore')}}" method="post" class="js-store">
                            @for ($i = 1; $i <= 8; $i++)
                                {{$i}} уровень <input type="number" name="level_{{$i}}" value="{{$affilate['level_'.$i]}}">%<br>
                            @endfor
                            <input type="submit" value="Сохранить" class="btn btn-primary">
                        </form>

                        <h2>Псевдоочки</h2>

                        <form action="{{route('storeHashPower')}}" method="post" class="js-store">
                            Включить <br>
                            <input type="radio" value="1" name="status" {{($hashPower['status'] == 1)?'checked':''}}> да
                            <br>
                            <input type="radio" value="0" name="status" {{($hashPower['status'] == 0)?'checked':''}}> нет
                            <br>
                            Название <br>
                            <input type="text" name="name" value="{{$hashPower['name']}}"> <br>
                            Цена по отношению к валюте <br>
                            <input type="number" name="price" value="{{$hashPower['price']}}"><br>
                            Сколько процентов псевдоочков от пополнения баланса <br>
                            <input type="number" name="payment" value="{{$hashPower['payment']}}">% <br>
                            Сколько процентов псевдоочков от потраченных на покупки <br>
                            <input type="number" name="buy_order" value="{{$hashPower['buy_order']}}">% <br>
                            <div class="col-lg-6">
                                За пополнение рефералами <br>
                                @for ($i = 1; $i <= 8; $i++)
                                    {{$i}} ур <input type="number" name="level_{{$i}}" value="{{$hashPower['level_'.$i]}}">% <br>
                                @endfor
                            </div>
                            <div class="col-lg-6">
                                За покупки рефералами <br>
                                @for ($i = 1; $i <= 8; $i++)
                                    {{$i}} ур <input type="number" name="level_buy_{{$i}}" value="{{$hashPower['level_buy_'.$i]}}">%
                                    <br>
                                @endfor
                            </div>
                            <input type="submit" value="Сохранить" class="btn btn-primary">
                        </form>
                    </div>
                </div>
            </div>

        @endif

    </div>
@endsection
