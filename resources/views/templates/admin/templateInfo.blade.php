@extends("layout.admin.app")

@section('title', 'Админ панель')

@section('content')
    @if (Auth::id() == 1)
        <div class="box" style="overflow: visible;">
            <div class="box-header">
                <a href="{{route('templates')}}" class="btn btn-primary mb-10">Назад к шаблонам</a>
                <br>

                <form action="{{route('templatesStore')}}" class="js-store" method="post" data-load='.content'>
                    @csrf
                    <input type="hidden" name="id" value="{{$templateInfo['id']}}">

                    <h3 class="">Настройки для "{{$templateInfo['name_ru']}}"
                        @if ($templateInfo['status'] == 1)
                            <button class="btn btn-success btn-xs" disabled="disabled">Активирован</button>
                        @else
                            <input type="hidden" name="activate" value="1">
                            <button class="btn btn-success btn-xs" name="activate">Активировать этот шаблон</button>
                            <p class="btn btn-danger btn-xs"  data-toggle="modal" data-target="#delete-template">Удалить шаблон</p>
                        @endif
                    </h3>
                </form>

                <div class="modal fade" id="delete-template">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span></button>
                                <h4 class="modal-title" style="text-align: center; color: #FF8D00;">Удалить шаблон</h4>
                            </div>
                            <div class="modal-body">
                                <p>
                                Если вы удалите шаблон, то все виджеты и блоки которые были созданы в этом шаблоне будут удалены. Вы уверены что хотите продолжить?
                                <form action="{{route('templatesStore')}}" method="post" class="js-store">
                                    @csrf
                                    <input type="hidden" name="delete" value="1">
                                    <input type="hidden" name="id" value="{{$templateInfo['id']}}">
                                    <button class="btn btn-danger">Удалить</button>
                                </form>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body">
                @foreach($pages as $page)
                    <h4 class="box-title">Страница: {{$page['name']}}</h4>
                    <div style="border-left: 1px solid #ff8d00; padding-left: 20px;">
                    @forelse($grids as $grid)
                        @if ($grid['page'] == $page['link'])
                            <form action="{{route('templatesDelete')}}" class="js-store" method="post" data-load='.content'>
                                @csrf
                                <input type="hidden" name="id" value="{{$grid['id_in_system']}}">
                                <input type="hidden" name="template" value="{{$grid['template']}}">
                                <p style="font-weight: bold;">Область - {{$grid['name_ru']}}
                                    <button class="btn btn-danger btn-xs">Удалить</button>
                                </p>
                            </form>
                            <div style="border-left: 1px solid #ff8d00; padding-left: 20px;">

                                <table class="table table-hover">
                                    <tbody>
                                        <tr>
                                            <th style="text-align: left;">Название</th>
                                            <th style="text-align: left;">Тип блока</th>
                                            <th style="text-align: left;">Виджет</th>
                                            <th>Настройка</th>
                                        </tr>
                                        @forelse($grid['children'] as $child)
                                            <tr>
                                                <td>{{$child['name_ru']}}</td>
                                                <td>{{\App\Libraries\Helpers::widgetName($child['type'])}}</td>
                                                <td>{{\App\Libraries\Helpers::widgetName($child['option_value']['widget'])}}</td>
                                                <td class="ta-center">
                                                    <form action="{{route('templatesDelete')}}" class="js-store" method="post" data-load='.content'>
                                                        <input type="hidden" name="id" value="{{$child['id_in_system']}}">
                                                        <input type="hidden" name="template" value="{{$child['template']}}">
                                                        @csrf
                                                        <button class="btn btn-danger btn-xs">Удалить</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr><td>Здесь нет виджетов</td></tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                        @endif
                    @empty
                        Здесь нет виджетов
                    @endforelse
                    </div>
                @endforeach

                @if ($templateInfo['id'] != 0)
                    <div class="col-lg-6">
                        <form action="{{route('addToStore')}}" class="js-store">
                            <h3>Добавить в магазин шаблонов</h3>
                            <p style="border-left: 3px solid #ff8d00; padding: 10px;">
                                Вы можете отправить свой шаблон на модерацию в магазин шаблонов. Если Ваш шаблон пройдет
                                проверку, вы будете получать 50% с каждой покупки этого шаблона.
                            </p>
                            <input type="hidden" value="1" name="status">
                            <input type="hidden" value="{{$templateInfo['id']}}" name="id">
                            <div class="form-group">
                                <label for="">Имя в магазине</label>
                                <input type="text" class="form-control" name="name" placeholder="Имя в магазине">
                            </div>
                            <div class="form-group">
                                <label for="">Ссылка на изображение</label>
                                <input type="text" class="form-control" name="img" placeholder="Ссылка на изображение">
                            </div>
                            <div class="form-group">
                                <label for="">Цена</label>
                                <input type="number" class="form-control" name="price" placeholder="Цена">
                            </div>
                            <button class="btn btn-primary mt-10">Добавить</button>
                        </form>
                    </div>
                @endif
                    @if ($templateInfo['id'] != 0)
                    <div class="col-lg-6">
                        <form action="{{route('addToStore')}}" class="js-store">
                            <h3>Добавить в библиотеку</h3>
                            <p style="border-left: 3px solid #ff8d00; padding: 10px;">
                                Вы можете добавить шаблон в свою библиотеку. Этот шаблон будет в библиотеке вашего аккаунта
                                на конструкторе, и вы сможете использовать его на любом из ваших сайтов.
                            </p>
                            <input type="hidden" value="0" name="status">
                            <input type="hidden" value="{{$templateInfo['id']}}" name="id">
                            <div class="form-group">
                                <label for="">Имя в магазине</label>
                                <input type="text" class="form-control" name="name" placeholder="Имя в магазине">
                            </div>
                            <div class="form-group">
                                <label for="">Ссылка на изображение</label>
                                <input type="text" class="form-control" name="img" placeholder="Ссылка на изображение">
                            </div>
                            <input type="hidden" class="form-control" name="price" placeholder="Цена">
                            <button class="btn btn-primary mt-10">Добавить</button>
                        </form>
                    </div>
                @endif
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    @endif

@endsection
