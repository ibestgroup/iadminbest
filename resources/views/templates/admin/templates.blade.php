@extends("layout.admin.app")

@section('title', __('main.admin_panel'))

@section('content')
    @if (Auth::id() == 1)
        <div class="box" style="overflow: visible;">
            <div class="box-header">
                <h3 class="box-title">{{ __('main.admin_you_templates') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-hover">
                    <tbody>
                    <tr>
                        <th>ID</th>
                        <th>{{ __('main.admin_name') }}</th>
                        <th>{{ __('main.admin_status') }}</th>
                        <th>{{ __('main.admin_settings') }}</th>
                    </tr>
                    @forelse($templates as $template)
                        <tr>
                            <td class="ta-center va-middle">{{$template->id}}</td>
                            <td class="va-middle">{{$template->name_ru}}</td>
                            <td class="ta-center va-middle">{!! ($template->status)?'<span class="label label-success">'.__('main.admin_enable_w') .'</span>':'<span class="label label-warning">'.__('main.admin_disable_w') .'</span>' !!}</td>
                            <td class="ta-center va-middle">
                                <a href="{{route('templateInfo', ['id' => $template->id])}}" class="btn btn-primary">{{ __('main.admin_settings') }}</a>
                                @if ($template->id != 1)
                                <p class="btn btn-danger" data-toggle="modal" data-target="#delete-template-{{$template->id}}">Удалить шаблон</p>
                                <div class="modal fade" id="delete-template-{{$template->id}}">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                                <h4 class="modal-title" style="text-align: center; color: #FF8D00;">Удалить шаблон</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                    Если вы удалите шаблон, то все виджеты и блоки которые были созданы в этом шаблоне будут удалены. Вы уверены что хотите продолжить?
                                                <form action="{{route('templatesStore')}}" method="post" class="js-store">
                                                    @csrf
                                                    <input type="hidden" name="delete" value="1">
                                                    <input type="hidden" name="id" value="{{$template->id}}">
                                                    <button class="btn btn-danger">Удалить</button>
                                                </form>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                </div>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>У вас нет созданных шаблонов</td>
                        </tr>
                    @endforelse

                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->

        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ __('main.admin_add_template') }}</h3>
            </div>

            <form action="{{route('templatesAdd')}}" class="js-store" method="post" data-load='.content'>
                @csrf
                <div class="box-body">
                    <div class="col-lg-6">
                        <input type="text" class="form-control" name="name_ru" placeholder="{{ __('main.admin_name_template') }}">
                    </div>
                    {{--<div class="col-lg-6">--}}
                        {{--<select name="status" class="selectpicker">--}}
                            {{--<option value="1">Включенный</option>--}}
                            {{--<option value="0">Выключенный</option>--}}
                        {{--</select>--}}
                    {{--</div>--}}
                </div>
                <div class="box-footer ta-center">
                    <button class="btn btn-primary">{{ __('main.admin_add') }}</button>
                </div>
            </form>
        </div>

        {{--<div class="box">--}}
            {{--<div class="box-header with-border">--}}
                {{--<h3 class="box-title">Магазин шаблонов</h3>--}}
            {{--</div>--}}

            {{--<div class="box-body">--}}
                {{--@foreach($storeTemplates as $store)--}}
                    {{--<div class="col-lg-3">--}}
                        {{--<h4>{{$store->name}}</h4>--}}
                        {{--<h5>{{($store->price > 0)?$store->price:'Бесплатно'}}</h5>--}}
                        {{--<form action="{{route('buyTemplate')}}" class="js-store" method="post">--}}
                            {{--@csrf--}}

                            {{--<input type="hidden" name="type_order" value="buy">--}}
                            {{--<input type="hidden" name="template_id" value="{{$store->id}}">--}}
                            {{--<button class="btn btn-primary">Купить</button>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--@endforeach--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="box">--}}
            {{--<div class="box-header with-border">--}}
                {{--<h3 class="box-title">Моя библиотека шаблонов</h3>--}}
            {{--</div>--}}

            {{--<div class="box-body">--}}
                {{--@foreach($libraryTemplates as $template)--}}
                    {{--<div class="col-lg-3">--}}
                        {{--<h4>{{$template->templateInfo->name}}</h4>--}}
                        {{--<h5>{{($template->templateInfo->price > 0)?$template->templateInfo->price:'Бесплатно'}}</h5>--}}
                        {{--@if ($template->templateInfo->user_id == \App\Libraries\Helpers::getSiteAdmin())--}}
                            {{--Это ваш шаблон--}}
                        {{--@endif--}}
                        {{--<form action="{{route('buyTemplate')}}" class="js-store" method="post">--}}
                            {{--<input type="hidden" name="type_order" value="import">--}}
                            {{--@csrf--}}
                            {{--<input type="hidden" name="template_id" value="{{$template->templateInfo->id}}">--}}
                            {{--<button class="btn btn-primary">Загрузить</button>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--@endforeach--}}
            {{--</div>--}}
        {{--</div>--}}
    @endif
@endsection
