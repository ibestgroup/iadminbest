@extends("layout.admin.app")

@section('title', __('main.admin_panel'))

@section('content')
    @if (Auth::id() == 1)
        <h3>Моя библиотека шаблонов</h3>
        <div class="row">
            <div class="row">
                <div class="col-lg-12" id="templates-market">
                    @forelse($libraryTemplates as $template)
                        <div class="col-lg-3 col-md-4 col-sm-6 mt-10">
                            <div class="box">
                                {{--<div class="box-header with-border">--}}
                                    {{--<h4 class="box-title">{{$template->templateInfo->name}}</h4>--}}
                                {{--</div>--}}

                                <div class="box-header with-border">
                                    <h4 class="box-title" style="white-space: nowrap; overflow: hidden; display: block; float: left;line-height: 34px; /*max-width: 75%; ">{{$template->templateInfo->name}}</h4>
                                    {{--<button class="btn btn-danger pull-right" style="color: #fff;"><i class="fa fa-times"></i></button>--}}
                                </div>
                                <div class="box-body">
                                    <div style="height: 200px; text-align: center;">
                                        <img src="{{($template->templateInfo->img == 'no_image')?'https://www.samsung.com/etc/designs/smg/global/imgs/support/cont/NO_IMG_600x600.png':$template->templateInfo->img}}" style="width: 100%; max-width: 225px;" alt="">
                                    </div>
                                    <h5>
{{--                                        {!! ($template->templateInfo->price > 0)?"Цена: ".\App\Libraries\Helpers::currencyConverter('RUB', \App\Libraries\Helpers::siteCurrency('code'), $template->templateInfo->price).\App\Libraries\Helpers::siteCurrency('icon'):'Бесплатно' !!}</h5>--}}

                                    @if ($template->templateInfo->price > 0)
                                        Цена: {!! \App\Libraries\Helpers::currencyConverter('RUB', \App\Libraries\Helpers::siteCurrency('code'), $template->templateInfo->price).\App\Libraries\Helpers::siteCurrency('icon') !!}
                                    @else
                                        Бесплатно
                                    @endif
                                    @if ($siteAdmin == $template->templateInfo->user_id)
                                        @if ($template->templateInfo->status == 1)
                                            <span class="label label-warning">На модерации</span>
                                        @elseif ($template->templateInfo->status == 2)
                                            <span class="label label-success">В магазине</span>
                                        @elseif ($template->templateInfo->status == 3)
                                            <span class="label label-primary">Для себя(проверен)</span>
                                        @else
                                            <span class="label label-primary">Для себя</span>
                                        @endif
                                    @endif
                                    <h5>{{ "Автор: ".\App\Libraries\Helpers::convertUsCore('login', $template->templateInfo->user_id)}}
                                        {!! ($siteAdmin == $template->templateInfo->user_id)?'<span class="label label-info">Вы автор</span>':'' !!}
                                    </h5>
                                    <form action="{{route('buyTemplate')}}" class="js-store" method="post">
                                        @csrf
                                        <input type="hidden" name="type_order" value="import">
                                        <input type="hidden" name="template_id" value="{{$template->templateInfo->id}}">
                                        <button class="btn btn-primary btn-flat m-auto">Загрузить</button>
                                        {!! ($siteAdmin == $template->templateInfo->user_id)?'<a class="btn btn-primary pull-right edit-template" data-toggle="modal" data-target="#edit-template-'.$template->templateInfo->id.'" style="color: #fff;"><i class="fa fa-edit"></i></a>':'' !!}
                                    </form>
                                    <div class="modal fade" id="edit-template-{{$template->templateInfo->id}}">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">×</span></button>
                                                    <h4 class="modal-title" style="text-align: center; color: #FF8D00;">Изменить шаблон</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>
                                                    <form action="{{route('templateMarketEdit')}}" method="post" class="js-store">
                                                        @csrf
                                                        <input type="hidden" name="delete" value="1">
                                                        <input type="hidden" name="id" value="{{$template->templateInfo->id}}">

                                                        <div class="form-group">
                                                            <label for="">Статус</label>
                                                            <select name="status" id="" class="form-control">
                                                                @if ($template->templateInfo->status < 2)
                                                                    <option value="0" {{($template->templateInfo->status == 0)?'selected="selected"':''}}>Оставить для себя</option>
                                                                    <option value="1" {{($template->templateInfo->status == 1)?'selected="selected"':''}}>Подать на модерацию</option>
                                                                @elseif ($template->templateInfo->status >= 2)
                                                                    <option value="3" {{($template->templateInfo->status == 0)?'selected="selected"':''}}>Убрать с продажи</option>
                                                                    <option value="2" {{($template->templateInfo->status == 2)?'selected="selected"':''}}>Оставить в магазине</option>
                                                                @endif
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Название</label>
                                                            <input type="text" class="form-control" name="name" value="{{$template->templateInfo->name}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Цена</label>
                                                            <input type="number" class="form-control" name="price" value="{{$template->templateInfo->price}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Изображение</label>
                                                            <input type="text" class="form-control" name="img" value="{{$template->templateInfo->img}}">
                                                        </div>
                                                        <button class="btn btn-primary">Сохранить</button>
                                                    </form>
                                                    </p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="col-lg-12 mt-10">
                            <div class="box">
                                <div class="box-body">
                                    В вашей библиотеке нет шаблонов
                                </div>
                            </div>
                        </div>
                    @endforelse
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <div class="custom-paginate clearfix" data-refresh="#templates-market">
                            {{$libraryTemplates->appends(request()->input())->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
