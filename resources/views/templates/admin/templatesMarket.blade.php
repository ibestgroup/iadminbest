@extends("layout.admin.app")

@section('title', __('main.admin_panel'))

@section('content')
    @if (Auth::id() == 1)
        <h3>Магазин шаблонов</h3>
        <div class="row">
            <div class="col-lg-12">
                <a href="{{route('templatesMarket')}}" class="btn btn-primary btn-flat">Все</a>
                <a href="{{route('templatesMarket')}}?type=free" class="btn btn-primary btn-flat">Бесплатные</a>
            </div>
            {{--<a href="" class="btn btn-primary btn-flat"></a>--}}
            <div class="row">
                <div class="col-lg-12" id="templates-market">
                    @forelse($storeTemplates as $store)
                    <div class="col-lg-3 col-md-4 col-sm-6 mt-10">
                        <div class="box">
                            <div class="box-header with-border">
                                <h4 class="box-title" style="white-space: nowrap; overflow: hidden; display: block; float: left;line-height: 34px; /*max-width: 75%;">{{$store->name}}</h4>
                                {{--<button class="btn btn-danger pull-right" style="color: #fff;"><i class="fa fa-times"></i></button>--}}
                            </div>
                            <div class="box-body">
                                <div style="height: 200px; text-align: center;">
                                    <img src="{{($store->img == 'no_image')?'https://www.samsung.com/etc/designs/smg/global/imgs/support/cont/NO_IMG_600x600.png':$store->img}}" style="width: 100%; max-width: 225px;" alt="">
                                </div>
                                <h5>
                                    @if ($store->price > 0)
                                        Цена: {!! \App\Libraries\Helpers::currencyConverter('RUB', \App\Libraries\Helpers::siteCurrency('code'), $store->price).\App\Libraries\Helpers::siteCurrency('icon') !!}
                                        {!! ($store->checkOrder())?'<span class="label label-success">Куплен</span>':'' !!}
                                    @else
                                        Бесплатно
                                    @endif
                                </h5>
                                <h5>{{ "Автор: ".\App\Libraries\Helpers::convertUsCore('login', $store->user_id)}}
                                    {!! ($siteAdmin == $store->user_id)?'<span class="label label-info">Вы автор</span>':'' !!}
                                </h5>
                                <form action="{{route('buyTemplate')}}" class="js-store" method="post">
                                    @csrf
                                    <input type="hidden" name="type_order" value="buy">
                                    <input type="hidden" name="template_id" value="{{$store->id}}">
                                    @if (!$store->checkTemplate())
                                        @if($siteAdmin == $store->user_id or $store->checkOrder())
                                            <button class="btn btn-success btn-flat m-auto">Загрузить</button>
                                        @else
                                            <button class="btn btn-primary btn-flat m-auto">Купить</button>
                                        @endif
                                        <p data-template="{{$store->id}}" class="btn btn-primary btn-flat pull-right demo-enable" style="color: #fff;">Демо</p>
                                    @else
                                        <a href="{{route('templatesLibrary')}}" class="btn btn-success btn-flat m-auto">Уже в библиотеке</a>
                                        {!! ($siteAdmin == $store->user_id)?'<a class="btn btn-primary pull-right edit-template" data-toggle="modal" data-target="#edit-template-'.$store->id.'" style="color: #fff;"><i class="fa fa-edit"></i></a>':'' !!}
                                    @endif
                                </form>
                                <div class="modal fade" id="edit-template-{{$store->id}}">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                                <h4 class="modal-title" style="text-align: center; color: #FF8D00;">Изменить шаблон</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>
                                                <form action="{{route('templateMarketEdit')}}" method="post" class="js-store">
                                                    @csrf
                                                    <input type="hidden" name="delete" value="1">
                                                    <input type="hidden" name="id" value="{{$store->id}}">
                                                    <div class="form-group">
                                                        <label for="">Название</label>
                                                        <input type="text" class="form-control" name="name" value="{{$store->name}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Цена</label>
                                                        <input type="number" class="form-control" name="price" value="{{$store->price}}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Изображение</label>
                                                        <input type="text" class="form-control" name="img" value="{{$store->img}}">
                                                    </div>
                                                    <button class="btn btn-primary">Сохранить</button>
                                                </form>
                                                </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @empty
                        <div class="col-lg-12 mt-10">
                            <div class="box">
                                <div class="box-body">
                                    Нет шаблонов по вашему запросу
                                </div>
                            </div>
                        </div>
                    @endforelse
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <div class="custom-paginate clearfix" data-refresh="#templates-market">
                            {{$storeTemplates->appends(request()->input())->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
