@extends("layout.admin.app")

@section('title', 'Админ панель')

@section('content')
    @inject('helper', "\App\Libraries\Helpers")
    @if (Auth::id() == 1)

        <div class="row">
            <div class="col-md-12">
                <div class="box">

                    <div class="box-header">
                        <a href="{{route('admin')}}" class="btn btn-primary btn-flat mt-5">Админ панель</a>
                        <a href="{{route('birds')}}" class="btn btn-primary btn-flat mt-5">Создать вклады</a>
                        <h2>Добавьте источники дохода</h2>
                        <p>Выберите с помощью чего будут добывать Ваши пользователи. Это могут быть деревья и фрукты, птицы и яйца, машины и заказы. Все что Вам вздумается.</p>
                    </div>
                    <div class="box-body">

                        <div class="col-md-12 type-order-item">
                            <form action="{{route('storeTypesOrder')}}" method="post" class="js-store">
                                @csrf
                                <div class="col-lg-8">
                                    Введите название(в единственном числе) <input type="text" class="form-control" name="bird" placeholder="Например: вклады под процент" autocomplete="off">
                                    Что будет производить?
                                    <span class="form-prompt" data-toggle="tooltip" data-placement="top"
                                          title='Это поле нужно только в том случае если у Вас в проекте есть придуманная Вами валюта.
                                    Если на вашем сайте пользователи будут просто вкладывать и получать рубли, просто напишите "рубль" или "руб".'>?</span>
                                    <input type="text" class="form-control" name="egg" placeholder="Например: рубли" autocomplete="off">

                                    {{--Сколько произведенных единиц будут равняться одному рублю?--}}

                                    {{--<span class="form-prompt" data-toggle="tooltip" data-placement="top"--}}
                                          {{--title='Если у вклады будут выплачивать рубли, то поставьте 1.'>?</span>--}}
                                    {{--(если указать 100, то за 100 произведенных единиц пользователь получит один рубль)--}}
                                    {{--<input type="number" name="price" class="form-control" autocomplete="off">--}}

                                    Сколько производимых единиц будут по цене 1 {{($helper->checkCurrency())?$currency['name']:'рубля'}}?
                                    <div class="form-group mb-0">
                                        <div class="input-group">
                                            <input type="text" name="price" class="form-control" value="" id="price-current" placeholder="Количество единиц">
                                            <span class="input-group-addon">
                                                @if ($helper->checkCurrency())
                                                    = 1 {{$currency['name']}}
                                                @else
                                                    = 1 рубль
                                                @endif
                                            </span>
                                        </div>
                                    </div>

                                    {{--Срок жизни <br>--}}
                                    {{--<input type="radio" value="0" name="end" checked>Бесконечный--}}
                                    {{--<input type="radio" value="1" name="end">Определенный срок(указывается в объекте)<br>--}}
                                    {{--<div class="only-end" style="display: none;">--}}
                                        {{--Сбор только по окончанию срока жизни <br>--}}
                                        {{--<input type="radio" value="0" name="only_end" checked>Нет--}}
                                        {{--<input type="radio" value="1" name="only_end">Да<br>--}}
                                    {{--</div>--}}

                                    Сколько при обмене будет будет идти на баланс для вывода
                                    <input class="form-control" type="number" name="percent_cashout" placeholder="Сколько процентов на вывод" autocomplete="off">
                                </div>
                                <div class="col-lg-4">
                                    Изображение того что будет производить
                                    <div class="kv-avatar text-center" style="width: 250px;">
                                        <div class="file-loading">
                                            <input name="image" type="file" data-default="/iadminbest/storage/app/resource/no-image.png">
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" value="Добавить" class="btn btn-primary type-order-item-btn">
                            </form>
                        </div>
                            <h2>Ваши существующие категории объектов</h2>
                            @foreach($typesOrder as $order)
                                <div class="col-lg-12 type-order-item">
                                    <form action="{{route('editTypesOrder')}}" method="post" class="js-store" style="float: left;">
                                        @csrf
                                        <div class="col-lg-8">
                                            <input type="hidden" name="type_id" value="{{$order['id']}}">
                                            Название объекта(в единственном числе) <input type="text" class="form-control" name="bird" placeholder="Например: птица" value="{{$order['bird']}}">
                                            Что производит?(в единственном числе) <input type="text" class="form-control" name="egg" placeholder="Например: яйцо" value="{{$order['egg']}}">

                                            {{--Сколько единиц будут равняться одному серебру? (если указать 100, то за 100 произведенных единиц пользователь получит одно серебро)--}}
                                            {{--<input type="number" name="price" class="form-control" value="{{$order['price']}}">--}}

                                            Сколько производимых единиц будут по цене 1 {{($helper->checkCurrency())?$currency['name']:'рубля'}}?
                                            <div class="form-group mb-0">
                                                <div class="input-group">
                                                    <input type="text" name="price" class="form-control" value="{{$order['price']}}" id="price-current" placeholder="Количество единиц">
                                                    <span class="input-group-addon">
                                                        @if ($helper->checkCurrency())
                                                            = 1 {{$currency['name']}}
                                                        @else
                                                            = 1 рубль
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                            {{--Срок жизни <br>--}}
                                            {{--<input type="radio" value="0" name="end" {{($order['end'] == 0)?'checked':''}}>Бесконечный--}}
                                            {{--<input type="radio" value="1" name="end" {{($order['end'] == 1)?'checked':''}}>Определенный срок(указывается в объекте)<br>--}}
                                            {{--<div class="only-end" style="display: none;">--}}
                                                {{--Сбор только по окончанию срока жизни <br>--}}
                                                {{--<input type="radio" value="0" name="only_end" {{($order['only_end'] == 0)?'checked':''}}>Нет--}}
                                                {{--<input type="radio" value="1" name="only_end" {{($order['only_end'] == 1)?'checked':''}}>Да<br>--}}
                                            {{--</div>--}}
                                            Сколько процентов на вывод
                                            <input type="number" class="form-control" name="percent_cashout" id="percent_cashout-{{$order['id']}}" placeholder="Сколько процентов на вывод" value="{{$order['percent_cashout']}}">
                                            {{--<label for="name-btn-{{$order['id']}}">Надпись на кнопке для сбора товара</label>--}}
                                            {{--<input type="text" class="form-control" name="name_btn" id="name-btn-{{$order['id']}}" placeholder="Имя" value="{{($order->name_btn)?$order->name_btn:'Собрать "'.$order->egg.'"'}}"--}}
                                            {{--<label>Цвет категории</label>--}}
                                            {{--<div class="clearfix"></div>--}}
                                            {{--<div class="col-lg-6 col-md-6 pl-0">--}}
                                                {{--<label for="color-{{$order['id']}}">Общий цвет</label>--}}
                                                {{--<input type="color" class="form-control" name="color" id="color-{{$order['id']}}" value="{{$order['color']}}">--}}
                                            {{--</div>--}}
                                            {{--<div class="col-lg-6 col-md-6 pr-0">--}}
                                                {{--<label for="color-text-{{$order['id']}}">Цвет текста</label>--}}
                                                {{--<input type="color" class="form-control" name="color_text" id="color-text-{{$order['id']}}" value="{{$order['color_text']}}">--}}
                                            {{--</div>--}}
                                        </div>
                                        <div class="col-lg-4">
                                            Изображение того что будет производить
                                            <div class="kv-avabtar text-center" style="width: 250px;">
                                                <div class="file-loading">
                                                    <input name="image" type="file" data-default="/iadminbest/storage/app/{{$order['image']}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <input type="submit" value="Сохранить" class="btn btn-primary type-order-item-btn" name="edit">
                                    </form>

                                    <form action="{{route('deleteTypesOrder')}}" method="post" class="js-store">
                                        @csrf
                                        <input type="hidden" name="type_id" value="{{$order['id']}}">
                                        <input type="submit" value="Удалить" class="btn btn-danger type-order-item-btn" name="delete">
                                    </form>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
