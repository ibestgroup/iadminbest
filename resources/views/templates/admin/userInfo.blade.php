@extends("layout.admin.app")

@section('title', __('main.admin_panel'))

@section('content')
    @inject('helper', "\App\Libraries\Helpers")
    @if (Auth::id() == 1)
        <div class="box">
            <!-- /.box-header -->
            <div class="box-header">
                <a href="{{route('allUsers')}}?page={{(isset($_COOKIE['page-all-users']))?$_COOKIE['page-all-users']:'false'}}" class="btn btn-primary">{{ __('main.back') }}</a>
                <a href="{{route('allUsers')}}" class="btn btn-primary">{{ __('main.to_list_users') }}</a>
            </div>
            <div class="box-body table-responsive no-padding">
                @if (empty($user))
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>{{ __('main.user_not_fount') }}</th>
                        </tr>
                    </table>
                @else
                    <form class="js-userEdit" action="{{route('userStore')}}" method="post">
                        @csrf
                        <input type="hidden" name="us_id" value="{{$user->id}}">
                        <div class="userInfo">
                            <div class="col-lg-12">
                                <div class="col-lg-3">ID</div>
                                <div class="col-lg-9">{{$user->id}}</div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-3">{{ __('main.login_name') }}</div>
                                <div class="col-lg-9">{{$user->login}}</div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-3">E-main</div>
                                <div class="col-lg-9">{{$user->email}}</div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-3">{{ __('main.user_date_register') }}</div>
                                <div class="col-lg-9">{{$user->regdate}}</div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-3">{{ __('main.user_who_invite') }}</div>
                                <div class="col-lg-9">
                                    <a class="btn btn-primary mb-5" href="{{route('userInfo', ['id' => $user->refer])}}">#{{$user->refer}} - {{\App\Libraries\Helpers::convertUs('login', $user->refer)}}</a>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-3">{{ __('main.user_surname') }}</div>
                                <div class="col-lg-9">{{($user->users_data->lastname)?$user->users_data->lastname:'фамилия не указана'}}</div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-3">{{ __('main.user_name') }}</div>
                                <div class="col-lg-9">{{($user->users_data->name)?$user->users_data->name:'имя не указано'}}</div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-3">{{ __('main.user_fathername') }}</div>
                                <div class="col-lg-9">{{($user->users_data->fathername)?$user->users_data->fathername:'отчество не указано'}}</div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-3">{{ __('main.user_balance') }}</div>
                                <div class="col-lg-9">
                                    @if ($helper->siteType() == 1)
                                        {{$user->balance}}
                                    @else
                                        {{ __('main.for_cashout') }}: {{$user->balance}} | {{ __('main.for_buy') }}: {{$user->balance_cashout}}
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-3">{{ __('main.user_privilege') }}</div>
                                <div class="col-lg-9">
                                    <select name="privelege">
                                        <option value="1" {{($user->privelege == 1)?'selected':''}}>{{\App\Libraries\Helpers::privelegeName(1)}}</option>
                                        <option value="2" {{($user->privelege == 2)?'selected':''}}>{{\App\Libraries\Helpers::privelegeName(2)}}</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-3">{{ __('main.widg_referrals') }}</div>
                                <div class="col-lg-9">
                                    @forelse($referrals as $item)
                                        @if ($item->id != 1)
                                            <a class="btn {{($item->type_follow)?'btn-primary':'btn-danger'}} mb-5" href="{{route('userInfo', ['id' => $item->id])}}">#{{$item->id}} - {{$item->login}}</a>
                                        @endif
                                    @empty
                                        У пользователя нет рефералов
                                    @endforelse
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-3">{{ __('main.user_buys') }}</div>
                                <div class="col-lg-9">
                                @forelse($activates as $activate)
                                    @if ($helper->siteType() == 1)
                                        <p class="btn btn-primary mb-5">{{$activate->marks->name}} #{{$activate['id']}}</p>
                                    @else
                                        <p class="btn btn-primary mb-5">{{$activate->bird->name}} #{{$activate['id']}}</p>
                                    @endif
                                @empty
                                        {{ __('main.user_not_buys') }}
                                @endforelse
                                </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <button class="btn btn-primary" style="display: block; margin: 10px auto">{{ __('main.save') }}</button>
                    </form>
                @endif
            </div>
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ __('main.activations') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody><tr>
                        <th>ID</th>
                        <th>{{ __('main.tariff') }}</th>
                        <th>{{ __('main.date') }}</th>
                    </tr>
                    @forelse ($paymentOrders as $paymentOrder)
                    <tr class="ta-center">
                        <td>{{$paymentOrder->id}}</td>
                        <td>{{\App\Helpers\MarksHelper::markName($paymentOrder->mark)}}</td>
                        <td>{{$paymentOrder->date}}</td>
                    </tr>
                    @empty
                        <tr style="width: 100%">
                            <td>{{ __('main.no_activation') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ __('main.payments') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody><tr>
                        <th>ID</th>
                        <th>{{ __('main.payment_system') }}</th>
                        <th>{{ __('main.amount') }}</th>
                        <th>{{ __('main.date') }}</th>
                    </tr>
                    @forelse ($payments as $payment)
                    <tr class="ta-center">
                        <td>{{$payment->id}}</td>
                        <td>Payeer</td>
                        <td>{{$payment->amount}}</td>
                        <td>{{$payment->date}}</td>
                    </tr>
                    @empty
                        <tr style="width: 100%">
                            <td>{{ __('main.no_paymentss') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">{{ __('main.cashout') }}</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody><tr>
                        <th>ID</th>
                        <th>{{ __('main.wallet') }}</th>
                        <th>{{ __('main.amount') }}</th>
                        <th>{{ __('main.date') }}</th>
                    </tr>
                    @forelse ($cashouts as $cashout)
                    <tr class="ta-center">
                        <td>{{$cashout->id}}</td>
                        <td>{{$cashout->to}}</td>
                        <td>{{$cashout->amount}}</td>
                        <td>{{$cashout->date}}</td>
                    </tr>
                    @empty
                        <tr>
                            <td style="width: 100%">{{ __('main.no_cashout') }}</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    @endif
@endsection
