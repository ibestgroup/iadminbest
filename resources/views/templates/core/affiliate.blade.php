@extends("layout.core.app")

@section('title', __('main.affiliate_programm'))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{!! __('main.affiliate_programm') !!}</h3>
                    </div>

                    <div class="box-body">
                        {!! __('main.ref_link') !!}: {{(isset($_SERVER['HTTPS']))?'https':'http'}}://{{$_SERVER['HTTP_HOST']}}/?ref={{Auth::user()->login}}
                        {{--<b>партнерская программа проходит стадию тестирования, скоро здесь будет ваша реферальная ссылка</b>--}}
                        {{--{{(isset($_SERVER['HTTPS']))?'https':'http'}}://{{$_SERVER['HTTP_HOST']}}/?id={{Auth::id()}}--}}
                        <h3 class="mt-5">{!! __('main.your_balance') !!}: {{\App\Models\CorePartnerBalance::where('user_id', Auth::id())->first()['rub']}} руб.</h3>

                        <br>
                        {!! __('main.affiliate_desc') !!}
                        <table class="table table-bordered">
                            <tbody>
                            {!! __('main.affiliate_th') !!}
                            @foreach($rangs as $key => $rang)
                            <tr
                            @if ($key + 1 == \App\Models\Users::where('id', Auth::id())->first()['privelege'])
                                style="font-weight: bold";
                            @endif
                            >
                                <td>{{$key+1}}</td>
                                <td>
                                    {{$rang['name']}}
                                    @if ($key + 1 == \App\Models\Users::where('id', Auth::id())->first()['privelege'])
                                        ({!! __('main.your_rang') !!})
                                    @endif
                                </td>
                                <td>{{$rang['to']}}</td>
                                <td><span class="badge bg-green">{{$rang['percent']}}%</span></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>

                        {!! __('main.total_ref') !!}: {{\App\Models\Users::where('refer', Auth::id())->count()}}
                        <table class="table table-bordered">
                            <tbody>
                            {!! __('main.refferals_th') !!}
                        @forelse ($referals as $referal)
                            <tr class="ta-center">
                                <td>{{$referal->id}}</td>
                                <td><a href="{{route('createUserAffiliate', ['id' => $referal->id])}}">{{$referal->login}}</a></td>
                                <td>{{\App\Models\CoreSiteList::where('user_id', $referal->id)->count()}}</td>
                                <td>{{round(\App\Models\CorePartnerPayments::where('site_admin', $referal->id)->sum('amount'), 2)}}</td>
                            </tr>

                        @empty

                            {!! __('main.no_ref') !!}
                        @endforelse
                            </tbody>
                        </table>
                    </div>
                    <div class="box-footer clearfix">
                        {{$referals->links()}}
                    </div>
                </div>
        </div>
    </div>
@endsection
