@extends("layout.core.app")

@section('title', 'Партнерская программа')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    @if (isset($siteInfo->name))
                        <h3 class="box-title">Партнерские начисления по сайту {{$siteInfo->name}}</h3>
                    @else
                        <h3 class="box-title">Партнерские начисления по всем сайтам</h3>
                    @endif
                </div>

                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Сумма пополнения</th>
                            <th>Ваше отчисление</th>
                            <th>Статус на момент операции</th>
                            <th>Дата</th>
                        </tr>

                        @forelse ($payments as $payment)
                            <tr class="ta-center">
                                <td>{{$payment['id']}}</td>
                                <td>{{$payment['amount']}}</td>
                                <td>+{{$payment['parent_amount']}}</td>
                                <td>{{$privilegeInfo[$payment['privilege'] - 1]['name']}}({{$privilegeInfo[$payment['privilege'] - 1]['percent']}}%)</td>
                                <td>{{$payment['date']}}</td>
                            </tr>

                        @empty
                            У вас нет ни одного реферала
                        @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="box-footer clearfix">
                    {{$payments->links()}}
                </div>
        </div>
    </div>
@endsection
