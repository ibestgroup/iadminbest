@extends("layout.core.app")

@section('title', 'Партнерская программа')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Сайты пользователя {{$user->login}}</h3>
                </div>

                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Название сайта</th>
                            <th>Количество пользователей</th>
                            <th>Всего пополнений</th>
                            <th>Заработано по партнерской</th>
                        </tr>

                        @forelse ($sites as $site)
                            <tr class="ta-center">
                                <td>{{$site['id']}}</td>
                                <td><a href="{{route('createSiteAffiliate', ['id' => $site['id']])}}">{{$site['name']}}</a></td>
                                <td>{{$site['users']}}</td>
                                <td>{{$site['payment']}}</td>
                                <td>{{$site['profit']}}</td>
                            </tr>

                        @empty
                            У вас нет ни одного реферала
                        @endforelse
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
@endsection
