<!DOCTYPE html>
<html lang="en">
<head>
    <title>iAdmin.best - конструктор проектов</title>
    <meta name="enot" content="1841592915861vnM5EBZL_RLt6Jey8UJREkD63tL_T9aS" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,700,800" rel="stylesheet">

    <link rel="shortcut icon" type="image/png" href="/iadminbest/favicon.ico"/>
    <meta name="verify-admitad" content="7921c58f87" />
    <meta name="propeller" content="220bfba10817d8d9f7c818a1413c29dd">

    <link rel="stylesheet" href="/iadminbest/public/css/app.css">

    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/animate.css">

    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/owl.carousel.min.css">
    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/owl.theme.default.min.css">
    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/magnific-popup.css">

    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/aos.css">

    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/ionicons.min.css">

    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/bootstrap-datepicker.css">
    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/jquery.timepicker.css">

    <meta name="nextpay-site-verification" content="1fda66d960ce3096d53317fb37ae2763"/>
    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/flaticon.css">
    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/icomoon.css">
    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/style.css">
    <link rel="stylesheet" href="/iadminbest/public/css/landing_core/mystyle.css">
    <meta name="clckd" content="51dc26d2748b32349ec72f033b9a143b" />

    <link href="https://fonts.googleapis.com/css?family=Poiret+One" rel="stylesheet">
    <script src="https://kit.fontawesome.com/289e5f1e22.js" crossorigin="anonymous"></script>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(56723590, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/56723590" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <a href="//freekassa.ru/"><img src="//www.free-kassa.ru/img/fk_btn/9.png" title="Приём оплаты на сайте картами"></a>
    <!-- /Yandex.Metrika counter -->
    <script data-ad-client="ca-pub-7640044855853499" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="/">iAdmin.best</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> {{__('main.menu')}}
        </button>


        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="index.html" class="nav-link">{{__('main.home')}}</a></li>
                <li class="nav-item"><a href="#about" class="nav-link">{{__('main.about')}}</a></li>
                {{--<li class="nav-item dropdown">--}}
                {{--<a class="nav-link dropdown-toggle" href="portfolio.html" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Portfolio</a>--}}
                {{--<div class="dropdown-menu" aria-labelledby="dropdown04">--}}
                {{--<a class="dropdown-item" href="portfolio.html">Portfolio</a>--}}
                {{--<a class="dropdown-item" href="portfolio-single.html">Portfolio Single</a>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li class="nav-item"><a href="blog.html" class="nav-link">Case Studies</a></li>--}}
                {{--<li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li>--}}
                <li class="nav-item cta"><a href="{{route('register')}}" class="nav-link"><span>{{__('main.registration')}}</span></a></li>
                <li class="nav-item cta"><a href="{{route('login')}}" class="nav-link"><span>{{__('main.login')}}</span></a></li>
                <a href="/?lang=ru" style="padding-top: 10px;"><img src="https://st.depositphotos.com/3538103/5128/i/450/depositphotos_51280113-stock-photo-national-flag-of-the-russian.jpg" width="30px" height="20px" alt=""></a>
                <a href="/?lang=en" style="padding-top: 10px; margin-left: 10px;"><img src="https://st.depositphotos.com/1141341/2558/i/450/depositphotos_25587499-stock-photo-flag-of-uk.jpg" width="30px" height="20px" alt=""></a>
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->

<!-- <div class="js-fullheight"> -->
<div class="hero-wrap js-fullheight">
    <div class="overlay"></div>
    <div id="particles-js"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center" data-scrollax-parent="true">
            <div class="col-md-6 text-center" data-scrollax=" properties: { translateY: '70%' }">
                <h1 class="mb-4 wow fadeInDown" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }" style="font-family: 'Poiret One', cursive;">{!! __('main.home_header') !!}</h1>
                <p><a href="{{route('register')}}" class="btn btn-primary btn-outline-white fz20 px-5 py-3">{{__('main.registration')}}</a></p>
            </div>
        </div>
    </div>
</div>

{{--<div class="bg-light">--}}
{{--<section class="ftco-section-featured ">--}}
{{--<div class="container-fluid" data-scrollax-parent="true">--}}
{{--<div class="row no-gutters d-flex align-items-center" data-scrollax=" properties: { translateY: '-30%'}">--}}

{{--<div class="col-md-3 mb-3">--}}
{{--<a href="#" class="featured-img">--}}
{{--<div class="text-1 p-4 d-flex align-items-center">--}}
{{--<h3>The Verge<br><span class="tag">Website</span></h3>--}}
{{--</div>--}}
{{--<img src="/hyipbuilder/public/images/image_1.jpg" class="img-fluid" alt="">--}}
{{--<div class="text p-4 d-flex align-items-center">--}}
{{--<div class="user d-flex align-items-center">--}}
{{--<div class="user-img mr-3" style="background-image: url(/hyipbuilder/public/images/person_1.jpg);"></div>--}}
{{--<h3>John Bruce <br><span class="position">Designer</span></h3>--}}
{{--</div>--}}
{{--</div>--}}
{{--</a>--}}
{{--</div>--}}

{{--<div class="col-md-6">--}}
{{--<div class="row no-gutters">--}}
{{--<div class="col-md-12">--}}
{{--<div class="row no-gutters d-flex align-items-end">--}}

{{--<div class="col-md-8">--}}
{{--<a href="#" class="featured-img">--}}
{{--<div class="text-1 p-4 d-flex align-items-center">--}}
{{--<h3>Racks<br><span class="tag">Website</span></h3>--}}
{{--</div>--}}
{{--<img src="/hyipbuilder/public/images/image_5.jpg" class="img-fluid" alt="">--}}
{{--<div class="text p-4 d-flex align-items-center">--}}
{{--<div class="user d-flex align-items-center">--}}
{{--<div class="user-img mr-3" style="background-image: url(/hyipbuilder/public/images/person_1.jpg);"></div>--}}
{{--<h3>John Bruce <br><span class="position">Designer</span></h3>--}}
{{--</div>--}}
{{--</div>--}}
{{--</a>--}}
{{--</div>--}}

{{--<div class="col-md-4">--}}
{{--<a href="#" class="featured-img">--}}
{{--<div class="text-1 p-4 d-flex align-items-center">--}}
{{--<h3>Zendesk<br><span class="tag">Website</span></h3>--}}
{{--</div>--}}
{{--<img src="/hyipbuilder/public/images/image_4.jpg" class="img-fluid" alt="">--}}
{{--<div class="text p-4 d-flex align-items-center">--}}
{{--<div class="user d-flex align-items-center">--}}
{{--<div class="user-img mr-3" style="background-image: url(/hyipbuilder/public/images/person_1.jpg);"></div>--}}
{{--<h3>John Bruce <br><span class="position">Designer</span></h3>--}}
{{--</div>--}}
{{--</div>--}}
{{--</a>--}}
{{--</div>--}}

{{--</div>--}}
{{--</div>--}}

{{--<div class="col-md-12">--}}
{{--<div class="row no-gutters d-flex align-items-start">--}}
{{--<div class="col-md-8">--}}
{{--<a href="#" class="featured-img">--}}
{{--<div class="text-1 p-4 d-flex align-items-center">--}}
{{--<h3>Curator<br><span class="tag">Website</span></h3>--}}
{{--</div>--}}
{{--<img src="/hyipbuilder/public/images/image_6.jpg" class="img-fluid" alt="">--}}
{{--<div class="text p-4 d-flex align-items-center">--}}
{{--<div class="user d-flex align-items-center">--}}
{{--<div class="user-img mr-3" style="background-image: url(/hyipbuilder/public/images/person_1.jpg);"></div>--}}
{{--<h3>John Bruce <br><span class="position">Designer</span></h3>--}}
{{--</div>--}}
{{--</div>--}}
{{--</a>--}}
{{--</div>--}}
{{--<div class="col-md-4">--}}
{{--<a href="#" class="featured-img">--}}
{{--<div class="text-1 p-4 d-flex align-items-center">--}}
{{--<h3>Tasty<br><span class="tag">Website</span></h3>--}}
{{--</div>--}}
{{--<img src="/hyipbuilder/public/images/image_3.jpg" class="img-fluid" alt="">--}}
{{--<div class="text p-4 d-flex align-items-center">--}}
{{--<div class="user d-flex align-items-center">--}}
{{--<div class="user-img mr-3" style="background-image: url(/hyipbuilder/public/images/person_1.jpg);"></div>--}}
{{--<h3>John Bruce <br><span class="position">Designer</span></h3>--}}
{{--</div>--}}
{{--</div>--}}
{{--</a>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="col-md-3">--}}
{{--<a href="#" class="featured-img">--}}
{{--<div class="text-1 p-4 d-flex align-items-center">--}}
{{--<h3>Voyage<br><span class="tag">Website</span></h3>--}}
{{--</div>--}}
{{--<img src="/hyipbuilder/public/images/image_2.jpg" class="img-fluid" alt="">--}}
{{--<div class="text p-4 d-flex align-items-center">--}}
{{--<div class="user d-flex align-items-center">--}}
{{--<div class="user-img mr-3" style="background-image: url(/hyipbuilder/public/images/person_1.jpg);"></div>--}}
{{--<h3>John Bruce <br><span class="position">Designer</span></h3>--}}
{{--</div>--}}
{{--</div>--}}
{{--</a>--}}
{{--</div>--}}

{{--</div>--}}
{{--<div class="row mt-5 d-flex justify-content-center">--}}
{{--<div class="col-md-8 text-center heading-section ">--}}
{{--<h2 class="h1">UI/UX, visual, <strong class="px-3">Web designer</strong> with more than 12 years of experience in designing websites and mobile apps.</h2>--}}
{{--<p><a href="#" class="btn btn-primary mt-3 py-3 px-5">Get in touch</a></p>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</section>--}}
{{--</div>--}}


<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-6 text-center heading-section wow fadeInUp">
                {{--<span class="subheading">Our Services</span>--}}
                <h2 class="mb-4" id="about">{!! __('main.instrument') !!}</h2>
                <p>{!! __('main.desc') !!}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4 d-flex align-self-stretch wow fadeInLeft">
                <div class="media block-6 services d-block text-center">
                    <div class="d-flex justify-content-center"><div class="icon color-3 d-flex justify-content-center mb-3"><span class="fa fa-users"></span></div></div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">{!! __('main.list_h_1') !!}</h3>
                        <p>{!! __('main.list_desc_1') !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 d-flex align-self-stretch wow fadeInLeft">
                <div class="media block-6 services d-block text-center">
                    <div class="d-flex justify-content-center"><div class="icon color-1 d-flex justify-content-center mb-3"><span class="fa fa-users"></span></div></div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">{!! __('main.list_h_2') !!}</h3>
                        <p>{!! __('main.list_desc_2') !!}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-4 d-flex align-self-stretch wow fadeInRight">
                <div class="media block-6 services d-block text-center">
                    <div class="d-flex justify-content-center"><div class="icon color-2 d-flex justify-content-center mb-3"><span class="fa fa-users"></span></div></div>
                    <div class="media-body p-2 mt-3">
                        <h3 class="heading">{!! __('main.list_h_3') !!}</h3>
                        <p>{!! __('main.list_desc_3') !!}</p>
                    </div>
                </div>
            </div>
            <p style="font-size: 14px;">{!! __('main.list_add_info') !!}</p>

            {{--<div class="col-md-6 col-lg-3 d-flex align-self-stretch wow fadeInRight">--}}
            {{--<div class="media block-6 services d-block text-center">--}}
            {{--<div class="d-flex justify-content-center"><div class="icon color-4 d-flex justify-content-center mb-3"><span class="align-self-center icon-live_help"></span></div></div>--}}
            {{--<div class="media-body p-2 mt-3">--}}
            {{--<h3 class="heading">Help &amp; Supports</h3>--}}
            {{--<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
</section>

<section class="ftco-section-parallax">
    <div class="parallax-img d-flex align-items-center">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-7 text-center heading-section heading-section-white ">
                    <h2>{!! __('main.ready') !!}</h2>
                    <p>{!! __('main.ready_desc') !!}</p>
                    <div class="row d-flex justify-content-center mt-5">
                        <div class="col-md-6">
                            <form action="#" class="subscribe-form">
                                <div class="form-group">
                                    <span class="icon fas fa-arrow-right"></span>
                                    <input type="text" class="form-control" placeholder="mysite.iadmin.best">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{{--<section class="ftco-section">--}}
{{--<div class="container">--}}
{{--<div class="row no-gutters justify-content-center mb-5 pb-5">--}}
{{--<div class="col-md-7 text-center heading-section ">--}}
{{--<span class="subheading">Works</span>--}}
{{--<h2 class="mb-4">View our works below to see our design and way of development.</h2>--}}
{{--<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row">--}}
{{--<div class="block-3 d-md-flex " data-scrollax-parent="true">--}}
{{--<a href="portfolio.html" class="image" style="background-image: url('/hyipbuilder/public/images/work-1.jpg'); " data-scrollax=" properties: { translateY: '-20%'}">--}}
{{--</a>--}}
{{--<div class="text">--}}
{{--<h4 class="subheading">Illustration</h4>--}}
{{--<h2 class="heading"><a href="portfolio.html">Even the all-powerful Pointing has no control</a></h2>--}}
{{--<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>--}}
{{--<p><a href="portfolio.html" class="btn btn-primary px-4">View Portfolio</a></p>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="block-3 d-md-flex " data-scrollax-parent="true">--}}
{{--<a href="portfolio.html" class="image image-2 order-2" style="background-image: url('/hyipbuilder/public/images/work-2.jpg');" data-scrollax=" properties: { translateY: '-20%'}"></a>--}}
{{--<div class="text order-1">--}}
{{--<h4 class="subheading">Application</h4>--}}
{{--<h2 class="heading"><a href="portfolio.html">Even the all-powerful Pointing has no control</a></h2>--}}
{{--<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>--}}
{{--<p><a href="portfolio.html" class="btn btn-primary px-4">View Portfolio</a></p>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="block-3 d-md-flex " data-scrollax-parent="true">--}}
{{--<a href="portfolio.html" class="image" style="background-image: url('/hyipbuilder/public/images/work-3.jpg'); " data-scrollax=" properties: { translateY: '-20%'}"></a>--}}
{{--<div class="text">--}}
{{--<h4 class="subheading">Web Design</h4>--}}
{{--<h2 class="heading"><a href="portfolio.html">Even the all-powerful Pointing has no control</a></h2>--}}
{{--<p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar.</p>--}}
{{--<p><a href="portfolio.html" class="btn btn-primary px-4">View Portfolio</a></p>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row">--}}
{{--<div class="col-md-12 text-center">--}}
{{--<span><a href="#" class="btn btn-primary py-3 px-5">View All Projects</a></span>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</section>--}}

{{--<section class="ftco-section testimony-section bg-light">--}}
{{--<div class="container">--}}
{{--<div class="row justify-content-center mb-5 pb-5">--}}
{{--<div class="col-md-7 text-center heading-section ">--}}
{{--<span class="subheading">Customer Says</span>--}}
{{--<h2 class="mb-4">Our satisfied customer says</h2>--}}
{{--<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row ">--}}
{{--<div class="col-md-12">--}}
{{--<div class="carousel-testimony owl-carousel ftco-owl">--}}
{{--<div class="item text-center">--}}
{{--<div class="testimony-wrap p-4 pb-5">--}}
{{--<div class="user-img mb-4" style="background-image: url(/hyipbuilder/public/images/person_1.jpg)">--}}
{{--<span class="quote d-flex align-items-center justify-content-center">--}}
{{--<i class="icon-quote-left"></i>--}}
{{--</span>--}}
{{--</div>--}}
{{--<div class="text">--}}
{{--<p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>--}}
{{--<p class="name">Dennis Green</p>--}}
{{--<span class="position">Marketing Manager</span>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="item text-center">--}}
{{--<div class="testimony-wrap p-4 pb-5">--}}
{{--<div class="user-img mb-4" style="background-image: url(/hyipbuilder/public/images/person_2.jpg)">--}}
{{--<span class="quote d-flex align-items-center justify-content-center">--}}
{{--<i class="icon-quote-left"></i>--}}
{{--</span>--}}
{{--</div>--}}
{{--<div class="text">--}}
{{--<p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>--}}
{{--<p class="name">Dennis Green</p>--}}
{{--<span class="position">Interface Designer</span>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="item text-center">--}}
{{--<div class="testimony-wrap p-4 pb-5">--}}
{{--<div class="user-img mb-4" style="background-image: url(/hyipbuilder/public/images/person_3.jpg)">--}}
{{--<span class="quote d-flex align-items-center justify-content-center">--}}
{{--<i class="icon-quote-left"></i>--}}
{{--</span>--}}
{{--</div>--}}
{{--<div class="text">--}}
{{--<p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>--}}
{{--<p class="name">Dennis Green</p>--}}
{{--<span class="position">UI Designer</span>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="item text-center">--}}
{{--<div class="testimony-wrap p-4 pb-5">--}}
{{--<div class="user-img mb-4" style="background-image: url(/hyipbuilder/public/images/person_1.jpg)">--}}
{{--<span class="quote d-flex align-items-center justify-content-center">--}}
{{--<i class="icon-quote-left"></i>--}}
{{--</span>--}}
{{--</div>--}}
{{--<div class="text">--}}
{{--<p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>--}}
{{--<p class="name">Dennis Green</p>--}}
{{--<span class="position">Web Developer</span>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="item text-center">--}}
{{--<div class="testimony-wrap p-4 pb-5">--}}
{{--<div class="user-img mb-4" style="background-image: url(/hyipbuilder/public/images/person_1.jpg)">--}}
{{--<span class="quote d-flex align-items-center justify-content-center">--}}
{{--<i class="icon-quote-left"></i>--}}
{{--</span>--}}
{{--</div>--}}
{{--<div class="text">--}}
{{--<p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>--}}
{{--<p class="name">Dennis Green</p>--}}
{{--<span class="position">System Analytics</span>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</section>--}}

{{--<section class="ftco-section ftco-counter" id="section-counter">--}}
{{--<div class="container">--}}
{{--<div class="row justify-content-center mb-5 pb-5">--}}
{{--<div class="col-md-7 text-center heading-section heading-section-white ">--}}
{{--<h2>Our achievements</h2>--}}
{{--<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row">--}}
{{--<div class="col-md-6 col-lg-4 d-flex justify-content-center counter-wrap ">--}}
{{--<div class="block-18 text-center">--}}
{{--<div class="text">--}}
{{--<strong class="number" data-number="400">0</strong>--}}
{{--<span>Customers are satisfied with our professional support</span>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="col-md-6 col-lg-4 d-flex justify-content-center counter-wrap ">--}}
{{--<div class="block-18 text-center">--}}
{{--<div class="text">--}}
{{--<strong class="number" data-number="1000">0</strong>--}}
{{--<span>Amazing preset options to be mixed and combined</span>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="col-md-6 col-lg-4 d-flex justify-content-center counter-wrap ">--}}
{{--<div class="block-18 text-center">--}}
{{--<div class="text">--}}
{{--<strong class="number" data-number="8000">0</strong>--}}
{{--<span>Average response time on live chat support channel</span>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</section>--}}

{{--<section class="ftco-section bg-light">--}}
{{--<div class="container">--}}
{{--<div class="row justify-content-center mb-5 pb-5">--}}
{{--<div class="col-md-7 text-center heading-section ">--}}
{{--<span class="subheading">Blog</span>--}}
{{--<h2>Recent Blog</h2>--}}
{{--<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="row">--}}
{{--<div class="col-md-4 ">--}}
{{--<div class="blog-entry">--}}
{{--<a href="blog-single.html" class="block-20" style="background-image: url('/hyipbuilder/public/images/image_1.jpg');">--}}
{{--</a>--}}
{{--<div class="text p-4 d-block">--}}
{{--<div class="meta mb-3">--}}
{{--<div><a href="#">July 12, 2018</a></div>--}}
{{--<div><a href="#">Admin</a></div>--}}
{{--<div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>--}}
{{--</div>--}}
{{--<h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="col-md-4 ">--}}
{{--<div class="blog-entry" data-aos-delay="100">--}}
{{--<a href="blog-single.html" class="block-20" style="background-image: url('/hyipbuilder/public/images/image_2.jpg');">--}}
{{--</a>--}}
{{--<div class="text p-4">--}}
{{--<div class="meta mb-3">--}}
{{--<div><a href="#">July 12, 2018</a></div>--}}
{{--<div><a href="#">Admin</a></div>--}}
{{--<div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>--}}
{{--</div>--}}
{{--<h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="col-md-4 ">--}}
{{--<div class="blog-entry" data-aos-delay="200">--}}
{{--<a href="blog-single.html" class="block-20" style="background-image: url('/hyipbuilder/public/images/image_3.jpg');">--}}
{{--</a>--}}
{{--<div class="text p-4">--}}
{{--<div class="meta mb-3">--}}
{{--<div><a href="#">July 12, 2018</a></div>--}}
{{--<div><a href="#">Admin</a></div>--}}
{{--<div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>--}}
{{--</div>--}}
{{--<h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</section>--}}
<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-12 text-center heading-section wow fadeInUp">
                {{--<span class="subheading">Our Services</span>--}}
                {!! __('main.features') !!}
            </div>
        </div>
    </div>
</section>

<section class="ftco-section-parallax">
    <div class="parallax-img align-items-center">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-7 text-center heading-section heading-section-white ">
                    <h2>{!! __('main.price') !!}</h2>
                    <p>{!! __('main.price_desc') !!}</p>
                    <div class="row d-flex justify-content-center mt-5">
                        <div class="col-md-6">
                            <form action="#" class="subscribe-form">
                                <div class="form-group">
                                    <span class="icon icon-paper-plane"></span>
                                    <input type="text" class="form-control" placeholder="mysite.iadmin.best">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="color: #fff; padding-top: 200px;">
            <div class="row mb-5">
                {!! __('main.footer') !!}
            </div>
        </div>
    </div>

</section>

{{--<footer class="ftco-footer ftco-bg-dark ftco-section">--}}
    {{--<div class="container">--}}
        {{--<div class="row mb-5">--}}
            {{--<div class="col-md">--}}
                {{--<div class="ftco-footer-widget mb-4">--}}
                    {{--<h2 class="ftco-heading-2">INVEST ME</h2>--}}
                    {{--<p>Конструктор инвестиционных проектов.</p>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md">--}}
                {{--<div class="ftco-footer-widget mb-4 ml-5">--}}
                    {{--<h2 class="ftco-heading-2">Страницы</h2>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li><a href="/" class="py-2 d-block">Главная</a></li>--}}
                        {{--<li><a href="{{route('login')}}" class="py-2 d-block">Войти</a></li>--}}
                        {{--<li><a href="{{route('register')}}" class="py-2 d-block">Регистрация</a></li>--}}
                        {{--<li><a href="#" class="py-2 d-block">Portfolio</a></li>--}}
                        {{--<li><a href="#" class="py-2 d-block">About</a></li>--}}
                        {{--<li><a href="#" class="py-2 d-block">Contact</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md">--}}
                {{--<div class="ftco-footer-widget mb-4">--}}
                    {{--<h2 class="ftco-heading-2">E-mail</h2>--}}
                    {{--<ul class="list-unstyled">--}}
                        {{--<li><a href="#" class="py-2 d-block">support@investme.click</a></li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-md">--}}
                {{--<div class="ftco-footer-widget mb-4">--}}
                {{--<ul class="ftco-footer-social list-unstyled float-md-left float-lft">--}}
                {{--<li class=""><a href="#"><span class="icon-twitter"></span></a></li>--}}
                {{--<li class=""><a href="#"><span class="icon-facebook"></span></a></li>--}}
                {{--<li class=""><a href="#"><span class="icon-instagram"></span></a></li>--}}
                {{--</ul>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
        {{--<div class="col-md-12 text-center">--}}

        {{--<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->--}}
        {{--Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>--}}
        {{--<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>--}}
        {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<a href="//www.free-kassa.ru/"><img src="//www.free-kassa.ru/img/fk_btn/9.png" title="Прием платежей на сайте"></a>--}}
    {{--<a href="https://www.fkwallet.ru"><img src="https://www.fkwallet.ru/assets/2017/images/btns/icon_wallet1.png" title="Обмен криптовалют"></a>--}}
{{--</footer>--}}



<!-- loader -->
{{--<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>--}}


<script src="/iadminbest/public/js/landing_core/jquery.min.js"></script>
<script src="/iadminbest/public/js/landing_core/jquery-migrate-3.0.1.min.js"></script>
<script src="/iadminbest/public/js/landing_core/popper.min.js"></script>
<script src="/iadminbest/public/js/landing_core/bootstrap.min.js"></script>
<script src="/iadminbest/public/js/landing_core/jquery.easing.1.3.js"></script>
<script src="/iadminbest/public/js/landing_core/jquery.waypoints.min.js"></script>
<script src="/iadminbest/public/js/landing_core/jquery.stellar.min.js"></script>
<script src="/iadminbest/public/js/landing_core/owl.carousel.min.js"></script>
<script src="/iadminbest/public/js/landing_core/jquery.magnific-popup.min.js"></script>
<script src="/iadminbest/public/js/landing_core/aos.js"></script>
<script src="/iadminbest/public/js/landing_core/jquery.animateNumber.min.js"></script>
<script src="/iadminbest/public/js/landing_core/bootstrap-datepicker.js"></script>
<script src="/iadminbest/public/js/landing_core/jquery.timepicker.min.js"></script>
<script src="/iadminbest/public/js/landing_core/particles.min.js"></script>
<script src="/iadminbest/public/js/landing_core/particle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
{{--<script>--}}
    {{--new WOW().init();--}}
{{--</script>--}}
{{--<script src="http://127.0.0.1/hyipbuilder/public/js/landing_core/scrollax.min.js"></script>--}}
<script src="/iadminbest/public/js/landing_core/google-map.js"></script>
<script src="/iadminbest/public/js/landing_core/main.js"></script>

</body>
</html>