<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>MT - Money Tree</title>
    <!-- Latest compiled and minified CSS -->
    <!-- Favicon -->
    <link rel="icon" href="/moneytree/public/img/qr-mt-3.png">

    <!-- Core Stylesheet -->
    <link href="/moneytree/public/css/landing/style.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="/moneytree/public/css/landing/responsive.css" rel="stylesheet">

</head>
<body>
<!-- Preloader Start -->
<div id="preloader">
    <div class="colorlib-load"></div>
</div>

<!-- ***** Header Area Start ***** -->
<header class="header_area animated">
    <div class="container-fluid">
        <div class="row align-items-center">
            <!-- Menu Area Start -->
            <div class="col-12 col-lg-8">
                <div class="menu_area">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <!-- Logo -->
                        <a class="navbar-brand" href="/">MT</a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <!-- Menu Area -->
                        <div class="collapse navbar-collapse" id="ca-navbar">
                            <ul class="navbar-nav ml-auto" id="nav">
                                <li class="nav-item"><a class="nav-link" href="/">Главная</a></li>
                                <li class="nav-item"><a class="nav-link" href="/marketing">Маркетинг</a></li>
                            </ul>
                            <div class="sing-up-button d-lg-none">
                                <a href="/login">Вход</a>
                            </div>
                            <div class="sing-up-button d-lg-none">
                                <a href="/register">Регистрация</a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- Signup btn -->
            <div class="col-12 col-lg-2">
                <div class="sing-up-button d-none d-lg-block">
                    <a href="/login">Вход</a>
                </div>
            </div>
            <div class="col-12 col-lg-2">
                <div class="sing-up-button d-none d-lg-block">
                    <a href="/register">Регистрация</a>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->
<!-- ***** Wellcome Area Start ***** -->
<section class="wellcome_area clearfix" id="home">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12 col-md">
                <div class="wellcome-heading">
                    <div class="clearfix"></div>
                    <div class="row" style="text-align: center;">
                        {{--<h2 class="col-lg-6" style="text-align: center; color: #fff; margin-top: 100px;">Дерево с малыми плодами</h2>--}}
                        {{--<h2 class="col-lg-6" style="text-align: center; color: #fff; margin-top: 100px;">Дерево с большими плодами</h2>--}}
                        <div class="col-lg-9">
                            <div class="tarif tarif-1">
                                <h1 style="text-align: center; color: #fff;">Вишневый сад</h1>
                                <img src="/moneytree/public/img/landing/marketing/tarif-1.png" width="90%" alt="">
                            </div>
                            <div class="tarif tarif-2" style="display: none;">
                                <h1 style="text-align: center; color: #fff;">Вишневый сад</h1>
                                <img src="/moneytree/public/img/landing/marketing/tarif-2.png" width="90%" alt="">
                            </div>
                            <div class="tarif tarif-3" style="display: none;">
                                <h1 style="text-align: center; color: #fff;">Яблоневый сад</h1>
                                <img src="/moneytree/public/img/landing/marketing/tarif-3.png" width="90%" alt="">
                            </div>
                            <div class="tarif tarif-4" style="display: none;">
                                <h1 style="text-align: center; color: #fff;">Яблоневый сад</h1>
                                <img src="/moneytree/public/img/landing/marketing/tarif-4.png" width="90%" alt="">
                            </div>
                            <div class="tarif tarif-5" style="display: none;">
                                <h1 style="text-align: center; color: #fff;">Апельсиновый сад</h1>
                                <img src="/moneytree/public/img/landing/marketing/tarif-5.png" width="90%" alt="">
                            </div>
                            <div class="tarif tarif-6" style="display: none;">
                                <h1 style="text-align: center; color: #fff;">Апельсиновый сад</h1>
                                <img src="/moneytree/public/img/landing/marketing/tarif-6.png" width="90%" alt="">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <ul>
                                <li class="li-head">Вишневый сад</li>
                                <li class="li-item li-item-active" data-tab="tarif-1">Дерево с малыми плодами</li>
                                <li class="li-item" data-tab="tarif-2">Дерево с большими плодами</li>
                                <li class="li-head">Яблоневый сад</li>
                                <li class="li-item" data-tab="tarif-3">Дерево с малыми плодами</li>
                                <li class="li-item" data-tab="tarif-4">Дерево с большими плодами</li>
                                <li class="li-head">Апельсиновый сад</li>
                                <li class="li-item" data-tab="tarif-5">Дерево с малыми плодами</li>
                                <li class="li-item" data-tab="tarif-6">Дерево с большими плодами</li>
                            </ul>
                        </div>
                            {{--<img src="/moneytree/public/img/landing/marketing/tarif-2.png" alt="">--}}
                        <style>
                            .li-head {
                                color: #fff; font-size: 22px; padding: 10px;
                            }
                            .li-item {
                                border: 4px solid #fff; padding: 10px 0px; text-align: center; font-size: 18px; border-radius: 9px; color: #fff; margin-top: 6px;
                                transition: all .3s linear;
                            }
                            .li-item:hover, .li-item-active {
                                background: #fff; color: #6953e7; cursor: pointer;
                            }
                        </style>
                    </div>

                </div>
                <div class="get-start-area">
                    <!-- Form Start -->
                    <form action="#" method="post" class="form-inline">
                        {{--<input type="email" class="form-control email" placeholder="name@company.com">--}}
                        {{--<input type="submit" class="submit" value="Регистрация" style="cursor: pointer">--}}
                    </form>
                    <!-- Form End -->
                </div>
            </div>
        </div>
    </div>
    <!-- Welcome thumb -->
    <div class="welcome-thumb wow fadeInDown" data-wow-delay="0.5s">
        {{--<img src="/moneytree/public/img/landing/marketing/tarif-1.png" alt="">--}}
    </div>
</section>
<!-- ***** Wellcome Area End ***** -->


<!-- ***** Footer Area Start ***** -->
<footer class="footer-social-icon text-center section_padding_70 clearfix">
    <!-- footer logo -->
    <div class="footer-text">
        <h2>MT</h2>
    </div>
    <!-- social icon-->
    <div class="footer-social-icon">
        {{--<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>--}}
        {{--<a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>--}}
        {{--<a href="#"> <i class="fa fa-instagram" aria-hidden="true"></i></a>--}}
        {{--<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>--}}
    </div>
{{--<div class="footer-menu">--}}
{{--<nav>--}}
{{--<ul>--}}
{{--<li><a href="#">About</a></li>--}}
{{--<li><a href="#">Terms &amp; Conditions</a></li>--}}w
{{--<li><a href="#">Privacy Policy</a></li>--}}
{{--<li><a href="#">Contact</a></li>--}}
{{--</ul>--}}
{{--</nav>--}}
{{--</div>--}}
<!-- Foooter Text-->
    <div class="copyright-text">
        <!-- ***** Removing this text is now allowed! This template is licensed under CC BY 3.0 ***** -->
        support@moneytree.ru
        <p>Copyright ©2019 MT - Money Tree <a href="https://moneytree.best" target="_blank">Money Tree</a></p>
    </div>
</footer>
<!-- ***** Footer Area Start ***** -->

<!-- Jquery-2.2.4 JS -->
<script src="/moneytree/public/js/landing/jquery-2.2.4.min.js"></script>
<!-- Popper js -->
<script src="/moneytree/public/js/landing/popper.min.js"></script>
<!-- Bootstrap-4 Beta JS -->
<script src="/moneytree/public/js/landing/bootstrap.min.js"></script>
<!-- All Plugins JS -->
<script src="/moneytree/public/js/landing/plugins.js"></script>
<!-- Slick Slider Js-->
<script src="/moneytree/public/js/landing/slick.min.js"></script>
<!-- Footer Reveal JS -->
<script src="/moneytree/public/js/landing/footer-reveal.min.js"></script>
<!-- Active JS -->
<script src="/moneytree/public/js/landing/active.js"></script>
<script>
    $('.li-item').on('click', function () {
        console.log(123);
        let liNow = $('.li-item-active'),
            liNowData = liNow.data('tab'),
            liResult = $(this).data('tab');

        liNow.removeClass('li-item-active');
        $(this).addClass('li-item-active');
        $('.'+liNowData).hide(1000);
        $('.'+liResult).show(1000);
    });
</script>
</body>

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title -->
    <title>Colorlib App - App Landing Page</title>
    <!-- Latest compiled and minified CSS -->
    <!-- Favicon -->
    <link rel="icon" href="/moneytree/public/img/landing/core-img/favicon.ico">

    <!-- Core Stylesheet -->
    <link href="/moneytree/public/css/landing/style.css" rel="stylesheet">

    <!-- Responsive CSS -->
    <link href="/moneytree/public/css/landing/responsive.css" rel="stylesheet">

</head>

</html>
