@extends("layout.core.app")

@section('content')
    <!-- Small boxes (Stat box) -->
    {{--<div class="overlay" style="background: rgba(255,255,255,0.99)">--}}
    {{--<i class="fa fa-refresh fa-spin"></i>--}}
    {{--</div>--}}
    <div class="row">
        @if (0)
            <div class="col-lg-12">
                <div class="alert alert-{{$ad['type']}}">
                    <h4><i class="icon fa fa-info"></i> {{$ad['head']}}</h4>
                    {{$ad['text']}}
                </div>
            </div>
            <div class="clearfix"></div>
        @endif
        <div class="clearfix"></div>


        <div class="col-lg-12 col-md-12">
            <div class="box p-10">
                <div class="box-header">
                    <h3>{{__('main.my_sites')}}</h3>
                </div>
                <div class="box-body no-padding">
                    <table class="table table-condensed">
                        <tbody>
                        <tr>
                            {!! __('main.sites_th') !!}
                        </tr>

                        @forelse($siteList as $site)
                            <tr>
                                <td class="ta-center">{{$site['id']}}</td>
                                <td class="ta-center">
                                    {{--{{stristr($site['subdomain'], '_')}}--}}
                                    <img src="/iadminbest/storage/app/{{\App\Models\CoreSiteList::siteInfo($site['id'])->logo}}" alt="" width="25px"> {{\App\Models\CoreSiteList::siteInfo($site['id'])->name}}
                                </td>
                                <td class="ta-center"><a href="https://{{preg_replace('/_iadminbest/', '', $site['subdomain'])}}.iadmin.work">https://{{preg_replace('/_iadminbest/', '', $site['subdomain'])}}.iadmin.work</a></td>
                                <td class="ta-center">
                                    {{$site->create_date}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td></td>
                                <td>{!! __('main.no_sites') !!}</td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>

                    <h3 class="ta-center">{!! __('main.create_site') !!}</h3>

                    <form action="{{route('createSite')}}" method="post" class="js-createSite">
                        @csrf
                        <div class="input-group p-10" style="width: 100%; margin: auto;">
                            {{--<div class="col-md-4 pl-0">--}}
                            {{--</div>--}}
                            <div class="col-md-6 pr-0">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="domain" placeholder="mysite" autocomplete="off">
                                    <span class="input-group-addon">.iadmin.work</span>
                                </div>
                            </div>
                            <div class="col-md-2 pr-0">
                                <div class="input-group">
                                    <select name="site_type" class="form-control" id="">
                                        {!! __('main.select_site') !!}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 pr-0">
                                <div class="input-group">
                                    <select name="lang" class="form-control" id="">
                                        <option value="ru">Русский</option>
                                        <option value="en">English</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2 pr-0">
                                <div class="input-group">
                                    <select name="currency" class="form-control" id="">
                                        <option value="1">RUB</option>
                                        <option value="2">USD</option>
                                        <option value="3">EUR</option>
                                        <option value="4">BTC</option>
                                        <option value="5">ETH</option>
                                        <option value="6">LTC</option>
                                        <option value="7">XRP</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <input type="submit" value="{!! __('main.create_site') !!}" class="btn btn-primary create-site-btn">
                    </form>
                </div>
            </div>

        </div>
        <!-- /.row (main row) -->
@endsection
