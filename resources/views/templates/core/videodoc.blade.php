@extends("layout.core.app")

@section('title', 'Видео документация')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Видео инструкции по работе с конструктором</h3>
                    </div>

                    <div class="box-body">
                        <h3>Создание сайта и настройка внешнего вида</h3>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/QJieV0L0meU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <h3>Основные настройка сайта</h3>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/kobIftcbB-M" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <h3>Создание товаров и ресурсов</h3>
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/isSjJTUIoeg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
        </div>
    </div>
@endsection
