@extends("layout.core.app")

@section('title', 'Информация о тарифе')

@section('content')

    <div class="row">
        <div class="col-md-12">
                {{--{{dd($order)}}--}}
                @if (\App\Models\SiteInfo::find(1)->start == 0)
                <div class="box box-default">
                    <div class="box-header">
                        <h4>Проект еще не стартовал</h4>
                    </div>
                    <div class="box-body">

                    </div>
                </div>
                @else
                <div class="box box-default">
                    <div class="box-header">
                        <h4>Выберите тариф для активации</h4>
                    </div>


                    <div class="box-body">
                    @foreach($marks as $item)
                        <div class="col-lg-4">
                            <a href="javascript:activate({{$item->id}});" class="btn btn-primary"
                               style="display: block; width: 350px; height: 350px; line-height: 350px; border-radius: 350px; margin: auto; font-size: 20px;">{{$item->name}}</a>
                        </div>
                    @endforeach
                    </div>

                </div>
                @endif
        </div>

    </div>
@endsection