@extends("layout.app")

@section('title', 'Создать тикет')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Редактировать объявление</h3>
                    </div>

                    <div class="box-body">
                        <form action="{{route('editAd')}}" role="form" method="post" class="js-adEdit">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <div>
                                    <label>
                                        <input type="radio" name="status" id="status1" value="0" {{($ad['state'] == 0)?'checked':''}}> Выключено
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="status" value="1" id="status2" {{($ad['state'] == 1)?'checked':''}}> Включено
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <label>
                                        <input type="radio" name="type" value="1" {{($ad['type'] == 1)?'checked':''}}> Успех
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="type" value="2" {{($ad['type'] == 2)?'checked':''}}> Ошибка
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="type" value="3" {{($ad['type'] == 3)?'checked':''}}> Предупреждение
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="type" value="4" {{($ad['type'] == 4)?'checked':''}}> Информация
                                    </label>
                                </div>
                            </div>
                        <!-- text input -->
                            <div class="form-group">
                                <label>Заголовок объявления</label>
                                <input type="text" class="form-control" placeholder="Заголовок" name="name" value="{{$ad['head']}}">
                            </div>

                            <!-- textarea -->
                            <div class="form-group">
                                <label>Описание</label>
                                <textarea class="form-control" rows="3" placeholder="Суть объявления" name="text">{{$ad['text']}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </form>

                    </div>
                    <script>
                        // console.log($('.js-adEdit').length);
                        // import AdEdit from '/resources/assets/js/forms/AdEdit.js';
                        // new AdEdit();
                    </script>
                </div>
        </div>
    </div>
@endsection
