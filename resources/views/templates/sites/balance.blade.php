@extends("layout.app")

@section('title', 'Пополнить баланс')

@section('content')

    @include('templates.sites.pagebuilder.editPanel', array('page' => 'balance'))
    <div class="row">
        <div class="col-lg-12 profile-page page-content">
            <div class="col-lg-12 p-0 page-refresh for-grid">
                {{\App\Http\Controllers\ProfileController::pageConstruct('balance')}}
            </div>
        </div>

        @include('templates.sites.pagebuilder.modals', array('page' => 'balance'))
    </div>

@endsection
