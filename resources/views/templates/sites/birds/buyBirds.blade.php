@extends("layout.app")

@section('title', 'Магазин')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box col-lg-12">
                <div class="box-header">
                    <h3 class="box-title">Магазин</h3>
                </div>

                <a href="{{route('buyBirds')}}" class="btn btn-primary">Все</a>
                @foreach($typesOrder as $order)
                    <a href="{{route('buyBirds')}}?type={{$order['id']}}" class="btn btn-primary">{{$order['bird']}}</a>
                @endforeach

                <div class="box-body">
                    @foreach($birds as $bird)
                        <div class="col-md-6 col-lg-4 col-xs-12 col-sm-12 ta-center birds-list mb-10">
                            <div class="col-md-12 col-lg-12 p-0 for-equal-height">
                                <h2>{{$bird['name']}}</h2>
                                <div class="birds-list-body">
                                    <img src="/iadminbest/storage/app/{{$bird['image']}}" class="image-item ta-center mb-20">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td style="width: 40%">Цена</td>
                                                <td>
                                                    @if ($bird['range'])
                                                        от {{$bird['price']}}<img src="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['image_current']}}" alt="" width="20px">
                                                        до {{$bird['max_price']}}<img src="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['image_current']}}" alt="" width="20px">
                                                    @else
                                                        {{$bird['price']}}<img src="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['image_current']}}" alt="" width="20px">
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>У вас</td>
                                                <td>{{\App\Models\BirdsOrder::where('user_id', Auth::id())->where('bird_id', $bird['id'])->sum('number')}}</td>
                                            </tr>
                                                @if ($bird->end)
                                                    <tr>
                                                        <td>Срок жизни</td>
                                                        <td style="line-height: 9px;">{{round($bird['second']/3600, 3)}} час(а/ов)<br>или {{$bird['second']}} секунд(ы)</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Доход</td>
                                                        <td style="line-height: 9px;">{{$bird['profit']}}<img src="/iadminbest/storage/app/{{$bird->typesOrder->image}}" width="20px" alt="">
                                                            @if($bird->only_end)
                                                                <br>по истечению срока
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @else
                                                    <tr>
                                                        <td>Доход/час</td>
                                                        <td>{{round($bird['profit']/$bird['second']*3600, 3)}}<img src="/iadminbest/storage/app/{{$bird->typesOrder->image}}" width="20px" alt=""> в час</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Доход/сек</td>
                                                        <td>{{round($bird['profit']/$bird['second'], 4)}}<img src="/iadminbest/storage/app/{{$bird->typesOrder->image}}" width="20px" alt=""> в секунду</td>
                                                    </tr>
                                                @endif
                                        </tbody>
                                    </table>
                                    <form action="{{route('buyBirds')}}" role="form" method="post" class="js-buyBirds">
                                        @csrf
                                        <input type="hidden" name="bird_id" value="{{$bird['id']}}">
                                        @if ($bird['range'] == 1)
                                            <input type="number" name="sum" placeholder="Сумма" value="{{(int)$bird['max_price']}}">
                                        @endif
                                        <input type="submit" class="btn btn-primary" value="{{($bird['buy_btn'])?$bird['buy_btn']:'Сделать вклад'}}">
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection
