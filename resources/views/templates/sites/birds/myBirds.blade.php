@extends("layout.app")

@section('title', 'Мои владения')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box col-lg-12">
                    <div class="box-header">
                        <h3 class="box-title">Мои владения</h3>
                    </div>
                    <a href="{{route('myBirds')}}" class="btn btn-primary">Все</a>

                    @foreach($typesOrder as $order)
                        <a href="{{route('myBirds')}}?type={{$order['id']}}" class="btn btn-primary">{{$order['bird']}}</a>
                    @endforeach
                    <div class="box-body">
                        {{--@foreach($birdsInfo as $birdInfo)--}}
                            {{--<div class="col-md-6 col-lg-4 col-xs-12 col-sm-12 for-equal-height">--}}
                                {{--{{$birdInfo->typesOrder->end}}--}}
                                {{--<img class="ta-center" src="/hyipbuilder/storage/app/{{$birds[$birdInfo['id']]['image']}}" alt=""><br>--}}
                                {{--Тип: {{\App\Libraries\Helpers::getTypeOrder($birds[$birdInfo['id']]['type'], 'bird')}}<br>--}}
                                {{--Тип: {{$birdInfo->typesOrder->bird}}<br>--}}
                                {{--Имя: {{$birds[$birdInfo['id']]['name']}} <br>--}}
                                {{--Количество: {{$birds[$birdInfo['id']]['number']}} <br>--}}
                                {{--{{\App\Libraries\Helpers::getTypeOrder($birdInfo['type'], 'egg')}}: {{$birds[$birdInfo['id']]['egg']}}--}}
                                {{--<img src="/storage/app/{{\App\Models\TypesOrder::where('id', $birdInfo['type'])->first()['image']}}" width="20px" alt=""> <br>--}}

                                {{--<form action="{{route('collectEgg')}}" method="post" class="js-collectEgg">--}}
                                    {{--@csrf--}}
                                    {{--<input type="hidden" name="bird_id" value="{{$birds[$birdInfo['id']]['id']}}">--}}
                                    {{--<button type="submit" class="btn btn-primary">Собрать яйца</button>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}

                            @forelse($birds as $bird)
                                {{--<div class="col-md-6 col-lg-4 col-xs-12 col-sm-12 for-equal-height">--}}
                                    {{--<img class="ta-center image-item" src="/hyipbuilder/storage/app/{{$bird->bird->image}}" alt="" ><br>--}}
                                    {{--Тип: {{\App\Libraries\Helpers::getTypeOrder($birds[$birdInfo['id']]['type'], 'bird')}}<br>--}}
                                    {{--Тип: {{$bird->bird->typesOrder->bird}}<br>--}}
                                    {{--Имя: {{$bird->bird->name}} <br>--}}
                                    {{--Количество: {{$bird->number}} <br>--}}
                                    {{--{{$bird->bird->typesOrder->egg}}: <span class="egg-number" id="egg-number-{{$bird->id}}">{{$bird->egg}}</span>--}}
                                    {{--<img src="/hyipbuilder/storage/app/{{$bird->bird->typesOrder->image}}" width="20px" alt=""> <span class="for-reload"></span><br>--}}
                                    {{--@if ($bird->bird->typesOrder->end)--}}
                                        {{--<span class="timer-remind">{{$bird->remain_time}}</span>--}}
                                    {{--@endif--}}
                                {{--</div>--}}
                            <style>
                                @php
                                    $styleBg = '';
                                    $styleBorder = '';
                                    $styleText = ''
                                @endphp
                                @if ($bird->bird->typesOrder->color != '')
                                    @php
                                        $styleBg = 'background: '.$bird->bird->typesOrder->color.';';
                                        $styleBorder = 'border-color: '.$bird->bird->typesOrder->color.';';
                                        $styleText = 'color: '.$bird->bird->typesOrder->color_text.';'
                                    @endphp
                                @endif
                            </style>
                            <div class="col-md-6 col-lg-4 col-xs-12 col-sm-12 ta-center birds-list mb-10">
                                <div class="col-md-12 col-lg-12 p-0 for-equal-height" style="{{$styleBorder}}">
                                    <h2 style="{{$styleBg}} {{$styleText}}">{{$bird->bird->name}}</h2>
                                    <div class="birds-list-body">
                                        <img src="/hyipbuilder/storage/app/{{$bird->bird->image}}" class="image-item ta-center mb-20">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <td style="width: 40%">Тип</td>
                                                <td>{{$bird->bird->typesOrder->bird}}</td>
                                            </tr>
                                            <tr>
                                                <td style="width: 40%">Ресурс</td>
                                                <td>{{$bird->bird->typesOrder->egg}}</td>
                                            </tr>
                                            <tr>
                                                <td>Название</td>
                                                <td>{{$bird->bird->name}}</td>
                                            </tr>
                                            @if ($bird->bird->typesOrder->end)
                                                <tr>
                                                    <td>Осталось</td>
                                                    <td><span class="timer-remind">{{$bird->remain_time}}</span> сек</td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td>Количество</td>
                                                    <td>{{$bird->number}}</td>
                                                </tr>
                                            @endif
                                            <tr>
                                                <td>{{$bird->bird->typesOrder->egg}}</td>
                                                <td><span class="egg-number" id="egg-number-{{$bird->id}}">{{$bird->egg_round}}</span><img src="/hyipbuilder/storage/app/{{$bird->bird->typesOrder->image}}" width="20px" alt=""></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        <form action="{{route('collectEgg')}}" method="post" class="js-collectEgg">
                                            @csrf
                                            <input type="hidden" name="bird_id" value="{{$bird->id}}">
                                            @if ($bird->bird->typesOrder->name_btn == '')
                                                <button type="submit" class="btn btn-primary" style="{{$styleBg}} {{$styleText}} {{$styleBorder}}">Собрать "{{$bird->bird->typesOrder->egg}}"</button>
                                            @else
                                                <button type="submit" class="btn btn-primary" style="{{$styleBg}} {{$styleText}} {{$styleBorder}}">{{$bird->bird->typesOrder->name_btn}}</button>
                                            @endif
                                        </form>
                                    </div>
                                </div>
                            </div>
                            @empty
                                Вы еще ничего не приобретали из этой категории <a href="{{route('buyBirds')}}?type={{app('request')->input('type')}}" class="btn btn-primary">В магазин</a>
                            @endforelse
                    </div>
                </div>
        </div>
    </div>
@endsection
