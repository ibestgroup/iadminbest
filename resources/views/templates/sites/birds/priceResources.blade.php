@extends("layout.app")

@section('title', 'Информация о ресурсах')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box col-lg-12">
                <div class="box-header">
                    <h3 class="box-title">Ресурсы</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th style="width: 20px">Ресурс</th>
                            <th style="width: 135px">Изображение</th>
                            <th>1<img src="/hyipbuilder/storage/app/{{$siteInfo['image_current']}}" alt="" width="20px"> к ресурсу</th>
                            <th>1 рубль к ресурсу</th>
                            <th>1 ресурс к рублю</th>
                            <th>% на вывод</th>
                        </tr>
                        @foreach($typesOrder as $order)
                            <tr>
                                <td>{{$order['egg']}}</td>
                                <td><img src="/hyipbuilder/storage/app/{{$order['image']}}" alt="" width="50px"></td>
                                <td>{{$order['price']}}<img src="/hyipbuilder/storage/app/{{$order['image']}}" alt="" width="20px"></td>
                                <td>{{1*$order['price']*$siteInfo['price_current']}}<img src="/hyipbuilder/storage/app/{{$order['image']}}" alt="" width="20px"></td>
                                <td>{{1/$order['price']/$siteInfo['price_current']}}<i class="fa fa-rub" aria-hidden="true"></i></td>
                                <td>{{$order['percent_cashout']}}%</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
