@extends("layout.app")

@section('title', 'Склад')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box col-lg-12">
                <div class="box-header">
                    <h3 class="box-title">Склад</h3>
                </div>
                <div class="box-body">
                    {{--@foreach($birds as $bird)--}}
                        {{--<div class="col-md-6 col-lg-4 col-xs-12 col-sm-12 for-equal-height">--}}
                            {{--<h2>{{$bird['name']}}</h2>--}}
                            {{--<img src="/storage/app/{{$bird['image']}}" alt="">--}}
                            {{--<h4>Цена: {{$bird['price']}}</h4>--}}
                            {{--<h4>У вас: {{\App\Models\BirdsOrder::where('user_id', Auth::id())->where('bird_id', $bird['id'])->sum('number')}}</h4>--}}
                            {{--<h4>Доход: {{$bird['profit']}}/{{$bird['second']}}сек</h4>--}}
                            {{--<form action="{{route('buyBirds')}}" role="form" method="post" class="js-buyBirds">--}}
                                {{--@csrf--}}
                                {{--<input type="hidden" name="bird_id" value="{{$bird['id']}}">--}}
                                {{--<input type="submit" class="btn btn-primary" value="Купить">--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--@endforeach--}}
                    @foreach($typesOrder as $order)
                        {{--<div class="col-md-6 col-lg-4 col-xs-12 col-sm-12 for-equal-height">--}}
                            {{--Источник: {{$order->bird}}<br>--}}
                            {{--Товар: {{$order->egg}}<br>--}}
                            {{--<img src="/hyipbuilder/storage/app/{{$order->image}}" alt="" class="image-item ta-center"> <br>--}}
                            {{--Количество: {{$order->egg_number}}<br>--}}
                            {{--Из них на баланс для вывода: {{$order->egg_number_cashout}}<br>--}}
                            {{--В переводе на валюту: {{$order->egg_number/$order->price}}--}}

                        {{--</div>--}}
                        @php
                            $styleBg = '';
                            $styleBorder = '';
                            $styleText = ''
                        @endphp
                        @if ($order->color != '')
                            @php
                                $styleBg = 'background: '.$order->color.';';
                                $styleBorder = 'border-color: '.$order->color.';';
                                $styleText = 'color: '.$order->color_text.';'
                            @endphp
                        @endif
                        <div class="col-md-6 col-lg-4 col-xs-12 col-sm-12 ta-center birds-list mb-10">
                            <div class="col-md-12 col-lg-12 p-0 for-equal-height" style="{{$styleBorder}}">
                                <h2 style="{{$styleBg}} {{$styleText}}">{{$order->egg}}</h2>
                                <div class="birds-list-body">
                                    <img src="/hyipbuilder/storage/app/{{$order->image}}" class="image-item ta-center mb-20">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <td style="width: 40%">Товар</td>
                                            <td>{{$order->egg}}</td>
                                        </tr>
                                        <tr>
                                            <td>Источник</td>
                                            <td>{{$order->bird}}</td>
                                        </tr>
                                        <tr>
                                            <td>Количество</td>
                                            <td>{{$order->egg_number}} <img src="/hyipbuilder/storage/app/{{$order->image}}" width="20px"></td>
                                        </tr>
                                        <tr>
                                            <td>В переводе на валюту</td>
                                            <td style="line-height: 100%">{{round($order->egg_number/$order->price, 5)}}<img src="/hyipbuilder/storage/app/{{$siteInfo['image_current']}}" width="20px"></td>
                                        </tr>
                                        <tr>
                                            <td>Из них на баланс для вывода</td>
                                            <td style="line-height: 100%">{{round($order->egg_number_cashout/$order->price, 5)}} <img src="/hyipbuilder/storage/app/{{$siteInfo['image_current']}}" width="20px"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <form action="{{route('storeStock')}}" method="post" class="js-collectEgg">
                                        @csrf
                                        <input type="hidden" name="type_order" value="{{$order->id}}">
                                        <button type="submit" class="btn btn-primary" style="{{$styleBg}} {{$styleText}} {{$styleBorder}}">Продать всё</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
@endsection
