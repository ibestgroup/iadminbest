@extends("layout.app")

@section('title', 'Получить бонус')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Получить бонус</h3>
                    </div>

                    <div class="box-body">
                        <form action="{{route('getBonus')}}" role="form" method="post" class="js-getBonus">
                            @csrf
                            <button type="submit" class="btn btn-primary">Получить бонус</button>
                        </form>

                    </div>
                </div>
        </div>
    </div>
@endsection
