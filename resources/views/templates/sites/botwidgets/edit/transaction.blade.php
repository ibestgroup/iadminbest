<div class="row">
    <div class="col-md-12 col-lg-12">
        <h3>Настройки виджета</h3>
        <div class="form-check">
            <input type="checkbox" name="widget_settings[only_first_page]" value="1" class="form-check-input">
            <label class="form-check-label">Только первая страница</label>
        </div>
        <div class="col-sm-6 col-md-6 pl-0">
            <label>Количество транзакций</label>
            <input type="number" name="widget_settings[per_page]" class="form-control">
        </div>
        <div class="col-sm-6 col-md-6 pr-0">
            <label>Тип транзакциий</label>
            <select name="widget_settings[type]" id="" class="form-control">
                <option value="payment">Пополнения</option>
                <option value="cashout">Выводы</option>
                <option value="payments_order">Транзакции тарифов</option>
            </select>
        </div>
    </div>
</div>