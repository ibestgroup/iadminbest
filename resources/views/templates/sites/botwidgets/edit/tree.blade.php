<div class="row">
    <div class="col-md-12 col-lg-12">
        <h3>Настройки виджета</h3>
        <div class="form-check">
            <input type="checkbox" name="widget_settings[only_first_page]" value="1" class="form-check-input">
            <label class="form-check-label">Только первая страница</label>
        </div>
        <div class="col-sm-6 col-md-6 pl-0">
            <label>Количество тарифов</label>
            <input type="number" name="widget_settings[per_page]" class="form-control">
        </div>
    </div>
</div>