@extends("layout.app")

@section('title', 'Подключение аккаунта BUXone.click')

@section('content')
    <div class="row">
        {{--<div class="nav-tabs-custom">--}}
            {{--<ul class="nav nav-tabs">--}}
                {{--<li class="active"><a href="#auth" data-toggle="tab" aria-expanded="true">Элементы</a></li>--}}
                {{--<li><a href="#style-setting" data-toggle="tab" aria-expanded="true">Оформление</a></li>--}}
            {{--</ul>--}}
            {{--<div class="tab-content">--}}
                {{--<div class="tab-pane active" id="main-setting">--}}
                {{--</div>--}}
                {{--<div class="tab-pane" id="main-setting">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}


        @if (\App\Models\SyncBuxone::where('user_id', Auth::id())->count())
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Ваш аккаунт на iBux.best</h3>
                    </div>

                    <div class="box-body">
                        К Вашему профилю уже подключен аккаунт от <a href="https://ibux.best">iBux.best</a>
                        <h3>Ваш логин на iBux.best: {{$userBuxone['username']}}</h3>



                        <div class="col-lg-6" style="border: 1px solid #ff8d00; background: #ff8d000f; border-radius: 10px; padding: 10px;">
                            <h3>Перевести деньги на аккаунт</h3>
                            <form action="{{route('cashout')}}" role="form" method="post" class="js-store">
                                {{ csrf_field() }}
                                <input type="hidden" name="address" value="{{\App\Models\SyncBuxone::where('user_id', Auth::id())->first()->user_buxone}}">
                                <input type="hidden" name="system" value="221">
                                <input type="number" name="sum" id="cashOut-sum" class="form-control col-lg-6" style="width: 50%;">
                                <input type="submit" class="btn btn-primary col-lg-6" style="width: 50%;" value="Перевести средства">
                            </form>
                        </div>
                        <div class="clearfix"></div>
                        <form action="{{route('buxoneLogout')}}" role="form" method="post" class="js-buxone mt-10">
                            {{ csrf_field() }}
                            <input type="submit" class="btn btn-primary" value="Отключить профиль {{$userBuxone['username']}}">
                        </form>
                    </div>
                    <script>
                        // console.log($('.js-adEdit').length);
                        // import AdEdit from '/resources/assets/js/forms/AdEdit.js';
                        // new AdEdit();
                    </script>
                </div>
            </div>
        @else

        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="col-lg-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#auth-ibux" data-toggle="tab" aria-expanded="true">Регистрация</a></li>
                            <li><a href="#register-ibux" data-toggle="tab" aria-expanded="true">Привязка аккаунта</a></li>
                        </ul>
                        <div class="tab-content">

                            <div class="tab-pane active" id="auth-ibux">
                                <div class="box-body">
                                    <div class="box-header">
                                        <h3 class="box-title">Зарегистрировать новый аккаунт</h3>
                                    </div>
                                    <form action="{{route('buxoneRegister')}}" role="form" method="post" class="js-buxone">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="login-register">Логин</label>
                                            <input type="text" class="form-control" name="login" id="login-register" placeholder="Выберите логин" value="{{$resultLogin}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="pass-register">Пароль</label>
                                            <input type="password" class="form-control" name="pass" id="pass-register" placeholder="Пароль">
                                        </div>
                                        <div class="form-group">
                                            <label for="pass-register">Повторите пароль</label>
                                            <input type="password" class="form-control" name="re_pass" id="pass-register" placeholder="Повторите пароль">
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="Зарегистрироваться в один клик">

                                    </form>

                                </div>
                            </div>


                            <div class="tab-pane" id="register-ibux">
                                <div class="box-body">
                                    <div class="box-header">
                                        <h3 class="box-title">Авторизоваться</h3>
                                    </div>
                                    <form action="{{route('buxoneAuth')}}" role="form" method="post" class="js-buxone">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label for="login-auth">Логин</label>
                                            <input type="text" class="form-control" name="login" id="login-auth" placeholder="Логин">
                                        </div>
                                        <div class="form-group">
                                            <label for="password">Пароль</label>
                                            <input type="password" class="form-control" name="pass" id="password" placeholder="Пароль">
                                        </div>
                                        <input type="submit" class="btn btn-success" value="Привязать аккаунт">
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
@endsection
