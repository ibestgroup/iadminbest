@extends('layout.app')

@section('title', 'Вывод средств')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Вывод средств</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{route("cashout")}}" class="js-cashOut">
                    @csrf
                    <div class="box-body">
                        <div class="form-group" style="">
                            <label for="system">Платежная система</label>
                            <select name="system" class="form-control select2" id="system">
                                @foreach($paymentSystems as $system)
                                    <option value="{{$system->code}}" data-param='{{$system->toJson()}}')>
                                        {{$system->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        {{--<input type="hidden" name="system" value="114">--}}
                        <div class="form-group">
                            <label for="cashOut-sum">Сумма на вывод</label>
                            <input type="number" name="sum" class="form-control" id="cashOut-sum" placeholder="Сумма">
                        </div>
                        <div class="form-group">
                            <label for="address">Кошелек для вывода</label>
                            <input type="text" name="address" class="form-control" id="address" placeholder="Кошелек">
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Вывести</button>
                    </div>
                </form>
            </div>
        </div>

        {{--<div class="col-md-6" style="">--}}
            {{--<div class="box box-success">--}}
                {{--<div class="box-header">--}}
                    {{--<div class="box-header">--}}
                        {{--<h3 class="box-title">Параметры вывода</h3>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="box-body">--}}
                    {{--<div class="table-responsive js-cashOut-result">--}}
                        {{--<table class="table">--}}
                            {{--<tbody>--}}
                                {{--<tr>--}}
                                    {{--<th>Минимальная сумма для снятия:</th>--}}
                                    {{--<td class="js-cashOut-min-show"></td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<th>Комиссия MoneyTree (<span></span>):</th>--}}
                                    {{--<td class="js-cashOut-commission-show"></td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<th>Итого:</th>--}}
                                    {{--<td class="js-cashOut-result-show"></td>--}}
                                {{--</tr>--}}
                            {{--</tbody>--}}
                        {{--</table>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">История вывода средств</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>ID</th>
                            <th>Сумма</th>
                            <th>Платежная система</th>
                            <th>Статус</th>
                            <th>Дата</th>
                        </tr>
                        @foreach($cashouts as $cashout)
                            <tr>
                                <td>{{$cashout->id}}</td>
                                <td>{{$cashout->amount}} {!! \App\Libraries\Helpers::getCurrent(\App\Models\SiteInfo::find(1)['value'], 'icon') !!}</td>
                                <td>{{$cashout->payment_system}}</td>
                                <td><span class="label label-success" style="background: #0abebc!important;">Подтверждена</span></td>
                                <td>{{$cashout->date}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    {{$cashouts->links()}}
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection