@extends('layout.app')

@section('title', 'Вывод средств')

@section('content')
    <div class="row js-cashOutCrypto">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Вывод средств</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{route("cashout")}}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="cashOut-sum">Сколько</label>
                            <input name="sum" class="form-control" id="cashOut-sum" placeholder="Сумма">
                        </div>
                        @if ($siteInfo['value'] == 8)
                        Тэг назначения: <input type="text" class="form-control" name="dest_tag" placeholder="Тэг назначения" required=""><br>
                        @endif
                        <div class="form-group">
                            <label for="address">Кошелек для вывода</label>
                            <input type="text" name="address" class="form-control" id="address" placeholder="Кошелек">
                        </div>
                        <input type="submit" class="btn btn-primary form-control" style="margin-top: 10px;" name="cashout" value="Вывести">
                    </div>
                </form>
            </div>
        </div>
        {{--<div class="col-md-4">--}}
            {{--<div class="box box-success">--}}
                {{--<div class="box-header">--}}
                    {{--<div class="box-header">--}}
                        {{--<h3 class="box-title">Параметры вывода</h3>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="box-body">--}}
                    {{--<div class="table-responsive js-cashOut-result">--}}
                        {{--<table class="table">--}}
                            {{--<tbody>--}}
                                {{--<tr>--}}
                                    {{--<th>Минимальная сумма для снятия:</th>--}}
                                    {{--<td class="js-cashOut-min-show"></td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<th>Комиссия (<span></span>%):</th>--}}
                                    {{--<td class="js-cashOut-commission-show"></td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<th>Итого:</th>--}}
                                    {{--<td class="js-cashOut-result-show"></td>--}}
                                {{--</tr>--}}
                            {{--</tbody>--}}
                        {{--</table>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        </div>
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">История вывода средств</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <tbody>
                        <tr>
                            <th>ID</th>
                            <th>Сумма</th>
                            <th>Платежная система</th>
                            <th>Статус</th>
                            <th>Дата</th>
                        </tr>
                        @foreach($cashouts as $cashout)
                            <tr>
                                <td>{{$cashout->id}}</td>
                                <td>{{$cashout->amount}} {!! \App\Libraries\Helpers::getCurrent(\App\Models\SiteInfo::find(1)['value'], 'icon') !!}</td>
                                <td>{{$cashout->payment_system}}</td>
                                <td><span class="label label-success">Approved</span></td>
                                <td>{{$cashout->date}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    {{$cashouts->links()}}
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection