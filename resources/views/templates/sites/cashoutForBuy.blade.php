@extends("layout.app")

@section('title', 'Перевести на баланс для покупок')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Перевести на баланс для покупок</h3>
                    </div>

                    <div class="box-body">
                        <form action="{{route('cashoutForBuy')}}" method="post" class="js-cashoutForBuy">
                            @csrf
                            <input type="number" name="amount" class="form-control">
                            <button type="submit" class="btn btn-primary">Перевести на баланс для покупок</button>
                        </form>

                    </div>
                </div>
        </div>
    </div>
@endsection
