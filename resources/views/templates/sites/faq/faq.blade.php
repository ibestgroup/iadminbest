@extends("layout.app")

@section('title', 'FAQ')

@section('content')
        @if (Auth::id() == 1)
        <div class="box">
            <div class="box-header">
                <div class="box-header">
                    <h3 class="box-title">Создать вопрос</h3>
                </div>

                <div class="box-body">
                    <form action="{{route('addFaq')}}" role="form" method="post" class="js-faqAdd">
                    {{ csrf_field() }}
                    <!-- text input -->
                        <div class="form-group">
                            <label>Название</label>
                            <input type="text" class="form-control" placeholder="Название" name="name">
                        </div>

                        <!-- textarea -->
                        <div class="form-group">
                            <label>Описание</label>
                            <textarea class="form-control" rows="3" placeholder="Суть вопроса" name="text"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Задать вопрос</button>
                    </form>
                </div>
            </div>
        </div>
        @endif
        <div class="refresh">
        @foreach ($items as $item)
            <div class="box box-default collapsed-box" data-id="{{$item['id']}}">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$item['ask']}}</h3>

                    <div class="box-tools pull-right">

                        @if (Auth::id() == 1)
                            <form action="{{route('deleteFaq')}}" style="display: inline-block">
                                @csrf
                                <button class="btn btn-danger btn-xs js-delete-faq">Удалить</button>
                            </form>
                        @endif
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="/*display: none;*/">
                    {{$item['answer']}}
                </div>
                <!-- /.box-body -->
            </div>
        @endforeach
        </div>
@endsection
