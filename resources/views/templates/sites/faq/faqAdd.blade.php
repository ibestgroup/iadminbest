@extends("layout.admin.app")

@section('title', __('main.faq'))

@section('content')
    <div class="row refresh">
        <div class="col-md-6">
            @if (Auth::id() == 1)
                <div class="box js-edit-faq">
                    <div class="box-header">
                        <div class="box-header">
                            <h3 class="box-title">{{ __('main.create_ask') }}</h3>
                        </div>

                        <div class="box-body">
                            <form action="{{route('addFaq')}}" role="form" method="post" class="js-faqAdd">
                            {{ csrf_field() }}
                            <!-- text input -->
                                <div class="form-group">
                                    <label>{{ __('main.ask') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ __('main.ask') }}" name="name">
                                </div>

                                <!-- textarea -->
                                <div class="form-group">
                                    <label>{{ __('main.answer') }}</label>
                                    <textarea class="form-control" rows="3" placeholder="{{ __('main.answer') }}" name="text"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">{{ __('main.create_ask') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="col-md-6">
            <div>
                @foreach ($items as $item)

                    <div class="panel box box-primary">
                        <div class="box-header">
                            <h4 class="box-title">
                                <a data-toggle="collapse" href=".ask-{{$item['id']}}" aria-expanded="false" class="collapsed">
                                    {{$item['ask']}}
                                </a>
                            </h4>
                            <div class="box-tools pull-right">

                                <a href="{{route('infoFaq', ['id' => $item['id']])}}" class="btn btn-info btn-xs">{{ __('main.edit') }}</a>
                                @if (Auth::id() == 1)
                                    <form action="{{route('deleteFaq')}}" method="post" style="display: inline-block">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$item['id']}}">
                                        <button class="btn btn-danger btn-xs">{{ __('main.delete') }}</button>
                                    </form>
                                @endif
                            </div>
                        </div>
                        <div id="ask-{{$item['id']}}" class="panel-collapse collapse ask-{{$item['id']}}" aria-expanded="false" style="height: 0px;">
                            <div class="box-body">
                                {{$item['answer']}}
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
    </div>
@endsection
