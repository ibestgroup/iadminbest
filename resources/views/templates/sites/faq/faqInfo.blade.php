@extends("layout.admin.app")

@section('title', 'Частозадаваемые вопросы')

@section('content')
    <div class="row refresh">
        <div class="col-md-12">
            @if (Auth::id() == 1)
                <div class="box js-edit-faq">
                    <div class="box-header">
                        <a href="{{route('addFaq')}}" class="btn btn-primary">Назад к вопросам</a> <br>
                        <h3 class="box-title">Создать вопрос</h3>
                    </div>

                    <div class="box-body">
                        <form action="{{route('editFaq')}}" role="form" method="post" class="js-store">
                        {{ csrf_field() }}
                        <!-- text input -->
                            <input type="hidden" value="{{$item['id']}}" name="id">
                            <div class="form-group">
                                <label>Название</label>
                                <input type="text" class="form-control" placeholder="Название" name="ask" value="{{$item['ask']}}">
                            </div>

                            <!-- textarea -->
                            <div class="form-group">
                                <label>Описание</label>
                                <textarea class="form-control" rows="3" placeholder="Суть вопроса" name="answer">{{$item['answer']}}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Созранить</button>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
