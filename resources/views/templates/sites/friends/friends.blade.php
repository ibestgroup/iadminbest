@extends("layout.app")

@section('title', 'Друзья')

@section('content')
    <div class="box refresh">
        <div class="box-header">
            <a href="{{route('friends')}}" class="btn btn-primary">Мои друзья</a>
            <a href="{{route('friends')}}?type=new" class="btn btn-primary">
                Новые заявки {{(\App\Models\Friends::where('toid', Auth::id())->where('status', 0)->count())?'+'.\App\Models\Friends::where('toid', Auth::id())->where('status', 0)->count():''}}
            </a>
            <a href="{{route('friends')}}?type=out" class="btn btn-primary">
                Исходящие заявкии {{(\App\Models\Friends::where('fromid', Auth::id())->where('status', 0)->count())?'('.\App\Models\Friends::where('fromid', Auth::id())->where('status', 0)->count().')':''}}
            </a>
            <br>
            <br>
            <h3 class="box-title">{{$title}}
            @php
                $overflowObj = new \App\Libraries\Multimark(1, 1, true);
                $par_res[0] = $overflowObj->overflowMulti();
                echo $par_res[0];
            @endphp
            </h3>

        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
                @forelse ($friends as $key => $item)

                    @if ($type == 'new')
                        <div class="col-lg-4">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header bg-blue">
                                    <div class="widget-user-image">
                                        <img class="img-circle" src="{{$item->userOut->avatar}}}" alt="User Avatar">
                                    </div>
                                    <!-- /.widgets-user-image -->
                                    <h3 class="widget-user-username">{{$item->userOut->login}}</h3>
                                    <h5 class="widget-user-desc">{{$item->userOut->users_data->lastname}} {{$item->userOut->users_data->name}}</h5>
                                    <form action="{{route('confirmFriend')}}" class="js-friends" method="post" data-id="{{$key}}">
                                        <input type="hidden" name="id_user" value="{{$item->fromid}}">
                                        <button class="btn btn-primary">Подтвердить</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @elseif ($type == 'out')
                        <div class="col-lg-4">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header bg-red">
                                    <div class="widget-user-image">
                                        <img class="img-circle" src="{{$item->user->avatar}}}" alt="User Avatar">
                                    </div>
                                    <!-- /.widgets-user-image -->
                                    <h3 class="widget-user-username">{{$item->user->login}}</h3>
                                    <h5 class="widget-user-desc">{{$item->user->users_data->lastname}} {{$item->user->users_data->name}}</h5>
                                    <form action="{{route('deleteFriend')}}" class="js-friends" method="post" data-id="{{$key}}">
                                        <input type="hidden" name="id_user" value="{{$item->toid}}">
                                        <button class="btn btn-warning">Отклонить</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-lg-4">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header bg-yellow">
                                    <div class="widget-user-image">
                                        <img class="img-circle" src="{{$item->userOut->avatar}}}" alt="User Avatar">
                                    </div>
                                    <!-- /.widgets-user-image -->
                                    <h3 class="widget-user-username">{{$item->userOut->login}}</h3>
                                    <h5 class="widget-user-desc">{{$item->userOut->users_data->lastname}} {{$item->userOut->users_data->name}}</h5>
                                    <form action="{{route('deleteFriend')}}" class="js-friends" method="post" data-id="{{$key}}">
                                        <input type="hidden" name="id_user" value="{{$item->toid}}">
                                        <button class="btn btn-danger">Удалить из друзей</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endif

                @empty
                    <div style="text-align:center">{{$emptyMessage}}</div>
                @endforelse
        </div>
        <div class="box-footer clearfix">
            {{$friends->links()}}
        </div>
    </div>
@endsection
