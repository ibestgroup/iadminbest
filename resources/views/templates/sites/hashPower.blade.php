@extends("layout.app")

@section('title', 'Создать тикет')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Про {{$hashPower['name']}}</h3>
                    </div>

                    <div class="box-body">
                        @if ($hashPower['status'] == 1)
                            <p>На нашем сайте есть дополнительная валюта - <b>{{$hashPower['name']}}</b></p>
                            <p>Вы можете вывести столько, сколько этой валюты.</p>
                            <p>У Вас на балансе: {{Auth::user()->balance_hash_power}} <b>{{$hashPower['name']}}</b></p>
                            <p>1 {{\App\Models\SiteInfo::find(1)['name_current']}} = {{$hashPower['price']}} {{$hashPower['name']}}</p>

                            <p>Вы получаете {{$hashPower['name']}} за:</p>
                            @if ($hashPower['payment'])
                                {{$hashPower['payment']}}% от пополнения своего баланса <br>
                            @endif
                            @if ($hashPower['buy_order'])
                                {{$hashPower['buy_order']}}% от своих покупок <br>
                            @endif
                            @for($i = 0; $i <= 8; $i++)
                                @if ($hashPower['level_'.$i])
                                    {{$hashPower['level_'.$i]}}% за пополнения рефералом {{$i}} уровня <br>
                                @endif
                            @endfor
                            @for($i = 0; $i <= 8; $i++)
                                @if ($hashPower['level_buy_'.$i])
                                    {{$hashPower['level_buy_'.$i]}}% за покупки рефералом {{$i}} уровня <br>
                                @endif
                            @endfor
                        @else
                            <p>На нашем сайте отсутствуют любого рода псевдоочки. Вы можете вывести в любое время свои средства в полном размере.</p>
                        @endif
                    </div>
                </div>
        </div>
    </div>
@endsection
