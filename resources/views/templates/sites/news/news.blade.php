@extends("layout.core.app")

@section('title', 'Новости сайта')

@section('content')

    @php
        $template = \App\Models\Templates::where('status', 1)->first()['id'];
        $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);

    @endphp
    @include('templates.sites.pagebuilder.editPanel', array('page' => 'news'))
    <div class="row main-main">
        {{--Рекламное объявление--}}
        @if ($ad['state'])
            <div class="col-lg-12">
                <div class="alert alert-{{$ad['type']}} alert-dismissible">
                    <h4><i class="icon fa fa-info"></i> {{$ad['head']}}</h4>
                    {{$ad['text']}}
                </div>
            </div>
            <div class="clearfix"></div>
        @endif


        <div class="profile-page page-content">
            <div class="col-lg-12 p-0 page-refresh for-grid" data-id="content">
                {{\App\Http\Controllers\ProfileController::pageConstruct('news')}}
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        @include('templates.sites.pagebuilder.modals', array('page' => 'news'))
    </div>

        {{--<ul class="timeline" >--}}
            {{--@php--}}
                {{--$take = (isset($optionValue['last_news']))?$optionValue['last_news']:5;--}}
                {{--$news = \App\Models\Transaction::whereIn('type', $types)->orderBy('date', 'desc')->take($take)->get();--}}
            {{--@endphp--}}

            {{--@foreach($news as $item)--}}
            {{--<!-- timeline time label -->--}}
            {{--<li class="time-label">--}}
                {{--<span class="bg-red">--}}
                    {{--{{date('Y-d-m', strtotime($item['date']))}}--}}
                {{--</span>--}}
            {{--</li>--}}
            {{--<!-- /.timeline-label -->--}}

            {{--<!-- timeline item -->--}}
            {{--<li data-id="{{$item['id']}}">--}}
                {{--<!-- timeline icon -->--}}
                {{--<i class="fa fa-envelope bg-blue"></i>--}}
                {{--<div class="timeline-item">--}}
                    {{--<span class="time"><i class="fa fa-clock-o"></i> {{date('H-i-s', strtotime($item['date']))}}</span>--}}

                    {{--<h3 class="timeline-header">{{$item['prev']}}</h3>--}}

                    {{--<div class="timeline-body">--}}
                        {{--{{$item['text']}}--}}
                    {{--</div>--}}

                    {{--<div class="timeline-footer">--}}
                        {{--<a class="btn btn-primary btn-xs" href="{{route('goToNews', ['id' => $item['id']])}}">Подробнее</a>--}}
                        {{--@if (Auth::id() == 1)--}}
                            {{--<a class="btn btn-danger btn-xs js-delete-news" data-action="{{route('deleteNews')}}">Удалить</a>--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</li>--}}
            {{--<!-- END timeline item -->--}}
            {{--@endforeach--}}
        {{--</ul>--}}
        {{--@if (Auth::id() == 1)--}}
            {{--<a href="{{route('addNews')}}" class="btn btn-primary">Добавить новость</a>--}}
        {{--@endif--}}
@endsection
