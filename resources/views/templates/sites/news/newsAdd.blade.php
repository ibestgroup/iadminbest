@extends("layout.admin.app")

@section('title', __('main.create_news'))

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">

                    <div class="box-header">
                        <h3 class="box-title">{{ __('main.create_news') }}</h3>
                    </div>

                    <div class="box-body">
                        <form action="{{route('addNews')}}" role="form" method="post" class="js-newsAdd">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label>{{ __('main.head') }}</label>
                                <input type="text" class="form-control" placeholder="{{ __('main.head') }}" name="name">
                            </div>
                            <div class="form-group">
                                <label>{{ __('main.bg_color_icon') }}</label>
                                <input type="color" class="form-control" placeholder="Название" name="color" value="#eeeeee">
                            </div>
                            <div class="form-group">
                                <label>{{ __('main.icon') }}</label> <br>
                                <select class="selectpicker" name="icon">
                                    {!! \App\Libraries\Helpers::selectOption('icon', 'fa fa-tachometer') !!}
                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{ __('main.icon_color') }}</label>
                                <input type="color" class="form-control" placeholder="Название" name="color_icon" value="#666666">
                            </div>

                            <div class="form-group">
                                <label>{{ __('main.text') }}</label>
                                <textarea class="form-control" rows="3" placeholder="{{ __('main.text') }}" name="text"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">{{ __('main.publish') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
