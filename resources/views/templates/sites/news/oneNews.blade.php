@extends("layout.app")
@extends("components.sidebar")

@section('title', 'Новости')

@section('content')
    <div class="box" id="eee">
        <div class="box-header">
            <p><a class="btn btn-primary" href="{{route('news')}}">К списку новостей</a></p>
            <h3 class="box-title">#{{$news['id']}}/{{$news['prev']}}</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            {{$news['text']}}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{$news['date']}}
        </div>
        </div>
    </div>
@endsection
