@extends("layout.core.app")

@section('title', 'Новости сайта')

@section('content')
    @inject('helper', "\App\Libraries\Helpers")

    <ul class="timeline" >
            @foreach($notice as $item)
            <!-- timeline time label -->

            @if ($dateMain != date('Y-d-m', strtotime($item['date'])))
            <li class="time-label">
                <span style="background:#7a43b6; color: #fff;">
                    {{date('Y-d-m', strtotime($item['date']))}}
                </span>
            </li>
            @endif
                    @php
                        $dateMain = date('Y-d-m', strtotime($item['date']));
                    @endphp
            <!-- /.timeline-label -->

            <!-- timeline item -->
            <li data-id="{{$item['id']}}">
                <!-- timeline icon -->

                <i class="fa fa-{{$helper->noticeGet($item->subtype, 'icon')}} bg-{{$helper->noticeGet($item->subtype, 'color')}}"></i>
                <div class="timeline-item">
                    <span class="time"><i class="fa fa-clock-o"></i> {{date('H:i:s', strtotime($item['date']))}}</span>

                    <h3 class="timeline-header">{!! $item['head'] !!}
                        @if ($item->view == 0)
                            @php
                                \App\Models\Notice::where('id', $item->id)->update(['view' => 1]);
                            @endphp
                            <span class="btn btn-warning btn-xs">new</span>
                        @endif
                    </h3>

                    <div class="timeline-body">
                        {!! $item['text'] !!}
                    </div>

                    <div class="timeline-footer">
                        {{--<a class="btn btn-primary btn-xs" href="{{route('goToNews', ['id' => $item['id']])}}">Подробнее</a>--}}
                        {{--@if (Auth::id() == 1)--}}
                            {{--<a class="btn btn-danger btn-xs js-delete-news" data-action="{{route('deleteNews')}}">Удалить</a>--}}
                        {{--@endif--}}
                    </div>
                </div>
            </li>
            <!-- END timeline item -->
            @endforeach
        </ul>
    {{$notice->links()}}
@endsection
