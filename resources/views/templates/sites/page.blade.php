@extends("layout.app")

@section('content')
    @php
        $template = \App\Models\Templates::where('status', 1)->first()['id'];
        $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);

    @endphp
    @include('templates.sites.pagebuilder.editPanel', array('page' => 'profile'))
    <div class="row main-main">
        {{--Рекламное объявление--}}
        @if ($ad['state'])
            <div class="col-lg-12">
                <div class="alert alert-{{$ad['type']}} alert-dismissible">
                    <h4><i class="icon fa fa-info"></i> {{$ad['head']}}</h4>
                    {{$ad['text']}}
                </div>
            </div>
            <div class="clearfix"></div>
        @endif


        {{--Кнопка друзей--}}
        @if ($profile != Auth::id())
            <div class="col-lg-12 refresh" style="padding-bottom: 10px;">
                {!! \App\Libraries\Helpers::friendsButton($profile) !!}
            </div>
            <div class="clearfix"></div>
        @endif




        <div class="profile-page page-content">
            <div class="col-lg-12 p-0 page-refresh for-grid" data-id="content">
                {{\App\Http\Controllers\ProfileController::pageConstruct('profile', 'content')}}
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        @include('templates.sites.pagebuilder.modals', array('page' => 'profile'))
    </div>

@endsection
