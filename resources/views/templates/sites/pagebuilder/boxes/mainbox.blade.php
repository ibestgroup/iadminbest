@inject('helper', "\App\Libraries\Helpers")
<div class="col-md-{{$optionValue['col']}} col-xs-12 main-box box-{{$item['id_in_system']}}" id="box-{{$item['id_in_system']}}" data-id="{{$item['id_in_system']}}">
    <style>
          {!! $helper->getStyle($item['id_in_system'], $demo) !!}
    </style>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                @php
                    if (isset($optionValue['widget'])) {
                        $widget = $optionValue['widget'];
                    } else {
                        $widget = 'mainbox';
                    }
                @endphp
                @include('templates.sites.pagebuilder.widgets.'.$widget, array('section' => 'head', 'box' => $item))
            </h3>
        </div>


        @include('templates.sites.pagebuilder.widgets.'.$widget, array('section' => 'body', 'box' => $item))

    </div>
    <div class="edit-main-box">
        <div class="js-builder-btn" data-toggle="modal" data-id="{{$item['id_in_system']}}" data-widget="{{$optionValue['widget']}}"  data-target="#profile-builder-box-edit">
            <i class="fa fa-edit"></i>
        </div>
        <div class="js-delete-btn" data-toggle="modal" data-id="{{$item['id_in_system']}}" data-widget="{{$optionValue['widget']}}"  data-target="#delete-box-{{$item['id_in_system']}}">
            <i class="fa fa-times"></i>
        </div>
        <div class="js-box-name">{{$item['name_ru']}}</div>
    </div>

    <div class="modal fade" id="delete-box-{{$item['id_in_system']}}">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" style="text-align: center; color: #FF8D00;">Удалить блок?</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Подтверждая действие, вы удалите этот блок. Отменить действие будет невозможно. Вы действительно хотите продолжить?
                    <form action="{{route('templatesDelete')}}" method="post" class="js-store">
                        @csrf
                        <input type="hidden" name="edit" value="delete">
                        <input type="hidden" name="id" value="{{$item['id_in_system']}}">
                        <button class="btn btn-danger">Удалить</button>
                    </form>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</div>