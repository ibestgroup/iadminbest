@inject('helper', "\App\Libraries\Helpers")
@php
    if (isset($optionValue['widget'])) {
        $widget = $optionValue['widget'];
    } else {
        $widget = 'mainbox';
    }
@endphp
<div class="box-{{$item['id_in_system']}}" id="box-{{$item['id_in_system']}}" data-id="{{$item['id_in_system']}}" style="position: relative;">
    <style>
        {{$helper->getStyle($item['id_in_system'], $demo)}}
    </style>
    @include('templates.sites.pagebuilder.widgets.'.$widget)
    <div class="edit-main-box" style="height: 0px;">
        <div class="js-builder-btn" data-toggle="modal" data-id="{{$item['id_in_system']}}" data-widget="{{$optionValue['widget']}}"  data-target="#profile-builder-box-edit">
            <i class="fa fa-edit"></i>
        </div>
        <div class="js-box-name">{{$item['name_ru']}}</div>
    </div>
</div>