<div class="col-md-{{$optionValue['col']}} main-box" id="box-{{$item['id']}}" data-id="{{$item['id']}}">
    <div class="box" style="background-color: #f00;">

        <div class="box-header">
            <h3 class="box-title">
                @yield('name')
            </h3>
        </div>

        <div class="box-body">

            @yield('widgets')

        </div>
    </div>
</div>