<style>
    @if (isset($optionValue['box_style']))
        .box-{{$item['id']}} .box {
            background-color: {{$optionValue['box_style']['bg']}};
        }
    @endif
</style>
<div class="col-md-{{$optionValue['col']}} main-box box-{{$item['id']}}" id="box-{{$item['id']}}" data-id="{{$item['id']}}">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                @php
                    if (isset($optionValue['widget'])) {
                        $widget = $optionValue['widget'];
                    } else {
                        $widget = 'mainbox';
                    }
                @endphp
                @include('templates.sites.pagebuilder.widgets.'.$widget, array('section' => 'head'))
            </h3>
        </div>


        @include('templates.sites.pagebuilder.widgets.'.$widget, array('section' => 'body'))

    </div>
</div>