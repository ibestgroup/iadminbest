@php
$template = \App\Models\Templates::where('status', 1)->first()['id'];
$styleId = \App\Models\SiteElements::where('type', 'style')->where('name', 'main-style')->where('template', $template)->first()['id_in_system'];
$mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);
$showPanel = (isset($_COOKIE['panel-redactor']))?$_COOKIE['panel-redactor']:'false';
@endphp
<div style="position: relative;">
    <div class="col-lg-12 panel-redactor {{($showPanel == 'true')?'show-panel':''}}" style="background: #fff; margin-bottom: 10px; position: relative; z-index: 1049;{{($showPanel == 'false')?'display:none;':''}}">
        <h4 style="margin-top: 15px;">{{__('main.edit_panel')}}</h4>
        <a href="{{route('admin')}}" class="btn btn-app">
            <i class="fas fa-2x fa-cogs" style="display: block;"></i>{{__('main.admin_panel')}}
        </a>
        <a class="btn btn-app" data-toggle="modal" data-target="#profile-builder-grid">
            <i class="fa fa-2x fa-border-none" style="display: block;"></i>{{__('main.add_area')}}
        </a>
        <a class="btn btn-app" data-toggle="modal" data-target="#profile-builder-box">
            <i class="fa fa-2x fa-plus" style="display: block;"></i>{{__('main.add_element')}}
        </a>
        <a class="js-outline-grid btn btn-app" style="cursor: pointer;">
            <i class="fas fa-2x fa-border-style" style="display: block;"></i>{{__('main.on_grid')}}
        </a>
        <a class="js-activate-grid btn btn-app" style="cursor: pointer;">
            <i class="fas fa-2x fa-hand-paper" style="display: block;"></i>{{__('main.sort')}}
        </a>
        <a class="js-edit-elements btn btn-app" style="cursor: pointer;">
            <i class="fas fa-2x fa-cubes" style="display: block;"></i>{{__('main.setting_elements')}}
        </a>
        <a class="js-site-redactor btn btn-app" style="cursor: pointer;">
            <i class="fas fa-2x fa-tools" style="display: block;"></i>{{__('main.edit_site')}}
        </a>

        <div class="clearfix"></div>
        <style>
            .site-redactor-panel {
                border-bottom-color: #000;
            }
        </style>

        <div class="site-redactor-panel mt-10" style="height: 0px; overflow: hidden;">
            <div class="in-panel" style="border: 1px solid #ddd; overflow: auto; background: #f4f4f4; width: 0px; margin: auto; border-radius: 7px;">
                <div class="fix-width" style="width: 1200px; padding: 0 20px;">
                    <form action="{{route('updateElement')}}" method="post" class="js-editSite">
                        @csrf
                        <input type="hidden" name="type" value="main-style">
                        <input type="hidden" name="block_id" value="{{$styleId}}">
                        <div class="col-lg-4 pl-0" style="border-right: 1px solid #d5d5d5;">
                            <h4>{{ __('main.site_area') }}</h4>
                            <div class="site-mini">
                                <div class="site-mini-header">
                                    {{ __('main.site_header') }} <input type="checkbox" name="header[status]" {{($mainStyle['header']['status'] == 1)?'checked="checked"':''}} value="1">
                                </div>
                                <div class="site-mini-top">
                                    {{ __('main.site_top_sidebar') }} <input type="checkbox" name="top_sidebar[status]" {{($mainStyle['top_sidebar']['status'] == 1)?'checked="checked"':''}} value="1">
                                </div>
                                <div class="site-mini-left">
                                    {!! __('main.site_left_menu') !!}
                                    <input type="checkbox" name="left_sidebar[status]" {{($mainStyle['left_sidebar']['status'] == 1)?'checked="checked"':''}} value="1">
                                </div>
                                <div class="site-mini-content">
                                    {!! __('main.site_content') !!}
                                    <input type="checkbox" checked="checked" disabled>
                                </div>
                                <div class="site-mini-right">
                                    {!! __('main.site_right_menu') !!}
                                    <input type="checkbox" name="right_sidebar[status]" {{($mainStyle['right_sidebar']['status'] == 1)?'checked="checked"':''}} value="1">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <h4>{{ __('main.left_menu') }}</h4>
                            <div class="col-lg-6 pl-0">
                                <div class="form-group">
                                    <label>{{ __('main.bg_color') }}</label>
                                    <input type="color" class="form-control" value="{{$mainStyle['left_sidebar']['style']['bg_color']}}" name="left_sidebar[style][bg_color]">
                                </div>
                            </div>
                            <div class="col-lg-6 pr-0">
                                <div class="form-group">
                                    <label>{{ __('main.width') }}</label>
                                    <input type="number" class="form-control" value="{{$mainStyle['left_sidebar']['style']['width']}}" name="left_sidebar[style][width]">
                                </div>
                            </div>
                            <div class="col-lg-12 p-0">
                                <div class="form-group">
                                    <label>{{ __('main.bg_img') }}</label>
                                    <input type="text" class="form-control" value="{{$mainStyle['left_sidebar']['style']['bg_img']}}" name="left_sidebar[style][bg_img]">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <h4>{{ __('main.right_menu') }}</h4>
                            <div class="col-lg-6 pl-0">
                                <div class="form-group">
                                    <label>{{ __('main.bg_color') }}</label>
                                    <input type="color" class="form-control" value="{{$mainStyle['right_sidebar']['style']['bg_color']}}" name="right_sidebar[style][bg_color]">
                                </div>
                            </div>
                            <div class="col-lg-6 pr-0">
                                <div class="form-group">
                                    <label>{{ __('main.width') }}</label>
                                    <input type="number" class="form-control" value="{{$mainStyle['right_sidebar']['style']['width']}}" name="right_sidebar[style][width]">
                                </div>
                            </div>
                            <div class="col-lg-12 p-0">
                                <div class="form-group">
                                    <label>{{ __('main.bg_img') }}</label>
                                    <input type="text" class="form-control" value="{{$mainStyle['right_sidebar']['style']['bg_img']}}" name="right_sidebar[style][bg_img]">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4" style="border-right: 1px solid #d5d5d5;">
                            <h4>{{ __('main.top_sidebar') }}</h4>
                            <div class="col-lg-6 pl-0">
                                <div class="form-group">
                                    <label>{{ __('main.bg_color') }}</label>
                                    <input type="color" class="form-control" value="{{$mainStyle['top_sidebar']['style']['bg_color']}}" name="top_sidebar[style][bg_color]">
                                </div>
                            </div>
                            <div class="col-lg-6 pr-0">
                                <div class="form-group">
                                    <label>{{ __('main.height') }}</label>
                                    <input type="number" class="form-control" value="{{$mainStyle['top_sidebar']['style']['height']}}" name="top_sidebar[style][height]">
                                </div>
                            </div>
                            <div class="col-lg-12 p-0">
                                <div class="form-group">
                                    <label>{{ __('main.bg_img') }}</label>
                                    <input type="text" class="form-control" value="{{$mainStyle['top_sidebar']['style']['bg_img']}}" name="top_sidebar[style][bg_img]">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr>
                            <h4>{{ __('main.site_header') }}</h4>
                            <div class="col-lg-12 p-0">
                                <div class="col-lg-12 pl-0">
                                    <div class="form-group">
                                        {{ __('main.full_width') }}
                                        <input type="checkbox" class="boxed-header" {{($mainStyle['header']['width_full'] == 1)?'checked="checked"':''}} name="header[width_full]" value="1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 pl-0">
                                <div class="form-group header-width">
                                    <label>{{ __('main.width') }}</label>
                                    <input type="number" class="form-control width-header" value="{{$mainStyle['header']['width']}}" name="header[width]"
                                           @if($mainStyle['header']['width_full'] == 1)
                                           disabled="disabled"
                                            @endif
                                    >
                                </div>
                            </div>
                            <div class="col-lg-6 pr-0">
                                <div class="form-group">
                                    <label>{{ __('main.height') }}</label>
                                    <input type="number" class="header-height form-control" value="{{$mainStyle['header']['height']}}" name="header[height]">
                                </div>
                            </div>
                            <div class="col-lg-12 p-0">
                                <div class="form-group">
                                    <label>{{ __('main.image') }}</label>
                                    <input type="text" class="header-image form-control" value="{{$mainStyle['header']['img']}}" name="header[img]">
                                </div>
                            </div>
                            <b>{{ __('main.border') }}</b>
                            <div class="form-group col-lg-12 p-0">
                                <div class="col-lg-4 pl-0">
                                    <label>{{ __('main.width') }}</label>
                                    <input type="number" class="form-control" name="header[border][width]" value="{{$mainStyle['header']['border']['width']}}">
                                </div>
                                <div class="col-lg-4">
                                    <label>{{ __('main.color') }}</label>
                                    <input type="color" class="form-control" name="header[border][color]" value="{{$mainStyle['header']['border']['color']}}">
                                </div>
                                <div class="col-lg-4 pr-0">
                                    <label>{{ __('main.type') }}</label>
                                    <select class="form-control" name="header[border][style]">
                                        {!! \App\Libraries\Helpers::selectOption('type_border', $mainStyle['header']['border']['style']) !!}
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <b>{{ __('main.border_radius') }}</b>
                            <div class="form-group col-lg-12 p-0">
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.top_left') }}</label>
                                    <input type="number" class="form-control" name="header[border_radius][tl]" value="{{$mainStyle['header']['border_radius']['tl']}}">
                                </div>
                                <div class="col-lg-6 pr-0">
                                    <label>{{ __('main.top_right') }}</label>
                                    <input type="number" class="form-control" name="header[border_radius][tr]" value="{{$mainStyle['header']['border_radius']['tr']}}">
                                </div>
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.bottom_left') }}</label>
                                    <input type="number" class="form-control" name="header[border_radius][bl]" value="{{$mainStyle['header']['border_radius']['bl']}}">
                                </div>
                                <div class="col-lg-6 pr-0">
                                    <label>{{ __('main.bottom_right') }}</label>
                                    <input type="number" class="form-control" name="header[border_radius][br]" value="{{$mainStyle['header']['border_radius']['br']}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 pr-0">
                            <h4>{{ __('main.main_setting') }}</h4>
                            <b>{{ __('main.setting_width') }}</b>
                            <div class="clearfix"></div>
                            <div class="col-lg-6 pl-0" style="height: 60px; padding: 20px 0;">
                                <div class="form-group">
                                    {{ __('main.full_width') }}
                                    <input type="checkbox" class="boxed-site" {{($mainStyle['main_style']['width_full'] == 1)?'checked':''}} name="main_style[width_full]" value="1">
                                </div>
                            </div>
                            <div class="col-lg-6 pr-0">
                                <div class="form-group">
                                    <label>{{ __('main.width') }}</label>
                                    <input type="number" class="form-control width-site" value="{{$mainStyle['main_style']['width']}}" name="main_style[width]"
                                        @if($mainStyle['main_style']['width_full'] == 1)
                                        disabled="disabled"
                                        @endif
                                    >
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <h4>{{ __('main.main_bg') }}</h4>
                            <div class="form-group col-lg-6 pl-0">
                                <label>{{ __('main.color') }}</label>
                                <input type="color" class="bg-color form-control" value="{{$mainStyle['main_style']['bg_color_body']}}" name="main_style[bg_color_body]">
                            </div>
                            <div class="form-group col-lg-6 pr-0">
                                <label>{{ __('main.image') }}</label>
                                <input type="text" class="bg-image form-control" value="{{$mainStyle['main_style']['bg_img_body']}}" name="main_style[bg_img_body]">
                            </div>
                            <h4>{{ __('main.content_bg') }}</h4>
                            <div class="form-group col-lg-6 pl-0">
                                <label>{{ __('main.color') }}</label>
                                <input type="color" class="bg-color-sidebar form-control" value="{{$mainStyle['main_style']['bg_color_content']}}" name="main_style[bg_color_content]">
                            </div>
                            <div class="form-group col-lg-6 pr-0">
                                <label>{{ __('main.image') }}</label>
                                <input type="text" class="bg-image-sidebar form-control" value="{{$mainStyle['main_style']['bg_img_content']}}" name="main_style[bg_img_content]">
                            </div>
                            <div class="clearfix"></div>

                            <b>{{ __('main.border') }}</b>
                            <div class="form-group col-lg-12 p-0">
                                <div class="col-lg-4 pl-0">
                                    <label>{{ __('main.width') }}</label>
                                    <input type="number" class="form-control" value="{{$mainStyle['main_style']['border']['width']}}" name="main_style[border][width]">
                                </div>
                                <div class="col-lg-4">
                                    <label>{{ __('main.color') }}</label>
                                    <input type="color" class="form-control" value="{{$mainStyle['main_style']['border']['color']}}" name="main_style[border][color]">
                                </div>
                                <div class="col-lg-4 pr-0">
                                    <label>{{ __('main.type') }}</label>
                                    <select class="form-control"name="main_style[border][style]">
                                        {!! \App\Libraries\Helpers::selectOption('type_border', $mainStyle['main_style']['border']['style']) !!}
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <b>{{ __('main.border_radius') }}</b>
                            <div class="form-group col-lg-12 p-0">
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.top_left') }}</label>
                                    <input type="number" class="form-control" name="main_style[border_radius][tl]" value="{{$mainStyle['main_style']['border_radius']['tl']}}">
                                </div>
                                <div class="col-lg-6 pr-0">
                                    <label>{{ __('main.top_right') }}</label>
                                    <input type="number" class="form-control" name="main_style[border_radius][tr]" value="{{$mainStyle['main_style']['border_radius']['tr']}}">
                                </div>
                                <div class="col-lg-6 pl-0">
                                    <label>{{ __('main.bottom_left') }}</label>
                                    <input type="number" class="form-control" name="main_style[border_radius][bl]" value="{{$mainStyle['main_style']['border_radius']['bl']}}">
                                </div>
                                <div class="col-lg-6 pr-0">
                                    <label>{{ __('main.bottom_right') }}</label>
                                    <input type="number" class="form-control" name="main_style[border_radius][br]" value="{{$mainStyle['main_style']['border_radius']['br']}}">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <b>{{ __('main.site_margin_top') }}</b>
                            <div class="clearfix"></div>
                            <div class="col-lg-6 pl-0">
                                <div class="form-group">
                                    <label>{{ __('main.margin') }}</label>
                                    <input type="number" class="form-control" value="{{$mainStyle['main_style']['margin_top']}}" name="main_style[margin_top]">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <input type="submit" class="btn btn-primary" value="{{ __('main.admin_save') }}">
                </form>
            </div>
        </div>

        <script>
            $(document).ready(function () {
                let inPanel = $('.in-panel'),
                    panel = $('.site-redactor-panel');

                $('.js-site-redactor').on('click', function () {
                    console.log(inPanel.height());
                    if (panel.height() != 0) {
                        inPanel.animate({width: 0}, {duration: 500, complete: function () {
                                panel.animate({height: 0})
                            }}, 'linear');
                    } else {
                        panel.animate({height: inPanel.height() + 20}, {duration: 500, complete: function () {
                                inPanel.animate({width: '100%'})
                            }}, 'linear');
                    }
                });

                let fixWidth = $('.fix-width');
                fixWidth.css({'width': fixWidth.parents('.site-redactor-panel').width() + 'px'});
            });
        </script>
    </div>
</div>
</div>