@inject('helper', "\App\Libraries\Helpers")
<div class="form-group col-lg-6">
    <h4>{{ __('main.main_setting') }}</h4>
    <label>{{ __('main.head') }}</label>
    <input type="text" class="form-control" name="head" placeholder="{{ __('main.head') }}" value="{{$optionValue['head']}}">
    {!! $helper->getForm('align', 'box', 'head', $optionValue) !!}
    <label>{{ __('main.width') }}</label>
    <select class="form-control" name="col">
        {!! $helper->selectOption('column', $optionValue['col']) !!}
    </select>
</div>
<div class="form-group col-lg-6">
    <h4>{{ __('main.color') }}</h4>
    <div class="form-group col-lg-4 pl-0">
        <label>{{ __('main.main_bg') }}</label>
        {{--<input type="color" class="form-control" name="box_style[bg]" placeholder="Цвет фона" value="{{(isset($optionValue['box_style']['bg']))?$optionValue['box_style']['bg']:''}}">--}}
        {!! $helper->getForm('color', 'box', '', $optionValue, 'bg') !!}
    </div>
    <div class="form-group col-lg-4">
        <label>{{ __('main.head') }} {{ __('main.bg') }}</label>
        {{--<input type="color" class="form-control" name="box_style[bg_head]" placeholder="Цвет фона" value="{{(isset($optionValue['box_style']['bg_head']))?$optionValue['box_style']['bg_head']:''}}">--}}

        {!! $helper->getForm('color', 'box', 'head', $optionValue, 'bg') !!}
    </div>
    <div class="form-group col-lg-4 pr-0">
        <label>{{ __('main.head') }} {{ __('main.text') }}</label>
        <input type="color" class="form-control" name="box_style[color_head]" placeholder="Цвет фона" value="{{(isset($optionValue['box_style']['color_head']))?$optionValue['box_style']['color_head']:''}}">

    </div>
    <div class="col-lg-12 p-0">
        <h4>{{ __('main.border') }}</h4>
        {!! $helper->getForm('border', 'box', '', $optionValue) !!}
        <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>

<div class="col-lg-6">
    <h5>{{ __('main.border_radius') }}</h5>
    {!! $helper->getForm('border_radius', 'box', '', $optionValue) !!}
    <div class="clearfix"></div>
</div>

<div class="col-lg-6">
    <h5>{{ __('main.margins') }}</h5>
    <div class="form-group col-lg-6 pl-0">
        <label>{{ __('main.margin_left') }}</label>
        <input class="form-control" name="box_style[padding][left]" value="{{(isset($optionValue['box_style']['padding']['left']))?$optionValue['box_style']['padding']['left']:'15'}}">
    </div>
    <div class="form-group col-lg-6 pr-0">
        <label>{{ __('main.margin_right') }}</label>
        <input class="form-control" name="box_style[padding][right]" value="{{(isset($optionValue['box_style']['padding']['right']))?$optionValue['box_style']['padding']['right']:'15'}}">
    </div>
</div>
<div class="clearfix"></div>
