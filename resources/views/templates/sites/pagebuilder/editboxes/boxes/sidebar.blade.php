@inject('helper', "\App\Libraries\Helpers")
<div class="form-group col-lg-6">
    <label>Отступ сверху</label>
    <input type="number" class="form-control" name="box_style[margin][top]" value="{{(isset($optionValue['box_style']['margin']['top']))?$optionValue['box_style']['margin']['top']:''}}">
    <label>Отступ снизу</label>
    <input type="number" class="form-control" name="box_style[margin][bottom]" value="{{(isset($optionValue['box_style']['margin']['bottom']))?$optionValue['box_style']['margin']['bottom']:''}}">
</div>
<div class="form-group col-lg-6">
    <label>Прижать к краю</label>
    {!! $helper->getForm('float', 'box', '', $optionValue) !!}
</div>
<div class="clearfix"></div>
