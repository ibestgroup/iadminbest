@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">

    <div class="form-group col-lg-6 p-0">
        <select name="width_tarif" id="">
            {!! $helper->selectOption('column', (isset($optionValue['width_tarif'])?$optionValue['width_tarif']:'')) !!}
        </select>
    </div>
    <div class="clearfix"></div>

    <div class="col-lg-6 pl-0">
        <label>{{ __('main.padding_button') }}</label>
        {!! $helper->getForm('padding', 'widget', 'tarif', $optionValue) !!}
    </div>
    <div class="col-lg-6 pr-0">
        {!! $helper->getForm('align', 'widget', 'tarif', $optionValue) !!}
    </div>
    <div class="clearfix"></div>
    <h4>{{ __('main.design_button') }}</h4>
    <div class="col-lg-12 p-0">
        {!! $helper->getForm('btn', 'widget', 'tarif', $optionValue) !!}
    </div>
    <div class="clearfix"></div>


</div>