@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            {{--<li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">Элементы</a></li>--}}
            <li class="active"><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="style-setting">
                <h4>{{ __('main.mains') }}</h4>
                <div class="form-group col-lg-6 pl-0">
                    <label>{{ __('main.text') }}</label>
                    <textarea type="text" class="form-control" name="text_area" placeholder="{{ __('main.text') }}">
                        {{(isset($optionValue['text_area']))?$optionValue['text_area']:''}}
                    </textarea>
                    {!! $helper->getForm('align', 'widget', 'text_area', $optionValue) !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'text_area', $optionValue) !!}
                </div>
                <div class="clearfix"></div>

                <h4>{{ __('main.setting_button') }}</h4>
                {!! $helper->getForm('btn', 'widget', '', $optionValue) !!}

                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.text') }}</label>
                    <input type="text" class="form-control" name="link[text]" value="{{(isset($optionValue['link']['text']))?$optionValue['link']['text']:'Пополнить'}}">
                </div>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>