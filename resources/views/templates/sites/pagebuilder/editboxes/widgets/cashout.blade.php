@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            {{--<li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">Элементы</a></li>--}}
            <li class="active"><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="style-setting">
                <h4>{{ __('main.mains') }}</h4>
                <div class="form-group col-lg-6 pl-0">

                    {!! $helper->getForm('align', 'widget', 'label', $optionValue) !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'text', $optionValue, 'label') !!}
                </div>
                <div class="clearfix"></div>


                <h4>{{ __('main.admin_design_inputs') }}</h4>
                <h5>{{ __('main.color') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'input', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'input', $optionValue) !!}
                </div>

                <div class="clearfix"></div>
                <h5>{{ __('main.border') }}</h5>
                {!! $helper->getForm('border', 'widget', 'input', $optionValue) !!}

                <h5>{{ __('main.border_radius') }}</h5>
                {!! $helper->getForm('border_radius', 'widget', 'input', $optionValue) !!}
                <div class="clearfix"></div>

                <h4>{{ __('main.setting_button') }}</h4>
                {!! $helper->getForm('btn', 'widget', '', $optionValue) !!}

                <h4>Платежные системы</h4>
                @php
                $paymentSystems = \App\Models\CashoutSystemCore::where(function ($q) use ($helper) {
                    $q->where('currency', $helper->siteCurrency())->orWhere('currency', 0);
                })->get();
                @endphp
                @foreach($paymentSystems as $item)
                    <input type="checkbox" value="{{ $item->id }}" name="payment_system[]"
                            {{ isset($optionValue['payment_system']) && in_array($item->id, $optionValue['payment_system']) ? 'checked="checked"' : '' }}>{{ $item->name }}<br>
                @endforeach

                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.text') }}</label>
                    <input type="text" class="form-control" name="link[text]" value="{{(isset($optionValue['link']['text']))?$optionValue['link']['text']:'Вывести'}}">
                </div>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>