@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">

    <div class="form-group col-lg-12 p-0 editor-change">

        <textarea name="content" id="editor" rows="10" cols="80">
            {{$optionValue['content']}}
        </textarea>

        <div class="clearfix"></div>
    </div>

    <div class="clearfix"></div>
</div>