@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            {{--<li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">Элементы</a></li>--}}
            <li class="active"><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="style-setting">
                <h4>{{ __('main.mains') }}</h4>
                <div class="form-group col-lg-6 pl-0">
                    <label>{{ __('main.text') }}</label>
                    <input type="text" class="form-control" name="label_sum" placeholder="{{ __('main.text') }}" value="{{(isset($optionValue['label_sum']))?$optionValue['label_sum']:''}}">

                    {!! $helper->getForm('align', 'widget', 'label', $optionValue) !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'text', $optionValue, 'label') !!}
                </div>
                <div class="clearfix"></div>


                <h4>{{ __('main.admin_design_inputs') }}</h4>
                <h5>{{ __('main.color') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'input', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'input', $optionValue) !!}
                </div>

                <div class="clearfix"></div>
                <h5>{{ __('main.border') }}</h5>
                {!! $helper->getForm('border', 'widget', 'input', $optionValue) !!}

                <h5>{{ __('main.border_radius') }}</h5>
                {!! $helper->getForm('border_radius', 'widget', 'input', $optionValue) !!}
                <div class="clearfix"></div>

                <h4>{{ __('main.setting_button') }}</h4>
                {!! $helper->getForm('btn', 'widget', '', $optionValue) !!}

                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.text') }}</label>
                    <input type="text" class="form-control" name="link[text]" value="{{(isset($optionValue['link']['text']))?$optionValue['link']['text']:'Пополнить'}}">
                </div>

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>