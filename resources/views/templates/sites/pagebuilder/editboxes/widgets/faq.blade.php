@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            {{--<li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">Элементы</a></li>--}}
            <li class="active"><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="style-setting">

                <h4>{{ __('main.decor') }}</h4>
                <h5>{{ __('main.ask') }}</h5>
                <div class="col-lg-4 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'head', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-4 p-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'head', $optionValue) !!}
                </div>
                <div class="col-lg-4 pr-0">
                    <label>{{ __('main.align') }}</label>
                    {!! $helper->getForm('align', 'widget', 'head', $optionValue) !!}
                </div>
                <div class="clearfix"></div>
                <h5>{{ __('main.border') }}</h5>
                {!! $helper->getForm('border', 'widget', 'head', $optionValue) !!}
                <div class="clearfix"></div>

                <h5>{{ __('main.answer') }}</h5>
                <div class="col-lg-4 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'text', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-4 p-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'text', $optionValue) !!}
                </div>
                <div class="col-lg-4 pr-0">
                    <label>{{ __('main.align') }}</label>
                    {!! $helper->getForm('align', 'widget', 'text', $optionValue) !!}
                </div>
                <div class="clearfix"></div>
                <h5>{{ __('main.border') }}</h5>
                {!! $helper->getForm('border', 'widget', 'text', $optionValue) !!}
                <div class="clearfix"></div>

                <h5>{{ __('main.border_radius') }}</h5>
                {!! $helper->getForm('border_radius', 'widget', 'body', $optionValue) !!}
                <div class="clearfix"></div>

            </div>

        </div>
    </div>
</div>