@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">{{ __('main.elements') }}</a></li>
            <li><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main-setting">

                <div class="col-lg-12 mt-20">
                    <label>{{ __('main.elements') }}</label>
                </div>
                <div class="clearfix"></div>
                <div class="sort ui-state-default ui-sortable-handle element-links element-hide example-disabled" id="links-element-number">
                    <div class="col-lg-4 pl-0">
                        <input type="text" class="form-control" name="links[element-number][text]">
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="links[element-number][link]">
                            {!! \App\Libraries\Helpers::selectOption('value_links', '') !!}
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="links[element-number][icon]">
                            {!! \App\Libraries\Helpers::selectOption('icon', '') !!}
                        </select>
                    </div>
                    <div class="col-lg-2 pr-0 for-btns">
                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="sort-list element-links-count">
                    @if (isset($optionValue['links']))
                        @foreach($optionValue['links'] as $key => $item)
                            <div class="sort ui-state-default" id="links-{{$key}}">
                                <div class="col-lg-4 pl-0">
                                    <input type="text" class="form-control" name="links[{{$key}}][text]" value="{{$optionValue['links'][$key]['text']}}">
                                </div>
                                <div class="col-lg-3">
                                    <select class="form-control" name="links[{{$key}}][link]">
                                        {!! \App\Libraries\Helpers::selectOption('value_links', $optionValue['links'][$key]['link']) !!}
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select class="form-control selectpicker" name="links[{{$key}}][icon]">
                                        {!! \App\Libraries\Helpers::selectOption('icon', $optionValue['links'][$key]['icon']) !!}
                                    </select>
                                </div>
                                <div class="col-lg-2 pr-0 for-btns">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="for-btn-add" data-example=".element-links"></div>
                <p class="btn-primary btn js-activate-sort-list">{{ __('main.configure') }}</p>
                <div class="clearfix"></div>

            </div>

            <div class="tab-pane" id="style-setting">

                <h4>{{ __('main.setting_button') }}</h4>
                {!! $helper->getForm('btn', 'widget', '', $optionValue) !!}

                <h4>{{ __('main.icon_color') }}</h4>

                <div class="form-group col-lg-12 p-0">
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.btn_static') }}</label>
                        {!! $helper->getForm('color', 'widget', 'icon_btn', $optionValue) !!}
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.btn_hover') }}</label>
                        {!! $helper->getForm('color', 'widget', 'icon_btn_h', $optionValue) !!}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>