@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">

    <div class="tab-pane active" id="tarif-list">

        <div class="col-lg-6 pl-0">
            <h4>{{ __('main.modal_main_setting-widget') }}</h4>
            <label>{{ __('main.number_records') }}</label>
            <input type="number" class="form-control" name="last_deposits" value="{{(isset($optionValue['last_deposits'])?$optionValue['last_deposits']:'')}}">
            <input type="checkbox" name="first_page" value="1" {{(isset($optionValue['first_page'])?($optionValue['first_page'] == 1)?'checked="checked"':'':'')}}> {{ __('main.only_first_page') }}
            <br>
            <label>{{ __('main.what_display') }}</label>
            <br>
            <input type="checkbox" name="top_btn"  value="1" {{(isset($optionValue['top_btn']))?($optionValue['top_btn']==1)?'checked="checked"':'':''}}> {{ __('main.top_button') }}
            <br>
        </div>
        <div class="col-lg-6 pr-0">
            <h4>{{ __('main.elements_with_tariff') }}</h4>
            <h5>{{ __('main.dividing_lines') }}</h5>
            {!! $helper->getForm('border', 'widget', 'line', $optionValue) !!}
            <h5>{{ __('main.font') }}</h5>
            {!! $helper->getForm('align', 'widget', '', $optionValue) !!}
        </div>
        <div class="col-lg-6 pr-0">
            <h4>{{ __('main.table_color') }}</h4>
            <div class="col-lg-12">
                <h5>{{ __('main.table_head_color') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'th', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'th', $optionValue) !!}
                </div>
            </div>
            <div class="col-lg-12">
                <h5>{{ __('main.static_color') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'tr', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'tr', $optionValue) !!}
                </div>
            </div>
            <div class="col-lg-12">
                <h5>{{ __('main.hover_color') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'tr_h', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'tr_h', $optionValue) !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6 pl-0">
            <h4>{{ __('main.top_button') }}</h4>
            {!! $helper->getForm('btn', 'widget', 'top', $optionValue) !!}
        </div>
        <div class="col-lg-6 pr-0">
            <h4>{{ __('main.button_more') }}</h4>
            {!! $helper->getForm('btn', 'widget', 'more', $optionValue) !!}
        </div>
        <div class="col-lg-6 pl-0">
            <h4>{{ __('main.pagination') }}</h4>
            {!! $helper->getForm('pagination', 'widget', '', $optionValue) !!}
        </div>
        <div class="clearfix"></div>

        <div class="col-lg-12">
            <h4>{{ __('main.elements') }}</h4>
        </div>

        <div class="sort ui-state-default ui-sortable-handle element-list element-hide example-disabled" id="list-element-number">
            <div class="col-lg-4">
                <input type="text" class="form-control" name="list[element-number][name]" value="">
            </div>
            <div class="col-lg-4">
                <select class="form-control" name="list[element-number][value]">
                    {!! \App\Libraries\Helpers::selectOption('value_deposit', '') !!}
                </select>
            </div>
            <div class="col-lg-2">
                <select class="form-control" name="list[element-number][icon]">
                    {!! \App\Libraries\Helpers::selectOption('icon', '') !!}
                </select>
            </div>
            <div class="col-lg-2 pr-0 for-btns">
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="sort-list element-list-count" data-example=".element-list">
            @if (isset($optionValue['list']))
                @foreach($optionValue['list'] as $key => $item)
                    <div class="sort ui-state-default" id="list-{{$key}}">
                        <div class="col-lg-4">
                            <input type="text" class="form-control" name="list[{{$key}}][name]" value="{{(isset($optionValue['list'][$key]['name']))?$optionValue['list'][$key]['name']:''}}">
                        </div>
                        <div class="col-lg-4">
                            <select class="form-control selectpicker" name="list[{{$key}}][value]">
                                {!! \App\Libraries\Helpers::selectOption('value_deposit', $optionValue['list'][$key]['value']) !!}
                            </select>
                        </div>
                        <div class="col-lg-2">
                            <select class="form-control selectpicker" name="list[{{$key}}][icon]">
                                {!! \App\Libraries\Helpers::selectOption('icon', (isset($optionValue['list'][$key]['icon']))?$optionValue['list'][$key]['icon']:'') !!}
                            </select>
                        </div>
                        <div class="col-lg-2 for-btns">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                @endforeach
            @endif
        </div>

        <div class="for-btn-add" data-example=".element-list"></div>
        <p class="btn-primary btn js-activate-sort-list">{{ __('main.configure') }}</p>
        <div class="clearfix"></div>

    </div>





    {{--<div class="tab-pane" id="tarif-tree">--}}

        {{--<div class="col-lg-6 pl-0">--}}
            {{--<h4>Реферальное дерево</h4>--}}
            {{--<div class="nav-tabs-custom">--}}
                {{--<ul class="nav nav-tabs">--}}
                    {{--<li class="active"><a href="#tree-place" data-toggle="tab" aria-expanded="true">Занятые места</a></li>--}}
                    {{--<li><a href="#tree-place-free" data-toggle="tab" aria-expanded="true">Свободные места</a></li>--}}
                {{--</ul>--}}
                {{--<div class="tab-content">--}}
                    {{--<div class="tab-pane active" id="tree-place">--}}
                        {{--<h5>Рамка аватаров</h5>--}}
                        {{--{!! $helper->getForm('border', 'widget', 'img', $optionValue) !!}--}}
                        {{--<h5>Закругление краев</h5>--}}
                        {{--{!! $helper->getForm('border_radius', 'widget', 'img', $optionValue) !!}--}}
                        {{--<h5>Текст</h5>--}}
                        {{--<label>Цвет текста</label>--}}
                        {{--{!! $helper->getForm('color', 'widget', 'img', $optionValue) !!}--}}
                        {{--<label>Шрифт</label>--}}
                        {{--{!! $helper->getForm('align', 'widget', 'img', $optionValue) !!}--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                    {{--<div class="tab-pane" id="tree-place-free">--}}
                        {{--<h5>Рамка аватаров</h5>--}}
                        {{--{!! $helper->getForm('border', 'widget', 'img_free', $optionValue) !!}--}}
                        {{--<h5>Закругление краев</h5>--}}
                        {{--{!! $helper->getForm('border_radius', 'widget', 'img_free', $optionValue) !!}--}}
                        {{--<h5>Текст</h5>--}}
                        {{--<label>Цвет текста</label>--}}
                        {{--{!! $helper->getForm('color', 'widget', 'img_free', $optionValue) !!}--}}
                        {{--<label>Шрифт</label>--}}
                        {{--{!! $helper->getForm('align', 'widget', 'img_free', $optionValue) !!}--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-lg-6 pl-0">--}}
            {{--<h4>Текст над деревом</h4>--}}
            {{--<h5>Цвет</h5>--}}
            {{--{!! $helper->getForm('color', 'widget', 'h3_tree', $optionValue) !!}--}}
            {{--<h5>Шрифт</h5>--}}
            {{--{!! $helper->getForm('align', 'widget', 'h3_tree', $optionValue) !!}--}}
        {{--</div>--}}
        {{--<div class="clearfix"></div>--}}
        {{--<div class="col-lg-6 pr-0">--}}
            {{--<h4>Элементы операций</h4>--}}
            {{--<h5>Разделительные линии</h5>--}}
            {{--{!! $helper->getForm('border', 'widget', 'line_tree', $optionValue) !!}--}}
            {{--<div class="col-lg-12 p-0">--}}
                {{--{!! $helper->getForm('align', 'widget', 'table_tree', $optionValue) !!}--}}
            {{--</div>--}}
            {{--<div class="clearfix"></div>--}}
        {{--</div>--}}
        {{--<div class="col-lg-6 pr-0">--}}
            {{--<h5>Цвета заголовка таблицы</h5>--}}
            {{--<div class="col-lg-6 pl-0">--}}
                {{--<label>Цвет фона</label>--}}
                {{--{!! $helper->getForm('color', 'widget', 'th_tree', $optionValue, 'bg') !!}--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 pr-0">--}}
                {{--<label>Цвет текста</label>--}}
                {{--{!! $helper->getForm('color', 'widget', 'th_tree', $optionValue) !!}--}}
            {{--</div>--}}
            {{--<div class="clearfix"></div>--}}

            {{--<h4>Цвета транзакций</h4>--}}
            {{--<h5>Исходящие транзакции</h5>--}}
            {{--<div class="col-lg-6 pl-0">--}}
                {{--<label>Цвет фона</label>--}}

                {{--{!! $helper->getForm('color', 'widget', 'from_tree', $optionValue, 'bg') !!}--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 pr-0">--}}
                {{--<label>Цвет текста</label>--}}
                {{--{!! $helper->getForm('color', 'widget', 'from_tree', $optionValue) !!}--}}
            {{--</div>--}}
            {{--<div class="clearfix"></div>--}}
        {{--</div>--}}
        {{--<div class="col-lg-6 pr-0">--}}
            {{--<h5>Входящие транзакции</h5>--}}
            {{--<div class="col-lg-6 pl-0">--}}
                {{--<label>Цвет фона</label>--}}
                {{--{!! $helper->getForm('color', 'widget', 'to', $optionValue, 'bg') !!}--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 pr-0">--}}
                {{--<label>Цвет текста</label>--}}
                {{--{!! $helper->getForm('color', 'widget', 'to', $optionValue) !!}--}}
            {{--</div>--}}


            {{--<div class="clearfix"></div>--}}
            {{--<h5>Цвет при наведении</h5>--}}
            {{--<div class="col-lg-6 pl-0">--}}
                {{--<label>Фон при наведении</label>--}}
                {{--{!! $helper->getForm('color', 'widget', 'td_h_tree', $optionValue, 'bg') !!}--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 pr-0">--}}
                {{--<label>Текст при наведении</label>--}}
                {{--{!! $helper->getForm('color', 'widget', 'td_h_tree', $optionValue) !!}--}}
            {{--</div>--}}
            {{--<div class="clearfix"></div>--}}
        {{--</div>--}}
        {{--<div class="clearfix"></div>--}}

        {{--<div class="col-lg-12">--}}
            {{--<h4>Элементы</h4>--}}
        {{--</div>--}}
        {{--<div class="clearfix"></div>--}}
        {{--<div class="sort ui-state-default ui-sortable-handle element-rows element-hide example-disabled" id="rows-element-number">--}}
            {{--<div class="col-lg-4 pl-0">--}}
                {{--<input type="text" class="form-control" name="rows[element-number][head]" value="">--}}
            {{--</div>--}}
            {{--<div class="col-lg-3">--}}
                {{--<select class="form-control" name="rows[element-number][value]">--}}
                    {{--{!! \App\Libraries\Helpers::selectOption('value_transaction', '') !!}--}}
                {{--</select>--}}
            {{--</div>--}}
            {{--<div class="col-lg-2 pr-0 for-btns">--}}
            {{--</div>--}}
            {{--<div class="clearfix"></div>--}}
        {{--</div>--}}


        {{--<div class="sort-list element-rows-count">--}}
            {{--@if (isset($optionValue['rows']))--}}
                {{--@foreach($optionValue['rows'] as $key => $item)--}}
                    {{--<div class="sort ui-state-default" id="rows-{{$key}}">--}}
                        {{--<div class="col-lg-4 pl-0">--}}
                            {{--<input type="text" class="form-control" name="rows[{{$key}}][head]" value="{{$optionValue['rows'][$key]['head']}}">--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-3">--}}
                            {{--<select class="form-control selectpicker" name="rows[{{$key}}][value]">--}}
                                {{--{!! \App\Libraries\Helpers::selectOption('value_transaction', $optionValue['rows'][$key]['value']) !!}--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{--<div class="col-lg-2 pr-0 for-btns">--}}
                        {{--</div>--}}
                        {{--<div class="clearfix"></div>--}}
                    {{--</div>--}}
                {{--@endforeach--}}
            {{--@endif--}}
        {{--</div>--}}

        {{--<div class="for-btn-add" data-example=".element-rows"></div>--}}
        {{--<p class="btn-primary btn js-activate-sort-list">Настроить</p>--}}
        {{--<div class="clearfix"></div>--}}
    {{--</div>--}}
</div>