@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            {{--<li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">Элементы</a></li>--}}
            <li class="active"><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="style-setting">
                <div class="form-group col-lg-6 pl-0">
                    <h4>{{ __('main.main_setting') }}</h4>
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.number_records') }}</label>
                        <input type="number" class="form-control" name="last_news" value="{{(isset($optionValue['last_news']))?$optionValue['last_news']:''}}">
                    </div>
                    <div class="clearfix"></div>


                    <h4>{{ __('main.head_news') }}</h4>
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.bg_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'head', $optionValue, 'bg') !!}
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.text_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'head', $optionValue) !!}
                    </div>
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.date_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'date', $optionValue) !!}
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.align') }}</label>
                        {!! $helper->getForm('align', 'widget', 'head', $optionValue) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-lg-12 p-0">
                        <h5>{{ __('main.border') }}</h5>
                        {!! $helper->getForm('border', 'widget', 'head', $optionValue) !!}
                    </div>
                    <h4>{{ __('main.setting_time_line') }}</h4>
                    <h5>{{ __('main.area_date') }}</h5>
                    <div class="col-lg-4 pl-0">
                        <label>{{ __('main.bg_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'timeline_date', $optionValue, 'bg') !!}
                    </div>
                    <div class="col-lg-4 p-0">
                        <label>{{ __('main.color_line') }}</label>
                        {!! $helper->getForm('color', 'widget', 'timeline_line', $optionValue, 'bg') !!}
                    </div>
                    <div class="col-lg-4 pr-0">
                        <label>{{ __('main.text_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'timeline', $optionValue) !!}
                    </div>
                    <div class="col-lg-12 p-0">
                        <h5>{{ __('main.border_radius') }} {{ __('main.in_area_date') }}</h5>
                        {!! $helper->getForm('border_radius', 'widget', 'timeline', $optionValue) !!}
                    </div>
                    <div class="col-lg-12 p-0">
                        <h5>{{ __('main.border_radius') }} {{ __('main.in_area_icon') }}</h5>
                        {!! $helper->getForm('border_radius', 'widget', 'timeline_icon', $optionValue) !!}
                    </div>
                </div>
                <div class="form-group col-lg-6 p-0">
                    <h4>{{ __('main.text_news') }}</h4>
                    <div class="col-lg-4 pl-0">
                        <label>{{ __('main.bg_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'text', $optionValue, 'bg') !!}

                    </div>
                    <div class="col-lg-4">
                        <label>{{ __('main.text_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'text', $optionValue) !!}

                    </div>
                    <div class="col-lg-4 pr-0">
                        <label>{{ __('main.align_text_color') }}</label>
                        {!! $helper->getForm('align', 'widget', 'text', $optionValue) !!}
                    </div>
                    <h4>{{ __('main.footer-news') }}</h4>
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.bg_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'footer', $optionValue, 'bg') !!}

                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.align') }}</label>
                        {!! $helper->getForm('align', 'widget', 'footer', $optionValue) !!}
                    </div>
                    <h4>{{ __('main.design_btn') }}</h4>
                    {!! $helper->getForm('btn', 'widget', '', $optionValue) !!}

                </div>
                <div class="clearfix"></div>


            </div>

        </div>
    </div>
</div>