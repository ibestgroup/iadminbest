@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">{{ __('main.elemenst') }}</a></li>
            <li><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main-setting">
                <div class="col-lg-12">
                    <label>{{ __('main.place_avatar') }}</label>
                </div>

                <div class="clearfix"></div>

                <div class="col-lg-4 pl-0">
                    <select class="form-control" name="image">
                        {!! \App\Libraries\Helpers::selectOption('image', (isset($optionValue['image']))?$optionValue['image']:'') !!}
                    </select>
                </div>
                <div class="col-lg-3">
                    <select class="form-control selectpicker" name="text">
                        {!! \App\Libraries\Helpers::selectOption('value', (isset($optionValue['image']))?$optionValue['text']:'') !!}
                    </select>
                </div>
                <div class="col-lg-3">
                    <select class="form-control selectpicker" name="icon">
                        {!! \App\Libraries\Helpers::selectOption('icon', (isset($optionValue['icon']))?$optionValue['icon']:'') !!}
                    </select>
                </div>


                <div class="col-lg-12 mt-20">
                    <label>{{ __('main.elements') }}</label>
                </div>
                <div class="clearfix"></div>
                <div class="sort-list">
                    @if (isset($optionValue['list']))
                        @foreach($optionValue['list'] as $key => $item)
                            {{--<input type="text" class="form-control" name="head" placeholder="Название блока" value="{{$optionValue['head']}}">--}}
                            <div class="sort ui-state-default" id="list-{{$key}}">
                                <div class="col-lg-4 pl-0">
                                    <input type="text" class="form-control" name="list[{{$key}}][name]" value="{{$optionValue['list'][$key]['name']}}">
                                </div>
                                <div class="col-lg-3">
                                    <select class="form-control" name="list[{{$key}}][value]">
                                        {!! \App\Libraries\Helpers::selectOption('value', $optionValue['list'][$key]['value']) !!}
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select class="form-control selectpicker" name="list[{{$key}}][icon]">
                                        {!! \App\Libraries\Helpers::selectOption('icon', $optionValue['list'][$key]['icon']) !!}
                                    </select>
                                </div>
                                <div class="col-lg-2 pr-0 for-btns">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="for-btn-add"></div>
                <p class="btn-primary btn js-activate-sort-list">{{ __('main.configure') }}</p>
                <div class="clearfix"></div>

            </div>

            <div class="tab-pane" id="style-setting">
                <h4>{{ __('main.image') }}</h4>
                {{--<h5>Ширина</h5>--}}
                <div class="form-group col-lg-12 p-0">
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.width') }}</label>
                        <input type="number" class="form-control" name="widget_style[width_img]" value="{{(isset($optionValue['widget_style']['width_img'])?$optionValue['widget_style']['width_img']:'')}}">
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.text_color') }}</label>
                        <input type="color" class="form-control" name="widget_style[color_login]" value="{{(isset($optionValue['widget_style']['color_login'])?$optionValue['widget_style']['color_login']:'')}}">
                    </div>
                </div>
                <h5>{{ __('main.border_radius') }}</h5>
                <div class="form-group col-lg-12 p-0">
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.top_left') }}</label>
                        <input type="number" class="form-control" name="widget_style[border_radius_tl]" value="{{(isset($optionValue['widget_style']['border_radius_tl'])?$optionValue['widget_style']['border_radius_tl']:'')}}">
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.top_right') }}</label>
                        <input type="number" class="form-control" name="widget_style[border_radius_tr]" value="{{(isset($optionValue['widget_style']['border_radius_tr'])?$optionValue['widget_style']['border_radius_tr']:'')}}">
                    </div>
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.bottom_left') }}</label>
                        <input type="number" class="form-control" name="widget_style[border_radius_bl]" value="{{(isset($optionValue['widget_style']['border_radius_bl'])?$optionValue['widget_style']['border_radius_bl']:'')}}">
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.bottom_right') }}</label>
                        <input type="number" class="form-control" name="widget_style[border_radius_br]" value="{{(isset($optionValue['widget_style']['border_radius_br'])?$optionValue['widget_style']['border_radius_br']:'')}}">
                    </div>
                </div>
                <div class="clearfix"></div>
                <h5>{{ __('main.border') }}</h5>
                <div class="form-group col-lg-12 p-0">
                    <div class="col-lg-4 pl-0">
                        <label>{{ __('main.width') }}</label>
                        <input type="number" class="form-control" name="widget_style[border_width_img]" value="{{(isset($optionValue['widget_style']['border_width_img'])?$optionValue['widget_style']['border_width_img']:'')}}">
                    </div>
                    <div class="col-lg-4">
                        <label>{{ __('main.color') }}</label>
                        <input type="color" class="form-control" name="widget_style[border_color_img]" placeholder="Цвет фона" value="{{(isset($optionValue['widget_style_img']['border_color_img']))?$optionValue['widget_style']['border_color_img']:''}}">
                    </div>
                    <div class="col-lg-4 pr-0">
                        <label>{{ __('main.type') }}</label>
                        <select class="form-control" name="widget_style[border_style_img]">
                            {!! $helper->selectOption('type_border', (isset($optionValue['widget_style']['border_style_img'])?$optionValue['widget_style']['border_style_img']:'')) !!}
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>

                <h4>{{ __('main.elements_color') }}</h4>

                <div class="col-lg-4 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    <input type="color" class="form-control" name="widget_style[bg_color_elements]" placeholder="Цвет фона" value="{{(isset($optionValue['widget_style']['bg_color_elements']))?$optionValue['widget_style']['bg_color_elements']:''}}">
                </div>
                <div class="col-lg-4 p-0">
                    <label>{{ __('main.text_left') }}</label>
                    <input type="color" class="form-control" name="widget_style[color_tl]" placeholder="Цвет фона" value="{{(isset($optionValue['widget_style']['color_tl']))?$optionValue['widget_style']['color_tl']:''}}">
                </div>
                <div class="col-lg-4 p-0">
                    <label>{{ __('main.text_right') }}</label>
                    <input type="color" class="form-control" name="widget_style[color_tr]" placeholder="Цвет фона" value="{{(isset($optionValue['widget_style']['color_tr']))?$optionValue['widget_style']['color_tr']:''}}">
                </div>
                <div class="clearfix"></div>
                <h4>{{ __('main.dividing_lines') }}</h4>
                <div class="form-group col-lg-12 p-0">
                    <div class="col-lg-4 pl-0">
                        <label>{{ __('main.width') }}</label>
                        <input type="number" class="form-control" name="widget_style[border_width_line]" value="{{(isset($optionValue['widget_style']['border_width_line'])?$optionValue['widget_style']['border_width_line']:'')}}">
                    </div>
                    <div class="col-lg-4">
                        <label>{{ __('main.color') }}</label>
                        <input type="color" class="form-control" name="widget_style[border_color_line]" placeholder="Цвет фона" value="{{(isset($optionValue['widget_style']['border_color_line']))?$optionValue['widget_style']['border_color_line']:''}}">
                    </div>
                    <div class="col-lg-4 pr-0">
                        <label>{{ __('main.type') }}</label>
                        <select class="form-control" name="widget_style[border_style_line]">
                            {!! $helper->selectOption('type_border', (isset($optionValue['widget_style']['border_style_line'])?$optionValue['widget_style']['border_style_line']:'')) !!}
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>