@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">{{ __('main.elements') }}</a></li>
            <li><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main-setting">
                <div class="col-lg-12">
                    <label>{{ __('main.place_avatar') }}</label>
                </div>
                @php
                    $optionValue = \App\Libraries\Helpers::ifIsset($optionValue);
                @endphp
                <div class="clearfix"></div>

                <div class="col-lg-4 pl-0">
                    <select class="form-control" name="image">
                        {!! \App\Libraries\Helpers::selectOption('image', (isset($optionValue['image']))?$optionValue['image']:'') !!}
                    </select>
                </div>
                <div class="col-lg-3">
                    <select class="form-control selectpicker" name="text">
                        {!! \App\Libraries\Helpers::selectOption('value', (isset($optionValue['image']))?$optionValue['text']:'') !!}
                    </select>
                </div>
                <div class="col-lg-3">
                    <select class="form-control selectpicker" name="icon">
                        {!! \App\Libraries\Helpers::selectOption('icon', (isset($optionValue['icon']))?$optionValue['icon']:'') !!}
                    </select>
                </div>




                <div class="col-lg-12 mt-20">
                    <label>{{ __('main.elements') }}</label>
                </div>
                <div class="clearfix"></div>
                <div class="sort ui-state-default ui-sortable-handle element-list element-hide example-disabled" id="list-element-number">
                    <div class="col-lg-4 pl-0">
                        <input type="text" class="form-control" name="list[element-number][name]" value="">
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="list[element-number][value]">
                            {!! \App\Libraries\Helpers::selectOption('value', '') !!}
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="list[element-number][icon]">
                            {!! \App\Libraries\Helpers::selectOption('icon', '') !!}
                        </select>
                    </div>
                    <div class="col-lg-2 pr-0 for-btns">
                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="sort-list element-list-count" data-example=".element-list">
                    @if (isset($optionValue['list']))
                        @foreach($optionValue['list'] as $key => $item)
                            <div class="sort ui-state-default" id="list-{{$key}}">
                                <div class="col-lg-4 pl-0">
                                    <input type="text" class="form-control" name="list[{{$key}}][name]" value="{{$optionValue['list'][$key]['name']}}">
                                </div>
                                <div class="col-lg-3">
                                    <select class="form-control selectpicker" name="list[{{$key}}][value]">
                                        {!! \App\Libraries\Helpers::selectOption('value', $optionValue['list'][$key]['value']) !!}
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select class="form-control selectpicker" name="list[{{$key}}][icon]">
                                        {!! \App\Libraries\Helpers::selectOption('icon', $optionValue['list'][$key]['icon']) !!}
                                    </select>
                                </div>
                                <div class="col-lg-2 pr-0 for-btns">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="for-btn-add" data-example=".element-list"></div>
                <p class="btn-primary btn js-activate-sort-list">{{ __('main.configure') }}</p>
                <div class="clearfix"></div>



                <h4>{{ __('main.btn') }}</h4>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.text') }}</label>
                    <input type="text" class="form-control" name="link[text]" value="{{(isset($optionValue['link']['text']))?$optionValue['link']['text']:''}}">
                </div>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.link') }}</label>
                    <input type="text" class="form-control" name="link[link]" value="{{(isset($optionValue['link']['link']))?$optionValue['link']['link']:''}}">
                </div>

                <div class="clearfix"></div>

            </div>

            <div class="tab-pane" id="style-setting">
                <h4>{{ __('main.image') }}</h4>
                <div class="form-group col-lg-12 p-0">
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.width') }}</label>
                        {!! $helper->getForm('width', 'widget', 'img', $optionValue) !!}
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.text_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'login', $optionValue) !!}
                    </div>
                </div>
                <h5>{{ __('main.border_radius') }}</h5>
                <div class="form-group col-lg-12 p-0">
                    {!! $helper->getForm('border_radius', 'widget', '', $optionValue) !!}
                </div>
                <div class="clearfix"></div>

                <h5>{{ __('main.border') }}</h5>
                {!! $helper->getForm('border', 'widget', 'img', $optionValue) !!}
                <div class="clearfix"></div>

                <h4>{{ __('main.color') }}</h4>

                <div class="col-lg-4 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'elements', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-4 p-0">
                    <label>{{ __('main.text_left') }}</label>
                    {!! $helper->getForm('color', 'widget', 'tl', $optionValue) !!}

                </div>
                <div class="col-lg-4 p-0">
                    <label>{{ __('main.text_right') }}</label>
                    {!! $helper->getForm('color', 'widget', 'tr', $optionValue) !!}
                </div>
                <div class="clearfix"></div>
                <h4>{{ __('main.divifing_lines') }}</h4>

                {!! $helper->getForm('border', 'widget', 'line', $optionValue) !!}
                <div class="clearfix"></div>

                <h4>{{ __('main.design_btn') }}</h4>
                {!! $helper->getForm('btn', 'widget', '', $optionValue) !!}
            </div>
        </div>
    </div>
</div>