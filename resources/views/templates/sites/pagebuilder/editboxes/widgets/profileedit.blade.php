@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">{{ __('main.elements') }}</a></li>
            <li><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main-setting">

                {{--<div class="col-lg-12">--}}
                    {{--<label>Элементы</label>--}}
                {{--</div>--}}
                {{--<div class="clearfix"></div>--}}
                {{--<div class="sort ui-state-default ui-sortable-handle element-rows element-hide example-disabled" id="rows-element-number">--}}
                    {{--<div class="col-lg-4 pl-0">--}}
                        {{--<input type="text" class="form-control" name="rows[element-number][head]" value="">--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-3">--}}
                        {{--<select class="form-control" name="rows[element-number][value]">--}}
                            {{--{!! \App\Libraries\Helpers::selectOption('value_transaction', '') !!}--}}
                        {{--</select>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-2 pr-0 for-btns">--}}
                    {{--</div>--}}
                    {{--<div class="clearfix"></div>--}}
                {{--</div>--}}


                {{--<div class="sort-list element-rows-count">--}}
                    {{--@if (isset($optionValue['rows']))--}}
                        {{--@foreach($optionValue['rows'] as $key => $item)--}}
                            {{--<div class="sort ui-state-default" id="rows-{{$key}}">--}}
                                {{--<div class="col-lg-4 pl-0">--}}
                                    {{--<input type="text" class="form-control" name="rows[{{$key}}][head]" value="{{$optionValue['rows'][$key]['head']}}">--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-3">--}}
                                    {{--<select class="form-control selectpicker" name="rows[{{$key}}][value]">--}}
                                        {{--{!! \App\Libraries\Helpers::selectOption('value_transaction', $optionValue['rows'][$key]['value']) !!}--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-2 pr-0 for-btns">--}}
                                {{--</div>--}}
                                {{--<div class="clearfix"></div>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}
                    {{--@endif--}}
                {{--</div>--}}

                {{--<div class="for-btn-add" data-example=".element-rows"></div>--}}
                {{--<p class="btn-primary btn js-activate-sort-list">Настроить</p>--}}
                {{--<div class="clearfix"></div>--}}

                <div class="col-lg-12 mt-20">
                    <label>{{ __('main.elements') }}</label>
                </div>

                <div class="sort ui-state-default ui-sortable-handle element-input element-hide example-disabled" id="input-element-number">
                    <div class="col-lg-4 pl-0">
                        <input type="text" class="form-control" name="input[element-number][name]">
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="input[element-number][value]">
                            {!! \App\Libraries\Helpers::selectOption('value_input', '') !!}
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="input[element-number][icon]">
                            {!! \App\Libraries\Helpers::selectOption('icon', '') !!}
                        </select>
                    </div>
                    <div class="col-lg-2 pr-0 for-btns">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="sort-list element-input-count">
                    @if (isset($optionValue['input']))
                        @foreach($optionValue['input'] as $key => $item)
                            <div class="sort ui-state-default" id="input-{{$key}}">
                                <div class="col-lg-4 pl-0">
                                    <input type="text" class="form-control" name="input[{{$key}}][name]" value="{{$optionValue['input'][$key]['name']}}">
                                </div>
                                <div class="col-lg-3">
                                    <select class="form-control" name="input[{{$key}}][value]">
                                        {!! \App\Libraries\Helpers::selectOption('value_input', $optionValue['input'][$key]['value']) !!}
                                    </select>
                                </div>
                                <div class="col-lg-3">
                                    <select class="form-control selectpicker" name="input[{{$key}}][icon]">
                                        {!! \App\Libraries\Helpers::selectOption('icon', $optionValue['input'][$key]['icon']) !!}
                                    </select>
                                </div>
                                <div class="col-lg-2 pr-0 for-btns">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    @endif
                </div>

                <div class="for-btn-add" data-example=".element-input"></div>
                <p class="btn-primary btn js-activate-sort-list">{{ __('main.configure') }}</p>
                <div class="clearfix"></div>

            </div>

            <div class="tab-pane" id="style-setting">
                <div class="form-group col-lg-6 pl-0">
                    <h4>{{ __('main.image') }}</h4>
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.width') }}</label>
                        {!! $helper->getForm('width', 'widget', 'avatar', $optionValue) !!}
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.text_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'avatar', $optionValue) !!}
                    </div>
                    <div class="form-group col-lg-12 p-0">
                        {!! $helper->getForm('border', 'widget', 'avatar', $optionValue) !!}
                    </div>
                    <div class="form-group col-lg-12 p-0">
                        {!! $helper->getForm('border_radius', 'widget', 'avatar', $optionValue) !!}
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-6 pr-0">
                    <h4>{{ __('main.text') }}</h4>
                    <div class="col-lg-3 pl-0">
                        <label>{{ __('main.color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'text', $optionValue, 'label') !!}
                    </div>
                    <div class="col-lg-9 pr-0">
                        <label>{{ __('main.font') }}</label>
                        {!! $helper->getForm('align', 'widget', 'text', $optionValue, 'label') !!}
                    </div>
                    <div class="clearfix"></div>

                    <h4>{{ __('main.design_inputs') }}</h4>
                    <h5>{{ __('main.color') }}</h5>
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.bg_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'input', $optionValue, 'bg') !!}
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.text_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'input', $optionValue) !!}
                    </div>

                    <div class="clearfix"></div>
                    <h5>{{ __('main.border') }}</h5>
                    {!! $helper->getForm('border', 'widget', 'input', $optionValue) !!}

                    <h5>{{ __('main.border_radius') }}</h5>
                    {!! $helper->getForm('border_radius', 'widget', 'input', $optionValue) !!}
                    <div class="clearfix"></div>

                    <h4>{{ __('main.setting-button') }}</h4>
                    {!! $helper->getForm('btn', 'widget', '', $optionValue) !!}

                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>