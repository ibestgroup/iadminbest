@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            {{--<li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">Элементы</a></li>--}}
            <li class="active"><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="style-setting">

                <h4>{{ __('main.design_banner') }}</h4>
                <h5>{{ __('main.border') }}</h5>
                {!! $helper->getForm('border', 'widget', 'img', $optionValue) !!}
                <div class="clearfix"></div>

                <h4>{{ __('main.design_head_banner') }}</h4>
                <h5>{{ __('main.color') }}</h5>
                <div class="col-lg-12 p-0">
                    <div class="col-lg-3 pl-0">
                        <label>{{ __('main.bg_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'head', $optionValue, 'bg') !!}
                    </div>
                    <div class="col-lg-3 p-0">
                        <label>{{ __('main.text_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'head', $optionValue) !!}
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.font') }}</label>
                        {!! $helper->getForm('align', 'widget', 'head', $optionValue) !!}
                    </div>
                </div>
                <h5>{{ __('main.design_element_banner') }}</h5>
                <div class="col-lg-12 p-0">
                    <div class="col-lg-6 pl-0">
                        <label>{{ __('main.border_radius') }}</label>
                        {!! $helper->getForm('border_radius', 'widget', 'promo', $optionValue) !!}
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.bg_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'promo', $optionValue, 'bg') !!}
                        <div class="clearfix"></div>
                        <h5>{{ __('main.border') }}</h5>
                        {!! $helper->getForm('border', 'widget', 'promo', $optionValue) !!}
                    </div>
                </div>
                <h4>{{ __('main.ref_link') }}</h4>
                <h5>{{ __('main.color') }}</h5>
                <div class="col-lg-12 p-0">
                    <div class="col-lg-3 pl-0">
                        <label>{{ __('main.bg_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'reflink', $optionValue, 'bg') !!}
                    </div>
                    <div class="col-lg-3 p-0">
                        <label>{{ __('main.text_color') }}</label>
                        {!! $helper->getForm('color', 'widget', 'reflink', $optionValue) !!}
                    </div>
                    <div class="col-lg-6 pr-0">
                        <label>{{ __('main.font') }}</label>
                        {!! $helper->getForm('align', 'widget', 'reflink', $optionValue) !!}
                    </div>
                    <div class="col-lg-4 pl-0">
                        <label>{{ __('main.padding') }}</label>
                        {!! $helper->getForm('padding', 'widget', 'reflink', $optionValue) !!}
                    </div>
                    <div class="col-lg-4 p-0">
                        <label>{{ __('main.border_radius') }}</label>
                        {!! $helper->getForm('border_radius', 'widget', 'reflink', $optionValue) !!}
                    </div>
                    <div class="col-lg-4 pr-0">
                        <label>{{ __('main.border') }}</label>
                        {!! $helper->getForm('border', 'widget', 'reflink', $optionValue) !!}
                    </div>
                </div>
                <h4>{{ __('main.design_input_with_code') }}</h4>
                <h5>{{ __('main.color') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'input', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'input', $optionValue) !!}
                </div>

                <div class="clearfix"></div>
                <h5>{{ __('main.border') }}</h5>
                {!! $helper->getForm('border', 'widget', 'input', $optionValue) !!}

                <h5>{{ __('main.border_radius') }}</h5>
                {!! $helper->getForm('border_radius', 'widget', 'input', $optionValue) !!}
                <div class="clearfix"></div>

            </div>

        </div>
    </div>
</div>