@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">{{ __('main.elements') }}</a></li>
            <li><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main-setting">
                <h4>{{ __('main.admin_setting') }}</h4>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.number_records') }}</label>
                    <input type="number" class="form-control" name="last_referrals" value="{{(isset($optionValue['last_referrals'])?$optionValue['last_referrals']:'')}}">
                    <input type="checkbox" name="first_page" value="1" {{(isset($optionValue['first_page'])?($optionValue['first_page'] == 1)?'checked=checked':'':'')}} {{(isset($optionValue['random_referrals'])?($optionValue['random_referrals'] == 1)?'disabled=disabled':'':'')}}>
                    {{ __('main.only_first_page') }}
                    <br>
                    <input type="checkbox" name="random_referrals" value="1" {{(isset($optionValue['random_referrals'])?($optionValue['random_referrals'] == 1)?'checked=checked':'':'')}}>
                    {{ __('main.show_random_ref') }}
                </div>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.what_display') }}</label>
                    <br>
                    <input type="checkbox" name="active"  value="1" {{(isset($optionValue['active']))?($optionValue['active']==1)?'checked=checked':'':''}}> {{ __('main.active_ref') }}
                    <br>
                    <input type="checkbox" name="not_active"  value="1" {{(isset($optionValue['not_active']))?($optionValue['not_active']==1)?'checked=checked':'':''}}> {{ __('main.non_active_ref') }}
                    <br>
                </div>



                <div class="col-lg-12">
                    <label>{{ __('main.elements') }}</label>
                </div>

                <div class="sort ui-state-default ui-sortable-handle element-list element-hide example-disabled" id="list-element-number">
                    <div class="col-lg-4 pl-0">
                        <input type="text" class="form-control" name="list[element-number][name]" value="">
                    </div>
                    <div class="col-lg-4">
                        <select class="form-control" name="list[element-number][value]">
                            {!! \App\Libraries\Helpers::selectOption('value', '') !!}
                        </select>
                    </div>
                    <div class="col-lg-2">
                        <select class="form-control" name="list[element-number][icon]">
                            {!! \App\Libraries\Helpers::selectOption('icon', '') !!}
                        </select>
                    </div>
                    <div class="col-lg-2 pr-0 for-btns">
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>
                <div class="sort-list element-list-count" data-example=".element-list">
                    @if (isset($optionValue['list']))
                        @foreach($optionValue['list'] as $key => $item)
                            <div class="sort ui-state-default" id="list-{{$key}}">
                                <div class="col-lg-4 pl-0">
                                    <input type="text" class="form-control" name="list[{{$key}}][name]" value="{{(isset($optionValue['list'][$key]['name']))?$optionValue['list'][$key]['name']:''}}">
                                </div>
                                <div class="col-lg-4">
                                    <select class="form-control selectpicker" name="list[{{$key}}][value]">
                                        {!! \App\Libraries\Helpers::selectOption('value', $optionValue['list'][$key]['value']) !!}
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <select class="form-control selectpicker" name="list[{{$key}}][icon]">
                                        {!! \App\Libraries\Helpers::selectOption('icon', (isset($optionValue['list'][$key]['icon']))?$optionValue['list'][$key]['icon']:'') !!}
                                    </select>
                                </div>
                                <div class="col-lg-2 pr-0 for-btns">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="for-btn-add" data-example=".element-list">

                </div>
                <p class="btn-primary btn js-activate-sort-list">{{ __('main.configure') }}</p>
                <div class="clearfix"></div>
            </div>
            <div class="tab-pane" id="style-setting">
                <h4>{{ __('main.column_with_value') }}</h4>
                <h5>{{ __('main.divining_lines') }}</h5>
                {!! $helper->getForm('border', 'widget', 'line', $optionValue) !!}
                <div class="col-lg-6">
                    {!! $helper->getForm('align', 'widget', 'table', $optionValue) !!}
                </div>
                <div class="clearfix"></div>


                <h5>{{ __('main.table_head_color') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'th', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'th', $optionValue) !!}
                </div>
                <div class="clearfix"></div>

                <h4>{{ __('main.users_color') }}</h4>
                <h5>{{ __('main.activity') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>

                    {!! $helper->getForm('color', 'widget', 'activate', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'activate', $optionValue) !!}
                </div>
                <div class="clearfix"></div>


                <h5>{{ __('main.unactivity') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'not_activate', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'not_activate', $optionValue) !!}
                </div>


                <div class="clearfix"></div>
                <h5>{{ __('main.hover_color') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'td_h', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'td_h', $optionValue) !!}
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 p-0">
                    <label>{{ __('main.pagination') }}</label>
                    {!! $helper->getForm('pagination', 'widget', '', $optionValue) !!}
                </div>
                <div class="clearfix"></div>


            </div>
        </div>
    </div>

</div>