@inject('helper', "\App\Libraries\Helpers")
<div class="form-group">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#main-setting" data-toggle="tab" aria-expanded="true">{{ __('main.elements') }}</a></li>
            <li><a href="#style-setting" data-toggle="tab" aria-expanded="true">{{ __('main.decor') }}</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="main-setting">
                <h4>{{ __('main.admin_settings') }}</h4>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.number_records') }}</label>
                    <input type="number" class="form-control" name="last_transaction" value="{{(isset($optionValue['last_transaction'])?$optionValue['last_transaction']:'')}}">

                    <input type="checkbox" name="first_page" value="1" {{(isset($optionValue['first_page'])?($optionValue['first_page'] == 1)?'checked="checked"':'':'')}}> {{ __('main.only_first_page') }}
                </div>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.what_display') }}</label>
                    <br>
                    <input type="checkbox" name="payments_order"  value="1" {{(isset($optionValue['payments_order']))?($optionValue['payments_order']==1)?'checked="checked"':'':''}}> {{ __('main.transaction_payments_order') }}
                    <br>
                    <input type="checkbox" name="cashout"  value="1" {{(isset($optionValue['cashout']))?($optionValue['cashout']==1)?'checked="checked"':'':''}}> {{ __('main.transaction_cashout') }}
                    <br>
                    <input type="checkbox" name="payment"  value="1" {{(isset($optionValue['payment']))?($optionValue['payment']==1)?'checked="checked"':'':''}}> {{ __('main.transaction_payment') }}
                    <br>
                </div>


                <div class="col-lg-12">
                    <label>{{ __('main.elements') }}</label>
                </div>
                <div class="clearfix"></div>
                <div class="sort ui-state-default ui-sortable-handle element-rows element-hide example-disabled" id="rows-element-number">
                    <div class="col-lg-4 pl-0">
                        <input type="text" class="form-control" name="rows[element-number][head]" value="">
                    </div>
                    <div class="col-lg-3">
                        <select class="form-control" name="rows[element-number][value]">
                            {!! \App\Libraries\Helpers::selectOption('value_transaction', '') !!}
                        </select>
                    </div>
                    <div class="col-lg-2 pr-0 for-btns">
                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="sort-list element-rows-count">
                    @if (isset($optionValue['rows']))
                        @foreach($optionValue['rows'] as $key => $item)
                        <div class="sort ui-state-default" id="rows-{{$key}}">
                            <div class="col-lg-4 pl-0">
                                <input type="text" class="form-control" name="rows[{{$key}}][head]" value="{{$optionValue['rows'][$key]['head']}}">
                            </div>
                            <div class="col-lg-3">
                                <select class="form-control selectpicker" name="rows[{{$key}}][value]">
                                    {!! \App\Libraries\Helpers::selectOption('value_transaction', $optionValue['rows'][$key]['value']) !!}
                                </select>
                            </div>
                            <div class="col-lg-2 pr-0 for-btns">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        @endforeach
                    @endif
                </div>

                <div class="for-btn-add" data-example=".element-rows"></div>
                <p class="btn-primary btn js-activate-sort-list">{{ __('main.configure') }}</p>
                <div class="clearfix"></div>
            </div>



            <div class="tab-pane" id="style-setting">
                <h4>{{ __('main.element_transaction') }}</h4>
                <h5>{{ __('main.dividing_lines') }}</h5>
                {!! $helper->getForm('border', 'widget', 'line', $optionValue) !!}
                <div class="col-lg-6 p-0">
                    {!! $helper->getForm('align', 'widget', 'table', $optionValue) !!}
                </div>
                <div class="clearfix"></div>


                <h5>{{ __('main.table_head_color') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'th', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'th', $optionValue) !!}
                </div>
                <div class="clearfix"></div>

                <h4>{{ __('main.color_transaction') }}</h4>
                <h5>{{ __('main.out_transaction') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>

                    {!! $helper->getForm('color', 'widget', 'from', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>{{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'from', $optionValue) !!}
                </div>
                <div class="clearfix"></div>


                <h5>{{ __('main.in_transaction') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'to', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>Цвет {{ __('main.text_color') }}/label>
                    {!! $helper->getForm('color', 'widget', 'to', $optionValue) !!}
                </div>


                <div class="clearfix"></div>
                <h5>{{ __('main.hover_color') }}</h5>
                <div class="col-lg-6 pl-0">
                    <label>{{ __('main.bg_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'td_h', $optionValue, 'bg') !!}
                </div>
                <div class="col-lg-6 pr-0">
                    <label>Текст {{ __('main.text_color') }}</label>
                    {!! $helper->getForm('color', 'widget', 'td_h', $optionValue) !!}
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 p-0">
                    <label>{{ __('main.pagination') }}</label>
                    {!! $helper->getForm('pagination', 'widget', '', $optionValue) !!}
                </div>
                <div class="clearfix"></div>


            </div>
        </div>
    </div>

</div>