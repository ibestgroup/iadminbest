<div class="modal fade" id="profile-builder-grid">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" style="text-align: center; color: #FF8D00;">{{ __('main.modal_add_area') }}</h4>
            </div>
            <div class="modal-body">
                <p>
                <form action="{{route('profileBuilder')}}" method="post" class="js-profile-builder">
                    @csrf
                    <div class="clearfix"></div>
                    <div class="col-lg-12">
                        <input type="hidden" value="{{$page}}" name="page">
                        <input type="hidden" name="type" value="grid">
                        <div class="form-group">
                            <label>{{ __('main.modal_name_area') }}</label>
                            <input type="text" class="form-control" name="name_ru" placeholder="{{ __('main.modal_name_area') }}">
                        </div>
                        <div class="form-group">
                            <label>{{ __('main.modal_place_area') }}</label>
                            <select class="form-control selectpicker" name="grid">
                                <option value="content">{{ __('main.in_main') }}</option>
                                <option value="left">{{ __('main.in_left_menu') }}</option>
                                <option value="top">{{ __('main.in_top_menu') }}</option>
                                <option value="right">{{ __('main.in_right_menu') }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{ __('main.width') }}</label>
                            <select class="form-control selectpicker" name="col">
                                {!! \App\Libraries\Helpers::selectOption('column') !!}
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12 mt-10">
                        <input type="submit" class="btn btn-primary" value="{{ __('main.create') }}">
                    </div>
                    <div class="clearfix"></div>
                </form>
                </p>
            </div>
            <div class="modal-footer">
                {{--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>--}}
                <button type="button" class="btn btn-primary button-close" data-dismiss="modal">{{ __('main.close') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade grid-list-refresh" id="profile-builder-box">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" style="text-align: center; color: #FF8D00;">{{ __('main.modal_add_widget') }}</h4>
            </div>
            <div class="modal-body">
                <p>
                <form action="{{route('profileBuilder')}}" method="post" class="js-profile-builder">
                    @csrf
                    <input type="hidden" value="{{$page}}" name="page">
                    <div class="col-lg-12">
                        <div class="col-lg-9">
                            <div class="form-group">
                                <label>{{ __('main.modal_name_widget') }}</label>
                                <input type="text" class="form-control" name="name_ru" placeholder="{{ __('main.modal_name_widget') }}">
                            </div>
                            <div class="form-group">
                                <label>{{ __('main.modal_place_widget') }}</label>
                                <select class="form-control selectpicker" name="grid">
                                    @php
                                        $template = \App\Libraries\Helpers::siteTemplate();
                                        $items = \App\Models\SiteElements::where('type', 'grid')->where('template', $template)->where(function ($query) use ($page) {
                                            $query->where('page', $page)
                                                    ->orWhere('page', 'all');})->get();

                                        $arrayArea = [
                                            'content' => __('main.in_main'),
                                            'top' => __('main.in_left_menu'),
                                            'left' => __('main.in_top_menu'),
                                            'right' => __('main.in_right_menu')
                                            ];
                                    @endphp

                                    @foreach($arrayArea as $key => $area)

                                        @php
                                            $template = \App\Libraries\Helpers::siteTemplate();
                                            $items = \App\Models\SiteElements::where('type', 'grid')->where('template', $template)->where(function ($query) use ($page) {
                                                $query->where('page', $page)
                                                        ->orWhere('page', 'all');})->where('parent_id', $key)->get();

                                            $arrayArea = ['content', 'top', 'left', 'right'];
                                        @endphp

                                        <optgroup label="{{$area}}">
                                            @forelse($items as $item)
                                                <option value="{{$item['id_in_system']}}">{{$item['name_ru']}}</option>
                                            @empty
                                                <option disabled>{{ __('main.modal_create_area_on_page') }}</option>
                                            @endforelse
                                        </optgroup>

                                    @endforeach


                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{ __('main.modal_widget') }}</label>
                                <select class="form-control selectpicker" name="widget">
                                    {{--<option value="profilebox">Аватар с информацией</option>--}}
                                    {{--<option value="transaction">Блок с операциями</option>--}}
                                    {{--<option value="balance">Форма пополнения</option>--}}
                                    {{--<option value="referrals">Список рефералов</option>--}}
                                    {{--<option value="news">Новости</option>--}}
                                    {{--<option value="faq">Частозадаваемые вопросы</option>--}}
                                    {{--<option value="promo">Промоматериалы</option>--}}
                                    {{--<option value="profileedit">Редактирование профиля</option>--}}
                                    {{--<option value="cashout">Форма вывода</option>--}}
                                    {{--<option value="activate">Активация тарифов</option>--}}
                                    {{--<option value="tarifs">Мои тарифы</option>--}}
                                    {{--<option value="content">С текстом</option>--}}
                                    {{--<option value="store">Тарифы для инвестиций</option>--}}
                                    {{--<option value="mydeposits">Мои вклады</option>--}}
                                    {{--<option value="horizontalmenu">Горизонтальное меню</option>--}}
                                    {{--<option value="verticalmenu">Вертикальное меню</option>--}}
                                    {!! \App\Libraries\Helpers::selectOption('widgets', '', ['widget' => (isset($_GET['widget']))?$_GET['widget']:'']) !!}

                                </select>
                            </div>
                            <div class="form-group refresh-select-box">
                                <label>{{ __('main.type') }}</label>
                                <select class="form-control selectpicker" name="type">
                                    {{--<option value="mainbox">Обычный блок</option>--}}
                                    {{--<option value="smallbox">Маленький блок</option>--}}
                                    {!! \App\Libraries\Helpers::selectOption('boxes', '', ['widget' => (isset($_GET['widget']))?$_GET['widget']:'']) !!}
                                </select>
                            </div>
                            <div class="form-group">
                                <label>{{ __('main.width') }}</label>
                                <select class="form-control selectpicker" name="col">
                                    {!! \App\Libraries\Helpers::selectOption('column') !!}
                                </select>
                            </div>
                            <label>{{ __('main.modal_head_widget') }}</label>
                            <input type="text" class="form-control" name="head" placeholder="{{ __('main.modal_head_widget') }}">
                        </div>
                        <div class="col-lg-12">
                            <input type="submit" class="btn btn-primary mt-10" value="{{ __('main.create') }}">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </form>
                </p>
            </div>
            <div class="modal-footer">
                {{--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>--}}
                <button type="button" class="btn btn-primary button-close" data-dismiss="modal">{{ __('main.close') }}</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="profile-builder-box-edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" style="text-align: center; color: #FF8D00;">{{ __('main.modal_edit_block') }}</h4>
            </div>
            <div class="modal-body">
                <p>
                <form action="{{route('updateElement')}}" method="post" class="js-profile-builder">
                    <input type="hidden" value="{{$page}}" name="page">
                    @csrf
                    @php
                        $typeBlock = '';
                        if (isset($_GET['type'])) {
                            $typeBlock = $_GET['type'];
                        }

                        $idBlock = 0;
                        if (isset($_GET['id'])) {
                            $idBlock = $_GET['id'];
                        }

                        $block = \App\Models\SiteElements::where('id_in_system', $idBlock)->where('template', \App\Libraries\Helpers::siteTemplate())->first();
                        $block['option_value'] = unserialize($block['option_value']);

                        $type = (isset($block['type']))?$block['type']:'';
                    @endphp
                    <div class="refresh-box-edit" id="edit-form-box" data-block-id="1" data-link="{{\Illuminate\Support\Facades\Request::url()}}">
                        @if($type == 'grid')
                            <div class="form-group">
                                <label>{{ __('main.width') }}</label>
                                <input type="hidden" name="block_id" value="{{$idBlock}}">
                                <select class="form-control" name="col">
                                    {!! \App\Libraries\Helpers::selectOption('column', $block['option_value']['col']) !!}
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        @else

                            <h3 class="ta-center">{{ __('main.modal_editor_block') }}</h3>
                            <div class="col-lg-12 refresh-edit-box" style="padding: 20px 20px 0px;  background-image: url(https://skaimgs.appspot.com/kak-sdelat-fon-v-fotoshope-iarche/img_88.jpg);">

                                @if ($idBlock)
                                    <input type="hidden" name="block_id" value="{{$idBlock}}">
                                    {{--{{dd($idBlock)}}--}}
                                    @include('templates.sites.pagebuilder.boxes.'.$type, array(
                                        'section' => 'body',
                                        'item' => $block,
                                        'optionValue' => $block['option_value'],
                                    ))
                                @endif
                            </div>

                            <div class="col-lg-12 for-input-post">
                                <div class="nav-tabs-custom mt-10">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#block-settings" data-toggle="tab" aria-expanded="true">{{ __('main.modal_setting_block') }}</a></li>
                                        <li><a href="#widget-settings" data-toggle="tab" aria-expanded="true">{{ __('main.modal_setting_widget') }}</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="block-settings">
                                            <h3 class="mt-0">{{ __('main.modal_setting_block') }}</h3>
                                            @if ($idBlock)
                                                @include('templates.sites.pagebuilder.editboxes.boxes.'.$type, array(
                                                    'section' => 'form',
                                                    'block' => $block,
                                                    'optionValue' => $block['option_value']
                                                ))
                                        </div>
                                        <div class="tab-pane" id="widget-settings">
                                            <h3 class="mt-0">{{ __('main.modal_setting_widget') }}</h3>
                                            @include('templates.sites.pagebuilder.editboxes.widgets.'.$block['option_value']['widget'], array(
                                                'section' => 'form',
                                                'block' => $block,
                                                'optionValue' => $block['option_value']
                                            ))
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        @endif
                        <div class="clearfix"></div>
                    </div>
                    <button class="btn btn-primary">{{ __('main.save') }}</button>

                </form>
                </p>
            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                {{--<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Закрыть</button>--}}
                <button type="button" class="btn btn-primary button-close" data-dismiss="modal">{{ __('main.close') }}</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>