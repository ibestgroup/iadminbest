@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    <style>
        .{{$item['name']}} > img {
            margin: 0 auto;
            display: block;
        }
    </style>
    <div class="box-body {{$item['name']}}">
        @php
            $marks = \App\Models\MarksInside::where('status', 2)->get();
        @endphp

        @foreach($marks as $item)
            <div class="col-lg-{{(isset($optionValue['width_tarif']))?$optionValue['width_tarif']:'6'}} mt-10">
                <a href="javascript:activate({{$item->id}});" class="btn btn-activate-tarif"
                   style="">{{$item->name}}</a>
            </div>
        @endforeach

    </div>
@endif
