@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    @php
        $paymentSystems = \App\Models\PaymentSystems::where(function ($q) use ($helper) {
            $q->where('currency', $helper->siteCurrency());
        })->where('status', 1);

        if (isset($optionValue['payment_system']) and is_array($optionValue['payment_system'])) {
            $paymentSystems = $paymentSystems->whereIn('id', $optionValue['payment_system']);
        }
        $paymentSystems = $paymentSystems->get();
    @endphp
    @if ($helper->siteCurrency() < 4)
    <div class="box-body box-profile {{$item['name']}}">
        <form action="{{route('balance')}}" role="form" method="get">
            @csrf
            {{--@if ($helper->siteCurrency() == 2 or $helper->siteCurrency() == 3)--}}
            @if (\App\Models\PaymentSystems::where('currency', $helper->siteCurrency())->count() > 1)
                <div class="form-group" style="">
                    {{--<label for="system">{{ __('main.payment_system') }}</label>--}}
                    <select name="system" class="form-control select2" id="system">

                        @foreach($paymentSystems as $system)
                            <option value="{{$system->code}}" data-param='{{$system->toJson()}}')>
                                {{$system->name}}
                            </option>
                        @endforeach
                    </select>
                </div>
            @else
                <input type="hidden" name="system" value="1">
            @endif

            <div class="form-group">
                <p class="label-sum">{{(isset($optionValue['label_sum']))?$optionValue['label_sum']:'Введите сумму'}}</p>
                <input type="number" step='0.1' class="form-control" id="amount" placeholder="{{(isset($optionValue['label_sum']))?$optionValue['label_sum']:'Введите сумму'}}" name="amount">
                <input type="hidden" value="RUB" name="valval">
            </div>
            <button type="submit" class="btn btn-primary">{{(isset($optionValue['link']['text']))?$optionValue['link']['text']:'Пополнить'}}</button>
        </form>
    </div>
    @else
        @php
            $address = \App\Models\PaymentsCrypto::where('id_user', Auth::id())->where('status', 0)->first()->address;
        @endphp
        <div class="box-body box-profile {{$item['name']}}">
            <div class="alert alert-warning" role="alert">
                {!! __('main.crypto_payment') !!}
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">{!! __('main.crypto_address') !!}</h3>
                </div>
                <div class="panel-body">
                    <strong>
                            {{$address}}
                    </strong>
                </div>
            </div>
        </div>
    @endif


@endif
