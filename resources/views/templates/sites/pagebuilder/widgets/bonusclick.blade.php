@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    <div class="box-body box-profile {{$item['name']}}">
        {{--<form action="{{route('balance')}}" role="form" method="get">--}}
            {{--@csrf--}}
            {{--<button type="submit" class="btn btn-primary">{{(isset($optionValue['link']['text']))?$optionValue['link']['text']:'Пополнить'}}</button>--}}
        {{--</form>--}}
        <div class="box-body">
            <div class="form-group">
                <p class="text-bonus">{{(isset($optionValue['text_area']))?$optionValue['text_area']:''}}</p>
            </div>
            <form action="{{route('getBonus')}}" role="form" method="post" class="js-getBonus">
                @csrf
                <button type="submit" class="btn btn-primary">{{(isset($optionValue['link']['text']))?$optionValue['link']['text']:'Получить'}}</button>
            </form>

        </div>
    </div>
@endif
