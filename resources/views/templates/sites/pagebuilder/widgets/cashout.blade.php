@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    @php

        $paymentSystems = \App\Models\CashoutSystemCore::where(function ($q) use ($helper) {
            $q->where('currency', $helper->siteCurrency())->orWhere('currency', 0);
        });

        if (isset($optionValue['payment_system']) and is_array($optionValue['payment_system'])) {
            $paymentSystems = $paymentSystems->whereIn('id', $optionValue['payment_system']);
        }
        $paymentSystems = $paymentSystems->get();
        $siteId = \App\Libraries\Helpers::getSiteId();
        $userId = Auth::id();
        $teleg = \App\Models\TelegramCore::where('site_id', $siteId)->where('user_id', $userId)->where('status', 1);
        $telegCount = $teleg->count();
        $telegInfo = $teleg->first();
    @endphp
        @if (1)
            <form role="form" action="{{route("cashout")}}" class="js-cashOut">
                @csrf
                <div class="box-body {{$item['name']}}">
                    <div class="form-group" style="">
                        <label for="system">
                            {{ __('main.payment_system') }}
                            <i class="fas fa-question-circle" data-toggle="modal" data-target="#system-fee" style="cursor: pointer;"></i>
                        </label>
                        <select name="system" class="form-control select2" id="system">

                            @foreach($paymentSystems as $system)
                                <option value="{{$system->code}}" data-param='{{$system->toJson()}}')>
                                    {{$system->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="cashOut-sum">{{(isset($optionValue['lable_sum']))?$optionValue['lable_sum']:__('main.enter_amount')}}</label>
                        <input type="number" name="sum" {!! \App\Libraries\Helpers::stepForInput() !!} class="form-control" id="cashOut-sum" placeholder="{{ __('main.amount') }}">
                    </div>
                    <div class="form-group">
                        <label for="address">{{ __('main.wallet_for_withdraw') }}</label>
                        <input type="text" name="address" class="form-control" id="address" placeholder="{{ __('main.wallet') }}">
                    </div>
                    {!! \App\Libraries\Helpers::iAuth('form') !!}

                    {{--@if (\App\Libraries\Helpers::iAuth())--}}
                    {{--@if (0)--}}
                        {{--<div style="width: 100%; background: white; display: none;" class="iauth-protected">--}}
                            {{--<h3 class="ta-center"><span class="iauth-color">iAuth</span> защита</h3>--}}
                            {{--<div class="p-10">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label for="address">{{ __('main.wallet_for_withdraw') }}</label>--}}
                                    {{--<div class="iauth-btn iauth-get-code mt-10">--}}
                                        {{--<span class="iauth-get-code">ПОЛУЧИТЬ КОД</span>--}}
                                    {{--</div>--}}
                                    {{--<input type="hidden" name="iauth_code" class="" placeholder="Введите код">--}}
                                    {{--<div id="pinlogin-cashout" class="iauth-pinlogin"></div>--}}
                                    {{--<div class="btn btn-primary iauth-get-code" style="margin-top: 10px;">Получить код</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    {{--@else--}}
                        {{--<div class="js-telegram-confirm" style="">--}}
                            {{--<div class="telegram-confirm-message iauth-protected" style="min-height: 60px; background: #fff; display: none; text-align: center;">--}}
                                {{--<h3 class="ta-center">--}}
                                    {{--<span class="iauth-color">iAuth</span> защита--}}
                                    {{--<i class="fa fa-times teleg-close" style="float: right; cursor: pointer;"></i>--}}
                                {{--</h3>--}}
                                {{--<h4>Обезопасьте свои средства, подключите Telegram защиту</h4>--}}
                                {{--<div class="form-group" style="background: transparent">--}}
                                    {{--<input type="text" class="form-control" name="telegram" placeholder="Введите телеграм">--}}
                                {{--</div>--}}
                                {{--<div class="telegram-for-content" style="border-bottom: 1px solid #efefef; padding: 2px;">--}}

                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--@endif--}}
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">{{(isset($optionValue['link']['text']))?$optionValue['link']['text']:'Вывести'}}</button>
                </div>
            </form>
        @else

            <form role="form" action="{{route("cashout")}}" class="js-cashOut">
                @csrf
                <div class="box-body {{$item['name']}}">
                    <div class="form-group" style="">
                        <label for="system">{{ __('main.payment_system') }}</label>
                        <select name="system" class="form-control select2" id="system">

                            @foreach($paymentSystems as $system)
                                <option value="{{$system->code}}" data-param='{{$system->toJson()}}')>
                                    {{$system->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="cashOut-sum">{{(isset($optionValue['lable_sum']))?$optionValue['lable_sum']:__('main.enter_amount')}}</label>
                        <input type="number" name="sum" class="form-control" id="cashOut-sum" placeholder="{{ __('main.amount') }}">
                    </div>
                    <div class="form-group">
                        <label for="address">{{ __('main.wallet_for_withdraw') }}</label>
                        <input type="text" name="address" class="form-control" id="address" placeholder="{{ __('main.wallet') }}">
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">{{(isset($optionValue['link']['text']))?$optionValue['link']['text']:'Вывести'}}</button>
                </div>
            </form>

        @endif
@endif