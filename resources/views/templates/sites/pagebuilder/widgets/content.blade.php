@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    <div class="box-body">

        {{--@php--}}
            {{--$arrayValues = $helper->valueArray();--}}

            {{--foreach ($arrayValues as $val) {--}}
                {{--$result = $helper->convertValue($val);--}}

                {{--$optionValue['content'] = str_replace('{'.$val.'}', $result, $optionValue['content']);--}}

            {{--}--}}
        {{--@endphp--}}

        {!! $optionValue['content']??'' !!}

    </div>
@endif
