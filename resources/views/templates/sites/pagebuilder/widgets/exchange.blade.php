@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')


    <div class="box-body box-profile {{$item['name']}}">
        <form action="{{route('cashoutForBuy')}}" role="form" method="get" class="js-store">
            @csrf
            <div class="form-group">
                <p class="label-sum">{{(isset($optionValue['label_sum']))?$optionValue['label_sum']:'Введите сумму'}}</p>
                <input type="number" class="form-control" id="amount" placeholder="Введите сумму" name="amount">
            </div>
            <button type="submit" class="btn btn-primary">{{(isset($optionValue['link']['text']))?$optionValue['link']['text']:'Обменять'}}</button>
        </form>
    </div>
@endif
