@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    <div class="refresh">
        @php
            $items = \App\Models\Faq::all();
        @endphp

        <div class="box-body">
            <div class="box-group">
                @foreach ($items as $item)
                <div class="panel box box-primary">
                    <div class="box-header">
                        <h4 class="box-title">
                            <a data-toggle="collapse" data-parent="#accordion" href=".ask-{{$item['id']}}" aria-expanded="false" class="collapsed">
                                {{$item['ask']}}
                            </a>
                        </h4>
                    </div>
                    <div id="ask-{{$item['id']}}" class="panel-collapse collapse ask-{{$item['id']}}" aria-expanded="false" style="height: 0px;">
                        <div class="box-body">
                            {{$item['answer']}}
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
