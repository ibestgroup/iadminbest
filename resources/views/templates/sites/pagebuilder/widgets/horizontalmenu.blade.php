@inject('helper', "\App\Libraries\Helpers")

@php
    $items = $optionValue['links'];
@endphp

<div class="navbar-custom-menu">
    <ul class="nav navbar-nav all-slides-top in">
        @foreach($items as $item)
        <li class="ui-sortable-handle"><a href="{{$item['link']}}"><i class="{{ $item['icon'] }}"></i> {!! $item['text'] !!}</a></li>
        @endforeach
    </ul>
</div>