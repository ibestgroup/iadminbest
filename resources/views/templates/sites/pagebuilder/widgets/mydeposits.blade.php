@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')

    @php

        $birdsCount = \App\Models\BirdsOrder::where('user_id', Auth::id())->where('status', '<', 2)
            ->whereHas('bird.typesOrder', function ($q) {
                $q->where('status', 1);
            })->count();

        if (!empty($_GET['type'])) {
            $birdsCount = \App\Models\BirdsOrder::where('user_id', Auth::id())->where('status', '<', 2)
                ->whereHas('bird', function ($q) {
                    $q->where('type', $_GET['type']);
                })->whereHas('bird.typesOrder', function ($q) {
                    $q->where('status', 1);
                })->count();
        }

        $take = $optionValue['last_deposits'] ?? 5;
        $firstPage = $optionValue['first_page'] ?? 0;

        if ($birdsCount < $take) {
            $take = $birdsCount;
        }

        $birds = \App\Models\BirdsOrder::where('user_id', Auth::id())->where('status', '<', 2)
            ->whereHas('bird.typesOrder', function ($q) {
                $q->where('status', 1);
            });

        if (!empty($_GET['type'])) {
            $birds = \App\Models\BirdsOrder::where('user_id', Auth::id())->where('status', '<', 2)
                ->whereHas('bird', function ($q) {
                    $q->where('type', $_GET['type']);
                })->whereHas('bird.typesOrder', function ($q) {
                    $q->where('status', 1);
                });
        }

        if ($firstPage) {
            $birds = $birds->take($take)->get();
        } else {
            $birds = $birds->paginate($take);
        }

        $typesOrder = \App\Models\TypesOrder::where('status', 1)->get();

        foreach ($birds as $key => $bird) {

            $resultEnd = $bird['created_at'] + $bird->bird->second;
            $resultTime = time();
            $status = 0;
            if ($resultEnd <= $resultTime and $bird->bird->end == 1) {
                $resultTime = $resultEnd;
                $status = 1;
            }


             if ($bird->bird->range == 1) {
                $birds[$key]['egg'] =
                    ($resultTime - $bird['updated_at']) *
                    ($bird['number']/100) * ($bird->bird->profit/$bird->bird->second) +
                    $bird->balance;

            } else {

                $birds[$key]['egg'] =
                    ($resultTime - $bird['updated_at']) *
                    ($bird['number'] * $bird->bird->profit/$bird->bird->second) +
                    $bird->balance;

            }


            $birds[$key]['remain_time'] = $resultEnd - $resultTime;
            $birds[$key]['egg_round'] = round($birds[$key]['egg'], 5);

            \App\Models\BirdsOrder::where('id', $bird['id'])->update([
                'balance' => $birds[$key]['egg']
            ]);
            if ($birds[$key]['egg'] != $bird['balance']) {
                if ($bird->bird->end == 1) {
                    if ($bird['updated_at'] < $resultEnd) {
                        \App\Models\BirdsOrder::where('id', $bird['id'])->update([
                            'updated_at' => $resultTime,
                            'status' => $status,
                        ]);
                    }
                } else {
                    \App\Models\BirdsOrder::where('id', $bird['id'])->update([
                        'updated_at' => $resultTime,
                        'status' => $status,
                    ]);
                }
            }
        }
        $url = \Illuminate\Support\Facades\Request::url();
    @endphp


<div class="col-lg-12 p-0 referal-refresh js-myDeposits" data-link="{{$url}}" data-id="{{$box['id']}}"z>

    <div class="top-btn p-10">
        @if (isset($optionValue['top_btn']))
            @if ($optionValue['top_btn'])
                <a href="{{$url}}" class="btn btn-primary">Все</a>
                @foreach($typesOrder as $order)
                    <a href="{{$url}}?type={{$order['id']}}" class="btn btn-primary">{{$order['bird']}}</a>
                @endforeach
            @endif
        @endif
    </div>

    <div class="box-body">
        {{--<div class="progress-deposit" style="height: 10px; background: #aaa; width: 0px;"></div>--}}
        <table class="table table-responsive js-collect-refresh">
            <tbody>
            <tr>
                @if (isset($optionValue['list']))
                    @foreach($optionValue['list'] as $item)
                        <th>{{(isset($item['name']))?$item['name']:''}} <i class="{{$item['icon']}}"></i></th>
                    @endforeach
                @endif
            </tr>


            @foreach($birds as $bird)
                <tr class="" id="refresh-box-{{$box['id']}}">
                    @if (isset($optionValue['list']))
                        @foreach($optionValue['list'] as $item)
                            <td>
                                @if ($item['value'] == 'deposit_balance')
                                    <span class="egg-number" id="egg-number-{{$bird->id}}">{{$bird->egg_round}}</span><img src="/iadminbest/storage/app/{{$bird->bird->typesOrder->image}}" width="20px" alt="">
                                @elseif ($item['value'] == 'deposit_seconds')
                                    @if ($bird->bird->end)
                                        <span class="timer-remind" id="timer-remind-{{$bird->id}}">{{$bird->remain_time}}</span> сек
                                    @else
                                        <i class="fas fa-infinity"></i>
                                    @endif
                                @elseif ($item['value'] == 'deposit_btn')
                                    <form action="{{route('collectEgg')}}" method="post" class="js-collectEgg">
                                        @csrf
                                        <input type="hidden" name="bird_id" value="{{$bird->id}}">
                                        <button type="submit" class="btn btn-primary pick-up">{{($item['name'])?$item['name']:'Забрать'}}</button>
                                    </form>
                                @else
                                    {!! \App\Libraries\Helpers::convertValue($item['value'], '', ['deposit_id' => $bird->id]) !!}
                                @endif
                            </td>
                        @endforeach
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (!$firstPage)
            <div class="box-footer p-0 clearfix">
                {{$birds->links()}}
            </div>
        @endif






        {{--@forelse($birds as $bird)--}}

            {{--<div class="col-md-6 col-lg-4 col-xs-12 col-sm-12 ta-center birds-list mb-10">--}}
                {{--<div class="col-md-12 col-lg-12 p-0 for-equal-height">--}}
                    {{--<h2>{{$bird->bird->name}}</h2>--}}
                    {{--<div class="birds-list-body">--}}
                        {{--<img src="/iadminbest/storage/app/{{$bird->bird->image}}" class="image-item ta-center mb-20">--}}
                        {{--<table class="table table-bordered">--}}
                            {{--<tbody>--}}
                            {{--<tr>--}}
                                {{--<td style="width: 40%">Тип</td>--}}
                                {{--<td>{{$bird->bird->typesOrder->bird}}</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td style="width: 40%">Ресурс</td>--}}
                                {{--<td>{{$bird->bird->typesOrder->egg}}</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td>Название</td>--}}
                                {{--<td>{{$bird->bird->name}}</td>--}}
                            {{--</tr>--}}
{{--                            @if ($bird->bird->end)--}}
                                {{--<tr>--}}
                                    {{--<td>Осталось</td>--}}
                                    {{--<td><span class="timer-remind">{{$bird->remain_time}}</span> сек</td>--}}
                                {{--</tr>--}}
                            {{--@else--}}
                                {{--<tr>--}}
                                    {{--<td>Количество</td>--}}
                                    {{--<td>{{$bird->number}}</td>--}}
                                {{--</tr>--}}
                            {{--@endif--}}
                            {{--<tr>--}}
                                {{--<td>{{$bird->bird->typesOrder->egg}}</td>--}}
                                {{--<td><span class="egg-number" id="egg-number-{{$bird->id}}">{{$bird->egg_round}}</span><img src="/iadminbest/storage/app/{{$bird->bird->typesOrder->image}}" width="20px" alt=""></td>--}}
                            {{--</tr>--}}
                            {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<form action="{{route('collectEgg')}}" method="post" class="js-collectEgg">--}}
                            {{--@csrf--}}
                            {{--<input type="hidden" name="bird_id" value="{{$bird->id}}">--}}
                            {{--<button type="submit" class="btn btn-primary">{{($bird->bird->buy_btn)?$bird->bird->buy_btn:'Сделать вклад'}}</button>--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--@empty--}}

            {{--Вы еще ничего не приобретали из этой категории <a href="{{route('buyBirds')}}?type={{app('request')->input('type')}}" class="btn btn-primary">В магазин</a>--}}

        {{--@endforelse--}}

    </div>
</div>
@endif
