@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    <div class="box-body box-profile {{$item['name']}}"><ul class="timeline" >

        @php
            $take = (isset($optionValue['last_news']))?$optionValue['last_news']:5;
            $news = \App\Models\News::orderBy('date', 'desc')->take($take)->get();
        @endphp
        @foreach($news as $item)
            <!-- timeline time label -->
                <li class="time-label">
                <span>
                    {{date('Y-d-m', strtotime($item['date']))}}
                </span>
                </li>
                <!-- /.timeline-label -->

                <!-- timeline item -->
                <li data-id="{{$item['id']}}">
                    <!-- timeline icon -->
                    <i class="{{($item['icon'] != '')?$item['icon']:'fa fa-envelope'}}" style="background: {{($item['color'] != '')?$item['color']:'#d2d6de'}}; color: {{($item['color_icon'] != '')?$item['color_icon']:'#666'}};"></i>
                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> {{date('H-i-s', strtotime($item['date']))}}</span>

                        <h3 class="timeline-header">{{$item['prev']}}</h3>

                        <div class="timeline-body">
                            {{$item['text']}}
                        </div>

                        <div class="timeline-footer">
                            {{--<a class="btn btn-primary btn-xs" href="{{route('goToNews', ['id' => $item['id']])}}">Подробнее</a>--}}
                            @if (Auth::id() == 1)
                                <a class="btn btn-danger btn-xs js-delete-news" data-action="{{route('deleteNews')}}">Удалить</a>
                            @endif
                        </div>
                    </div>
                </li>
                <!-- END timeline item -->
            @endforeach
        </ul>
    </div>
@endif
