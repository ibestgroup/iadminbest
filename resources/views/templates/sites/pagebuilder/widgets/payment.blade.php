@if ($section == 'head')
    {{$item['name']}}
@elseif ($section == 'body')
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
            <tbody>
            <tr>
                <th>ID</th>
                <th>Пользователь</th>
                <th>Сумма</th>
                <th>Статус</th>
                <th>Дата</th>
            </tr>

            @foreach ($payments as $payment)
                <tr>
                    <td>{{ $payment['id'] }}</td>
                    <td>{{ $payment['payment_user'] }}</td>
                    <td>{{ $payment['amount'] }}</td>
                    <td><span class="label label-success">Подтверждена</span></td>
                    <td>{{ $payment['date'] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endif