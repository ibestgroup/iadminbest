@inject('helper', "\App\Libraries\Helpers")
@php
    if (!isset($section)) {
        $section = '';
    }
@endphp
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    <style>
        #box-{{$item['id_in_system']}} img {
            margin: 0 auto;
            display: block;
        }
    </style>
    <div class="box-body box-profile profilebox-{{$item['name']}}">
        @if (isset($optionValue['image']))
            {!! $helper->convertValue($optionValue['image']) !!}
        @else
            <img class="profile-user-img img-responsive img-circle" src="{{$helper->getAvatar(Auth::id(), 'link')}}" alt="{{Auth::user()->login}}">

        @endif

        <h3 class="profile-username text-center">
            @if (isset($optionValue['text']))
                {{$helper->convertValue($optionValue['text'])}}
            @else
                {{Auth::user()->login}}
            @endif
            @if (isset($optionValue['icon']))
                <i class="{{$optionValue['icon']}}"></i>
            @endif
        </h3>

        <ul class="list-group list-group-unbordered">
            @if (isset($optionValue['list']))
                @foreach($optionValue['list'] as $item)
                    @if (isset($item['name']))
                        <li class="list-group-item">
                            <b>{{$item['name']}}</b> <a class="pull-right">{!! \App\Libraries\Helpers::convertValue($item['value']) !!} <i class="{{$item['icon']}}"></i></a>
                        </li>
                    @endif
                @endforeach
            @endif
        </ul>

        <a href="{{(isset($optionValue['link']['link']))?$optionValue['link']['link']:'/logout'}}" class="btn btn-primary btn-block"><b>{{(isset($optionValue['link']['text']))?$optionValue['link']['text']:'Выйти'}}</b></a>
    </div>
@endif
