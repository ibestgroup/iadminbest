@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    <style>
        .{{$item['name']}} > img {
            margin: 0 auto;
            display: block;
        }
    </style>
    <div class="box-body box-profile {{$item['name']}}">

            <form autocomplete="off" role="form" action="{{route('profile.edit')}}" class="js-store" enctype="multipart/form-data">
                @csrf
                @if (isset($optionValue['input']))
                    @foreach($optionValue['input'] as $item)
                        {!! $helper->getInput($item) !!}
                    @endforeach
                @endif
                {{--<select class="form-group" name="confirm_cashout">--}}
                    {{--<option value="0">Без подтверждения</option>--}}
                    {{--<option value="1">Подтверждение по почте</option>--}}
                {{--</select>--}}
                {!! \App\Libraries\Helpers::iAuth('form') !!}
                <button class="btn btn-primary btn-block">{{__('main.save')}}</button>
            </form>

    </div>
@endif
