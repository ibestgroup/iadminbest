@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    <div class="refresh">
        @if (Auth::id() == 1)
            <a class="btn btn-primary" href="{{route('addPromo')}}">{{ __('main.add_promo') }}</a>
        @endif
        <h3 class="box-title reflink">{{ __('main.ref_link') }}: {{(isset($_SERVER['HTTPS']))?'https':'http'}}://{{$_SERVER['HTTP_HOST']}}/?ref={{Auth::user()->login}}</h3>

        @php
            $items = \App\Models\Promo::all();
        @endphp

        @foreach ($items as $item)
            <div class="box box-default element-promo" data-id="{{$item['id']}}">
                <div class="box-header promo-head">
                    <h3 class="box-title">{{$item['name']}}</h3>

                    <div class="pull-right">

                        @if (Auth::id() == 1)
                            <a class="btn btn-danger btn-xs js-delete-promo" data-action="{{route('deletePromo')}}">{{ __('main.delete') }}</a>
                            <a class="btn btn-danger btn-xs" href="{{route('editViewPromo', ['id' => $item['id']])}}">{{ __('main.edit') }}</a>
                        @endif
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <img src="{{$item['image_link']}}" alt="" width="{{$item['width']}}px" height="{{$item['height']}}px">
                    <br>
                    <br>
                    <textarea style="width: 100%;" rows="3"><a href="{{(isset($_SERVER['HTTPS']))?'https':'http'}}://{{$_SERVER['HTTP_HOST']}}/?ref={{Auth::user()->login}}"><img src="{{$item['image_link']}}" alt="" width="{{$item['width']}}" height="{{$item['height']}}"></a></textarea>
                </div>
                <!-- /.box-body -->
            </div>
        @endforeach
    </div>
@endif
