@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    <div class="box-body table-responsive">
        <table class="table table-hover">
            <tbody>
            @php
                $take = (isset($optionValue['last_referrals']))?$optionValue['last_referrals']:5;
                $firstPage = (isset($optionValue['first_page'])?($optionValue['first_page'])?1:0:0);

                $randomReferrals = (isset($optionValue['random_referrals']))?$optionValue['random_referrals']:0;
                $types[0] = (isset($optionValue['active']))?($optionValue['active'])?1:99999:99999;
                $types[1] = (isset($optionValue['not_active']))?($optionValue['not_active'])?0:99999:99999;

                $refs = \App\Models\Users::where('refer', Auth::id())->where('id', '!=', 1)->whereIn('type_follow', $types);

                if ($refs->count() < $take) {
                    $take = $refs->count();
                }

                if ($randomReferrals) {
                    $referrals = \App\Models\Users::where('refer', Auth::id())->where('id', '!=', 1)->whereIn('type_follow', $types)->get()->random($take);
                } else {
                    if ($firstPage) {
                        $referrals = \App\Models\Users::where('refer', Auth::id())->where('id', '!=', 1)->whereIn('type_follow', $types)->orderBy('regdate', 'desc')->take($take)->get();
                    } else {
                        $referrals = \App\Models\Users::where('refer', Auth::id())->where('id', '!=', 1)->whereIn('type_follow', $types)->orderBy('regdate', 'desc')->paginate($take);
                    }
                }
            @endphp

            <tr>
                @if (isset($optionValue['list']))
                    @foreach($optionValue['list'] as $item)
                        <th>{{(isset($item['name']))?$item['name']:''}} <i class="{{$item['icon']}}"></i></th>
                    @endforeach
                @endif
            </tr>
            @foreach ($referrals as $referral)


                <tr class="{{($referral['type_follow'] == 0)?'not-activate-user':'activate-user'}}">
                    @if (isset($optionValue['list']))
                        @foreach($optionValue['list'] as $item)
                            <td>
                                @php
                                    $currencyIcon = \App\Libraries\Helpers::siteCurrency('icon');
                                @endphp
                                {!! \App\Libraries\Helpers::convertValue($item['value'], $referral['id']) !!}{!! ($item['value'] == 'balance' or $item['value'] == 'profit')?$currencyIcon:'' !!}
                            </td>
                        @endforeach
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>

        @if (!$randomReferrals and !$firstPage)
            <div class="box-footer p-0 clearfix">
                {{$referrals->links()}}
            </div>
        @endif
    </div>
@endif