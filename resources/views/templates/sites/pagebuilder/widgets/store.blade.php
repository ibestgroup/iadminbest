@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')

    @php

        $birds = \App\Models\Birds::all();
        $typesOrder = \App\Models\TypesOrder::where('status', 1)->get();

        if (!empty($_GET['type'])) {
            $birds = \App\Models\Birds::where('type', $_GET['type'])->get();
        }


        $url = \Illuminate\Support\Facades\Request::url();
    @endphp


<div class="col-lg-12 p-0 referal-refresh js-myStore" data-link="{{$url}}">

    <div class="top-btn p-10">
        @if (isset($optionValue['top_btn']))
            @if ($optionValue['top_btn'])
                <a href="{{$url}}" class="btn btn-primary">Все</a>
                @foreach($typesOrder as $order)
                    <a href="{{$url}}?type={{$order['id']}}" class="btn btn-primary">{{$order['bird']}}</a>
                @endforeach
            @endif
        @endif
    </div>

    <div class="box-body">
        <table class="table table-responsive">
            <tbody>
            <tr>
                @if (isset($optionValue['list']))
                    @foreach($optionValue['list'] as $item)
                        <th>{{(isset($item['name']))?$item['name']:''}} <i class="{{$item['icon']}}"></i></th>
                    @endforeach
                @endif
            </tr>


        @foreach($birds as $bird)


            <tr>
                @if (isset($optionValue['list']))
                    @foreach($optionValue['list'] as $item)
                        <td>
                            @if ($item['value'] == 'price')

                                @if ($bird['range'])
                                    от {{$bird['price']}}<img src="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['image_current']}}" alt="" width="20px">
                                    до {{$bird['max_price']}}<img src="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['image_current']}}" alt="" width="20px">
                                @else
                                    {{$bird['price']}}<img src="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['image_current']}}" alt="" width="20px">
                                @endif

                            @elseif ($item['value'] == 'name')
                                {{$bird['name']}}
                            @elseif ($item['value'] == 'second_to_day')
                                {{round($bird['second']/86400, 3)}} д.
                            @elseif ($item['value'] == 'second_to_hour')
                                {{round($bird['second']/3600, 3)}} ч.
                            @elseif ($item['value'] == 'second_to_minute')
                                {{round($bird['second']/60, 3)}} мин.
                            @elseif ($item['value'] == 'second')
                                {{$bird['second']}} сек.
                            @elseif ($item['value'] == 'store_profit_all')
                                @if ($bird->end)
                                    @if($bird->range)
                                        {{$bird['profit']}}%
                                    @else
                                        {{round($bird['profit'], 3)}}<img src="/iadminbest/storage/app/{{$bird->typesOrder->image}}" width="20px" alt="">
                                    @endif
                                @else
                                    <i class="fas fa-infinity"></i>
                                @endif
                            @elseif ($item['value'] == 'store_profit_second')

                                {{round($bird['profit']/$bird['second'], 3)}}<img src="/iadminbest/storage/app/{{$bird->typesOrder->image}}" width="20px" alt="">/сек.
                            @elseif ($item['value'] == 'store_profit_minute')

                                {{round($bird['profit']/$bird['second']*60, 3)}}<img src="/iadminbest/storage/app/{{$bird->typesOrder->image}}" width="20px" alt="">/мин.
                            @elseif ($item['value'] == 'store_profit_hour')
                                {{round($bird['profit']/$bird['second']*3600, 3)}}
                                @if ($bird->range == 1)
                                    %
                                @else
                                    <img src="/iadminbest/storage/app/{{$bird->typesOrder->image}}" width="20px" alt="">
                                @endif
                                /ч.

                            @elseif ($item['value'] == 'store_profit_day')
                                {{round($bird['profit']/$bird['second']*86400, 3)}}<img src="/iadminbest/storage/app/{{$bird->typesOrder->image}}" width="20px" alt="">/д.
                            @elseif ($item['value'] == 'number')
                                {{\App\Models\BirdsOrder::where('user_id', Auth::id())->where('bird_id', $bird['id'])->sum('number')}}
                            @elseif ($item['value'] == 'store_btn')
                                <form action="{{route('buyBirds')}}" role="form" method="post" class="js-buyBirds">
                                    @csrf
                                    <input type="hidden" name="bird_id" value="{{$bird['id']}}">
                                    @if ($bird['range'] == 1)
                                        <input type="number" name="sum" placeholder="Сумма" value="{{(int)$bird['max_price']}}">
                                    @endif
                                    <input type="submit" class="btn btn-primary pick-up" value="{{($bird['buy_btn'])?$bird['buy_btn']:'Сделать вклад'}}">
                                </form>
                            @else
                                {{--{!! \App\Libraries\Helpers::convertValue($item['value'], '', ['deposit_id' => $bird->id]) !!}--}}
                                ываыавваыв
                            @endif
                        </td>
                    @endforeach
                @endif
            </tr>


            {{--<div class="col-md-6 col-lg-4 col-xs-12 col-sm-12 ta-center birds-list mb-10">--}}
                {{--<div class="col-md-12 col-lg-12 p-0 for-equal-height">--}}
                    {{--<h2>{{$bird['name']}}</h2>--}}
                    {{--<div class="birds-list-body">--}}
                        {{--<img src="/iadminbest/storage/app/{{$bird['image']}}" class="image-item ta-center mb-20">--}}
                        {{--<table class="table table-bordered">--}}
                            {{--<tbody>--}}
                            {{--<tr>--}}
                                {{--<td style="width: 40%">Цена</td>--}}
                                {{--<td>--}}
                                    {{--@if ($bird['range'])--}}
                                        {{--от {{$bird['price']}}<img src="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['image_current']}}" alt="" width="20px">--}}
                                        {{--до {{$bird['max_price']}}<img src="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['image_current']}}" alt="" width="20px">--}}
                                    {{--@else--}}
                                        {{--{{$bird['price']}}<img src="/iadminbest/storage/app/{{\App\Models\SiteInfo::find(1)['image_current']}}" alt="" width="20px">--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                            {{--<tr>--}}
                                {{--<td>У вас</td>--}}
                                {{--<td>{{\App\Models\BirdsOrder::where('user_id', Auth::id())->where('bird_id', $bird['id'])->sum('number')}}</td>--}}
                            {{--</tr>--}}
                            {{--@if ($bird->end)--}}
                                {{--<tr>--}}
                                    {{--<td>Срок жизни</td>--}}
                                    {{--<td style="line-height: 9px;">{{round($bird['second']/3600, 3)}} час(а/ов)<br>или {{$bird['second']}} секунд(ы)</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>Доход</td>--}}
                                    {{--<td style="line-height: 9px;">{{$bird['profit']}}<img src="/iadminbest/storage/app/{{$bird->typesOrder->image}}" width="20px" alt="">--}}
                                        {{--@if($bird->only_end)--}}
                                            {{--<br>по истечению срока--}}
                                        {{--@endif--}}
                                    {{--</td>--}}
                                {{--</tr>--}}
                            {{--@else--}}
                                {{--<tr>--}}
                                    {{--<td>Доход/час</td>--}}
                                    {{--<td>{{round($bird['profit']/$bird['second']*3600, 3)}}<img src="/iadminbest/storage/app/{{$bird->typesOrder->image}}" width="20px" alt=""> в час</td>--}}
                                {{--</tr>--}}
                                {{--<tr>--}}
                                    {{--<td>Доход/сек</td>--}}
                                    {{--<td>{{round($bird['profit']/$bird['second'], 4)}}<img src="/iadminbest/storage/app/{{$bird->typesOrder->image}}" width="20px" alt=""> в секунду</td>--}}
                                {{--</tr>--}}
                            {{--@endif--}}
                            {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<form action="{{route('buyBirds')}}" role="form" method="post" class="js-buyBirds">--}}
                            {{--@csrf--}}
                            {{--<input type="hidden" name="bird_id" value="{{$bird['id']}}">--}}
                            {{--@if ($bird['range'] == 1)--}}
                                {{--<input type="number" name="sum" placeholder="Сумма" value="{{(int)$bird['max_price']}}">--}}
                            {{--@endif--}}
                            {{--<input type="submit" class="btn btn-primary" value="{{($bird['buy_btn'])?$bird['buy_btn']:'Сделать вклад'}}">--}}
                        {{--</form>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        @endforeach
            </tbody>
        </table>

    </div>



    {{--<div class="box-body">--}}
        {{--<div class="progress-deposit" style="height: 10px; background: #aaa; width: 0px;"></div>--}}
        {{--<table class="table table-responsive">--}}
            {{--<tbody>--}}
            {{--<tr>--}}
                {{--@if (isset($optionValue['list']))--}}
                    {{--@foreach($optionValue['list'] as $item)--}}
                        {{--<th>{{(isset($item['name']))?$item['name']:''}} <i class="{{$item['icon']}}"></i></th>--}}
                    {{--@endforeach--}}
                {{--@endif--}}
            {{--</tr>--}}


            {{--@foreach($birds as $bird)--}}
                {{--<tr class="js-collect-refresh">--}}
                    {{--@if (isset($optionValue['list']))--}}
                        {{--@foreach($optionValue['list'] as $item)--}}
                            {{--<td>--}}
                                {{--@if ($item['value'] == 'deposit_balance')--}}
                                    {{--<span class="egg-number" id="egg-number-{{$bird->id}}">{{$bird->egg_round}}</span><img src="/iadminbest/storage/app/{{$bird->bird->typesOrder->image}}" width="20px" alt="">--}}
                                {{--@elseif ($item['value'] == 'deposit_seconds')--}}
                                    {{--@if ($bird->bird->end)--}}
                                        {{--<span class="timer-remind" id="timer-remind-{{$bird->id}}">{{$bird->remain_time}}</span> сек--}}
                                    {{--@else--}}
                                        {{--<i class="fas fa-infinity"></i>--}}
                                    {{--@endif--}}
                                {{--@elseif ($item['value'] == 'deposit_btn')--}}
                                    {{--<form action="{{route('collectEgg')}}" method="post" class="js-collectEgg">--}}
                                        {{--@csrf--}}
                                        {{--<input type="hidden" name="bird_id" value="{{$bird->id}}">--}}
                                        {{--<button type="submit" class="btn btn-primary pick-up">{{($item['name'])?$item['name']:'Забрать'}}</button>--}}
                                    {{--</form>--}}
                                {{--@else--}}
                                    {{--{!! \App\Libraries\Helpers::convertValue($item['value'], '', ['deposit_id' => $bird->id]) !!}--}}
                                {{--@endif--}}
                            {{--</td>--}}
                        {{--@endforeach--}}
                    {{--@endif--}}
                {{--</tr>--}}
            {{--@endforeach--}}
            {{--</tbody>--}}
        {{--</table>--}}
        {{----}}
        {{----}}
        {{----}}
        {{--@if (!$firstPage)--}}
            {{--<div class="box-footer p-0 clearfix">--}}
                {{--{{$birds->links()}}--}}
            {{--</div>--}}
        {{--@endif--}}

    {{--</div>--}}
</div>
@endif
