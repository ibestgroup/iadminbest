@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')

    @php
        $marks = \App\Models\MarksInside::where('status', 2)->get();
        $markGet = (isset($_GET['mark']))?$_GET['mark']:0;

        $firstPage = (isset($optionValue['first_page']))?$optionValue['first_page']:0;
        $orders = \Illuminate\Support\Facades\DB::table('marks_inside')
            ->select(['marks_inside.*', 'marks_inside.level as m_level', 'orders_mark_inside.*'])
            ->leftJoin('orders_mark_inside', 'marks_inside.id', '=', 'orders_mark_inside.mark')
            ->where('id_user','=', Auth::id())
            ->where('marks_inside.status','>','0')
            ->where(function ($q) use ($optionValue) {
                if (isset($optionValue['mark_view']) and $optionValue['mark_view'] != 0) {
                    $q->where('marks_inside.id', $optionValue['mark_view']);
                }
            });


        $take = (isset($optionValue['last_tarifs']))?$optionValue['last_tarifs']:5;
        $markView = $optionValue['mark_view'] ?? 0;
        $markIdView = $optionValue['mark_id_view'] ?? 0;

        if ($markGet) {
            $ordersCount = $orders->where('mark', $markGet)->count();
            $orders = $orders->where('mark', $markGet)->paginate($take);
        } else {
            $ordersCount = $orders->count();
            $orders = $orders->paginate($take);
            $markGet = 0;
        }

        $url = \Illuminate\Support\Facades\Request::url();

        $marks = \App\Models\MarksInside::where('status', '>', 0)->get();
    @endphp

    <script src="/iadminbest/public/js/svgArrow.js"></script>
    @if (!isset($_GET['tarif_id']))
        <div class="col-lg-12 tarif-list referal-refresh">
            <div class="top-btn">
                @if (isset($optionValue['top_btn']))
                    @if ($optionValue['top_btn'] and !$markView)
                        <a href="{{$url}}" class="btn btn-primary mt-5">Все</a>
                        @foreach($marks as $mark)
                            <a href="{{$url}}?mark={{$mark->id}}" class="btn btn-primary mt-5">{{$mark->name}}</a>
                        @endforeach
                    @endif
                @endif
            </div>
            @if ($markGet == 0)
                <h3>Всего тарифов - {{$ordersCount}}</h3>
            @else
                <h3>Клонов в {{\App\Helpers\MarksHelper::markName($markGet)}} - {{$ordersCount}} клон(ов)</h3>
            @endif
            @if (!$firstPage)
                {{$orders->appends(request()->input())->links()}}
            @endif
            <div>
                <table class="table table-responsive">
                    <tbody>
                    <tr>
                        @if (isset($optionValue['list']))
                            @foreach($optionValue['list'] as $item)
                                <th>{{(isset($item['name']))?$item['name']:''}} <i class="{{$item['icon']}}"></i></th>
                            @endforeach
                        @endif
                    </tr>


                    @foreach($orders as $order)
                        <tr>
                            @if (isset($optionValue['list']))
                                @foreach($optionValue['list'] as $item)
                                    <td>
                                        @if ($item['value'] == 'mark_more')
                                            <a href="{{\Illuminate\Support\Facades\Request::url()}}?tarif_id={{$order->id}}" class="btn btn-primary more">{{$item['name']}} <i class="{{$item['icon']}}"></i></a>
                                        @else
                                            {!! \App\Libraries\Helpers::convertValue($item['value'], '', ['mark_id' => $order->id]) !!}
                                        @endif
                                    </td>
                                @endforeach
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @if (!$firstPage)
                {{$orders->appends(request()->input())->links()}}
            @endif
        </div>
    @else

        <div class="col-lg-12 referal-refresh">
        @php
            if (isset($optionValue['next_overflow']) && $optionValue['next_overflow']) {
                $mark = \App\Models\OrdersMarkInside::where('id', $_GET['tarif_id'])->first()->mark;
                $overflowObj = new \App\Libraries\Multimark(1, $mark, true);
                $overflowRes = $overflowObj->overflowMulti($_GET['tarif_id']);
                $nextOrder = \App\Models\OrdersMarkInside::where('id', $overflowRes)->first();
                $nextUser = \App\Models\Users::where('id', $nextOrder->id_user)->first();
            }
            $order = \App\Models\OrdersMarkInside::where('id', $_GET['tarif_id'])->first();
            $paymentsOrder = \App\Models\PaymentsOrder::where('from_wallet', $_GET['tarif_id'])->orWhere('wallet', $_GET['tarif_id'])->get();
        @endphp
            <style>
                .box .overlay, .overlay-wrapper .overlay {
                    background: rgba(255, 255, 255, 1);
                }
            </style>
            <div class="js-tree-tarif tarif-tree" data-path="{{route("treeDetail", ['id' => $order->id])}}" id="sbox-my-tarifs">
                    <div class="top-btn">
                        <a href="{{$url}}" class="btn btn-primary mt-5">Назад к тарифам</a>
                    </div>
                    <h3>{{\App\Helpers\MarksHelper::markName($order->mark)}}
                        @if ($markIdView)
                            (#{{$order->id}})
                        @endif
                    </h3>
                    <h3>
                        @if (isset($optionValue['next_overflow']) && $optionValue['next_overflow'])
                            Следующий на очереди в этой структуре {{ $nextUser->login }}
                        @endif
                    </h3>

                    @php
                        $domain = new \App\Libraries\Domain();
                        $subdom = $domain->getDB();
                        $sitesForTree = ['jaguar_iadminbest', 'saswe_iadminbest'];
                    @endphp

                    @if ($order->marks->width > 8 or $order->marks->level > 6 or in_array($subdom, $sitesForTree) or $order->marks->overflow == 0)
                        <div class="col-xs-12">
                            <button type="button" class="btn btn-primary btn-flat js-tree-show">Реферальное дерево</button>
                        </div>
                        <div class="col-xs-12 margin js-tree-container" data-path="{{route("treeDetail", ['id' => $order->id])}}">
                        </div>
                    @else

                        <div style='text-align:center; min-height: 30px; width: 100%; float: left;'>
                            <div style='padding: 10px 2px; font-size: 19px;'>
                                @php
                                    $parentOrder = \App\Models\OrdersMarkInside::where('id', $order->par_or)->first();
                                    $parent = \App\Models\Users::where('id', $parentOrder->id_user)->first();
                                    $avatar = "/iadminbest/storage/app/".$parent->avatar;

                                    if ($parent->avatar == 'no_image') {
                                        $avatar = "/iadminbest/storage/app/".\App\Models\SiteInfo::find(1)->first()['no_image'];
                                    } else {
                                        $avatar = "/iadminbest/storage/app/".$parent->avatar;
                                    }
                                @endphp
                                @if (isset($optionValue['level_up_view']) and $optionValue['level_up_view'])
                                <div class='img-tree'>
                                    <a href='?tarif_id={{$parentOrder->id}}'><img src="{{$avatar}}" style='width: 100px; height: 100px; max-width: 100%;' class="img-circle-my" alt="{{$parent->login}}"></a>
                                    <br>{{$parent->login}}<br>{{$parentOrder->level}} уровень
                                </div>
                                @endif
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        @php
                            $tree = new \App\Libraries\Multimark(Auth::id(), $order->mark, true);
                            $result = $tree->createTree($order->id);
                            $result = [0 => [
                                'id' => $order->id,
                                'user' => $order->id_user,
                                'name' => \App\Libraries\Helpers::convertUs('login', $order->id_user),
                                'children' => $result
                            ]];
                            \App\Http\Controllers\ReferalTree::referralPayList($result, $order->mark, $order->id, 0, $url, $optionValue['free_place_naming'] ?? 'свободное место');
                        @endphp

                    @endif

                    <div class="clearfix"></div>
                    <div class="col-lg-12 col-md-12">


                        <table class="table table-hover">
                            <tbody>
                            <tr>
                                @if (isset($optionValue['rows']))
                                    @foreach($optionValue['rows'] as $item)
                                        <th>{{$item['head']}}</th>
                                    @endforeach
                                @endif
                            </tr>
                            @php
                                $payments = \App\Models\PaymentsOrder::where('from_wallet', $_GET['tarif_id'])->orWhere('wallet', $_GET['tarif_id'])->orderBy('date', 'desc')->paginate(10);
                            @endphp
                            @foreach ($payments as $payment)

                                <tr class="{{($payment['to_user'] == Auth::id())?'to-transaction':'from-transaction'}}">

                                    @if (isset($optionValue['rows']))
                                        @foreach($optionValue['rows'] as $item)
                                            <td>
                                                @if ($item['value'] == 'amount')
                                                    {{$payment->amount}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}
                                                @elseif ($item['value'] == 'to_user' or $item['value'] == 'from_user')
                                                    {{$helper->convertUs('login', $payment[$item['value']])}}
                                                @elseif ($item['value'] == 'id_transaction')
                                                    {{$payment->id}}
                                                @else
                                                    {{$payment[$item['value']]}}
                                                @endif
                                            </td>
                                        @endforeach
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="custom-paginate clearfix" data-refresh="#box-my-tarifs">
                            {{$payments->appends(request()->input())->links()}}
                        </div>
                </div>
            </div>
        </div>
    @endif
@endif
