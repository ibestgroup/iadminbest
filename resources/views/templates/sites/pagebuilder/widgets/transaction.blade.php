@inject('helper', "\App\Libraries\Helpers")
@if ($section == 'head')
    {{$optionValue['head']}}
@elseif ($section == 'body')
    <div class="box-body table-responsive">
        <table class="table table-hover">
            <tbody>
            <tr>
                @if (isset($optionValue['rows']))
                    @foreach($optionValue['rows'] as $item)
                        <th>{{$item['head']}}</th>
                    @endforeach
                @endif
            </tr>
            @php

                $types[0] = ($optionValue['payment'])?'payment':'';
                $types[1] = ($optionValue['payments_order'])?'payments_order':'';
                $types[2] = ($optionValue['cashout'])?'cashout':'';
                if (\App\Libraries\Helpers::siteCurrency('type') == 'crypto' and $types[0] != '') {
                    $types[0] = 'payment_crypto';
                }

                $take = (isset($optionValue['last_transaction']))?$optionValue['last_transaction']:5;
                $firstPage = (isset($optionValue['first_page'])?($optionValue['first_page'])?1:0:0);

                $transaction = \App\Models\Transaction::whereIn('type', $types)->count();

                //Check transaction status
                $cashouts = \App\Models\Cashout::where('payment_system', 955)->where('status', 0)->get();
                foreach ($cashouts as $cashout) {
                    $payId = (string)$cashout->id_payment;
                    $params = ['id' => $payId];
                    $sign = md5(implode('', $params) . '89ec8d252bc9a43ec1acb477ea740c05');

                    $transaction = json_decode(Curl::to('https://merchant.betatransfer.io/api/info?token=bcbbaeade259b067d9631b0ed714d703')
                        ->withData([
                            'id' => $payId,
                            'sign' => $sign,
                        ])
                        ->post());
                    if (isset($transaction->status)) {
                        if ($transaction->status == 'success') {
                            \App\Models\Cashout::where('id', $cashout->id)->update(['status' => 2]);
                        } elseif ($transaction->status == 'cancel') {
                            $backToBalance = $transaction->amount + $transaction->commission;
                            \App\Models\Users::where('id', $cashout->id_user)->increment('balance', $backToBalance);
                            \App\Models\Cashout::where('id', $cashout->id)->update(['status' => -1]);
                        }
                    }

                }

                if ($transaction < $take) {
                    $take = $transaction;
                }

                if ($firstPage) {
                    $payments = \App\Models\Transaction::whereIn('type', $types)->where(function ($q) {
                        $q->where('from_user', Auth::id())->orWhere('to_user', Auth::id());
                    })->orderBy('date', 'desc')->take($take)->get();
                } else {
                    $payments = \App\Models\Transaction::whereIn('type', $types)->where(function ($q) {
                        $q->where('from_user', Auth::id())->orWhere('to_user', Auth::id());
                    })->orderBy('date', 'desc')->paginate($take);
                }
            @endphp
            @foreach ($payments as $payment)

                <tr class="{{($payment['to_user'] == Auth::id())?'to-transaction':'from-transaction'}}">

                    @if (isset($optionValue['rows']))
                        @foreach($optionValue['rows'] as $item)
                            <td>
                                @if ($item['value'] == 'amount')
                                    {{$payment->info($payment->type)->amount}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</i>
                                @elseif ($item['value'] == 'to_user' or $item['value'] == 'from_user')
                                    {{$helper->convertUs('login', $payment[$item['value']])}}
{{--                                    {{($payment[$item['value']] == Auth::id())?'':''}}--}}
                                @elseif ($item['value'] == 'id_transaction')
                                    {{$payment->id}}
                                @else
                                    {{$payment[$item['value']]}}
                                @endif
                            </td>
                        @endforeach
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (!$firstPage)
        <div class="box-footer p-0 clearfix">
            {{$payments->links()}}
        </div>
        @endif
    </div>
@endif