@inject('helper', "\App\Libraries\Helpers")

@php
    $items = $optionValue['links'];
@endphp
    <div class="clearfix"></div>
    <ul class="sidebar-menu" data-widget="tree">
        @foreach($items as $item)
            <li>
                <a href="{{$item['link']}}"><i class="{{ $item['icon'] }}"></i> {!! $item['text'] !!}</a>
            </li>
        @endforeach
    </ul>
