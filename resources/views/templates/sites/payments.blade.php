@extends("layout.app")

@section('title', 'История операций')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">История операций</h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody>
                    <tr>
                        <th>ID</th>
                        <th>Пользователь</th>
                        <th>Сумма</th>
                        <th>Статус</th>
                        <th>Дата</th>
                    </tr>

                    @foreach ($payments as $payment)
                        <tr>
                            <td>{{ $payment['id'] }}</td>
                            <td>{{ $payment['payment_user'] }}</td>
                            <td>{{ $payment['amount'] }}</td>
                            <td><span class="label label-success">Подтверждена</span></td>
                            <td>{{ $payment['date'] }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            {{$payments->links()}}
        </div>
    </div>
@endsection
