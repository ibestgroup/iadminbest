@extends("layout.app")

@section('content')

    @php
        //$hasher = new \App\Libraries\MD5Hasher();
        //echo $hasher->make('123123',['MPHO93']);
        //$ola = 0.25;
        //$ole = ($ola)?$ola:0;
        //echo $ole;
    @endphp
    {{--<form method='post' action='https://www.nixmoney.com/merchant.jsp'>--}}
        {{--<input type='hidden' name='PAYEE_ACCOUNT' value='U73277330987934'>--}}
        {{--<input type='number' name='PAYMENT_AMOUNT' value='100.00'>--}}
        {{--<input type='text' name='PAYEE_NAME' value='iAdmin.best'>--}}
        {{--<input type='text' name='PAYMENT_URL' value='https://icash.best'>--}}
        {{--<input type='text' name='NOPAYMENT_URL' value='https://icash.best'>--}}
        {{--<input type='hidden' name='BAGGAGE_FIELDS' value='NAME'>--}}
        {{--<input type="submit">--}}
    {{--</form>--}}

    @php
        if ($demo) {
            $classTemplate = \App\Models\CoreTemplates::query();
            $classSiteElements = \App\Models\CoreSiteElements::query();
            $template = $demo;
        } else {
            $classTemplate = \App\Models\Templates::query();
            $classSiteElements = \App\Models\SiteElements::query();
            $template = \App\Libraries\Helpers::siteTemplate();
        }
        $mainStyle = unserialize($classSiteElements->where('name', 'main-style')->where('template', $template)->first()['option_value']);
    @endphp
    @if (Auth::id() == 1 and $demo == 0)
        @include('templates.sites.pagebuilder.editPanel', array('page' => $page))
    @endif
    @if ($countPage)
    <div class="row main-main">

        <div class="profile-page page-content">
            <div class="col-lg-12 p-0 page-refresh for-grid" data-id="content">
                @if (\App\Libraries\Helpers::access($page))
                    {{\App\Http\Controllers\ProfileController::pageConstruct($page, 'content', $demo)}}
                @else
                    <div class="col-lg-12">
                        <p style="font-size: 20px; background: #eee; border: 3px solid #ddd; border-radius: 15px; padding: 20px;">
                            {{ __('main.no_access') }} <br>
                            {{\App\Libraries\Helpers::access($page, 'message')}}
                        </p>
                    </div>
                @endif
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>

        {{--<form action="https://perfectmoney.is/api/step1.asp" method="POST">--}}
            {{--<p>--}}
                {{--<input type="hidden" name="PAYEE_ACCOUNT" value="U22457714">--}}
                {{--<input type="hidden" name="PAYEE_NAME" value="iAdmin.best">--}}
                {{--<input type="number" step="0.01" name="PAYMENT_AMOUNT" value="" placeholder="Сумма">--}}
                {{--<input type="hidden" name="PAYMENT_UNITS" value="USD">--}}
                {{--<input type="hidden" name="STATUS_URL"--}}
                       {{--value="http://dohodmlm.iadminlocal.best/checkpaypm">--}}
                {{--<input type="hidden" name="PAYMENT_URL"--}}
                       {{--value="http://dohodmlm.iadminlocal.best/successpaypm">--}}
                {{--<input type="hidden" name="NOPAYMENT_URL"--}}
                       {{--value="http://dohodmlm.iadminlocal.best/failspaypm">--}}
                {{--<input type="hidden" name="BAGGAGE_FIELDS"--}}
                       {{--value="ORDER_NUM CUST_NUM">--}}
                {{--<input type="hidden" name="ORDER_NUM" value="1">--}}
                {{--<input type="submit" name="PAYMENT_METHOD" value="PerfectMoney account">--}}
            {{--</p>--}}
        {{--</form>--}}

    </div>
    @else
        Такой страницы не существует
    @endif
    <div>

        {!! \App\Libraries\Helpers::adsense('content') !!}
    {{--@if (\App\Libraries\Helpers::adsense())--}}
        {{--<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>--}}
        {{--<!-- footer-profile -->--}}
        {{--<ins class="adsbygoogle"--}}
             {{--style="display:block"--}}
             {{--data-ad-client="ca-pub-7640044855853499"--}}
             {{--data-ad-slot="6263478999"--}}
             {{--data-ad-format="auto"--}}
             {{--data-full-width-responsive="true"></ins>--}}
        {{--<script>--}}
            {{--(adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--</script>--}}

            {{--<a href="https://prtglp.ru/affiliate/11073561">--}}
                {{--<img src="https://glopart.ru/uploads/images/96628/77ebccde423a4d58ab2428c4d0241777.jpg" width="100%" alt="">--}}
            {{--</a>--}}
    {{--@endif--}}
    </div>
    <div class="clearfix"></div>
    <div class="modal fade" id="system-fee">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" style="text-align: center; color: #333;">Комиссии</h4>
                </div>
                <div class="modal-body">
                    @php
                        $cashoutSystem = \App\Models\CashoutSystemCore::where('currency', \App\Libraries\Helpers::siteCurrency())->orWhere('currency', 0)->get();
                    @endphp

                    @foreach($cashoutSystem as $item)
                        {{--<h4></h4>--}}
                        <p>
                            <b>{{$item->name}}</b>
                            <br>
                            Комиссия: <b>{{$item->fee}}% {!! ($item->custom_fee != 0)?'+ '.$item->custom_fee.\App\Libraries\Helpers::siteCurrency('icon'):'' !!}</b>
                            <br>
                            Сумма для вывода:
                            минимальная <b>{{$item->min}}{!! \App\Libraries\Helpers::siteCurrency('icon')!!}</b>
                            максимальная <b>{{$item->max}}{!! \App\Libraries\Helpers::siteCurrency('icon')!!}</b>
                        </p>
                    @endforeach
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
