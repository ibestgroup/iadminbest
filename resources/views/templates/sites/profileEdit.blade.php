@extends("layout.core.app")

@section('title', 'Редактирование профиля')

@section("content")
    @php
        $user = Auth::user()->users_data
    @endphp
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">Редактирование персональных данных</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{route('profile.edit')}}" class="js-profile-edit" enctype="multipart/form-data">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="kv-avatar text-center">
                            <div class="file-loading">
                                <input id="avatar" name="avatar" type="file" data-default="/moneytree/storage/app/{{Auth::user()->avatar}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="last-name">Фамилия</label>
                            <input type="text" name="last_name" class="form-control" value="{{$user->lastname}}" id="last-name" placeholder="Фамилия">
                        </div>
                        <div class="form-group">
                            <label for="name">Имя</label>
                            <input type="text" name="name" value="{{$user->name}}" class="form-control" id="name" placeholder="Имя">
                        </div>
                        <div class="form-group">
                            <label for="second-name">Отчество</label>
                            <input type="text" name="second_name" class="form-control" value="{{$user->fathername}}" id="second-name" placeholder="Отчество">
                        </div>
                        <label for="second-name">Отчество</label>
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fab fa-skype"></i></span>
                            <input type="text" name="skype" class="form-control" value="{{$user->skype}}" id="skype" placeholder="Логин skype">
                        </div>
                        <div class="form-group">
                            <label>Пол</label>
                            <div class="radio">
                                <input type="radio" name="sex" id="male" value="0" checked="">
                                <label for="male">
                                    Мужской
                                </label>
                            </div>
                            <div class="radio">
                                <input type="radio" name="sex" id="female" value="1">
                                <label for="female">
                                    Женский
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Сохранить изменения</button>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Изменение пароля</h3>
                </div>
                <form class="js-profile-edit" action="{{route('profile.edit')}}">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="password">Новый пароль</label>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Пароль">
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation">Повторите пароль</label>
                            <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Повторите пароль">
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header width-border">
                    <h3 class="box-title">Социальные сети</h3>
                </div>
                <form role="form" action="{{route('profile.edit')}}" class="js-profile-edit">
                    @csrf
                    <div class="box-body">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fab fa-skype"></i></span>
                            <input type="text" name="skype" class="form-control" value="{{$user->skype}}" id="skype" placeholder="Логин skype">
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fab fa-vk"></i></span>
                            <input type="text" name="vk" class="form-control" value="{{$user->vk}}" id="vk" placeholder="Ссылка на профиль в вконтакте">
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fab fa-facebook"></i></span>
                            <input type="text" name="facebook" class="form-control" value="{{$user->facebook}}" id="facebook" placeholder="Ссылка на профиль в facebook">
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fab fa-twitter"></i></span>
                            <input type="text" name="twitter" class="form-control" id="twitter" value="{{$user->twitter}}" placeholder="Ссылка на профиль в twitter">
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fab fa-instagram"></i></span>
                            <input type="text" name="instagram" class="form-control" id="instagram" value="{{$user->instagram}}" placeholder="Ссылка на профиль в instagramm">
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection