@extends("layout.core.app")

@section('title', 'Промоматериалы')

@section('content')

    @php
        $template = \App\Models\Templates::where('status', 1)->first()['id'];
        $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);

    @endphp
    @include('templates.sites.pagebuilder.editPanel', array('page' => 'promo'))
    <div class="row main-main">


        <div class="profile-page page-content">
            <div class="col-lg-12 p-0 page-refresh for-grid" data-id="content">
                {{\App\Http\Controllers\ProfileController::pageConstruct('promo')}}
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        @include('templates.sites.pagebuilder.modals', array('page' => 'promo'))
    </div>


    {{--@if (Auth::id() == 1)--}}
        {{--<a class="btn btn-primary" href="{{route('addPromo')}}">Добавить промоматериал</a>--}}
    {{--@endif--}}
    {{--<h3 class="box-title">Ваша реферальная ссылка: {{(isset($_SERVER['HTTPS']))?'https':'http'}}://{{$_SERVER['HTTP_HOST']}}/?ref={{Auth::user()->login}}</h3>--}}
    {{--@foreach ($items as $item)--}}
        {{--<div class="box box-default" data-id="{{$item['id']}}">--}}
            {{--<div class="box-header with-border">--}}
                {{--<h3 class="box-title">{{$item['name']}}</h3>--}}

                {{--<div class="pull-right">--}}

                    {{--@if (Auth::id() == 1)--}}
                        {{--<a class="btn btn-danger btn-xs js-delete-promo" data-action="{{route('deletePromo')}}">Удалить</a>--}}
                        {{--<a class="btn btn-danger btn-xs" href="{{route('editViewPromo', ['id' => $item['id']])}}">Редактировать</a>--}}
                    {{--@endif--}}
                {{--</div>--}}
                {{--<!-- /.box-tools -->--}}
            {{--</div>--}}
            {{--<!-- /.box-header -->--}}
            {{--<div class="box-body">--}}
                {{--<img src="{{$item['image_link']}}" alt="" width="{{$item['width']}}" height="{{$item['height']}}">--}}
                {{--<br>--}}
                {{--<br>--}}
                {{--<textarea cols="30" rows="10">--}}
                    {{--<a href="{{(isset($_SERVER['HTTPS']))?'https':'http'}}://{{$_SERVER['HTTP_HOST']}}/?id={{Auth::id()}}"><img src="{{$item['image_link']}}" alt="" width="{{$item['width']}}" height="{{$item['height']}}"></a>--}}
                {{--</textarea>--}}
            {{--</div>--}}
            {{--<!-- /.box-body -->--}}
        {{--</div>--}}
    {{--@endforeach--}}
@endsection
