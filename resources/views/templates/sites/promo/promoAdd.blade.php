@extends("layout.admin.app")

@section('title', __('main.admin_create_promo'))

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <div class="box-header">
                        <h3 class="box-title">{{ __('main.admin_create_promo') }}</h3>
                    </div>

                    <div class="box-body">
                        <form action="{{route('addPromo')}}" role="form" method="post" class="js-promo">
                        {{ csrf_field() }}
                        <!-- text input -->
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>{{ __('main.admin_name') }}</label>
                                    <input type="text" class="form-control" placeholder="{{ __('main.admin_name') }}" name="name">
                                </div>

                                <!-- textarea -->
                                <div class="form-group">
                                    <label>{{ __('main.link_image') }}</label>
                                    <input class="form-control" placeholder="{{ __('main.link_image') }}" name="image">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>{{ __('main.width_banner') }}</label>
                                    <input class="form-control" placeholder="{{ __('main.width_banner') }}" type="number" name="width">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>{{ __('main.height_banner') }}</label>
                                    <input class="form-control" placeholder="{{ __('main.height_banner') }}" type="number" name="height">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">{{ __('main.add') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            @foreach ($items as $item)
                <div class="box box-default" data-id="{{$item['id']}}">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{$item['name']}}</h3>

                        <div class="pull-right">

                            @if (Auth::id() == 1)
                                <a class="btn btn-danger btn-xs js-delete-promo" data-action="{{route('deletePromo')}}">Удалить</a>
                                <a class="btn btn-danger btn-xs" href="{{route('editViewPromo', ['id' => $item['id']])}}">Редактировать</a>
                            @endif
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <img src="{{$item['image_link']}}" alt="" width="{{$item['width']}}px" height="{{$item['height']}}px">
                        <br>
                        <br>
                        <textarea cols="30" rows="10">
                    <a href="{{(isset($_SERVER['HTTPS']))?'https':'http'}}://{{$_SERVER['HTTP_HOST']}}/?id={{Auth::id()}}"><img src="{{$item['image_link']}}" alt="" width="{{$item['width']}}px" height="{{$item['height']}}px"></a>
                </textarea>
                    </div>
                    <!-- /.box-body -->
                </div>
            @endforeach
        </div>
    </div>
@endsection
