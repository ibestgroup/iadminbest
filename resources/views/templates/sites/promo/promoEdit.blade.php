@extends("layout.admin.app")

@section('title', __('main.admin_edit_promo'))

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <div class="box-header">
                        <h3 class="box-title">{{ __('main.admin_edit_promo') }}</h3>
                    </div>

                    <div class="box-body">
                        <form action="{{route('editPromo')}}" role="form" method="post" class="js-promo">
                        {{ csrf_field() }}
                            <input type="hidden" class="form-control" value="{{$item['id']}}" name="id">
                        <!-- text input -->
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>{{ __('main.admin_name') }}</label>
                                    <input type="text" class="form-control" value="{{$item['name']}}" name="name">
                                </div>

                                <!-- textarea -->
                                <div class="form-group">
                                    <label>{{ __('main.link_image') }}</label>
                                    <input class="form-control" placeholder="Ссылка" value="{{$item['image_link']}}" name="image">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>{{ __('main.width_banner') }}</label>
                                    <input class="form-control" placeholder="{{ __('main.width_banner') }}" value="{{$item['width']}}" type="number" name="width">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>{{ __('main.height_banner') }}</label>
                                    <input class="form-control" placeholder="{{ __('main.height_banner') }}" value="{{$item['height']}}" type="number" name="height">
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">{{ __('main.save') }}</button>
                            <a class="btn btn-primary" href="{{route('addPromo')}}">{{ __('main.list_promo') }}</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
