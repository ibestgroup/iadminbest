@php
    use Endroid\QrCode\ErrorCorrectionLevel;
    use Endroid\QrCode\LabelAlignment;
    use Endroid\QrCode\QrCode;
    use Endroid\QrCode\Response\QrCodeResponse;
    use Illuminate\Support\Facades\Auth;
    // Create a basic QR code

    $qrCode = new QrCode('https://'.$_SERVER['HTTP_HOST'].'/?ref='.Auth::user()->login);
    $qrCode->setSize(300);


    if ($image == 'avatar') {
        $path = 'storage/app';
        $pathImg = Auth::user()->avatar;
    } else {
        $path = 'public/img';
        $pathImg = $image;
    }


    // Set advanced options
    $qrCode->setWriterByName('png');
    $qrCode->setMargin(10);
    $qrCode->setEncoding('UTF-8');
    $qrCode->setErrorCorrectionLevel(new ErrorCorrectionLevel(ErrorCorrectionLevel::HIGH));
    $qrCode->setForegroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
    $qrCode->setBackgroundColor(['r' => 89, 'g' => 53, 'b' => 222, 'a' => 1]);
    $qrCode->setLogoPath(__DIR__.'/../../../'.$path.'/'.$pathImg);
    $qrCode->setLogoSize(100, 100);
    $qrCode->setRoundBlockSize(true);
    $qrCode->setValidateResult(false);
    $qrCode->setWriterOptions(['exclude_xml_declaration' => true]);

    // Directly output the QR code
    header('Content-Type: '.$qrCode->getContentType());
    echo $qrCode->writeString();
@endphp