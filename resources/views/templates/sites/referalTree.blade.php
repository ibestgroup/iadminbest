@extends("layout.app")

@section('title', 'Тарифы и реферальное дерево')

@section('content')

    @php
        $template = \App\Models\Templates::where('status', 1)->first()['id'];
        $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);

    @endphp

        @include('templates.sites.pagebuilder.editPanel', array('page' => 'tarifs'))
    @if (!isset($_GET['tarif_id']))
    <div class="row main-main">
        {{--Рекламное объявление--}}


        <div class="profile-page page-content">
            <div class="col-lg-12 p-0 page-refresh for-grid" data-id="content">
                {{\App\Http\Controllers\ProfileController::pageConstruct('tarifs')}}
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        @include('templates.sites.pagebuilder.modals', array('page' => 'tarifs'))
    </div>


    {{--@if (!isset($_GET['tarif_id']))--}}
    {{--<div class="referal-refresh">--}}
        {{--<script src="/iadminbest/public/js/svgArrow.js"></script>--}}
        {{--<div>--}}
            {{--<a href="/profile/referalTree" class="btn btn-primary mt-5">Все</a>--}}
            {{--@foreach($marks as $mark)--}}
                {{--<a href="/profile/referalTree?mark={{$mark->id}}" class="btn btn-primary mt-5">{{$mark->name}}</a>--}}
            {{--@endforeach--}}
            {{--@if ($markGet == 0)--}}
                {{--<h3>Всего тарифов - {{$ordersCount}}</h3>--}}
            {{--@else--}}
                {{--<h3>Клонов в {{\App\Helpers\MarksHelper::markName($markGet)}} - {{$ordersCount}} клон(ов)</h3>--}}
            {{--@endif--}}
        {{--</div>--}}
        {{--{{$orders->appends(request()->input())->links()}}--}}
        {{--<div>--}}

            {{--@foreach($orders as $order)--}}
                {{--{{dd($order)}}--}}
                {{--<div class="col-md-4">--}}
                    {{--<div class="col-lg-4">--}}
                        {{--<a href="/profile/referalTree?tarif_id={{$order->id}}" class="btn btn-primary"--}}
                           {{--style="display: block; width: 350px; height: 350px; line-height: 350px; border-radius: 350px; margin: auto; font-size: 20px;">{{\App\Helpers\MarksHelper::markName($order->mark)}} (#{{$order->id}})</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--@endforeach--}}
            {{--<div class="clearfix"></div>--}}
        {{--</div>--}}
        {{--{{$orders->appends(request()->input())->links()}}--}}
    {{--</div>--}}


    {{--@else--}}
        {{--@php--}}
            {{--$order = \App\Models\OrdersMarkInside::where('id', $_GET['tarif_id'])->first();--}}
            {{--$paymentsOrder = \App\Models\PaymentsOrder::where('from_wallet', $_GET['tarif_id'])->orWhere('wallet', $_GET['tarif_id'])->get();--}}
        {{--@endphp--}}
    {{--<div class="referal-refresh">--}}
        {{--<div class="box box-default js-tree-tarif" data-path="{{route("treeDetail", ['id' => $order->id])}}">--}}
            {{--<div class="box-header">--}}
                {{--<div>--}}
                    {{--<a href="/profile/referalTree" class="btn btn-primary mt-5">Все</a>--}}
                    {{--<a href="/profile/referalTree" class="btn btn-primary mt-5">Назад к тарифам</a>--}}
                {{--</div>--}}
                {{--<h3>{{\App\Helpers\MarksHelper::markName($order->mark)}} (#{{$order->id}})</h3>--}}
            {{--</div>--}}
            {{--<div class="box-body">--}}
                {{--@php--}}
                    {{--$tree = new \App\Libraries\Multimark(Auth::id(), $order->mark, true);--}}
                    {{--$result = $tree->createTree($order->id);--}}
                    {{--$result = [0 => [--}}
                        {{--'id' => $order->id,--}}
                        {{--'user' => $order->id_user,--}}
                        {{--'name' => \App\Libraries\Helpers::convertUs('login', $order->id_user),--}}
                        {{--'children' => $result--}}
                    {{--]];--}}
                    {{--\App\Http\Controllers\ReferalTree::referralPayList($result, $order->mark, $order->id);--}}
                {{--@endphp--}}

                {{--<div class="clearfix"></div>--}}
                {{--<div class="col-lg-12 col-md-12">--}}


                    {{--<table class="table table-hover transactions">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                            {{--<th>#</th>--}}
                            {{--<th>Тип</th>--}}
                            {{--<th>От кого</th>--}}
                            {{--<th>Кому</th>--}}
                            {{--<th>Сумма</th>--}}
                            {{--<th>Дата</th>--}}
                            {{--<th>Тип</th>--}}
                            {{--<th>Куда</th>--}}
                            {{--<th>Уровень</th>--}}
                        {{--</tr>--}}

                        {{--@foreach ($paymentsOrder as $key => $item)--}}
                            {{--<tr class="{{($item->to_user == Auth::id())?'transaction-incoming':'transaction-outgoing'}}">--}}
                                {{--<td>{{ \App\Libraries\Helpers::idTransaction($item->id, $item->table) }}</td>--}}
                                {{--@if ($item->to_user == Auth::id())--}}
                                    {{--<td><span class="glyphicon glyphicon-import" aria-hidden="true"></span></td>--}}
                                {{--@else--}}
                                    {{--<td><span class="glyphicon glyphicon-export" aria-hidden="true"></span></td>--}}
                                {{--@endif--}}
                                {{--<td>--}}
                                    {{--@if ($item->wallet == 'to_balance')--}}
                                        {{--Вывод на баланс--}}
                                    {{--@else--}}
                                        {{--{{ \App\Libraries\Helpers::convertUs('login', $item->from_user) }}--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                {{--<td>{{ \App\Libraries\Helpers::convertUs('login', $item->to_user) }}</td>--}}
                                {{--<td>{{ $item->amount }} <i class="fa fa-rub"></i></td>--}}
                                {{--<td>{{ date('d-m-Y h:i:s', strtotime($item->date)) }}</td>--}}
                                {{--<td>{{$item->type}}</td>--}}
                                {{--<td>{{$item->wallet}}</td>--}}
                                {{--<td>{{$item->level}}</td>--}}
                            {{--</tr>--}}
                        {{--@endforeach--}}
                        {{--</tbody>--}}
                    {{--</table>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    @endif
@endsection