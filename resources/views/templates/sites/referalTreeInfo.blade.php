@extends("layout.app")

@section('title', 'Информация о тарифе')

@section('content')
    <script src="/newmlm/public/js/svgArrow.js"></script>

    <div class="row referal-refresh">
        <div class="col-md-12">
                {{--{{dd($order)}}--}}

                <div class="box box-default js-tree-tarif" data-path="{{route("treeDetail", ['id' => $order->id])}}">
                    <div class="box-header">
                        <div>
                            <a href="/profile/referalTree" class="btn btn-primary mt-5">Все</a>
                            <a href="/profile/referalTree" class="btn btn-primary mt-5">Назад к тарифам</a>
                        </div>
                        <h3>{{\App\Helpers\MarksHelper::markName($order->mark)}} (#{{$order->id}})</h3>
                    </div>
                    <div class="box-body">
                        @php
                            $tree = new \App\Libraries\Multimark(Auth::id(), $order->mark, true);
                            $result = $tree->createTree($order->id);
                            $result = [0 => [
                                'id' => $order->id,
                                'user' => $order->id_user,
                                'name' => \App\Libraries\Helpers::convertUs('login', $order->id_user),
                                'children' => $result
                            ]];
                            \App\Http\Controllers\ReferalTree::referralPayList($result, $order->mark, $order->id);
                        @endphp
                        {{--<div class="row">--}}
                            {{--<div class="col-md-6 col-sm-6 col-xs-12 col-lg-4">--}}
                                {{--<div class="info-box bg-aqua" style="background: #7e4de1!important;">--}}
                                    {{--<span class="info-box-icon">--}}
                                        {{--<i class="fa fa-coins"></i>--}}
                                    {{--</span>--}}
                                    {{--<div class="info-box-content">--}}
                                        {{--<span class="info-box-text">Доход тарифа</span>--}}
                                        {{--<span class="info-box-number">{{$order->balance}}</span>--}}
                                    {{--</div>--}}
                                    {{--<!-- /.info-box-content -->--}}
                                {{--</div>--}}
                                {{--<!-- /.info-box -->--}}
                            {{--</div>--}}
                            {{--<div class="col-md-6 col-sm-6 col-xs-12 col-lg-4">--}}
                                {{--<div class="info-box bg-aqua">--}}
                                    {{--<span class="info-box-icon">--}}
                                        {{--<i class="fa fa-user-circle"></i>--}}
                                    {{--</span>--}}
                                    {{--<div class="info-box-content">--}}
                                        {{--<span class="info-box-text">Кол-во рефералов</span>--}}
                                        {{--<span class="info-box-number">{{\App\Models\PaymentsOrder::where('wallet', $order->id)->count()}}/{{\App\Helpers\MarksHelper::markUser($order->m_level, $order->width)}}</span>--}}
                                    {{--</div>--}}
                                    {{--<!-- /.info-box-content -->--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="col-md-12 col-sm-6 col-xs-12 col-lg-4">--}}
                                {{--<div class="info-box bg-aqua">--}}
                                    {{--<span class="info-box-icon"><i class="fa fa-boxes"></i></span>--}}

                                    {{--<div class="info-box-content form-group">--}}
                                        {{--<form action="{{route('cashoutOrder')}}" method="post" class="js-cashoutOrder" data-id-order="{{$order->id}}" id="order-{{$order->id}}">--}}
                                            {{--{{csrf_field()}}--}}
                                            {{--<input type="hidden" name="id_order" value="{{$order->id}}">--}}
                                            {{--<div class="col-xs-6">--}}
                                                {{--<input type="number" name="amount" placeholder="Введите сумму" class="form-control">--}}
                                            {{--</div>--}}
                                            {{--<button type="submit" class="btn btn-primary btn-flat">Вывести</button>--}}
                                        {{--</form>--}}
                                        {{--<div class="col-lg-12 p-0" id="balance-{{$order->id}}">--}}
                                            {{--@if($order->status == 2)--}}
                                                {{--<b>Тариф закрыт</b>--}}
                                            {{--@else--}}
                                                {{--<b>Баланс тарифа: {{$order->balance - $order->cashout}}</b>--}}
                                                {{--<br>--}}
                                                {{--<b>Выведено: {{$order->cashout}}/{{\App\Helpers\MarksHelper::markProfit($order->mark)}}</b>--}}
                                            {{--@endif--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<!-- /.info-box-content -->--}}
                                {{--</div>--}}
                            {{--</div>--}}

                            {{--@for ($i = 1; $i <= $order->marks->level; $i++)--}}
                                {{--@php--}}
                                    {{--$people = \App\Models\PaymentsOrder::where('wallet', $order->id)->where('level', $i)->count();--}}
                                    {{--$peopleMax = \App\Helpers\MarksHelper::peopleLevel($order->mark, $i);--}}

                                    {{--if ($people >= $peopleMax) {--}}
                                        {{--$bg = 'green';--}}
                                    {{--} else {--}}
                                        {{--$bg = 'yellow';--}}
                                    {{--}--}}
                                {{--@endphp--}}
                                {{--<div class="col-md-12 col-sm-6 col-xs-12 col-lg-4">--}}
                                    {{--<div class="info-box bg-{{$bg}}">--}}
                                        {{--<span class="info-box-icon" style="font-size: 25px;">{{$i}} ур</span>--}}

                                        {{--<div class="info-box-content form-group">--}}
                                            {{--<h4>Людей {{\App\Models\PaymentsOrder::where('wallet', $order->id)->where('level', $i)->count()}}/{{\App\Helpers\MarksHelper::peopleLevel($order->mark, $i)}}</h4>--}}
                                            {{--<h4>Доход {{\App\Models\PaymentsOrder::where('wallet', $order->id)->where('level', $i)->sum('amount')}}/{{\App\Helpers\MarksHelper::profitLevel($order->mark, $i)}}</h4>--}}
                                        {{--</div>--}}
                                        {{--<!-- /.info-box-content -->--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endfor--}}

                            <div class="clearfix"></div>
                            <div class="col-lg-12 col-md-12">


                                <table class="table table-hover transactions">
                                    <tbody>
                                    <tr>
                                        {{--<th>#</th>--}}
                                        <th>Тип</th>
                                        <th>От кого</th>
                                        <th>Кому</th>
                                        <th>Сумма</th>
                                        <th>Дата</th>
                                        <th>Тип</th>
                                        <th>Куда</th>
                                        <th>Уровень</th>
                                    </tr>

                                    @foreach ($paymentsOrder as $key => $item)
                                        <tr class="{{($item->to_user == Auth::id())?'transaction-incoming':'transaction-outgoing'}}">
                                            {{--<td>{{ \App\Libraries\Helpers::idTransaction($item->id, $item->table) }}</td>--}}
                                            @if ($item->to_user == Auth::id())
                                                <td><span class="glyphicon glyphicon-import" aria-hidden="true"></span></td>
                                            @else
                                                <td><span class="glyphicon glyphicon-export" aria-hidden="true"></span></td>
                                            @endif
                                            <td>
                                                @if ($item->wallet == 'to_balance')
                                                    Вывод на баланс
                                                @else
                                                    {{ \App\Libraries\Helpers::convertUs('login', $item->from_user) }}
                                                @endif
                                            </td>
                                            <td>{{ \App\Libraries\Helpers::convertUs('login', $item->to_user) }}</td>
                                            <td>{{ $item->amount }} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</td>
                                            <td>{{ date('d-m-Y h:i:s', strtotime($item->date)) }}</td>
                                            <td>{{$item->type}}</td>
                                            <td>{{$item->wallet}}</td>
                                            <td>{{$item->level}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

    </div>
@endsection