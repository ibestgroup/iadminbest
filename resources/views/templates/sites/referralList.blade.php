@extends("layout.core.app")

@section('title', 'Список рефералов')

@section('content')

    @php
        $template = \App\Models\Templates::where('status', 1)->first()['id'];
        $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);

    @endphp
    @include('templates.sites.pagebuilder.editPanel', array('page' => 'referrals'))
    <div class="row main-main">
        {{--Рекламное объявление--}}
        @if ($ad['state'])
            <div class="col-lg-12">
                <div class="alert alert-{{$ad['type']}} alert-dismissible">
                    <h4><i class="icon fa fa-info"></i> {{$ad['head']}}</h4>
                    {{$ad['text']}}
                </div>
            </div>
            <div class="clearfix"></div>
        @endif


        <div class="profile-page page-content">
            <div class="col-lg-12 p-0 page-refresh for-grid" data-id="content">
                {{\App\Http\Controllers\ProfileController::pageConstruct('referrals')}}
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        @include('templates.sites.pagebuilder.modals', array('page' => 'referrals'))
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Фильтры</h3>
                </div>
                <div class="box-body">
                    <form action="{{route('lastday')}}">
                        <div class="form-group has-feedback">
                            <label for="lastdays">Показать рефералов за последние (?) дней</label>
                            <input type="number" class="form-control" id="lastdays" value="{{$days?$days:7}}" name="days">
                            <span class="glyphicon glyphicon-calendar form-control-feedback"></span>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Показать</button>
                        </div>
                    </form>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>

    {{--<div class="box">--}}
        {{--<div class="box-header">--}}
            {{--<h3 class="box-title">Список рефералов--}}
                {{--@if(isset($days))--}}
                    {{--<small>(за последние {{$days}} дней)</small>--}}
                {{--@endif--}}
            {{--</h3>--}}

            {{--<form role="form" action="{{route('lastday')}}" method="get">--}}
                {{--<div class="box-body">--}}

                {{--</div>--}}
                {{--<!-- /.box-body -->--}}
            {{--</form>--}}

            {{--<div class="box-tools">--}}
                {{--<div class="input-group input-group-sm" style="width: 150px;">--}}
                    {{--<input type="text" name="table_search" class="form-control pull-right" placeholder="Search">--}}

                    {{--<div class="input-group-btn">--}}
                        {{--<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- /.box-header -->--}}
        {{--<div class="box-body table-responsive no-padding">--}}
            {{--<table class="table table-hover">--}}
                {{--<tbody>--}}
                {{--<tr>--}}
                    {{--<th>ID</th>--}}
                    {{--<th>Логин</th>--}}
                    {{--<th>Фамилия</th>--}}
                    {{--<th>Имя</th>--}}
                    {{--<th>Дата регистрации</th>--}}
                    {{--<th>Последняя активность</th>--}}
                {{--</tr>--}}
                {{--@foreach ($referral as $refer)--}}
                    {{--<tr>--}}
                        {{--<td>{{ $refer['id'] }}</td>--}}
                        {{--<td>{{ $refer['login'] }}</td>--}}
                        {{--<td>{{ $refer['users_data']['lastname'] }}</td>--}}
                        {{--<td>{{ $refer['users_data']['name'] }}--}}
                        {{--</td>--}}
                        {{--<td>{{ $refer['regdate'] }}</td>--}}
                        {{--<td>{{ $refer['last_active'] }}</td>--}}
                    {{--</tr>--}}
                {{--@endforeach--}}
                {{--</tbody>--}}
            {{--</table>--}}
        {{--</div>--}}
        {{--<div class="box-footer clearfix">--}}
            {{--{{$referral->appends(request()->input())->links()}}--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection
