<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <title>Document</title>
</head>
<body>
    <p class="js-site-redactor">das</p>
    <div class="site-redactor-panel mt-10" style="height: 0px; overflow: hidden;">
        <div class="in-panel" style="border: 1px solid #000; height: 200px; overflow: hidden; background: #aaa;">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deserunt, soluta.
        </div>
    </div>
</body>
<script>
    $(document).ready(function () {
        let inPanel = $('.in-panel'),
            panel = $('.site-redactor-panel');

        $('.js-site-redactor').on('click', function () {
            console.log(inPanel.height());
            if (panel.height() != 0) {
                panel.animate({height: 0}, 1000, 'linear');
            } else {
                panel.animate({height: inPanel.height()}, 1000, 'linear');
            }
        });
    });
</script>
</html>