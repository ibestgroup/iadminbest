@extends("layout.app")

@section('title', 'Список рефералов')

@section('content')
    <div class="box" id="eee">
        <div class="box-header">
            <p><a class="btn btn-primary" href="/profile/tickets">К списку тикетов</a></p>
            <h3 class="box-title">#{{$ticket['id_ticket']}}/{{$ticket['name_ticket']}}</h3>
        </div>
        <!-- /.box-header -->
                <div class="box-body">
                    {{$ticket['text']}}
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    {{$ticket['date']}}
                </div>
                <!-- /.box-footer-->
                <div class="box-footer box-comments">
                    @foreach ($answers as $answer)
                    <div class="box-comment">
                        <!-- User image -->
                        <img class="img-circle img-sm" src="{{Auth::user()->avatar}}" alt="User Image">

                        <div class="comment-text">
                              <span class="username">
                                {{$answer['id_user']}}
                                <span class="text-muted pull-right">{{$answer['date']}}</span>
                              </span><!-- /.username -->
                            {{$answer['text']}}
                        </div>
                        <!-- /.comment-text -->
                    </div>
                    @endforeach
                    <!-- /.box-comment -->
                </div>
        <div class="box-footer">
            <form action="" method="post" class="js-answerAdd">
                <img class="img-responsive img-circle img-sm" src="../dist/img/user4-128x128.jpg" >
                <!-- .img-push is used to add margin to elements next to floating images -->
                <div class="img-push">
                    {{ csrf_field() }}
                    <input type="hidden" name="id_ticket" value="{{$ticket['id_ticket']}}">
                    <input type="hidden" name="id_user" value="{{Auth::user()->id}}">
                    <div class="form-group">
                        <input type="text" name="text" class="form-control input-sm" placeholder="Press enter to post comment">
                    </div>
                    <button type="submit" class="btn btn-primary">Посмотреть</button>
                </div>
            </form>
        </div>
        <div class="box-footer clearfix">
            {{$answers->appends(request()->input())->links()}}
        </div>
    </div>
@endsection
