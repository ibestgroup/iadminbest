@extends("layout.app")

@section('title', 'Тикеты')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Тикеты</h3>

            <div class="box-tools">
                <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
                <tbody>
                <tr>
                    <th>ID</th>
                    <th>Название</th>
                    <th>Пользователь</th>
                    <th>Статус</th>
                    <th>Дата</th>
                </tr>
                @foreach ($tickets as $ticket)
                    <tr onclick="window.open('{{route('goToTicket', ['id' => $ticket['id_ticket']])}}', '_self')">
                        <td>{{ $ticket['id_ticket'] }}</td>
                        <td>{{ $ticket['name_ticket'] }}</td>
                        <td>{{ $ticket['id_user'] }}</td>
                        <td>{{ $ticket['open'] }}</td>
                        <td>{{ $ticket['date'] }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="box-footer clearfix">
            {{$tickets->appends(request()->input())->links()}}
        </div>
    </div>
@endsection
