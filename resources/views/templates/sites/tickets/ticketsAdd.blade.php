@extends("layout.app")

@section('title', 'Создать тикет')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <div class="box-header">
                        <h3 class="box-title">Задать вопрос</h3>
                    </div>

                    <div class="box-body">
                        <form action="{{route('addTicket')}}" role="form" method="post" class="js-ticketAdd">
                            {{ csrf_field() }}
                            <!-- text input -->
                            <div class="form-group">
                                <label>Название</label>
                                <input type="text" class="form-control" placeholder="Название" name="name">
                            </div>

                            <!-- textarea -->
                            <div class="form-group">
                                <label>Описание</label>
                                <textarea class="form-control" rows="3" placeholder="Суть вопроса" name="text"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Задать вопрос</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
