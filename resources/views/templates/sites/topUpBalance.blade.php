@extends("layout.app")

@section('title', 'Информация о тарифе')

@section('content')
    <div class="col-lg-12" style="background: #fff; border-radius: 13px; border: 4px solid #eee; min-height: 200px; padding: 30px; font-size: 17px; font-family: Arial;">
        <p style="padding-top: 10px;">Вам необходимо сделать перевод денежных средств на указанный ниже Payeer кошелек с комментарием.</p>
        <p style="padding-top: 10px;">Внимание, указание комментария обязательно, в противном случае платеж не будет зачислен.</p>
        <div class="col-lg-6 ta-center">
            <h2>Кошелек</h2>
            {{--<span id="SPAN_1">P26340592</span>--}}
            <span id="SPAN_1">P1051636730</span>
            {{--<span id="SPAN_1">P1008663143</span>--}}
        </div>
        <div class="col-lg-6 ta-center">
            <h2>Комментарий</h2>
            <span id="SPAN_1">{{$comment}}</span>
        </div>
        <div class="clearfix"></div>
        <p style="padding-top: 20px;">После перевода, нажмите кнопку "Проверить" и денежные средства зачислятся на баланс.</p>
        <form action="{{route('checkPaymentPayeer')}}" class="js-store">
            <input type="hidden" name="user_id" value="{{Auth::id()}}">
            <button id="BUTTON_1" style="margin: auto;" >
                ПРОВЕРИТЬ
            </button>
        </form>
        <h1 class="ta-center">Ваши пополнения</h1>
        <table width="100%" class="ta-center">
            <tr>
                <th style="text-align: center;">ID</th>
                <th style="text-align: center;">Сумма</th>
                <th style="text-align: center;">Дата</th>
            </tr>
            @forelse($payments as $item)
                <tr>
                    <td>{{$item->payment_id}}</td>
                    <td>{{$item->amount}} {!! \App\Libraries\Helpers::siteCurrency('icon') !!}</td>
                    <td>{{$item->date}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="3">
                        У вас нет ни одного пополнения этим способом
                    </td>
                </tr>
            @endforelse
        </table>
    </div>
    <style>
        #SPAN_1 {
            border-block-end-color: rgb(51, 204, 0);
            border-block-start-color: rgb(51, 204, 0);
            border-inline-end-color: rgb(51, 204, 0);
            border-inline-start-color: rgb(51, 204, 0);
            caret-color: rgb(51, 204, 0);
            color: rgb(51, 204, 0);
            column-rule-color: rgb(51, 204, 0);
            cursor: default;
            overflow-wrap: break-word;
            padding-block-end: 9px;
            padding-block-start: 11px;
            padding-inline-end: 24px;
            padding-inline-start: 24px;
            perspective-origin: 0px 0px;
            text-align: center;
            text-decoration: none solid rgb(51, 204, 0);
            transform-origin: 0px 0px;
            background: rgba(51, 204, 0, 0.1) none repeat scroll 0% 0% / auto padding-box border-box;
            border: 0px none rgb(51, 204, 0);
            border-radius: 10px;
            font: 700 21px / 28px arial, normal;
            outline: rgb(51, 204, 0) none 0px;
            padding: 11px 24px 9px;
        }/*#SPAN_1*/

        #SPAN_1:after {
            border-block-end-color: rgb(51, 204, 0);
            border-block-start-color: rgb(51, 204, 0);
            border-inline-end-color: rgb(51, 204, 0);
            border-inline-start-color: rgb(51, 204, 0);
            caret-color: rgb(51, 204, 0);
            color: rgb(51, 204, 0);
            column-rule-color: rgb(51, 204, 0);
            cursor: default;
            overflow-wrap: break-word;
            text-align: center;
            text-decoration: none solid rgb(51, 204, 0);
            border: 0px none rgb(51, 204, 0);
            font: 700 21px / 28px arial, normal;
            outline: rgb(51, 204, 0) none 0px;
        }/*#SPAN_1:after*/

        #SPAN_1:before {
            border-block-end-color: rgb(51, 204, 0);
            border-block-start-color: rgb(51, 204, 0);
            border-inline-end-color: rgb(51, 204, 0);
            border-inline-start-color: rgb(51, 204, 0);
            caret-color: rgb(51, 204, 0);
            color: rgb(51, 204, 0);
            column-rule-color: rgb(51, 204, 0);
            cursor: default;
            overflow-wrap: break-word;
            text-align: center;
            text-decoration: none solid rgb(51, 204, 0);
            border: 0px none rgb(51, 204, 0);
            font: 700 21px / 28px arial, normal;
            outline: rgb(51, 204, 0) none 0px;
        }/*#SPAN_1:before*/
        #BUTTON_1 {
            align-items: center;
            appearance: none;
            block-size: 49px;
            border-block-end-color: rgba(0, 0, 0, 0);
            border-block-end-style: solid;
            border-block-end-width: 3px;
            border-block-start-color: rgba(0, 0, 0, 0);
            border-block-start-style: solid;
            border-block-start-width: 3px;
            border-inline-end-color: rgba(0, 0, 0, 0);
            border-inline-end-style: solid;
            border-inline-end-width: 3px;
            border-inline-start-color: rgba(0, 0, 0, 0);
            border-inline-start-style: solid;
            border-inline-start-width: 3px;
            bottom: 0px;
            box-shadow: rgb(53, 117, 46) 0px 7px 15px -11px;
            caret-color: rgb(255, 255, 255);
            color: rgb(255, 255, 255);
            column-rule-color: rgb(255, 255, 255);
            cursor: pointer;
            display: flex;
            height: 49px;
            inline-size: 159.25px;
            inset-block-end: 0px;
            inset-block-start: 0px;
            inset-inline-end: 0px;
            inset-inline-start: 0px;
            left: 0px;
            letter-spacing: 1px;
            padding-block-end: 13.5px;
            padding-block-start: 13.5px;
            padding-inline-end: 33px;
            padding-inline-start: 33px;
            perspective-origin: 79.625px 24.5px;
            position: relative;
            right: 0px;
            text-decoration: none solid rgb(255, 255, 255);
            text-size-adjust: 100%;
            text-transform: uppercase;
            top: 0px;
            transform-origin: 79.625px 24.5px;
            width: 159.25px;
            background: rgb(65, 216, 0) none repeat scroll 0% 0% / auto padding-box border-box;
            border: 3px solid rgba(0, 0, 0, 0);
            border-radius: 25px;
            font: 700 14px / 16.1px "Proxima Nova", sans-serif;
            outline: rgb(255, 255, 255) none 0px;
            padding: 13.5px 33px;
            transition: background 0.3s ease-out 0s, opacity 0.3s ease-out 0s, color 0.3s ease 0s, border-color 0.3s ease 0s, transform 0.3s ease-out 0s;
        }/*#BUTTON_1*/

        #BUTTON_1:after {
            border-block-end-color: rgb(255, 255, 255);
            border-block-start-color: rgb(255, 255, 255);
            border-inline-end-color: rgb(255, 255, 255);
            border-inline-start-color: rgb(255, 255, 255);
            caret-color: rgb(255, 255, 255);
            color: rgb(255, 255, 255);
            column-rule-color: rgb(255, 255, 255);
            cursor: pointer;
            display: none;
            letter-spacing: 1px;
            text-decoration: none solid rgb(255, 255, 255);
            text-size-adjust: 100%;
            text-transform: uppercase;
            border: 0px none rgb(255, 255, 255);
            font: 700 14px / 16.1px "Proxima Nova", sans-serif;
            outline: rgb(255, 255, 255) none 0px;
        }/*#BUTTON_1:after*/

        #BUTTON_1:before {
            border-block-end-color: rgb(255, 255, 255);
            border-block-start-color: rgb(255, 255, 255);
            border-inline-end-color: rgb(255, 255, 255);
            border-inline-start-color: rgb(255, 255, 255);
            caret-color: rgb(255, 255, 255);
            color: rgb(255, 255, 255);
            column-rule-color: rgb(255, 255, 255);
            cursor: pointer;
            display: block;
            letter-spacing: 1px;
            text-decoration: none solid rgb(255, 255, 255);
            text-size-adjust: 100%;
            text-transform: uppercase;
            border: 0px none rgb(255, 255, 255);
            font: 700 14px / 16.1px "Proxima Nova", sans-serif;
            outline: rgb(255, 255, 255) none 0px;
        }/*#BUTTON_1:before*/

    </style>
@endsection