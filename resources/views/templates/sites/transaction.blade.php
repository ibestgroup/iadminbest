@extends("layout.app")

@section('title', 'История операций')

@section('content')


    @php
        $template = \App\Models\Templates::where('status', 1)->first()['id'];
        $mainStyle = unserialize(\App\Models\SiteElements::where('name', 'main-style')->where('template', $template)->first()['option_value']);

    @endphp
    @include('templates.sites.pagebuilder.editPanel', array('page' => 'transaction'))
    <div class="row main-main">
        {{--Рекламное объявление--}}
        @if ($ad['state'])
            <div class="col-lg-12">
                <div class="alert alert-{{$ad['type']}} alert-dismissible">
                    <h4><i class="icon fa fa-info"></i> {{$ad['head']}}</h4>
                    {{$ad['text']}}
                </div>
            </div>
            <div class="clearfix"></div>
        @endif


        <div class="profile-page page-content">
            <div class="col-lg-12 p-0 page-refresh for-grid" data-id="content">
                {{\App\Http\Controllers\ProfileController::pageConstruct('transaction')}}
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
        @include('templates.sites.pagebuilder.modals', array('page' => 'transaction'))
    </div>

    {{--<div class="row">--}}
    {{--<div class="col-md-12">--}}
        {{--<div class="box box-info">--}}
            {{--<div class="box-header with-border">--}}
                {{--<h3 class="box-title">Фильтры</h3>--}}
            {{--</div>--}}
            {{--<div class="box-body">--}}
                {{--<form action="{{route('lastday')}}">--}}
                    {{--<div class="form-group has-feedback">--}}
                    {{--<input type="checkbox" value="Y" name="cashout" {{($filter['cashout'] == 'Y')?'checked':''}}> Выводы<br>--}}
                    {{--<input type="checkbox" value="Y" name="transaction" {{($filter['transaction'] == 'Y')?'checked':''}}> Внутренние транзакции<br>--}}
                    {{--<input type="checkbox" value="Y" name="payment" {{($filter['payment'] == 'Y')?'checked':''}}> Пополнения<br>--}}
                    {{--</div>--}}
                    {{--<div class="form-group">--}}
                        {{--<button type="submit" class="btn btn-primary">Показать</button>--}}
                    {{--</div>--}}
                {{--</form>--}}
            {{--</div>--}}
            {{--<!-- /.box-body -->--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="box">--}}
        {{--<div class="box-header">--}}
            {{--<h3 class="box-title">История операций</h3>--}}

            {{--<div class="box-tools">--}}
                {{--<div class="input-group input-group-sm" style="width: 150px;">--}}
                    {{--<input type="text" name="table_search" class="form-control pull-right" placeholder="Search">--}}

                    {{--<div class="input-group-btn">--}}
                        {{--<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- /.box-header -->--}}
        {{--<div class="box-body table-responsive no-padding">--}}
            {{--<table class="table table-hover transactions">--}}
                {{--<tbody>--}}
                    {{--<tr>--}}
                        {{--<th>#</th>--}}
                        {{--<th>Тип</th>--}}
                        {{--<th>От кого</th>--}}
                        {{--<th>Кому</th>--}}
                        {{--<th>Сумма</th>--}}
                        {{--<th>Статус</th>--}}
                        {{--<th>Дата</th>--}}
                    {{--</tr>--}}

                    {{--@foreach ($items as $key => $item)--}}
                        {{--@if ($item['table'] == 'payment')--}}
                            {{--<tr class="transaction-payment">--}}
                                {{--<td>{{ \App\Libraries\Helpers::idTransaction($item['id'], $item['table']) }}</td>--}}
                                {{--<td><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></td>--}}
                                {{--<td>Пополнение</td>--}}
                                {{--<td>{{ \App\Libraries\Helpers::convertUs('login', $item['payment_user']) }}</td>--}}
                                {{--<td>{{ $item['amount'] }} {!! \App\Libraries\Helpers::getCurrent($siteInfo['value'], 'icon') !!}</td>--}}
                                {{--<td><span class="label label-success">Подтверждена</span></td>--}}
                                {{--<td>{{ date('d-m-Y h:i:s', $item['date']) }}</td>--}}
                            {{--</tr>--}}
                        {{--@elseif ($item['table'] == 'payments_order')--}}
                            {{--<tr class="{{($item['to_user'] == Auth::id())?'transaction-incoming':'transaction-outgoing'}}">--}}
                                {{--<td>{{ \App\Libraries\Helpers::idTransaction($item['id'], $item['table']) }}</td>--}}
                                {{--@if ($item['to_user'] == Auth::id())--}}
                                    {{--<td><span class="glyphicon glyphicon-import" aria-hidden="true"></span></td>--}}
                                {{--@else--}}
                                    {{--<td><span class="glyphicon glyphicon-export" aria-hidden="true"></span></td>--}}
                                {{--@endif--}}
                                {{--<td>{{ \App\Libraries\Helpers::convertUs('login', $item['from_user']) }}</td>--}}
                                {{--<td>{{ \App\Libraries\Helpers::convertUs('login', $item['to_user']) }}</td>--}}
                                {{--<td>{{ $item['amount'] }} {!! \App\Libraries\Helpers::getCurrent($siteInfo['value'], 'icon') !!}</td>--}}
                                {{--<td><span class="label label-success">Подтверждена</span></td>--}}
                                {{--<td>{{ date('d-m-Y h:i:s', $item['date']) }}</td>--}}
                            {{--</tr>--}}
                        {{--@elseif ($item['table'] == 'cashout')--}}
                            {{--<tr class="transaction-cashout">--}}
                                {{--<td>{{ \App\Libraries\Helpers::idTransaction($item['id'], $item['table']) }}</td>--}}
                                {{--<td><span class="glyphicon glyphicon-send" aria-hidden="true"></span></td>--}}
                                {{--<td>Вывод</td>--}}
                                {{--<td>{{ $item['to']}}</td>--}}
                                {{--<td>{{ $item['amount'] }} {!! \App\Libraries\Helpers::getCurrent($siteInfo['value'], 'icon') !!}</td>--}}
                                {{--<td>--}}
                                    {{--@if ($item['status'] == 'Canceled')--}}
                                        {{--<span class="label label-danger">Отменена</span>--}}
                                    {{--@elseif($item['status'] == 'Completed')--}}
                                        {{--<span class="label label-success">Подтверждена</span>--}}
                                    {{--@elseif($item['status'] == 'New')--}}
                                        {{--<span class="label label-warning">В обработке</span>--}}
                                    {{--@endif--}}
                                {{--</td>--}}
                                {{--<td>{{ date('d-m-Y h:i:s', $item['date']) }}</td>--}}
                            {{--</tr>--}}
                        {{--@endif--}}
                    {{--@endforeach--}}
                {{--</tbody>--}}
            {{--</table>--}}
        {{--</div>--}}
        {{--<div class="box-footer clearfix">--}}
            {{--{{$items->links()}}--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection
