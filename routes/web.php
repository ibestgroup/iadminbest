<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome', array('title' => 'Dashboard'));
//});

Route::get('/payeer_907884420.txt', function () {
    return '907884420';
});
Route::get('/adbean_8b4ac0a011ccc00c.txt', function () {
    return '8b4ac0a011ccc00c';
});
Route::get('/51dc26d2748b32349ec72f033b9a143b.html', function () {
    return '51dc26d2748b32349ec72f033b9a143b';
});
Route::get('/fk-verify.html', function () {
    return '437a058c28b804b694db0d3592a9624b';
});
Route::get('/payeer_1088463316.txt', function () {
    return '1088463316';
    $fileText = "1088463316";
    $myName = "payeer_1088463316.txt";
    $headers = ['Content-type'=>'text/plain', 'test'=>'YoYo', 'Content-Disposition'=>sprintf('attachment; filename="%s"', $myName),'X-BooYAH'=>'WorkyWorky','Content-Length'=>sizeof($fileText)];
    return \Illuminate\Http\Response::make($fileText, 200, $headers);
});
Route::get('/ads.txt', function () {
    return 'google.com, pub-7640044855853499, DIRECT, f08c47fec0942fa0';
});
Route::get('/check-delegate', 'ParkingDomainController@codeDelegate')->name('codeDelegate');

Route::get('/get-site-info', 'AdminCPController@json')->name('getSiteInfo');
Route::get('/get-user-info', 'ProfileController@json')->name('getUserInfo');
Route::get('/get-user-info/{id}', 'ProfileController@json')->name('getUserInfo');

Route::group(['prefix' => 'profile',  'middleware' => 'auth'], function()
{

    Route::group(['prefix' => 'ibux'], function () {
        Route::get('/', 'BuxoneController@create');
        Route::post('/register', 'BuxoneController@register')->name('buxoneRegister');
        Route::post('/auth', 'BuxoneController@auth')->name('buxoneAuth');
        Route::post('/logout', 'BuxoneController@logout')->name('buxoneLogout');
//        Route::post('/cashout', 'BuxoneController@cashout')->name('buxoneCashout');
    });

//    Route::get('/balance', 'BalanceController@balancePage');
    Route::get('/balance-get', 'BalanceController@balance')->name('balance');

    Route::group(['prefix' => 'admin', 'middleware' => 'admin'], function () {
        Route::get('/', 'AdminCPController@view')->name('admin');
        Route::get('/users-load', 'AdminCPController@loadUsers')->name('loadUsers');
        Route::get('/users', 'AdminCPController@allUsers')->name('allUsers');
        Route::get('/users/{id}', 'AdminCPController@userInfo')->name('userInfo');
        Route::post('/user-edit', 'AdminCPController@userStore')->name('userStore');

        Route::get('/sand-box', 'AdminCPController@sandBoxCreate')->name('sandBox');
        Route::post('/sand-box', 'AdminCPController@sandBox')->name('sandBox');

        Route::get('/marks', 'AdminCPController@markEdit')->name('markEdit');
        Route::get('/marks-new', 'AdminCPController@markEditNew')->name('markEditNew');
        Route::post('/marks', 'AdminCPController@markStore')->name('markEdit');
        Route::post('/mark-create', 'AdminCPController@markCreate')->name('markCreate');
        Route::post('/mark-delete', 'AdminCPController@markDelete')->name('markDelete');

        Route::get('/setting', 'AdminCPController@siteInfoEdit')->name('setting');
        Route::post('/setting', 'AdminCPController@siteInfoStore')->name('setting');
        Route::post('/currency', 'AdminCPController@currencyStore')->name('currency');

        Route::group(['prefix' => 'parking'], function () {
            Route::get('/', 'ParkingDomainController@create')->name('parking');
            Route::post('/add', 'ParkingDomainController@store')->name('storeParking');
            Route::post('/delegate', 'ParkingDomainController@delegate')->name('delegateParking');
            Route::post('/delete', 'ParkingDomainController@delete')->name('deleteParking');
        });

        Route::get('/cashout-request', 'AdminCPController@cashoutRequestCreate')->name('cashoutRequest');
        Route::post('/cashout-request', 'AdminCPController@cashoutRequestConfirm')->name('cashoutRequestConfirm');

        Route::get('/payments', 'AdminCPController@paymentsAdmin')->name('paymentsAdmin');
        Route::get('/cashouts', 'AdminCPController@cashoutsAdmin')->name('cashoutsAdmin');
        Route::get('/activates', 'AdminCPController@activates')->name('activatesAdmin');

        Route::get('/auto-activate', 'AdminCPController@autoActivate')->name('autoActivate');
        Route::post('/auto-activate', 'AdminCPController@autoActivateStore')->name('autoActivateStore');

        Route::post('/edit-menu', 'AdminCPController@storeMenu')->name('storeMenu');
        Route::post('/edit-site', 'AdminCPController@storeSite')->name('storeSite');

        Route::get('/notify', 'AdminCPController@createNotify')->name('createNotify');
        Route::post('/edit-notify', 'AdminCPController@storeNotify')->name('storeNotify');

        Route::group(['prefix' => 'pages'], function () {
            Route::get('/', 'AdminCPController@pages')->name('pages');
            Route::post('/edit', 'AdminCPController@pagesStore')->name('pagesStore');
            Route::post('/add', 'AdminCPController@pagesAdd')->name('pagesAdd');
        });

        Route::group(['prefix' => 'templates'], function () {
            Route::get('/', 'AdminCPController@templates')->name('templates');
            Route::get('/market', 'AdminCPController@templatesMarket')->name('templatesMarket');
            Route::get('/library', 'AdminCPController@templatesLibrary')->name('templatesLibrary');
            Route::get('/{id}', 'AdminCPController@templates')->name('templateInfo');
            Route::post('/edit', 'AdminCPController@templatesStore')->name('templatesStore');
            Route::post('/delete', 'AdminCPController@templatesDelete')->name('templatesDelete');
            Route::post('/add', 'AdminCPController@templatesAdd')->name('templatesAdd');
            Route::post('/buy', 'AdminCPController@buyTemplate')->name('buyTemplate');
            Route::post('/duplicate', 'AdminCPController@templateDuplicate')->name('templateDuplicate');
            Route::post('/addStore', 'AdminCPController@templateToStore')->name('addToStore');
            Route::post('/delete-library', 'AdminCPController@templateDeleteFromLibrary')->name('deleteFromLibrary');
            Route::post('/edit-market', 'AdminCPController@templateMarketEdit')->name('templateMarketEdit');
        });

        Route::get('/auth-edit', 'AdminCPController@authPages')->name('authPages');
        Route::post('/auth-edit', 'AdminCPController@authPagesStore')->name('authPagesStore');



        //invest adminCP
        Route::group(['prefix' => 'deposits'], function () {
            Route::get('/', 'AdminCPController@createBirds')->name('birds');
            Route::post('/add', 'AdminCPController@storeBirds')->name('storeBirds');
            Route::post('/edit', 'AdminCPController@updateBirds')->name('updateBirds');
            Route::post('/delete', 'AdminCPController@deleteBirds')->name('deleteBirds');
        });

        Route::group(['prefix' => 'bonus'], function () {
            Route::post('/add', "AdminCPController@storeBonus")->name('storeBonus');
            Route::post('/delete', "AdminCPController@deleteBonus")->name('deleteBonus');
        });

        Route::post('/bonuse-click', "AdminCPController@updateBonusSetting")->name('updateBonusSetting');

        Route::group(['prefix' => 'types'], function () {
            Route::get('/', 'AdminCPController@typesOrderCreate')->name('createTypesOrder');
            Route::post('/add', 'AdminCPController@typesOrderStore')->name('storeTypesOrder');
            Route::post('/edit', 'AdminCPController@typesOrderEdit')->name('editTypesOrder');
            Route::post('/delete', 'AdminCPController@typesOrderDelete')->name('deleteTypesOrder');
        });

        Route::post('/affiliate', 'AdminCPController@affiliateStore')->name('affiliateStore');
        Route::post('/edit-menu', 'AdminCPController@storeMenu')->name('storeMenu');
        Route::post('/edit-site', 'AdminCPController@storeSite')->name('storeSite');
        Route::post('/edit-hashpower', 'AdminCPController@storeHashPower')->name('storeHashPower');

        Route::get('/birds-list', 'AdminCPController@birdsList')->name('birdsList');
        Route::post('/birds-list/delete', 'AdminCPController@birdDelete')->name('birdDelete');
        Route::post('/birds-list/edit', 'AdminCPController@birdEdit')->name('birdEdit');


        Route::get('/documentation', 'AdminCPController@documentation')->name('documentation');
        Route::get('/documentationNew', 'AdminCPController@documentationNew')->name('documentationNew');

        Route::group(['prefix' => 'bot-settings'], function() {
            Route::get('/', 'BotSettingsController@index')->name('botSettings');
            Route::get('/manual', 'BotSettingsController@manual')->name('botSettingsManual');
            Route::group(['prefix' => 'get-json'], function() {
                Route::get('/', 'BotSettingsController@getJSON')->name('botSettingsGetJSON');
                Route::get('/{id}', 'BotSettingsController@getJSON')->name('botSettingsGetJSONById');
            });

            Route::group(['prefix' => 'action'], function() {
                Route::post('/create', 'BotSettingsController@create')->name('botSettingsCreate');
                Route::post('/update', 'BotSettingsController@update')->name('botSettingsUpdate');
                Route::post('/remove', 'BotSettingsController@remove')->name('botSettingsRemove');
                Route::post('/save-move', 'BotSettingsController@saveMove')->name('botSettingsSaveMove');
                Route::post('/line-break', 'BotSettingsController@lineBreak')->name('botSettingsLineBreak');
            });

            Route::post('/add-api-key', 'BotSettingsController@addApiKey')->name('addApiKey');
            Route::post('/get-form-widget', 'BotSettingsController@getFormWidget')->name('getFormWidget');
        });

    });

    Route::group(['prefix' => 'chart'], function () {
        Route::get('/payments', 'ProfileController@chartPayments')->name('chartPayments');
        Route::get('/activation', 'ProfileController@chartActivation')->name('chartActivation');
        Route::get('/visit', 'ProfileController@chartVisit')->name('chartVisit');
        Route::get('/register', 'ProfileController@chartRegister')->name('chartRegister');
    });

    Route::group(['prefix' => 'profile-builder', 'middleware' => 'admin'], function () {
        Route::post('/', 'ProfileBuilderController@store')->name('profileBuilder');
        Route::post('/update-element', 'ProfileBuilderController@updateElement')->name('updateElement');
    });

    Route::post('/get-selectoption', '\App\libraries\Helpers@selectOptionPost')->name('getSelectOption');

    Route::post('/update-sort', 'ProfileBuilderController@updateSort')->name('updateSort');
    Route::post('/info-element/{id}', 'ProfileBuilderController@infoElement')->name('infoElement');

    Route::get('/get-qr', 'ProfileController@getQR')->name('qr');
    Route::post('/roulette', 'ProfileController@roulette')->name('roulette');

    Route::get('/list-pidorov', 'ProfileController@listPidorov');



    // invest
    Route::get('/hash-power', 'AffiliateController@createHashPower')->name('createHashPower');
    Route::get('/affiliate', 'AffiliateController@createAffiliate')->name('createAffiliate');

//    Route::get('/cashout', 'CashoutController@create')->name('cashout');
    Route::post('/cashout', 'CashoutController@store')->name('cashout');
//    Route::get('/cashout-for-buy', 'CashoutController@createCashoutForBuy')->name('cashoutForBuy');
    Route::post('/cashout-for-buy', 'CashoutController@storeCashoutForBuy')->name('cashoutForBuy');

    Route::group(['prefix' => 'bonus'], function () {
        Route::get('/', 'BonusClickController@create')->name('bonus');
        Route::post('/get', 'BonusClickController@store')->name('getBonus');
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('/', 'BirdsController@create')->name('allBirds');
        Route::post('/', 'BirdsController@store')->name('buyBirds');

        Route::get('/my-orders', 'BirdsController@myBirds')->name('myBirds');
        Route::post('/collect', 'BirdsController@collectEgg')->name('collectEgg');

        Route::get('/stock', 'BirdsController@createStock')->name('createStock');
        Route::post('/stock-cashout', 'BirdsController@storeStock')->name('storeStock');

        Route::get('/resources-info', 'BirdsController@resourcesInfo')->name('resourcesInfo');
    });



    Route::get('/edit', 'ProfileController@showEditForm')->name('profileEdit');
    Route::post('/edit', 'ProfileController@store')->name('profile.edit');

    Route::get('/{uid}/widgets', 'WidgetsController@list')->where('uid', '[0-9]+');
    Route::get('/{uid}/widgets/{id}', 'WidgetsController@specific')->where(['uid' => '[0-9]+', 'id' => '[0-9]+']);


    Route::get('/notice', 'NoticeController@all')->name('notice');


    Route::get('/activate', 'ActivateController@activate')->name('activate');
    Route::get('/activate-page', 'ActivateController@create')->name('activatePage');


    Route::get('/referalTree', 'ReferalTree@create')->name('referalTree');
    Route::get('/referalTree/{id}', 'ReferalTree@detail')->where('id', '[0-9]+')->name('treeDetail');
//    Route::get('/referalTree/info/{id}', 'ReferalTree@info')->where('id', '[0-9]+')->name('treeDetail');

    Route::post('/cashout-order', 'CashoutController@cashoutOrder')->name('cashoutOrder');

//    Route::get('/cashout', 'CashoutController@create')->name('cashout');
    Route::post('/cashout', 'CashoutController@store')->name('cashout');

    Route::get('/transaction', 'TransactionController@all')->name('transaction');

    Route::group(['prefix' => 'payments'], function () {
        Route::get('/', 'PaymentsController@all')->name('payments');
        Route::get('/all', 'PaymentsController@incoming');
    });

    Route::group(['prefix' => 'referral-list'], function () {
        Route::get('/', 'ReferralListController@all')->name('referralList');
        Route::get('/active', 'ReferralListController@active')->name('referralListActive');
        Route::get('/reg-lastday', 'ReferralListController@regLastDay')->name('lastday');
    });

    Route::group(['prefix' => 'tickets'], function () {
        Route::get('/', 'TicketsController@all')->name('tickets');
        Route::get('/add', 'TicketsController@addTicket')->name('addTicket');
        Route::get('/{id}', 'TicketsController@view')->name('goToTicket')->where('id', '[0-9]+');

        Route::post('/add', 'TicketsController@store')->name('addTicket');
        Route::post('/{id}', 'TicketsController@addAnswer')->name('addAnswer')->where('id', '[0-9]+');
    });

    Route::group(['prefix' => 'news'], function () {
        Route::get('/', 'NewsController@all')->name('news');
        Route::get('/{id}', 'NewsController@view')->name('goToNews')->where('id', '[0-9]+');

        Route::get('/add', 'NewsController@addNews')->name('addNews')->middleware('admin');
        Route::post('/add', 'NewsController@store')->name('addNews');
        Route::post('/delete', 'NewsController@delete')->name('deleteNews');
    });

    Route::group(['prefix' => 'faq'], function () {
//        Route::get('/', 'FaqController@all')->name('faq');
        Route::get('/edit/{id}', 'FaqController@info')->name('infoFaq');

        Route::get('/add', 'FaqController@faqAdd')->name('addFaq');
        Route::post('/add', 'FaqController@store')->name('addFaq');
        Route::post('/delete', 'FaqController@delete')->name('deleteFaq');
        Route::post('/edit', 'FaqController@edit')->name('editFaq');
    });

    Route::group(['prefix' => 'promo'], function () {
        Route::get('/', 'PromoController@all')->name('promo');
        Route::get('/add', 'PromoController@promoAdd')->name('addPromo');
        Route::get('/edit/{id}', 'PromoController@promoViewEdit')->name('editViewPromo');

        Route::post('/add', 'PromoController@store')->name('addPromo');
        Route::post('/edit', 'PromoController@promoEdit')->name('editPromo');
        Route::post('/delete', 'PromoController@delete')->name('deletePromo');
    });

    Route::group(['prefix' => 'ad'], function () {
        Route::get('/', 'AdController@adEdit')->name('ad');
        Route::post('/edit', 'AdController@store')->name('editAd');
    });

    Route::group(['prefix' => 'friends'], function () {
        Route::get('/', 'FriendsController@create')->name('friends');
        Route::post('/add', 'FriendsController@addFriend')->name('addFriend');
        Route::post('/confirm', 'FriendsController@confirmFriend')->name('confirmFriend');
        Route::post('/delete', 'FriendsController@deleteFriend')->name('deleteFriend');
    });

    Route::post('/create-site', 'CreateSiteController@store')->name('createSite');

    Route::group(['prefix' => 'affiliate'], function () {
        Route::get('/', 'AffiliateController@createAffiliate')->name('createAffiliate');
        Route::get('/user/{id}', 'AffiliateController@userInfo')->name('createUserAffiliate');
        Route::get('/site/', 'AffiliateController@sitesInfo')->name('createSiteAffiliate');
        Route::get('/site/{id}', 'AffiliateController@siteInfo')->name('createSiteAffiliate');
    });

    Route::get('/videodoc', 'ProfileController@showVideoDoc');





    Route::get('/', 'ProfileController@create')->name('profile');
    Route::get('/user/{id}', 'ProfileController@create')->where('id', '[0-9]+');
    Route::get('/{page}', 'ProfileController@create')->name('page');

});

//Route::get('/activatetest', 'ActivateController@activate');

/*
 * Callback processing
 */
// Payeer
Route::get('/topupbalance', 'BalanceController@paymentPayeer')->middleware('auth')->name('paymentPayeer');
Route::post('/checkpaymentpayeer', 'BalanceCallbackController@checkPaymentPayeer')->middleware('auth')->name('checkPaymentPayeer');

Route::post('/checkpay', 'BalanceCallbackController@checkPay')->name('checkPay');
Route::get('/successpay', 'BalanceCallbackController@successPay')->name('successPay');
Route::post('/failspay', 'BalanceCallbackController@failsPay')->name('failsPay');
// PerfectMoney
Route::post('/checkpaypm', 'BalanceCallbackController@checkPayPM')->name('checkPayPM');
Route::get('/successpaypm', 'BalanceCallbackController@successPayPM')->name('successPayPM');
Route::post('/failspaypm', 'BalanceCallbackController@failsPayPM')->name('failsPayPM');
// Crypto(Coinpayments)
Route::post('/checkpaycrypto', 'BalanceCallbackController@checkPayCrypto')->name('checkPayCrypto');
Route::get('/successpaycrypto', 'BalanceCallbackController@successPayCrypto')->name('successPayCrypto');
Route::post('/failspaycrypto', 'BalanceCallbackController@failsPayCrypto')->name('failsPayCrypto');
// Betatransfer
Route::post('/checkpaybeta', 'BalanceCallbackController@checkPayBeta')->name('checkPayBeta');
//Route::get('/successpaybeta', 'BalanceCallbackController@successPayBeta')->name('successPayBeta');
//Route::get('/failspaybeta', 'BalanceCallbackController@failsPayBeta')->name('failsPayBeta');
Route::post('/checkcashoutbeta', 'BalanceCallbackController@checkCashoutBeta')->name('checkCashoutBeta');
// FinestHour
Route::get('/checkpayfinest', 'BalanceCallbackController@checkPayFinest')->name('checkPayFinest');
//Route::get('/successpayfinest', 'BalanceCallbackController@successPayFinest')->name('successPayFinest');
//Route::get('/failspayfinest', 'BalanceCallbackController@failsPayFinest')->name('failsPayFinest');
// Obmenka
// For POST-notify paste in console on payments page
// $('.PList div:nth-child(2) span.PField').each(function() {
//      let link = 'https://iadmin.work/checkpayobm?payment_id=' + $(this).text();
//      $(this).append('<a href="'+link+'" target="blank">Уведомить</a>');
// });

Route::get('/checkpayobm', 'BalanceCallbackController@checkPayObm')->name('checkPayObm');

Route::post('/checkpayfk', 'BalanceCallbackController@checkPayFK')->name('checkPayFK');
Route::get('/successpayfk', 'BalanceCallbackController@successPayFK')->name('successPayFK');
Route::post('/successpayfk', 'BalanceCallbackController@successPayFK')->name('successPayFK');
Route::post('/failspayfk', 'BalanceCallbackController@failsPayFK')->name('failsPayFK');





Route::get('/landing-edit', 'ConstructorController@create')->name('landingEdit');
Route::post('/landing-edit/iupload', function () {
    include base_path().'/constructor/iupload.php';
})->name('landingEditUpload');

Route::get('/verify/{token}', 'Auth\VerificationController@update');

Auth::routes(['verify' => true]);

Route::get('/logout', 'Auth\LogoutController@logout');



Route::get('/paymentcrypto', 'BalanceController@paymentCrypto')->name('paymentCrypto');

//Route::post('/telegrambot', 'TelegramController@telegram')->name('telegrambot');
Route::post('/telegram-add', 'TelegramController@telegramAdd')->name('telegramadd');
Route::post('/telegram-confirm', 'TelegramController@telegramConfirm')->name('telegramconfirm');
Route::post('/telegram-unlink', 'TelegramController@telegramUnlink')->name('telegramunlink');
Route::post('/telegram-sendcode', 'TelegramController@sendCode')->name('telegramSendCode');
Route::post('/telegram-notice', 'TelegramController@telegramNoticeEdit')->name('telegramNoticeEdit');

Route::post('/telegrambot', 'iAuthController@iAuth');
Route::post('/vkbot', 'iAuthController@iAuth');
Route::post('/i-auth', 'iAuthController@iAuth');
Route::post('/i-auth-add', 'iAuthController@iAuthAdd')->name('iAuthAdd');
Route::post('/i-auth-confirm', 'iAuthController@iAuthConfirm')->name('iAuthConfirm');
Route::post('/i-auth-unlink', 'iAuthController@iAuthUnlink')->name('iAuthUnlink');
Route::post('/i-auth-sendcode', 'iAuthController@sendCode')->name('iAuthSendCode');
Route::post('/i-auth-notice', 'iAuthController@iAuthNoticeEdit')->name('iAuthNoticeEdit');
Route::post('/i-auth-check-pair', 'iAuthController@iAuthCheckPair')->name('iAuthCheckPair');

//Route::get('/vktest', 'VkController@testVk');
//Route::post('/vk-add', 'VkController@vkAdd')->name('vkadd');
//Route::post('/vk-confirm', 'VkController@vkConfirm')->name('vkconfirm');
//Route::post('/vk-unlink', 'VkController@vkUnlink')->name('vkunlink');
//Route::post('/vk-sendcode', 'VkController@sendCode')->name('vkSendCode');
//Route::post('/vk-notice', 'VkController@vkNoticeEdit')->name('vkNoticeEdit');




/**
 * Index and fails
 */

Route::post('/handler-crypto', 'CryptoHandlerController@handler')->name('handlerCrypto');
Route::fallback('StartPageController@showPageOrFail');
Route::get('/', 'StartPageController@start');
Route::get('/marketing', 'StartPageController@marketing');



/**
 * Testteleg bot
 */

Route::post('/telegbot', 'TelegramBotController@telegBot');