let mix = require('laravel-mix'),
    path = '';
mix.setResourceRoot(path);
mix.setPublicPath(path);
mix.options({
    processCssUrls: false
});
mix.autoload({
    jquery: ['$', 'global.jQuery',"jQuery","global.$","jquery","global.jquery"]
});
mix.webpackConfig({
    module: {
        rules: [{
            test: /\.less/,
            enforce: "pre",
            loader: 'import-glob-loader'
        }, {
            test: /\.(woff2?|ttf|eot|svg|otf)$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                publicPath: path + 'public/fonts/',
                context: 'public',
                emitFile: false
            }
        }]
    }
}).less('resources/assets/less/Main.less', 'public/css/app.css', {sourceMap: true, omitSourceMapUrl: false})
    .react([
        'resources/assets/js/vendor/Tree.js',
        'resources/assets/js/vendor/TodoList.js',
        'resources/assets/js/vendor/PushMenu.js',
        'resources/assets/js/vendor/Layout.js',
        'resources/assets/js/vendor/ControlSidebar.js',
        'resources/assets/js/vendor/BoxWidget.js',
        'resources/assets/js/vendor/BoxRefresh.js',
        'resources/assets/js/vendor/demo.js',
        'resources/assets/js/vendor/adminlte.min.js',
        'resources/assets/js/app.js',
    ], 'public/js/app.js').version();

// mix.disableNotifications();
